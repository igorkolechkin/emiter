<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 15:20:41
         compiled from "8c579b1c649e72baaff4398475ee6e34853ce2e2" */ ?>
<?php /*%%SmartyHeaderCode:8848533095b23af1939be70-24985670%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c579b1c649e72baaff4398475ee6e34853ce2e2' => 
    array (
      0 => '8c579b1c649e72baaff4398475ee6e34853ce2e2',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '8848533095b23af1939be70-24985670',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23af193befc3_09754394',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23af193befc3_09754394')) {function content_5b23af193befc3_09754394($_smarty_tpl) {?><fieldset id="guifieldset_mWnYaX" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_NuL3yg" class="control-group">
            <label class="control-label" for="l__">
            Активный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_active').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_active" style="width:80px;" name="is_active">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_YuX2jH" class="control-group">
            <label class="control-label" for="l__">
            Индексировать            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_index').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_index" style="width:80px;" name="is_index">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_9Xvp4A" class="control-group">
            <label class="control-label" for="l__">
            В СЕО шаблонах            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_seo_template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_seo_template" style="width:80px;" name="in_seo_template">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_6HD5TO" class="control-group">
            <label class="control-label" for="l__">
            Системный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_system').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_system" style="width:80px;" name="is_system">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_ql4S2a" class="control-group">
            <label class="control-label" for="l__">
            Обязательный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_required').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_required" style="width:80px;" name="is_required">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_blniGI" class="control-group">
            <label class="control-label" for="l__">
            Уникальный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique" style="width:80px;" name="is_unique">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_SYJCRi" class="control-group">
            <label class="control-label" for="l__">
            Уникальный в родителе            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique_parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique_parent" style="width:80px;" name="is_unique_parent">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_4g5HFX" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable" style="width:80px;" name="is_configurable">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_SNn91P" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара (материалы)            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable_material').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable_material" style="width:80px;" name="is_configurable_material">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_f32oom" class="control-group">
            <label class="control-label" for="l__">
            Содержит несколько значений            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_multiple').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_multiple" style="width:80px;" name="is_multiple">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_IRhhx4" class="control-group">
            <label class="control-label" for="l__">
            В списке товаров            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_list').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_list" style="width:80px;" name="is_in_list">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_WqQodc" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров подробно            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card" style="width:80px;" name="is_in_card">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_KdHz7E" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров кратко             
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card_short').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card_short" style="width:80px;" name="is_in_card_short">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_DmvKUY" class="control-group">
            <label class="control-label" for="l__">
            В фильтре            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter" style="width:80px;" name="is_in_filter">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_xKsGCx" class="control-group">
            <label class="control-label" for="l__">
            В фильтре свернутый            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter_hidden').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter_hidden" style="width:80px;" name="is_in_filter_hidden">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_nuwbxp" class="control-group">
            <label class="control-label" for="l__">
            В поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search" style="width:80px;" name="is_in_search">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_2fVI23" class="control-group">
            <label class="control-label" for="l__">
            В расширенном поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search_advanced').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search_advanced" style="width:80px;" name="is_in_search_advanced">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_aSrx65" class="control-group">
            <label class="control-label" for="l__">
            Мультиязычный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#split_by_lang').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="split_by_lang" style="width:80px;" name="split_by_lang">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><?php }} ?>