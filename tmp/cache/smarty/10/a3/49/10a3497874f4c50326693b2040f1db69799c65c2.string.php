<?php /* Smarty version Smarty-3.1.13, created on 2018-06-19 11:33:05
         compiled from "10a3497874f4c50326693b2040f1db69799c65c2" */ ?>
<?php /*%%SmartyHeaderCode:20286700665b28bfc1ef5d86-53007611%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10a3497874f4c50326693b2040f1db69799c65c2' => 
    array (
      0 => '10a3497874f4c50326693b2040f1db69799c65c2',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '20286700665b28bfc1ef5d86-53007611',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b28bfc1f24251_37149734',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b28bfc1f24251_37149734')) {function content_5b28bfc1f24251_37149734($_smarty_tpl) {?><script>
    $(function(){
        $('#guitabs_nx9sy5').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="guitabs_nx9sy5" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#guitabs_nx9sy5_general"><span class="dotted">Общие</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="guitabs_nx9sy5_general">
                <div fnx="true" class="row-fluid"><div fnx="true" class="span6"><fieldset id="guifieldset_IkaHa0" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_TI5scR" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Жесткость фильтр" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_kYEpto" class="control-group">
            <label class="control-label" for="l__">
            Единица измерения            
                    </label>
                <div class="controls"><div id="input-container-3e34bdebd9bd5edda27e8728904a2552">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="unit_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="unit_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-ea41b99f7844c44b6740e1fb531a3e9c",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_72hdSY" class="control-group">
            <label class="control-label" for="l__">
            Системное название            
                    </label>
                <div class="controls"><div id="input-container-e266081b6e1f85de55ebe69e568928b9">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="sys_title" value="Zhestkost_filtr" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="sys_title"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-e266081b6e1f85de55ebe69e568928b9",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_KaasM7" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_efCD5P" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#details_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="details_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#details_tabs_details_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="details_tabs_details_russian_tab">
                <textarea class="form-control" style="height:150px;" name="details_russian" value="" setSplitByLang="1" id="details_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_79XK5q" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Параметры атрибута</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_fLF5lO" class="control-group">
            <label class="control-label" for="l__">
            Использовать как диапазон            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_range').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_range" style="width:80px;" name="is_range">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_fVdBUG" class="control-group">
            <label class="control-label" for="l__">
            Шаблон от            
                    </label>
                <div class="controls"><div id="input-container-a666a500bb5cbbdddf279f156efe2970">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_from_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_from_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a666a500bb5cbbdddf279f156efe2970",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_lpHuaL" class="control-group">
            <label class="control-label" for="l__">
            Шаблон до            
                    </label>
                <div class="controls"><div id="input-container-a337c42b307bf8869f033c7c388701ea">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_to_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_to_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a337c42b307bf8869f033c7c388701ea",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_K6u4Ty" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Значения по умолчанию</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_K09jwI" class="control-group">
            <label class="control-label" for="l__">
            Показывать подсказки в админ панели?            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_autocomplete_in_admin').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_autocomplete_in_admin" style="width:80px;" name="is_autocomplete_in_admin">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_nHbvuC" class="control-group">
            <label class="control-label" for="l__">
            Список значений            
                    </label>
                <div class="controls">	        	
        
                            																		            
    
    <script>
    $(function(){
        $('#select_options_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="select_options_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#select_options_tabs_select_options_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="select_options_tabs_select_options_russian_tab">
                <div class="gui-field-details-container">  <textarea class="form-control" style="height:300px;" name="select_options_russian" value="" details="Каждое новое значение с новой строки" setSplitByLang="1" id="select_options_russian"></textarea>	<div class="gui-details">Каждое новое значение с новой строки</div></div>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="span6"><fieldset id="guifieldset_hVEfBk" class="gui-fieldset" label="Настройки базы данных" style="display:none;">
        <div class="gui-fieldset-content">
        <div id="guirow_uTNLzD" class="control-group">
            <label class="control-label" for="l__">
            Тип колонки            
                    </label>
                <div class="controls">

        
        
<select class="gui-form-select" name="sql_type">
            <option selected="selected" value="VARCHAR" class="">VARCHAR</option>
            <option value="INT" class="">INT</option>
            <option value="TINYINT" class="">TINYINT</option>
            <option value="DOUBLE" class="">DOUBLE</option>
            <option value="TEXT" class="">TEXT</option>
            <option value="LONGTEXT" class="">LONGTEXT</option>
            <option value="ENUM" class="">ENUM</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_N5k7v3" class="control-group">
            <label class="control-label" for="l__">
            Длина            
                    </label>
                <div class="controls"><div id="input-container-a050e075bf253fba1056a9a12580166a">
                        <input class="form-control input-block-level" type="text" name="sql_lenght" value="255">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="sql_lenght"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a050e075bf253fba1056a9a12580166a",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_kXlAMm" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_IUUUzc" class="control-group">
            <label class="control-label" for="l__">
            Активный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_active').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_active" style="width:80px;" name="is_active">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_siCxzl" class="control-group">
            <label class="control-label" for="l__">
            Индексировать            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_index').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_index" style="width:80px;" name="is_index">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_hUIKGw" class="control-group">
            <label class="control-label" for="l__">
            В СЕО шаблонах            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_seo_template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_seo_template" style="width:80px;" name="in_seo_template">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_Duf8fZ" class="control-group">
            <label class="control-label" for="l__">
            Системный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_system').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_system" style="width:80px;" name="is_system">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_1N2fy7" class="control-group">
            <label class="control-label" for="l__">
            Обязательный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_required').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_required" style="width:80px;" name="is_required">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_JK9y3N" class="control-group">
            <label class="control-label" for="l__">
            Уникальный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique" style="width:80px;" name="is_unique">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_VKkwce" class="control-group">
            <label class="control-label" for="l__">
            Уникальный в родителе            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique_parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique_parent" style="width:80px;" name="is_unique_parent">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_XEUMJp" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable" style="width:80px;" name="is_configurable">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_XstluL" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара (материалы)            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable_material').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable_material" style="width:80px;" name="is_configurable_material">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_BgCN0r" class="control-group">
            <label class="control-label" for="l__">
            Содержит несколько значений            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_multiple').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_multiple" style="width:80px;" name="is_multiple">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_3HO13y" class="control-group">
            <label class="control-label" for="l__">
            В списке товаров            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_list').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_list" style="width:80px;" name="is_in_list">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_PmZi41" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров подробно            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card" style="width:80px;" name="is_in_card">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_U4BhJJ" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров кратко             
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card_short').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card_short" style="width:80px;" name="is_in_card_short">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_yNSHaG" class="control-group">
            <label class="control-label" for="l__">
            В фильтре            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter" style="width:80px;" name="is_in_filter">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_PzXwl0" class="control-group">
            <label class="control-label" for="l__">
            В фильтре свернутый            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter_hidden').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter_hidden" style="width:80px;" name="is_in_filter_hidden">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_BbcDRh" class="control-group">
            <label class="control-label" for="l__">
            В поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search" style="width:80px;" name="is_in_search">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_epqoGF" class="control-group">
            <label class="control-label" for="l__">
            В расширенном поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search_advanced').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search_advanced" style="width:80px;" name="is_in_search_advanced">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_HlMmER" class="control-group">
            <label class="control-label" for="l__">
            Мультиязычный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#split_by_lang').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="split_by_lang" style="width:80px;" name="split_by_lang">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div></div>            </div>
            </div>

    </div>
<?php }} ?>