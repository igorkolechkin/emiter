<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 15:10:54
         compiled from "9a52a4ee7ff9723d5d19f66671dbdbf734189e0f" */ ?>
<?php /*%%SmartyHeaderCode:2444818325b23acceca2df1-98806516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9a52a4ee7ff9723d5d19f66671dbdbf734189e0f' => 
    array (
      0 => '9a52a4ee7ff9723d5d19f66671dbdbf734189e0f',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '2444818325b23acceca2df1-98806516',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23accecb6437_81810213',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23accecb6437_81810213')) {function content_5b23accecb6437_81810213($_smarty_tpl) {?><div fnx="true" class="span6"><fieldset id="guifieldset_IhSj10" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_jkfXTQ" class="control-group">
            <label class="control-label" for="l__">
            Родительская категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="parent" name="parent">
            <option selected="selected" value="1" class="level-0">Корень</option>
            <option value="2" class="level-0">Товары без категории(не удалять)</option>
            <option value="1119" class="level-0">Матрасы</option>
            <option value="1120" class="level-0">Детские матрасы</option>
            <option value="1121" class="level-0">Тонкие матрасы</option>
            <option value="1124" class="level-0">Наматрасники</option>
            <option value="1125" class="level-0">Основания</option>
            <option value="1126" class="level-0">Подушки</option>
            <option value="1127" class="level-0">Кровати</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_8fI9bB" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Наматрасники" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_78gpBd" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="namatrasniki" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_b0XY9K" class="control-group">
            <label class="control-label" for="l__">
            Основная категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_main').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_main" style="width:80px;" name="is_main">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_tf9uEs" class="control-group">
            <label class="control-label" for="l__">
            Изображение в меню            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_PmRndv" class="control-group">
            <label class="control-label" for="l__">
            Баннер в меню            
                    </label>
                <div class="controls"><div id="input-container-image_banner">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_banner" name="image_banner" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_banner').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_BedmEw" class="control-group">
            <label class="control-label" for="l__">
            Изображение на главной            
                    </label>
                <div class="controls"><div id="input-container-image_main_page">
        <div class="gui-field-details-container">
        
                                <div class="gui-current-image with-image-info">
                <div class="gui-current-image-container">
                    <img src="http://sonvis.fnx.dp.ua/home/catalog/categories/1124/251453e26a1024798ae60ba2ac1d5d24.png" width="50" alt=""/>
                </div>
                <div class="gui-current-image-controls">
                    <label for="image_main_page_delete">
                        <input type="checkbox"
                               id="image_main_page_delete"
                               name="image_main_page_delete"
                               value="1"/>
                        <span class="lbl"></span>
                        Удалить                    </label>
                </div>
            </div>
                                            <div class="image-size-info">
                    <table>
                                                    <tr>
                                <td>Размер</td>
                                <td>197x197</td>
                            </tr>
                                                                            <tr>
                                <td>Вес</td>
                                <td>87.25 kb</td>
                            </tr>
                                            </table>
                </div>
            
        
                    <input class="gui-form-image" type="file" id="image_main_page" name="image_main_page" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_main_page').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_s4NmeY" class="control-group">
            <label class="control-label" for="l__">
            Изображение в категории            
                    </label>
                <div class="controls"><div id="input-container-image_category">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_category" name="image_category" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_category').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>