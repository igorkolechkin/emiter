<?php /* Smarty version Smarty-3.1.13, created on 2018-06-13 18:25:20
         compiled from "8fa7a9b232e5053eb987a43c04dd0e2d4e684a33" */ ?>
<?php /*%%SmartyHeaderCode:3948570275b213760055012-36999389%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fa7a9b232e5053eb987a43c04dd0e2d4e684a33' => 
    array (
      0 => '8fa7a9b232e5053eb987a43c04dd0e2d4e684a33',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '3948570275b213760055012-36999389',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b21376007d197_16576965',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b21376007d197_16576965')) {function content_5b21376007d197_16576965($_smarty_tpl) {?><div fnx="true" class="span6"><fieldset id="guifieldset_3KpWqE" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки базы данных</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_mUkWWS" class="control-group">
            <label class="control-label" for="l__">
            Тип колонки            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#sql_type').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="sql_type" style="width:100px;" name="sql_type">
            <option selected="selected" value="VARCHAR" class="">VARCHAR</option>
            <option value="INT" class="">INT</option>
            <option value="TINYINT" class="">TINYINT</option>
            <option value="DOUBLE" class="">DOUBLE</option>
            <option value="TEXT" class="">TEXT</option>
            <option value="LONGTEXT" class="">LONGTEXT</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_9p7wML" class="control-group">
            <label class="control-label" for="l__">
            Длина            
                    </label>
                <div class="controls"><div id="input-container-a050e075bf253fba1056a9a12580166a">
                        <input class="form-control input-block-level" type="text" name="sql_lenght" value="255">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="sql_lenght"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a050e075bf253fba1056a9a12580166a",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_0552eN" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_qQhmeA" class="control-group">
            <label class="control-label" for="l__">
            Активный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_active').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_active" style="width:80px;" name="is_active">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_25E9dw" class="control-group">
            <label class="control-label" for="l__">
            Индексировать            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_index').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_index" style="width:80px;" name="is_index">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_fqoFmt" class="control-group">
            <label class="control-label" for="l__">
            Системный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_system').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_system" style="width:80px;" name="is_system">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_li8t3n" class="control-group">
            <label class="control-label" for="l__">
            Обязательный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_required').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_required" style="width:80px;" name="is_required">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_Z6y1aN" class="control-group">
            <label class="control-label" for="l__">
            Уникальный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique" style="width:80px;" name="is_unique">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_SlO8iz" class="control-group">
            <label class="control-label" for="l__">
            Уникальный в родителе            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique_parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique_parent" style="width:80px;" name="is_unique_parent">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_kTEz12" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable" style="width:80px;" name="is_configurable">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_RkfY5S" class="control-group">
            <label class="control-label" for="l__">
            В списке товаров            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_list').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_list" style="width:80px;" name="is_in_list">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_Z0gwTW" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров подробно            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card" style="width:80px;" name="is_in_card">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_lPUSOF" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров кратко             
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card_short').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card_short" style="width:80px;" name="is_in_card_short">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_5g0hnI" class="control-group">
            <label class="control-label" for="l__">
            В фильтре            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter" style="width:80px;" name="is_in_filter">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_Fyh5zJ" class="control-group">
            <label class="control-label" for="l__">
            В фильтре свернутый            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter_hidden').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter_hidden" style="width:80px;" name="is_in_filter_hidden">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_lauUFz" class="control-group">
            <label class="control-label" for="l__">
            В поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search" style="width:80px;" name="is_in_search">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_supNFV" class="control-group">
            <label class="control-label" for="l__">
            В расширенном поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search_advanced').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search_advanced" style="width:80px;" name="is_in_search_advanced">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_D3Ye9M" class="control-group">
            <label class="control-label" for="l__">
            Мультиязычный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#split_by_lang').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="split_by_lang" style="width:80px;" name="split_by_lang">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>