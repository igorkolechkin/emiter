<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "e7863a309cbf42f449c4e2497387049b2ee61395" */ ?>
<?php /*%%SmartyHeaderCode:18833097695b279ebb816944-19508879%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e7863a309cbf42f449c4e2497387049b2ee61395' => 
    array (
      0 => 'e7863a309cbf42f449c4e2497387049b2ee61395',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '18833097695b279ebb816944-19508879',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebb82a212_42499631',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebb82a212_42499631')) {function content_5b279ebb82a212_42499631($_smarty_tpl) {?><fieldset id="guifieldset_2qUTFh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_kURTHV" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_zi9cd1" class="control-group">
            <label class="control-label" for="l__">
            Файл            
                    </label>
                <div class="controls"><div id="input-container-upload_file">
    
                    <input class="gui-form-image" type="file" id="upload_file" name="upload_file">
        
        <div class="gui-details">
                    </div>

        <div class="gui-details">
            <!-- Блок вывода информации о файле и удаление файла -->
                    </div>
        
        <script>
        $(function () {
            $('#upload_file').ace_file_input({
                no_file: 'Файл не выбран ...',
                btn_choose: 'Выберите файл',
                btn_change: 'Изменить файл',
                droppable: true,
                onchange: null,
                thumbnail: 'small' //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_UWje3x" class="control-group">
            <label class="control-label" for="l__">
            Отображать в sitemap.xml            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_sitemap').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_sitemap" style="width:80px;" name="in_sitemap">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_qYCJpe" class="control-group">
            <label class="control-label" for="l__">
            Файл шаблона            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="template" style="width:200px;" name="template">
            <option selected="selected" value="" class="">По умолчанию</option>
            <option value="contacts" class="">Контакты</option>
            <option value="about" class="">О компании</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_k7SlYC" class="control-group">
            <label class="control-label" for="l__">
            Позиция            
                    </label>
                <div class="controls"><div id="input-container-4757fe07fd492a8be0ea6a760d683d6e">
                        <input class="form-control input-block-level" type="text" name="position" value="0">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="position"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-4757fe07fd492a8be0ea6a760d683d6e",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><?php }} ?>