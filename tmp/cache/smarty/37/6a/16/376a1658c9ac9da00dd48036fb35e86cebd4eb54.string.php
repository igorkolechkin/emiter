<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "376a1658c9ac9da00dd48036fb35e86cebd4eb54" */ ?>
<?php /*%%SmartyHeaderCode:2198705425b279ebb9b3802-70010650%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '376a1658c9ac9da00dd48036fb35e86cebd4eb54' => 
    array (
      0 => '376a1658c9ac9da00dd48036fb35e86cebd4eb54',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '2198705425b279ebb9b3802-70010650',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebb9f1f84_17567155',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebb9f1f84_17567155')) {function content_5b279ebb9f1f84_17567155($_smarty_tpl) {?>
        <script>
            function __imgMetaTemplate(containerCount, fields) {
                
                return '<div class="control-group"><div class="control-label">Название изображения</div><div class="controls"><textarea class="form-control gallery-img-meta-2778 " id="gallery-img-meta-2778-meta_title" type="text" data-name="meta_title" name="img_meta_title">'+fields["meta_title"]+'</textarea></div></div><div class="control-group"><div class="control-label">Альтернативный текст</div><div class="controls"><textarea class="form-control gallery-img-meta-2778 " id="gallery-img-meta-2778-meta_alt" type="text" data-name="meta_alt" name="img_meta_alt">'+fields["meta_alt"]+'</textarea></div></div><div class="control-group"><div class="control-label"></div><div class="controls"><button onclick="return __imgMetaSave(' + containerCount + ', $(\'.gallery-img-meta-2778\'))" class="btn btn-success" name="save" value="Сохранить" type="button">Сохранить</button></div></div>';
            }

            function __imgMetaSave(containerCount, elements) {

                $('#f38697edb272f61bda9b5af6d7a44bc4_meta')
                    .html('Данные сохранены. Нажмите на изображение, чтобы заполнить дополнительные данные');

                $("#44d8a613518837ba52f616e4d5d43706").find('li').each(function (i) {
                    var $this = $(this);
                    if (i == containerCount) {
                        $(elements).each(function () {
                            console.log($this.find('.img-meta-' + $(this).attr('data-name')), $(this).val());
                            $this.find('.img-meta-' + $(this).attr('data-name')).val($(this).val());
                        });
                        return false;
                    }
                });

                return false;
            }

            $(function () {
                //templateMY
                $("#44d8a613518837ba52f616e4d5d43706").sortable({
                    placeholder: "placeholder"
                });
                //$("#44d8a613518837ba52f616e4d5d43706").disableSelection();

                $("#44d8a613518837ba52f616e4d5d43706").find('li').each(function (i) {
                    $(this).click(function () {
                        var fields = [];
                        $(this).find('.img-meta').each(function () {
                            fields[$(this).attr('data-name')] = $(this).val();
                        });

                        $('#f38697edb272f61bda9b5af6d7a44bc4_meta').html(__imgMetaTemplate(i, fields));
                        viewEditor();
                        //return false;
                    })
                });

                $("#44d8a613518837ba52f616e4d5d43706").find('li label').click(function (e) {
                    e.stopPropagation();
                });

                $('#gallery_uploader').ace_file_input({
                    no_file: 'Ничего не выбрано ...',
                    btn_choose: 'Выберите',
                    btn_change: 'Изменить',
                    droppable: true,
                    onchange: null,
                    thumbnail: false
                });

                $('#gallery_uploader').change(function () {
                    var $input = $("#gallery_uploader");
                    var $files = $input.prop('files');


                    $($files).each(function (a) {
                        var formData = new FormData;
                        formData.append('gallery', $files[a]);
                        var $photo = formData.get('gallery');
                        if (!$photo.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }

                        createImage($photo);
                        $.ajax({
                            url: '/lib/Creator/Gallery/FileUploader.php',
                            data: formData,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                $("input[value='" + $photo['name'] + "'").val(data['filename']);
                                $.data($photo).addClass('done');
                            }
                        })

                    });
                });

                var dropbox = $('#98070686b92d02fae22469718d3c72ac'),
                    message = $('.message', dropbox),
                    previews = $('#209fc2aaee1be9671b02a2d79fa64bfa');

                dropbox.filedrop({
                    fallback_id: 'upload_button',
                    paramname: 'gallery',
                    maxfiles: 50,
                    maxfilesize: 20,
                    url: '/lib/Creator/Gallery/FileUploader.php',

                    dragEnter: function (e) {
                        dropbox.addClass('enter')
                    },
                    //dragOver : function(e) {
                    //  dropbox.removeClass('enter')
                    // } ,
                    dragLeave: function (e) {
                        dropbox.removeClass('enter')
                    },
                    drop: function (e) {
                        dropbox.removeClass('enter')
                    },

                    uploadFinished: function (i, file, response/*json*/) {
                        $.data(file).addClass('done');
                    },

                    error: function (err, file) {
                        switch (err) {
                            case 'BrowserNotSupported':
                                showMessage('Your browser does not support HTML5 file uploads!');
                                break;
                            case 'TooManyFiles':
                                alert('Too many files! Please select 5 at most! (configurable)');
                                break;
                            case 'FileTooLarge':
                                alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
                                break;
                            default:
                                break;
                        }
                    },

                    beforeEach: function (file) {
                        if (!file.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }
                    },

                    uploadStarted: function (i, file, len) {
                        createImage(file);
                    },

                    progressUpdated: function (i, file, progress) {
                        $.data(file).find('.progress').width(progress);
                    }

                });

                var template = '<div class="preview">' +
                    '<span class="imageHolder">' +
                    '<img />' +
                    '<span class="uploaded"></span>' +
                    '</span>' +
                    '<div class="progressHolder">' +
                    '<div class="progress"></div>' +
                    '</div>' +
                    '</div>';


                function createImage(file) {

                    var preview = $(template),
                        image = $('img', preview),
                        input = $('<input />');

                    input.attr('type', 'hidden')
                        .attr('name', 'gallery[new][]')
                        .attr('value', file.name);

                    var reader = new FileReader();

                    image.width = 100;
                    image.height = 100;

                    reader.onload = function (e) {
                        image.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(file);

                    input.appendTo(previews)
                    preview.appendTo(previews);

                    $.data(file, preview);
                }

                function showMessage(msg) {
                    message.html(msg);
                }

                viewEditor();

                function viewEditor() {
                    /*CKEDITOR.replace('.view-editor', {
                     height : '300px'
                     });*/
                }
            });
        </script>

        <div class="form-element-gallery" id="f38697edb272f61bda9b5af6d7a44bc4"><div class="alert" id="f38697edb272f61bda9b5af6d7a44bc4_alert"><b>*ВНИМАНИЕ:</b> Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_).<br>Не загружайте много изображений за один раз, рекомендуется не более 5 - 10 изображений за раз.</div><br><br><div class="drop-zone" id="98070686b92d02fae22469718d3c72ac"><span class="message">Переместите файлы сюда</span></div><div>
                            <label style="text-align: center" for="gallery_uploader">
                                <b>ИЛИ</b><br>
			                    Выберите изображения на компьютере
                            </label>
							<input type="file" id="gallery_uploader" name="gallery[new]" multiple>
						</div><div class="previews-container" id="209fc2aaee1be9671b02a2d79fa64bfa"></div><br><br><div class="well" id="f38697edb272f61bda9b5af6d7a44bc4_meta">Нажмите на изображение, чтобы заполнить дополнительные данные.</div><ul class="images-container" id="44d8a613518837ba52f616e4d5d43706"></ul><div class="clearfix"></div></div><?php }} ?>