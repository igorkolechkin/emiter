<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 14:33:30
         compiled from "9f83b92e834e56ea2ce9b66e89e299e1fdb5a1b0" */ ?>
<?php /*%%SmartyHeaderCode:17730151305b23a40acf3508-24310279%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f83b92e834e56ea2ce9b66e89e299e1fdb5a1b0' => 
    array (
      0 => '9f83b92e834e56ea2ce9b66e89e299e1fdb5a1b0',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '17730151305b23a40acf3508-24310279',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23a40ad1b1d7_59740185',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23a40ad1b1d7_59740185')) {function content_5b23a40ad1b1d7_59740185($_smarty_tpl) {?><div fnx="true" class="span6"><fieldset id="guifieldset_jsLiV9" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_2Gydh5" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_X7W7UH" class="control-group">
            <label class="control-label" for="l__">
            Единица измерения            
                    </label>
                <div class="controls"><div id="input-container-3e34bdebd9bd5edda27e8728904a2552">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="unit_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="unit_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-ea41b99f7844c44b6740e1fb531a3e9c",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_lNsju3" class="control-group">
            <label class="control-label" for="l__">
            Системное название            
                    </label>
                <div class="controls"><div id="input-container-e266081b6e1f85de55ebe69e568928b9">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="sys_title" value="" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="sys_title"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-e266081b6e1f85de55ebe69e568928b9",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_412qMn" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_KnNrbg" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#details_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="details_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#details_tabs_details_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="details_tabs_details_russian_tab">
                <textarea class="form-control" style="height:150px;" name="details_russian" value="" setSplitByLang="1" id="details_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_KayCUv" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Параметры атрибута</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_dXZjzr" class="control-group">
            <label class="control-label" for="l__">
            Использовать как диапазон            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_range').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_range" style="width:80px;" name="is_range">
            <option value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_ZMfrvc" class="control-group">
            <label class="control-label" for="l__">
            Шаблон от            
                    </label>
                <div class="controls"><div id="input-container-a666a500bb5cbbdddf279f156efe2970">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_from_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_from_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a666a500bb5cbbdddf279f156efe2970",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_kdHKAt" class="control-group">
            <label class="control-label" for="l__">
            Шаблон до            
                    </label>
                <div class="controls"><div id="input-container-a337c42b307bf8869f033c7c388701ea">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_to_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_to_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a337c42b307bf8869f033c7c388701ea",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_XIgycj" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Значения по умолчанию</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_qhAquC" class="control-group">
            <label class="control-label" for="l__">
            Показывать подсказки в админ панели?            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_autocomplete_in_admin').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_autocomplete_in_admin" style="width:80px;" name="is_autocomplete_in_admin">
            <option value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_i2I6eD" class="control-group">
            <label class="control-label" for="l__">
            Список значений            
                    </label>
                <div class="controls">	        	
        
                            																		            
    
    <script>
    $(function(){
        $('#select_options_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="select_options_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#select_options_tabs_select_options_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="select_options_tabs_select_options_russian_tab">
                <div class="gui-field-details-container">  <textarea class="form-control" style="height:300px;" name="select_options_russian" value="" details="Каждое новое значение с новой строки" setSplitByLang="1" id="select_options_russian"></textarea>	<div class="gui-details">Каждое новое значение с новой строки</div></div>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>