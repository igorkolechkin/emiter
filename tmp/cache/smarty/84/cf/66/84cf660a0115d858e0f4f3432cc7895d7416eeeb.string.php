<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 15:11:24
         compiled from "84cf660a0115d858e0f4f3432cc7895d7416eeeb" */ ?>
<?php /*%%SmartyHeaderCode:15993279155b23acec6b8574-25535774%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84cf660a0115d858e0f4f3432cc7895d7416eeeb' => 
    array (
      0 => '84cf660a0115d858e0f4f3432cc7895d7416eeeb',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '15993279155b23acec6b8574-25535774',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23acec6e89b0_74497796',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23acec6e89b0_74497796')) {function content_5b23acec6e89b0_74497796($_smarty_tpl) {?><script>
    $(function(){
        $('#guitabs_xgNwMj').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="guitabs_xgNwMj" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#guitabs_xgNwMj_general"><span class="dotted">Общие</span></a></li>
                            <li class=""><a data-toggle="tab" href="#guitabs_xgNwMj_seo"><span class="dotted">SEO</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="guitabs_xgNwMj_general">
                <div fnx="true" class="span6"><fieldset id="guifieldset_ekWNK6" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_HMjxS5" class="control-group">
            <label class="control-label" for="l__">
            Родительская категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="parent" name="parent">
            <option selected="selected" value="1" class="level-0">Корень</option>
            <option value="2" class="level-0">Товары без категории(не удалять)</option>
            <option value="1119" class="level-0">Матрасы</option>
            <option value="1120" class="level-0">Детские матрасы</option>
            <option value="1121" class="level-0">Тонкие матрасы</option>
            <option value="1124" class="level-0">Наматрасники</option>
            <option value="1125" class="level-0">Основания</option>
            <option value="1126" class="level-0">Подушки</option>
            <option value="1127" class="level-0">Кровати</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_QWwo3y" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Подушки" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_GQCT64" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="podushki" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_dV5ds5" class="control-group">
            <label class="control-label" for="l__">
            Основная категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_main').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_main" style="width:80px;" name="is_main">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_SqB7rT" class="control-group">
            <label class="control-label" for="l__">
            Изображение в меню            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_hTFjx3" class="control-group">
            <label class="control-label" for="l__">
            Баннер в меню            
                    </label>
                <div class="controls"><div id="input-container-image_banner">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_banner" name="image_banner" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_banner').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_BvjOIr" class="control-group">
            <label class="control-label" for="l__">
            Изображение на главной            
                    </label>
                <div class="controls"><div id="input-container-image_main_page">
        <div class="gui-field-details-container">
        
                                <div class="gui-current-image with-image-info">
                <div class="gui-current-image-container">
                    <img src="http://sonvis.fnx.dp.ua/home/catalog/categories/1126/673ea6ac1ec3c78cc30c5edd2e44beba.png" width="50" alt=""/>
                </div>
                <div class="gui-current-image-controls">
                    <label for="image_main_page_delete">
                        <input type="checkbox"
                               id="image_main_page_delete"
                               name="image_main_page_delete"
                               value="1"/>
                        <span class="lbl"></span>
                        Удалить                    </label>
                </div>
            </div>
                                            <div class="image-size-info">
                    <table>
                                                    <tr>
                                <td>Размер</td>
                                <td>640x447</td>
                            </tr>
                                                                            <tr>
                                <td>Вес</td>
                                <td>298.32 kb</td>
                            </tr>
                                            </table>
                </div>
            
        
                    <input class="gui-form-image" type="file" id="image_main_page" name="image_main_page" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_main_page').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_w531r0" class="control-group">
            <label class="control-label" for="l__">
            Изображение в категории            
                    </label>
                <div class="controls"><div id="input-container-image_category">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_category" name="image_category" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_category').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="span6"><fieldset id="guifieldset_W73ln7" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_RA21mS" class="control-group">
            <label class="control-label" for="l__">
            Атрибуты            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#attributeset_id').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="attributeset_id" name="attributeset_id">
            <option value="42" class="">Наматрасник</option>
            <option value="43" class="">Основания</option>
            <option selected="selected" value="1" class="">По умолчанию</option>
            <option value="44" class="">Подушки</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_KYfEmj" class="control-group">
            <label class="control-label" for="l__">
            Опубликовано            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_public').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_public" style="width:80px;" name="is_public">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_UiclAk" class="control-group">
            <label class="control-label" for="l__">
            В главном меню            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_menu').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_menu" style="width:80px;" name="in_menu">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_hxFpZ9" class="control-group">
            <label class="control-label" for="l__">
            На главной странице            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#on_main').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="on_main" style="width:80px;" name="on_main">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_ET7MaU" class="control-group">
            <label class="control-label" for="l__">
            Показывать фильтр            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#show_filter').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="show_filter" style="width:80px;" name="show_filter">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_F6oiqu" class="control-group">
            <label class="control-label" for="l__">
            Топ товары            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_top').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_top" style="width:80px;" name="is_top">
            <option value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_pv6MUs" class="control-group">
            <label class="control-label" for="l__">
            Отображать в sitemap.xml            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_sitemap').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_sitemap" style="width:80px;" name="in_sitemap">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_FeVMaL" class="control-group">
            <label class="control-label" for="l__">
            Баннер для категории            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#banner_id').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="banner_id" name="banner_id">
            <option selected="selected" value="" class="">Не выбран</option>
            <option value="2" class="">Баннер на главной</option>
            <option value="5" class="">Тонкие матрасы-топперы</option>
            <option value="3" class="">Баннер в категории Одеяла</option>
            <option value="4" class="">Идеальные матрасы</option>
            <option value="6" class="">Бесплатная адресная доставка по Украине</option>
            <option value="7" class="">Бесплатная адресная доставка по Украине</option>
            <option value="8" class="">Скидки до 40</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="row-fluid"><div fnx="true" class="span12"><fieldset id="guifieldset_MewtyJ" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Описание</legend>
        <div class="gui-fieldset-content">
                                                    <script>
        /*
        tinymce.init({
            selector: "#content_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'content_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#content_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="content_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#content_tabs_content_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="content_tabs_content_russian_tab">
                <textarea class="gui-form-wysiwyg" id="content_russian" name="content_russian" setSplitByLang="1"></textarea>            </div>
            </div>

    </div>

    </div>
</fieldset></div></div>            </div>
                    <div class="tab-pane " id="guitabs_xgNwMj_seo">
                <fieldset id="guifieldset_GWumfy" class="gui-fieldset">
            <legend class="gui-fieldset-legend">SEO</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_kwHpUs" class="control-group">
            <label class="control-label" for="l__">
            Заголовок title            
                    </label>
                <div class="controls"><div id="input-container-c49e760799098baf522d1c2283ad7087">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_title_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-33bf98c2753074fc6d813d862c7dd8d0",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_oki8qf" class="control-group">
            <label class="control-label" for="l__">
            Заголовок H1            
                    </label>
                <div class="controls"><div id="input-container-cff7c63951e29471bdd201e681557f81">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_h1_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_h1_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-af2bcecdbc30344699fc1a1e37542a3b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_fXm1of" class="control-group">
            <label class="control-label" for="l__">
            Ключевый слова            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_keywords_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_keywords_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_keywords_tabs_seo_keywords_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_keywords_tabs_seo_keywords_russian_tab">
                <textarea class="form-control" name="seo_keywords_russian" style="height:150px;" setSplitByLang="1" id="seo_keywords_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_aOtbn0" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_description_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_description_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_description_tabs_seo_description_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_description_tabs_seo_description_russian_tab">
                <textarea class="form-control" name="seo_description_russian" style="height:150px;" setSplitByLang="1" id="seo_description_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><fieldset id="guifieldset_BJO9Xl" class="gui-fieldset">
            <legend class="gui-fieldset-legend">SEO текст</legend>
        <div class="gui-fieldset-content">
                                                    <script>
        /*
        tinymce.init({
            selector: "#seo_text_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'seo_text_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#seo_text_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_text_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_text_tabs_seo_text_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_text_tabs_seo_text_russian_tab">
                <textarea class="gui-form-wysiwyg" id="seo_text_russian" name="seo_text_russian" setSplitByLang="1"></textarea>            </div>
            </div>

    </div>

    </div>
</fieldset>    </div>
</fieldset>            </div>
            </div>

    </div>
<?php }} ?>