<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "48d664142ccfd5dbd4ad28e4e91e87b99dacfb9e" */ ?>
<?php /*%%SmartyHeaderCode:14378644195b279ebb832c04-65086334%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '48d664142ccfd5dbd4ad28e4e91e87b99dacfb9e' => 
    array (
      0 => '48d664142ccfd5dbd4ad28e4e91e87b99dacfb9e',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '14378644195b279ebb832c04-65086334',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebb865ce7_63566645',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebb865ce7_63566645')) {function content_5b279ebb865ce7_63566645($_smarty_tpl) {?><div fnx="true" class="span5"><fieldset id="guifieldset_OHw9Vh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_1bDsu9" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Главная" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_4rJHWx" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="default" translit_source="title_russian" translit="true" details="Только буквы латинского алфавита">
                            <span id="checker-result-url_key"></span>
                        <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                                    var liStatus = false;
            if ($input.val() == '') {
                liStatus = true;
            }
            $('[name="title_russian"]').liTranslit({
                elAlias: $('[name="url_key"]'),
                status: liStatus,
                eventType: 'keyup',
                                translated: function (el, text, eventType) {
                    if (eventType != 'no event' && this.status) {
                        uniqueChecker(text);
                    }
                }
                            });
                                    $input.liTranslit();
            
                        
                        $input.on('keyup', function () {
                var _this = this;
                setTimeout(function () {
                    uniqueChecker($(_this).val());
                }, 200)
            });

            function uniqueChecker($check_value) {
                $('#checker-result-url_key').text('');
                if ($check_value.length > 2) {
                    $.ajax({
                        url: 'http://sonvis.fnx.dp.ua/acp/core/common/uniquechecker',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            value: $check_value,
                            field: 'url_key',
                            table: 'structure',
                            current: '3',
                        },
                        success: function (result) {
                            $('#checker-result-url_key')
                                .attr('class', result.type)
                                .text(result.message);
                        }
                    })
                }
            }
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_z56pjA" class="control-group">
            <label class="control-label" for="l__">
            Опубликовано            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_public').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_public" style="width:80px;" name="is_public">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_2qUTFh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_kURTHV" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_zi9cd1" class="control-group">
            <label class="control-label" for="l__">
            Файл            
                    </label>
                <div class="controls"><div id="input-container-upload_file">
    
                    <input class="gui-form-image" type="file" id="upload_file" name="upload_file">
        
        <div class="gui-details">
                    </div>

        <div class="gui-details">
            <!-- Блок вывода информации о файле и удаление файла -->
                    </div>
        
        <script>
        $(function () {
            $('#upload_file').ace_file_input({
                no_file: 'Файл не выбран ...',
                btn_choose: 'Выберите файл',
                btn_change: 'Изменить файл',
                droppable: true,
                onchange: null,
                thumbnail: 'small' //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_UWje3x" class="control-group">
            <label class="control-label" for="l__">
            Отображать в sitemap.xml            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_sitemap').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_sitemap" style="width:80px;" name="in_sitemap">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_qYCJpe" class="control-group">
            <label class="control-label" for="l__">
            Файл шаблона            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="template" style="width:200px;" name="template">
            <option selected="selected" value="" class="">По умолчанию</option>
            <option value="contacts" class="">Контакты</option>
            <option value="about" class="">О компании</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_k7SlYC" class="control-group">
            <label class="control-label" for="l__">
            Позиция            
                    </label>
                <div class="controls"><div id="input-container-4757fe07fd492a8be0ea6a760d683d6e">
                        <input class="form-control input-block-level" type="text" name="position" value="0">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="position"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-4757fe07fd492a8be0ea6a760d683d6e",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>