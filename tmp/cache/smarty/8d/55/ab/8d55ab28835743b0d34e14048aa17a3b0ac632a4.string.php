<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "8d55ab28835743b0d34e14048aa17a3b0ac632a4" */ ?>
<?php /*%%SmartyHeaderCode:6273162425b279ebba9ec00-11779145%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d55ab28835743b0d34e14048aa17a3b0ac632a4' => 
    array (
      0 => '8d55ab28835743b0d34e14048aa17a3b0ac632a4',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '6273162425b279ebba9ec00-11779145',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebbb3e619_61465166',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebbb3e619_61465166')) {function content_5b279ebbb3e619_61465166($_smarty_tpl) {?><script>
    $(function(){
        $('#guitabs_DGkl4p').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="guitabs_DGkl4p" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#guitabs_DGkl4p_general"><span class="dotted">Общие</span></a></li>
                            <li class=""><a data-toggle="tab" href="#guitabs_DGkl4p_gallery"><span class="dotted">Галерея</span></a></li>
                            <li class=""><a data-toggle="tab" href="#guitabs_DGkl4p_content"><span class="dotted">Содержимое</span></a></li>
                            <li class=""><a data-toggle="tab" href="#guitabs_DGkl4p_additional_content"><span class="dotted">Дополнительное содержимое</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="guitabs_DGkl4p_general">
                <div fnx="true" class="row-fluid"><div fnx="true" class="span5"><fieldset id="guifieldset_OHw9Vh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_1bDsu9" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Главная" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_4rJHWx" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="default" translit_source="title_russian" translit="true" details="Только буквы латинского алфавита">
                            <span id="checker-result-url_key"></span>
                        <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                                    var liStatus = false;
            if ($input.val() == '') {
                liStatus = true;
            }
            $('[name="title_russian"]').liTranslit({
                elAlias: $('[name="url_key"]'),
                status: liStatus,
                eventType: 'keyup',
                                translated: function (el, text, eventType) {
                    if (eventType != 'no event' && this.status) {
                        uniqueChecker(text);
                    }
                }
                            });
                                    $input.liTranslit();
            
                        
                        $input.on('keyup', function () {
                var _this = this;
                setTimeout(function () {
                    uniqueChecker($(_this).val());
                }, 200)
            });

            function uniqueChecker($check_value) {
                $('#checker-result-url_key').text('');
                if ($check_value.length > 2) {
                    $.ajax({
                        url: 'http://sonvis.fnx.dp.ua/acp/core/common/uniquechecker',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            value: $check_value,
                            field: 'url_key',
                            table: 'structure',
                            current: '3',
                        },
                        success: function (result) {
                            $('#checker-result-url_key')
                                .attr('class', result.type)
                                .text(result.message);
                        }
                    })
                }
            }
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_z56pjA" class="control-group">
            <label class="control-label" for="l__">
            Опубликовано            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_public').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_public" style="width:80px;" name="is_public">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_2qUTFh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_kURTHV" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_zi9cd1" class="control-group">
            <label class="control-label" for="l__">
            Файл            
                    </label>
                <div class="controls"><div id="input-container-upload_file">
    
                    <input class="gui-form-image" type="file" id="upload_file" name="upload_file">
        
        <div class="gui-details">
                    </div>

        <div class="gui-details">
            <!-- Блок вывода информации о файле и удаление файла -->
                    </div>
        
        <script>
        $(function () {
            $('#upload_file').ace_file_input({
                no_file: 'Файл не выбран ...',
                btn_choose: 'Выберите файл',
                btn_change: 'Изменить файл',
                droppable: true,
                onchange: null,
                thumbnail: 'small' //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_UWje3x" class="control-group">
            <label class="control-label" for="l__">
            Отображать в sitemap.xml            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_sitemap').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_sitemap" style="width:80px;" name="in_sitemap">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_qYCJpe" class="control-group">
            <label class="control-label" for="l__">
            Файл шаблона            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="template" style="width:200px;" name="template">
            <option selected="selected" value="" class="">По умолчанию</option>
            <option value="contacts" class="">Контакты</option>
            <option value="about" class="">О компании</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_k7SlYC" class="control-group">
            <label class="control-label" for="l__">
            Позиция            
                    </label>
                <div class="controls"><div id="input-container-4757fe07fd492a8be0ea6a760d683d6e">
                        <input class="form-control input-block-level" type="text" name="position" value="0">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="position"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-4757fe07fd492a8be0ea6a760d683d6e",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="span1">
                                            </div><div fnx="true" class="span6"><fieldset id="guifieldset_ScSyuI" class="gui-fieldset">
            <legend class="gui-fieldset-legend">SEO</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_Qi6sqA" class="control-group">
            <label class="control-label" for="l__">
            Заголовок title            
                    </label>
                <div class="controls"><div id="input-container-c49e760799098baf522d1c2283ad7087">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_title_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-33bf98c2753074fc6d813d862c7dd8d0",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_yNu4GF" class="control-group">
            <label class="control-label" for="l__">
            Заголовок H1            
                    </label>
                <div class="controls"><div id="input-container-cff7c63951e29471bdd201e681557f81">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_h1_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_h1_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-af2bcecdbc30344699fc1a1e37542a3b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_YZ5vNF" class="control-group">
            <label class="control-label" for="l__">
            Ключевый слова            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_keywords_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_keywords_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_keywords_tabs_seo_keywords_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_keywords_tabs_seo_keywords_russian_tab">
                <textarea class="form-control" name="seo_keywords_russian" style="height:150px;" setSplitByLang="1" id="seo_keywords_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_wabTYA" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_description_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_description_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_description_tabs_seo_description_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_description_tabs_seo_description_russian_tab">
                <textarea class="form-control" name="seo_description_russian" style="height:150px;" setSplitByLang="1" id="seo_description_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_25o633" class="control-group">
            <label class="control-label" for="l__">
            СЕО текст            
                    </label>
                <div class="controls">                                            <script>
        /*
        tinymce.init({
            selector: "#seo_text_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'seo_text_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#seo_text_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_text_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_text_tabs_seo_text_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_text_tabs_seo_text_russian_tab">
                <textarea class="gui-form-wysiwyg" id="seo_text_russian" name="seo_text_russian" style="height:400px;" setSplitByLang="1"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
</textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div></div>            </div>
                    <div class="tab-pane " id="guitabs_DGkl4p_gallery">
                <fieldset id="guifieldset_s7xa4I" class="gui-fieldset">
        <div class="gui-fieldset-content">
                <script>
            function __imgMetaTemplate(containerCount, fields) {
                
                return '<div class="control-group"><div class="control-label">Название изображения</div><div class="controls"><textarea class="form-control gallery-img-meta-2778 " id="gallery-img-meta-2778-meta_title" type="text" data-name="meta_title" name="img_meta_title">'+fields["meta_title"]+'</textarea></div></div><div class="control-group"><div class="control-label">Альтернативный текст</div><div class="controls"><textarea class="form-control gallery-img-meta-2778 " id="gallery-img-meta-2778-meta_alt" type="text" data-name="meta_alt" name="img_meta_alt">'+fields["meta_alt"]+'</textarea></div></div><div class="control-group"><div class="control-label"></div><div class="controls"><button onclick="return __imgMetaSave(' + containerCount + ', $(\'.gallery-img-meta-2778\'))" class="btn btn-success" name="save" value="Сохранить" type="button">Сохранить</button></div></div>';
            }

            function __imgMetaSave(containerCount, elements) {

                $('#f38697edb272f61bda9b5af6d7a44bc4_meta')
                    .html('Данные сохранены. Нажмите на изображение, чтобы заполнить дополнительные данные');

                $("#44d8a613518837ba52f616e4d5d43706").find('li').each(function (i) {
                    var $this = $(this);
                    if (i == containerCount) {
                        $(elements).each(function () {
                            console.log($this.find('.img-meta-' + $(this).attr('data-name')), $(this).val());
                            $this.find('.img-meta-' + $(this).attr('data-name')).val($(this).val());
                        });
                        return false;
                    }
                });

                return false;
            }

            $(function () {
                //templateMY
                $("#44d8a613518837ba52f616e4d5d43706").sortable({
                    placeholder: "placeholder"
                });
                //$("#44d8a613518837ba52f616e4d5d43706").disableSelection();

                $("#44d8a613518837ba52f616e4d5d43706").find('li').each(function (i) {
                    $(this).click(function () {
                        var fields = [];
                        $(this).find('.img-meta').each(function () {
                            fields[$(this).attr('data-name')] = $(this).val();
                        });

                        $('#f38697edb272f61bda9b5af6d7a44bc4_meta').html(__imgMetaTemplate(i, fields));
                        viewEditor();
                        //return false;
                    })
                });

                $("#44d8a613518837ba52f616e4d5d43706").find('li label').click(function (e) {
                    e.stopPropagation();
                });

                $('#gallery_uploader').ace_file_input({
                    no_file: 'Ничего не выбрано ...',
                    btn_choose: 'Выберите',
                    btn_change: 'Изменить',
                    droppable: true,
                    onchange: null,
                    thumbnail: false
                });

                $('#gallery_uploader').change(function () {
                    var $input = $("#gallery_uploader");
                    var $files = $input.prop('files');


                    $($files).each(function (a) {
                        var formData = new FormData;
                        formData.append('gallery', $files[a]);
                        var $photo = formData.get('gallery');
                        if (!$photo.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }

                        createImage($photo);
                        $.ajax({
                            url: '/lib/Creator/Gallery/FileUploader.php',
                            data: formData,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                $("input[value='" + $photo['name'] + "'").val(data['filename']);
                                $.data($photo).addClass('done');
                            }
                        })

                    });
                });

                var dropbox = $('#98070686b92d02fae22469718d3c72ac'),
                    message = $('.message', dropbox),
                    previews = $('#209fc2aaee1be9671b02a2d79fa64bfa');

                dropbox.filedrop({
                    fallback_id: 'upload_button',
                    paramname: 'gallery',
                    maxfiles: 50,
                    maxfilesize: 20,
                    url: '/lib/Creator/Gallery/FileUploader.php',

                    dragEnter: function (e) {
                        dropbox.addClass('enter')
                    },
                    //dragOver : function(e) {
                    //  dropbox.removeClass('enter')
                    // } ,
                    dragLeave: function (e) {
                        dropbox.removeClass('enter')
                    },
                    drop: function (e) {
                        dropbox.removeClass('enter')
                    },

                    uploadFinished: function (i, file, response/*json*/) {
                        $.data(file).addClass('done');
                    },

                    error: function (err, file) {
                        switch (err) {
                            case 'BrowserNotSupported':
                                showMessage('Your browser does not support HTML5 file uploads!');
                                break;
                            case 'TooManyFiles':
                                alert('Too many files! Please select 5 at most! (configurable)');
                                break;
                            case 'FileTooLarge':
                                alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
                                break;
                            default:
                                break;
                        }
                    },

                    beforeEach: function (file) {
                        if (!file.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }
                    },

                    uploadStarted: function (i, file, len) {
                        createImage(file);
                    },

                    progressUpdated: function (i, file, progress) {
                        $.data(file).find('.progress').width(progress);
                    }

                });

                var template = '<div class="preview">' +
                    '<span class="imageHolder">' +
                    '<img />' +
                    '<span class="uploaded"></span>' +
                    '</span>' +
                    '<div class="progressHolder">' +
                    '<div class="progress"></div>' +
                    '</div>' +
                    '</div>';


                function createImage(file) {

                    var preview = $(template),
                        image = $('img', preview),
                        input = $('<input />');

                    input.attr('type', 'hidden')
                        .attr('name', 'gallery[new][]')
                        .attr('value', file.name);

                    var reader = new FileReader();

                    image.width = 100;
                    image.height = 100;

                    reader.onload = function (e) {
                        image.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(file);

                    input.appendTo(previews)
                    preview.appendTo(previews);

                    $.data(file, preview);
                }

                function showMessage(msg) {
                    message.html(msg);
                }

                viewEditor();

                function viewEditor() {
                    /*CKEDITOR.replace('.view-editor', {
                     height : '300px'
                     });*/
                }
            });
        </script>

        <div class="form-element-gallery" id="f38697edb272f61bda9b5af6d7a44bc4"><div class="alert" id="f38697edb272f61bda9b5af6d7a44bc4_alert"><b>*ВНИМАНИЕ:</b> Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_).<br>Не загружайте много изображений за один раз, рекомендуется не более 5 - 10 изображений за раз.</div><br><br><div class="drop-zone" id="98070686b92d02fae22469718d3c72ac"><span class="message">Переместите файлы сюда</span></div><div>
                            <label style="text-align: center" for="gallery_uploader">
                                <b>ИЛИ</b><br>
			                    Выберите изображения на компьютере
                            </label>
							<input type="file" id="gallery_uploader" name="gallery[new]" multiple>
						</div><div class="previews-container" id="209fc2aaee1be9671b02a2d79fa64bfa"></div><br><br><div class="well" id="f38697edb272f61bda9b5af6d7a44bc4_meta">Нажмите на изображение, чтобы заполнить дополнительные данные.</div><ul class="images-container" id="44d8a613518837ba52f616e4d5d43706"></ul><div class="clearfix"></div></div>    </div>
</fieldset>            </div>
                    <div class="tab-pane " id="guitabs_DGkl4p_content">
                <fieldset id="guifieldset_pzY5nH" class="gui-fieldset">
        <div class="gui-fieldset-content">
                                                    <script>
        /*
        tinymce.init({
            selector: "#content_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'content_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#content_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="content_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#content_tabs_content_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="content_tabs_content_russian_tab">
                <textarea class="gui-form-wysiwyg" id="content_russian" name="content_russian" style="height:400px;" setSplitByLang="1"></textarea>            </div>
            </div>

    </div>

    </div>
</fieldset>            </div>
                    <div class="tab-pane " id="guitabs_DGkl4p_additional_content">
                <fieldset id="guifieldset_2Qvvtr" class="gui-fieldset">
        <div class="gui-fieldset-content">
                                                    <script>
        /*
        tinymce.init({
            selector: "#additional_text_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'additional_text_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#additional_text_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="additional_text_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#additional_text_tabs_additional_text_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="additional_text_tabs_additional_text_russian_tab">
                <textarea class="gui-form-wysiwyg" id="additional_text_russian" name="additional_text_russian" style="height:400px;" setSplitByLang="1"></textarea>            </div>
            </div>

    </div>

    </div>
</fieldset>            </div>
            </div>

    </div>
<?php }} ?>