<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "1a19932c10835d116fdac3b9f877972eaf09947c" */ ?>
<?php /*%%SmartyHeaderCode:2486304255b279ebb953f72-03180341%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a19932c10835d116fdac3b9f877972eaf09947c' => 
    array (
      0 => '1a19932c10835d116fdac3b9f877972eaf09947c',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '2486304255b279ebb953f72-03180341',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebb9a16f8_49368597',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebb9a16f8_49368597')) {function content_5b279ebb9a16f8_49368597($_smarty_tpl) {?><div fnx="true" class="row-fluid"><div fnx="true" class="span5"><fieldset id="guifieldset_OHw9Vh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_1bDsu9" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Главная" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_4rJHWx" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="default" translit_source="title_russian" translit="true" details="Только буквы латинского алфавита">
                            <span id="checker-result-url_key"></span>
                        <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                                    var liStatus = false;
            if ($input.val() == '') {
                liStatus = true;
            }
            $('[name="title_russian"]').liTranslit({
                elAlias: $('[name="url_key"]'),
                status: liStatus,
                eventType: 'keyup',
                                translated: function (el, text, eventType) {
                    if (eventType != 'no event' && this.status) {
                        uniqueChecker(text);
                    }
                }
                            });
                                    $input.liTranslit();
            
                        
                        $input.on('keyup', function () {
                var _this = this;
                setTimeout(function () {
                    uniqueChecker($(_this).val());
                }, 200)
            });

            function uniqueChecker($check_value) {
                $('#checker-result-url_key').text('');
                if ($check_value.length > 2) {
                    $.ajax({
                        url: 'http://sonvis.fnx.dp.ua/acp/core/common/uniquechecker',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            value: $check_value,
                            field: 'url_key',
                            table: 'structure',
                            current: '3',
                        },
                        success: function (result) {
                            $('#checker-result-url_key')
                                .attr('class', result.type)
                                .text(result.message);
                        }
                    })
                }
            }
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_z56pjA" class="control-group">
            <label class="control-label" for="l__">
            Опубликовано            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_public').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_public" style="width:80px;" name="is_public">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_2qUTFh" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_kURTHV" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_zi9cd1" class="control-group">
            <label class="control-label" for="l__">
            Файл            
                    </label>
                <div class="controls"><div id="input-container-upload_file">
    
                    <input class="gui-form-image" type="file" id="upload_file" name="upload_file">
        
        <div class="gui-details">
                    </div>

        <div class="gui-details">
            <!-- Блок вывода информации о файле и удаление файла -->
                    </div>
        
        <script>
        $(function () {
            $('#upload_file').ace_file_input({
                no_file: 'Файл не выбран ...',
                btn_choose: 'Выберите файл',
                btn_change: 'Изменить файл',
                droppable: true,
                onchange: null,
                thumbnail: 'small' //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_UWje3x" class="control-group">
            <label class="control-label" for="l__">
            Отображать в sitemap.xml            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#in_sitemap').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="in_sitemap" style="width:80px;" name="in_sitemap">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_qYCJpe" class="control-group">
            <label class="control-label" for="l__">
            Файл шаблона            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#template').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="template" style="width:200px;" name="template">
            <option selected="selected" value="" class="">По умолчанию</option>
            <option value="contacts" class="">Контакты</option>
            <option value="about" class="">О компании</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_k7SlYC" class="control-group">
            <label class="control-label" for="l__">
            Позиция            
                    </label>
                <div class="controls"><div id="input-container-4757fe07fd492a8be0ea6a760d683d6e">
                        <input class="form-control input-block-level" type="text" name="position" value="0">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="position"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-4757fe07fd492a8be0ea6a760d683d6e",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="span1">
                                            </div><div fnx="true" class="span6"><fieldset id="guifieldset_ScSyuI" class="gui-fieldset">
            <legend class="gui-fieldset-legend">SEO</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_Qi6sqA" class="control-group">
            <label class="control-label" for="l__">
            Заголовок title            
                    </label>
                <div class="controls"><div id="input-container-c49e760799098baf522d1c2283ad7087">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_title_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-33bf98c2753074fc6d813d862c7dd8d0",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_yNu4GF" class="control-group">
            <label class="control-label" for="l__">
            Заголовок H1            
                    </label>
                <div class="controls"><div id="input-container-cff7c63951e29471bdd201e681557f81">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_h1_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_h1_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-af2bcecdbc30344699fc1a1e37542a3b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_YZ5vNF" class="control-group">
            <label class="control-label" for="l__">
            Ключевый слова            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_keywords_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_keywords_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_keywords_tabs_seo_keywords_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_keywords_tabs_seo_keywords_russian_tab">
                <textarea class="form-control" name="seo_keywords_russian" style="height:150px;" setSplitByLang="1" id="seo_keywords_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_wabTYA" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_description_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_description_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_description_tabs_seo_description_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_description_tabs_seo_description_russian_tab">
                <textarea class="form-control" name="seo_description_russian" style="height:150px;" setSplitByLang="1" id="seo_description_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_25o633" class="control-group">
            <label class="control-label" for="l__">
            СЕО текст            
                    </label>
                <div class="controls">                                            <script>
        /*
        tinymce.init({
            selector: "#seo_text_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'seo_text_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#seo_text_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_text_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_text_tabs_seo_text_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_text_tabs_seo_text_russian_tab">
                <textarea class="gui-form-wysiwyg" id="seo_text_russian" name="seo_text_russian" style="height:400px;" setSplitByLang="1"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
</textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div></div><?php }} ?>