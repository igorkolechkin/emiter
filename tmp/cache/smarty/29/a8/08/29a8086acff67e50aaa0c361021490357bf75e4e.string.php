<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 16:14:39
         compiled from "29a8086acff67e50aaa0c361021490357bf75e4e" */ ?>
<?php /*%%SmartyHeaderCode:6252728915b27b03f458247-08600926%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29a8086acff67e50aaa0c361021490357bf75e4e' => 
    array (
      0 => '29a8086acff67e50aaa0c361021490357bf75e4e',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '6252728915b27b03f458247-08600926',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b27b03f47f152_68826104',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b27b03f47f152_68826104')) {function content_5b27b03f47f152_68826104($_smarty_tpl) {?><fieldset id="guifieldset_IYkUUp" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_uEBgFw" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-b068931cc450442b63f5b3d276ea4297">
                        <input class="form-control input-block-level" type="text" name="name" value="УТП на главной">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="name"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-b068931cc450442b63f5b3d276ea4297",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_VWGi9t" class="control-group">
            <label class="control-label" for="l__">
            Системное название            
                    </label>
                <div class="controls"><div id="input-container-c616c43f754c6cffcedd86717c329d2c">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="system_name" value="utp.on.main" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="system_name"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-c616c43f754c6cffcedd86717c329d2c",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_zzGSIr" class="control-group">
            <label class="control-label" for="l__">
            Заголовок            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        <input class="form-control input-block-level" type="text" name="title" value="Неоспоримые преимущества">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="title"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-d5d3db1765287eef77d7927cc956f50a",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_KWL8kV" class="control-group">
            <label class="control-label" for="l__">
            Дополнительный текст            
                    </label>
                <div class="controls">                                            <script>
        /*
        tinymce.init({
            selector: "#additional_content_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'additional_content_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#additional_content_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="additional_content_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#additional_content_tabs_additional_content_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="additional_content_tabs_additional_content_russian_tab">
                <textarea class="gui-form-wysiwyg" id="additional_content_russian" name="additional_content_russian" style="height:400px;" setSplitByLang="1"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_Y6xd8M" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_DxO7h2" class="control-group">
            <label class="control-label" for="l__">
            Ссылка на страницу            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
                        <input class="form-control input-block-level" type="text" name="url_key" value="">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_PSczSa" class="control-group">
            <label class="control-label" for="l__">
            Опубликовано            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_public').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_public" style="width:120px;" name="is_public">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><?php }} ?>