<?php /* Smarty version Smarty-3.1.13, created on 2018-06-13 18:25:54
         compiled from "d0c9598f6e7e8304a3d3081b470870c50aad01c0" */ ?>
<?php /*%%SmartyHeaderCode:4282112555b2137822baae9-69014306%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd0c9598f6e7e8304a3d3081b470870c50aad01c0' => 
    array (
      0 => 'd0c9598f6e7e8304a3d3081b470870c50aad01c0',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '4282112555b2137822baae9-69014306',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b21378230fb78_22142133',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b21378230fb78_22142133')) {function content_5b21378230fb78_22142133($_smarty_tpl) {?><div fnx="true" class="row-fluid"><div fnx="true" class="span6"><fieldset id="guifieldset_WJIik0" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_TAvRkC" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Тип матраса" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_2CluOR" class="control-group">
            <label class="control-label" for="l__">
            Единица измерения            
                    </label>
                <div class="controls"><div id="input-container-3e34bdebd9bd5edda27e8728904a2552">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="unit_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="unit_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-ea41b99f7844c44b6740e1fb531a3e9c",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_btbGTb" class="control-group">
            <label class="control-label" for="l__">
            Системное название            
                    </label>
                <div class="controls"><div id="input-container-e266081b6e1f85de55ebe69e568928b9">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="sys_title" value="type_of_mattress" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="sys_title"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-e266081b6e1f85de55ebe69e568928b9",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_UgQzNB" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
                                <div class="gui-current-image with-image-info">
                <div class="gui-current-image-container">
                    <img src="http://sonvis.fnx.dp.ua/home/catalog_attributes/page452/type_of_mattress.png" width="50" alt=""/>
                </div>
                <div class="gui-current-image-controls">
                    <label for="image_delete">
                        <input type="checkbox"
                               id="image_delete"
                               name="image_delete"
                               value="1"/>
                        <span class="lbl"></span>
                        Удалить                    </label>
                </div>
            </div>
                                            <div class="image-size-info">
                    <table>
                                                    <tr>
                                <td>Размер</td>
                                <td>24x24</td>
                            </tr>
                                                                            <tr>
                                <td>Вес</td>
                                <td>1.24 kb</td>
                            </tr>
                                            </table>
                </div>
            
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_QeuBh4" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#details_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="details_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#details_tabs_details_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="details_tabs_details_russian_tab">
                <textarea class="form-control" style="height:150px;" name="details_russian" value="" setSplitByLang="1" id="details_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_vOXSdM" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Параметры атрибута</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_CpWvlq" class="control-group">
            <label class="control-label" for="l__">
            Использовать как диапазон            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_range').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_range" style="width:80px;" name="is_range">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_SSRKu7" class="control-group">
            <label class="control-label" for="l__">
            Шаблон от            
                    </label>
                <div class="controls"><div id="input-container-a666a500bb5cbbdddf279f156efe2970">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_from_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_from_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a666a500bb5cbbdddf279f156efe2970",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_ZYbwNh" class="control-group">
            <label class="control-label" for="l__">
            Шаблон до            
                    </label>
                <div class="controls"><div id="input-container-a337c42b307bf8869f033c7c388701ea">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="range_to_template" value="" details="где %s - значение">
                                <div class="gui-details">где %s - значение</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="range_to_template"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a337c42b307bf8869f033c7c388701ea",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_HvZnNE" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Сортировка</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_ItT9U5" class="control-group">
            <label class="control-label" for="l__">
            Порядок сортировки значений            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#sort_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="sort_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#sort_tabs_sort_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="sort_tabs_sort_russian_tab">
                <textarea class="form-control" name="sort_russian" details="" setSplitByLang="1" id="sort_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_5tL04w" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Значения по умолчанию</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_uuPeQP" class="control-group">
            <label class="control-label" for="l__">
            Показывать подсказки в админ панели?            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_autocomplete_in_admin').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_autocomplete_in_admin" style="width:80px;" name="is_autocomplete_in_admin">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_s9UqWF" class="control-group">
            <label class="control-label" for="l__">
            Список значений            
                    </label>
                <div class="controls">	        	
        
                            																		            
    
    <script>
    $(function(){
        $('#select_options_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="select_options_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#select_options_tabs_select_options_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="select_options_tabs_select_options_russian_tab">
                <div class="gui-field-details-container">  <textarea class="form-control" style="height:300px;" name="select_options_russian" value="" details="Каждое новое значение с новой строки" setSplitByLang="1" id="select_options_russian"></textarea>	<div class="gui-details">Каждое новое значение с новой строки</div></div>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><div fnx="true" class="span6"><fieldset id="guifieldset_DX6F7D" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки базы данных</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_fxVvjr" class="control-group">
            <label class="control-label" for="l__">
            Тип колонки            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#sql_type').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="sql_type" style="width:100px;" name="sql_type">
            <option selected="selected" value="VARCHAR" class="">VARCHAR</option>
            <option value="INT" class="">INT</option>
            <option value="TINYINT" class="">TINYINT</option>
            <option value="DOUBLE" class="">DOUBLE</option>
            <option value="TEXT" class="">TEXT</option>
            <option value="LONGTEXT" class="">LONGTEXT</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_nuU4Sy" class="control-group">
            <label class="control-label" for="l__">
            Длина            
                    </label>
                <div class="controls"><div id="input-container-a050e075bf253fba1056a9a12580166a">
                        <input class="form-control input-block-level" type="text" name="sql_lenght" value="255">
                        
        <script>
        $(function () {
                                                            var $input = $('input[name="sql_lenght"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-a050e075bf253fba1056a9a12580166a",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><fieldset id="guifieldset_YwdcKy" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Настройки</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_t3i51X" class="control-group">
            <label class="control-label" for="l__">
            Активный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_active').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_active" style="width:80px;" name="is_active">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_pM3wz0" class="control-group">
            <label class="control-label" for="l__">
            Индексировать            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_index').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_index" style="width:80px;" name="is_index">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_cGlQda" class="control-group">
            <label class="control-label" for="l__">
            Системный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_system').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_system" style="width:80px;" name="is_system">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_JAHiDg" class="control-group">
            <label class="control-label" for="l__">
            Обязательный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_required').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_required" style="width:80px;" name="is_required">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_NrMwDK" class="control-group">
            <label class="control-label" for="l__">
            Уникальный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique" style="width:80px;" name="is_unique">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_82PuhT" class="control-group">
            <label class="control-label" for="l__">
            Уникальный в родителе            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_unique_parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_unique_parent" style="width:80px;" name="is_unique_parent">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_1uGzlU" class="control-group">
            <label class="control-label" for="l__">
            Для настраиваемого товара            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_configurable').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_configurable" style="width:80px;" name="is_configurable">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_fZywUY" class="control-group">
            <label class="control-label" for="l__">
            В списке товаров            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_list').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_list" style="width:80px;" name="is_in_list">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_Bbf9GJ" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров подробно            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card" style="width:80px;" name="is_in_card">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_GiwzCP" class="control-group">
            <label class="control-label" for="l__">
            В карте товаров кратко             
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_card_short').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_card_short" style="width:80px;" name="is_in_card_short">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_EkMy4e" class="control-group">
            <label class="control-label" for="l__">
            В фильтре            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter" style="width:80px;" name="is_in_filter">
            <option value="0" class="">Нет</option>
            <option selected="selected" value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_fZZsNE" class="control-group">
            <label class="control-label" for="l__">
            В фильтре свернутый            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_filter_hidden').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_filter_hidden" style="width:80px;" name="is_in_filter_hidden">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_V1ZcBb" class="control-group">
            <label class="control-label" for="l__">
            В поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search" style="width:80px;" name="is_in_search">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_LXn2wd" class="control-group">
            <label class="control-label" for="l__">
            В расширенном поиске            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_in_search_advanced').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_in_search_advanced" style="width:80px;" name="is_in_search_advanced">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_XrTqKt" class="control-group">
            <label class="control-label" for="l__">
            Мультиязычный            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#split_by_lang').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="split_by_lang" style="width:80px;" name="split_by_lang">
            <option selected="selected" value="0" class="">Нет</option>
            <option value="1" class="">Да</option>
    </select>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div></div><?php }} ?>