<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 15:11:35
         compiled from "5b980e59339e9a8f2e8148198bc4c24b52cd1b05" */ ?>
<?php /*%%SmartyHeaderCode:12626771635b23acf7cdc429-89442177%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b980e59339e9a8f2e8148198bc4c24b52cd1b05' => 
    array (
      0 => '5b980e59339e9a8f2e8148198bc4c24b52cd1b05',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '12626771635b23acf7cdc429-89442177',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23acf7ced769_52417392',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23acf7ced769_52417392')) {function content_5b23acf7ced769_52417392($_smarty_tpl) {?><div fnx="true" class="span6"><fieldset id="guifieldset_xj0nKT" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_Q5SbES" class="control-group">
            <label class="control-label" for="l__">
            Родительская категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#parent').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="parent" name="parent">
            <option selected="selected" value="1" class="level-0">Корень</option>
            <option value="2" class="level-0">Товары без категории(не удалять)</option>
            <option value="1119" class="level-0">Матрасы</option>
            <option value="1120" class="level-0">Детские матрасы</option>
            <option value="1121" class="level-0">Тонкие матрасы</option>
            <option value="1124" class="level-0">Наматрасники</option>
            <option value="1125" class="level-0">Основания</option>
            <option value="1126" class="level-0">Подушки</option>
            <option value="1127" class="level-0">Кровати</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_PmQCNr" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Подушки" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_Un8qyt" class="control-group">
            <label class="control-label" for="l__">
            Url путь            
                    </label>
                <div class="controls"><div id="input-container-777db0d7c104a29c155f6e16cd54807b">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="url_key" value="podushki" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="url_key"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-777db0d7c104a29c155f6e16cd54807b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_NyflJZ" class="control-group">
            <label class="control-label" for="l__">
            Основная категория            
                    </label>
                <div class="controls">    <script>
        $(function(){
            $('#is_main').chosen();
        });
    </script>

        
        
<select class="gui-form-select" id="is_main" style="width:80px;" name="is_main">
            <option selected="selected" value="1" class="">Да</option>
            <option value="0" class="">Нет</option>
    </select>
</div>
        <div style="clear:both"></div></div><div id="guirow_iTuEhP" class="control-group">
            <label class="control-label" for="l__">
            Изображение в меню            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_aNC0Ae" class="control-group">
            <label class="control-label" for="l__">
            Баннер в меню            
                    </label>
                <div class="controls"><div id="input-container-image_banner">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_banner" name="image_banner" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_banner').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_7UXFOe" class="control-group">
            <label class="control-label" for="l__">
            Изображение на главной            
                    </label>
                <div class="controls"><div id="input-container-image_main_page">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_main_page" name="image_main_page" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_main_page').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_i3mNuG" class="control-group">
            <label class="control-label" for="l__">
            Изображение в категории            
                    </label>
                <div class="controls"><div id="input-container-image_category">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image_category" name="image_category" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)" path="categories">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image_category').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>