<?php /* Smarty version Smarty-3.1.13, created on 2018-06-18 14:59:55
         compiled from "b4138b5bb5e17c189cb84bafa37ff08c4a885ba3" */ ?>
<?php /*%%SmartyHeaderCode:3627077905b279ebb932394-61123509%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4138b5bb5e17c189cb84bafa37ff08c4a885ba3' => 
    array (
      0 => 'b4138b5bb5e17c189cb84bafa37ff08c4a885ba3',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '3627077905b279ebb932394-61123509',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b279ebb94c8f2_47390524',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b279ebb94c8f2_47390524')) {function content_5b279ebb94c8f2_47390524($_smarty_tpl) {?><div fnx="true" class="span6"><fieldset id="guifieldset_ScSyuI" class="gui-fieldset">
            <legend class="gui-fieldset-legend">SEO</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_Qi6sqA" class="control-group">
            <label class="control-label" for="l__">
            Заголовок title            
                    </label>
                <div class="controls"><div id="input-container-c49e760799098baf522d1c2283ad7087">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_title_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-33bf98c2753074fc6d813d862c7dd8d0",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_yNu4GF" class="control-group">
            <label class="control-label" for="l__">
            Заголовок H1            
                    </label>
                <div class="controls"><div id="input-container-cff7c63951e29471bdd201e681557f81">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="seo_h1_russian" value="SONVIS — матрасы для всей семьи" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="seo_h1_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-af2bcecdbc30344699fc1a1e37542a3b",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_YZ5vNF" class="control-group">
            <label class="control-label" for="l__">
            Ключевый слова            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_keywords_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_keywords_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_keywords_tabs_seo_keywords_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_keywords_tabs_seo_keywords_russian_tab">
                <textarea class="form-control" name="seo_keywords_russian" style="height:150px;" setSplitByLang="1" id="seo_keywords_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_wabTYA" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#seo_description_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_description_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_description_tabs_seo_description_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_description_tabs_seo_description_russian_tab">
                <textarea class="form-control" name="seo_description_russian" style="height:150px;" setSplitByLang="1" id="seo_description_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div><div id="guirow_25o633" class="control-group">
            <label class="control-label" for="l__">
            СЕО текст            
                    </label>
                <div class="controls">                                            <script>
        /*
        tinymce.init({
            selector: "#seo_text_russian",
            relative_urls : false,
            convert_urls : false,
            language : 'ru',
            language_url : '/langs/ru.js',
            height : 300,
            autosave_ask_before_unload: false,


            fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 26px 36px",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor moxiemanager"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor  | fontselect fontsizeselect",
            image_advtab: true

        });
        */
        $(function(){
            CKEDITOR.replace( 'seo_text_russian', {
                height: '300px'
            });
        });
    </script>

    <script>
    $(function(){
        $('#seo_text_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="seo_text_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#seo_text_tabs_seo_text_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="seo_text_tabs_seo_text_russian_tab">
                <textarea class="gui-form-wysiwyg" id="seo_text_russian" name="seo_text_russian" style="height:400px;" setSplitByLang="1"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
</textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset></div><?php }} ?>