<?php /* Smarty version Smarty-3.1.13, created on 2018-06-15 14:29:04
         compiled from "18ccb491a04f4c563feb2db5b43165da9b9715c8" */ ?>
<?php /*%%SmartyHeaderCode:10313328335b23a3004fb563-92025001%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18ccb491a04f4c563feb2db5b43165da9b9715c8' => 
    array (
      0 => '18ccb491a04f4c563feb2db5b43165da9b9715c8',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '10313328335b23a3004fb563-92025001',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5b23a300511c69_14913088',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b23a300511c69_14913088')) {function content_5b23a300511c69_14913088($_smarty_tpl) {?><fieldset id="guifieldset_nqTTyd" class="gui-fieldset">
            <legend class="gui-fieldset-legend">Общие</legend>
        <div class="gui-fieldset-content">
        <div id="guirow_pbEgKR" class="control-group">
            <label class="control-label" for="l__">
            Название            
                    </label>
                <div class="controls"><div id="input-container-d5d3db1765287eef77d7927cc956f50a">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="title_russian" value="Количество пружин на кв.м." setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="title_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-fa70c0e974125e63d45ac73aa7d157a5",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_2cTXck" class="control-group">
            <label class="control-label" for="l__">
            Единица измерения            
                    </label>
                <div class="controls"><div id="input-container-3e34bdebd9bd5edda27e8728904a2552">
                        
                                                            <div class="field-text-language">
                                        <input class="form-control input-block-level" type="text" name="unit_russian" value="" setSplitByLang="1" style="background:url(http://sonvis.fnx.dp.ua/lng/ru/media/russian.png) no-repeat 3px center;padding-left:30px;margin:2px 0;">
                </div>
                                    
        <script>
        $(function () {
                                                            var $input = $('input[name="unit_russian"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-ea41b99f7844c44b6740e1fb531a3e9c",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_j0xxhy" class="control-group">
            <label class="control-label" for="l__">
            Системное название            
                    </label>
                <div class="controls"><div id="input-container-e266081b6e1f85de55ebe69e568928b9">
        <div class="gui-field-details-container">
                            <input class="form-control input-block-level" type="text" name="sys_title" value="Number_of_springs" details="Только буквы латинского алфавита">
                                <div class="gui-details">Только буквы латинского алфавита</div>
    </div>

        <script>
        $(function () {
                                                            var $input = $('input[name="sys_title"]');

            
                        $input.autocomplete({
                appendTo: "#input-container-e266081b6e1f85de55ebe69e568928b9",
                minLength: 0,
                autoFocus: true,
                source: [],
            }).focus(function () {
                $(this).autocomplete("search");
            });
            
                        
            
                                });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_oNzfw6" class="control-group">
            <label class="control-label" for="l__">
            Изображение            
                    </label>
                <div class="controls"><div id="input-container-image">
        <div class="gui-field-details-container">
        
        
                    <input class="gui-form-image" type="file" id="image" name="image" details="*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)">
                        <div class="gui-details">*ВНИМАНИЕ: Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_)</div>
    </div>

        <script>
        $(function () {
            $('#image').ace_file_input({
                no_file: 'Изображение не выбрано ...',
                btn_choose: 'Выберите изображение',
                btn_change: 'Изменить изображение',
                droppable: true,
                onchange: null,
                thumbnail: false //| true | large
                //whitelist:'gif|png|jpg|jpeg',
                //blacklist:'exe|php'
                //onchange:''
            });
        });
    </script>
</div>
</div>
        <div style="clear:both"></div></div><div id="guirow_6SQNrV" class="control-group">
            <label class="control-label" for="l__">
            Описание            
                    </label>
                <div class="controls">	        	
        
                            									            
    
    <script>
    $(function(){
        $('#details_tabs').tab([]);
    });
</script>

<div class="tabbable">
            <ul id="details_tabs" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#details_tabs_details_russian_tab"><span class="dotted">Русский язык</span></a></li>
                    </ul>
    
    <div class="tab-content">
                    <div class="tab-pane active" id="details_tabs_details_russian_tab">
                <textarea class="form-control" style="height:150px;" name="details_russian" value="" setSplitByLang="1" id="details_russian"></textarea>            </div>
            </div>

    </div>

</div>
        <div style="clear:both"></div></div>    </div>
</fieldset><?php }} ?>