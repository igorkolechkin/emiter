<?php
	//Определяем протокол под которым работаем
	if(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		define("PROTOCOL", "https://");
	} else {
		define("PROTOCOL", "http://");
	}

	// URL list
	define('BASE_URL', PROTOCOL . $_SERVER['HTTP_HOST'] . '/');
	define('BASE_URL_CLEAN', $_SERVER['HTTP_HOST'] . '/');
	define('DOMAIN', $_SERVER['HTTP_HOST']);

	// Список путей к системным директориям
	define('APP_DIR_ABSOLUTE', BASE_DIR . 'app' . DS);
	define('ETC_DIR_ABSOLUTE', BASE_DIR . 'etc' . DS);
	define('HOME_DIR_ABSOLUTE', BASE_DIR . 'home' . DS);
	define('LNG_DIR_ABSOLUTE', BASE_DIR . 'lng' . DS);
	define('LIB_DIR_ABSOLUTE', BASE_DIR . 'lib' . DS);
	define('TMP_DIR_ABSOLUTE', BASE_DIR . 'tmp' . DS);
	define('VAR_DIR_ABSOLUTE', BASE_DIR . 'var' . DS);
	define('CACHE_DIR_ABSOLUTE', TMP_DIR_ABSOLUTE . 'cache' . DS);
	define('THEMES_DIR_ABSOLUTE', VAR_DIR_ABSOLUTE . 'themes' . DS);

	define('THEMES_DIR_CLEAN', 'var/themes/');

	define('APP_DIR_URL', BASE_URL . 'app/');
	define('HOME_DIR_URL', BASE_URL . 'home/');
	define('LNG_DIR_URL', BASE_URL . 'lng/');
	define('TMP_DIR_URL', BASE_URL . 'tmp/');
	define('VAR_DIR_URL', BASE_URL . 'var/');


	// Первоначальные настройки движка
	define('ACP_NAME', 'acp');
	define('APP_ETC_DIR', 'etc/');

	/** Для работы Xhprof */
	define('BEGIN_MICROTIME', microtime(true));
