<?php

	class Fenix_Xhprof_Model_Backend_Xhprof extends Fenix_Resource_Model {

		/**
		 * Статистика по идентификатору
		 *
		 * @param $id
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getStatisticById($id) {
			if((int)$id < 1) {
				return false;
			}

			$Engine = Fenix_Engine::getInstance();
			$Engine->setSource('xhprof/xhprof');

			$this->setTable('xhprof_statistic');

			$Select = $this->select()
			               ->setIntegrityCheck(false);

			$Select->from(array(
				's' => $this->_name,
			), $Engine->getColumns(array('prefix' => 's')));

			$Select->where('s.id = ?', (int)$id);
			$Select->limit(1);

			$Result = $this->fetchRow($Select);

			return $Result;
		}

		/**
		 * Список статистик
		 *
		 * @return Zend_Db_Select
		 */
		public function getStatisticsAsSelect() {
			$Engine = Fenix_Engine::getInstance();
			$Engine->setSource('xhprof/xhprof');

			$this->setTable('xhprof_statistic');

			$Select = $this->select()
			               ->setIntegrityCheck(false);

			$Select->from(array(
				's' => $this->_name,
			), $Engine->getColumns(array('prefix' => 's')));

			$Select->order('s.runtime desc');
			$Select->order('s.create_date desc');

			return $Select;
		}

		public function getStatisticsList() {
			return $this->fetchAll($this->getStatisticsAsSelect());
		}

		/**
		 * Список статистик
		 *
		 * @return Zend_Db_Select
		 */
		public function getUniqueStatisticsAsSelect() {
			$Engine = Fenix_Engine::getInstance();
			$Engine->setSource('xhprof/xhprof');

			$this->setTable('xhprof_statistic');

			$Select = $this->select()
			               ->setIntegrityCheck(false);

			$Select->joinInner(array(
				's2' => $this->_name,
			), 's.url = s2.url AND s.runtime = MaxRunTime', array(
				'url',
				new Zend_Db_Expr('MAX(s2.runtime) as MaxRunTime'),
			));

			$Select->from(array(
				's' => $this->_name,
			), $Engine->getColumns(array('prefix' => 's')));

			$Select->group('s.url');

			$Select->order('s.runtime desc');
			$Select->order('s.create_date desc');

			return $Select;
		}

		public function getUniqueStatisticsList() {
			return $this->fetchAll($this->getUniqueStatisticsAsSelect());
		}


		/**
		 * Удаление статистики
		 */
		public function deleteStatistic($Statistic_id) {
			if((int)$Statistic_id < 1) {
				return false;
			}
			$this->setTable('xhprof_statistic');

			return
				$this->delete($this->getAdapter()->quoteInto('id = ?', $Statistic_id));
		}


		/** Сохранение статистики */
		public function saveStatistic($data = array()) {
			if(empty($data)) {
				return false;
			}

			$this->setTable('xhprof_statistic');

			return $this->insert($data);
		}

		/** Очистка таблицы от старых строк статистики */
		public function updateTable() {
			$this->setTable('xhprof_statistic');

			return $this->delete(array(
				'create_date < (NOW() - INTERVAL 3 DAY)', // записи старше 3х дней
			));
		}

		/** Очистка таблицы от старых строк статистики */
		public function clearTable() {
			$this->setTable('xhprof_statistic');

			return $this->delete('1=1');
		}
	}