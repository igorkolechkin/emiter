<?php
	
	class Fenix_Xhprof_Controller_Admin_Index extends Fenix_Controller_Action {
		public function preDispatch() {
			$this->getHelper('rules')->checkDevRedirect();

			$Engine = new Fenix_Engine_Database();
			$Engine->setDatabaseTemplate('xhprof/xhprof')
			       ->prepare()
			       ->execute();

			Fenix::getModel('xhprof/backend_xhprof')->updateTable();
		}
		
		public function indexAction() {
			if($rows = $this->getRequest()->getQuery('row')) {
				if(isset($rows['statisticsList'])) {
					foreach((array)$rows['statisticsList'] AS $_rowId) {
						Fenix::getModel('xhprof/backend_xhprof')->deleteStatistic($_rowId);
					}

					$redirect = 'xhprof' . ($this->getRequest()->getParam('all') != null ? '?all=1' : '');
					Fenix::redirect($redirect);
				}
			}

			/**
			 * Отображение
			 */
			$Creator = Fenix::getCreatorUI();

			// Событие
			$Event = $Creator->loadPlugin('Events_Session');

			// Кнопули
			$Buttonset = $Creator->loadPlugin('Buttonset')
			                     ->setContent(array(
				                     $Creator->loadPlugin('Button')
				                             ->setValue(Fenix::lang("Отобразить все"))
				                             ->appendClass('btn-info')
				                             ->setType('button')
				                             ->setOnclick('self.location.href=\'' . Fenix::getUrl('xhprof?all=1') . '\'')
				                             ->fetch(),
				                     $Creator->loadPlugin('Button')
				                             ->setValue(Fenix::lang("Отобразить только уникальные"))
				                             ->appendClass('btn-info')
				                             ->setOnclick('self.location.href=\'' . Fenix::getUrl('xhprof') . '\'')
				                             ->setType('button')
				                             ->fetch(),
				                     $Creator->loadPlugin('Button')
				                             ->setValue(Fenix::lang("Очистить таблицу"))
				                             ->appendClass('btn-danger')
				                             ->setType('button')
				                             ->setOnclick('if(confirm(\'' . Fenix::lang('Вы уверенны что желаете очистить таблицу?') . '\')) self.location.href=\'' . Fenix::getUrl('xhprof/clearTable') . '\'')
				                             ->fetch(),
			                     ));

			// Заголовок страницы
			$Title = $Creator->loadPlugin('Title')
			                 ->setTitle(Fenix::lang("Xhprof статистика"))
			                 ->setButtonset($Buttonset->fetch());

			// Хлебные крошки
			$_crumb = array();
			$_crumb[] = array(
				'label' => Fenix::lang("Панель управления"),
				'uri'   => Fenix::getUrl(),
				'id'    => 'main',
			);
			$_crumb[] = array(
				'label' => Fenix::lang("Xhprof статистика"),
				'uri'   => '',
				'id'    => 'xhprof',
			);
			$this->_helper->BreadCrumbs($_crumb);

			if($this->getRequest()->getParam('all') != null) {
				/** Все результаты */
				$statisticsList = Fenix::getModel('xhprof/backend_xhprof')->getStatisticsAsSelect();
				$table_title_desc = ' (' . Fenix::lang('все') . ')';
			} else {
				/** только уникальные, с самым высоким показателем загрузки */
				$statisticsList = Fenix::getModel('xhprof/backend_xhprof')->getUniqueStatisticsAsSelect();
				$table_title_desc = ' (' . Fenix::lang('только уникальные и самые нагруженные') . ')';
			}

			// Таблица
			$Table = $Creator->loadPlugin('Table_Db_Generator')
			                 ->setTableId('statisticsList')
			                 ->setTitle(Fenix::lang("Собранная статистика") . $table_title_desc)
			                 ->setData($statisticsList)
			                 ->setCheckall()
			                 ->setStandartButtonset()
			                 ->setCellCallback('url', function($value, $data, $column, $table) {
				                 return '<a href="' . $value . '">' . $value . '</a>';
			                 })
			                 ->setCellCallback('runtime', function($value, $data, $column, $table) {
				                 return Fenix::getHelper('xhprof/backend_xhprof')->getRuntimeHtml($value);
			                 })
			                 ->setCellCallback('run_id', function($value, $data, $column, $table) {
				                 return '<a href="' . PROTOCOL . DOMAIN . sprintf('/var/xhprof/?run=%s&source=%s', $data['run_id'], $data['profiler_namespace']) . '">' . $value . '</a>';
			                 })
			                 ->setCellCallback('server_code', function($value, $data, $column, $table) {
				                 return Fenix::getHelper('xhprof/backend_xhprof')->getServerAnswerHtml($value);
			                 })
			                 ->setCellCallback('user_id', function($value, $data, $column, $table) {
				                 return Fenix::getHelper('xhprof/backend_xhprof')->getUserHtml($value, $data);
			                 })
			                 ->setCellCallback('create_date', function($value, $data, $column, $table) {
				                 return Fenix::getDate($value)->format('d.m.Y H:i:s');
			                 });

			$Table->addAction(array(
				'type'  => 'link',
				'icon'  => 'eye-open',
				'title' => Fenix::lang("Подробнее"),
				'url'   => Fenix::getUrl('xhprof/view/id/{$data->id}'),
			));
			$Table->addAction(array(
				'type'  => 'confirm',
				'icon'  => 'trash',
				'title' => Fenix::lang("Удалить"),
				'url'   => Fenix::getUrl('xhprof/delete/id/{$data->id}' . ($this->getRequest()->getParam('all') != null ? '?all=1' : '')),
			));

			// Тайтл страницы
			$Creator->getView()
			        ->headTitle(Fenix::lang("Xhprof статистика"));

			$Creator->setLayout()
			        ->oneColumn(array(
				        $Title->fetch(),
				        $Event->fetch(),
				        $Table->fetch('xhprof/xhprof'),
			        ));
		}

		public function viewAction() {
			$statistic_id = (int)$this->getRequest()->getParam('id');

			$currentStatistic = Fenix::getModel('xhprof/backend_xhprof')->getStatisticById($statistic_id);

			if($currentStatistic == null) {
				Fenix::getCreatorUI()
				     ->loadPlugin('Events_Session')
				     ->setType(Creator_Events::TYPE_ERROR)
				     ->setMessage(Fenix::lang("Статистика не найдена"))
				     ->saveSession();

				Fenix::redirect('xhprof');
			}

			$Creator = Fenix::getCreatorUI();

			// Хлебные крошки
			$_crumb = array();
			$_crumb[] = array(
				'label' => Fenix::lang("Панель управления"),
				'uri'   => Fenix::getUrl(),
				'id'    => 'main',
			);
			$_crumb[] = array(
				'label' => Fenix::lang("Xhprof статистика"),
				'uri'   => '',
				'id'    => 'xhprof',
			);
			$_crumb[] = array(
				'label' => Fenix::lang("Подробности статистики") . ' #' . $statistic_id,
				'uri'   => '',
				'id'    => 'xhprof',
			);
			$this->_helper->BreadCrumbs($_crumb);

			// Заголовок страницы
			$Title = $Creator->loadPlugin('Title')
			                 ->setTitle(Fenix::lang("Подробности статистики") . ' #' . $statistic_id);


			// Событие
			$Event = $Creator->loadPlugin('Events_Session');

			$Creator->setLayout()
			        ->oneColumn(array(
				        $Title->fetch(),
				        $Event->fetch(),
				        Fenix::getHelper('xhprof/backend_xhprof')->getViewContent($currentStatistic),
			        ));
		}

		public function clearTableAction() {
			Fenix::getModel('xhprof/backend_xhprof')->clearTable();

			Fenix::getCreatorUI()
			     ->loadPlugin('Events_Session')
			     ->setType(Creator_Events::TYPE_OK)
			     ->setMessage(Fenix::lang("Таблица успешно очищена"))
			     ->saveSession();

			Fenix::redirect('xhprof');
		}

		/**
		 * Удалить статистику
		 */
		public function deleteAction() {
			$statistic_id = (int)$this->getRequest()->getParam('id');

			$redirect = 'xhprof';
			$redirect .= $this->getRequest()->getParam('all') != null ? '?all=1' : '';

			if($statistic_id == null) {
				Fenix::getCreatorUI()
				     ->loadPlugin('Events_Session')
				     ->setType(Creator_Events::TYPE_ERROR)
				     ->setMessage(Fenix::lang("Статистика не найдена"))
				     ->saveSession();

				Fenix::redirect($redirect);
			}

			Fenix::getModel('xhprof/backend_xhprof')->deleteStatistic($statistic_id);

			Fenix::getCreatorUI()
			     ->loadPlugin('Events_Session')
			     ->setType(Creator_Events::TYPE_OK)
			     ->setMessage(Fenix::lang("Статистика удалена"))
			     ->saveSession();

			Fenix::redirect($redirect);
		}
	}