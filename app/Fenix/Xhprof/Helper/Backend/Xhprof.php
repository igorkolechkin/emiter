<?php

	class Fenix_Xhprof_Helper_Backend_Xhprof extends Fenix_Resource_Helper {
		public function getViewContent($statistic = null) {

			if($statistic == null || !is_object($statistic)) {
				return Fenix::getCreatorUI()
				            ->loadPlugin('Events_Session')
				            ->setType(Creator_Events::TYPE_ERROR)
				            ->setMessage(Fenix::lang("Статистика не найдена"))
				            ->fetch();
			}

			$Creator = Fenix::getCreatorUI();
			$GroupsTabs = $Creator->loadPlugin('Tabs');

			$GroupsTabs->addTab(
				'general_info',
				Fenix::lang("Общее"),
				$this->getGeneralInfo($statistic)
			);

			$GroupsTabs->addTab(
				'request_info',
				Fenix::lang('$_REQUEST'),
				$this->getJsonInfo($statistic->request_data)
			);

			$browser_data = Zend_Json::decode($statistic->browser_data);
			$GroupsTabs->addTab(
				'browser_info',
				Fenix::lang("Browser & Device"),
				$this->getPrintRInfo($browser_data)
			);


			$session_data = Zend_Json::decode($statistic->session_data);
			if(isset($browser_data['userAgent']) && isset($session_data[ '.' . $browser_data['userAgent'] ])) {
				$session_data[ '.' . $browser_data['userAgent'] ]['storage'] = unserialize($session_data[ '.' . $browser_data['userAgent'] ]['storage']);
			}
			if(isset($session_data[ '.' . $browser_data['userAgent'] ]['storage']) && isset($session_data[ '.' . $browser_data['userAgent'] ]['storage']['device'])) {
				$session_data[ '.' . $browser_data['userAgent'] ]['storage']['device'] = unserialize($session_data[ '.' . $browser_data['userAgent'] ]['storage']['device']);
			}
			$GroupsTabs->addTab(
				'session_info',
				Fenix::lang('$_SESSION'),
				$this->getPrintRInfo($session_data)
			);

			$GroupsTabs->addTab(
				'debug_data',
				Fenix::lang('Debug info'),
				$this->getPrintRInfo($statistic->debug_data)
			);

			return $Creator->loadPlugin('Container')->setContent($GroupsTabs->fetch())->compile();
		}

		public function getGeneralInfo($statistic) {
			$Creator = Fenix::getCreatorUI();
			$html = '';

			/** URL */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('URL') . ':</b> ' . '<a href="' . $statistic->url . '">' . $statistic->url . '</a>')
			                 ->fetch();

			/** Собрана */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Собрана') . ':</b> ' . Fenix::getDate($statistic->create_date)->format('d.m.Y H:i:s'))
			                 ->fetch();

			/** Время выполнения */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Время выполнения') . ':</b> ' . $this->getRuntimeHtml($statistic->runtime))
			                 ->fetch();

			/** Ответ сервера (код) */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Ответ сервера (код)') . ':</b> ' . $this->getServerAnswerHtml($statistic->server_code))
			                 ->fetch();

			/** Ответ сервера (код) */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Пользователь') . ':</b> ' . $this->getUserHtml($statistic->user_id, $statistic))
			                 ->fetch();

			/** ID xhprof статистики */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<br><br><b>' . Fenix::lang('ID xhprof статистики') . ':</b> ' . $statistic->run_id)
			                 ->fetch();

			/** profiler_namespace */
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Profiler namespace') . ':</b> ' . $statistic->profiler_namespace)
			                 ->fetch();

			/** Открыть статистику XHprof */
			$profiler_url = PROTOCOL . DOMAIN . sprintf('/var/xhprof/?run=%s&source=%s', $statistic->run_id, $statistic->profiler_namespace);
			$html .= $Creator->loadPlugin('Row')
			                 ->setContent('<b>' . Fenix::lang('Открыть статистику XHprof') . ':</b> <a href="' . $profiler_url . '">' . $profiler_url . '</a>')
			                 ->fetch();

			return $Creator->loadPlugin('Row')->setContent($html)->fetch();
		}

		public function getJsonInfo($json = null) {
			$Creator = Fenix::getCreatorUI();

			return $Creator->loadPlugin('Row')->setContent($this->getPrintRInfo(Zend_Json::decode($json)))->fetch();
		}

		public function getUnserializeInfo($serialize_info = '') {
			$Creator = Fenix::getCreatorUI();

			return $Creator->loadPlugin('Row')->setContent($this->getPrintRInfo(unserialize($serialize_info)))->fetch();
		}

		public function getPrintRInfo($data) {

			ob_start();
			echo '<pre>';
			if(!empty($data)) {
				try {
					print_r($data);
				} catch(Exception $e) {
					Fenix::dump($data);
					new Fenix_Exception($e);
				}
			} else {
				echo 'data empty';
			}
			echo '</pre>';
			$html = ob_get_contents();
			ob_end_clean();

			return $html;
		}

		public function getRuntimeHtml($value) {
			$runtime = $value / 1000;

			/** Если загрузка длилась более 2 секунд, то стоит об этом задуматься */
			if($runtime > 2) {
				$class = 'red';
			} else {
				$class = 'green';
			}

			return '<b class="' . $class . '">' . $runtime . ' ' . Fenix::lang('секунд') . '</b>';
		}

		public function getServerAnswerHtml($server_code) {
			$code_type = floor($server_code / 100);

			switch($code_type) {
				case '1':
					$class = 'information blue';
					$answer = 'Information';
					break;
				case '2':
					$class = 'success green';
					$answer = 'Success';
					break;
				case '3':
					$class = 'redirection orange';
					$answer = 'Redirection';
					break;
				case '4':
					$class = 'client-error red';
					$answer = 'Client Error';
					break;
				case '5':
					$class = 'server-error red';
					$answer = 'Server Error';
					break;
				default:
					$class = 'default';
					$answer = 'Default';
					break;
			}

			return '<b class="' . $class . '">' . $server_code . ' - ' . $answer . '</b>';
		}

		public function getUserHtml($user_id, $data) {

			if(is_object($data)) {
				$data = $data->toArray();
			}

			$pathinfo = Fenix::getRequest()->preparePathinfo(str_replace(array(
				'http://' . $_SERVER['HTTP_HOST'], 'https://' . $_SERVER['HTTP_HOST'],
			), '', $data['url']));

			if($pathinfo->isAcp) {
				$user = Fenix::getModel('core/administrators')->getAdministratorById((int)$user_id);

				if($user) {
					$url = '/acp/core/administrators/edit/id/' . (int)$user_id;

					$name = $user->fullname;
					$name .= ($user->job != null) ? ' (' . $user->job . ')' : '';
				}
			} else {
				$user = Fenix::getModel('customer/backend_customer')->getCustomerById((int)$user_id);

				if($user) {
					$url = '/acp/customer/edit/id/' . (int)$user_id;
					$name = ($user->fullname != null) ? $user->fullname : 'без имени';
				}
			}

			$html = '';
			if($user) {
				$html .= '<a href="' . $url . '"><b>' . $name . '</b></a>';
			} elseif($pathinfo->isAcp) {
				$html .= '<b class="red">' . Fenix::lang('Администратор с id') . ' ' . (int)$user_id . ' ' . Fenix::lang('не найден') . '</b>';
			} elseif((int)$user_id == 0) {
				$html .= '<b class="purple">' . Fenix::lang('Не зарегистрированный пользователь') . '</b>';
			} else {
				$html .= '<b class="red">' . Fenix::lang('Пользователь с id') . ' ' . (int)$user_id . ' ' . Fenix::lang('не найден') . '</b>';
			}

			return $html;
		}


		/** Отображаем блок на фронте */
		public function viewFooterInfo($msg = '') {
			$html = '<style>
						body {
							min-height: 100vh;
							margin-bottom: 100px;
						}
						body:after{
							content: "";
							display: block;
							clear: both;
						}
						#xhprof_info {    
							    position: fixed;
							    z-index: 10000;
							    bottom: 0;
							    left: 50%;
							    width: 300px;
							    margin: 10px;
							    margin-left: -150px;
							    background: #2d2d2d;
							    border: 2px solid #5d5d5d;
							    border-radius: 5px;
							    text-align: center;
							    color: #fefefe;
							    padding: 10px;
							    font-size: 14px;
							    opacity: 0.5;
							    transition: 0.3s ease;
						}
						#xhprof_info * {
							margin-bottom: 3px;
						}
						#xhprof_info:hover{
							opacity: 1;
						}
						#xhprof_info .title {
							text-transform: uppercase;
							font-size: 16px;
						}
						#xhprof_info a {
							color: #fefefe;
							text-transform: uppercase;
						}
                    </style>';

			$html .= '<div id="xhprof_info">';
			$html .= '	<div class="msg">
							<b class="title">Xhprof end!</b>
							<p>' . $msg . '</p>
						</div>';
			$html .= '</div>';
			echo $html;
		}
	}