<?php
class Fenix_Actions_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        Fenix_Debug::log('index controller begin');
        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::lang("Акции"),
            'uri'   => Fenix::getUrl('actions'),
            'id'    => 'actions'
        );


        $category_id = Fenix::ALL;

        if(Fenix::getRequest()->getUriSegment(1)!=null){
            $active_category = Fenix::getModel('catalog/categories')->getCategoryByUrlKey(Fenix::getRequest()->getUriSegment(1));
            $category_id = $active_category->id;
            $_crumbs[] = array(
                'label' => $active_category->title,
                'uri'   => '/actions',
                'id'    => 'actions'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Fenix::dump($cat_list);

        $actions_list = Fenix::getModel('sale/sale')->getSaleListByCategory(array(
            'is_active'   => Fenix::IS_ACTIVE,
            'on_main'     => Fenix::ALL,
            'category_id' => $category_id
        ));

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('actions'),
            'title' => Fenix::lang("Акции"),
        ), $actions_list);

        /*if(Fenix::getRequest()->getUriSegment(1)!=null){
            $actions_list = Fenix::getModel('sale/sale')->getCategorySaleList($active_category->id);
        }*/

        $cat_list = Fenix::getModel('sale/sale')->getSaleCategories();

        $Creator->getView()->assign(array(
            'active'   => Fenix::getRequest()->getUriSegment(1),
            'categories'   => $cat_list,
            'actions'   => $actions_list
        ));

        $Creator ->setLayout()
            ->render('actions/list.phtml');

        Fenix_Debug::log('index controller end');
    }

    public function viewAction()
    {
        Fenix_Debug::log('view controller begin');
        $id = Fenix::getRequest()->getUriSegment(2);
        $action = Fenix::getModel('sale/sale')->getSaleById($id);

        //Fenix::dump($action);
        if($action->only_view==1 && $action->attributeset !='sale'){
            $products_list=null;
        }
        elseif($action->attributeset =='sale'){
            $products_list = Fenix::getModel('catalog/products')->getSaleProductsListLight($action);
        }
        else{
            $products_list = Fenix::getModel('catalog/products')->getSaleProductsList($action);
        }
        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::lang("Акции"),
            'uri'   => Fenix::getUrl('actions'),
            'id'    => 'actions'
        );
        $_crumbs[] = array(
            'label' => $action->title,
            'uri'   => Fenix::getUrl(''),
            'id'    => $action->id
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        $cat_list = Fenix::getModel('sale/sale')->getSaleCategories();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('actions'),//TODO тут какой-то треш, я хз что конкретно указывать ИСПРАВЬТЕ кто в курсе и закомитте!
            'title' => $action->title,
        ), $products_list);

        $Creator->getView()->assign(array(
            'action'   => $action,
            'categories'   => $cat_list,
            'products'   => $products_list
        ));

        $Creator ->setLayout()
            ->render('actions/action.phtml');

        Fenix_Debug::log('view controller end');
    }

    public function viewtAction()
    {
        Fenix_Debug::log('index controller begin');
        /**
         * Обработка пост запросов
         */
        //Список товаров
        $id = Fenix::getRequest()->getUriSegment(2);
        $action = Fenix::getModel('sale/sale')->getSaleById($id);
        $productsList = Fenix::getModel('catalog/products')->getSaleProductsList($action);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        $_crumbs[] = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'catalog'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();
        $seo = Fenix::getModel('core/seo')->getSeoByName('catalog');
        if ($seo) {
            //Устанавливаем Meta-данные
            $this->setMeta($Creator, array(
                'url'   => Fenix::getUrl(),//TODO тут какой-то треш, я хз что конкретно указывать ИСПРАВЬТЕ кто в курсе и закомитте!
                'title' => $seo->seo_title,
                'keywords' => $seo->seo_keywords,
                'description' => $seo->seo_description,
            ), $productsList);
        } else {
            //Устанавливаем Meta-данные
            $this->setMeta($Creator, array(
                'url'   => Fenix::getUrl(),//TODO тут какой-то треш, я хз что конкретно указывать ИСПРАВЬТЕ кто в курсе и закомитте!
                'title' => Fenix::lang("Каталог"),
            ), $productsList);
        }

        $Creator->getView()->assign(array(
            //'category'   => $currentCategory,
            //'navigation' => $currentNavigation,
            //'collection' => $categoryInfo,
            'products'   => $productsList
        ));
        $Creator ->setLayout()
            ->render('actions/products_list.phtml');
        Fenix_Debug::log('index controller end');
    }
}