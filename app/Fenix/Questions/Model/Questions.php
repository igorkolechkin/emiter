<?php
class Fenix_Questions_Model_Questions extends Fenix_Resource_Model
{
    /**
     * По умолчанию вопросов на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний вопросов
     */
    const DEFAULT_LAST_ARTICLES = 1;

    public function getLastQuestions()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/questions');

        $this   ->setTable('questions');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('questions/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('questions/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список вопросов в рубрике или общий список вопросов
     *
     * @param null $rubric
     * @return Zend_Paginator
     */
    public function getQuestionsList($rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/questions');

        $this   ->setTable('questions');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('questions_relations')
            ), 'a.id = _r.article_id', false);
            $Select->join(array(
                'r' => $this->getTable('questions_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('questions/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('questions/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Статья по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getArticle($url, $rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/questions');

        $this   ->setTable('questions');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', urldecode($url))
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('questions_relations')
            ), 'a.id = _r.article_id', false);
            $Select->join(array(
                'r' => $this->getTable('questions_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function addArticle($data, $_rubricId)
    {
        $id = $this->setTable('questions')
                        ->insert($data);

        $data = array();
        $data['url_key'] = $id;
        $this->setTable('questions')
            ->update($data, 'id = ' . (int) $id);

        // Рубрика
        $this->setTable('questions_relations')
            ->delete('article_id = ' . (int) $id);

        $this->insert(array(
            'article_id' => (int) $id,
            'rubric_id'  => (int) $_rubricId
        ));

        return (int) $id;
    }
}