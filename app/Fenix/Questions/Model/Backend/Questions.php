<?php
class Fenix_Questions_Model_Backend_Questions extends Fenix_Resource_Model
{
    /**
     * Статья по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getArticleById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/questions');

        $this->setTable('questions');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getQuestionsAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/questions');

        $this->setTable('questions');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.create_date desc');

        return $Select;
    }

    /**
     * Новая статья
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addArticle($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->addRecord($req);

        // Удаляем все старые связи
        $this->setTable('questions_relations')
            ->delete('article_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'article_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }

        return (int) $id;
    }

    /**
     * Редактировать статью
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editArticle($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->editRecord($current, $req);

        // Удаляем все старые связи
        $this->setTable('questions_relations')
             ->delete('article_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'article_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }

        return (int) $id;
    }

    /**
     * Удаление статьи
     *
     * @param $current
     */
    public function deleteArticle($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('questions/questions', $current->attributeset)
                 ->deleteRecord($current);

        $this->setTable('questions_relations')
             ->delete('article_id = ' . (int) $current->id);
    }
}