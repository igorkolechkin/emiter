<?php
class Fenix_Questions_Model_Rubric extends Fenix_Resource_Model
{
    /**
     * Рубрика по URL
     *
     * @param $url
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRubricByUrl($url)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/rubric');

        $this->setTable('questions_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->where('r.url_key = ?', $url);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список рубрик
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getRubricList()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('questions/rubric');

        $this->setTable('questions_rubric');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Result = $this->fetchAll($Select);

        return $Result;
    }
}