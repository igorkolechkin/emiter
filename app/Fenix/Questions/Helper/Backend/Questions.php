<?php
class Fenix_Questions_Helper_Backend_Questions extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'questions' && Fenix::getRequest()->getUrlSegment(1) != 'rubric';
    }
}