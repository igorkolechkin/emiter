<?php
class Fenix_Questions_Controller_Index extends Fenix_Controller_Action
{
    /**
     * Роутинг статей
     *
     * @throws Exception
     */
    public function preDispatch()
    {
        $first  = $this->getRequest()->getUrlSegment(1);
        $second = $this->getRequest()->getUrlSegment(2);

        if ($first != null && $second != null) {
            $rubric  = Fenix::getModel('questions/rubric')->getRubricByUrl($first);
            if ($rubric == null) {
                throw new Exception();
            }
            else {
                $article = Fenix::getModel('questions/questions')->getArticle($second, $rubric);
                if ($article == null) {
                    throw new Exception();
                }
                else {
                    $this->articleAction($article, $rubric);
                }
            }
        }
        else {
            if ($first != null && $second == null) {
                $article = Fenix::getModel('questions/questions')->getArticle($first);
                if ($article == null) {
                    $rubric = Fenix::getModel('questions/rubric')->getRubricByUrl($first);
                    if ($rubric == null) {
                        throw new Exception();
                    }
                    else {
                        $this->_indexAction($rubric);
                    }
                }
                else {
                    $this->articleAction($article);
                }
            }
            else {
                $this->_indexAction();
            }
        }
    }

    /**
     * Нужен для работы диспетчера
     */
    public function indexAction(){}

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     */
    private function _indexAction($rubric = null)
    {
        $collection = Fenix::getCollection('questions/load')->load($rubric);
        $_rubric    = $collection->getRubric();
        $_questions  = Fenix::getModel('questions/questions')->getQuestionsList($rubric);


        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('questions/seo/h1', Fenix::lang("Статьи")),
            'uri'   => Fenix::getUrl('questions'),
            'id'    => 'questions'
        );
        if ($_rubric !== false) {
            $_crumbs[] = array(
                'label' => $_rubric->getTitle(),
                'uri'   => $_rubric->getUrl(),
                'id'    => 'rubric'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();
        
        //Устанавливаем Meta-данные
        if($_rubric !== false) {
            $this->setMeta($Creator, $_rubric, $_questions);
        } else {
            $this->setMeta($Creator, array(
                'url'         => Fenix::getUrl('questions'),
                'title'       => Fenix::getConfig('questions/seo/title'),
                'keywords'    => Fenix::getConfig('questions/seo/keywords'),
                'description' => Fenix::getConfig('questions/seo/description'),
            ), $_questions);
        }

        $Creator ->getView()->assign(array(
            'rubric'     => $_rubric,
            'questions'   => $_questions,
            'collection' => $collection
        ));

        $Creator ->setLayout()
                 ->render('questions/list.phtml');
    }

    /**
     * Читаем статью
     *
     * @param $article
     * @param null $rubric
     */
    private function articleAction($article, $rubric = null)
    {
        $_article = Fenix::getCollection('questions/article')->load($article, $rubric);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('questions/seo/h1', Fenix::lang("Новости")),
            'uri'   => Fenix::getUrl('questions'),
            'id'    => 'questions'
        );
        if ($rubric != null) {
            $_crumbs[] = array(
                'label' => $rubric->title,
                'uri'   => Fenix::getUrl('questions/' . $rubric->url_key),
                'id'    => 'rubric'
            );
        }

        $_crumbs[] = array(
            'label' => $_article->getTitle(),
            'uri'   => $_article->getUrl(),
            'id'    => 'article'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $_article);

        $Creator ->getView()->assign(array(
            'rubric'  => $rubric,
            'article' => $_article
        ));
        $Creator ->setLayout()
                 ->render('questions/read.phtml');
    }
}