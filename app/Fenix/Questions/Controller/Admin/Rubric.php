<?php
class Fenix_Questions_Controller_Admin_Rubric extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('questionsAll');

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('questions/rubric')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('questions/relations')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('questions/questions')
                ->prepare()
                ->execute();
    }

    /**
     * Управление рубриками
     */
    public function indexAction()
    {
        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentRubric = Fenix::getModel('questions/backend_rubric')->getRubricById($_rowId);
                    Fenix::getModel('questions/backend_rubric')->deleteRubric($currentRubric);
                }

                Fenix::redirect('questions/rubric');
            }
        }

        $rubricList  = Fenix::getModel('questions/backend_rubric')->getRubricsAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая рубрика"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('questions/rubric/add') . '\'')
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Управление рубриками"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'questions',
            'uri'   => Fenix::getUrl('questions')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('questions/rubric')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление рубриками"))
            ->setData($rubricList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('questions/rubric/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('questions/rubric/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Рубрики статей"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('questions/rubric')
        ));
    }

    /**
     * Новая рубрика
     */
    public function addAction()
    {
        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'questions',
            'uri'   => Fenix::getUrl('questions')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('questions/rubric')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('questions/rubric', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('questions/backend_rubric')->addRubric($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Рубрика создана"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('questions/rubric');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('questions/rubric/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('questions/rubric/edit/id/' . $id);
            }

            Fenix::redirect('questions/rubric');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая рубрика"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать статью
     */
    public function editAction()
    {
        $currentRubric = Fenix::getModel('questions/backend_rubric')->getRubricById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRubric == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Рубрика не найдена"))
                ->saveSession();

            Fenix::redirect('questions/rubric');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'questions',
            'uri'   => Fenix::getUrl('questions')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('questions/rubric')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentRubric->toArray())
                   ->setData('current', $currentRubric);

        // Источник
        $Form      ->setSource('questions/rubric', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('questions/backend_rubric')->editRubric($Form, $currentRubric, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Рубрика отредактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('questions/rubric');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('questions/rubric/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('questions/rubric/edit/id/' . $id);
            }

            Fenix::redirect('questions/rubric');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать рубрику"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить рубрику
     */
    public function deleteAction()
    {
        $currentRubric = Fenix::getModel('questions/backend_rubric')->getRubricById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRubric == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Рубрика не найдена"))
                ->saveSession();

            Fenix::redirect('questions/rubric');
        }

        Fenix::getModel('questions/backend_rubric')->deleteRubric($currentRubric);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Рубрика удалена"))
            ->saveSession();

        Fenix::redirect('questions/rubric');
    }
}