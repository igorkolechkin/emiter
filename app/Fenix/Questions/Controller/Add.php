<?php
class Fenix_Questions_Controller_Add extends Fenix_Controller_Action
{

    public function preDispatch()
    {
    }

    public function indexAction(){
        $rubric = Fenix::getRequest()->getPost('rubric');
        $data['name'] = Fenix::getRequest()->getPost('name');
        $data['title'] = Fenix::getRequest()->getPost('name');
        $data['specialisation'] = Fenix::getRequest()->getPost('specialisation');
        $data['question'] = Fenix::getRequest()->getPost('question');
        $data['content'] = Fenix::getRequest()->getPost('content');
        $data['lang'] = Fenix::getRequest()->getPost('lang');
        $data['user_id'] = Fenix::getRequest()->getPost('user_id');
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['is_public'] = '0';

        if(Fenix::getRequest()->getPost('name')!='' && Fenix::getRequest()->getPost('specialisation')!='' && Fenix::getRequest()->getPost('rubric')!='' && Fenix::getRequest()->getPost('question')!='' && Fenix::getRequest()->getPost('content')!=''){
            Fenix::getModel('questions/questions')->addArticle($data,$rubric);
            echo $data['name'].', Ваш вопрос добавлен, после одобрения администратором он появится в разделе <a href="/questions">Вопросы</a>';
        }
        else{
            echo 'Ошибочка, данные не заполнены';
        }
    }
}