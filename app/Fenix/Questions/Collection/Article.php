<?php
class Fenix_Questions_Collection_Article extends Fenix_Object
{
    use Fenix_Traits_SeoCollection;

    public function load($article, $rubric)
    {
        if ($article instanceof Zend_Db_Table_Row) {
            $article = $article->toArray();
        }

        $article['seo']     = $this->getSeoMetadata($article, 'questions_article');
        $article['url']     = Fenix::getUrl('questions/' . ($rubric != null ? $rubric->url_key . '/' : null) . $article['url_key']);

        $this->setData($article);

        return $this;
    }

    public function getGallery()
    {
        $_gallery = (array) unserialize($this->getData('gallery'));

        $data = array();
        foreach ($_gallery AS $_image) {
            $data[] = Fenix::getCollection('questions/gallery')->setImage($_image);
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }


    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getImage()
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_URL . $this->getData('image');
            return $image;
        }

        return false;
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function getImageFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');

            return Fenix_Image::frame($image, $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageResize($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');

            return Fenix_Image::resize($image, $width, $height);
        }

        return false;
    }

    public function getImageAdapt($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');

            return Fenix_Image::adapt($image, $width, $height);
        }

        return false;
    }
}