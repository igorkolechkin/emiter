<?php
class Fenix_Questions_Collection_Gallery extends Fenix_Object
{
    public function setImage($_image)
    {
        $this->setData($_image);

        return $this;
    }

    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getImage()
    {
        if ($this->getData('image') != null) {
            return HOME_DIR_URL . $this->getData('image');
        }

        return false;
    }

    public function getTitle()
    {
        return $this->getData('meta_title');
    }

    public function getAlt()
    {
        return $this->getData('meta_alt');
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function getImageFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');
            return Fenix_Image::frame($image, $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageResize($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');

            return Fenix_Image::resize($image, $width, $height);
        }

        return false;
    }
    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageAdapt($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $image = HOME_DIR_ABSOLUTE . $this->getData('image');

            return Fenix_Image::adapt($image, $width, $height);
        }

        return false;
    }
}