<?php
class Fenix_Questions_Collection_Questions extends Fenix_Resource_Collection
{
    public function load($rubric)
    {
        $_questions = Fenix::getModel('questions/questions')->getQuestionsList($rubric);

        $_questions = $_questions->getCurrentItems()->toArray();

        $_data = array();
        foreach ($_questions AS $_article) {
            $_data[] = Fenix::getCollection('questions/article')->load($_article, $rubric);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data
        ));
    }

    public function getLastQuestions()
    {
        $_questions = Fenix::getModel('questions/questions')->getLastQuestions();

        $_data = array();
        foreach ($_questions AS $_article) {
            $_data[] = Fenix::getCollection('questions/article')->load($_article, null);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}