<?php
class Fenix_Questions_Collection_Load extends Fenix_Resource_Collection
{
    public function load($rubric = null)
    {
        return new Fenix_Object(array(
            'data' => array(
                'rubric'  => Fenix::getCollection('questions/rubric')->load($rubric),
                'questions' => Fenix::getCollection('questions/questions')->load($rubric)
            )
        ));
    }
}