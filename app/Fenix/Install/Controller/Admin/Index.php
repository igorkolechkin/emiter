<?php

/**
 * Created by PhpStorm.
 * User: dobrik
 * Date: 31.05.2017
 * Time: 16:57
 */
class Fenix_Install_Controller_Admin_Index extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkDevRedirect();
    }

    public function indexAction()
    {
        $Creator = Fenix::getCreatorUI();

        $Creator->getView()
            ->headTitle(Fenix::lang("Установка"));

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка"),
            'uri' => Fenix::getUrl('install'),
            'id' => 'sync'
        );
        $this->_helper->BreadCrumbs($_crumb);

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Начать установку"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick("self.location.href='" . Fenix::getUrl('install/stepone') . "'")
                    ->fetch()
            ));

        $Event = $Creator->loadPlugin('Events')->appendClass(Creator_Events::TYPE_OK)->setMessage('Тут можно подготовить CMS к работе');

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Fenix Engine Установка"))
            ->setButtonset($Buttonset->fetch());

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            array($Event->fetch())
        ));

    }

    public function steponeAction()
    {
        $Creator = Fenix::getCreatorUI();
        $_directories = Fenix::getModel('install/directories');

        $Event = $Creator->loadPlugin('Events');
        $directories = $Creator->loadPlugin('Row');
        if (($check = $_directories->checkDirectories()) !== true) {
            $Event->appendClass(Creator_Events::TYPE_ERROR)->setMessage($check);
        } else {
            $Event->appendClass(Creator_Events::TYPE_OK)->setMessage('Директории успешно созданы, проверьте права доступа на папки:');
            $dirs = array();
            foreach ($_directories->getDirectoriesList() as $dir) {
                $dirs[] = $Creator->loadPlugin('Row')->setContent($dir)->fetch();
            }
            $directories->setContent($dirs)->setStyle('color:#cc7832;background-color:#232525;border-radius:3px; padding: 10px');
        }

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка"),
            'uri' => Fenix::getUrl('install'),
            'id' => 'sync'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Шаг 1 (Директории)"),
            'uri' => Fenix::getUrl('stepOne'),
            'id' => 'sync'
        );

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Шаг 2"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick("self.location.href='" . Fenix::getUrl('install/steptwo') . "'")
                    ->fetch()
            ));

        $this->_helper->BreadCrumbs($_crumb);

        $Creator->getView()
            ->headTitle(Fenix::lang("Установка - Шаг 1"));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Fenix Engine Установка - Шаг 1"))
            ->setButtonset($Buttonset->fetch());

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            array($Event->fetch()
            , $directories->fetch())
        ));
    }


    /**
     *
     * Экшн отображения второго шага, обработки всех XML
     *
     */
    public function steptwoAction()
    {

        //$Engine = Fenix_Engine_Database::getInstance();
        Fenix::getModel('core/cache')->cleanCache('table');

        /*$Engine->setDatabaseTemplate('catalog/attributes')
            ->prepare()
            ->execute();*/

        $Creator = Fenix::getCreatorUI();

        $Event = $Creator->loadPlugin('Events');

        $xmls_count = Fenix::getModel('install/database')->getXmlsCount();

        $Progressbar = $Creator->loadPlugin('Progressbar')
            ->setId('productsEditEvents')
            ->setMin(0)
            ->setMax($xmls_count)
            ->setValue(0)
            ->setLabel('Таблицы обрабатываются...')
            ->setIsStriped(true)
            ->setColor(Creator_Progressbar::BAR_COLOR_SUCCESS)
            ->setProcessUrl(Fenix::getUrl('install/database'))
            ->setParams(array(
                'event' => 'start',
                'n'     => 0
            ));

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка"),
            'uri' => Fenix::getUrl('install'),
            'id' => 'sync'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Шаг 2 (База данных)"),
            'uri' => Fenix::getUrl('stepOne'),
            'id' => 'sync'
        );

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Шаг 3"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick("self.location.href='" . Fenix::getUrl('install/stepthree') . "'")
                    ->fetch()
            ));

        $this->_helper->BreadCrumbs($_crumb);

        $Creator->getView()
            ->headTitle(Fenix::lang("Установка - Шаг 2"));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Fenix Engine Установка - Шаг 2, установка базы данных"))
            ->setButtonset($Buttonset->fetch());

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            array(/*$Event->fetch(),*/
                $Progressbar->fetch())
        ));
    }

    /**
     * Генерация метаданных для шторма чтоб были подсказочки
     */
    public function stepthreeAction()
    {
        $Creator = Fenix::getCreatorUI();

        $Event = $Creator->loadPlugin('Events');

        $Result = Fenix::getModel('install/metadata')->generateMeta();

        if($Result){
            $Event->appendClass(Creator_Events::TYPE_OK)->setMessage('Файл ".phpstorm.meta.php" успешно сгенерирован, обновите его в корне сайта');
        }else{
            $Event->appendClass(Creator_Events::TYPE_ERROR)->setMessage('Ошибка генерации файла, проверьте права доступа к файлу ".phpstorm.meta.php" в корне сайта');
        }

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка"),
            'uri' => Fenix::getUrl('install'),
            'id' => 'sync'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Шаг 3 (Мета данные для IDE phpstorm)"),
            'uri' => Fenix::getUrl('stepThree'),
            'id' => 'sync'
        );

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Завершить установку"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick("self.location.href='" . Fenix::getUrl('install/finish') . "'")
                    ->fetch()
            ));

        $this->_helper->BreadCrumbs($_crumb);

        $Creator->getView()
            ->headTitle(Fenix::lang("Установка - Шаг 3"));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Fenix Engine Установка - Шаг 3, генерация мета данных для IDE"))
            ->setButtonset($Buttonset->fetch());

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            array($Event->fetch())
        ));
    }

    /**
     *
     * Экшн для аяксового отображения процесса обработки всех XML'ек
     *
     * @throws Exception
     */
    public function databaseAction()
    {
        if(!$this->getRequest()->isXmlHttpRequest()){
            throw new Exception();
        }

        $params = $this->getRequest()->getPost('params');
        $params = unserialize($params);
        $n = $params['n'];

        $_tables = Fenix::getModel('install/database');
        $result = $_tables->prepareTables($n, 5);
        $params['n'] = $result->current;
        $percent = round($result->current / $result->count * 100, 2);
        $data = array(
            'values' => array(
                $percent
            ),
            'labels' => array(
                'Текущая XML : ' . $result->table . ' ' . $result->current . '/' . $result->count . ' ' . $percent . '%'
            ),
            'params' => serialize($params),
            'info'=>serialize($result)
        );

        echo json_encode($data);
    }

    public function finishAction()
    {
        $Creator = Fenix::getCreatorUI();
        $Event = $Creator->loadPlugin('Events');
        $Event->appendClass(Creator_Events::TYPE_OK)->setMessage('Установка прошла успешно! Сайт готов к работе!');

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка"),
            'uri' => Fenix::getUrl('install'),
            'id' => 'sync'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Установка завершена"),
            'uri' => Fenix::getUrl('stepOne'),
            'id' => 'sync'
        );

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Fenix Engine Установка"));

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            array($Event->fetch())
        ));
    }
}