<?php

class Fenix_Install_Model_Directories extends Fenix_Resource_Model
{
    // путь к конфигу кеша
    private $cache_xml_path;

    public function __construct($config = array())
    {
        $this->cache_xml_path = APP_DIR_ABSOLUTE . 'Fenix/Core/etc/cache.xml';
    }

    /**
     *
     * Массив корневых директорий для загрузки всяких штук
     *
     * @return array
     */
    private function getDefaultDirectories()
    {
        return array(
            HOME_DIR_ABSOLUTE,
            TMP_DIR_ABSOLUTE,
            TMP_DIR_ABSOLUTE . 'uploads',
        );
    }

    /**
     *
     * Получение всех директорий и унификация
     *
     * @return array
     */
    public function getDirectoriesList()
    {
        $directories = $this->getDefaultDirectories();
        //сливаем списки полученных директорий
        $directories = array_merge($directories, $this->getCacheDirectories());
        // убираем дубликаты чтоб зря не проверять
        $directories = array_unique($directories);

        return $directories;
    }

    /**
     *
     * Получение директорий кеша
     *
     * @return array
     */
    private function getCacheDirectories()
    {
        $path_list = array();
        if (($xmlContent = file_get_contents($this->cache_xml_path)) !== false) {
            $doc = new DOMDocument();
            $doc->loadXML($xmlContent);
            $xpath = new DOMXpath($doc);
            $pathes = $xpath->query('/core/cache/*/backendOptions/cache_dir');
            foreach ($pathes as $path) {
                $path_list[] = CACHE_DIR_ABSOLUTE . $path->getAttribute('value');
            }
        }

        return $path_list;
    }

    /**
     * Создание системных директорий
     *
     * Проверяем существует ли директория если нет, создаем
     */
    public function checkDirectories()
    {
        foreach ($this->getDirectoriesList() as $dir) {
            if (!is_dir($dir)) {
                if (!mkdir($dir, 0777, true)) return 'Не удалось создать директорию : ' . $dir;
            }
        }
        return true;
    }

}