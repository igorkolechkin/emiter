<?php

class Fenix_Install_Model_Metadata extends Fenix_Resource_Model
{

    private $collect = array('Collection', 'Model', 'Helper');

    private $folders = array();

    public function generateMeta()
    {
        $classes = $this->collectClasses();

        return $this->writeMetaIDE($classes);
    }


    /**
     * Собираем классы, форматируем аля под стиль Fenix::getModel('catalog/products')
     */
    public function collectClasses()
    {
        //получаем все папки в свойтво $folders
        $this->getFoldersRecursive(BASE_DIR . 'app');
        //получаем все php файлы внутри
        $files = $this->getClasses();

        //получаем имена классов
        $classes = $this->get_php_classes($files);
        //Отделяем Fenix или локал че оно там у нас
        //строим ассоц массив где ключ отформатированное имя класса => значение его полный путь
        $tmp_classes = array();
        foreach ($classes as $class) {
            $tmp_class = str_replace('Fenix_', '', $class);
            $tmp_class = str_replace('Local_', '', $tmp_class);
            $tmp_classes[$class . '::class'] = $tmp_class;
        }
        //переворачиваем массив чтоб остались Local после удаления повторений
        $tmp_classes = array_reverse($tmp_classes);
        $tmp_classes = array_unique($tmp_classes);

        return $this->getClassesArray($tmp_classes);
    }

    /**
     *
     * Пишем в файл инфо по классам
     *
     * @param $classes
     */
    private function writeMetaIDE($classes)
    {
        $string_to_write = '<?php' . "\n"; // "\t" .
        $string_to_write .= 'namespace PHPSTORM_META {' . "\n"; // "\t" .
        foreach ($classes as $type => $_classes) {
            $string_to_write .= "\t" . 'override(\Fenix::get' . ucfirst($type) . '(0),' . "\n";
            $string_to_write .= "\t\t" . 'map([' . "\n";
            foreach ($_classes as $fenix_style => $full_class) {
                $string_to_write .= "\t\t\t" . '"' . $fenix_style . '" => ' . "\\" . $full_class . ',' . "\n";
            }
            $string_to_write .= "\t\t" . ']));' . "\n";

            $string_to_write .= "\n";
        }
        $string_to_write .= '}';
        
        $result = file_put_contents(BASE_DIR . '.phpstorm.meta.php', $string_to_write);
        return $result===false?false:true;
    }

    /**
     *
     * строим массив коллекций хелперов и моделей в правильном формате
     *
     * @param array $classes
     * @return array
     */
    private function getClassesArray(array $classes)
    {
        $array = array();
        foreach ($classes as $class_full => $class_short) {
            foreach ($this->collect as $type) {
                if (preg_match('/' . $type . '/', $class_short)) {
                    $tmp_name = str_replace('_' . $type . '_', '/', $class_short);
                    $array[$type][strtolower($tmp_name)] = $class_full;
                }
            }
        }
        return $array;
    }

    /**
     *
     * Получаем названия классов по полному пути к php файлу
     *
     * @param $files
     * @return array
     */
    private function get_php_classes($files)
    {
        $classes = array();
        foreach ($files as $file) {
            $php_code = file_get_contents($file);
            $tokens = token_get_all($php_code);
            $count = count($tokens);
            for ($i = 2; $i < $count; $i++) {
                if ($tokens[$i - 2][0] == T_CLASS
                    && $tokens[$i - 1][0] == T_WHITESPACE
                    && $tokens[$i][0] == T_STRING
                ) {

                    $class_name = $tokens[$i][1];
                    $classes[] = $class_name;
                }
            }


        }
        return $classes;
    }

    /**
     *
     * Получаем все вложенные папки рекурсивно
     *
     * @param $folder
     * @return bool
     */
    private function getFoldersRecursive($folder)
    {
        $res = glob($folder . '/*', GLOB_ONLYDIR);
        if (empty($res)) return false;
        foreach ($res as $_folder) {
            foreach ($this->collect as $item) {
                if (preg_match('/' . $item . '/', $_folder)) {
                    $this->folders[] = $_folder;
                }
            }
            $this->getFoldersRecursive($_folder);
        }
    }

    /**
     *
     * Получаем все php файлы из папок
     *
     * @return array
     */
    private function getClasses()
    {
        $result = array();
        foreach ($this->folders as $item) {
            $result = array_merge($result, glob($item . '/*.php'));
        }

        return $result;
    }
}