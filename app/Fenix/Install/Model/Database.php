<?php

class Fenix_Install_Model_Database extends Fenix_Resource_Model
{
    /**
     *
     * Обработка XML
     *
     * @param $start - с какой XML начинаем обрабатывать
     * @param int $step_qty - по скок обрабатываем за раз
     * @return object
     */
    public function prepareTables($start, $step_qty = 1)
    {
        $xmls = $this->collectAttributesXmls();
        $xml_formatted = $this->formatXmlList($xmls);
        for ($i = 0; $i < $step_qty; $i++) {
            $index = $i + $start;
            if ( ! isset($xml_formatted[$index])) {
                break;
            }
            try {
            $Engine = new Fenix_Engine_Database();
            $Engine->setDatabaseTemplate($xml_formatted[$index])
                ->prepare()
                ->execute();
            } catch (\Exception $e) {
                $Engine = new Fenix_Engine_Database();
                $Engine->setDatabaseTemplate($xml_formatted[$index])
                    ->prepare()
                    ->execute();
            }
        }

        $table = 'Finish!';
        if (isset($xml_formatted[$index])) {
            $table = $xml_formatted[$index];
        }

        $current = $index + 1 > count($xml_formatted) ? count($xml_formatted) : $index + 1;

        return (object)array('count' => count($xml_formatted), 'current' => $current, 'table' => $table);
    }

    public function getXmlsCount()
    {
        $xmls = $this->collectAttributesXmls();

        return count($this->formatXmlList($xmls));
    }

    /**
     *
     * Получаем список XML'ек с параметрами таблиц
     *
     * @return array
     */
    private function collectAttributesXmls()
    {
        $modules = glob(APP_DIR_ABSOLUTE . 'Fenix' . DS . '*', GLOB_ONLYDIR);

        //колбасим папки Local и Fenix
        $xmls = array();
        foreach ($modules as $module) {
            $xmls = array_merge($xmls, glob($module . DS . 'etc' . DS . 'attributes' . DS . '*.xml'));
        }

        return $xmls;
    }

    /**
     *
     * форматируем под такой формат setDatabaseTemplate('catalog/products')
     * берем только те у которых есть инфа о БД
     *
     * @param $xml
     * @return array $data
     */
    private function formatXmlList($xml)
    {
        $data = array();
        foreach ($xml as $key => $_xml) {
            $_xml_obj = new Zend_Config_Xml($_xml, null, array(
                'allowModifications' => true
            ));

            if (isset($_xml_obj->fields) && isset($_xml_obj->table)) {
                //тут будем екзекутить табличку
                $tmp = str_replace(APP_DIR_ABSOLUTE, '', $_xml);
                $tmp = str_replace('Fenix' . DS, '', $tmp);
                $tmp = str_replace('Local' . DS, '', $tmp);
                $tmp = str_replace('etc' . DS . 'attributes' . DS, '', $tmp);
                $tmp = str_replace('.xml', '', $tmp);
                $tmp = str_replace(DS, '/', $tmp);
                $tmp = strtolower($tmp);
                $data[] = $tmp;
            }
        }
        $data = array_unique($data);

        return $data;
    }
}