<?php
class Fenix_Sitemap_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Карта сайта"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang('Карта сайта'))
                           ->fetch();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'         => Fenix::getUrl('sitemap'),
            'title'       => Fenix::lang('Карта сайта'),
        ));

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Creator->getView()->render('layout/page/sitemap.phtml')
        ));
    }
}