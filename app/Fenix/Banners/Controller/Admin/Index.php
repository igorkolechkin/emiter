<?php

class Fenix_Banners_Controller_Admin_Index extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('bannersAll');

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('banners/banners')
            ->prepare()
            ->execute();
    }

    public function indexAction()
    {
        $bannersList = Fenix::getModel('banners/backend_banners')->getBannersListAsSelect();

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Баннера"),
                'uri'   => Fenix::getUrl('banners'),
                'id'    => 'mail'
            )
        ));

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                ->setValue(Fenix::lang("Новый баннер"))
                ->setType('button')
                ->appendClass('btn-primary')
                ->setOnclick('self.location.href=\'' . Fenix::getUrl('banners/add') . '\'')
                ->fetch()
        ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Баннера"))
            ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('banners')
            ->setTitle(Fenix::lang("Баннера"))
            ->setData($bannersList)
            ->setStandartButtonset();

        $Table->setCellCallback('image', function ($value, $data, $column, $table) {
            if ($data->image == null) {
                return;
            }

            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 100, 100);

            return '<img src="' . $url . '" width="50" alt="" />';
        });


        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('banners/edit/id/{$data->id}')
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('banners/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Баннера"));

        // Рендер страницы
        $Creator->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('banners/banners')
        ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Баннера"),
                'uri'   => Fenix::getUrl('banners'),
                'id'    => 'banners'
            ),
            array(
                'label' => Fenix::lang("Новый баннер"),
                'uri'   => Fenix::getUrl('banners/add'),
                'id'    => 'add'
            )
        ));

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        // Источник
        $Form->setSource('banners/banners', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->addRecord(
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Баннер создан"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('banners');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('banners/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('banners/edit/id/' . $id);
            }

            Fenix::redirect('banners');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новый баннер"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentBanner = Fenix::getModel('banners/backend_banners')->getBannerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentBanner == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Баннер не найден"))
                ->saveSession();

            Fenix::redirect('banners');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Баннера"),
                'uri'   => Fenix::getUrl('banners'),
                'id'    => 'banners'
            ),
            array(
                'label' => Fenix::lang("Редактировать баннер"),
                'uri'   => Fenix::getUrl('banners/edit/id/' . $currentBanner->id),
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentBanner->toArray())
            ->setData('current', $currentBanner);

        // Источник
        $Form->setSource('banners/banners', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->editRecord($currentBanner, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Баннер отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('banners');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('banners/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('banners/edit/id/' . $id);
            }

            Fenix::redirect('banners');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать баннер"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $currentBanner = Fenix::getModel('banners/backend_banners')->getBannerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentBanner == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Баннер не найден"))
                ->saveSession();

            Fenix::redirect('banners');
        }

        $Creator = Fenix::getCreatorUI();

        $Creator->loadPlugin('Form_Generator')
            ->setSource('banners/banners', 'default')
            ->deleteRecord($currentBanner);

        $Creator->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Баннер удалён"))
            ->saveSession();

        Fenix::redirect('banners');
    }
}