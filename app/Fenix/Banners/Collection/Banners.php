<?php
class Fenix_Banners_Collection_Banners extends Fenix_Resource_Collection
{
    private $_banner = null;

    public function load($name)
    {
        $_data = Fenix::getModel('banners/backend_banners')->getBannerByName($name);

        return new Fenix_Object(array(
            'data'     => $_data
        ));
    }
}