<?php
class Fenix_Banners_Model_Backend_Banners extends Fenix_Resource_Model
{
    /**
     * Баннер по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getBannerById($id)
    {
        $this->setTable('banners');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Баннер по имени
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getBannerByName($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('banners/banners');

        $this->setTable('banners');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine->getColumns());
        $Select ->where('name = ?', (string) $name);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список баннеров
     *
     * @return Zend_Db_Select
     */
    public function getBannersListAsSelect()
    {
        $this->setTable('banners');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'b' => $this->_name
        ));

        return $Select;
    }

    public function getBannersList(){

        return $this->fetchAll($this->getBannersListAsSelect());
    }
}