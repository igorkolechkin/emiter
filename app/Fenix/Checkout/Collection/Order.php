<?php

class Fenix_Checkout_Collection_Order extends Fenix_Resource_Collection
{
    public function getOrderInfo($orderId = null)
    {
        if ($orderId == null) {
            $orderId = Fenix::getModel('checkout/process')->getSessionId();
        }

        $order = Fenix::getModel('checkout/process')->getOrderById(
            $orderId
        );
        if ($order == null) {
            return false;
        }

        if ($order->total_qty == null) {
            return false;
        }

        $order = $order->toArray();

        $order['delivery'] = $this->_getDelivery($order);
        $order['pay_variants_formatted'] = $this->_getPayVariantFormatted($order);
        $order['products'] = $this->getProducts($order['id']);

        return new Fenix_Object(array(
            'data' => $order
        ));
    }

    /**
     * Собираем данные о доставке заказа
     *
     * @param array $data
     * @return object
     */
    private function _getDelivery($data)
    {
        switch ($data['delivery_type']) {
            case 'novaja_pochta_address':
                $delivery = '&laquo;Новая почта&raquo; - адресная доставка';
                break;

            case 'novaja_pochta_sklad':
                $delivery = '&laquo;Новая почта&raquo; - доставка на склад';
                break;

            case 'pickup':
                $delivery = 'Самовывоз';
                break;

            case 'ukrpochta_address':
                $delivery = '&laquo;Укрпочта&raquo; - адресная доставка';
                break;

            case 'ukrpochta_otdelenie':
                $delivery = '&laquo;Укрпочта&raquo; - доставка в отделение';
                break;

            case 'courier':
                $delivery = 'Доставка курьером';
                break;

            default:
                $delivery = null;
        }

        return (object) array(
            'type'           => $data['delivery_type'],
            'type_formatted' => $delivery,
            'address'        => $data['delivery_address'],
            'city'           => $data['delivery_type'] != 'pickup' ? $data['delivery_city'] : '',
            'store'          => $data['delivery_store'],
        );
    }

    private function _getPayVariantFormatted($data)
    {
        switch ($data['pay_variants']) {
            case 'cash':
                $pay_variant = 'Наличными при получении';
                break;
            case 'card':

            case 'courier_payment':
                $pay_variant = 'Оплата курьеру';
                break;
            case 'exchange':
                $pay_variant = 'Безналичный расчет';
                break;
            case 'delivery-cash':
                $pay_variant = 'Наложенным платежом';
                break;
            case 'liqpay':
                $pay_variant = 'Предоплата через LiqPay';
                break;
            case 'privat24':
                $pay_variant = 'Предоплата через Приват24';
                break;
            default:
                $pay_variant = $data['pay_variants'];
        }

        return $pay_variant;
    }

    public function getProducts($orderId = null, $parent = 0)
    {
        if ($orderId == null) {
            $orderId = Fenix::getModel('checkout/process')->getSessionId();
        }

        $products = Fenix::getModel('checkout/process')->getOrderProducts(
            $orderId,
            $parent
        );

        $data = array();
        foreach ($products AS $_product) {
            $_data = $_product->toArray();
            $data[] = Fenix::getCollection('catalog/products_product')->setProduct($_data);
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }

    public function calculateTotals($order)
    {

        $orderInfo = new Fenix_Object(array(
            'data' => $order
        ));
        $total = 0;
        $discount = 0;
        $totalSum = $orderInfo->getTotalSum();
        $deliveryMax = Fenix::getConfig('checkout_order_delivery_max');
        $deliveryMin = Fenix::getConfig('checkout_order_delivery_min');
        $deliveryLimit = Fenix::getConfig('checkout_order_delivery_limit');
        $discountCard = Fenix::getModel('catalog/backend_discount')->getDiscountCardById($orderInfo->getDiscountCard());
        $discountByPrice = Fenix::getModel('checkout/order')->getOrderDiscountByPrice($orderInfo);
        $totalPriceNoDiscount = 0;
        $totalPriceHasDiscount = 0;
        $totalPriceOld = 0;
        $totalPrice = 0;
        $totalDiscount = 0;
        foreach ($orderInfo->getProducts() as $_product) {
            $productDiscount = $_product->getCpDiscount();
            if ($productDiscount > 0) //Товар со своей скидкой
            {
                $_currentPrice = ($_product->getOrderUnitPrice() - round($_product->getOrderUnitPrice() * $productDiscount / 100)) * $_product->getOrderQty();
                $totalPriceHasDiscount += $_currentPrice;
                $totalPriceOld += $_currentPrice;
            }
            else { //Товар без скидки
                $_currentPrice = $_product->getOrderUnitPrice() * $_product->getOrderQty();
                $totalPriceNoDiscount += $_currentPrice;
                $totalPriceOld += $_currentPrice;
            }
        }
        $totalDiscount = round($totalPriceNoDiscount * $discountByPrice / 100);
        $totalPrice = $totalPriceHasDiscount + $totalPriceNoDiscount - $totalDiscount;
        if ($orderInfo) {
            $totalPrice += $orderInfo->getDeliveryPrice();
        }
        $total = new Fenix_Object(array(
            'data' => array(
                'total_price_old' => $totalPriceOld,
                'total_discount'  => $totalDiscount,
                'total_price'     => $totalPrice,
                'total_delivery'  => $orderInfo->getDeliveryPrice()
            )
        ));

        return $total;
    }
}