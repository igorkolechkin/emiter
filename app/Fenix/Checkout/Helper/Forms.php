<?php

class Fenix_Checkout_Helper_Forms extends Fenix_Resource_Helper
{

    /**
     *   Форма купить в один клик
     */
    static public function getBuyOneClickForm()
    {
        /* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
        $unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

        $Form = Fenix::getCreatorUI()->loadPlugin('Form');
        $form_id = 'buyOneClickForm_' . $unique_id;
        $Form->containerClass = 'buyOneClickForm-container';
        $Form->unique_id = $unique_id;
        $Form->reset = true;

        $Form->setClass('buyOneClick-form')
            ->setId($form_id)
            ->setAction('/checkout/process/oneclick')
            ->enableAjax();

        $column = array();

        $column[] = $Form->loadPlugin('Row')
            ->setContent("<div class='alert alert-info' role='alert'>" . Fenix::lang('Оставьте заявку и наш менеджер свяжеться с Вами для подтверждения заказа') . "</div>")
            ->fetch();

        // Как к вам обращаться?
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("name")
            ->setId("name")
            ->setValue($Form->getRequest()->getPost('name'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang(''))
            ->setClass('form-control name')
            ->fetch();

        $column[] = $Form->loadPlugin('Row')
//            ->setLabel(Fenix::lang('Как к Вам обращаться?') . '<span class="required"></span>')
            ->setClass('form-group')
            ->setContent('<label>' . Fenix::lang('Как к Вам обращаться?') . '</label>' . $Field)
            ->fetch();

        $Field = $Form->loadPlugin('Form_Text')
            ->setName("cellphone")
            ->setId("cellphone")
            ->setValue($Form->getRequest()->getPost('cellphone'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setClass('form-control cellphone')
            ->fetch();

        $column[] = $Form->loadPlugin('Row')
//            ->setLabel(Fenix::lang('Телефон') . ':<span class="required"></span>')
            ->setClass('form-group')
            ->setContent('<label>' . Fenix::lang('Телефон') . '</label>' . $Field . '<small class="form-text text-muted">' . Fenix::lang('Для подтверждения заказа и уточнения деталей') . '</small>')
            ->fetch();

        /** ID товара */
        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("pid")
            ->setType("hidden")
            ->setId("pid")
            ->setValue($Form->getRequest()->getPost('pid'))
            ->setClass('form-control pid')
            ->fetch();

        /** Название товара */
        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("product")
            ->setType("hidden")
            ->setId("product")
            ->setValue($Form->getRequest()->getPost('product'))
            ->setClass('form-control product')
            ->fetch();

        /** Количество товара */
        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("qty")
            ->setType("hidden")
            ->setId("qty")
            ->setValue($Form->getRequest()->getPost('qty'))
            ->setClass('form-control qty')
            ->fetch();

        /** Ссылка на страницу товара */
        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("product_url")
            ->setType("hidden")
            ->setId("product_url")
            ->setValue($Form->getRequest()->getPost('product_url'))
            ->setClass('form-control product_url')
            ->fetch();

        $Field = $Form->loadPlugin('Button')
            ->setType("submit")
            ->setClass('btn btn-primary')
            ->setValue(Fenix::lang("Купить в 1 клик"))
            ->fetch();

        $column[] = $Form->loadPlugin('Row')
            ->setClass('text-center')
            ->setContent($Field)
            ->fetch();

        $Form->setContent($column);
        $Form->compile();

        return $Form;
    }
}