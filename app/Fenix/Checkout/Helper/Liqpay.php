<?php
class Fenix_Checkout_Helper_Liqpay extends Fenix_Resource_Helper
{
    public function getLiqpayForm($orderInfo){

        $orderText = 'Оплата заказа №' . $orderInfo->getId();
        $liqpay = new LiqPay(Fenix::getConfig('general/liqpay/public'),Fenix::getConfig('general/liqpay/privat'));

        $merchant_id = Fenix::getConfig('liqpayKey');
        $url         = "https://www.liqpay.com/api/pay";
        $sum         = $orderInfo->getTotalPrice();

        //Fenix::dump($orderInfo->getTotalPrice());

        $result_url  = Fenix::getUrl('checkout/liqpay/return/order/' . $orderInfo->getId());
        //$result_url  = Fenix::getUrl('checkout/ok/order/' . $orderId);
        $server_url  = Fenix::getUrl('checkout/liqpay/server/order/' . $orderInfo->getId());


        $Form = $liqpay->cnb_form(array(
            'amount'         => $sum,
            'currency'       => 'UAH',
            'description'    => $orderText,
            'order_id'       => $orderInfo->getId(),
            'type'           => 'buy',
            'result_url'     => $result_url,
            'server_url'     => $server_url
        ));
        //Fenix::dump($Form);
        return $Form;
    }
}