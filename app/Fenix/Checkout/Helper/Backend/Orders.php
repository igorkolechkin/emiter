<?php

class Fenix_Checkout_Helper_Backend_Orders extends Fenix_Resource_Model
{
    public function getOrdersTable($ordersList)
    {
        $Table = Fenix::getCreatorUI()->loadPlugin('Table_Db');


        $Table->setTableId('cList');
        $Table->setClass('table table-bordered');
        $Table->setTitle(Fenix::lang("Управление заказами"));
        $Table->setData($ordersList);
        $Table->setStandartButtonset();

        $Table->setRowCallback(function ($table, $data) {
            switch ($data->status) {
                case Fenix_Checkout_Model_Order::STATUS_PENDING:
                    $table->appendStyle('background:#ffc0c0 !important;');
                    break;
                case Fenix_Checkout_Model_Order::STATUS_PROCESS:
                    $table->appendStyle('background:#95a3e7 !important;');
                    break;
                case Fenix_Checkout_Model_Order::STATUS_HOLD:
                    $table->appendStyle('background:#fff4c0 !important;');
                    break;
                case Fenix_Checkout_Model_Order::STATUS_CANCELED:
                    $table->appendStyle('background:#bbbbbb !important;');
                    break;
                case Fenix_Checkout_Model_Order::STATUS_FINISHED:
                    $table->appendStyle('background:#96c787 !important;');
                    break;
            }
        });

        // Id
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('id')
                ->setTitle(Fenix::lang("ID"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('id')
                ->setSqlCellNameAs('id')
                ->setHeaderAttributes(array(
                    'width' => '50'
                ))
        );

        // Дата
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('create_date')
                ->setTitle(Fenix::lang("Дата создания"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'calendar'
                ))
                ->setSqlCellName('create_date')
                ->setSqlCellNameAs('co.create_date')
                ->setHeaderAttributes(array(
                    'width' => '150'
                ))
                ->setCellCallback(function ($value) {
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                })
        );

        // Клиент
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('name')
                ->setTitle(Fenix::lang("Клиент"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('name')
                ->setSqlCellNameAs('co.name')
        );

        // Телефон
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('cellphone')
                ->setTitle(Fenix::lang("Телефон"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('cellphone')
                ->setSqlCellNameAs('co.cellphone')
        );

        // Доставка
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('email')
                ->setTitle(Fenix::lang("Доставка"))
                ->setSortable(true)
                /*->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))*/
                ->setSqlCellName('delivery_type')
                ->setSqlCellNameAs('co.delivery_type')
                ->setCellCallback(function ($value, $order) {
                    //$store = Fenix::getModel('catalog/stores')->getStoreById($order->store_id);

                    $method = lcfirst(str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $value))));

                    if (method_exists($this, $method)) {
                        $result = $this->$method($order);

                        return implode('<br>', $result);
                    } else {
                        return Fenix::lang('Покупка в один клик');
                    }

//                    $result = '';
//                    switch ($value) {
//                        case 'newmail':
//                            $result .= 'Город: ' . $order->delivery_city . '</br>';
//                            $result .= 'Склад: ' . $order->delivery_warehouse . '</br>';
//                            break;
//                        case 'pickup':
//
//                            $result .= 'Город: ' . $order->delivery_city . '</br>';
//                            $result .= 'Склад: ' . $order->delivery_address . '</br>';
//                            /*if ($store)
//                                $result .= ' Магазин:' . $store->title_russian . ', ' . $store->city_russian . ', ' . $store->address_russian . '</br>';
//                            else
//                                $result .= 'Магазин: не был выбран клиентом' . '</br>';*/
//                            break;
//                        case 'address':
//
//                            $result .= 'Адресная доставка</br>';
//                            $result .= 'Город: ' . $order->delivery_city . '</br>';
//                            $result .= 'Адрес: ' . $order->delivery_address . '</br>';
//                            /*if ($store)
//                                $result .= ' Магазин:' . $store->title_russian . ', ' . $store->city_russian . ', ' . $store->address_russian . '</br>';
//                            else
//                                $result .= 'Магазин: не был выбран клиентом' . '</br>';*/
//                            break;
//                        default:
//                            $result .= 'Покупка в один клик</br>';
//                        /*if ($store)
//                            $result .= ' Магазин:' . $store->title_russian . ', ' . $store->city_russian . ', ' . $store->address_russian . '</br>';
//                        else
//                            $result .= 'Магазин: не был выбран клиентом' . '</br>';*/
//
//                    }

                })
        );

        // Email
        /*$Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('email')
                ->setTitle(Fenix::lang("Email"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('email')
                ->setSqlCellNameAs('co.email')
        );*/

        // Всего товаров
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('total_qty')
                ->setTitle(Fenix::lang("Товаров"))
                ->setSqlCellName('total_qty')
        );

        // Сумма
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('total_price')
                ->setTitle(Fenix::lang("Товаров на сумму"))
                ->setSqlCellName('total_price')
                ->setCellCallback(function ($value) {
                    return Fenix::getHelper('catalog/decorator')->decoratePrice(round($value, 2));
                })
        );
        // Использовано бонусов
        /*  $Table ->addColumn(
              $Table->loadPlugin('Table_Db_Column')
                  ->setId('used_bonus')
                  ->setTitle(Fenix::lang("Бонусы"))
                  ->setSqlCellName('used_bonus')
                  ->setCellCallback(function($value) {
                      return Fenix::getHelper('catalog/decorator')->decoratePrice(ceil($value));
                  })
          );*/
        // Итого
        /* $Table ->addColumn(
             $Table->loadPlugin('Table_Db_Column')
                 ->setId('total_sum_with_bonus')
                 ->setTitle(Fenix::lang("Итого"))
                 ->setSqlCellName('total_sum_with_bonus')
                 ->setCellCallback(function($value) {
                     return Fenix::getHelper('catalog/decorator')->decoratePrice(ceil($value));
                 })
         );*/
        // Коментарий
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('comment')
                ->setTitle(Fenix::lang("Коментарий"))
                ->setSqlCellName('comment')
        );
        // Статус
        $Table->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('status')
                ->setTitle(Fenix::lang("Статус"))
                ->setSqlCellName('status')
                ->setCellCallback(function ($value) {
                    return Fenix::getModel('checkout/order')->decorateStatus($value);
                })
        );

        // Акшаны
        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'info-sign',
            'title' => Fenix::lang("Просмотр"),
            'url'   => Fenix::getUrl('checkout/view/id/{$data->id}')
        ));

        /*
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('checkout/delete/id/{$data->id}')
        ));*/

        return $Table->fetch();
    }

    private function ukrpochtaAddress($order)
    {
        $result[] = '&laquo;Укрпочта&raquo; - адресная доставка';
        $result[] = 'Город: ' . $order->delivery_city;
        $result[] = 'Адрес: ' . $order->delivery_address;

        return $result;
    }

    private function ukrpochtaOtdelenie($order)
    {
        $result[] = '&laquo;Укрпочта&raquo; - доставка в отделение';
        $result[] = 'Город: ' . $order->delivery_city;
        $result[] = 'Отделение: ' . $order->delivery_address;

        return $result;
    }

    private function pickup($order)
    {
        $result[] = 'Самовывоз';
        $result[] = 'Адрес: ' . $order->delivery_address;

        return $result;
    }

    private function courier($order)
    {
        $result[] = 'Доставка курьером';
        $result[] = 'Адрес: ' . $order->delivery_address;

        return $result;
    }

    private function novajaPochtaSklad($order)
    {
        $result[] = '&laquo;Новая почта&raquo; - доставка на склад';
        $result[] = 'Город: ' . $order->delivery_city;
        $result[] = 'Склад: ' . $order->delivery_address;

        return $result;
    }

    private function novajaPochtaAddress($order)
    {
        $result[] = '&laquo;Новая почта&raquo; - адресная доставка';
        $result[] = 'Город: ' . $order->delivery_city;
        $result[] = 'Адрес: ' . $order->delivery_address;

        return $result;
    }
}