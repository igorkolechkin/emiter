<?php
class Fenix_Checkout_Model_Liqpay extends Fenix_Resource_Model
{
    public function liqpayRespond($orderId)
    {
        $req   = Fenix::getRequest();
        $liqpay      = Fenix::getConfig('general/liqpay/public');
        $private_key = Fenix::getConfig('general/liqpay/private');

        $sign = base64_encode( sha1(
            $private_key .
            $req->getPost('amount') .
            $req->getPost('currency') .
            $req->getPost('public_key') .
            $req->getPost('order_id') .
            $req->getPost('type') .
            $req->getPost('description') .
            $req->getPost('status') .
            $req->getPost('transaction_id') .
            $req->getPost('sender_phone')
            , 1 ));

        $check = ($req->getPost('signature') == $sign ? true : false);

        $data = array(
            'epay_status' => $req->getPost('status'),
            'epay_check'  => ($check ? 1 : 0),
            'epay_answer' => serialize($req->getPost())
        );

        $this->setTable('checkout_orders')
             ->update(
             $data,
                 $this->getAdapter()->quoteInto('id = ?', (int) $orderId)
            );

        /*if($check == true && $req->getPost('status')=='success' )
            self::sendLiqpayEmail($req);*/

        return $orderId;
    }
}