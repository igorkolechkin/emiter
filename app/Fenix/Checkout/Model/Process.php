<?php
class Fenix_Checkout_Model_Process extends Fenix_Resource_Model
{
    public function getOrderById($orderId)
    {
        $this->setTable('checkout_orders');
        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'co' => $this->_name
        ), array(
            '*',
            new Zend_Db_Expr('(SELECT SUM(cp.qty) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id) AS total_qty')
        ));

        $Select ->joinLeft(array(
            '_c' => $this->getTable('catalog_coupons')
        ), '_c.id = co.coupon_id', array(
            '_c.coupon AS discount_coupon',
            '_c.discount AS discount',
        ));

        $Select ->where('co.id = ?', (int) $orderId);

        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        if (Fenix::isDev()){
//            Fenix::dump($Select->assemble(),$Result);
        }


        return $Result;
    }

    public function updateOrderTotal($orderId){
        $this->setTable('checkout_orders');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        /**
         * Считаем суммы по товаром
         */

        $Select ->from(array(
            'co' => $this->_name
        ), array(
            new Zend_Db_Expr('(SELECT ROUND(SUM(cp.base_price * cp.qty),2) FROM '  . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id AND cp.parent_product = 0) AS total_base_price'),
            new Zend_Db_Expr('(SELECT ROUND(SUM(cp.price * cp.qty),2) FROM '  . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id AND cp.parent_product = 0) AS total_price'),
            new Zend_Db_Expr('(SELECT ROUND(SUM(cp.price * cp.qty),2) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id AND cp.parent_product = 0 AND cp.discount = 0) AS total_nodiscount_price'),
            new Zend_Db_Expr('(SELECT ROUND(SUM(cp.price * cp.qty),2) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id AND cp.parent_product = 0 AND cp.discount > 0) AS total_discount_price'),
        ));
        $Select ->where('co.id = ?', (int) $orderId);
        $Select->limit(1);

        $totals = $this->fetchRow($Select);

        if($totals)
            $data = array(
                'total_base_price'       => $totals->total_base_price,
                'total_price'            => $totals->total_price,
                'total_discount_price'   => $totals->total_discount_price,
                'total_nodiscount_price' => $totals->total_nodiscount_price
            );
        else
            $data = array(
                'total_base_price'       => 0,
                'total_price'            => 0,
                'total_discount_price'   => 0,
                'total_nodiscount_price' => 0
            );
        $this->setTable('checkout_orders')
             ->update($data, $this->getAdapter()->quoteInto('id = ?', $orderId));


        //Обновляем расчеты по скидкам
        Fenix::getModel('checkout/order')->updateActiveSales((int)$orderId);

        /**
         * Считаем стоимость доставки
         */
        /*
        $order = Fenix::getCollection('checkout/order')->getOrderInfo((int)$orderId);
        if ($order) {
            $deliveryMax   = Fenix::getConfig('checkout_order_delivery_max');
            $deliveryMin   = Fenix::getConfig('checkout_order_delivery_min');
            $deliveryLimit = Fenix::getConfig('checkout_order_delivery_limit');
            if (@$totals->total_order_price < $deliveryLimit) {
                $delivery = $deliveryMax;
            } else {
                $delivery = $deliveryMin;
            }
            //Fenix::dump($delivery, $order);
            if ($order->getDeliveryType() == 'pickup' || $order->getDeliveryType() == null) {
                $delivery = 0;
            }
            $data = array(
                'delivery_price' => $delivery
            );
            $this->setTable('checkout_orders')
                 ->update($data, $this->getAdapter()->quoteInto('id = ?', $orderId));
        }*/



    }
    /**
     * Обновление товаров в корзине
     *
     * @param Fenix_Controller_Request_Http $req
     */
    public function updateProducts(Fenix_Controller_Request_Http $req)
    {
        $result = '';
        $this->setTable('checkout_products');
        foreach ((array) $req->getPost('data') AS $_i => $_data) {
            $last_product_data = $_data;
            //if ($_data['qty'] > 0) $_data['qty'] = 1; //Fix: Только один товар в корзине
            if ((int)$_data['qty'] > 0 && (int)$_data['order_unit_id'] > 0) {
                $this->update(array(
                    'qty'  => (int)$_data['qty']/*,
                    'size' => (string)$_data['size']*/
                ), 'id = ' . (int)$_data['order_unit_id']);
            }
        }

        $orderId = $req->getPost('order_id');
        if($orderId)
        {
            //обновляем вид доставки если задано
            if ($req->getPost('delivery_type')) {
                $deliveryData = array(
                    'delivery_type' => $req->getPost('delivery_type')
                );
                $this->setTable('checkout_orders')
                     ->update($deliveryData, $this->getAdapter()->quoteInto('id = ?' , (int) $orderId));
            }
            //Обновляем итого в заказе
            Fenix::getModel('checkout/process')->updateOrderTotal((int)$orderId);

            $order = Fenix::getCollection('checkout/order')->getOrderInfo((int)$orderId);
            if($order){

                $result = json_encode(array(
                    'total_price'            => (double)$order->getTotalPrice(),
                    'total_order_price'      => (double)$order->getTotalOrderPrice(),
                    'total_discount_price'   => (double)$order->getTotalDiscountPrice(),
                    'total_nodiscount_price' => (double)$order->getTotalNodiscountPrice(),
                    'discount_percent'       => (double)$order->getDiscountPercent(),
                    'discount_amount'        => (double)$order->getDiscountAmount(),
                    'delivery_price'         => (double)$order->getDeliveryPrice()
                ));
            }

        }
        return $result;
    }


    /**
     * Список товаров в заказе
     *
     * @param $orderId
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getOrderProducts($orderId, $parent = 0)
    {

        $this->setTable('checkout_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $cols = array(
            'cp.id AS order_product_id',
            'cp.qty AS cp_qty',
            'cp.type AS cp_type',
            'cp.parent_product AS cp_parent_product',
            'cp.discount AS cp_discount',
            'cp.base_price AS cp_base_price',
            'cp.price AS cp_price',
            'cp.group_sku AS group_sku',
            'cp.params AS cp_params'
        );

        $Select->from(array(
            'cp' => $this->_name
        ), $cols);

        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'cp.product_id = p.id', Fenix::getModel('catalog/products')->productColumns());

        $Select ->where('cp.order_id = ?', (int) $orderId)
                ->where('cp.parent_product = ?', $parent);


        $Select ->order('cp.id asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Добавление товара в корзину
     *
     * @param $_product
     * @param string $type
     * @param array $params
     * @param null $child
     * @internal param int $qty
     * @internal param string $width
     */
    public function addProduct($_product, $type='default', $params = array(), $set = null)
    {

        $orderId = $this->getSessionId();
        if($orderId == null){
            $orderId = $this->createCheckoutId();
        }
        $product = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        $this    ->setTable('checkout_products');
        if(Fenix::getRequest()->getParam('multiset')){
            $type = 'multiset';
        }

        $qty = Fenix::getRequest()->getParam('qty');
        $qty = ($qty != null && (int)$qty > 0) ? (int)$qty : 1;

        /**
         * Комплект
         */
        if('set' == $type){
            /** *******************************************************************************
             *  Добавляем основной товар
             *  *******************************************************************************
             */
            if($set == null) throw new Exception('Комплект не существует');

            $priceInfo = Fenix::getModel('catalog/products')->getPriceInfo($product, $params);

            $basePrice  = Fenix::getModel('catalog/products_set')->getSetPrice($set); // TODO исправить что бы была старая цена
            $orderPrice = Fenix::getModel('catalog/products_set')->getSetPrice($set);

            $data      = array(
                'parent_product'     => 0,
                'order_id'   => $orderId,
                'set_id'     => $set->id,
                'type'       => $type,
                'product_id' => $_product->id,
                'params'     => serialize($params),
                'group_sku'      => $priceInfo->group_sku,
                'discount'   => 0,
                'base_price' => $basePrice,
                'price'      => $orderPrice,
                'qty'        => 1,
                'add_date'   => new Zend_Db_Expr('NOW()')
            );

            $parentId     = $this->insert($data);

            /** *******************************************************************************
             *  Добавляем товары комплекта
             *  *******************************************************************************
             */
            $setProducts = Fenix::getModel('catalog/products_set')->getProductsListBySetId($set->id);
            //Добавляем товары в корзину
            foreach ($setProducts as $i => $_setProduct) {
                //Если это основной товар пропускаем его
                if($_setProduct->id == $product->id) continue;

                $basePrice = Fenix::getModel('catalog/products_set')->getSetProductPriceOld($_setProduct);
                $orderPrice = Fenix::getModel('catalog/products_set')->getSetProductPrice($_setProduct);
                $discount = Fenix::getModel('catalog/products_set')->getSetProductDiscount($_setProduct);

                $data = array(
                    'parent_product' => $parentId,
                    'order_id'   => $orderId,
                    'set_id'     => $set->id,
                    'type'       => $type,
                    'product_id' => $_setProduct->id,
                    'params'     => serialize(array()),
                    'discount'   => $discount,
                    'base_price' => $basePrice,
                    'price'      => $orderPrice,
                    'qty'        => 1,
                    'add_date'   => new Zend_Db_Expr('NOW()')
                );
                //Fenix::dump($unitPrice, $data, $tmpData);
                $this->insert($data);
                //}
            }
        }

        /** *******************************************************************************
         *  Один товар ИЛИ товар-комплект - при кол-ве более 1го все остальные не попадают под комплект
         *  *******************************************************************************
         */
        // TODO проверить с комплектами, пока с той версией в которой это делал  уменя не получилось. Hevlaskis
        if(empty($type) || 'default' == $type || ('set' == $type && $qty > 1)){
            $qty = ('set' == $type) ? $qty - 1 : $qty;

            //Проверка есть ли в корзине
            $testSelect = $this->setTable('checkout_products')
                               ->select()
                               ->from(array(
                                   'cp' => $this->_name
                               ));

            $testSelect->where('set_id = ?', 0);
            $testSelect->where('order_id = ?', (int)$orderId);
            $testSelect->where('product_id = ?', (int)$_product->id);

            if (!isset($params['product_id'])) {
                $params['product_id'] = $_product->id;
            }

            //$params = $this->prepareParams($params);

            $testSelect->where('params = ?', serialize($params));

            $_test = $this->fetchRow($testSelect);



            if ($_test) {
                // Если уже в корзине добавляем количество
                $this->update(array(
                    'qty' => new Zend_Db_Expr('qty + '.$qty)
                ),
                    '('.$this->getAdapter()->quoteInto('set_id = ?', 0).') 
                        AND
                    (' . $this->getAdapter()->quoteInto('id = ?', $_test->id) . ' OR ' .
                         $this->getAdapter()->quoteInto('parent_product = ?', $_test->id) .'
                    )'
                );
            } else {

                //Если нет в корзине добавляем
                $priceInfo = Fenix::getModel('catalog/products')->getPriceInfo($product, $params);
                //$priceInfo = Fenix::getModel('catalog/configurable')->getPriceInfo($params);
                //$product   = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());

                $data = array(
                    'parent_product' => 0,
                    'order_id'       => $orderId,
                    'set_id'         => 0,
                    'type'           => $type,
                    'product_id'     => $_product->id,
                    'params'         => serialize($params),
                    'group_sku'      => $priceInfo->group_sku,
                    'discount'       => 0,
                    'base_price'     => $priceInfo->price_old > 0 ? $priceInfo->price_old : $priceInfo->price,
                    'price'          => $priceInfo->price,
                    'qty'            => $qty,
                    'add_date'       => new Zend_Db_Expr('NOW()')
                );

                $rowId = $this->insert($data);
            }
        }

        //Обновляем итого в заказе
        $this->updateOrderTotal($orderId);
    }
    public function prepareParams($params){

        $lang = Fenix_Language::getInstance();

        if ($params == null) return array();

        foreach($params as $name => $value){
            $attr = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($name);
            if($attr) {
                if ($attr->split_by_lang == '1') {
                    unset($params[$name]);
                    //foreach ($lang->getLanguagesList() as $_lang) {
                    $params[$name . '_' . $lang->getCurrentLanguage()->name] = $value;
                    //}
                }
            }
        }
        return $params;
    }
    public function updateProduct($productId, $params){

        $orderId = $this->getSessionId();

        //Проверка есть ли в корзине
        $testSelect = $this->setTable('checkout_products')
                           ->select()
                           ->from(array(
                               'cp' => $this->_name
                           ));
        $testSelect->where('id = ?', (int)$productId);

        $params          = $this->prepareParams($params);
        $checkoutProduct = $this->fetchRow($testSelect);

        if ($checkoutProduct) {
            $priceInfo = Fenix::getModel('catalog/configurable')->getPriceInfo($params);
//Fenix::dump($params,$priceInfo);
            if (in_array($checkoutProduct->type, array('multiset'))) {
                //Обновляем цену комплекта если товар из комплекта
                /*if (Fenix::isDev()){
                Fenix::dump($checkoutProduct->type,$priceInfo);
                }*/

                if($checkoutProduct->parent_product == '0'){
                    /*
                       switch($checkoutProduct->type){
                        case 'multiset':
                    */
                            $priceInfo         = Fenix::getModel('catalog/configurable')->getPriceInfo($params);
                            $priceInfoCheckout = Fenix::getModel('catalog/configurable')->getPriceInfo(unserialize($checkoutProduct->params));

                            $_setProducts = $this->getOrderProducts($orderId, $checkoutProduct->id);
                            $setProducts = Fenix::getCollection('catalog/products')->setList($_setProducts);

                            $_product   = Fenix::getModel('catalog/products')->getProductbyId($params['product_id']);
                            //Fenix::dump($productId,$params,$_product);
                            $product   = Fenix::getCollection('catalog/products_product')->setProduct(
                                $_product->toArray(), $priceInfo
                            );

                            //Какой комплект добавлен
                            $set = Fenix::getModel('catalog/backend_products')->getMultiSetById($checkoutProduct->set_id);
                            //Товары комплекта
                            $setPrice    = $product->getCheckoutSetPrice($set, $setProducts);
                            $setPriceOld = $product->getSetPriceOld($set, $setProducts);

                            //Обновляем основной товар в мультикомплекте
                            //Fenix::dump($priceInfo->price - $priceInfoCheckout->price,$priceInfo->price - $priceInfoCheckout->price);
                            //$priceDelta = $priceInfo->price - $priceInfoCheckout->price;

                            $data = array(
                                //'parent_product'     => 0,
                                //'order_id'   => $orderId,
                                'discount'   => 0,
                                'base_price' => $setPriceOld,//new Zend_Db_Expr('base_price + (' . $priceDelta . ')'),
                                'price'      => $setPrice,// new Zend_Db_Expr('price + (' . $priceDelta . ')'),
                                //'qty'        => 1,
                                'params'      => serialize($params),
                                'modify_date'   => new Zend_Db_Expr('NOW()')
                            );
                            $data = array_merge($data, $params);

                            $rowCount = $this->update($data,$this->getAdapter()->quoteInto('id = ?', $productId));
                            //break;
                        /*default:
                            //Обновляем основной товар
                            $priceDelta = $priceInfo->price - $checkoutProduct->base_price;

                            $data = array(
                                //'parent_product'     => 0,
                                //'order_id'   => $orderId,
                                'discount'   => $priceInfo->discount,
                                'base_price' => $priceInfo->price,
                                'price'      => new Zend_Db_Expr('price + (' . $priceDelta . ')'),
                                //'qty'        => 1,
                                'add_date'   => new Zend_Db_Expr('NOW()')
                            );
                            $data = array_merge($data, $params);
                            $rowId = $this->update($data,$this->getAdapter()->quoteInto('id = ?', $productId));
                    }*/


                }else{
                    //Fenix::dump($params,$priceInfo);
                    //Обновляем товар комплекта
                    $data = array(
                        //'parent_product'     => 0,
                        //'order_id'   => $orderId,
                        'discount'   => $priceInfo->discount,
                        'base_price' => $priceInfo->base_price,
                        'price'      => $priceInfo->price,
                        'params'      => serialize($params),
                        //'qty'        => 1,
                        'add_date'   => new Zend_Db_Expr('NOW()')
                    );
                    $data = array_merge($data, $params);

                    if (Fenix::isDev()){
                    //Fenix::dump($data);
                    }

                    $rowCount = $this->update($data,$this->getAdapter()->quoteInto('id = ?', $productId));

                    /**
                     * Обновляем цену комплекта
                     */
                    //Основной товар комплекта
                    $testSelect->reset(Zend_Db_Select::WHERE);
                    $testSelect->where('id = ?', $checkoutProduct->parent_product);
                    $checkoutParentProduct = $this->fetchRow($testSelect);


                    //Товары комплекта
                    $_setProducts = $this->getOrderProducts($orderId, $checkoutParentProduct->id);
                    $setProducts = Fenix::getCollection('catalog/products')->setList($_setProducts);

                    //Загружаем основной товар комплекта
                    if (Fenix::isDev()){
                    //Fenix::dump(unserialize($checkoutParentProduct->params));
                    }

                    $priceInfoCheckout = Fenix::getModel('catalog/configurable')->getPriceInfo(unserialize($checkoutParentProduct->params));
                    $_product   = Fenix::getModel('catalog/products')->getProductbyId($checkoutParentProduct->product_id);
                    $product   = Fenix::getCollection('catalog/products_product')->setProduct(
                        $_product->toArray(), $priceInfoCheckout
                    );

                    //Какой комплект добавлен
                    $set = Fenix::getModel('catalog/backend_products')->getMultiSetById($checkoutParentProduct->set_id);
                    //Товары комплекта


                    $setPrice    = $product->getCheckoutSetPrice($set, $setProducts);
                    $setPriceOld = $product->getSetPriceOld($set, $setProducts);

                    //Обновляем основной товар в мультикомплекте
                    //Fenix::dump($priceInfo->price - $priceInfoCheckout->price,$priceInfo->price - $priceInfoCheckout->price);
                    //$priceDelta = $priceInfo->price - $priceInfoCheckout->price;

                    $data = array(
                        //'parent_product'     => 0,
                        //'order_id'   => $orderId,
                        'discount'   => 0,
                        'base_price' => $setPriceOld,//new Zend_Db_Expr('base_price + (' . $priceDelta . ')'),
                        'price'      => $setPrice,// new Zend_Db_Expr('price + (' . $priceDelta . ')'),
                        //'qty'        => 1,
                        'modify_date'   => new Zend_Db_Expr('NOW()')
                    );

                    $rowCount = $this->update($data,$this->getAdapter()->quoteInto('id = ?', $checkoutParentProduct->id));
                    /*
                    $priceDelta = $priceInfo->price - $checkoutProduct->price;
                    $data = array(
                        'price' => new Zend_Db_Expr('price + (' . $priceDelta.')')
                    );
                    $this->update($data, $this->getAdapter()->quoteInto('id = ?', $checkoutProduct->parent_product));
                    */
                    if (Fenix::isDev()){
                        //Fenix::dump($setPrice, $setPriceOld);
                    }
                }
            }else{
                //Простой товар

                $data = array(
                    //'parent_product'     => 0,
                    //'order_id'   => $orderId,
                    'discount'   => $priceInfo->discount,
                    'base_price' => $priceInfo->base_price,
                    'price'      => $priceInfo->price,
                    'params'     => serialize($params),
                    //'qty'        => 1,
                    'add_date'   => new Zend_Db_Expr('NOW()')
                );
                $data = array_merge($data, $params);

                $rowId = $this->update($data,$this->getAdapter()->quoteInto('id = ?', $productId));
            }

            $this->updateOrderTotal($orderId);
        } else {

        }

    }
    public function updateProductQty($req){

        $orderId = $this->getSessionId();

        $productId = $req->getPost('product_id');
        $data = array(
            'qty' => $req->getPost('qty')
        );
        $this->setTable('checkout_products');
        $product = $this->fetchRow($this->getAdapter()->quoteInto('id = ?', $productId));

        if($product)
        {
            $res = $this->update($data,
                $this->getAdapter()->quoteInto('id = ?', $productId) .' OR ' .
                $this->getAdapter()->quoteInto('parent_product = ?', $productId)
            );
        }
        $this->updateOrderTotal($orderId);

    }

    public function removeProduct($req)
    {
        $orderId = $this->getSessionId();
        $deletedProductId = (int) $req->getPost('id');

        $this->setTable('checkout_products');
        $deletedProduct = $this->fetchRow(
            $this->getAdapter()->quoteInto('order_id = ?', $orderId) . ' AND ' .
            $this->getAdapter()->quoteInto('id = ?', $deletedProductId)
        );
        if($deletedProduct){
            $parentId = (int) $deletedProduct->parent_product;
            //Удаляем из корзины
            $this->delete(
                $this->getAdapter()->quoteInto('order_id = ?', $orderId) . ' AND ' .
                $this->getAdapter()->quoteInto('id = ?', $deletedProductId)
            );
            //Если у товара не указан родитель и это комплект, добавляем товары комплекта без родительского
            if ($parentId == 0 && $deletedProduct->type == 'set'){
                //Проверяем что в комплекте еще есть товары
                $setProducts = $this->getOrderProducts($orderId, $deletedProductId);
                foreach($setProducts as $_setProduct){
                    //Получаем данные о родительском товаре
                    $product         = Fenix::getModel('catalog/products')->getProductById($_setProduct->id);
                    //Удаляем родительский

                    $this->setTable('checkout_products')
                        ->delete(
                        $this->getAdapter()->quoteInto('order_id = ?', $orderId) . ' AND ' .
                        $this->getAdapter()->quoteInto('id = ?', $_setProduct->order_product_id)
                    );
                    //и добавляем его без комплекта
                    $this->addProduct($product, 'default', unserialize($_setProduct->cp_params));
                }
            }
            //Если у товара указан родитель и это комплект, пересчитываем цену комплекта
            elseif ($parentId > 0 && $deletedProduct->type == 'set'){
                $data = array(
                    'price'      => new Zend_Db_Expr('price - ' . $deletedProduct->price),
                    'base_price' => new Zend_Db_Expr('base_price - ' . $deletedProduct->base_price)
                );
                $this->update($data,
                    $this->getAdapter()->quoteInto('order_id = ?', $orderId) . ' AND ' .
                    $this->getAdapter()->quoteInto('id = ?', $parentId)
                    );
                //Проверяем что в комплекте еще есть товары
                $setProducts = $this->getOrderProducts($orderId, $parentId);
                if ($setProducts->count() == 0) {
                    //Все товары удалены
                    //Получаем данные о родительском товаре
                    $checkoutParentProduct = $this->fetchRow($this->getAdapter()->quoteInto('id = ?', $parentId));
                    $parentProduct         = Fenix::getModel('catalog/products')->getProductById($checkoutParentProduct->product_id);
                    //Удаляем родительский
                    $this->delete(
                        $this->getAdapter()->quoteInto('order_id = ?', $orderId) . ' AND ' .
                        $this->getAdapter()->quoteInto('id = ?', $parentId)
                    );
                    //и добавляем его без комплекта
                    $this->addProduct($parentProduct, 'default', unserialize($checkoutParentProduct->params));
                }
            }
        }



        //Обновляем итого в заказе
        $orderId = $this->getSessionId();

        self::updateOrderTotal($orderId);

    }

    public function oneClickBuy($req)
    {
        $productId = $this->getRequest()->getParam('pid');
        $product   = Fenix::getModel('catalog/products')->getProductById($productId);
        $qty = ($this->getRequest()->getParam('qty') != null) ? $this->getRequest()->getParam('qty') : 1;
        $orderId = $this->getSessionId();
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);

        if ($product == null)
            return;

        $User    = Fenix::getModel('session/auth')->getUser();
        $customerId = 0;
        if ($User instanceof Zend_Db_Table_Row) {
            $customerId = $User->id;
        }

        if(!$this->getRequest()->getParam('name')) {
            $this->getRequest()->setParam('name', Fenix::lang('Заказ без оформления'));
        }


        $unitPrice = Fenix::getModel('catalog/products')->getProductPrice($product);
        //Если есть скидка занижаем цену в заказе
        if ($product->discount > 0)
            $orderPrice = $unitPrice - round(($unitPrice / 100 * $product->discount), 0);
        else
            $orderPrice = $unitPrice;

        $this->setTable('checkout_orders');
        $orderId = $this->insert(array(
            'abandoned'        => '0',
            'customer_id'      => $customerId,
            'status'           => 1,
            'create_date'      => new Zend_Db_Expr('NOW()'),
            'cellphone'        => $this->getRequest()->getParam('cellphone'),
            'name'             => $this->getRequest()->getParam('name'),
            'comment'          => Fenix::lang("В один клик"),
            'total_base_price' => $unitPrice,
            'total_price'      => $orderPrice,
        ));

        $this->setTable('checkout_products');
        $data = array(
            'parent_product' => 0,
            'order_id'       => $orderId,
            'type'           => '',
            'product_id'     => $product->id,
            'base_price'     => $unitPrice,
            'price'          => $orderPrice,
            'qty'            => $qty,
            'add_date'       => new Zend_Db_Expr('NOW()'),
        );
        $this->insert($data);

        //Обновляем итого в заказе
        self::updateOrderTotal($orderId);

        //Отправляем сообщение на почту
        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        Fenix::getModel('checkout/order')->sendNotify($order);
    }


    /**
     * Создание сессии для заказа
     *
     * @return int
     */
    public function getSessionId()
    {
        $this->setTable('checkout_orders');

        $cookieId = (array_key_exists('FenixCheckoutId', $_COOKIE) ? $_COOKIE['FenixCheckoutId'] : null);

        $_test = $this->fetchRow('id = ' . (int) $cookieId);

        if($_test)
            return $cookieId;
        else
            return false;
    }

    /**
     * Создает новый заказ в бд
     *
     */
    public function createCheckoutId(){

        $id = $this->insert(array(
            'abandoned'     => '1',
            'status'        => 0,
            'create_date'   => new Zend_Db_Expr('NOW()')
        ));

        setcookie('FenixCheckoutId', $id, time() + 220752000, '/');

        return $id;
    }

    /**
     * Пересохранение идентификатора оформленного заказа во временный, для отображения заказа на странице "OK"
     *
     * @return int
     */
    public function convertSessionId()
    {
        $currentId = $_COOKIE['FenixCheckoutId'];
        $_COOKIE['FenixCheckoutOrderId'] = $_COOKIE['FenixCheckoutId'];
        $_COOKIE['FenixCheckoutId'] = '';

        setcookie('FenixCheckoutOrderId', $currentId, time() + 220752000, '/');
        setcookie('FenixCheckoutId', '', time() - 3600,'/');

        return (int) $_COOKIE['FenixCheckoutOrderId'];
    }

    /**
     * Идентификатор оформленного заказа
     *
     * @return int
     */
    public function getSessionIdForSuccess()
    {
        return (int) @$_COOKIE['FenixCheckoutOrderId'];
    }


    public function saveCoupon($coupon)
    {
        $orderId = $this->getSessionId();

        $this->setTable('checkout_orders')->update(array(
            'coupon_id' => $coupon->id
        ), 'id = ' . (int) $orderId);

        return;
    }
    public function saveDiscountCard($discountCard)
    {
        $orderId = $this->getSessionId();

        $this->setTable('checkout_orders')->update(array(
            'discount_card'    => $discountCard->id,
            'discount_percent' => $discountCard->discount
        ), 'id = ' . (int) $orderId);

        return;
    }

    /**
     * Редирект на главную если страница успешного оформления уже открывалась
     */
    public function redirectRefreshSuccess(){
        $order_id = isset($_COOKIE['FenixCheckoutOrderId']) ? $_COOKIE['FenixCheckoutOrderId'] : '0';
        $viewed   = isset($_COOKIE['FenixCheckoutSuccessViewed']) ? $_COOKIE['FenixCheckoutSuccessViewed'] : null;
        if($viewed == $order_id){
            Fenix::redirect('/');
            exit;
        }
        else{
            setcookie('FenixCheckoutSuccessViewed', $order_id, time() + 220752000, '/');
        }
    }
    public function removeDiscountCard()
    {
        $orderId = $this->getSessionId();

        $this->setTable('checkout_orders')->update(array(
            'discount_card' => 0
        ), 'id = ' . (int) $orderId);

        return;
    }

    public function removeCoupon()
    {
        $orderId = $this->getSessionId();

        $this->setTable('checkout_orders')->update(array(
            'coupon_id' => 0
        ), 'id = ' . (int) $orderId);

        return;
    }
    public function setBonus($bonus)
    {
        $orderId = $this->getSessionId();
        /*$this->setTable('checkout_orders');
        $order = $this->fetchRow( 'id = ' . (int) $orderId);
        */
        $this->setTable('checkout_orders')->update(array(
            'used_bonus' => $bonus
        ), 'id = ' . (int) $orderId);

        return;
    }
    public function removeBonus()
    {
        $orderId = $this->getSessionId();

        $this->setTable('checkout_orders')->update(array(
            'used_bonus' => 0
        ), 'id = ' . (int) $orderId);

        return;
    }

    public function fastOrder($req)
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();

        $User    = Fenix::getModel('session/auth')->getUser();
        $customerId = 0;
        if ($User instanceof Zend_Db_Table_Row) {
            $customerId = $User->id;
        }

        $abandoned = '0';

        $data = array(
            'create_date'        => new Zend_Db_Expr('NOW()'),
            'abandoned'          => $abandoned,
            'status'             => Fenix_Checkout_Model_Order::STATUS_PENDING,
            'customer_id'        => $customerId,
            'name'               => 'Заказ без оформления',
            'cellphone'          => $req->getPost('cellphone'),
            'email'              => '',
            'delivery_type'      => '',
            'delivery_city'      => '',
            'pay_variants'       => '',
            'comment'            => 'Заказ без оформления',
        );

        $this->setTable('checkout_orders')->update($data, 'id = ' . (int) $orderId);

        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        /*if ($order->getCouponId() > 0) {
            $this->setTable('catalog_coupons')->update(array(
                'is_active' => '0'
            ), 'id = ' . (int) $order->getCouponId());
        }

        // Убираем бонусы если они привышают 50% суммы заказа
        $total = Fenix::getModel('checkout/order')->getTotalPrice();
        if ($order->getUsedBonus() > $total / 2) {
            Fenix::getModel('checkout/process')->removeBonus();
        }*/
        Fenix::getModel('checkout/order')->sendNotify($order);

        return (int) $orderId;
    }

}