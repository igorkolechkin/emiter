<?php
class Fenix_Checkout_Model_Backend_Orders extends Fenix_Resource_Model
{
    public function getOrdersListAsSelect()
    {
        $this->setTable('checkout_orders');
        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'co' => $this->_name
        ), array(
            '*',
            new Zend_Db_Expr('(SELECT SUM(cp.qty) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id) AS total_qty')
        ));

        $Select ->where('co.abandoned = ?', '0');
        $Select ->order('co.create_date desc');

        return $Select;
    }

    public function addProduct($req){
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo((int)$req->getParam('order'));

        $product   = Fenix::getModel('catalog/products')->getProductbyId((int)$req->getParam('id'));


        if($orderInfo && $product){
            //price from catalog
            $unitPrice = Fenix::getModel('catalog/products')->getProductPrice($product) ;
            //$unitPrice = Fenix::getModel('catalog/products')->getStoreProductPrice($product) ;



            $data = array(
                'parent'     => 0,
                'order_id'   => $orderInfo->getId(),
                'product_id' => $product->id,
                'base_price' => $unitPrice,
                'qty'        => 1,
                'length'     => '',
                'width'      => '',
                'add_date'   => new Zend_Db_Expr('NOW()')
            );
            $this->setTable('checkout_products');
            $this->insert($data);
        }

        return false;

    }

    public function removeProduct($req){
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo((int)$req->getParam('order'));

        if($orderInfo){
            $productId = (int) $req->getParam('unit');

            $this->setTable('checkout_products');
            $this->delete('id = ' . $productId . ' OR parent_product = ' . $productId);
        }

        return false;
    }

    public function updateProducts($req){
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo((int)$req->getParam('order'));

        if($orderInfo){
            $this->setTable('checkout_products');
            foreach ((array) $req->getPost('data') AS $_i => $_data) {

                if ((int)$_data['qty'] > 0 && (int)$_data['order_unit_id'] > 0) {
                    $this->update(array(
                        'qty'  => (int)$_data['qty'],
                        'size' => (string)$_data['size']
                    ), 'id = ' . (int)$_data['order_unit_id']);
                }
            }
        }

        return false;

    }
    public function editDelivery($req){
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo((int)$req->getParam('id'));

        //Fenix::dump($orderInfo);
        if($orderInfo){
            $this->setTable('checkout_orders');
            $data = array(
                'store_id'           => (int)$req->getPost('store_id'),
                'delivery_type'      => (string)$req->getPost('delivery_type'),
                'delivery_city'      => (string)$req->getPost('delivery_city'),
                'delivery_warehouse' => (string)$req->getPost('delivery_warehouse'),
                'address'            => (string)$req->getPost('address')
            );

            if ($req->getPost('delivery_type') == 'pickup') {
                $data['delivery_price'] = 0;
            } else {
                if ($orderInfo->getTotalSum() > Fenix::getConfig('checkout_order_delivery_limit'))
                    $data['delivery_price'] = Fenix::getConfig('checkout_order_delivery_min');
                else
                    $data['delivery_price'] = Fenix::getConfig('checkout_order_delivery_max');
            }
            $this->update($data, 'id = ' . (int) $orderInfo->id);
        }

        return false;

    }
}