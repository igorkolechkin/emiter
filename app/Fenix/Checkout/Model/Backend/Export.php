<?php
class Fenix_Checkout_Model_Backend_Export extends Fenix_Resource_Model
{
    public function getCheckoutEmailListAsSelect()
    {
        $this->setTable('checkout_orders');

        $Select = $this->select();
        $Select->where('email <> ""');
        $Select->group('email');
        $Select ->from($this->_name);

        return $Select;
    }

    public function exportCheckoutEmailsToExcel(){
        $list = $this->fetchAll(self::getCheckoutEmailListAsSelect());
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");


        $cols = array(
            'email' => array(
                'index' => 0,
                'data' => (object)array('sys_title' => 'email')
            )
        );

        $prefixCount = 0;//0 доп колонки
        $row = 2;

        foreach ($list as $i => $record) {
            foreach($cols as $attr) {
                $attributeSysTitle = $attr["data"]->sys_title;
                //Атрибуты ENUM
                if(isset($attr["data"]->sql_type)&& $attr["data"]->sql_type == 'ENUM')
                {
                    if($value =$record->{$attributeSysTitle}=='0')
                        $value = 'нет';
                    else
                        $value = 'да';
                }
                else {
                    $id = $record->{$attributeSysTitle};
                    switch($attributeSysTitle){
                        /* case 'parent':
                             $category = Fenix::getCollection('catalog/categories')->getCategoryById($id);
                             if($category)
                                 $value = $category->getTitle();
                             else
                                 $value = $id;
                             break;
                         case 'attributeset_id':
                             $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById($id);
                             if($attributeset)
                                 $value = $attributeset->title;
                             else
                                 $value = $id;
                             break;
                         case 'operation_id':
                             $category = Fenix::getModel('catalog/backend_attributeset_categories')->getCategoryById($id);
                             if($category)
                                 $value = $category->title;
                             else
                                 $value = $id;
                             break;
                         case 'currency_id':
                             $currency = Fenix::getModel('core/currency')->getCurrencyById($id);
                             if($currency)
                                 $value = $currency->alias;
                             else
                                 $value = $id;
                             break;*/
                        default:
                            $value = $record->{$attributeSysTitle};
                    }

                }
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueByColumnAndRow(
                            $cols[$attributeSysTitle]["index"],
                                $row+$i,
                                $value
                    );
            }
        }
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="my_objects.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}