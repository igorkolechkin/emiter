<?php
class Fenix_Checkout_Model_Privat24part extends Fenix_Resource_Model
{
    public function serverRespond($respondXML, $respondXMLClear = null)
    {
        $fp = fopen(TMP_DIR_ABSOLUTE . "" . date('Y_m_d-H_i_s-')  . ".xml", "w+");
        fwrite($fp, $respondXMLClear);
        fclose($fp);

        $fp = fopen(TMP_DIR_ABSOLUTE . "" . date('Y_m_d-H_i_s-serialize')  . ".xml", "w+");
        fwrite($fp, serialize($GLOBALS['HTTP_RAW_POST_DATA']));
        fclose($fp);

        list($orderId, $tokenCount) = explode('-', $respondXML->orderId);

        Fenix::getModel('core/mail')->sendMail('dev.karpenko@gmail.com', 'no-reply@' . $_SERVER['HTTP_HOST'], 'privat24part serverRespond',
            serialize(Fenix::getRequest()->getPost()) . '|$orderId:' . serialize($orderId) . '|$tokenCount:' . serialize($tokenCount).'| status'.$respondXML->paymentState
        );

        if($respondXML->paymentState == Fenix_Api_Privat24part::STATUS_SUCCESS)
        {
            Fenix::getModel('core/mail')->sendMail('dev.karpenko@gmail.com', 'no-reply@' . $_SERVER['HTTP_HOST'], 'privat24part serverRespond success',
                serialize(Fenix::getRequest()->getPost()) . '|$orderId:' . serialize($orderId) . '|$tokenCount:' . serialize($tokenCount).'| status'.$respondXML->paymentState
            );

            Fenix::getModel('checkout/order')->processEpay(
                $orderId,
                $respondXMLClear
            );
        }
        else{
            Fenix::getModel('core/mail')->sendMail('dev.karpenko@gmail.com', 'no-reply@' . $_SERVER['HTTP_HOST'], 'privat24part serverRespond fail',
                serialize(Fenix::getRequest()->getPost()) . '|$orderId:' . serialize($orderId) . '|$tokenCount:' . serialize($tokenCount).'| status'.$respondXML->paymentState
            );
            Fenix::getModel('checkout/order')->processEpayFail(
                $orderId,
                $respondXMLClear
            );
        }
        return $respondXML->orderId . '=' . $respondXML->paymentState;
    }

    public function saveToken($order, $responseXML){
        $this->setTable('checkout_orders');
        $data = array(
            'token'      => $responseXML->token,
            'token_time' => date('Y.m.d H:i:s'),
            //'token_count' => new Zend_Db_Expr('token_count + 1')
        );
        $this->update($data, $this->getAdapter()->quoteInto('id = ?', $order->getId()));

    }

    public function nextToken($order){
        $this->setTable('checkout_orders');
        $data = array(
            'token_count' => new Zend_Db_Expr('token_count + 1')
        );
        $this->update($data, $this->getAdapter()->quoteInto('id = ?', $order->getId()));

    }
}