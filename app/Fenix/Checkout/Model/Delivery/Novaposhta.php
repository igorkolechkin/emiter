<?php
class Fenix_Checkout_Model_Delivery_Novaposhta extends Fenix_Resource_Model
{
    private static $API_KEY = '7f086f03f9c1c999c756eca52155c839'; //cache
    //private static $API_KEY = '641a69cb4a2fc3af93e2b5bc4770f489'; 925

    public function getCitiesList()
    {	
	
	
		 // api новая почта 2.0
        //if(empty($data)){
           $cities= Fenix::getModel('checkout/delivery_novaposhta2')->getCitiesList();
           return new Fenix_Object_Rowset(array(
            'data' => $cities
           ));                     
        //}
        // $cacheId  = 'novaposhta';
        // $cache    = Fenix_Cache::getCache('checkout/delivery');


        // if ($cache !== false) {
            // /*Fenix::dump($cache->load($cacheId));*/
            // if (!$data = $cache->load($cacheId)) {

                // $data  = $this->_getWarehouse();
                // $cache ->save($data, $cacheId);
            // }
        // }
        // else {
            // $data = $this->_getWarehouse();
        // }

        // $city = array();
        // foreach ($data->result->whs->warenhouse AS $_warehouse) {
            // $city[$_warehouse->cityRu] = array(
                // 'name' => $_warehouse->cityRu
            // );
        // }

        // $city = array_values($city);

        // return new Fenix_Object_Rowset(array(
            // 'data' => $city
        // ));
    }

    public function getWarehouseList($city)
    {
		// api новая почта 2.0
        //if(empty($data)){
           $cities= Fenix::getModel('checkout/delivery_novaposhta2')->getWarehouse($city);
           return new Fenix_Object_Rowset(array(
            'data' => $cities
           )); 
                     
        //}
        // $cacheId  = 'novaposhta';
        // $cache    = Fenix_Cache::getCache('checkout/delivery');

        // if ($cache !== false) {
            // if (!$data = $cache->load($cacheId)) {

                // $data  = $this->_getWarehouse();
                // $cache ->save($data, $cacheId);
            // }
        // }
        // else {
            // $data = $this->_getWarehouse();
        // }

        // $warehouse = array();
        // foreach ($data->result->whs->warenhouse AS $_warehouse) {
            // if ($_warehouse->cityRu == $city) {
                // $warehouse[] = array(
                    // 'name'    => $_warehouse->addressRu,
                    // 'code'    => $_warehouse->wareId,
                    // 'phone'   => $_warehouse->phone,
                    // 'address' => $_warehouse->addressRu
                // );
            // }
        // }

        // return new Fenix_Object_Rowset(array(
            // 'data' => $warehouse
        // ));
    }



    private function _getWarehouse()
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?><file><auth>' . self::$API_KEY . '</auth><warenhouse/></file>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://orders.novaposhta.ua/xml.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        return new Zend_Config_Xml($response);
    }
}