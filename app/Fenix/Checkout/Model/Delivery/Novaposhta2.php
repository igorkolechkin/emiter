<?php


/**
 * класс для работы c новая почта api 2.0
 */
class Fenix_Checkout_Model_Delivery_Novaposhta2 extends Fenix_Resource_Model
{

    /**
     *ключ api
     * @var type
     */
    private $apiKey = 'a0d80fd43cbef6faef56a74fb62b225f';

    /**
     *url запрос
     * @var type
     */
    private $urlReq = 'https://api.novaposhta.ua/v2.0/xml/';


    /**
     * список городов
     * @return type
     */
    public function getCitiesList()
    {

        $data = array();
        //проверяем есть ли кеш такой
        $cacheId = 'novaposhta_cities';
        if ($cache = $this->getCache($cacheId)) {
            return $cache;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <file>
                    <modelName>Address</modelName>
                    <calledMethod>getCities</calledMethod>
                    <apiKey>' . $this->apiKey . '</apiKey>
                </file>';

        //запрос на получение данных
        $_data = $this->getDataRequest($xml)
            ->get('data')
            ->item;

        //перебираем
        foreach ($_data as $key => $value) {
            $data[$value->DescriptionRu] = array('name' => $value->DescriptionRu);
        }

        //сохраняем в кеш
        $cache = $this->setCache($cacheId, $data);

        return $data;

    }


    /**
     * список складов
     * @param type $city
     * @return type
     */
    public function getWarehouse($city)
    {
        $data = array();

        if ($city == '') {
            return $data;
        }

        //проверяем есть ли кеш такой
        $cacheId = 'novaposhta_wh_' . md5($city);
        if ($cache = $this->getCache($cacheId)) {
            //Fenix::dump($cache);
            return $cache;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
        <root>
            <modelName>AddressGeneral</modelName>
            <calledMethod>getWarehouses</calledMethod>
            <methodProperties>
            <CityName>' . $city . '</CityName>
            </methodProperties>
            <apiKey>' . $this->apiKey . '</apiKey>
        </root>';

        //запрос на получение данных
        $_data = $this->getDataRequest($xml)->data->current();

        if ($_data->CityDescriptionRu) {
            $_data = array($_data);
        }

        //перебираем
        foreach ($_data as $key => $value) {
            if ($value->CityDescriptionRu == $city) {
                $data[] = array(
                    'name'    => $value->DescriptionRu,
                    'code'    => $value->TypeOfWarehouse,
                    'phone'   => $value->Phone,
                    'address' => $value->DescriptionRu
                );
            }

        }
        //Fenix::dump($data);
        //сохраняем в кеш
        $cache = $this->setCache($cacheId, $data);

        return $data;

    }


    /**
     * получить кеш
     * @param type $cacheId
     * @return boolean
     */
    public function getCache($cacheId)
    {
        //кеширование
        $cache = Fenix_Cache::getCache('checkout/delivery');
        if ($cache !== false) {
            if ($data = $cache->load($cacheId)) {
                return $data;
            }
        }

        return false;
    }

    /**
     * записать кеш
     * @param type $cacheId
     * @param type $data
     * @return boolean
     */
    public function setCache($cacheId, $data)
    {
        //кеширование
        $cache = Fenix_Cache::getCache('checkout/delivery');
        if ($cache !== false) {
            $cache->save($data, $cacheId);

            return true;
        }

        return false;
    }

    /**
     * получить данные(проверки)
     * @param type $params
     * @param type $cacheId
     * @return type
     */
    private function getDataRequest($params)
    {

        //проверка на пустоту
        $request = $this->curlRequest($params);
        if (empty($request)) {
            return array();
        }

        //правильно ли обработало
        $data = new Zend_Config_Xml($request);
        //Fenix::dump($data);
        if ( ! $data && $data->get('success') != 'true') {
            return array();
        }

        return $data;
    }


    /**
     * curl запрос
     * @param type $dataXml
     * @param type $url
     * @return type
     */
    private function curlRequest($dataXml, $url = null)
    {

        if ($url === null) {
            $url = $this->urlReq;
        }
//        Fenix::dump($dataXml,$url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataXml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        //Fenix::dump($response);

        return $response;
    }


}