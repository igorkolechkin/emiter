<?php
class Fenix_Checkout_Model_Mailer extends Fenix_Resource_Model
{
    public function decoratePay($status)
    {
        switch ($status) {
            case self::STATUS_PENDING:
                return 'В ожидании';
                break;
            case self::STATUS_PROCESS:
                return 'В обработке';
                break;
            case self::STATUS_CANCELED:
                return 'Отменён';
                break;
            case self::STATUS_FINISHED:
                return 'Завершён';
                break;
            case self::STATUS_HOLD:
                return 'На удержании';
                break;
        }
    }

    /**
     * Оформление заказа
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function sendMail(Fenix_Controller_Request_Http $req)
    {
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo();

        $assignData = array(
            'name'          => $this->getRequest()->getPost('name'),
            'orderId'       => $orderInfo->id,
            'delivery'      => $orderInfo->getDelivery()->type_formatted . ' ' . $orderInfo->getDelivery()->address . ($orderInfo->getDelivery()->address == '' ? ' ' . $orderInfo->getDelivery()->city . ', ' . $orderInfo->getDelivery()->warehouse : null),
            'cellphone'     => $req->getPost('cellphone'),
            'email'         => $req->getPost('email'),
            'comment'       => $req->getPost('comment'),
            'orderdate'     => date('d.m.Y'),
            'table'         => Zend_Layout::getMvcInstance()->getView()->partial('checkout/table-info-mailer.phtml', array('products' => $orderInfo->getProducts(), 'orderInfo' => $orderInfo, 'mail' => true))
        );

        Fenix::getModel('core/mail')->sendAdminMail(array(
            'template' => 'checkout_form_admin', // шаблон письма
            'assign'   => $assignData,
        ));

        Fenix::getModel('core/mail')->sendUserMail(array(
            'to'       => $req->getPost('email'),
            'template' => 'checkout_form', // шаблон письма
            'assign'   => $assignData,
        ));

        return true;
    }

    /**
     * Изменение статуса заказа
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function sendMailStatus($orderId, $statusId)
    {
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);

        Fenix::getModel('core/mail')->sendUserMail(array(
            'to'       => $orderInfo->getEmail(),
            'template' => 'change_status', // шаблон письма
            'assign'   => array(
                'name'           => $orderInfo->getName(),
                'status'         => Fenix::getModel('checkout/order')->decorateStatus($statusId),
                'orderId'        => $orderInfo->getId(),
                'delivery'       => $orderInfo->getDelivery()->address . ($orderInfo->getDelivery()->address ? ' ' . $orderInfo->getDelivery()->city . ', ' . $orderInfo->getDelivery()->warehouse : null),
                'cellphone'      => $orderInfo->getCellphone(),
                'email'          => $orderInfo->getEmail()
            ),
        ));

        return true;
    }
}