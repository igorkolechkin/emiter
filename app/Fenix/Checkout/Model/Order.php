<?php

class Fenix_Checkout_Model_Order extends Fenix_Resource_Model
{
    //новый, комплектуется, в процессе, доставки, доставлен, перезвонить
    const STATUS_PENDING   = 1; //Новый
    const STATUS_PROCESS   = 2; //В процессе
    const STATUS_CANCELED  = 3; //Отменен
    const STATUS_FINISHED  = 4; //Завершен
    const STATUS_HOLD      = 5; //на удержании
    const STATUS_EQUIPMENT = 6; //комплектуется
    const STATUS_DELIVERY  = 7; //доставка
    const STATUS_DELIVERED = 8; //доставлен
    const STATUS_CALL      = 9; //перезвонить

    public function updateStatus($statusId, $orderId)
    {
        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);

        $this->setTable('checkout_orders')->update(array(
            'status' => (int ) $statusId
        ), 'id = ' . (int) $orderId);

        switch ($order->getStatus()) {
            case self::STATUS_PENDING: //'В ожидании';
            case self::STATUS_PROCESS: //'В обработке';
            case self::STATUS_CANCELED://'Отменён';
            case self::STATUS_HOLD:    //return 'На удержании';
                if ($statusId == self::STATUS_FINISHED) {
                    //Начисление бонусов
                    /* Fenix::getModel('customer/bonus')->creditPoints(
                         $order,
                         'Заказ №' . $order->getId() . '. '
                         . 'Статус:' . self::decorateStatus($statusId) . '. Начисление баллов');

                     //Если использовались бонусы списываем их
                     if ($order->getUsedBonus() > 0) {
                         Fenix::getModel('customer/bonus')->debitMoney($order,
                             'Заказ №' . $order->getId() . '. '
                             . 'Статус:' . self::decorateStatus($statusId) . '. ' .
                             'Использовано ' . $order->getUsedBonus() .' бонусных средств');
                     }*/
                }
                break;
            case self::STATUS_FINISHED: //'Завершён'


                if ($statusId != self::STATUS_FINISHED) {
                    /* //Отмена начисления бонусов
                     Fenix::getModel('customer/bonus')->debitPoints(
                         $order,
                         'Заказ №' . $order->getId() . '. '
                         . 'Статус:' . self::decorateStatus($statusId) . '. Отмена начисления баллов');

                     //Если использовались бонусы возвращаем бонусы
                     if ($order->getUsedBonus() > 0) {
                         Fenix::getModel('customer/bonus')->creditMoney($order,
                             'Заказ №' . $order->getId() . '. '
                             . 'Статус:' . self::decorateStatus($statusId) . '. ' .
                             'Возврат ' . $order->getUsedBonus() .' бонусных средств');
                     }*/

                }
                else {

                }
                break;
        }
        if ($statusId == self::STATUS_FINISHED) {
            Fenix::getModel('catalog/backend_promo')->addLastOrder($order);
        }

        return true;
    }

    public function updateComment($comment, $orderId)
    {
        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);

        $this->setTable('checkout_orders')->update(array(
            'comment' => (string) $comment
        ), 'id = ' . (int) $orderId);

        return true;
    }

    public function decorateStatus($status)
    {
        switch ($status) {
            case self::STATUS_PENDING:
                return 'Новый';
                break;
            case self::STATUS_PROCESS:
                return 'В процессе';
                break;
            case self::STATUS_CANCELED:
                return 'Отменён';
                break;
            case self::STATUS_FINISHED:
                return 'Завершён';
                break;
            case self::STATUS_HOLD:
                return 'На удержании';
                break;
            case self::STATUS_EQUIPMENT:
                return 'Комплектуется';
                break;
            case self::STATUS_DELIVERY:
                return 'Доставка';
                break;
            case self::STATUS_DELIVERED:
                return 'Доставлен';
                break;
            case self::STATUS_CALL:
                return 'Перезвонить';
                break;

        }
    }

    /**
     * Оформление заказа
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function processOrder(Fenix_Controller_Request_Http $req)
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);

        $User = Fenix::getModel('session/auth')->getUser();
        $customerId = 0;
        if ($User instanceof Zend_Db_Table_Row) {
            $customerId = $User->id;
        }

        $abandoned = '0';

        $data = array(
            'create_date'   => new Zend_Db_Expr('NOW()'),
            'abandoned'     => $abandoned,
            'status'        => self::STATUS_PENDING,
            'customer_id'   => $customerId,
            'name'          => $req->getPost('name'),
            'cellphone'     => $req->getPost('cellphone'),
            'email'         => $req->getPost('email'),
            'comment'       => $req->getPost('comment'),
            'delivery_type' => $req->getPost('delivery_type'),
            'delivery_city' => trim($req->getPost('delivery_city'))
        );

        switch ($req->getPost('payment_variants')) {
            case 'card':
                $data['pay_variants'] = $req->getPost('payment_card');
                break;
            case 'liqpay':
                $data['pay_variants'] = 'liqpay';
                break;
            default:
                $data['pay_variants'] = $req->getPost('payment_variants');

        }

        if ($req->getPost('delivery_type') == 'pickup') {
            $data['delivery_address'] = trim($req->getPost('delivery_city'));
        }

        if ($req->getPost('delivery_type') == 'novaja_pochta_sklad') {
            $data['delivery_address'] = trim($req->getPost('delivery_novaja_pochta_sklad'));
        }

        if ($req->getPost('delivery_type') == 'novaja_pochta_address') {
            $data['delivery_address'] = trim($req->getPost('delivery_novaja_pochta_address'));
        }

        if ($req->getPost('delivery_type') == 'ukrpochta_otdelenie') {
            $data['delivery_address'] = trim($req->getPost('delivery_ukrpochta_otdelenie'));
        }

        if ($req->getPost('delivery_type') == 'ukrpochta_address') {
            $data['delivery_address'] = trim($req->getPost('delivery_ukrpochta_address'));
        }

        if ($req->getPost('delivery_type') == 'courier') {
            $data['delivery_address'] = trim($req->getPost('delivery_address'));
        }
        //Добавляем скидку по цене если она больше чем по карте
        /*   $discountByPrice = Fenix::getModel('checkout/order')->getOrderDiscountByPrice($order);
           if ($discountByPrice > $order->getDiscountPercent())
               $data['discount_percent'] = $discountByPrice;
   */
        //Cтоимость доставки
        /*        if ($req->getPost('delivery_type') == 'pickup') {
                    $data['delivery_price'] = 0;
                } else {
                    if ($order->getTotalSum() > Fenix::getConfig('checkout_order_delivery_limit'))
                        $data['delivery_price'] = Fenix::getConfig('checkout_order_delivery_min');
                    else
                        $data['delivery_price'] = Fenix::getConfig('checkout_order_delivery_max');
                }
        */

        $this->setTable('checkout_orders')->update($data, 'id = ' . (int) $orderId);

        $order = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        if ($order->getCouponId() > 0) {
            $coupon = Fenix::getModel('catalog/backend_coupons')->getCouponById($order->getCouponId());
            if ($coupon && $coupon->is_reusable == '0') {
                $this->setTable('catalog_coupons')->update(array(
                    'is_active' => '0'
                ), 'id = ' . (int) $order->getCouponId());
            }
        }

        // Убираем бонусы если они привышают 50% суммы заказа
        $total = Fenix::getModel('checkout/order')->getTotalPrice();
        if ($order->getUsedBonus() > $total / 2) {
            Fenix::getModel('checkout/process')->removeBonus();
        }
        $this->sendNotify($order);

        //Обновляем итого в заказе
        Fenix::getModel('checkout/process')->updateOrderTotal($orderId);

        return (int) $orderId;
    }

    /**
     * Обновление выбранного магазина
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function setStore(Fenix_Controller_Request_Http $req)
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        $data = array(
            'store_id' => $req->getPost('store_id')
        );

        $result = $this->setTable('checkout_orders')->update($data, 'id = ' . (int) $orderId);

        return $result;
    }

    public function sendNotify($orderInfo)
    {
        $style = 'border-collapse: collapse;border:1px solid #d9d9d9;padding:5px;';

        $userInfo = Fenix::getCreatorUI()->loadPlugin('Table')->setId('userInfo');
        $userInfo->setStyle($style);
        $userInfo->addCell('name', 'label', 'Имя')
            ->setStyle($style)
            ->addCell('name', 'value', $orderInfo->getName())
            ->setStyle($style);

        $userInfo->addCell('cellphone', 'label', 'Телефон')
            ->setStyle($style)
            ->addCell('cellphone', 'value', $orderInfo->getCellphone())
            ->setStyle($style);

        $userInfo->addCell('email', 'label', 'Email')
            ->setStyle($style)
            ->addCell('email', 'value', $orderInfo->getEmail())
            ->setStyle($style);

        $userInfo->addCell('city', 'label', 'Город')
            ->setStyle($style)
            ->addCell('city', 'value', $orderInfo->getDelivery()->city)
            ->setStyle($style);

        $productsTable = Fenix::getCreatorUI()->loadPlugin('Table')->setId('productsTable');
        $productsTable->setStyle($style);

        $productsTable->addHeader('sku', 'Артикул')
            ->setStyle($style)
            ->appendStyle('text-align:left;')
            ->addHeader('name', 'Товар')
            ->setStyle($style)
            ->appendStyle('text-align:left;')
            ->addHeader('price', 'Цена')
            ->setStyle($style)
            ->appendStyle('text-align:left;')
            ->addHeader('qty', 'Кол-во')
            ->setStyle($style)
            ->appendStyle('text-align:left;')
            ->addHeader('title', 'Всего')
            ->setStyle($style)
            ->appendStyle('text-align:left;');

        foreach ($orderInfo->getProducts() AS $_product) {
            $productsTable->addCell('id_' . $_product->getOrderProductId(), 'sku', $_product->getId_1c())
                ->setStyle($style);
            $productsTable->addCell('id_' . $_product->getOrderProductId(), 'title', $_product->getTitle())
                ->setStyle($style);
            $productsTable->addCell('id_' . $_product->getOrderProductId(), 'price', Fenix::getHelper('catalog/decorator')->decoratePrice($_product->getCpPrice()))
                ->setStyle($style);
            $productsTable->addCell('id_' . $_product->getOrderProductId(), 'qty', $_product->getCpQty())
                ->setStyle($style);
            $productsTable->addCell('id_' . $_product->getOrderProductId(), 'total', Fenix::getHelper('catalog/decorator')->decoratePrice($_product->getCpPrice() * $_product->getCpQty()))
                ->setStyle($style);

            if ($_product->getCpType() == 'kit' || $_product->getCpType() == 'kit2') {
                $childproducts = Fenix::getCollection('checkout/order')->getProducts($orderInfo->getId(), $_product->getOrderUnitId());
                foreach ($childproducts AS $_productKit) {
                    $productsTable->addCell('id_kit_' . $_productKit->getOrderUnitId(), 'sku', '↳')
                        ->setStyle($style)
                        ->appendStyle('text-align:right;')
                        ->appendStyle('font-size:21px;');
                    $productsTable->addCell('id_kit_' . $_productKit->getOrderUnitId(), 'title', $_productKit->getTitle())
                        ->setColspan(4)
                        ->setStyle($style);
                }
            }
        }
        $summary = '<p style="text-align: left">Товаров на сумму: ' . $orderInfo->getTotalPrice() . ' грн';
        /*$summary .= '<br/>Стоимость доставки: ' . $orderInfo->getDeliveryPrice() . ' грн';*/
//        $summary .= '<br/>Скидка: ' . $orderInfo->getTotalDiscountPrice() . ' грн';
//        $summary .= '<br/>Итого к оплате: ' . ($orderInfo->getTotalOrderPrice() + $orderInfo->getDeliveryPrice()) . ' грн';
        $summary .= '</p>';

        //$store = Fenix::getModel('catalog/stores')->getStoreById($orderInfo->getStoreId());

        $deliveryBlock[] = '<p>' . $orderInfo->getDelivery()->type_formatted . '</p>';

        if ($orderInfo->getDelivery()->city != '') {
            $deliveryBlock[] = '<p>' . $orderInfo->getDelivery()->city . '</p>';
        }

        $deliveryBlock[] = '<p>' . $orderInfo->getDelivery()->address . '</p>';

        $dataForEmail = array(
            'name'      => $this->getRequest()->getPost('name'),
            'orderId'   => $orderInfo->id,
            'cellphone' => $this->getRequest()->getPost('cellphone') ? $this->getRequest()->getPost('cellphone') : $orderInfo->getCellphone(),
            'email'     => $this->getRequest()->getPost('email'),
            'comment'   => $this->getRequest()->getPost('comment'),
            'orderdate' => date('d.m.Y'),
            'info'      => $userInfo->fetch(),
            'products'  => $productsTable->fetch() . $summary,
            'order'     => $orderInfo,
            'delivery'  => implode('', $deliveryBlock),
        );

        Fenix::getModel('core/mail')->sendAdminMail(array(
            'template' => 'checkout_form_admin', // шаблон письма
            'assign'   => $dataForEmail,
        ));

        Fenix::getModel('core/mail')->sendUserMail(array(
            'to'       => $orderInfo->getEmail(),
            'template' => 'checkout_form', // шаблон письма
            'assign'   => $dataForEmail,
        ));
    }

    public function processEpay($orderId, $answer)
    {
        $data = array(
            'abandoned'   => '0',
            'is_epay'     => '1',
            'epay_answer' => serialize($answer)
        );
        $this->setTable('checkout_orders')->update($data, 'id = ' . (int) $orderId);
    }

    public function getTotalPrice()
    {
        $order = Fenix::getCollection('checkout/order')->getOrderInfo();
        $products = Fenix::getCollection('checkout/order')->getProducts();
        $total = 0;
        foreach ($products AS $i => $_product) {
            $total += ($_product->getOrderUnitPrice() * $_product->getOrderQty());
            if ($_product->getCpType() == 'kit' || $_product->getCpType() == 'kit2') {
                $childproducts = Fenix::getCollection('checkout/order')->getProducts(null, $_product->getOrderUnitId());
                foreach ($childproducts AS $_childproduct) {
                    $total += ($_childproduct->getOrderUnitPrice() * $_product->getOrderQty());
                }
            }
        }

        return $total;
    }

    /**
     * Товар в корзине?
     * @param $product
     * @return bool
     */
    public function inCheckout($product)
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        $this->setTable('checkout_products');
        $_test = $this->fetchRow('order_id = ' . (int) $orderId . ' AND product_id = ' . (int) $product->getId());
        if ($_test) {
            return true;
        }
        else {
            return false;
        }
    }


    public function getOrderDiscountByPrice($orderInfo)
    {
        $total = 0;
        if ($orderInfo) {
            foreach ($orderInfo->getProducts() AS $_product) {
                if ($_product->getCpType() == 'kit') {
                    if ((int) $_product->getCpParent() == 0 && (int) $_product->getCpDiscount() == 0) {
                        $total += ($_product->getOrderUnitPrice() * $_product->getOrderQty());
                    }
                }
                else if ((int) $_product->getCpDiscount() == 0) {
                    $total += ($_product->getOrderUnitPrice() * $_product->getOrderQty());
                }
            }
        }

        $dicount = 0;
        if ($orderInfo) {
            if ((int) $total >= (int) Fenix::getConfig('checkout_discount_limit_1')) {
                $dicount = Fenix::getConfig('checkout_discount_percent_1');
            }
            if ((int) $total >= (int) Fenix::getConfig('checkout_discount_limit_2')) {
                $dicount = Fenix::getConfig('checkout_discount_percent_2');
            }
            if ((int) $total >= (int) Fenix::getConfig('checkout_discount_limit_3')) {
                $dicount = Fenix::getConfig('checkout_discount_percent_3');
            }
        }

        return (int) $dicount;

    }

    public function updateProductsPrice($orderId)
    {

        /*  $products = Fenix::getCollection('checkout/order')->getProducts($orderId);
          foreach ($products as $_product) {
              $unitPrice = Fenix::getModel('catalog/products')->getProductPrice($_product);
              $this->setTable('checkout_products');
              $_test = $this->fetchRow('order_id = ' . (int)$orderId . ' AND product_id = ' . (int)$_product->getId());

                  //Fenix::dump($orderId,$_product->getId(), $_test);
              //Fenix::dump($_test);
              if ($_test) {
                  $data = array(
                      'unit_price' => $unitPrice
                  );
                  $this->update($data, 'id = ' . (int)$_test->id);
              }
          }
  */
    }

    public function calculateTotalInfo($order)
    {
        $saleList = Fenix::getModel('sale/sale')->getActiveSalesList('checkout');

        if ($saleList->count() > 0) {
            foreach ($saleList as $sale) {
                $_order = Fenix::getModel('sale/sale')->useSale($order, $sale);
            }
        }

        $discountByPrice = Fenix::getModel('checkout/order')->getOrderDiscountByPrice($order);

        $totalPriceNoDiscount = 0;
        $totalPriceHasDiscount = 0;

        $totalPriceOld = 0;
        $totalPrice = 0;
        $totalDiscount = 0;
        if ($order) {
            foreach ($order->getProducts() as $_product) {

                $productDiscount = $_product->getCpDiscount();
                if ($productDiscount > 0) //Товар со своей скидкой
                {
                    $_currentPrice = ($_product->getOrderUnitPrice() - round($_product->getOrderUnitPrice() * $productDiscount / 100)) * $_product->getOrderQty();
                    $totalPriceHasDiscount += $_currentPrice;
                    $totalPriceOld += $_currentPrice;
                }
                else { //Товар без скидки
                    $_currentPrice = $_product->getOrderUnitPrice() * $_product->getOrderQty();
                    $totalPriceNoDiscount += $_currentPrice;
                    $totalPriceOld += $_currentPrice;
                }
            }
        }

        $totalDiscount = round($totalPriceNoDiscount * $discountByPrice / 100);
        $totalPrice = $totalPriceHasDiscount + $totalPriceNoDiscount - $totalDiscount;

        if ($order) {
            $totalPrice += $order->getDeliveryPrice(); //Итого к оплате  + Доставка
        }

        $result = new Fenix_Object(array(
            'data' => array(
                'totalDiscount' => $totalDiscount,
                'totalPriceOld' => $totalPriceOld,
                'totalPrice'    => $totalPrice
            )
        ));

        //Fenix::dump($result);
        return $result;
    }

    /**
     * Обновляем расчеты скидок
     * @param null $orderId
     * @return bool
     *
     */
    public function updateActiveSales($orderId = null)
    {

        if ($orderId == null) {
            $orderId = Fenix::getModel('checkout/process')->getSessionId();
        }

        $order = Fenix::getModel('checkout/process')->getOrderById(
            $orderId
        );
//        /Fenix::dump($orderId);
        if ($order == null) {
            return false;
        }

        if ($order->total_qty == null) {
            return false;
        }

        //Обнуляем скидку
        self::setOrderDiscount($order->id, 0, 0);

        $products = Fenix::getCollection('checkout/order')->getProducts($order->id);
        $salesList = Fenix::getModel('sale/sale')->getActiveSalesList('product');
        foreach ($salesList as $sale) {
            $match = false;
            //Скидка зависит от:
            switch ($sale->param) {
                //Количества товаров в заказе
                case 'product-qty':
                    switch ($sale->compare) {
                        case 'equal':
                            if ($order->total_qty == $sale->param_value) {
                                $match = true;
                            }
                            break;
                        case 'less':
                            if ($order->total_qty < $sale->param_value) {
                                $match = true;
                            }
                            break;
                        case 'more':
                            if ($order->total_qty > $sale->param_value) {
                                $match = true;
                            }

                            break;
                    }
                    break;
                //Количества товаров в заказе
                case 'checkout-sum':

                    switch ($sale->compare) {
                        case 'equal':
                            if ($order->total_order_price == $sale->param_value) {
                                $match = true;
                            }
                            break;
                        case 'less':
                            if ($order->total_order_price < $sale->param_value) {
                                $match = true;
                            }
                            break;
                        case 'more':
                            if ($order->total_order_price > $sale->param_value) {
                                $match = true;
                            }

                            break;
                    }
                    break;
                case 'product-sum':
                    $match = true;
                    break;
            }
            if ($match) {
                //Скидка влияет на
                switch ($sale->target) {
                    //Cумму товаров без скидок
                    case 'order-nodiscount-price':
                        //Расчет скидки =       Сумма товаров без скидки       / 100 * Скидка в %
                        $discountAmount = round($order->total_nodiscount_price / 100 * $sale->discount, 0);
                        //Итого          = Сума товаров со скидкой      + Сумма товаров без скидки       -  Скидка
                        $totalOrderPrice = $order->total_discount_price + $order->total_nodiscount_price - $discountAmount;
                        self::setOrderTotalPrice($order->id, $totalOrderPrice);
                        self::setOrderDiscount($order->id, $sale->discount, $discountAmount);
                        break;
                    case 'product':
                        $saleProducts = $this->getSaleProductsIds($sale->id);
                        $new_price = 0;
                        foreach ($products as $product) {
                            if (in_array($product->id, $saleProducts)) {
                                $price = $product->cp_base_price / 100 * $sale->discount;
                                $new_price += $product->cp_base_price - $price;
                            }
                            else {
                                $new_price += $product->cp_base_price;
                            }
                        }
                        //Расчет скидки =       Сумма товаров без скидки       / 100 * Скидка в %
                        $discountAmount = $order->total_nodiscount_price - $new_price;
                        //Итого          = Сума товаров со скидкой      + Сумма товаров без скидки       -  Скидка
                        $totalOrderPrice = round($new_price, 0);
                        self::setOrderTotalPrice($order->id, $totalOrderPrice);
                        self::setOrderDiscount($order->id, $sale->discount, $discountAmount);
                        break;
                }
            }
        }
        $order = Fenix::getModel('checkout/process')->getOrderById(
            $orderId
        );
    }

    private function getSaleProductsIds($id)
    {
        $this->setTable('sale_products');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            's' => $this->getTable()
        ), array('product_id'));
        $Select->where('s.record_id = ?', $id);
        $Result = $this->fetchAll($Select);
        $idList = array();
        foreach ($Result as $relation) {
            $idList[] = $relation->product_id;
        }

        return $idList;
    }

    /**
     * Сохраняем сумму итого
     * @param $orderId
     * @param $totalOrderPrice
     * @return int
     */
    public function setOrderTotalPrice($orderId, $totalOrderPrice)
    {
        $data = array(
            'total_price' => $totalOrderPrice
        );
        $result = $this->setTable('checkout_orders')
            ->update($data, $this->getAdapter()->quoteInto('id = ?', (int) $orderId));

        return $result;
    }

    /**
     * Сохраняем скидку для заказа
     * @param $orderId
     * @param $discount
     * @param $discountAmount
     * @return int
     */
    public function setOrderDiscount($orderId, $discount, $discountAmount)
    {
        $data = array(
            'discount_percent' => $discount,
            'discount_amount'  => $discountAmount
        );

        $result = $this->setTable('checkout_orders')
            ->update($data, $this->getAdapter()->quoteInto('id = ?', (int) $orderId));

        return $result;
    }
}