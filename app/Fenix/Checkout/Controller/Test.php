<?php
class Fenix_Checkout_Controller_Test extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo(
            467581
        );

        $style    = 'border-collapse: collapse;border:1px solid #d9d9d9;padding:5px;';

        $userInfo = Fenix::getCreatorUI()->loadPlugin('Table');
        $userInfo ->setStyle($style);
        $userInfo ->addCell('name', 'label', 'Имя')
                  ->setStyle($style)
                  ->addCell('name', 'value', $orderInfo->getName())
                  ->setStyle($style);

        $userInfo ->addCell('cellphone', 'label', 'Телефон')
                  ->setStyle($style)
                  ->addCell('cellphone', 'value', $orderInfo->getCellphone())
                  ->setStyle($style);

        $userInfo ->addCell('email', 'label', 'Email')
                  ->setStyle($style)
                  ->addCell('email', 'value', $orderInfo->getEmail())
                  ->setStyle($style);

        $userInfo ->addCell('city', 'label', 'Город')
                  ->setStyle($style)
                  ->addCell('city', 'value', $orderInfo->getDelivery()->city)
                  ->setStyle($style);

        $productsTable = Fenix::getCreatorUI()->loadPlugin('Table');
        $productsTable ->setStyle($style);

        $productsTable ->addHeader('sku',   'Артикул')
                       ->setStyle($style)
                       ->appendStyle('text-align:left;')

                       ->addHeader('name',  'Товар')
                       ->setStyle($style)
                       ->appendStyle('text-align:left;')

                       ->addHeader('price', 'Цена')
                       ->setStyle($style)
                       ->appendStyle('text-align:left;')

                       ->addHeader('qty',   'Кол-во')
                       ->setStyle($style)
                       ->appendStyle('text-align:left;')

                       ->addHeader('title', 'Всего')
                       ->setStyle($style)
                       ->appendStyle('text-align:left;');

        foreach ($orderInfo->getProducts() AS $_product) {
            $productsTable ->addCell('id_' . $_product->getOrderUnitId(), 'sku',   $_product->getSku())
                           ->setStyle($style);
            $productsTable ->addCell('id_' . $_product->getOrderUnitId(), 'title', $_product->getTitle())
                           ->setStyle($style);
            $productsTable ->addCell('id_' . $_product->getOrderUnitId(), 'price', Fenix::getHelper('catalog/decorator')->decoratePrice($_product->getCpOrderPrice()))
                           ->setStyle($style);
            $productsTable ->addCell('id_' . $_product->getOrderUnitId(), 'qty',   $_product->getOrderQty())
                           ->setStyle($style);
            $productsTable ->addCell('id_' . $_product->getOrderUnitId(), 'total', Fenix::getHelper('catalog/decorator')->decoratePrice($_product->getCpOrderPrice() * $_product->getOrderQty()))
                           ->setStyle($style);

            if ($_product->getCpType() == 'kit' || $_product->getCpType() == 'kit2') {
                $childproducts = Fenix::getCollection('checkout/order')->getProducts($orderInfo->getId(), $_product->getOrderUnitId());
                foreach ($childproducts AS $_productKit) {
                    $productsTable ->addCell('id_kit_' . $_productKit->getOrderUnitId(), 'sku',   '↳')
                                   ->setStyle($style)
                                   ->appendStyle('text-align:right;')
                                   ->appendStyle('font-size:21px;');
                    $productsTable ->addCell('id_kit_' . $_productKit->getOrderUnitId(), 'title', $_productKit->getTitle())
                                   ->setColspan(4)
                                   ->setStyle($style);
                }
            }
        }

        Fenix::getModel('core/mail')->sendAdminMail(array(
            'template' => 'checkout.new.order', // шаблон письма
            'assign'   => array(
                'info'     => $userInfo->fetch(),
                'products' => $productsTable->fetch(),
                'order'    => $orderInfo
            ),
        ));
    }
}