<?php

class Fenix_Checkout_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionIdForSuccess();
        //Обновляем итого в заказе
        Fenix::getModel('checkout/process')->updateOrderTotal($orderId);
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo();

        if ($orderInfo === false) {
            Fenix::redirect('');
        }


        if ($this->getRequest()->isPost()) {
            Fenix::getModel('checkout/order')->processOrder(
                $this->getRequest()
            );

            if ($this->getRequest()->getPost('pay_variants') == 'privat24') {
                Fenix::redirect('checkout/privat24');
            }

            if ($this->getRequest()->getPost('payment_variants') == 'privat24part') {
                Fenix::redirect('checkout/privat24part');
            }

            if ($this->getRequest()->getPost('pay_variants') == 'liqpay') {
                Fenix::redirect('checkout/liqpay');
            }

            Fenix::getModel('checkout/process')->convertSessionId();
            Fenix::redirect('checkout/success');
        }
        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => 'Оформление заказа',
            'uri'   => Fenix::getUrl('checkout'),
            'id'    => 'order'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();


        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('checkout'),
            'title' => Fenix::lang('Оформление заказа'),
        ));

        $Creator->setLayout('layout-order/layout')
            ->render('checkout/order.phtml');
    }

    public function successAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionIdForSuccess();
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        $products = Fenix::getCollection('checkout/order')->getProducts($orderId);

        //Fenix::getModel('checkout/process')->redirectRefreshSuccess();

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => 'Заказ оформлен',
            'uri'   => Fenix::getUrl('checkout/success'),
            'id'    => 'success'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('checkout/success'),
            'title' => Fenix::lang('Заказ оформлен'),
        ));

        $Creator->getView()->assign(array(
            'orderInfo' => $orderInfo,
            'products'  => $products
        ));
        $Creator->setLayout('layout-order/layout')
            ->render('checkout/success.phtml');
    }

    public function printAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionIdForSuccess();
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        $products = Fenix::getCollection('checkout/order')->getProducts($orderId);

        //Fenix::getModel('checkout/process')->redirectRefreshSuccess();

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => 'Заказ оформлен',
            'uri'   => Fenix::getUrl('checkout/success'),
            'id'    => 'success'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('checkout/success'),
            'title' => Fenix::lang('Заказ оформлен'),
        ));

        $Creator->getView()->assign(array(
            'orderInfo' => $orderInfo,
            'products'  => $products
        ));
        $Creator->setLayout('layout-print/layout')
            ->render('checkout/print.phtml');
    }

    public function storesAction()
    {
        $city = Fenix::getRequest()->getPost('city');
        $productId = Fenix::getRequest()->getPost('product_id');

        if ($city && $productId) {
            $_product = Fenix::getModel('catalog/products')->getProductById($productId);
            $product = Fenix::getCollection('catalog/products_product')->load($_product);
            echo $this->view->partial('catalog/blocks/card-markets.phtml', array(
                'cityName' => $city,
                'product'  => $product
            ));
            exit;
        }
        throw new Exception();
    }

    public function storestocardAction()
    {

        $city = Fenix::getRequest()->getPost('city');

        $size = Fenix::getRequest()->getPost('size');
        $productSku = Fenix::getRequest()->getPost('sku');
        if ($city == null || trim($city) == '') {
            $city = Fenix::getModel('ipcheck/location')->getSelectedCity();
        }
        $city = Fenix::getModel('ipcheck/location')->getSelectedCity();
        if ($city == '') {
            $city = 'Киев';
        }

        $cache = Fenix::getModel('ipcheck/location')->getCache();
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();

        if ($city != null && $city != '') {
            $cache->{'localCity_' . $lang->name} = $city;
        }

        if ($city && $productSku) {
            $_product = Fenix::getModel('catalog/products')->getProductBySku($productSku);

            $product = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());

            echo $this->view->partial('catalog/blocks/card-markets.phtml', array(
                'cityName'    => $city,
                'productSize' => $size,
                'product'     => $product
            ));
            exit;
        }
        throw new Exception();
    }

    public function storestoorderAction()
    {
        $city = Fenix::getRequest()->getPost('city');

//        $city  = Fenix::getRequest()->getParam('city');

        $cache = Fenix::getModel('ipcheck/location')->getCache();
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();

        if ($city != null && $city != '') {
            $cache->{'localCity_' . $lang->name} = $city;
        }

        if ($city) {
            echo $this->view->partial('layout-order/html/stores.phtml');
            exit;
        }

    }
}