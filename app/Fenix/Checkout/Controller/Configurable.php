<?php
class Fenix_Checkout_Controller_Configurable extends Fenix_Controller_Action
{
    public function indexAction()
    {

    }

    public function priceAction(){
        $params = Fenix::getRequest()->getPost('params');

        $priceInfo = Fenix::getModel('catalog/configurable')->getPriceInfo($params);
        if($priceInfo)
            echo json_encode($priceInfo);
        else
            echo json_encode((object)array(
                'error' => 1
            ));
    }
    public function updateAction(){

        $params = Fenix::getRequest()->getPost('params');
        $productId = Fenix::getRequest()->getPost('order_product_id');
        Fenix::getModel('checkout/process')->updateProduct($productId, $params);
        echo 'configurable updated';
    }
}