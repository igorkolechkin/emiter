<?php
class Fenix_Checkout_Controller_Privat24 extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->setLayout('layout-order/layout')
                 ->render('checkout/epay/privat24.phtml');
    }

    public function returnAction()
    {
        parse_str($_POST['payment'], $params);

        if ($params['state'] == 'ok') {

            list($time, $orderId) = explode('-', $params['order']);

            Fenix::getModel('checkout/order')->processEpay(
                (int) $orderId,
                $params
            );

            Fenix::getModel('checkout/process')->convertSessionId();
            Fenix::redirect('checkout/success');
        }
        else {
            Fenix::redirect('checkout/privat24/fail');
        }
    }

    public function failAction()
    {
        Fenix::redirect('checkout');
    }
}