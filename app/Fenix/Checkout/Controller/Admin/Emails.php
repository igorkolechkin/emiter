<?php
class Fenix_Checkout_Controller_Admin_Emails extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('checkoutAll');
    }

    public function indexAction()
    {
        $customerList = Fenix::getModel('catalog/backend_emails')->getEmailListAsSelect();
        $Table        = Fenix::getHelper('catalog/backend_emails')->getTable($customerList);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("База Email"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Title   = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("База Email"))
            ->fetch();

        $Creator ->getView()
            ->headTitle(Fenix::lang("База Email"));

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Event->fetch(),
            $Table->fetch()
        ));

        //Fenix::dump();
    }
}