<?php
class Fenix_Checkout_Controller_Admin_Index extends Fenix_Controller_Action
{

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('checkoutAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('checkout/checkout_orders')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('checkout/checkout_products')
                ->prepare()
                ->execute();
    }

    public function indexAction()
    {
        $orders = Fenix::getModel('checkout/backend_orders')->getOrdersListAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Корзина заказов"));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Корзина заказов"));

        // Хлебные крошки
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Корзина заказов"),
            'uri'   => Fenix::getUrl('checkout'),
            'id'    => 'catalog'
        );
        $this->_helper->BreadCrumbs($_crumb);

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            Fenix::getHelper('checkout/backend_orders')->getOrdersTable($orders)
        ));
    }

    public function viewAction()
    {
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo(
            (int) $this->getRequest()->getParam('id')
        );

        if ($orderInfo === false) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Заказ не найден"))
                ->saveSession();
            Fenix::redirect('checkout');
        }

        if ($this->getRequest()->isPost()) {
            //Редактирование доставки в заказе?
            if ($this->getRequest()->getPost('action') == 'edit-delivery') {
                Fenix::getModel('checkout/backend_orders')->editDelivery(Fenix::getRequest());
            } else {
                //По умолчанию статус и коментарий
                Fenix::getModel('checkout/order')->updateStatus(
                     $this->getRequest()->getPost('status'),
                         $orderInfo->getId()
                );
                Fenix::getModel('checkout/order')->updateComment(
                     $this->getRequest()->getPost('comment'),
                         $orderInfo->getId()
                );
                Fenix::redirect('checkout/');
                exit;
            }
            Fenix::redirect('checkout/view/id/' . $orderInfo->getId());
        }

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

       // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Детали заказа"));

        // Хлебные крошки
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Корзина заказов"),
            'uri'   => Fenix::getUrl('checkout'),
            'id'    => 'checkout'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Детали заказа"),
            'uri'   => Fenix::getUrl('checkout'),
            'id'    => 'view'
        );
        $this->_helper->BreadCrumbs($_crumb);

        $Creator->getView()->assign(array(
            'orderInfo' => $orderInfo
        ));

        $Creator->setLayout()
                ->render('checkout/order.phtml');
    }

    /**
     * Добавляем товар в заказ
     *
     */
    public function addAction(){

        Fenix::getModel('checkout/backend_orders')->addProduct(Fenix::getRequest());

        Fenix::redirect('checkout/view/id/'.Fenix::getRequest()->getParam('order'));
    }
    /**
     * Удаляем товар из заказ
     *
     */
    public function removeAction(){

        Fenix::getModel('checkout/backend_orders')->removeProduct(Fenix::getRequest());

        Fenix::redirect('checkout/view/id/'.Fenix::getRequest()->getParam('order'));
    }
    /**
     * Обновляем товары из заказа
     *
     */
    public function updateAction(){

        Fenix::getModel('checkout/backend_orders')->updateProducts(Fenix::getRequest());

        echo 'update';
        //Fenix::redirect('checkout/view/id/'.Fenix::getRequest()->getParam('order'));
    }

    /**
     * Список email покупателей
     */
    public function exportAction()
    {
        $orderList = Fenix::getModel('checkout/backend_export')->getCheckoutEmailListAsSelect();

        $Table         = Fenix::getHelper('checkout/backend_export')->getTable($orderList);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Список email'ов"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                $Creator->loadPlugin('Button')
                        ->appendClass('btn-warning')
                        ->setValue(Fenix::lang("Экспорт списка"))
                        ->setType('button')
                        ->setOnclick('self.location.href=\'' . Fenix::getUrl('checkout/exportfile') . '\'')
                        ->fetch()
            ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Список email'ов"))
                           ->setButtonSet($Buttonset)
                           ->fetch();

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Список email'ов"));

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Event->fetch(),
            $Table->fetch()
        ));
    }
    public function exportfileAction()
    {
        $excelFile = Fenix::getModel('checkout/backend_export')->exportCheckoutEmailsToExcel();

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="my_objects.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}