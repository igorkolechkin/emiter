<?php
class Fenix_Checkout_Controller_Admin_Stock extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('checkoutAll');
    }
    public function indexAction()
    {
        $customerList = Fenix::getModel('catalog/backend_stock')->getStockListAsSelect();
        $Table        = Fenix::getHelper('catalog/backend_stock')->getTable($customerList);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Сообщить о наличии"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Title   = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Сообщить о наличии"))
            ->fetch();

        $Creator ->getView()
            ->headTitle(Fenix::lang("Сообщить о наличии"));

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Event->fetch(),
            $Table->fetch()
        ));

        //Fenix::dump();
    }
}