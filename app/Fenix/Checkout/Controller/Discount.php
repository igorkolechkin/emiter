<?php
class Fenix_Checkout_Controller_Discount extends Fenix_Controller_Action
{
    public function indexAction()
    {

        $discountCard = Fenix::getModel('catalog/backend_discount')->getDiscountCard(
            $this->getRequest()->getPost('card')
        );

        if ($discountCard == null) {
            Fenix::getModel('checkout/process')->removeDiscountCard();
            echo Fenix::getCreatorUI()->loadPlugin('Events')
                    ->appendClass(Creator_Events::TYPE_ERROR)
                    ->setMessage('Дисконтная карта не найдена')
                    ->fetch();
        }
        else {

            Fenix::getModel('checkout/process')->saveDiscountCard(
                $discountCard
            );
            //Fenix::getModel('checkout/process')->removeBonus();

            echo Fenix::getCreatorUI()->loadPlugin('Events')
                ->appendClass(Creator_Events::TYPE_OK)
                ->setMessage('Скидка по дисконтной карте пересчитана')
                ->fetch();
        }
        $this->view->assign(array(
            'discount' => $discountCard,
            'delivery'=>$this->getRequest()->getPost('delivery')
        ));
        echo $this->view->render('checkout/order-summary.phtml');
    }
}