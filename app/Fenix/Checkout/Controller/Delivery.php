<?php
class Fenix_Checkout_Controller_Delivery extends Fenix_Controller_Action
{
    public function indexAction()
    {
        throw new Exception();
    }

    public function novaposhtaAction()
    {
        switch ($this->getRequest()->getQuery('action')) {
            case 'warehouse':
                $list = Fenix::getModel('checkout/delivery_novaposhta')->getWarehouseList(
                    $this->getRequest()->getQuery('city')
                );

                echo Zend_Json::encode($list->getData());
            break;
        }

        exit;
    }
}