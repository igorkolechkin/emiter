<?php
class Fenix_Checkout_Controller_Liqpay extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $orderId   = Fenix::getModel('checkout/process')->getSessionIdForSuccess();
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo();
        //Fenix::dump($orderInfo);
        $products  = Fenix::getCollection('checkout/order')->getProducts($orderInfo->id);

        $Creator = Fenix::getCreatorUI();
        $Creator ->getView()->assign(array(
            'orderInfo' => $orderInfo,
            'products'  => $products
        ));
        $Creator = Fenix::getCreatorUI();
        $Creator ->setLayout()
                 ->render('checkout/epay/liqpay.phtml');
    }

    public function returnAction()
    {
        parse_str($_POST['payment'], $params);

        if ($params['state'] == 'ok') {

            list($time, $orderId) = explode('-', $params['order']);

            Fenix::getModel('checkout/order')->processEpay(
                (int) $orderId,
                $params
            );

            Fenix::getModel('checkout/process')->convertSessionId();
            Fenix::redirect('checkout/success');
        }
        else {
            Fenix::redirect('checkout/privat24/fail');
        }
    }

    public function serverAction()
    {
        $order = Fenix::getRequest()->getParam('order');
        if ($order && $order != '' /*&& Fenix::getRequest()->getPost()*/) {
            Fenix::getModel('checkout/liqpay')->liqpayRespond($order);
            echo 'ok';
            exit;
        }
        echo 'error';
    }

    public function failAction()
    {
        Fenix::redirect('checkout');
    }
}