<?php

class Fenix_Checkout_Controller_Process extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        Fenix::getModel('checkout/order')->updateProductsPrice($orderId);

        echo $this->view->partial('checkout/table.phtml', array());
    }

    public function orderAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        Fenix::getModel('checkout/order')->updateProductsPrice($orderId);

        echo $this->view->partial('checkout/table-info.phtml', array());
    }

    public function sidebarAction()
    {
        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        Fenix::getModel('checkout/order')->updateProductsPrice($orderId);

        echo $this->view->partial('checkout/sidebar.phtml', array());
    }

    public function buyblockAction()
    {

        $orderId = Fenix::getModel('checkout/process')->getSessionId();
        $_product = Fenix::getModel('catalog/products')->getProductById(Fenix::getRequest()->getParam('id'));
        $product = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        Fenix::getModel('checkout/order')->updateProductsPrice($orderId);

        echo $this->view->partial('catalog/products/buy-block.phtml', array(
            'product' => $product
        ));
    }

    public function addAction()
    {
        $productId = (int) $this->getRequest()->getParam('pid');
        $product = Fenix::getModel('catalog/products')->getProductById($productId);
        $params = $this->getRequest()->getPost();
        $set = Fenix::getModel('catalog/products_set')->getSetById($this->getRequest()->getParam('set'));

        if ($product == null) {
            throw new Exception();
        }

        Fenix::getModel('checkout/process')->addProduct(
            $product,
            (string) $this->getRequest()->getParam('type'),
            $params,
            $set
        );
        exit;
    }

    public function updateAction()
    {
        $result = Fenix::getModel('checkout/process')->updateProducts(
            $this->getRequest()
        );
        echo $result;
        exit;
    }

    public function qtyAction()
    {
        Fenix::getModel('checkout/process')->updateProductQty(
            $this->getRequest()
        );
        exit;
    }

    public function removeAction()
    {
        Fenix::getModel('checkout/process')->removeProduct(
            $this->getRequest()
        );
        exit;
    }

    public function storeAction()
    {
        Fenix::getModel('checkout/order')->setStore(
            $this->getRequest()
        );
        exit;
    }

    public function oneclickAction()
    {
        $Form = Fenix::getHelper('checkout/forms')->getBuyOneClickForm();

        if ($Form->ok()) {
            Fenix::getModel('checkout/process')->oneClickBuy(
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Спасибо! Наш менеджер свяжется с Вами в ближайшее время."))
                ->saveSession();

            $res['success_msg'] = Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();
        }
        else {
            $res['error'] = $Form->fetch();
        }

        echo Zend_Json::encode($res);
        exit;
    }

    public function fastorderAction()
    {
        Fenix::getModel('checkout/process')->fastOrder(
            $this->getRequest()
        );
        Fenix::getModel('checkout/process')->convertSessionId();

        $res['success_msg'] = Fenix::getCreatorUI()
            ->loadPlugin('Events')
            ->setType(Creator_Events::TYPE_OK)
            ->setClass('alert fade in alert-success')
            ->setMessage(Fenix::lang("Спасибо ! Наш менеджер свяжется с Вами в ближайшее время"))->fetch();

        echo Zend_Json::encode($res);

    }
}