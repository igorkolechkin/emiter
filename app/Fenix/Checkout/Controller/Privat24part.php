<?php

class Fenix_Checkout_Controller_Privat24part extends Fenix_Controller_Action
{
    public function indexAction()
    {
        //Fenix::redirect('/');
        //$str = unserialize('s:168:"{"orderId":"15492175","paymentState":"CANCELED","signature":"TBODQv49UC5WULpL7LJb5qSA9Us=","message":"Client hasn\'t completed payment","storeId":"5F6B4F0A3E8E4307B273"}";');
        //$str = unserialize('s:329:"Тестовая операция успешно проведена5F6B4F0A3E8E4307B27315492192SUCCESS7Dg5lq4HgALpdb2IPVTagUNhYb0=";');
        //Fenix::dump(json_decode($str));
        $Creator = Fenix::getCreatorUI();

        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo();

        $paymentUrl = null;

        $privatApi = new Fenix_Api_Privat24part($orderInfo, Fenix_Api_Privat24part::MERCHANT_TYPE_PARTS);
        $createNewCheckout = true;


        /* if ($orderInfo->getToken() != '' && $orderInfo->getTokenTime() != '0000-00-00 00:00:00') {
             $timeThen   = new Zend_Date($orderInfo->getTokenTime());
             $timeNow    = new Zend_Date();
             $difference = $timeNow->sub($timeThen);
             $measure = new Zend_Measure_Time($difference->toValue(), Zend_Measure_Time::SECOND);
             $measure->convertTo(Zend_Measure_Time::MINUTE);
             if ($measure->getValue() < 15) {
                 $createNewCheckout = false;
             }
         }*/
        if ($createNewCheckout) {
            Fenix::getModel('checkout/privat24part')->nextToken($orderInfo);
            $respondXml = $privatApi->sendOrderInfo();


            /*Fenix::dump($orderInfo->getToken() != '' && $orderInfo->getTokenTime() != '0000-00-00 00:00:00',
                $orderInfo->getToken(), $orderInfo->getTokenTime(),$respondXml
            );*/
            if (
                //$privat24part->checkOrderRespondSignature($respondXml) &&
                $respondXml->state == Fenix_Api_Privat24part::STATUS_SUCCESS
            ) {
                $paymentUrl = $privatApi->getPaymentUrl($respondXml);
                Fenix::getModel('checkout/privat24part')->saveToken($orderInfo, $respondXml);
            }
            else {
                Fenix::dump($respondXml);
                $url = Fenix::getUrl();

                if ($_SERVER['REMOTE_ADDR'] == "91.193.69.155") {
                    Fenix::dump($respondXml);
                }
                else {
                    echo '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                }

                //echo 'Сервис временно не доступен. Попробуйте еще раз через несколько минут.';
                exit;
            }
        }
        else {
            $paymentUrl = $privatApi->getPaymentUrlByToken($orderInfo->token);;
        }


        /*if ($_SERVER['REMOTE_ADDR']=="91.193.69.155"){
        Fenix::dump($order->token);
        }

        if($order->token=='')
        {
            $respondXml = $privat24part->sendOrderInfo();
            if (
                //$privat24part->checkOrderRespondSignature($respondXml) &&
                $respondXml->state == Fenix_Api_Privat24part::STATUS_SUCCESS
            ){
                $paymentUrl = $privat24part->getPaymentUrl($respondXml);

                Fenix::getModel('checkout/privat24part')->saveToken($order, $respondXml);
            }
            else{

                //Fenix::dump($respondXml);
            }
        }
        else{
            $paymentUrl =  $privat24part->getPaymentUrlByToken($order->token);;
        }*/

        //Fenix::dump($privat24part->checkSignature('E5iIL6j06B8U4N4NpMzKkbJjO5M='));
        //$xml = $privat24par->sendOrderInfo();
        //Fenix::dump($xml);
        //Fenix::dump($xml);
//        Fenix::dump($privat24par->getXml());

//        Fenix::getModel('core/mail')->sendMail('phppro80@gmail.com',  'no-reply@' . $_SERVER['HTTP_HOST'],'privat23part', serialize(Fenix::getRequest()->getPost()));

        $Creator->getView()->assign(array(
            'paymenUrl' => $paymentUrl
        ));
        $Creator->setLayout('layout-order/layout')
            ->render('checkout/epay/privat24part.phtml');
    }

    public function topaymentAction()
    {

    }

    public function returnAction()
    {

        Fenix::getModel('core/mail')->sendMail('phppro80@gmail.com', 'no-reply@' . $_SERVER['HTTP_HOST'], 'privat24part return',
            serialize(Fenix::getRequest()->getPost()) . '|' . serialize($GLOBALS['HTTP_RAW_POST_DATA'])
        );
        /*
            parse_str($_POST['payment'], $params);

            if ($params['state'] == 'ok') {

                list($time, $orderId) = explode('-', $params['order']);

                Fenix::getModel('checkout/order')->processEpay(
                    (int) $orderId,
                    $params
                );
        */
        Fenix::getModel('checkout/process')->convertSessionId();
        Fenix::redirect('checkout/success');
        /* }
         else {
             Fenix::redirect('checkout/privat24part/fail');
         }*/
    }

    public function failAction()
    {
        Fenix::redirect('checkout');
    }

    public function serverAction()
    {

        Fenix::getModel('core/mail')->sendMail('phppro80@gmail.com', 'no-reply@' . $_SERVER['HTTP_HOST'], 'privat24part server',
            serialize(Fenix::getRequest()->getPost()) . '|' . '<pre>' . serialize(str_replace(array('<', '>'), array('[', ']'), $GLOBALS['HTTP_RAW_POST_DATA'])) . '</pre>'
        );

        $respondXMLClear = $GLOBALS['HTTP_RAW_POST_DATA'];
        $respondXML = new Zend_Config_Xml($GLOBALS['HTTP_RAW_POST_DATA']);

        Fenix::getModel('checkout/privat24part')->serverRespond($respondXML, $respondXMLClear);

    }
}