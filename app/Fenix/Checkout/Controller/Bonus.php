<?php
class Fenix_Checkout_Controller_Bonus extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $User    = Fenix::getModel('session/auth')->getUser();
        if (!$User instanceof Zend_Db_Table_Row) exit;


        $useBonus = $this->getRequest()->getPost('bonus');

        Fenix::getModel('checkout/process')->removeCoupon();

        $total = Fenix::getModel('checkout/order')->getTotalPrice();

        if ($User->bonus_money >= $useBonus) {


            if ($useBonus < $total / 2) {
                $result = Fenix::getModel('checkout/process')->setBonus($useBonus);

                echo Fenix::getCreatorUI()->loadPlugin('Events')
                    ->appendClass(Creator_Events::TYPE_OK)
                    ->setMessage('Бонусы использованы в расчете стоимости заказа')
                    ->fetch();
            } else {

                $result = Fenix::getModel('checkout/process')->setBonus(0);

                echo Fenix::getCreatorUI()->loadPlugin('Events')
                    ->appendClass(Creator_Events::TYPE_ERROR)
                    ->setMessage('Количесвто бонусов не может превышать 50% стоимости заказа')
                    ->fetch();
            }
        } else {
            Fenix::getModel('checkout/process')->setBonus(0);
            echo Fenix::getCreatorUI()->loadPlugin('Events')
                ->appendClass(Creator_Events::TYPE_ERROR)
                ->setMessage('У Вас недостаточно бонусов')
                ->fetch();
        }


        echo $this->view->partial('checkout/table.phtml', array('type' => 'small'));
    }
}