<?php
class Fenix_Checkout_Controller_Coupon extends Fenix_Controller_Action
{
    public function indexAction()
    {

        $coupon = Fenix::getModel('catalog/backend_coupons')->getActiveCoupon(
            $this->getRequest()->getPost('coupon_id')
        );

        if ($coupon == null) {
            echo Fenix::getCreatorUI()->loadPlugin('Events')
                    ->appendClass(Creator_Events::TYPE_ERROR)
                    ->setMessage('Купон не найден')
                    ->fetch();
        }
        else {

            Fenix::getModel('checkout/process')->saveCoupon(
                $coupon
            );

            echo Fenix::getCreatorUI()->loadPlugin('Events')
                ->appendClass(Creator_Events::TYPE_OK)
                ->setMessage('Купон применён')
                ->fetch();

            echo $coupon->details;
        }

        //echo $this->view->partial('checkout/table.phtml', array('type' => 'small'));
    }
}