<?php

class Fenix_Feedback_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {

    }

    public function callbackAction()
    {
        // Форма сообщения
        /* дополнил возможность создания одинаковых форм с разными id => unique_id */
        $Form = Fenix::getHelper('feedback/forms')->getCallbackForm();

        if ($Form->ok()) {
            $messages = Fenix::getModel('core/mail')->getDefaultMessages();

            $mailResult = Fenix::getModel('core/mail')->sendAdminMail(array(
                'template' => 'callback', // шаблон письма
                'assign'   => $Form->getRequest()->getPost(),
            ));

            if ($mailResult) {
                $res['success_msg'] = $messages['success_msg'];
            }
            else {
                $res['error_msg'] = $messages['error_msg'];
            }
        }
        else {
            $res['error'] = $Form->fetch();
        }

        echo Zend_Json::encode($res);
    }

    public function feedbackformAction()
    {
        // Форма сообщения
        /* дополнил возможность создания одинаковых форм с разными id => unique_id */
        $Form = Fenix::getHelper('feedback/forms')->getFeedBackForm();

        if ($Form->ok()) {
            $messages = Fenix::getModel('core/mail')->getDefaultMessages();

            $mailResult = Fenix::getModel('core/mail')->sendAdminMail(array(
                'template' => 'feedback', // шаблон письма
                'assign'   => $Form->getRequest()->getPost(),
            ));

            if ($mailResult) {
                $res['success_msg'] = $messages['success_msg'];
            }
            else {
                $res['error_msg'] = $messages['error_msg'];
            }
        }
        else {
            $res['error'] = $Form->fetch();
        }

        echo Zend_Json::encode($res);
    }

    public function messageAction()
    {
        // Форма сообщения
        /* дополнил возможность создания одинаковых форм с разными id => unique_id */
        $Form = Fenix::getHelper('feedback/forms')->getMessageForm();

        if ($Form->ok()) {
            $messages = Fenix::getModel('core/mail')->getDefaultMessages();

            $mailResult = Fenix::getModel('core/mail')->sendAdminMail(array(
                'template' => 'feedback', // шаблон письма
                'assign'   => $Form->getRequest()->getPost(),
            ));

            if ($mailResult) {
                $res['success_msg'] = $messages['success_msg'];
            } else {
                $res['error_msg'] = $messages['error_msg'];
            }
        } else {
            $res['error'] = $Form->fetch();
        }

        echo Zend_Json::encode($res);
    }
}