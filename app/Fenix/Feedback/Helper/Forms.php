<?php

class Fenix_Feedback_Helper_Forms extends Fenix_Resource_Helper
{
    /**
     *   Форма обратной связи
     */
    static public function getCallbackForm()
    {
        /* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
        $unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');


        $Form = Fenix::getCreatorUI()->loadPlugin('Form');
        $form_id = 'callbackForm_' . $unique_id;
        $Form->containerClass = 'callbackForm-container';
        $Form->unique_id = $unique_id;
        $Form->reset = true;

        $Form->setClass('callback-form')
            ->setId($form_id)
            ->setAction('/feedback/callback')
            ->enableAjax();

        $Content = [];

        // Ваше имя
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("firstname")
            ->setId("firstname")
            ->setValue($Form->getRequest()->getPost('firstname'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang('Ваше имя'))
            ->setClass('form-control firstname')
            ->fetch();

        $Content[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang("Ваше имя") . ':<span class="required"></span>')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();
        // Телефон
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("cellphone")
            ->setId("cellphone")
            ->setValue($Form->getRequest()->getPost('cellphone'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang('Телефон'))
            ->setClass('form-control cellphone')
            //->setDetails(Fenix::lang('*Для подтверждения заказа и уточнения деталей'))
            ->fetch();

        $Content[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang("Телефон") . ':<span class="required"></span>')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();
        // Кнопка
        // Удобное время
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("time_info")
            ->setId("time_info")
            ->setValue($Form->getRequest()->getPost('time_info'))
            ->setClass('form-control time_info')
            ->fetch();

        $Content[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang("Если нужно, укажите, пожалуйста, время удобное для звонка") . ':')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();

        // Кнопка
        $Content[] = $Form->loadPlugin('Button')
            ->setType("submit")
            ->setClass('btn btn-primary')
            ->setValue('Отправить')
            ->fetch();

        $Form->setContent($Content);
        $Form->compile();

        return $Form;
    }

    /**
     *  Пользовательская форма. В тексте должна содержаться "переменная" [$feedBackForm]
     *  Пример использования:
     *      // ассоциируем переменную feedBackForm с формой
     *      Fenix_Smarty::getInstance()->assign('feedBackForm',
     *      Fenix::getHelper('feedback/forms')->getFeedBackForm()->fetch());
     *      // выводим текстовый блок с обработкой данных
     *      echo Fenix_Smarty::getInstance()->fetchString($this->current->getContent(), true);
     */
    static public function getFeedBackForm($unique_id = false)
    {
        /* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
        $unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

        $Form = Fenix::getCreatorUI()
            ->loadPlugin('Form');
        $form_id = 'feedBackForm_' . $unique_id;
        $Form->containerClass = 'feedBackForm-container';
        $Form->unique_id = $unique_id;
        $Form->reset = true;

        $Form->setClass('feedBack-form')
            ->setId($form_id)
            ->setAction('/feedback/feedbackform')
            ->enableAjax();

        $column = array();
        $columnLeft = array();
        $columnRight = array();
        $column[] = $Form->loadPlugin('Row')
            ->setContent("<div class='form-title'>" . Fenix::lang('Написать нам') . "</div>")
            ->fetch();
        $Content[] = $Form->loadPlugin('Container')
            ->setClass('column')
            ->setContent($column)
            ->fetch();
        $column = array();


        // Как к вам обращаться?
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("firstname")
            ->setId("firstname")
            ->setValue($Form->getRequest()->getPost('firstname'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang('ФИО'))
            ->setClass('form-control firstname')
            ->fetch();
        $column[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang('ФИО') . '<span class="required"></span>')
            ->setContent($Field)
            ->fetch();

        // email
        $Field = $Form->loadPlugin('Form_Text')
            ->setType("email")
            ->setName("email")
            ->setId("email")
            ->setValue($Form->getRequest()->getPost('email'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
            ->setPlaceholder(Fenix::lang('Email'))
            ->setClass('form-control email')
            ->fetch();
        $column[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang('Email') . ':<span class="required"></span>')
            ->setContent($Field)
            ->fetch();

        /*$Content[] = $Form->loadPlugin('Container')
                          ->setClass('left-column')
                          ->setContent($columnLeft)
                          ->fetch();
        $Content[] = $Form->loadPlugin('Container')
                          ->setClass('right-column')
                          ->setContent($columnRight)
                          ->fetch();*/

        // текст вашего сообщения
        $Field = $Form->loadPlugin('Form_Textarea')
            ->setName("message")
            ->setId("message")
            ->setValue($Form->getRequest()->getPost('message'))
            ->setPlaceholder(Fenix::lang('Здесь вы можете задать свой вопрос или написать свои пожелания'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setClass('form-control time_info')
            ->fetch();
        $column[] = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang('Сообщение') . ':')
            ->setContent($Field)
            ->fetch();

        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("unique_id")
            ->setType("hidden")
            ->setValue($unique_id)
            ->fetch();

        // Кнопка
        $column[] = $Form->loadPlugin('Container')
            ->setContent('<button class="btn green" type="submit" value="Отправить">' . Fenix::lang('Отправить') . '</button>')
            ->setClass('btt')
            ->fetch();


        $Content[] = $Form->loadPlugin('Container')
            ->setClass('column')
            ->setContent($column)
            ->fetch();

        $Form->setContent($Content);
        $Form->compile();

        return $Form;
    }

    static public function getMessageForm($unique_id = false)
    {
        /* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
        $unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

        $Form = Fenix::getCreatorUI()
            ->loadPlugin('Form');
        $form_id = 'feedBackForm_' . $unique_id;
        $Form->unique_id = $unique_id;
        $Form->reset = true;

        $Form->setId($form_id)
            ->setAction('/feedback/message')
            ->setClass('form-inline form-message')
            ->enableAjax();

        $column = array();

        // Как к вам обращаться?
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("firstname")
            ->setId("firstname")
            ->setValue($Form->getRequest()->getPost('firstname'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang('Ваше имя'))
            ->setClass('form-control form-control-lg')
            ->fetch();

        $column[] = $Form->loadPlugin('Container')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();


        // email
        $Field = $Form->loadPlugin('Form_Text')
            ->setType("email")
            ->setName("email")
            ->setId("email")
            ->setValue($Form->getRequest()->getPost('email'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
            ->setPlaceholder(Fenix::lang('Ваш Email'))
            ->setClass('form-control form-control-lg')
            ->fetch();

        $column[] = $Form->loadPlugin('Container')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();

        // Телефон
        $Field = $Form->loadPlugin('Form_Text')
            ->setName("cellphone")
            ->setId("cellphone")
            ->setValue($Form->getRequest()->getPost('cellphone'))
            ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
            ->setPlaceholder(Fenix::lang('Телефон'))
            ->setClass('form-control form-control-lg')
            //->setDetails(Fenix::lang('*Для подтверждения заказа и уточнения деталей'))
            ->fetch();

        $column[] = $Form->loadPlugin('Container')
            ->setContent($Field)
            ->setClass('form-group')
            ->fetch();

        $column[] = $Form->loadPlugin('Form_Text')
            ->setName("unique_id")
            ->setType("hidden")
            ->setValue($unique_id)
            ->fetch();

        // Кнопка
        $column[] = $Form->loadPlugin('Button')
            ->setType("submit")
            ->setClass('btn btn-primary btn-lg')
            ->setValue(Fenix::lang('Отправить'))
            ->fetch();

        $Form->setContent($column);
        $Form->compile();

        return $Form;
    }
}