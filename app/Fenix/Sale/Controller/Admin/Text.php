<?php
class Fenix_Sale_Controller_Admin_Text extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('sale/sale_text')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_text_categories')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_text_products')
                ->prepare()
                ->execute();
    }

    /**
     * Управление текстами
     */
    public function indexAction()
    {

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentText = Fenix::getModel('sale/backend_text')->getTextById($_rowId);
                    Fenix::getModel('sale/backend_text')->deleteText($currentText);
                }

                Fenix::redirect('sale');
            }
        }

        $textList  = Fenix::getModel('sale/backend_text')->getTextAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('sale/text/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый текст"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('sale/text/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Текст в товарах"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Текст в товарах"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление текстами"))
            ->setData($textList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('begin_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('end_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });


        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('sale/text/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('sale/text/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Блоки"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('sale/sale_text')
        ));
    }

    /**
     * Новый текст
     */
    public function addAction()
    {

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Текст в товарах"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale/text')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'last_date' => date('Y-m-d'),
            'last_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Источник

        $Form      ->setSource('sale/sale_text', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('sale/backend_text')->addText($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Блок создан"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/text');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/text/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/text/edit/attributeset/' . $attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale/text');
            exit;
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый текст"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать скидку
     */
    public function editAction()
    {
        $currentText = Fenix::getModel('sale/backend_text')->getTextById(
            $this->getRequest()->getParam('id')
        );

        if ($currentText == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Блок не найдена"))
                ->saveSession();

            Fenix::redirect('sale/text');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Текст в товарах"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale/text')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentText->toArray();

        list($createDate, $createTime) = explode(' ', $currentText->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        list($beginDate, $beginTime) = explode(' ', $currentText->begin_date);

        $Defaults['begin_date'] = $beginDate;
        $Defaults['begin_time'] = $beginTime;

        list($endDate, $endTime) = explode(' ', $currentText->end_date);

        $Defaults['end_date'] = $endDate;
        $Defaults['end_time'] = $endTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentText);

        // Источник
        $Form      ->setSource('sale/sale_text', $currentText->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('sale/backend_text')->editText($Form, $currentText, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Блок от редактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/text');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/text/add/attributeset/' . $currentText->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/text/edit/attributeset/' . $currentText->attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale/text');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать текст"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить скидку
     */
    public function deleteAction()
    {
        $currentText = Fenix::getModel('sale/backend_text')->getTextById(
            $this->getRequest()->getParam('id')
        );

        if ($currentText == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Блок не найдена"))
                ->saveSession();

            Fenix::redirect('sale/text');
        }

        Fenix::getModel('sale/backend_text')->deleteText($currentText);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Блок удален"))
            ->saveSession();

        Fenix::redirect('sale/text');
    }
}