<?php
class Fenix_Sale_Controller_Admin_Index extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('sale/sale')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_categories')
                ->prepare()
                ->execute();
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_categories_view')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/relations')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/rubric')
                ->prepare()
                ->execute();

        //Привязки к поставщикам
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/suppliers_relations')
                ->prepare()
                ->execute();

        //Привязки к товарам
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_products')
                ->prepare()
                ->execute();

        //Привязки к товарам
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_products_from')
                ->prepare()
                ->execute();


        //История скидок товаров
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_discount')
                ->prepare()
                ->execute();
    }

    /**
     * Управление скидками
     */
    public function indexAction()
    {


        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentRecord = Fenix::getModel('sale/backend_sale')->getRecordById($_rowId);
                    Fenix::getModel('sale/backend_sale')->deleteRecord($currentRecord);
                }

                Fenix::redirect('sale');
            }
        }

        $saleList  = Fenix::getModel('sale/backend_sale')->getSaleAsSelect();
        //$saleList->where('is_active = ?', Fenix::IS_ACTIVE);

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('sale/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая скидка"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('sale/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Скидки"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Скидки"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление скидками"))
            ->setData($saleList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('begin_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('end_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });


        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('sale/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('sale/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Скидки"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('sale/sale')
        ));
    }

    /**
     * Управление скидками
     */
    public function allAction()
    {


        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentRecord = Fenix::getModel('sale/backend_sale')->getRecordById($_rowId);
                    Fenix::getModel('sale/backend_sale')->deleteRecord($currentRecord);
                }

                Fenix::redirect('sale');
            }
        }


        $saleList  = Fenix::getModel('sale/backend_sale')->getSaleAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('sale/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая скидка"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('sale/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Скидки"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Скидки"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('pagesList')
                           ->setTitle(Fenix::lang("Управление скидками"))
                           ->setData($saleList)
                           ->setCheckall()
                           ->setStandartButtonset()
                           ->setCellCallback('create_date', function($value) {
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('modify_date', function($value){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('begin_date', function($value){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('end_date', function($value){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('sale/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('sale/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Скидки"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('sale/sale')
        ));
    }

    /**
     * Новая скидка
     */
    public function addAction()
    {
        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Скидки"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'create_date' => date('Y-m-d'),
            'create_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('sale/sale', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('sale/backend_sale')->addRecord($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Скидкасоздана"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/edit/attributeset/' . $attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая скидка"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать скидку
     */
    public function editAction()
    {
        $currentRecord = Fenix::getModel('sale/backend_sale')->getRecordById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRecord == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Скидка не найдена"))
                ->saveSession();

            Fenix::redirect('sale');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Скидки"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentRecord->toArray();

        list($createDate, $createTime) = explode(' ', $currentRecord->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        list($beginDate, $beginTime) = explode(' ', $currentRecord->begin_date);

        $Defaults['begin_date'] = $beginDate;
        $Defaults['begin_time'] = $beginTime;

        list($endDate, $endTime) = explode(' ', $currentRecord->end_date);

        $Defaults['end_date'] = $endDate;
        $Defaults['end_time'] = $endTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentRecord);

        // Источник
        $Form      ->setSource('sale/sale', $currentRecord->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('sale/backend_sale')->editRecord($Form, $currentRecord, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Скидка от редактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/add/attributeset/' . $currentRecord->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/edit/attributeset/' . $currentRecord->attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать скидку"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить скидку
     */
    public function deleteAction()
    {
        $currentRecord = Fenix::getModel('sale/backend_sale')->getRecordById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRecord == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Скидка не найдена"))
                ->saveSession();

            Fenix::redirect('sale');
        }

        Fenix::getModel('sale/backend_sale')->deleteRecord($currentRecord);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Скидкаудалена"))
            ->saveSession();

        Fenix::redirect('sale');
    }
}