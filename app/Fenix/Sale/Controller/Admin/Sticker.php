<?php
class Fenix_Sale_Controller_Admin_Sticker extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('sale/sale_sticker')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_sticker_categories')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_sticker_products')
                ->prepare()
                ->execute();
    }

    /**
     * Управление текстами
     */
    public function indexAction()
    {

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentSticker = Fenix::getModel('sale/backend_sticker')->getStickerById($_rowId);
                    Fenix::getModel('sale/backend_sticker')->deleteSticker($currentSticker);
                }

                Fenix::redirect('sale');
            }
        }

        $stickerList  = Fenix::getModel('sale/backend_sticker')->getStickerAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('sale/sticker/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая наклейка"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('sale/sticker/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Наклейки товара"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Наклейки товара"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление наклейками"))
            ->setData($stickerList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('begin_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('end_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('image', function ($value, $data, $column, $table) {
                if ($data->image == null || $data->image == '')
                    return;

                $imageUrl = Fenix_Image::resize($data->image, 50, 50);
                return '<img src="' . $imageUrl . '" width="50" alt="" />';
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('sale/sticker/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('sale/sticker/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Наклейки"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('sale/sale_sticker')
        ));
    }

    /**
     * Новый текст
     */
    public function addAction()
    {

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Наклейки товара"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale/sticker')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'last_date' => date('Y-m-d'),
            'last_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Источник

        $Form      ->setSource('sale/sale_sticker', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('sale/backend_sticker')->addSticker($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Наклейка создана"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/sticker');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/sticker/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/sticker/edit/attributeset/' . $attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale/sticker');
            exit;
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая наклейка"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать скидку
     */
    public function editAction()
    {
        $currentSticker = Fenix::getModel('sale/backend_sticker')->getStickerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSticker == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Наклейка не найдена"))
                ->saveSession();

            Fenix::redirect('sale/sticker');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Наклейки товара"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale/sticker')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentSticker->toArray();

        list($createDate, $createTime) = explode(' ', $currentSticker->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        list($beginDate, $beginTime) = explode(' ', $currentSticker->begin_date);

        $Defaults['begin_date'] = $beginDate;
        $Defaults['begin_time'] = $beginTime;

        list($endDate, $endTime) = explode(' ', $currentSticker->end_date);

        $Defaults['end_date'] = $endDate;
        $Defaults['end_time'] = $endTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentSticker);

        // Источник
        $Form      ->setSource('sale/sale_sticker', $currentSticker->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('sale/backend_sticker')->editSticker($Form, $currentSticker, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Наклейка от редактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/sticker');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/sticker/add/attributeset/' . $currentSticker->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/sticker/edit/attributeset/' . $currentSticker->attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale/sticker');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать наклейку"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить скидку
     */
    public function deleteAction()
    {
        $currentSticker = Fenix::getModel('sale/backend_sticker')->getStickerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSticker == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Наклейка не найдена"))
                ->saveSession();

            Fenix::redirect('sale/sticker');
        }

        Fenix::getModel('sale/backend_sticker')->deleteSticker($currentSticker);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Наклейка удален"))
            ->saveSession();

        Fenix::redirect('sale/sticker');
    }
}