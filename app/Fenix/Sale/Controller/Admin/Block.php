<?php
class Fenix_Sale_Controller_Admin_Block extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('sale/sale_block')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('sale/sale_block_categories')
                ->prepare()
                ->execute();
    }

    /**
     * Управление блоками
     */
    public function indexAction()
    {

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentBlock = Fenix::getModel('sale/backend_block')->getBlockById($_rowId);
                    Fenix::getModel('sale/backend_block')->deleteBlock($currentBlock);
                }

                Fenix::redirect('sale');
            }
        }

        $blockList  = Fenix::getModel('sale/backend_block')->getBlockAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('sale/block/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый блок"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('sale/block/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Блоки в каталоге"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Блоки в каталоге"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление блоками"))
            ->setData($blockList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('begin_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('end_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });


        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('sale/block/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('sale/block/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Блоки"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('sale/sale_block')
        ));
    }

    /**
     * Новый блок
     */
    public function addAction()
    {

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Блоки в каталоге"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale/block')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'last_date' => date('Y-m-d'),
            'last_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Источник

        $Form      ->setSource('sale/sale_block', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('sale/backend_block')->addBlock($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Блок создан"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/block');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/block/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/block/edit/attributeset/' . $attributeset . '/id/' . $id);
            }
Fenix::dump('asdf');
            Fenix::redirect('sale/block');
            exit;
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый блок"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать скидку
     */
    public function editAction()
    {
        $currentBlock = Fenix::getModel('sale/backend_block')->getBlockById(
            $this->getRequest()->getParam('id')
        );

        if ($currentBlock == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Блок не найдена"))
                ->saveSession();

            Fenix::redirect('sale/block');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Блоки в каталоге"),
            'id'    => 'sale',
            'uri'   => Fenix::getUrl('sale')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentBlock->toArray();

        list($createDate, $createTime) = explode(' ', $currentBlock->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        list($beginDate, $beginTime) = explode(' ', $currentBlock->begin_date);

        $Defaults['begin_date'] = $beginDate;
        $Defaults['begin_time'] = $beginTime;

        list($endDate, $endTime) = explode(' ', $currentBlock->end_date);

        $Defaults['end_date'] = $endDate;
        $Defaults['end_time'] = $endTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentBlock);

        // Источник
        $Form      ->setSource('sale/sale_block', $currentBlock->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('sale/backend_block')->editBlock($Form, $currentBlock, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Блок от редактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('sale/block');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('sale/block/add/attributeset/' . $currentBlock->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('sale/block/edit/attributeset/' . $currentBlock->attributeset . '/id/' . $id);
            }

            Fenix::redirect('sale/block');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать блок"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить скидку
     */
    public function deleteAction()
    {
        $currentBlock = Fenix::getModel('sale/backend_block')->getBlockById(
            $this->getRequest()->getParam('id')
        );

        if ($currentBlock == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Блок не найдена"))
                ->saveSession();

            Fenix::redirect('sale/block');
        }

        Fenix::getModel('sale/backend_block')->deleteBlock($currentBlock);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Блок удален"))
            ->saveSession();

        Fenix::redirect('sale/block');
    }
}