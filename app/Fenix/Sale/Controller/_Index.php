<?php
class Fenix_Sale_Controller_Index extends Fenix_Controller_Action
{
    /**
     * Роутинг статей
     *
     * @throws Exception
     */
    public function preDispatch()
    {
        $first  = $this->getRequest()->getUrlSegment(1);
        $second = $this->getRequest()->getUrlSegment(2);

        if ($first != null && $second != null) {
            $rubric  = Fenix::getModel('sale/rubric')->getRubric($first);
            if ($rubric == null) {
                //Fenix::dump($rubric);
                throw new Exception();
            }
            else {
                //Fenix::dump($rubric);
                $Block = Fenix::getModel('sale/sale_block')->getBlock($second, $rubric);
                //Fenix::dump($Block);
                if ($Block == null) {
                    throw new Exception();
                }
                else {
                    $this->BlockAction($Block, $rubric);
                }
            }
        }
        else {
            if ($first != null && $second == null) {

                $Block = Fenix::getModel('sale/block')->getBlock($first);
                if ($Block == null) {
                    $rubric = Fenix::getModel('sale/rubric')->getRubric($first);
                    if ($rubric == null) {
                        throw new Exception();
                    }
                    else {
                        $this->_indexAction($rubric);
                    }
                }
                else {
                    $this->BlockAction($Block);
                }
            }
            else {
                $this->_indexAction();
            }
        }
    }

    /**
     * Нужен для работы диспетчера
     */
    public function indexAction(){}

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     */
    private function _indexAction($rubric = null)
    {
        //Fenix::redirect('/');
        //exit;
        $collection = Fenix::getCollection('sale/load')->load($rubric);
        $_rubric    = $collection->getRubric();
        $cityName = Fenix::getModel('ipcheck/location')->getSelectedCityName();
        $_sale  = Fenix::getModel('sale/sale_block')->getSaleListByCity($rubric,$cityName);


        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('sale/seo/h1', Fenix::lang("Скидки")),
            'uri'   => Fenix::getUrl('sale'),
            'id'    => 'sale'
        );
        if ($_rubric !== false) {
            $_crumbs[] = array(
                'label' => $_rubric->getTitle(),
                'uri'   => Fenix::getUrl('sale/' . $_rubric->getUrlKey()),
                'id'    => 'rubric'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        if ($_rubric !== false) {
            $this->setMeta($Creator, $_rubric, $_sale);
        }
        else {
            $this->setMeta($Creator, array(
                'url'         => Fenix::getUrl('sale'),
                'title'       => Fenix::getConfig('sale/seo/title'),
                'keywords'    => Fenix::getConfig('sale/seo/keywords'),
                'description' => Fenix::getConfig('sale/seo/description'),
            ), $_sale);
        }

        $Creator ->getView()->assign(array(
            'rubric'     => $_rubric,
            'sale'   => $_sale,
            'collection' => $collection
        ));

        $Creator ->setLayout()
                 ->render('sale/list.phtml');
    }

    /**
     * Читаем скидку
     *
     * @param $Block
     * @param null $rubric
     */
    private function BlockAction($Block, $rubric = null)
    {
        $_Block = Fenix::getCollection('sale/Block')->load($Block, $rubric);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('sale/seo/h1', Fenix::lang("Акции")),
            'uri'   => Fenix::getUrl('sale'),
            'id'    => 'sale'
        );
        if ($rubric != null) {
            $_crumbs[] = array(
                'label' => $rubric->title,
                'uri'   => Fenix::getUrl('sale/' . $rubric->url_key),
                'id'    => 'rubric'
            );
        }

        $_crumbs[] = array(
            'label' => $_Block->getTitle(),
            'uri'   => $_Block->getUrl(),
            'id'    => 'Block'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $_Block);

        $Creator ->getView()->assign(array(
            'rubric'  => $rubric,
            'block' => $_Block
        ));
        $Creator ->setLayout()
                 ->render('sale/read.phtml');
    }
}