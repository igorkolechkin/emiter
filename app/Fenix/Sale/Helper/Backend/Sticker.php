<?php
class Fenix_Sale_Helper_Backend_Sticker extends Fenix_Resource_Helper
{
    /**
     * Выбор категорий чекбоксами
     *
     * @param $stickerId
     * @return mixed
     */
    public function getCategoriesSelect($stickerId)
    {
        $sticker = Fenix::getCollection('sale/sticker')->loadStickerById($stickerId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categories',
            'stickerId' => $stickerId,
            'block'   => $sticker,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList()
        ));
    }
}