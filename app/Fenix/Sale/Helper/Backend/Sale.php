<?php
class Fenix_Sale_Helper_Backend_Sale extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'sale' && Fenix::getRequest()->getUrlSegment(1) != 'rubric';
    }
    public function getProductsFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/browser_sale.phtml', $options);
    }

    /**
     * Выбор категорий чекбоксами
     *
     * @param $saleId
     * @return mixed
     */
    public function getCategoriesFromSelect($saleId)
    {
        $block = Fenix::getCollection('sale/record')->loadSaleById($saleId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categoriesFrom',
            'blockId' => $saleId,
            'block'   => $block,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList(),
            'noForm' => true
        ));
    }

    /**
     * Выбор категорий чекбоксами
     *
     * @param $saleId
     * @return mixed
     */
    public function getCategoriesViewSelect($saleId)
    {
        $block = Fenix::getCollection('sale/record')->loadSaleById($saleId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categoriesView',
            'blockId' => $saleId,
            'block'   => $block,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList(),
            'noForm' => true
        ));
    }
    /**
     * Выбор категорий чекбоксами
     *
     * @param $saleId
     * @return mixed
     */
    public function getCategoriesToSelect($saleId)
    {
        $block = Fenix::getCollection('sale/record')->loadSaleById($saleId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categoriesTo',
            'blockId' => $saleId,
            'block'   => $block,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList(),
            'noForm' => true
        ));
    }
}