<?php
class Fenix_Sale_Helper_Backend_Rubric extends Fenix_Resource_Helper
{
    /**
     * Таблица с рубриками, для создания/редактирования статьи
     *
     * @param $recordId
     * @return mixed
     */
    public function getRubricsTable($recordId)
    {
        $RubricsList = Fenix::getModel('sale/backend_rubric')->getRubricsList();

        $Creator = Fenix::getCreatorUI();

        $Table = $Creator->loadPlugin('Table')
                         ->setStyle('margin-bottom:10px;');

        $Table ->addHeader('rule',      "Название группы")
               ->setColspan(2);
        $Table ->addHeader('sys_title', "Ссылка");

        $Model = Fenix::getModel()->setTable('sale_relations');

        $_postRubrics = (array) Fenix::getRequest()->getPost('rubric');

        foreach ($RubricsList AS $_rubric) {
            $Checkbox = $Creator->loadPlugin('Form_Checkbox')
                                ->setId('rubric_list_' . $_rubric->id)
                                ->setName('rubric[]')
                                ->setValue($_rubric->id);

            if ($recordId != null) {
                $checked = $Model->fetchRow(
                    'record_id = ' . (int) $recordId . ' AND rubric_id = ' . (int) $_rubric->id
                );

                if ($checked != null) {
                    $Checkbox->setChecked('checked');
                }
            }

            if (in_array($_rubric->id, $_postRubrics)) {
                $Checkbox->setChecked('checked');
            }

            $Table ->addCell('group_' . $_rubric->id, 'checkbox', '<label>' . $Checkbox->fetch() . '<span class="lbl"></span></label>')
                   ->setStyle('padding:5px 10px;width:1px;');
            $Table ->addCell('group_' . $_rubric->id, 'rule',      $_rubric->title)
                   ->setStyle('padding-top:5px;padding-bottom:5px;');
            $Table ->addCell('group_' . $_rubric->id, 'sys_title', $_rubric->url_key)
                   ->setStyle('padding-top:7px;padding-bottom:7px;');
        }

        return $Table->fetch();
    }
}