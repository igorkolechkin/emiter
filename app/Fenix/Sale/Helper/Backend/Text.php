<?php
class Fenix_Sale_Helper_Backend_text extends Fenix_Resource_Helper
{
    /**
     * Выбор категорий чекбоксами
     *
     * @param $textId
     * @return mixed
     */
    public function getCategoriesSelect($textId)
    {
        $text = Fenix::getCollection('sale/text')->loadTextById($textId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categories',
            'textId' => $textId,
            'block'   => $text,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList()
        ));
    }
}