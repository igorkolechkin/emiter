<?php
class Fenix_Sale_Helper_Backend_block extends Fenix_Resource_Helper
{
    /**
     * Выбор категорий чекбоксами
     *
     * @param $blockId
     * @return mixed
     */
    public function getCategoriesSelect($blockId)
    {
        $block = Fenix::getCollection('sale/block')->loadBlockById($blockId);

        $Creator = Fenix::getCreatorUI();

        return $Creator->getView()->partial('catalog/categories_select.phtml', array(
            'name'    => 'categories',
            'blockId' => $blockId,
            'block'   => $block,
            'categoriesList'   => Fenix::getModel('catalog/categories')->getCategoriesList()
        ));
    }
}