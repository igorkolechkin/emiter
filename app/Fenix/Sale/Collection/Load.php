<?php
class Fenix_Sale_Collection_Load extends Fenix_Resource_Collection
{
    public function load($rubric = null)
    {
        return new Fenix_Object(array(
            'data' => array(
                'rubric'  => Fenix::getCollection('sale/rubric')->load($rubric),
                'sale' => Fenix::getCollection('sale/sale')->load($rubric)
            )
        ));
    }

    public function getActiveSalesList($section = 'checkout'){
        $sales = Fenix::getModel('sale/sale')->getActiveSalesList($section);
        $data = array();
        foreach ($sales as $sale) {
            $data[] = Fenix::getCollection('sale/record')->loadSale($sale);

        }

        return new Fenix_Object_Rowset(array(
            'data' => $data,
            'rowClass' => false
        ));
    }
    public function loadTooltipSalesList(){
        $sales = Fenix::getModel('sale/sale')->getTooltipSalesList();
        $data = array();
        foreach ($sales as $sale) {
            $data[] = Fenix::getCollection('sale/record')->loadSale($sale);

        }

        return new Fenix_Object_Rowset(array(
            'data' => $data,
            'rowClass' => false
        ));
    }
}