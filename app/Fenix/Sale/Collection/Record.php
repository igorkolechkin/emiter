<?php
class Fenix_Sale_Collection_Record extends Fenix_Object
{
    use Fenix_Traits_SeoCollection;

    public function loadSale($record){
        if ($record instanceof Zend_Db_Table_Row) {
            $record = $record->toArray();
            $record['suppliers'] = Fenix::getModel('sale/backend_suppliers')->getSuppliers($record['id']);

            $categoriesId = Fenix::getModel('sale/backend_sale')->getSaleCategoriesId($record['id'],'from');
            $record['categoriesFrom'] = $categoriesId;
            $categoriesId = Fenix::getModel('sale/backend_sale')->getSaleCategoriesId($record['id'],'to');
            $record['categoriesTo'] = $categoriesId;

            $categoriesViewId = Fenix::getModel('sale/backend_sale')->getSaleViewCategoriesId($record['id']);
            $record['categoriesView'] = $categoriesViewId;
        }

        $this->setData($record);

        return $this;
    }

    public function loadSaleById($id){
        $record = Fenix::getModel('sale/sale')->getSaleById($id);

        if ($record instanceof Zend_Db_Table_Row) {
            $record = $record->toArray();
            $record['suppliers'] = Fenix::getModel('sale/backend_suppliers')->getSuppliers($record['id']);

            $categoriesId = Fenix::getModel('sale/backend_sale')->getSaleCategoriesId($record['id'],'from');

            $record['categoriesFrom'] = $categoriesId;
            $categoriesId = Fenix::getModel('sale/backend_sale')->getSaleCategoriesId($record['id'],'to');
            $record['categoriesTo'] = $categoriesId;

            $categoriesViewId = Fenix::getModel('sale/backend_sale')->getSaleViewCategoriesId($record['id']);
            $record['categoriesView'] = $categoriesViewId;
        }

        $this->setData($record);

        return $this;
    }
    public function load($record, $rubric)
    {
        if ($record instanceof Zend_Db_Table_Row) {
            $record = $record->toArray();
        }

        $record['seo']     = $this->getSeoMetadata($record, 'sale_record');
        $record['url']     = Fenix::getUrl('sale/' . ($rubric != null ? $rubric->url_key . '/' : null) . $record['url_key']);
        $record['gallery'] = $this->getGallery($record['gallery']);

        $this->setData($record);

        return $this;
    }

    public function getGallery($gallery)
    {
        $gallery = (array) unserialize($gallery);
        $gallery = array_map(function($value){
            $value['image'] = HOME_DIR_URL . $value['image'];
            return $value;
        }, $gallery);

        return new Fenix_Object_Rowset(array('data' => $gallery));
    }

    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getImage()
    {
        if ($this->getData('image') != null) {
            $info              = (object) unserialize($this->getData('image_info'));
            $imageUrl          = Fenix::createImageFromStreamInfo($this->getData('image'), $info);
            return $imageUrl;
        }

        return false;
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function getImageFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::frame($image, $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageResize($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::resize($image, $width, $height);
        }

        return false;
    }

    public function getImageAdapt($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::adapt($image, $width, $height);
        }

        return false;
    }
}