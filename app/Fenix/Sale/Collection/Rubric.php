<?php
class Fenix_Sale_Collection_Rubric extends Fenix_Resource_Collection
{
    use Fenix_Traits_SeoCollection;

    public function load($rubric)
    {
        if ($rubric == null) {
            return false;
        }

        $_data        = $rubric->toArray();
        $_data['seo'] = $this->getSeoMetadata($_data, 'sale_rubric');
        $_data['url'] = Fenix::getUrl('sale/' . $_data['url_key']);

        return new Fenix_Object(array(
            'data' => $_data
        ));
    }

    public function loadRubricList()
    {
        $_rubrics = Fenix::getModel('sale/rubric')->getRubricsList();


        $_data = array();
        foreach ($_rubrics as $_rubric) {
            $_data[]= Fenix::getCollection('sale/rubric')->load($_rubric);
        }
        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}