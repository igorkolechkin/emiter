<?php
class Fenix_Sale_Collection_Block extends Fenix_Object
{
    use Fenix_Traits_SeoCollection;

    public function loadBlock($block){
        if ($block instanceof Zend_Db_Table_Row) {
            $block = $block->toArray();
        }

        $categoriesId     = Fenix::getModel('sale/backend_block')->getBlockCategoriesId($block['id']);

        $block['categories']     = $categoriesId;
        $block['seo']     = $this->getSeoMetadata($block, 'sale_record');
        $block['url']     = Fenix::getUrl('sale/'.$block['url_key']);
        $block['gallery'] = $this->getGallery($block['gallery']);

        $this->setData($block);

        return $this;
    }

    public function loadBlockById($blockId){

        $block = Fenix::getModel('sale/backend_block')->getBlockById($blockId);
        if($block)
            return self::loadBlock($block);
        else
            return null;
    }

    public function load($record, $rubric)
    {
        if ($record instanceof Zend_Db_Table_Row) {
            $record = $record->toArray();
        }

        $record['seo']     = $this->getSeoMetadata($record, 'sale_record');
        $record['url']     = Fenix::getUrl('sale/' . ($rubric != null ? $rubric->url_key . '/' : null) . $record['url_key']);
        $record['gallery'] = $this->getGallery($record['gallery']);

        $this->setData($record);

        return $this;
    }

    public function getGallery($gallery)
    {
        $gallery = (array) unserialize($gallery);
        $gallery = array_map(function($value){
            $value['image'] = HOME_DIR_URL . $value['image'];
            return $value;
        }, $gallery);

        return new Fenix_Object_Rowset(array('data' => $gallery));
    }

    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getImage()
    {
        if ($this->getData('image') != null) {
            if (file_exists(BASE_DIR . $this->getData('image')))
                return BASE_URL . $this->getData('image');

            $info              = (object) unserialize($this->getData('image_info'));
            $imageUrl          = Fenix::createImageFromStreamInfo($this->getData('image'), $info);
            return $imageUrl;
        }

        return false;
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function getImageFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::frame($image, $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageResize($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::resize($image, $width, $height);
        }

        return false;
    }

    public function getImageAdapt($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::adapt($image, $width, $height);
        }

        return false;
    }
}