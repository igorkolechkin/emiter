<?php
class Fenix_Sale_Model_Text extends Fenix_Resource_Model
{
    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;

    public function getTextAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.priority asc');

        return $Select;
    }

    /**
     * Активные скидки
     * @param string $section
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveTextsList($section = 'checkout')
    {
        $regTitle = 'getActiveTextsList_' . $section;

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getTextAsSelect();
        $Select->where('a.section = ?', $section);
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    public function getTooltipTextsList(){
        $regTitle = 'getTooltipTextsList';

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getTextAsSelect();
        $Select->where('a.tooltip_in_products = ?', '1');
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    /**
     * Применяем скидку на заказе
     *
     */
    public function useText($order, $sale)
    {
    }
    /*
     *******************************************************************************
     */
    public function getLastText()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('sale/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('sale/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getTopText()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->where('a.is_top = ?', '1');
        $Select ->order('a.create_date desc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Select для блоков
     * @param null $rubric
     * @return Zend_Db_Select
     */
    public function getTextSelect($rubric = null){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        return $Select;
    }
    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     * @return Zend_Paginator
     */
    public function getTextList($rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Скидка по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getText($url, $rubric = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', urldecode($url))
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getTextListByCity($rubric = null, $city=''){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this   ->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }
        $Select->where(
               $this->getAdapter()->quoteInto('a.city = ?', $city) . ' OR ' .
               $this->getAdapter()->quoteInto('a.city = "Все"', null)
        );
        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    public function getCategoriesSelect($id, $name = 'parent', $selected = 0){

        $Select = self::getTextAsSelect();
        $Select->where('a.id = ?', $id);
        $Select->where('a.is_public = ?', '1');

        $text = $this->fetchRow($Select);

        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId($name)
                      ->setName($name);
        if ($text) {
            if ($selected) {
                $Field->setSelected((int)$selected);
            } else {
                $Field->setSelected((int)$text->parent);
            }
        }
        $Field->addOption(0, 'Не выбрана');
        $Field->addOption(1, 'Все категории');
        $categories = Fenix::getCollection('catalog/categories')->getCategoriesList();
        foreach ($categories as $_category){
            $Field->addOption($_category->getId(), $_category->getTitle(),'level-0');
            $subCategories = Fenix::getCollection('catalog/categories')->getCategoriesList($_category->getId());
            foreach ($subCategories as $_subCategory){
                $Field->addOption($_subCategory->getId(), $_subCategory->getTitle(),'level-1');
            }
        }

        return $Field->fetch();
    }

    /**
     * Проверка есть ли блок для категории
     * @param $categoryId
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function categoryHasText($categoryId){

        $Select = self::getTextSelect();

        $Select->join(array(
            'bc'=>$this->getTable('sale_text_categories')
        ),'a.id = bc.text_id',null);
        $Select->where('a.is_public = ? ','1');
        $Select->where('bc.category_id = ? ',$categoryId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->limit(1);
        $Select->group('a.id');
        $Select->order('a.position asc');

        $match = $this->fetchRow($Select);

        if (Fenix::isDev()){
        //Fenix::dump($categoryId,$Select->assemble());
        }

        return $match;
    }
    /**
     * Проверка есть ли блок для товара
     * @param $categoryId
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function productHasText($productId){

        $Select = self::getTextSelect();

        $Select->join(array(
            'sp'=>$this->getTable('sale_text_products')
        ),'a.id = sp.text_id',null);
        $Select->where('a.is_public = ? ','1');
        $Select->where('sp.product_id = ? ',$productId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->limit(1);
        $Select->group('a.id');

        $match = $this->fetchRow($Select);

        return $match;
    }
}