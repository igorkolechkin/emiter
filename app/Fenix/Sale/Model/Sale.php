<?php
class Fenix_Sale_Model_Sale extends Fenix_Resource_Model
{
    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;

    public function getSaleAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        $Select ->order('s.priority asc');

        return $Select;
    }

    /**
     * Активные скидки
     * @param string $section
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveSalesList($section = 'checkout')
    {
        $regTitle = 'getActiveSalesList_' . $section;

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getSaleAsSelect();
        $Select->where('s.section = ?', $section);
        $Select->where('s.is_active = ?', '1');
        $Select->where('NOW() BETWEEN s.begin_date AND s.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    public function getTooltipSalesList(){
        $regTitle = 'getTooltipSalesList';

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getSaleAsSelect();
        $Select->where('s.tooltip_in_products = ?', '1');
        $Select->where('s.is_active = ?', '1');
        $Select->where('NOW() BETWEEN s.begin_date AND s.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    /**
     * Применяем скидку на заказе
     *
     */
    public function useSale($order, $sale)
    {
    }
    /*
     *******************************************************************************
     */
    public function getLastSale()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('s.is_active = ?', '1');
        $Select ->order('s.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('sale/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('sale/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getTopSale()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('s.is_active = ?', '1');
        $Select ->where('s.is_top = ?', '1');
        $Select ->order('s.create_date desc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }



    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $categoryId
     * @param null $rubric
     * @return Zend_Paginator
     * @throws Zend_Paginator_Exception
     */
    public function getSaleList($options, $rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        //Фильтруем по рубрикам
        if ($rubric != null) {
            $Select->join(array(
                'sr' => $this->getTable('sale_relations')
            ), 's.id = sr.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), 'sr.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        //Фильтруем по категории
        if (isset($options['category_id']) && $options['category_id'] != Fenix::ALL) {
            $Select->join(array(
                'sc' => $this->getTable('sale_categories')
            ), 's.id = sc.sale_id AND sc.target = "from" AND ' . $this->getAdapter()->quoteInto('sc.category_id = ?', $options['category_id']), false);
        }

        if(isset($options['is_active']) && $options['is_active'] == Fenix::IS_ACTIVE)
        {
            $Select->where('s.is_active = ?', Fenix::IS_ACTIVE);

            $Select->where(
                $this->getAdapter()->quoteInto('s.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
                $this->getAdapter()->quoteInto('s.begin_date <= NOW()','')
            );
            $Select->where(
                $this->getAdapter()->quoteInto('s.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
                $this->getAdapter()->quoteInto('s.end_date >= NOW()','')
            );
        }

        if(isset($options['on_main']) && $options['on_main'] != Fenix::ALL)
        {
            $Select->where('s.on_main = ?', $options['on_main']);
        }

        $Select ->order('s.end_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }



    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $categoryId
     * @param null $rubric
     * @return Zend_Paginator
     * @throws Zend_Paginator_Exception
     */
    public function getSaleListByCategory($options, $rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        //Фильтруем по рубрикам
        if ($rubric != null) {
            $Select->join(array(
                'sr' => $this->getTable('sale_relations')
            ), 's.id = sr.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), 'sr.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }
        //Фильтруем по категории
        if (isset($options['category_id']) && $options['category_id'] != Fenix::ALL) {
            $Select->join(array(
                'sc' => $this->getTable('sale_categories_view')
            ), 's.id = sc.sale_id AND ' . $this->getAdapter()->quoteInto('sc.category_id = ?', $options['category_id']), false);
        }

        if(isset($options['is_active']) && $options['is_active'] == Fenix::IS_ACTIVE)
        {
            $Select->where('s.is_active = ?', Fenix::IS_ACTIVE);

            $Select->where(
                $this->getAdapter()->quoteInto('s.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
                $this->getAdapter()->quoteInto('s.begin_date <= NOW()','')
            );
            $Select->where(
                $this->getAdapter()->quoteInto('s.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
                $this->getAdapter()->quoteInto('s.end_date >= NOW()','')
            );
        }

        if(isset($options['on_main']) && $options['on_main'] != Fenix::ALL)
        {
            $Select->where('s.on_main = ?', $options['on_main']);
        }

        $Select ->order('s.end_date desc');

        $Result = $this->fetchAll($Select);
        //Fenix::dump($options,$Select->assemble(), $Result);

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
            ->setItemCountPerPage($perPage);

        return $paginator;
    }


    public function getSaleCategories($parentCategoryId = 1){

        $_catalog = Fenix::getModel('catalog/categories')->getCategoryById($parentCategoryId);
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');
        $colsCategory = $Engine ->getColumns(array('prefix'=>'c'));
/*
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');
        $colsCategory = $Engine ->getColumns(array('prefix'=>'c'));

        $this->setTable('sale_discount');
        $SelectA = $this->select()
                       ->setIntegrityCheck(false);
        $SelectA ->from(array(
            's' => $this->getTable()
        ), null);

        $cols[] = new Zend_Db_Expr('DISTINCT r.parent as cid');

        $SelectA ->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'r.product_id = s.product_id', $cols);

        $SelectA->join(array(
            'c' => $this->getTable('catalog')
        ), 'c.id = r.parent', null);

        $SelectA->join(array(
            'cc' => $this->getTable('catalog')
        ), 'cc.id = c.parent', '*');


        $SelectA->group('cc.id');

        $ResultA = $this->fetchAll($SelectA);*/

#
#        $this->setTable('sale_discount');
#        $SelectA = $this->select()
#            ->setIntegrityCheck(false);
#
#        $SelectA ->from(array(
#            's' => $this->getTable()
#        ), array('*'));
#
#        $SelectA ->join(array(
#            'r' => $this->getTable('catalog_relations')
#        ), 'r.product_id = s.product_id', null);
#
#        $SelectA->join(array(
#            'c' => $this->getTable('catalog')
#        ), 'c.id = r.parent', $colsCategory);
#
#/*
#        $SelectA ->where('r.left >= ?', (int) $_catalog->left)
#                ->where('r.right <= ?', (int) $_catalog->right);
#*/
#        $SelectA->join(array(
#            'sale' => $this->getTable('sale')
#        ), 'sale.id = s.sale_id', false);
#
#        $SelectA->where('sale.is_active = ?', Fenix::IS_ACTIVE);
#        $SelectA->where('c.parent = ?', $parentCategoryId);
#
#/*        $SelectA->where(
#            $this->getAdapter()->quoteInto('sale.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
#            $this->getAdapter()->quoteInto('sale.begin_date <= NOW()','')
#        );
#        $SelectA->where(
#            $this->getAdapter()->quoteInto('sale.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
#            $this->getAdapter()->quoteInto('sale.end_date >= NOW()','')
#        );
#*/
#
#        $SelectA->group('c.id');
#        $ResultA = $this->fetchAll($SelectA);

//Fenix::dump($ResultA->count(),$ResultA);



        $this->setTable('sale');
        $SelectB = $this->select()
                        ->setIntegrityCheck(false);
        $SelectB ->from(array(
            's' => $this->getTable()
        ), array('*'));
        $SelectB ->join(array(
            'sc' => $this->getTable('sale_categories_view')
        ), 'sc.sale_id = s.id', null);

        //$colsCategory = array('title_russian','url_key');
        $SelectB->join(array(
            'c' => $this->getTable('catalog')
        ), 'sc.category_id = c.id', $colsCategory);

        $SelectB->where('s.is_active = ?', Fenix::IS_ACTIVE);
        $SelectB->where('c.parent = ?', $parentCategoryId);

        /*$SelectB->where(
            $this->getAdapter()->quoteInto('s.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('s.begin_date <= NOW()','')
        );
        $SelectB->where(
            $this->getAdapter()->quoteInto('s.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('s.end_date >= NOW()','')
        );*/

        $SelectB->group('c.id');

        $ResultB = $this->fetchAll($SelectB);

       /* $ResultAll = array();
        foreach ($ResultA as $_category) {
            $ResultAll[$_category->id] = $_category;
        }
        foreach ($ResultB as $_category) {
            $ResultAll[$_category->id] = $_category;
        }
        $Result = new Fenix_Object_Rowset(array(
            'data'     => $ResultAll,
            'rowClass' => false
        ));*/

        return $ResultB;
    }

    public function getCategorySaleList($categoryId = 1){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');
        $cols = $Engine ->getColumns(array('prefix'=>'sale'));

#        $this->setTable('sale_discount');
#        $SelectA = $this->select()
#                        ->setIntegrityCheck(false);
#
#        $SelectA ->from(array(
#            's' => $this->getTable()
#        ), null);
#
#        $SelectA ->join(array(
#            'r' => $this->getTable('catalog_relations')
#        ), 'r.product_id = s.product_id', null);
#
#        $SelectA->join(array(
#            'c' => $this->getTable('catalog')
#        ), 'c.id = r.parent', null);
#
#
#        $SelectA ->where('r.parent = ?', (int) $categoryId);
#
#        $SelectA->join(array(
#            'sale' => $this->getTable('sale')
#        ), 'sale.id = s.sale_id', $cols);
#
#        $SelectA->where('sale.is_active = ?', Fenix::IS_ACTIVE);
#
#        $SelectA->where(
#            $this->getAdapter()->quoteInto('sale.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
#            $this->getAdapter()->quoteInto('sale.begin_date <= NOW()','')
#        );
#        $SelectA->where(
#            $this->getAdapter()->quoteInto('sale.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
#            $this->getAdapter()->quoteInto('sale.end_date >= NOW()','')
#        );
#
#        $SelectA->group('c.id');


        $this->setTable('sale_discount');
        $SelectA = $this->select()
                        ->setIntegrityCheck(false);
        $SelectA ->from(array(
            's' => $this->getTable()
        ), null);

        $cols[] = new Zend_Db_Expr('DISTINCT r.parent as cid');

        $SelectA ->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'r.product_id = s.product_id', null);

        $SelectA->join(array(
            'c' => $this->getTable('catalog')
        ), 'c.id = r.parent', null);

        $SelectA->join(array(
            'cc' => $this->getTable('catalog')
        ), 'cc.id = c.parent', null);

        $cols = $Engine ->getColumns(array('prefix'=>'sale'));
        $SelectA->join(array(
            'sale' => $this->getTable('sale')
        ), 'sale.id = s.sale_id', $cols);

        $SelectA->where('sale.is_active = ?', Fenix::IS_ACTIVE);
        $SelectA->where(
            $this->getAdapter()->quoteInto('sale.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('sale.begin_date <= NOW()','')
        );
        $SelectA->where(
            $this->getAdapter()->quoteInto('sale.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('sale.end_date >= NOW()','')
        );

        $SelectA->group('cc.id');

        $ResultA = $this->fetchAll($SelectA);


        $this->setTable('sale');
        $SelectB = $this->select()
                        ->setIntegrityCheck(false);

        $SelectB ->from(array(
            'sale' => $this->getTable()
        ), $cols);

        $SelectB ->join(array(
            'sc' => $this->getTable('sale_categories')
        ), 'sc.sale_id = sale.id AND sc.target="from"', null);

        $SelectB->join(array(
            'c' => $this->getTable('catalog')
        ), 'sc.category_id = c.id', null);

        $SelectB->where('sale.is_active = ?', Fenix::IS_ACTIVE);
        $SelectB->where(
            $this->getAdapter()->quoteInto('sale.begin_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('sale.begin_date <= NOW()','')
        );
        $SelectB->where(
            $this->getAdapter()->quoteInto('sale.end_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            $this->getAdapter()->quoteInto('sale.end_date >= NOW()','')
        );
        $SelectB->where('c.id = ?', $categoryId);
        $SelectB->group('c.id');

        $ResultB = $this->fetchAll($SelectB);
        $ResultAll = array();
        foreach ($ResultA as $_category) {
            $ResultAll[$_category->id] = $_category;
        }
        foreach ($ResultB as $_category) {
            $ResultAll[$_category->id] = $_category;
        }
        $Result = new Fenix_Object_Rowset(array(
            'data'     => $ResultAll,
            'rowClass' => false
        ));

        return $Result;

    }

    /**
     * Скидкапо url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecord($url, $rubric = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('s.url_key = ?', urldecode($url))
                ->where('s.is_active = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 's.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Скидка по id
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSaleById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        $Select ->where('s.id = ?', $id)
                ->where('s.is_active = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getSaleListByCity($rubric = null, $city=''){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this   ->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        $Select ->where('s.is_active = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 's.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }
        $Select->where(
               $this->getAdapter()->quoteInto('s.city = ?', $city) . ' OR ' .
               $this->getAdapter()->quoteInto('s.city = "Все"', null)
        );
        $Select ->order('s.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    public function getCategoriesSelect($id, $name = 'parent', $selected = 0){

        $Select = self::getSaleAsSelect();
        $Select->where('s.id = ?', $id);
        $Select->where('s.is_active = ?', '1');

        $block = $this->fetchRow($Select);

        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId($name)
                      ->setName($name);
        if ($block) {
            if ($selected) {
                $Field->setSelected((int)$selected);
            } else {
                $Field->setSelected((int)$block->parent);
            }
        }
        $Field->addOption(0, 'Не выбрана');
        $Field->addOption(1, 'Все категории');
        $categories = Fenix::getCollection('catalog/categories')->getCategoriesList();
        foreach ($categories as $_category){
            $Field->addOption($_category->getId(), $_category->getTitle(),'level-0');
            $subCategories = Fenix::getCollection('catalog/categories')->getCategoriesList($_category->getId());
            foreach ($subCategories as $_subCategory){
                $Field->addOption($_subCategory->getId(), $_subCategory->getTitle(),'level-1');
            }
        }

        return $Field->fetch();
    }

    public function productsInSale($productIdList){

        $inSale = false;

        //Активные скидки для корзины
        $salesList = Fenix::getCollection('sale/load')->getActiveSalesList('checkout');

        foreach($salesList as $indexSale =>$sale){
            //Скидка зависит от:
            switch($sale->param){
                //ОБЫЧНЫЕ АКЦИИ ПРОПУСКАЕМ

                //Количества товаров в заказе
                case 'product-qty':
                    break;
                //Сумма товаров в заказе
                case 'checkout-sum':
                    break;
                //Количество товаров по поставщику
                case 'supplier-qty':
                    break;

                //ИЩЕМ ТОЛКЬО В СПИСКАХ
                case 'list':
                case 'list-qty':

                    foreach($productIdList as $productId) {
                        $inSale = $this->inSaleGetFromProducts($sale,$productId);

                        //Если хотябы одно совпадение, прекращаем поиск
                        if($inSale) return $inSale;
                    }
            }

        }

        return $inSale;
    }

    public function inSaleGetFromProducts($sale, $productId){

        $_saleProducts = Fenix::getModel('sale/backend_products')->getProductsFromListByBlockId($sale->id);
        $saleProducts = array();
        if($_saleProducts->count()>0){
            foreach($_saleProducts as $saleProduct){
                $saleProducts[]= $saleProduct->product_id;
            }
        }
        //Находим в привязаных товарах
        if (in_array($productId ,$saleProducts ))
            return true;

        //Находим в привязаных категориях
        $parentIdList = $sale->categoriesFrom;
        $parentIdList[]=$sale->parent_from;

        $categoriesList = Fenix::getModel('catalog/products')->getCategoriesIdList($productId);
        $itersect = array_intersect($parentIdList, $categoriesList);
        if (count($itersect) > 0) {
            return true;
        }

       return false;
    }
}