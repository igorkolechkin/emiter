<?php
class Fenix_Sale_Model_Block extends Fenix_Resource_Model
{
    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;

    public function getBlockAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.priority asc');

        return $Select;
    }

    /**
     * Активные скидки
     * @param string $section
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveBlocksList($section = 'checkout')
    {
        $regTitle = 'getActiveBlocksList_' . $section;

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getBlockAsSelect();
        $Select->where('a.section = ?', $section);
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    public function getTooltipBlocksList(){
        $regTitle = 'getTooltipBlocksList';

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getBlockAsSelect();
        $Select->where('a.tooltip_in_products = ?', '1');
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    /**
     * Применяем скидку на заказе
     *
     */
    public function useBlock($order, $sale)
    {
    }
    /*
     *******************************************************************************
     */
    public function getLastBlock()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('sale/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('sale/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getTopBlock()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->where('a.is_top = ?', '1');
        $Select ->order('a.create_date desc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Select для блоков
     * @param null $rubric
     * @return Zend_Db_Select
     */
    public function getBlockSelect($rubric = null){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        return $Select;
    }
    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     * @return Zend_Paginator
     */
    public function getBlockList($rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Скидка по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getBlock($url, $rubric = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', urldecode($url))
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getBlockListByCity($rubric = null, $city=''){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this   ->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }
        $Select->where(
               $this->getAdapter()->quoteInto('a.city = ?', $city) . ' OR ' .
               $this->getAdapter()->quoteInto('a.city = "Все"', null)
        );
        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    public function getCategoriesSelect($id, $name = 'parent', $selected = 0){

        $Select = self::getBlockAsSelect();
        $Select->where('a.id = ?', $id);
        $Select->where('a.is_public = ?', '1');

        $block = $this->fetchRow($Select);

        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId($name)
                      ->setName($name);
        if ($block) {
            if ($selected) {
                $Field->setSelected((int)$selected);
            } else {
                $Field->setSelected((int)$block->parent);
            }
        }
        $Field->addOption(0, 'Не выбрана');
        $Field->addOption(1, 'Все категории');
        $categories = Fenix::getCollection('catalog/categories')->getCategoriesList();
        foreach ($categories as $_category){
            $Field->addOption($_category->getId(), $_category->getTitle(),'level-0');
            $subCategories = Fenix::getCollection('catalog/categories')->getCategoriesList($_category->getId());
            foreach ($subCategories as $_subCategory){
                $Field->addOption($_subCategory->getId(), $_subCategory->getTitle(),'level-1');
            }
        }

        return $Field->fetch();
    }

    /**
     * Проверка есть ли блок для категории
     * @param $categoryId
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function categoryHasBlock($categoryId){

        $Select = self::getBlockSelect();

        $Select->join(array(
            'bc'=>$this->getTable('sale_block_categories')
        ),'a.id = bc.block_id',null);
        $Select->where('a.is_public = ? ','1');
        $Select->where('bc.category_id = ? ',$categoryId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->limit(1);
        $Select->group('a.id');

        $match = $this->fetchRow($Select);

        return $match;
    }
}