<?php
class Fenix_Sale_Model_Rubric extends Fenix_Resource_Model
{
    /**
     * Рубрика по URL
     *
     * @param $url
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRubric($url)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/rubric');

        $this->setTable('sale_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->where('r.url_key = ?', $url);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
    /**
     * Список рубрик
     *
     * @return Zend_Db_Select
     */
    public function getRubricsAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/rubric');

        $this->setTable('sale_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->order('r.position asc');

        return $Select;
    }

    /**
     * Список рубрик
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getRubricsList()
    {
        $Select = $this->getRubricsAsSelect();
        return $this->fetchAll($Select);
    }
}