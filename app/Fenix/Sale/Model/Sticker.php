<?php
class Fenix_Sale_Model_Sticker extends Fenix_Resource_Model
{
    const POSITION_DEFAULT = 'default';
    const POSITION_CARD    = 'card';
    const POSITION_LIST    = 'product-list';

    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;

    public function getStickerAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.priority asc');

        return $Select;
    }
    public function getStickerById($stickerId){
        $cacheId = 'getStickerById_' . $stickerId;

        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);

        $Select = $this->getStickerAsSelect();
        $Select->where('id = ?', $stickerId);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Активные скидки
     * @param string $section
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveStickersList($section = 'checkout')
    {
        $regTitle = 'getActiveStickersList_' . $section;

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getStickerAsSelect();
        $Select->where('a.section = ?', $section);
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    public function getTooltipStickersList(){
        $regTitle = 'getTooltipStickersList';

        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);

        $Select = self::getStickerAsSelect();
        $Select->where('a.tooltip_in_products = ?', '1');
        $Select->where('a.is_public = ?', '1');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($regTitle, $Result);

        return $Result;
    }
    /**
     * Применяем скидку на заказе
     *
     */
    public function useSticker($order, $sale)
    {
    }
    /*
     *******************************************************************************
     */
    public function getLastSticker()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('sale/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('sale/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getTopSticker()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->where('a.is_top = ?', '1');
        $Select ->order('a.create_date desc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Select для блоков
     * @param null $rubric
     * @return Zend_Db_Select
     */
    public function getStickerSelect($rubric = null){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        return $Select;
    }
    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     * @return Zend_Paginator
     */
    public function getStickerList($rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Скидка по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSticker($url, $rubric = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', urldecode($url))
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getStickerListByCity($rubric = null, $city=''){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this   ->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('sale_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('sale_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }
        $Select->where(
               $this->getAdapter()->quoteInto('a.city = ?', $city) . ' OR ' .
               $this->getAdapter()->quoteInto('a.city = "Все"', null)
        );
        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('sale/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('sale/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    public function getCategoriesSelect($id, $name = 'parent', $selected = 0){

        $Select = self::getStickerAsSelect();
        $Select->where('a.id = ?', $id);
        $Select->where('a.is_public = ?', '1');

        $sticker = $this->fetchRow($Select);

        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId($name)
                      ->setName($name);
        if ($sticker) {
            if ($selected) {
                $Field->setSelected((int)$selected);
            } else {
                $Field->setSelected((int)$sticker->parent);
            }
        }
        $Field->addOption(0, 'Не выбрана');
        $Field->addOption(1, 'Все категории');
        $categories = Fenix::getCollection('catalog/categories')->getCategoriesList();
        foreach ($categories as $_category){
            $Field->addOption($_category->getId(), $_category->getTitle(),'level-0');
            $subCategories = Fenix::getCollection('catalog/categories')->getCategoriesList($_category->getId());
            foreach ($subCategories as $_subCategory){
                $Field->addOption($_subCategory->getId(), $_subCategory->getTitle(),'level-1');
            }
        }

        return $Field->fetch();
    }

    /**
     * Проверка есть ли блок для категории
     * @param $categoryId
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function categoryHasSticker($categoryId){

        $cacheId = 'categoryHasSticker_' . $categoryId;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);



        $Select = self::getStickerSelect();

        $Select->join(array(
            'bc'=>$this->getTable('sale_sticker_categories')
        ),'a.id = bc.sticker_id',null);
        $Select->where('a.is_public = ? ','1');
        $Select->where('bc.category_id = ? ',$categoryId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->limit(1);
        $Select->group('a.id');
        $Select->order('a.position asc');

        $match = $this->fetchRow($Select);

        if (Fenix::isDev()){
        //Fenix::dump($categoryId,$Select->assemble());
        }

        Zend_Registry::set($cacheId, $match);
        return $match;
    }

    /**
     * Проверка есть ли блок для товара
     * @param $categoryId
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function productHasSticker($productId){

        $cacheId = 'productHasSticker_' . $productId;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);



        $Select = self::getStickerSelect();

        $Select->join(array(
            'sp'=>$this->getTable('sale_sticker_products')
        ),'a.id = sp.sticker_id',null);
        $Select->where('a.is_public = ? ','1');
        $Select->where('sp.product_id = ? ',$productId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->limit(1);
        $Select->group('a.id');

        $match = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $match);

        return $match;
    }

    public function getProductStickers($productId, $positionType){

        $cacheId = 'getProductStickers_' . $productId . '_' . $positionType ;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);
        /**
         * Стикеры по артикулу
         */
        $Select = self::getStickerSelect();

        $Select->join(array(
            'sp'=>$this->getTable('sale_sticker_products')
        ),'a.id = sp.sticker_id',null);
        $Select->where('a.is_public = ? ', '1');
        $Select->where('a.position_type = ? ', $positionType);
        $Select->where('sp.product_id = ? ', $productId);
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');

        $matchByProducts = $this->fetchAll($Select);

        /**
         * Стикеры по категориям
         */
        $categoriesList  = Fenix::getModel('catalog/products')->getCategoriesIdList($productId);
        $Select = self::getStickerSelect();

        $Select->join(array(
            'bc'=>$this->getTable('sale_sticker_categories')
        ),'a.id = bc.sticker_id',null);
        $Select->where('a.is_public = ? ', '1');
        $Select->where('a.position_type = ? ', $positionType);
        $Select->where('bc.category_id IN ("' . implode('","', $categoriesList) . '")');
        $Select->where('NOW() BETWEEN a.begin_date AND a.end_date');
        $Select->group('a.id');
        $Select->order('a.position asc');

        $matchByCategory = $this->fetchAll($Select);

        $Result = array();
        foreach($matchByProducts as $sticker){
            $Result[$sticker->id] = $sticker;
        }
        foreach($matchByCategory as $sticker){
            $Result[$sticker->id] = $sticker;
        }
        $Result = new Fenix_Object_Rowset(
            array(
                'data'     => $Result,
                'rowClass' => false
            )
        );
        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }


}