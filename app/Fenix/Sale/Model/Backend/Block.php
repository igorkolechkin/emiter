<?php
class Fenix_Sale_Model_Backend_Block extends Fenix_Resource_Model
{
    /**
     * Скидкапо идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getBlockById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список
     *
     * @return Zend_Db_Select
     */
    public function getBlockAsSelect()
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_block');

        $this->setTable('sale_block');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.begin_date asc');

        return $Select;
    }

    /**
     * Новый блок
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addBlock($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));

        $id = $Form->addRecord($req);

        //Сохраняем категориии
        self::updateBlockCategories($id,$req);

        return (int) $id;
    }

    /**
     * Редактировать блоку
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editBlock($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));

        $id = $Form->editRecord($current, $req);


        //Сохраняем категориии
        self::updateBlockCategories($id,$req);

        return (int) $id;
    }

    /**
     * Удаление блока
     *
     * @param $current
     */
    public function deleteBlock($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('sale/sale_block', $current->attributeset)
                 ->deleteRecord($current);

        $this->setTable('sale_block_categories')
             ->delete('block_id = ' . (int) $current->id);
    }

    /**
     * Обновляем категории блока
     * @param $blockId
     * @param $req
     */
    public function updateBlockCategories($blockId, $req){

        $_list = $req->getPost('_list');

        // Удаляем все старые связи
        $this->setTable('sale_block_categories')
             ->delete('block_id = ' . (int) $blockId);

        //Проверяем есть ли категории
        if ($_list && isset($_list['categories']) && is_array($_list['categories'])) {

            // Добавляем новые
            foreach($_list['categories'] as $categoryId => $active){
                if($active=='1')
                {
                    $this->insert(array(
                        'block_id' => (int) $blockId,
                        'category_id'  => (int) $categoryId
                    ));
                }
            }
        }
    }

    /**
     * ID Категорий блока
     * @param $blockId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getBlockCategoriesId($blockId)
    {
        $this->setTable('sale_block_categories');
        $relationsList = $this->fetchAll($this->getAdapter()->quoteInto('block_id = ?', $blockId));
        $idList        = array();
        foreach ($relationsList as $relation) {
            $idList[] = $relation->category_id;
        }
        return $idList;
    }
}