<?php
class Fenix_Sale_Model_Backend_Text extends Fenix_Resource_Model
{
    /**
     * Скидкапо идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getTextById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список
     *
     * @return Zend_Db_Select
     */
    public function getTextAsSelect()
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_text');

        $this->setTable('sale_text');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.begin_date asc');

        return $Select;
    }

    /**
     * Новый блок
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addText($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));
        $id = $Form->addRecord($req);


        //Сохраняем категориии
        self::updateTextCategories($id,$req);
        //Сохраняем товары
        self::updateProducts($id,$req);
        //Импорт артикулов из файла
        $this->importSkuByFile($id,$req,'from_file');

        return (int) $id;
    }

    /**
     * Редактировать блоку
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editText($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));

        $id = $Form->editRecord($current, $req);


        //Сохраняем категориии
        self::updateTextCategories($id,$req);

        //Сохраняем товары
        self::updateProducts($id,$req);

        //Импорт артикулов из файла
        $this->importSkuByFile($id,$req,'from_file');

        return (int) $id;
    }

    /**
     * Удаление блока
     *
     * @param $current
     */
    public function deleteText($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('sale/sale_text', $current->attributeset)
                 ->deleteRecord($current);

        $this->setTable('sale_text_categories')
             ->delete('text_id = ' . (int) $current->id);
    }

    /**
     * Обновляем категории блока
     * @param $textId
     * @param $req
     */
    public function updateTextCategories($textId, $req){

        $_list = $req->getPost('_list');

        // Удаляем все старые связи
        $this->setTable('sale_text_categories')
             ->delete('text_id = ' . (int) $textId);
        //Проверяем есть ли категории
        if ($_list && isset($_list['categories']) && is_array($_list['categories'])) {

            // Добавляем новые
            foreach($_list['categories'] as $categoryId => $active){
                if($active=='1')
                {
                    $this->insert(array(
                        'text_id' => (int) $textId,
                        'category_id'  => (int) $categoryId
                    ));
                }
            }
        }
    }

    /**
     * ID Категорий блока
     * @param $textId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getTextCategoriesId($textId)
    {
        $this->setTable('sale_text_categories');
        $relationsList = $this->fetchAll($this->getAdapter()->quoteInto('text_id = ?', $textId));
        $idList        = array();
        foreach ($relationsList as $relation) {
            $idList[] = $relation->category_id;
        }
        return $idList;
    }


    public function getProductsBrowser($blockId, $name = 'sku')
    {
        return Fenix::getHelper('sale/backend_sale')->getProductsFilter(array(
            'name'     => $name,
            'url'      => Fenix::getUrl('catalog/products/find'),
            'products' => self::getProductsListByTextId($blockId)
        ));
    }

    public function getProductsListByTextId($blockId)
    {

        $this->setTable('sale_text_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), array('*'));

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());


        $Select ->where('_p.text_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        $result = $this->fetchAll($Select);

        return $this->fetchAll($Select);
    }

    public function updateProducts($id, $req){

        $list = (array)$req->getPost('_list');

        if ($list && isset($list['sku'])) {

            // Удаляем все старые связи
            $this->setTable('sale_text_products')
                 ->delete('text_id = ' . (int)$id);
            // Добавляем новые
            foreach ($list['sku'] AS $position => $_product_id) {
                $this->insert(array(
                    'text_id' => (int)$id,
                    'product_id'  => (int)$_product_id,
                    'position'  => (int)$position
                ));
            }
        }
    }

    public function importSkuByFile($sticker_id, $req, $field){

        $fileInfo = $req->getFiles($field);

        if($fileInfo && $fileInfo->error =='0'){

            $tableName = 'sale_text_products';

            // Удаляем все старые связи
            $this->setTable($tableName)
                 ->delete('text_id = ' . (int)$sticker_id);

            //Создаем обхект для импорта
            $file = $fileInfo->tmp_name;
            try{
                $Excel = Fenix_Excel::getObject($file);
            }catch (Exception $e){
                return;
            }

            //Перебираем листы excel
            $counter  = 0;
            foreach($Excel->getAllSheets() as $i => $sheet) {
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $headRows = 0; //Шапки нет
                $row = 0;
                //Перебираем ячейки
                $col = 0;
                if ($highestRow - $headRows > 0) {
                    for (; $row <= $highestRow; ++$row) {
                        $sku = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
                        if($sku!=''){
                            try{


                                $product  = Fenix::getModel('catalog/products')->getProductBySku($sku);
                                if($product){
                                    $this->setTable($tableName);
                                    $data = array(
                                        'text_id'  => (int)$sticker_id,
                                        'product_id' => (int)$product->id,
                                        'position'   => (int)$counter++
                                    );
                                    //Fenix::dump($tableName, $data);

                                    $this->insert($data);
                                }
                            }catch (Exception $e){
                                Fenix::dump($e);
                            }
                        }
                    }
                }
            }
        }
    }
}