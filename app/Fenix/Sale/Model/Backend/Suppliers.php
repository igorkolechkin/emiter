<?php
class Fenix_Sale_Model_Backend_Suppliers extends Fenix_Resource_Model
{
    public function getSuppliers($id){

        $cacheId = 'saleGetSuppliers_' . $id;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $this->setTable('sale_suppliers_relations');
        $Select    = $this->select()
                          ->from($this->_name)
                          ->where('record_id = ?', $id);
        $relations = $this->fetchAll($Select);


        $relationsIndex = array();
        if($relations->count()>0){
            foreach($relations as $_link){
                $relationsIndex[]=$_link->supplier;
            }
        }
        Zend_Registry::set($cacheId, $relationsIndex);

        return $relationsIndex;
    }
    public function getSuppliersCheckboxTable($id){

        $relationsIndex = self::getSuppliers($id);

        $Select    = Fenix::getModel('catalog/products')->getProductsListAsSelect(null);

        $Select->reset(Zend_Db_Select::GROUP);
        $Select->reset(Zend_Db_Select::ORDER);
        $Select->group('p.supplier_russian');
        $Select->order('p.supplier_russian');

        $list =  $this->fetchAll($Select);


        $catalogList = array();
        foreach ($list as $_product) {

            $supplier = str_replace('"','',$_product->supplier_russian);
            $catalogList[]=$supplier;
        }


        $content='';
        $content .='<div class="row-fluid categories-multiselect">';
        $content .='<div class="span6">';
        $content .='<ul id="groups-categories" class="item-list groups-list">';
        foreach($catalogList as $i=>$_supplier){
            $content .='<li class="clearfix" style="position: relative;cursor: pointer;" onclick="toggleCategory(event,this);">';
            $content .='<input type="checkbox" name="suppliers[]" value="' . $_supplier . '" '.(in_array($_supplier,$relationsIndex)?'checked="checked"':'').' class="gui-form-checkbox"/><span class="lbl"></span>';
            $content .='<span style="margin-left:5px;">' . $_supplier . '</span>';
            $content .='</li>';
        }
        $content .='</ul>';
        $content .='</div>';
        $content .='</div>';
        $content .='
        <script>
            function toggleCategory(e,elem){
                e.stopPropagation();
                $(elem).find("input").not($(elem).find(".groups-categories input")).trigger("click");
            }
        </script>
        ';

        return $content;

    }

}