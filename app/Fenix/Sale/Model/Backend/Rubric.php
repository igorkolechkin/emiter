<?php
class Fenix_Sale_Model_Backend_Rubric extends Fenix_Resource_Model
{
    /**
     * Рубрика по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRubricById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/rubric');

        $this->setTable('sale_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->where('r.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список рубрик
     *
     * @return Zend_Db_Select
     */
    public function getRubricsAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/rubric');

        $this->setTable('sale_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->order('r.position asc');

        return $Select;
    }

    /**
     * Список рубрик
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getRubricsList()
    {
        $Select = $this->getRubricsAsSelect();
        return $this->fetchAll($Select);
    }

    /**
     * Новая рубрика
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addRubric($Form, $req)
    {
        $id = $Form->addRecord($req);

        return (int) $id;
    }

    /**
     * Редактировать рубрику
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editRubric($Form, $current, $req)
    {
        $id = $Form->editRecord($current, $req);

        return (int) $id;
    }

    /**
     * Удаление рубрики
     *
     * @param $current
     */
    public function deleteRubric($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('sale/rubric', 'default')
                 ->deleteRecord($current);

        $this->setTable('sale_relations')
             ->delete('rubric_id = ' . (int) $current->id);
    }
}