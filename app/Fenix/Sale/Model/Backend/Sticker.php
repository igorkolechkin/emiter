<?php
class Fenix_Sale_Model_Backend_Sticker extends Fenix_Resource_Model
{
    /**
     * Скидкапо идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getStickerById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список
     *
     * @return Zend_Db_Select
     */
    public function getStickerAsSelect()
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale_sticker');

        $this->setTable('sale_sticker');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.position asc');

        return $Select;
    }

    /**
     * Новый блок
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addSticker($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));
        $id = $Form->addRecord($req);


        //Сохраняем категориии
        self::updateStickerCategories($id,$req);
        //Сохраняем товары
        self::updateProducts($id,$req);
        //Импорт артикулов из файла
        $this->importSkuByFile($id,$req,'from_file');

        $this->applyStickerbyId($id);

        return (int) $id;
    }

    /**
     * Редактировать блоку
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editSticker($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));

        $id = $Form->editRecord($current, $req);

        //Сохраняем категориии
        self::updateStickerCategories($id,$req);

        //Сохраняем товары
        self::updateProducts($id,$req);

        //Импорт артикулов из файла
        $this->importSkuByFile($id,$req,'from_file');

        $this->applyStickerbyId($id);

        return (int) $id;
    }

    /**
     * Удаление блока
     *
     * @param $current
     */
    public function deleteSticker($current)
    {
        //Обнуляем наклейки на товарах если они были поставлены по текущему стикеру
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_category_id'   => 0,
            'sticker_by_category'      => '',
            'sticker_by_category_date' => '0000-00-00 00:00:00'
        );
        $this->update($data, $this->getAdapter()->quoteInto('sticker_by_category_id = ?', $current->id));

        //Обнуляем наклейки на товарах если они были поставлены по текущему стикеру
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_category_id'   => 0,
            'sticker_by_category'      => '',
            'sticker_by_category_date' => '0000-00-00 00:00:00'
        );
        $this->update($data, $this->getAdapter()->quoteInto('sticker_by_category_id = ?', $current->id));


        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('sale/sale_sticker', $current->attributeset)
                 ->deleteRecord($current);

        $this->setTable('sale_sticker_categories')
             ->delete('sticker_id = ' . (int) $current->id);
    }

    /**
     * Обновляем категории блока
     * @param $stickerId
     * @param $req
     */
    public function updateStickerCategories($stickerId, $req){

        $_list = $req->getPost('_list');

        // Удаляем все старые связи
        $this->setTable('sale_sticker_categories')
             ->delete('sticker_id = ' . (int) $stickerId);
        //Проверяем есть ли категории
        if ($_list && isset($_list['categories']) && is_array($_list['categories'])) {

            // Добавляем новые
            foreach($_list['categories'] as $categoryId => $active){
                if($active=='1')
                {
                    $this->insert(array(
                        'sticker_id' => (int) $stickerId,
                        'category_id'  => (int) $categoryId
                    ));
                }
            }
        }
    }

    /**
     * ID Категорий блока
     * @param $stickerId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getStickerCategoriesId($stickerId)
    {
        $this->setTable('sale_sticker_categories');
        $relationsList = $this->fetchAll($this->getAdapter()->quoteInto('sticker_id = ?', $stickerId));
        $idList        = array();
        foreach ($relationsList as $relation) {
            $idList[] = $relation->category_id;
        }
        return $idList;
    }


    public function getProductsBrowser($blockId, $name = 'sku')
    {
        return Fenix::getHelper('sale/backend_sale')->getProductsFilter(array(
            'name'     => $name,
            'url'      => Fenix::getUrl('catalog/products/find'),
            'products' => self::getProductsListByStickerId($blockId)
        ));
    }

    public function getProductsListByStickerId($blockId)
    {

        $this->setTable('sale_sticker_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), array('*'));

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());


        $Select ->where('_p.sticker_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        $result = $this->fetchAll($Select);

        return $this->fetchAll($Select);
    }

    public function updateProducts($id, $req){

        $list = (array)$req->getPost('_list');

        if ($list && isset($list['sku'])) {

            // Удаляем все старые связи
            $this->setTable('sale_sticker_products')
                 ->delete('sticker_id = ' . (int)$id);
            // Добавляем новые
            foreach ($list['sku'] AS $position => $_product_id) {
                $this->insert(array(
                    'sticker_id' => (int)$id,
                    'product_id' => (int)$_product_id,
                    'position'   => (int)$position
                ));
            }
        }
    }
    public function importSkuByFile($sticker_id, $req, $field){

        $fileInfo = $req->getFiles($field);

        if($fileInfo && $fileInfo->error =='0'){

            $tableName = 'sale_sticker_products';

            // Удаляем все старые связи
            $this->setTable($tableName)
                 ->delete('sticker_id = ' . (int)$sticker_id);

            //Создаем обхект для импорта
            $file = $fileInfo->tmp_name;
            try{
                $Excel = Fenix_Excel::getObject($file);
            }catch (Exception $e){
                return;
            }

            //Перебираем листы excel
            $counter  = 0;
            foreach($Excel->getAllSheets() as $i => $sheet) {
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $headRows = 0; //Шапки нет
                $row = 0;
                //Перебираем ячейки
                $col = 0;
                if ($highestRow - $headRows > 0) {
                    for (; $row <= $highestRow; ++$row) {
                        $sku = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
                        if($sku!=''){
                            try{

                                $product  = Fenix::getModel('catalog/products')->getProductBySku($sku);
                                if($product){
                                    $this->setTable($tableName);
                                    $data = array(
                                        'sticker_id'  => (int)$sticker_id,
                                        'product_id' => (int)$product->id,
                                        'position'   => (int)$counter++
                                    );

                                    $this->insert($data);
                                }
                            }catch (Exception $e){
                                Fenix::dump($e);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Применяем наклейку на товары  по Id
     * @param $stickerId
     * @return bool
     */
    public function applyStickerbyId($stickerId){
        $sticker = $this->getStickerById($stickerId);
        if($sticker)
        {
            $this->applySticker($sticker);
        }
        else
            return false;
    }

    /**
     * Применяем стикер по объекту
     * @param $sticker
     */
    public function applySticker($sticker){

        $sticker = Fenix::getCollection('sale/sticker')->loadSticker($sticker);
        if($sticker->getPositionType() =='default'){
            $this->applyStickerByCategory($sticker);
            $this->applyStickerByProducts($sticker);
        }
    }

    /**
     * Применяем стикер по его категориям
     * @param $sticker
     * @return int
     */
    public function applyStickerByCategory($sticker){

        //Обнуляем наклейки на товарах если они были поставлены по текущему стикеру
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_category_id'   => 0,
            'sticker_by_category'      => '',
            'sticker_by_category_date' => '0000-00-00 00:00:00'
        );
        $this->update($data, $this->getAdapter()->quoteInto('sticker_by_category_id = ?', $sticker->getId()));


        //Применяем стикер только если он стандартный
        if($sticker->getPositionType() != Fenix_Sale_Model_Sticker::POSITION_DEFAULT) return 0;

        //Проверяем дату с которой включаеться стикер
        $now   = strtotime('Now');
        $begin = strtotime($sticker->getBeginDate());
        if ($now < $begin)
            return 0;

        //Проверяем есть ли выбранные категории
        if(count($sticker->getCategories())==0)
            return 0;

        //Собираем select для товаров
        $this->setTable('catalog_relations');
        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->getTable()
        ), null);

        // Джоиним товары
        $cols = array(
            'id'
        );

        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'p.id = r.product_id', $cols);

        $Select->where('r.parent IN("'.implode('","',$sticker->getCategories()).'")');
        $Select->where(
            $this->getAdapter()->quoteInto('p.sticker_by_category = ?', '') . ' OR ' .
            $this->getAdapter()->quoteInto('p.sticker_by_category_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            'p.sticker_by_category_date < NOW()'
            );

        $Select->group('p.id');

        $list = $this->fetchAll($Select);

        // Созраняем данные стикера в товары
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_category_id'   => $sticker->getId(),
            'sticker_by_category'      => $sticker->getImage('clearData'),
            'sticker_by_category_date' => $sticker->getEndDate()
        );

        $updateCount = 0;
        foreach ($list as $item) {
            $updateCount += $this->update($data, $this->getAdapter()->quoteInto('id = ?', $item->id));
        }

        return $updateCount;
    }

    /**
     * Применяем стикер по его товарам
     * @param $sticker
     */
    public function applyStickerByProducts($sticker){

        //Обнуляем наклейки на товарах если они были поставлены по текущему стикеру
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_product_id'   => 0,
            'sticker_by_product'      => '',
            'sticker_by_product_date' => '0000-00-00 00:00:00'
        );
        $this->update($data, $this->getAdapter()->quoteInto('sticker_by_product_id = ?', $sticker->getId()));

        //Применяем стикер только если он стандартный
        if($sticker->position_type != Fenix_Sale_Model_Sticker::POSITION_DEFAULT) return 0;

        //Проверяем дату с которой включаеться стикер
        $now   = strtotime('Now');
        $begin = strtotime($sticker->getBeginDate());
        if ($now < $begin)
            return false;

        //Собираем select для товаров
        $this->setTable('catalog_products');
        $Select = $this->select()
                       ->setIntegrityCheck(false);

        // Джоиним товары
        $cols = array(
            'id'
        );
        $Select ->from(array(
            'p' => $this->getTable()
        ), $cols);
        $Select ->join(array(
            'ssp' => $this->getTable('sale_sticker_products')
        ), 'p.id = ssp.product_id AND ssp.sticker_id = ' . $sticker->getId(), null);

        $Select->where(
            $this->getAdapter()->quoteInto('p.sticker_by_product = ?', '') . ' OR ' .
            $this->getAdapter()->quoteInto('p.sticker_by_product_date = ?', '0000-00-00 00:00:00') . ' OR ' .
            'p.sticker_by_category_date < NOW()'
        );

        $Select->group('p.id');

        $list = $this->fetchAll($Select);

        // Созраняем данные стикера в товары
        $this->setTable('catalog_products');
        $data = array(
            'sticker_by_product_id'   => $sticker->getId(),
            'sticker_by_product'      => $sticker->getImage('clearData'),
            'sticker_by_product_date' => $sticker->getEndDate()
        );

        $updateCount = 0;
        foreach ($list as $item) {
            $updateCount += $this->update($data, $this->getAdapter()->quoteInto('id = ?', $item->id));
        }

        return $updateCount;
    }
}