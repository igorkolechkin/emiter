<?php
class Fenix_Sale_Model_Backend_Products extends Fenix_Resource_Model
{
    public function getProductsBrowser($blockId, $name = 'saleProducts')
    {
        if($name == 'sku_from'){
            return Fenix::getHelper('sale/backend_sale')->getProductsFilter(array(
                'name'     => $name,
                'url'      => Fenix::getUrl('catalog/products/find'),
                'products' => self::getProductsFromListByBlockId($blockId)
            ));
        }

        return Fenix::getHelper('sale/backend_sale')->getProductsFilter(array(
            'name'     => $name,
            'url'      => Fenix::getUrl('catalog/products/find'),
            'products' => self::getProductsListByBlockId($blockId)
        ));
    }
    public function getProductsListByBlockId($blockId)
    {
        $cacheId = 'getProductsListByBlockId_' . $blockId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $this->setTable('sale_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), array('*'));

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());

        //$Select ->where('p.in_stock = ?', '1');
        $Select ->where('_p.record_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }
    public function getProductsListByBlockIdLite($blockId)
    {
        $cacheId = 'getProductsListByBlockIdLite_' . $blockId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $this->setTable('sale_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), array('*'));

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', array('p.sku','p.id'));

        //$Select ->where('p.in_stock = ?', '1');
        $Select ->where('_p.record_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    public function getProductsFromListByBlockId($blockId)
    {
        $cacheId = 'getProductsFromListByBlockId_' . $blockId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $this->setTable('sale_products_from');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), array('*'));

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());

        //$Select ->where('p.in_stock = ?', '1');
        $Select ->where('_p.record_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

}