<?php
class Fenix_Sale_Model_Backend_Sale extends Fenix_Resource_Model
{
    /**
     * Скидкапо идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecordById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getSaleAsSelect()
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('sale/sale');

        $this->setTable('sale');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));
        $Select->order('a.priority asc');
        $Select->order('a.begin_date asc');
        return $Select;
    }

    /**
     * Новая скидка
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addRecord($Form, $req)
    {
        $req->setPost('create_date', date('Y-m-d H:i:s'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));
        $attr_set = $req->getPost('attributeset');

        $id = $Form->addRecord($req);

        // Удаляем все старые связи
        $this->setTable('sale_relations')
            ->delete('record_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'record_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }
        //Обновляем привязки по поставщику
        self::updateSupplierRelations($id,$req);

        //Сохраняем товары
        self::updateSaleProducts($id,$req);

        //Сохраняем товары от которых зависит скидка
        self::updateFromProducts($id,$req);
        $this->importSaleProductByFile($id,$req,'saleFile');

        //Сохраняем категории
        self::updateSaleCategories($id,$req,'From');
        self::updateSaleCategories($id,$req,'To');
        self::updateSaleCategoriesView($id, $req);

        //Импорт артикулов по файлам
        $this->importSkuByFile($id,$req,'from');
        if($attr_set != 'sale'){
            $this->applySale($id,$req);
        }
//        else{
//            $this->updateSaleProducts($id, $req);
//        }


        return (int) $id;
    }

    /**
     * Редактировать скидку
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editRecord($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));
        $req->setPost('begin_date', $req->getPost('begin_date') . ' ' . $req->getPost('begin_time'));
        $req->setPost('end_date', $req->getPost('end_date') . ' ' . $req->getPost('end_time'));
        $attr_set = $req->getPost('attributeset');
        //Fenix::dump($req->getPost());

        $id = $Form->editRecord($current, $req);

        // Удаляем все старые связи
        $this->setTable('sale_relations')
             ->delete('record_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'record_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }

        //Обновляем привязки по поставщику
        self::updateSupplierRelations($id, $req);

        //Сохраняем товары

        $this->updateSaleProducts($id,$req);
        $this->importSaleProductByFile($id,$req,'saleFile');

        //Сохраняем товары от которых зависит скидка
        self::updateFromProducts($id,$req);

        //Сохраняем категории
        self::updateSaleCategories($id, $req,'From');
        self::updateSaleCategories($id, $req,'To');
        self::updateSaleCategoriesView($id, $req);

        //Импорт артикулов по файлам
        $this->importSkuByFile($id,$req,'from');
        $this->importSkuByFile($id,$req,'to');

        if($attr_set != 'sale'){
            $this->applySale($id,$req);
        }
//        else{
//            $this->updateSaleProducts($id, $req);
//        }
        return (int) $id;
    }

    /**
     * Удаление статьи
     *
     * @param $current
     */
    public function deleteRecord($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('sale/sale', $current->attributeset)
                 ->deleteRecord($current);

        $this->setTable('sale_relations')
             ->delete('record_id = ' . (int) $current->id);
    }

    public function updateSupplierRelations($id, $req){

        if ($req->getPost('suppliers')) {
            // Удаляем все старые связи
            $this->setTable('sale_suppliers_relations')
                 ->delete('record_id = ' . (int)$id);
            // Добавляем новые
            foreach ((array)$req->getPost('suppliers') AS $_supplier) {
                $this->insert(array(
                    'record_id' => (int)$id,
                    'supplier'  => (string)$_supplier
                ));
            }
        }
    }
    public function updateSaleProducts($id, $req){
        $list = (array)$req->getPost('_list');

        // Удаляем все старые связи
        $this->setTable('sale_products')
             ->delete('record_id = ' . (int)$id);

        if ($list && isset($list['saleProducts'])) {
            // Добавляем новые
            foreach ($list['saleProducts'] AS $position => $_product_id) {
                $this->insert(array(
                    'record_id'  => (int)$id,
                    'product_id' => (int)$_product_id,
                    'position'   => (int)$position
                ));
            }
        }
    }
    public function updateFromProducts($id, $req){

        $list = (array)$req->getPost('_list');
        if ($list && isset($list['sku_from'])) {
            // Удаляем все старые связи
            $this->setTable('sale_products_from')
                 ->delete('record_id = ' . (int)$id);
            // Добавляем новые
            foreach ($list['sku_from'] AS $position => $_product_id) {
                $this->insert(array(
                    'record_id' => (int)$id,
                    'product_id'  => (int)$_product_id,
                    'position'  => (int)$position
                ));
            }
        }
    }

    /**
     * Обновляем категории блока
     * @param $blockId
     * @param $req
     */
    public function updateSaleCategories($blockId, $req, $target){

        $targetDb = strtolower($target);

        $_list = $req->getPost('_list');

        // Удаляем все старые связи
        $this->setTable('sale_categories')
             ->delete('sale_id = ' . (int)$blockId . ' AND  target = "' . $targetDb . '"');

        //Проверяем есть ли категории
        if ($_list && isset($_list['categories' . $target]) && is_array($_list['categories' . $target])) {
            // Добавляем новые
            foreach ($_list['categories' . $target] as $categoryId => $active) {
                if ($active == '1') {
                    $data = array(
                        'sale_id'    => (int)$blockId,
                        'category_id' => (int)$categoryId,
                        'target'      => (string)$targetDb
                    );
                    $this->insert($data);
                }
            }
        }
    }

    /**
     * Обновляем категории блока
     * @param $blockId
     * @param $req
     */
    public function updateSaleCategoriesView($blockId, $req)
    {
        $_list  = $req->getPost('_list');
        $target = 'View';
        // Удаляем все старые связи
        $this->setTable('sale_categories_view')
             ->delete('sale_id = ' . (int)$blockId);

        //Проверяем есть ли категории
        if ($_list && isset($_list['categories' . $target]) && is_array($_list['categories' . $target])) {
            // Добавляем новые
            foreach ($_list['categories' . $target] as $categoryId => $active) {
                if ($active == '1') {
                    $data = array(
                        'sale_id'     => (int)$blockId,
                        'category_id' => (int)$categoryId
                    );
                    $this->insert($data);
                }
            }
        }
    }
    /**
     * ID Категорий блока
     * @param $blockId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSaleCategoriesId($blockId, $target)
    {
        $cacheId = 'getSaleCategoriesId_' . $blockId . '_' . $target;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $this->setTable('sale_categories');
        $relationsList = $this->fetchAll(
            $this->getAdapter()->quoteInto('sale_id = ?', $blockId) .' AND ' .
            $this->getAdapter()->quoteInto('target = ?', $target)
        );

        $idList        = array();
        foreach ($relationsList as $relation) {
            $idList[] = $relation->category_id;
        }

        Zend_Registry::set($cacheId, $idList);

        return $idList;
    }

    public function importSkuByFile($sale_id, $req, $field){

        $fileInfo = $req->getFiles($field . '_file');

        if($fileInfo && $fileInfo->error =='0'){
            if ($field == 'to')
                $tableName = 'sale_products';
            else {
                $tableName ='sale_products_'.$field;
            }
            // Удаляем все старые связи
            $this->setTable($tableName)
                 ->delete('record_id = ' . (int)$sale_id);

            //Создаем обхект для импорта
            $file = $fileInfo->tmp_name;
            try{
                $Excel = Fenix_Excel::getObject($file);
            }catch (Exception $e){
                return;
            }

            //Перебираем листы excel
            $counter  = 0;
            foreach($Excel->getAllSheets() as $i => $sheet) {
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $headRows = 0; //Шапки нет
                $row = 0;
                //Перебираем ячейки
                $col = 0;
                if ($highestRow - $headRows > 0) {
                    for (; $row <= $highestRow; ++$row) {
                        $sku = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
                        if($sku!=''){
                            try{


                            $product  = Fenix::getModel('catalog/products')->getProductBySku($sku);
                            if($product){
                                $this->setTable($tableName);
                                $data = array(
                                    'record_id'  => (int)$sale_id,
                                    'product_id' => (int)$product->id,
                                    'position'   => (int)$counter++
                                );

                                $this->insert($data);
                            }
                            }catch (Exception $e){
                                Fenix::dump($e);
                            }
                        }
                    }
                }
            }
        }
    }


    public function importSaleProductByFile($sale_id, $req, $field){

        $fileInfo = $req->getFiles($field);

        if($fileInfo && $fileInfo->error =='0'){

            $tableName = 'sale_products';

            // Удаляем все старые связи
            $this->setTable($tableName)
                 ->delete('record_id = ' . (int)$sale_id);

            //Создаем обхект для импорта
            $file = $fileInfo->tmp_name;
            try{
                $Excel = Fenix_Excel::getObject($file);
            }catch (Exception $e){
                return;
            }

            //Перебираем листы excel
            $counter  = 0;
            foreach($Excel->getAllSheets() as $i => $sheet) {
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $headRows = 0; //Шапки нет
                $row = 0;
                //Перебираем ячейки
                $col = 0;
                if ($highestRow - $headRows > 0) {
                    for (; $row <= $highestRow; ++$row) {
                        $sku = trim($sheet->getCellByColumnAndRow($col, $row)->getValue());
                        if($sku!=''){
                            try{


                                $product  = Fenix::getModel('catalog/products')->getProductBySku($sku);
                                if($product){
                                    $this->setTable($tableName);
                                    $data = array(
                                        'record_id'  => (int)$sale_id,
                                        'product_id' => (int)$product->id,
                                        'position'   => (int)$counter++
                                    );


                                    $this->insert($data);
                                }
                            }catch (Exception $e){
                                Fenix::dump($e);
                            }
                        }
                    }
                }
            }
        }
    }


    public function applySale($saleId, $req){

        //Удаляем старые данные о скидке
        $this->setTable('sale_discount');
        $count = $this->delete($this->getAdapter()->quoteInto('sale_id = ?', $saleId));

        $sale = Fenix::getCollection('sale/sale')->loadSaleById($saleId);

        //Не применяем скидку если она только для фроненда
        if($sale->getOnlyView()==Fenix::IS_ACTIVE){
            return;
        }
        $Select = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect();

        if (count($sale->categoriesFrom) > 0) {
            $Select->where('r.parent IN(' . implode(',', $sale->categoriesFrom) . ')');
        }
        $products = Fenix::getModel('sale/backend_products')->getProductsListByBlockIdLite($saleId);
        $productIds = array();
        foreach($products as $product){
            $productIds[] = $product->product_id;
        }
        if (count($productIds) > 0) {
            $Select->orWhere('p.id IN(' . implode(',', $productIds) . ')');
        }
        $saleProducts = $this->fetchAll($Select);



        $data = array(
            'sale_id'         => $saleId,
            'sale_begin_date' => $sale->getBeginDate(),
            'sale_end_date'   => $sale->getEndDate(),
            'sale_discount'   => $sale->getDiscount()
        );
        foreach($saleProducts as $product){
            $data['product_id'] = $product->id;
            $this->setTable('sale_discount');
            $this->insert($data);
        }
    }

    /**
     * ID Категорий блока для списка
     * @param $saleId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSaleViewCategoriesId($saleId)
    {
        $this->setTable('sale_categories_view');
        $relationsList = $this->fetchAll($this->getAdapter()->quoteInto('sale_id = ?', $saleId));
        $idList        = array();
        foreach ($relationsList as $relation) {
            $idList[] = $relation->category_id;
        }
        return $idList;
    }
}