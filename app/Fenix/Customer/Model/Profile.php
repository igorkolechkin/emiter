<?php

	class Fenix_Customer_Model_Profile extends Fenix_Resource_Model {
		/**
		 * Активация профиля пользователя
		 *
		 * @param $customer
		 *
		 * @return void
		 */
		public function activateProfile($customer) {
			$data = array(
				'is_active'     => '1',
				'activate_date' => new Zend_Db_Expr('NOW()'),
			);

			$this->setTable('customer')->update(
				$data,
				'id = ' . (int)$customer->id
			);

			return;
		}

		/**
		 * Профиль пользователя по его GUID
		 *
		 * @param $guid
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getProfileByGuid($guid) {
			$Select = $this->setTable('customer')
			               ->select()
			               ->from($this)
			               ->where('activate_key = ?', $guid)
			               ->limit(1);

			$Result = $this->fetchRow($Select);

			return $Result;
		}

		/**
		 * Шифрование пароля
		 *
		 * @param string $password Пароль
		 * @param string $salt     Соль
		 *
		 * @return string
		 */
		public function enctypePassword($password, $salt) {
			return md5(sha1($password) . sha1($salt));
		}

		/**
		 * Регистрация аккаунта
		 *
		 * @param Fenix_Controller_Request_Http $req
		 *
		 * @return void
		 */
		public function registerProfile($Form) {
			$req = Fenix::getRequest();

			$data = array(
				'recovery_code' => $req->getPost('code_word') ? $req->getPost('code_word') : '',
				'fullname'      => $req->getPost('fullname') ? $req->getPost('fullname') : '',
				'firstname'     => $req->getPost('firstname') ? $req->getPost('firstname') : '',
				'lastname'      => $req->getPost('lastname') ? $req->getPost('lastname') : '',
				'cellphone'     => $req->getPost('cellphone') ? $req->getPost('cellphone') : '',
				'email'         => $req->getPost('email') ? $req->getPost('email') : '',
				'type'          => $req->getPost('type') ? $req->getPost('type') : '',
				'password'      => $this->enctypePassword($req->getPost('password'), null),
				'register_date' => new Zend_Db_Expr('NOW()'),
			);

			$data['is_active'] = '1';
			$data['activate_key'] = Fenix::getGuid();

			//Добавляем в список емейлов
			/*Fenix::getModel('catalog/backend_emails')->addEmail(array(
				'email'     => $req->getPost('email') ? $req->getPost('email') : '',
				'cellphone' => $req->getPost('cellphone') ? $req->getPost('cellphone') : ''
			));*/

			$result = Fenix::getModel('core/mail')->sendUserMail(array(
				'to'       => $data['email'],
				'template' => 'customer.register', // шаблон письма
				'assign'   => $data,
			));

			if($result === true) {
				$this->setTable('customer')
				     ->insert($data);
			} else {
				Fenix::redirect('customer/register/fail');
			}

			return $result;
		}

		public function saveProfile($customer, $req) {
			$data = array(
				'lastname'  => $req->getPost('lastname'),
				'firstname' => $req->getPost('firstname'),
				'fullname'  => $req->getPost('fullname'),
				'cellphone' => $req->getPost('cellphone'),
				'email'     => $req->getPost('email'),
				'password'  => ($req->getPost('password') != null ? Fenix::getModel('customer/profile')->enctypePassword($req->getPost('password'), null) : $customer->password),
			);

			$this->setTable('customer')->update(
				$data,
				'id = ' . (int)$customer->id
			);

			return;
		}

		public function sendForgotMail($req) {
			$email = trim($req->getPost('email'));
			$user = Fenix::getModel('customer/profile')->getCustomerByEmail($email);
			$activate_limit = strtotime('+1 hour');

			$data = array(
				'recovery_key'        => Fenix::getGuid(),
				'activate_date_limit' => date('Y-m-d H:i:s', $activate_limit),
			);

			$id = $this->setTable('customer')->update(
				$data, $this->getAdapter()->quoteInto('email = ?', $email)
			);

			return Fenix::getModel('core/mail')->sendUserMail(array(
				'to'       => $req->getPost('email'),
				'template' => 'customer.recovery.request',
				'assign'   => array(
					'key'   => $data['recovery_key'],
					'name'  => (trim($user->fullname) != '' ? $user->fullname : null),
					'email' => $user->email,
					'date'  => date('Y/m/d H:i:s e P', $activate_limit),
				),
			));
		}

		public function getCustomerByEmail($email) {
			$Select = $this->setTable('customer')
			               ->select()
			               ->from($this->_name)
			               ->where('email = ?', (string)$email);

			return $this->fetchRow($Select);
		}

		/**
		 * Пользователь по ключу восстановления
		 *
		 * @param $key
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getUserByRecoveryKey($key) {
			$Select = $this->setTable('customer')
			               ->select()
			               ->from($this)
			               ->where('recovery_key = ?', $key)
			               ->where('activate_date_limit > NOW()')
			               ->limit(1);

			return $this->fetchRow($Select);
		}

		/**
		 * Выслать новый пароль
		 *
		 * @param $customer
		 */
		public function setRecoverPassword($customer, $req) {
			$data = array(
				'password'     => ($req->getPost('password') != null ? Fenix::getModel('customer/profile')->enctypePassword($req->getPost('password'), null) : $customer->password),
				'recovery_key' => '',
			);
			$this->setTable('customer')->update(
				$data,
				'id = ' . (int)$customer->id
			);

			return Fenix::getModel('core/mail')->sendUserMail(array(
				'to'       => $customer->email,
				'template' => 'customer.recovery.response',
				'assign'   => array(
					'name'  => (trim($customer->fullname) != '' ? $customer->fullname : null),
					'email' => $customer->email,
				),
			));
		}
	}