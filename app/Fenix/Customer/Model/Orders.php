<?php
class Fenix_Customer_Model_Orders extends Fenix_Resource_Model
{
    public function getOrdersListAsSelect($userId)
    {
        $Select = $this->setTable('checkout_orders')
                       ->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'co' => $this->_name
        ), array(
            '*',
            new Zend_Db_Expr('(SELECT SUM(cp.price * cp.qty) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id) AS total_sum'),
            new Zend_Db_Expr('(SELECT SUM(cp.qty) FROM ' . $this->getTable('checkout_products') . ' AS cp WHERE cp.order_id = co.id) AS total_qty')
        ));


        $Select ->where('co.customer_id = ?', (int) $userId)
                ->where('co.abandoned = ?', '0');
        $Select ->order('co.create_date desc');

        return $Select;
    }

    public function getOrdersList($userId)
    {
        $Select = $this->getOrdersListAsSelect($userId);


        $this->setTable('checkout_orders');
        return $this->fetchAll($Select);
    }
}