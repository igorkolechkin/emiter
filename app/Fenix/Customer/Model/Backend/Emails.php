<?php
class Fenix_Customer_Model_Backend_Emails extends Fenix_Resource_Model
{
    public function isEmail($data)
    {
        $this   ->setTable('mailbox');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('email = ?', $data['email']);

        $Result = $this->fetchRow($Select);

        if($Result)
            return true;
        else
            return false;

        return $Result;
    }


    public function getEmailById($id)
    {
        $this   ->setTable('mailbox');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', $id);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function deleteEmail($current)
    {
        $this->setTable('mailbox');
        $this->delete('id = ' . (int) $current->id);

        return (int) $current->id;
    }

    public function getEmailListAsSelect()
    {
        $this->setTable('mailbox');

        $Select = $this->select();
        //->setIntegrityCheck(false)

        $Select ->from($this->_name);

        return $Select;
    }

    /**
     * Новый профиль
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор клиента
     */
    public function addEmail($_data)
    {
        $this->setTable('mailbox');

        // Формируем массив
        $data = array(
            'email'         => $_data['email'],
            'cellphone'     => isset($_data['cellphone']) ? $_data['cellphone'] : ''
        );

        // Вставляем в базу
        $this->insert($data);

        return true;
    }

    /**
     * Оформление заказа
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function sendMail($id)
    {
        $this   ->setTable('mailbox');
        $Select = $this->select();
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        if (($email = $this->fetchRow($Select)) != null) {
            Fenix::getModel('core/mail')->sendUserMail(array(
                'to'       => $email->email,
                'template' => 'customer_mailbox', // шаблон письма
            ));

            return true;
        }

        return false;
    }
}