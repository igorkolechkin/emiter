<?php
class Fenix_Customer_Model_Backend_Customer extends Fenix_Resource_Model
{
    public function getCustomerById($id)
    {
        $this   ->setTable('customer');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);

        $Result = $this->fetchRow($Select);

        return $Result;
    }


    public function getCustomerListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('customer/customer');
        $Select = $this->setTable('customer')
                       ->select()
                       ->from($this, $Engine->getColumns());
        return $Select;
    }

    /**
     * Новый профиль
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор клиента
     */
    public function addProfile($Form, $req)
    {
        $id = $Form->addRecord($req);

        Fenix::getModel('core/common')->updateTreeIndex('structure');

        return $id;
    }

    /**
     * Редактировать профиль
     *
     * @param $current
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function editProfile($Form, $current, Fenix_Controller_Request_Http $req)
    {
        $id = $Form->editRecord($current, $req);

        return $id;
    }

    /**
     * Редактировать пароль клиента(профиля)
     *
     * @param $current
     * @param Fenix_Controller_Request_Http $req
     * @return int
     */
    public function editProfilePassword($current, Fenix_Controller_Request_Http $req)
    {

        $this->setTable('customer');

        // Формируем массив
        $data = array(
            'password'      => ($req->getPost('password') != null ? Fenix::getModel('customer/profile')->enctypePassword($req->getPost('password'), null) : $current->password),
        );

        // Обновляем в базу
        $this->update($data, 'id = ' . (int) $current->id);

        return (int) $current->id;
    }

    /**
     * Удаление профиля
     *
     * @param $current
     * @return int
     */
    public function deleteProfile($current)
    {
        /*if ($current->photo != null) {
            Fenix_Engine::getInstance()->removeImage($current->photo);
        }*/

        $this->setTable('customer');
        $this->delete('id = ' . (int) $current->id);

        return (int) $current->id;
    }
}