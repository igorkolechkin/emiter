<?php
class Fenix_Customer_Model_Regions extends Fenix_Resource_Model
{
    /**
     * Активация профиля пользователя
     *
     * @param $customer
     * @return void
     */
    public function getCountriesList()
    {
        $Select = $this->setTable('country')
            ->select()
            ->from($this);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Активация профиля пользователя
     *
     * @param $customer
     * @return void
     */
    public function getUserCountries($customerId)
    {
        $Select = $this->setTable('customer_countries')
            ->select()
            ->from($this)
            ->where('uid = ?', $customerId);

        $Result = $this->fetchAll($Select);

        return $Result;
    }


}