<?php

	class Fenix_Customer_Helper_Profile extends Fenix_Resource_Helper {
		public function getSessionForm() {
			/* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
			$unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

			$Form = Fenix::getCreatorUI()->loadPlugin('Form');

			$Form->containerClass = 'loginForm-container';
			$Form->unique_id = $unique_id;
			$Form->reset = true;

			$form_id = 'loginForm_' . $unique_id;

			$Form->setId($form_id)
			     ->setClass('login-form');

			$Form->enableAjax(true);
			$Form->setAction('/session/ajax');

			//Нужно для восстановления пароля
			if($Form->getRequest()->getQuery('email') != null) {
				$Form->setDefaults(array(
					'email' => $Form->getRequest()->getQuery('email'),
				));
			}

			$Content = array();

			$Content[] = $Form->loadPlugin('Events_Session')->fetch();


			/*$Field = '
                    <script>
                        $(function(){
                            $("#loginForm form").validate({
                                rules: {
                                    email: {
                                        required: true,
                                        email: true
                                    },
                                password: "required"
                                },
                                ignore: "div:hidden input, div:hidden select",
                                messages: {
                                    email: "Введите корректный e-mail",
                                password: "Введите пароль"
                                },
                                submitHandler : function(){
                                    $("#loginForm form").attr(\'action\',\'' . Fenix::getUrl('session/ajax') . '\').ajaxSubmit({
                                        url : \'' . Fenix::getUrl('session/login') . '\',
                                        success : function(responseText){
                                            $(\'#loginFormContainer\').html(responseText)
                                        }
                                    });
                                    return false;
                                }
                            });

                        });
                    </script>';


			$Content[] = $Form->loadPlugin('Container')
			                  ->setContent($Field)
			                  ->fetch();
*/

			// E-mail
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType("email")
			              ->setName("email")
			              ->setId("email")
			              ->setValue($Form->getRequest()->getPost('email'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
			              ->fetch();

			$Content[] = $Form->loadPlugin('Row')
			                  ->setLabel(Fenix::lang("Адрес e-mail") . ':<span class="required"></span>')
			                  ->setContent($Field)
			                  ->fetch();

			// Пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType("password")
			              ->setId("pass")
			              ->setName("password")
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->fetch();
			$Content[] = $Form->loadPlugin('Row')
			                  ->setLabel(Fenix::lang("Пароль") . ':<span class="required"></span>')
			                  ->setContent($Field)
			                  ->fetch();

			// Кнопуля
			$Field = array();
			$Field[] = $Form->loadPlugin('Button')
			                ->setType("submit")
			                ->setClass('btn btn-login')
			                ->setValue(Fenix::lang("Войти"))
			                ->fetch();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("button")
			                ->setClass('btn-link btn-registration')
			                ->setOnclick('return FenixUI.registrationDialog();')
			                ->setValue(Fenix::lang("Регистрация"))
			                ->fetch();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("button")
			                ->setClass('btn-link btn-pass-recovery')
			                ->setOnclick("location = '" . Fenix::getUrl('session/forgot') . "'; return false;")
			                ->setValue(Fenix::lang("Забыли пароль?"))
			                ->fetch();

			$Content[] = $Form->loadPlugin('row')
			                  ->setClass('btn-group')
			                  ->setContent($Field)
			                  ->fetch();

			// Авторизация
			$Content[] = $Form->getView()->render('customer/block/social-auth.phtml');

			$Form->setContent($Content);
			$Form->compile();

			return $Form;
		}

		public function getPopupSessionForm() {
			/* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
			$unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

			$Form = Fenix::getCreatorUI()->loadPlugin('Form');

			$Form->containerClass = 'loginFormPopup-container';
			$Form->unique_id = $unique_id;
			$Form->reset = true;

			$form_id = 'loginFormPopup_' . $unique_id;

			$Form->setId($form_id)
			     ->setClass('login-form-popup');

			$Form->enableAjax(true);
			$Form->setAction('/session/ajaxpopup');

			$Content = array();

			$Content[] = $Form->loadPlugin('Events_Session')->fetch();


			// E-mail
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType("email")
			              ->setName("email")
			              ->setId("email")
			              ->setValue($Form->getRequest()->getPost('email'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
			              ->fetch();

			$Content[] = $Form->loadPlugin('Row')
			                  ->setLabel(Fenix::lang("Е-mail") . ':<span class="required"></span>')
			                  ->setContent($Field)
			                  ->fetch();

			// Пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType("password")
			              ->setId("pass")
			              ->setName("password")
			              ->fetch();
			$Content[] = $Form->loadPlugin('Row')
			                  ->setLabel(Fenix::lang("Пароль") . ':<span class="required"></span>')
			                  ->setContent($Field)
			                  ->fetch();

			// Кнопуля
			$Field = array();
			$Field[] = $Form->loadPlugin('Button')
			                ->setType("submit")
			                ->setClass('btn btn-login')
			                ->setValue(Fenix::lang("Войти"))
			                ->fetch();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("button")
			                ->setClass('btn-link btn-registration')
			                ->setOnclick('return FenixUI.registrationDialog();')
			                ->setValue(Fenix::lang("Регистрация"))
			                ->fetch();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("button")
			                ->setClass('btn-link btn-pass-recovery')
			                ->setOnclick("location = '" . Fenix::getUrl('session/forgot') . "'; return false;")
			                ->setValue(Fenix::lang("Забыли пароль?"))
			                ->fetch();

			$Content[] = $Form->loadPlugin('row')
			                  ->setClass('btn-group')
			                  ->setContent($Field)
			                  ->fetch();

			// Авторизация
			$Content[] = $Form->getView()->render('customer/block/social-auth.phtml');

			$Form->setContent($Content);
			$Form->compile();

			return $Form;
		}

		public function getProfileForm() {
			/* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
			$unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

			$user = Fenix::getModel('session/auth')->getUser();

			$Form = Fenix::getCreatorUI()->loadPlugin('Form');

			$Form->containerClass = 'registerForm-container';
			$Form->unique_id = $unique_id;
			//$Form->reset = true;

			$form_id = 'registerForm_' . $unique_id;

			$Form->setId($form_id)
			     ->setClass('register-form vertical');

			$Form->enableAjax(true);
			$Form->setAction('/session/ajaxprofile');

			if($user instanceof Zend_Db_Table_Row) {
				$default = $user->toArray();
				unset($default['password']);

				$Form->setDefaults($default);
			}

			$Content = array();

			$Content[] = $Form->loadPlugin('Events_Session')->fetch();

			$Content = array();
			$ContentLeft = array();
			$ContentRight = array();

			$ContentLeft[] = $Form->loadPlugin('Container')
			                      ->setContent('<div class="page-title"><h2 class="text">' . Fenix::lang('Личные данные') . '</h2></div>')
			                      ->fetch();

			$ContentRight[] = $Form->loadPlugin('Container')
			                       ->setContent('<div class="page-title"><h2 class="text">' . Fenix::lang('Смена пароля') . '</h2></div>')
			                       ->fetch();


			// Имя
			$Field = $Form->loadPlugin('Form_Text')
			              ->setName('fullname')
			              ->setClass('fullname edit-style')
			              ->setValue($Form->getRequest()->getPost('fullname'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->fetch() . '<span class="edit-icon"></span>';
			$ContentLeft[] = $Form->loadPlugin('Row')
			                      ->setLabel(Fenix::lang("ФИО") . ':')
			                      ->setContent($Field)
			                      ->fetch();

			// Телефон
			$Field = $Form->loadPlugin('Form_Text')
			              ->setDetails(Fenix::lang('*Для подтверждения заказа и уточнения деталей'))
			              ->setName('cellphone')
			              ->setClass('cellphone edit-style')
			              ->setValue($Form->getRequest()->getPost('cellphone'))
					// ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
					      ->fetch();
			$ContentLeft[] = $Form->loadPlugin('Row')
			                      ->setLabel(Fenix::lang("Телефон") . ':')
			                      ->setContent($Field)
			                      ->fetch();

			// E-mail
			$Field = $Form->loadPlugin('Form_Text')
			              ->setDetails(Fenix::lang('*Для уведомлений о статусе Вашего заказа'))
			              ->setName("email")
			              ->setClass('email edit-style')
			              ->setValue($Form->getRequest()->getPost('email'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress());

			if($Form->getRequest()->getPost('email') != null && $user->email != trim($Form->getRequest()->getPost('email'))) {
				$email_validator = Fenix::getHelper('core/validation')->getFieldValidatorDbNoRecordExists(array(
					'table' => Fenix::getModel()->getTable('customer'),
					'field' => 'email',
				));
				$email_validator->getSelect()->where('id != ?', $user->id);
				$Field->setValidator($email_validator);
			}

			$Field = $Field->fetch() . '<span class="edit-icon"></span>';

			$ContentLeft[] = $Form->loadPlugin('Row')
			                      ->setLabel(Fenix::lang("E-mail") . ':')
			                      ->setContent($Field)
			                      ->fetch();

			$ContentLeft[] = $Form->loadPlugin('row')
			                      ->setContent('<button type="submit" class="btn btn-save"><span class="text">' . Fenix::lang('Сохранить') . '</span></button>')
			                      ->fetch();


			// Старый пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType('password')
			              ->setName('old_password')
			              ->setValue('');


			if($Form->getRequest()->getPost('old_password') != null) {
				$oldpassexist = Fenix::getHelper('core/validation')->getFieldValidatorDbRecordExists(array(
					'table' => Fenix::getModel()->getTable('customer'),
					'field' => 'password',
				), array(
					'ERROR_NO_RECORD_FOUND' => Fenix::lang('Вы ввели неверный старый пароль'),
				));
				$oldpassexist->getSelect()->reset('where');
				$oldpassexist->getSelect()->where('id = ?', $user->id);
				$oldpassexist->getSelect()->where('password = ?', Fenix::getModel('customer/profile')->enctypePassword($Form->getRequest()->getPost('old_password'), null));
				$Field->setValidator($oldpassexist);
			}

			if($Form->getRequest()->getPost('password') != null || $Form->getRequest()->getPost('password_confirm') != null) {
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty(array(
					'IS_EMPTY' => Fenix::lang('Для измения пароля необходимо ввести старый пароль'),
				)));
			}

			$ContentRight[] = $Form->loadPlugin('Row')
			                       ->setLabel(Fenix::lang("Старый пароль") . ':')
			                       ->setContent($Field->fetch())
			                       ->fetch();

			// Пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType('password')
			              ->setName('password')
			              ->setValue($Form->getRequest()->getPost('password'));

			if($Form->getRequest()->getPost('password') != null) {
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty());
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorStringLength(array(
					'min' => 4,
				)));
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorIdentical(array(
					'token' => $Form->getRequest()->getPost('password_confirm'),
				)));
			}

			$ContentRight[] = $Form->loadPlugin('Row')
			                       ->setLabel(Fenix::lang("Новый пароль") . ':')
			                       ->setContent($Field->fetch() . '<div class="note">' . Fenix::lang('Минимум 4 символа') . '</div>')
			                       ->fetch();

			// Подтверждение пароля
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType('password')
			              ->setName('password_confirm')
			              ->setValue($Form->getRequest()->getPost('password_confirm'));
			if($Form->getRequest()->getPost('password') != null) {
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty());
			}

			$ContentRight[] = $Form->loadPlugin('Row')
			                       ->setLabel(Fenix::lang("Новый пароль еще раз") . ':')
			                       ->setContent($Field->fetch())
			                       ->fetch();

			$ContentRight[] = $Form->loadPlugin('row')
			                       ->setContent('<button type="submit" class="btn btn-save"><span class="text">' . Fenix::lang('Сохранить') . '</span></button>')
			                       ->fetch();


			$Content[] = $Form->loadPlugin('Container')
			                  ->setClass('column-left')
			                  ->setContent($ContentLeft)
			                  ->fetch();
			$Content[] = $Form->loadPlugin('Container')
			                  ->setClass('column-right')
			                  ->setContent($ContentRight)
			                  ->fetch();

			$Form->setContent(
				$Form->loadPlugin('Container')
				     ->setClass('row-fluid')
				     ->setContent($Content)
				     ->fetch()
			);
			$Form->compile();

			return $Form;
		}

		/**
		 * Форма регистрации пользователя
		 *
		 * @param array $options
		 *      array(
		 *      'isAjax'=>true //использовать Ajax
		 *      )
		 *
		 * @return Fenix_Creator_Abstract
		 */
		public function getRegisterForm() {
			/* Генерируем уникальный id для возможности пользования одной формы множество раз на одной странице */
			$unique_id = (Fenix::getRequest()->getParam('unique_id') == null) ? Fenix::getRand(5) : Fenix::getRequest()->getParam('unique_id');

			$Form = Fenix::getCreatorUI()->loadPlugin('Form');

			$Form->containerClass = 'registerForm-container';
			$Form->unique_id = $unique_id;
			$Form->reset = true;

			$form_id = 'registerForm_' . $unique_id;

			$Form->setId($form_id)
			     ->setClass('register-form vertical');

			$Form->enableAjax(true);
			$Form->setAction('/session/ajaxregister');

			$Content = array();

			$Content[] = $Form->loadPlugin('Events_Session')->fetch();

			// Блок логина и пароля
			$Row = array();

			/*$Field = '
                    <script>
                        $(function(){
                            $("#registerForm form").validate({
                                rules: {
                                    email: {
                                        required: true,
                                        email: true
                                    },
                                    fullname: "required",
                                    cellphone: "required",
                                    password: "required",
                                    /*password_confirm: {
                                        required: true,
                                        equalTo: "#password"
                                    }*//*
                                },

                                ignore: "div:hidden input, div:hidden select",
                                messages: {
                                    email: "Введите корректный e-mail",
                                    fullname: "Введите имя",
                                    cellphone: "Введите телефон",
                                    password: "Введите пароль",
                                    /*password_confirm: {
                                        required: "Введите подтверждение пароля",
                                        equalTo: "Пароли не совпадают"
                                    }*//*
                                },
                                submitHandler : function(){
                                    $("#registerForm form").attr(\'action\',\'' . Fenix::getUrl('session/ajaxreg') . '\').ajaxSubmit({
                                        url : \'' . Fenix::getUrl('session/ajaxreg') . '\',
                                        success : function(responseText){
                                            $(\'#registerFormContainer\').html(responseText)
                                        }
                                    });
                                    return false;
                                }

                            });

                        });
                    </script>';

			$Content[] = $Form->loadPlugin('Container')
			                  ->setContent($Field)
			                  ->fetch();
*/

			// ФИО
			$Field = $Form->loadPlugin('Form_Text')
			              ->setName("fullname")
			              ->setId("fullname")
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValue($Form->getRequest()->getPost('fullname'))
			              ->fetch();

			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Имя") . ':<span class="required"></span>')
			              ->setContent($Field)
			              ->fetch();

			// Телефон
			$Field = $Form->loadPlugin('Form_Text')
			              ->setName("cellphone")
			              ->setId("cellphone")
			              ->setValue($Form->getRequest()->getPost('cellphone'))
			              ->setDetails(Fenix::lang('*Для подтверждения заказа и уточнения деталей'))
			              ->fetch();

			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Телефон") . ':<span class="required"></span>')
			              ->setContent($Field)
			              ->fetch();

			$Content[] = $Form->loadPlugin('Fieldset')
				//->setLegend("Данные пользователя")
				              ->setContent($Row)
			                  ->fetch();

			$Row = array();

			// E-mail
			$Field = $Form->loadPlugin('Form_Text')
				//->setDetails("На этот адрес будет отправлено сообщение с подтверждением регистрации.")
				          ->setType("email")
			              ->setName("email")
			              ->setValue($Form->getRequest()->getPost('email'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorDbNoRecordExists(array(
				              'table' => Fenix::getModel()->getTable('customer'),
				              'field' => 'email',
			              )))
			              ->fetch();

			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Адрес e-mail") . ':<span class="required"></span>')
			              ->setContent($Field)
			              ->fetch();

			// Пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setDetails(Fenix::lang('Минимум 4 символа'))
			              ->setType("password")
			              ->setId("password")
			              ->setName("password")
			              ->setValue($Form->getRequest()->getPost('password'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorStringLength(array(
				              'min' => 4,
			              )));

			if($Form->getRequest()->getPost('password') != null) {
				$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorIdentical(array(
					'token' => $Form->getRequest()->getPost('password_confirm'),
				)));
			}
			$Field = $Field->fetch();

			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Пароль") . ':')
			              ->setContent($Field)
			              ->fetch();

			$Field = $Form->loadPlugin('Form_Text')
			              ->setType("password")
			              ->setName("password_confirm")
			              ->setValue($Form->getRequest()->getPost('password_confirm'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->fetch();

			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Пароль еще раз") . ':')
			              ->setContent($Field)
			              ->fetch();

			// Кодовое слово
			/*$Field     = $Form->loadPlugin('Form_Text')
							  ->setDetails(Fenix::lang("Кодовое слово может понадобиться для восстановления пароля."))
							  ->setName("code_word")
							  ->setValue($Form->getRequest()->getPost('code_word'))
							  ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
							  ->fetch();

			$Row[]     = $Form->loadPlugin('Row')
							  ->setLabel(Fenix::lang("Кодовое слово") . ':<span class="required"></span>')
							  ->setContent($Field)
							  ->fetch();*/

			$Content[] = $Form->loadPlugin('Fieldset')
				//->setLegend("Данные авторизации")
				              ->setContent($Row)
			                  ->fetch();


			//			$Content[] = $Form->loadPlugin('Row')
			//				              ->setContent('<div class="terms">Регистрируясь на сайте, я соглашаюсь с <a class="dotted" href="">условиями</a>. </div>')
			//			                  ->fetch();

			// Кнопуля
			$Field = array();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("submit")
			                ->setClass('btn btn-register')
			                ->setValue(Fenix::lang("Зарегистрироваться"))
			                ->fetch();

			$Field[] = $Form->loadPlugin('Button')
			                ->setType("button")
			                ->setClass('btn-link btn-login')
			                ->setOnclick('return FenixUI.loginDialog();')
			                ->setValue(Fenix::lang("Войти"))
			                ->fetch();

			$Content[] = $Form->loadPlugin('row')
			                  ->setClass('btn-group')
			                  ->setContent($Field)
			                  ->fetch();


			$Form->setContent($Content);
			$Form->compile();

			return $Form;
		}

		/**
		 * когда забыл пароль и вводишь email
		 */

		public function getForgotForm() {
			$Creator = Fenix::getCreatorUI();
			$Form = $Creator->loadPlugin('Form');
			$Form->setId('session-form')->setClass('forgot-form');

			// Заголовок формы
			$Content = array(
				'<h1 class="page-title"><span class="text" style="text-align: center">' . Fenix::lang("Восстановление пароля") . '</span></h1>',
				$Form->loadPlugin('Events_Session')->fetch(),
			);

			if($Form->getRequest()->getQuery('email') != null) {
				$Form->setDefaults(array(
					'email' => $Form->getRequest()->getQuery('email'),
				));
			}
			//Сообщение
			$Content[] = $Form->loadPlugin('Container')
			                  ->setStyle('margin: 15px auto;max-width:600px;')
			                  ->setContent(Fenix::lang('Чтобы восстановить пароль, пожалуйста, введите адрес электронной почты, на который зарегистрирован ваш профиль'))
			                  ->fetch();
			// E-mail
			$validator = new Zend_Validate_Db_RecordExists(array(
				'table' => Fenix::getModel()->getTable('customer'),
				'field' => 'email',
			));
			$validator->setMessage(
				Fenix::lang("Адрес e-mail не найден"),
				Zend_Validate_Db_RecordExists::ERROR_NO_RECORD_FOUND
			);
			$Field = $Form->loadPlugin('Form_Text')
			              ->setName("email")
			              ->setValue($Form->getRequest()->getPost('email'))
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
			              ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
			              ->setValidator($validator)
			              ->fetch();
			$Content[] = $Form->loadPlugin('Row')
			                  ->setStyle('max-width:600px;margin-left:auto;margin-right:auto;')
			                  ->setLabel(Fenix::lang("E-mail") . '<span class="required"></span>')
			                  ->setContent($Field)
			                  ->fetch();
			// Кнопуля
			$Content[] = $Form->loadPlugin('row')
			                  ->setStyle('width:600px;margin-left:auto;margin-right:auto;')
			                  ->setLabel("&nbsp;")
			                  ->setContent('<button type="submit" class="btn btn-login"><span class="text">' . Fenix::lang('Воcстановить') . '</span></button>')
			                  ->fetch();

			$Form->setContent($Content);
			$Form->compile();

			return $Form;
		}

		/**
		 * изменение пароля при востановлении пароля
		 */
		public function getPasswordForm() {
			$Content = array();
			$Form = Fenix::getCreatorUI()->loadPlugin('Form');

			$form_id = 'passwordForm';
			$_key = $this->getRequest()->getUrlSegment(3);
			$Form->setId($form_id)
			     ->setAction('/session/forgot/activate/' . $_key)
			     ->setClass('personalDataForm')//     ->enableAjax()
			;


			// Пароль
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType('password')
			              ->setName('password')
			              ->setValue($Form->getRequest()->getPost('password'));

			$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty());
			$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorStringLength(array(
				'min' => 4,
			)));
			$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorIdentical(array(
				'token' => $Form->getRequest()->getPost('password_confirm'),
			)));

			$Field = $Field->fetch();
			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Пароль"))
			              ->setContent($Field)
			              ->fetch();

			// Подтверждение пароля
			$Field = $Form->loadPlugin('Form_Text')
			              ->setType('password')
			              ->setName('password_confirm')
			              ->setValue($Form->getRequest()->getPost('password_confirm'));

			$Field->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty());


			$Field = $Field->fetch();
			$Row[] = $Form->loadPlugin('Row')
			              ->setLabel(Fenix::lang("Подтверждение пароля"))
			              ->setContent($Field)
			              ->fetch();
			// Кнопуля
			$Row[] = $Form->loadPlugin('row')
			              ->setContent('<button type="submit" class="btn btn-save"><span class="text">' . Fenix::lang('Сохранить') . '</span></button>')
			              ->fetch();

			$Content[] = $Form->loadPlugin('Container')
			                  ->setClass('col-md-8')
			                  ->setContent($Row)
			                  ->fetch();
			/**
			 * Контейнер
			 */
			$Form->setContent(
				$Form->loadPlugin('Container')
				     ->setClass('row-fluid')
				     ->setContent($Content)
				     ->fetch()
			);
			$Form->compile();

			return $Form;
		}
	}