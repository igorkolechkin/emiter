<?php
class Fenix_Customer_Helper_Backend_Customer extends Fenix_Resource_Helper
{
    public function getTable($customerList)
    {
        $Table = Fenix::getCreatorUI()->loadPlugin('Table_Db');
        $Table ->setTableId('cList');
        $Table ->setTitle(Fenix::lang("Управление клиентами"));
        $Table ->setData($customerList);
        $Table ->setStandartButtonset();

        // Id
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('id')
                  ->setTitle(Fenix::lang("ID"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'range',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('id')
                  ->setSqlCellNameAs('id')
                  ->setHeaderAttributes(array(
                      'width' => '50'
                  ))
        );

        // Имя
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('fullname')
                  ->setTitle(Fenix::lang("Имя"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'single',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('fullname')
                  ->setSqlCellNameAs('fullname')
                  ->setCellCallback(function($value, $data){
                      return '<a href="' . Fenix::getUrl('customer/view/id/' . $data->id) . '">' . $value . '</a>';
                  })
        );

        // Телефон
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('cellphone')
                  ->setTitle(Fenix::lang("Телефон"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'single',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('cellphone')
                  ->setSqlCellNameAs('cellphone')
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('email')
                  ->setTitle(Fenix::lang("Email"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'single',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('email')
                  ->setSqlCellNameAs('email')
        );

        // Статус
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('status')
                  ->setTitle(Fenix::lang("Статус"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'single',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('status')
                  ->setSqlCellNameAs('status')
                  ->setHeaderAttributes(array(
                      'width' => '100'
                  ))
                  ->setCellCallback(function($value){
                      switch ($value) {
                          case 'start':
                              return Fenix::lang("Старт");
                          break;
                          case 'standart':
                              return Fenix::lang("Стандарт");
                          break;
                          case 'lux':
                              return Fenix::lang("Люкс");
                          break;
                          case 'vip':
                              return Fenix::lang("VIP");
                          break;
                      }
                  })
        );

        // Счет
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('account')
                  ->setTitle(Fenix::lang("Счет"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'range',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('account')
                  ->setSqlCellNameAs('account')
                  ->setHeaderAttributes(array(
                      'width' => '75'
                  ))
        );

        // Активирован
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('is_active')
                  ->setTitle(Fenix::lang("Активирован"))
                  ->setSortable(true)
                  ->setEditable(array(
                      'html'    => 'select',
                      'options' => array(
                          'option' => array(
                              array(
                                  'name'   => '1',
                                  '_value' => Fenix::lang("Да")
                              ),
                              array(
                                  'name'   => '0',
                                  '_value' => Fenix::lang("Нет")
                              )
                          )
                      )
                  ))
                  ->setFilter(array(
                      'type'    => 'single',
                      'html'    => 'select',
                      'options' => array(
                          'option' => array(
                              array(
                                  'name'   => '',
                                  '_value' => Fenix::lang("Все")
                              ),
                              array(
                                  'name'   => '1',
                                  '_value' => Fenix::lang("Да")
                              ),
                              array(
                                  'name'   => '0',
                                  '_value' => Fenix::lang("Нет")
                              )
                          )
                      )
                  ))
                  ->setSqlCellName('is_active')
                  ->setSqlCellNameAs('is_active')
                  ->setHeaderAttributes(array(
                      'width' => '75'
                  ))
        );

        // Акшаны

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('customer/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('customer/delete/id/{$data->id}')
        ));

        return $Table;
    }

    public function getForm($currentCustomer = null)
    {
        $Content   = array();
        $Form      = Fenix::getCreatorUI()->loadPlugin('Form');

        if ($currentCustomer != null) {
            $defaults = $currentCustomer->toArray();
            unset($defaults['password']);
            $Form->setDefaults($defaults);
        }

        /*
         * Заголовок формы
         */
        $Buttonset = $Form->loadPlugin('Buttonset')
                ->setContent(array(
                  $Form->loadPlugin('Button')
                       ->appendClass('btn-success')
                       ->setValue(Fenix::lang("Сохранить"))
                       ->setType('submit')
                       ->setName('save')
                       ->fetch(),
                  $Form->loadPlugin('Button')
                       ->appendClass('btn-primary')
                       ->setValue(Fenix::lang("Сохранить и создать"))
                       ->setType('submit')
                       ->setName('save_add')
                       ->fetch(),
                  $Form->loadPlugin('Button')
                       ->appendClass('btn-warning')
                       ->setValue(Fenix::lang("Применить"))
                       ->setType('submit')
                       ->setName('apply')
                       ->fetch(),
                  $Form->loadPlugin('Button')
                       ->appendClass('btn-danger')
                       ->setValue(Fenix::lang("Отменить"))
                       ->setType('reset')
                       ->fetch(),
                  $Form->loadPlugin('Button')
                       ->appendClass('btn-inverse')
                       ->setValue(Fenix::lang("Назад"))
                       ->setType('button')
                       ->setOnclick('self.location.href=\'' . Fenix::getUrl('customer') . '\'')
                       ->fetch()
                ));

        $Title   = $Form->loadPlugin('Title')
                        ->setTitle(Fenix::lang("Новый клиент"))
                        ->setButtonSet($Buttonset)
                        ->fetch();
        $Form    ->setContent($Title);

        //Левая колонка
        $Row        = array();

        // Данные авторизации
        $Fieldset   = array();

        // Email
        $Field      = $Form->loadPlugin('Form_Text')
                           ->setValidator(new Zend_Validate_NotEmpty())
                           ->setValidator(new Zend_Validate_EmailAddress())
                           ->setName('email')
                           ->setValue($Form->getRequest()->getPost('email'));

        if ($currentCustomer == null) {
            $Field->setValidator(new Zend_Validate_Db_NoRecordExists(array(
                'table' => Fenix::getModel()->getTable('customer'),
                'field' => 'email'
            )));
        }
        else {
            if ($Form->getRequest()->getPost('email') != $currentCustomer->email) {
                $Field->setValidator(new Zend_Validate_Db_NoRecordExists(array(
                    'table' => Fenix::getModel()->getTable('customer'),
                    'field' => 'email'
                )));
            }
        }

        $Field      = $Field->fetch();
        $Row[]      = $Form->loadPlugin('Row')
                           ->setLabel(Fenix::lang("Адрес e-mail*"))
                           ->setContent($Field)
                           ->fetch();
        $Fieldset[] = $Form->loadPlugin('Fieldset')
                           ->setLegend(Fenix::lang("Данные авторизации"))
                           ->setContent($Row)
                           ->fetch();

        // Активация профиля
        $Row        = array();

        // Активирован
        $Field      = $Form->loadPlugin('Form_Checkbox')
                           ->setValidator(new Zend_Validate_NotEmpty())
                           ->setName('is_active')
                           ->setValue('1');
        if ($Form->getRequest()->getPost('is_active') == '1') {
            $Field->setChecked($Form->getRequest()->getPost('is_active'));
        }
        $Field      = $Field->fetch();

        $Row[]      = $Form->loadPlugin('Row')
                           ->setLabel(Fenix::lang("Активирован"))
                           ->setContent($Field)
                           ->fetch();
        // Ключ активации
        $Field      = $Form->loadPlugin('Form_Text')
                           ->setName('activate_key')
                           ->setValue($Form->getRequest()->getPost('activate_key'))
                           ->fetch();
        $Row[]      = $Form->loadPlugin('Row')
                           ->setLabel(Fenix::lang("Ключ активации"))
                           ->setContent($Field)
                           ->fetch();
        $Fieldset[] = $Form->loadPlugin('Fieldset')
                           ->setLegend(Fenix::lang("Активация профиля"))
                           ->setContent($Row)
                           ->fetch();

        // Профиль
        $Row        = array();

         // Имя
        $Field      = $Form->loadPlugin('Form_Text')
                           ->setName('fullname')
                           ->setValue($Form->getRequest()->getPost('fullname'))
                           ->fetch();
        $Row[]      = $Form->loadPlugin('Row')
                           ->setLabel(Fenix::lang("Имя"))
                           ->setContent($Field)
                           ->fetch();
        // Телефон
        $Field      = $Form->loadPlugin('Form_Textarea')
                           ->setName('cellphone')
                           ->setValue($Form->getRequest()->getPost('cellphone'))
                           ->fetch();
        $Row[]      = $Form->loadPlugin('Row')
                           ->setLabel(Fenix::lang("Телефон"))
                           ->setContent($Field)
                           ->fetch();
        $Fieldset[] = $Form->loadPlugin('Fieldset')
                           ->setLegend(Fenix::lang("Профиль"))
                           ->setContent($Row)
                           ->fetch();
        $Content[]  = $Form->loadPlugin('Container')
                           ->setClass('span6')
                           ->setContent($Fieldset)
                           ->fetch();

        // Контейнер
        $Form    ->setContent(
            $Form->loadPlugin('Container')
                 ->setClass('row-fluid')
                 ->setContent($Content)
                 ->fetch()
        );

        $Form ->compile();
        return $Form;
    }

    /**
     * Форма рдактирования клиентcкого пароля
     */
    public function getPasswordForm($currentCustomer = null)
    {
        $Content   = array();
        $Row        = array();
        $Fieldset   = array();

        $Form      = Fenix::getCreatorUI()->loadPlugin('Form');

        // Заголовок формы
        $Buttonset = $Form->loadPlugin('Buttonset')
            ->setContent(array(
                $Form->loadPlugin('Button')
                    ->appendClass('btn-success')
                    ->setValue(Fenix::lang("Сохранить"))
                    ->setType('submit')
                    ->setName('save')
                    ->fetch(),
                $Form->loadPlugin('Button')
                    ->appendClass('btn-warning')
                    ->setValue(Fenix::lang("Применить"))
                    ->setType('submit')
                    ->setName('apply')
                    ->fetch(),
                $Form->loadPlugin('Button')
                    ->appendClass('btn-danger')
                    ->setValue(Fenix::lang("Отменить"))
                    ->setType('reset')
                    ->fetch(),
            ));

        $Title   = $Form->loadPlugin('Title')
            ->setTitle(Fenix::lang("Пароль клиента"))
            ->setButtonSet($Buttonset)
            ->fetch();
        $Form    ->setContent($Title);

        // Пароль
        $Field      = $Form->loadPlugin('Form_Text')
            ->setType('password')
            ->setName('password')
            ->setValue($Form->getRequest()->getPost('password'));
        if ($currentCustomer == null) {
            $Field->setValidator(new Zend_Validate_NotEmpty());
            $Field->setValidator(new Zend_Validate_Identical(array(
                'token' => $Form->getRequest()->getPost('password_confirm')
            )));
        }
        else {
            if ($Form->getRequest()->getPost('password') != null) {
                $Field->setValidator(new Zend_Validate_NotEmpty());
                $Field->setValidator(new Zend_Validate_Identical(array(
                    'token' => $Form->getRequest()->getPost('password_confirm')
                )));
            }
        }

        $Field      = $Field->fetch();
        $Row[]      = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang("Пароль*"))
            ->setContent($Field)
            ->fetch();

        // Подтверждение пароля
        $Field      = $Form->loadPlugin('Form_Text')
            ->setType('password')
            ->setName('password_confirm')
            ->setValue($Form->getRequest()->getPost('password_confirm'));
        if ($currentCustomer == null || $Form->getRequest()->getPost('password') != null) {
            $Field      ->setValidator(new Zend_Validate_NotEmpty());
        }
        $Field      = $Field->fetch();
        $Row[]      = $Form->loadPlugin('Row')
            ->setLabel(Fenix::lang("Подтверждение пароля*"))
            ->setContent($Field)
            ->fetch();
        $Fieldset[] = $Form->loadPlugin('Fieldset')
//            ->setLegend(Fenix::lang("Данные авторизации"))
            ->setContent($Row)
            ->fetch();

        $Content[]  = $Form->loadPlugin('Container')
            ->setClass('span12')
            ->setContent($Fieldset)
            ->fetch();

        // Контейнер
        $Form    ->setContent(
            $Form->loadPlugin('Container')
                ->setClass('row-fluid')
                ->setContent($Content)
                ->fetch()
        );

        $Form ->compile();
        return $Form;
    }

}