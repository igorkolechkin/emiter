<?php
class Fenix_Customer_Helper_Backend_Emails extends Fenix_Resource_Helper
{
    public function getTable($customerList)
    {
        $Creator   = Fenix::getCreatorUI();
        $Table = $Creator->loadPlugin('Table_Db');
        $Table ->setTableId('cList');
        $Table ->setCheckall();
        $Table ->setTitle(Fenix::lang("База E-mail"));
        $Table ->setData($customerList);
        $Table ->setButtonsetMailbox();

        // Id
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('id')
                ->setTitle(Fenix::lang("ID"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'range',
                    'html' => 'text'
                ))
                ->setSqlCellName('id')
                ->setSqlCellNameAs('id')
                ->setHeaderAttributes(array(
                    'width' => '50'
                ))
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('name')
                ->setTitle(Fenix::lang("Имя"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('name')
                ->setSqlCellNameAs('name')
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('email')
                ->setTitle(Fenix::lang("Email"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('email')
                ->setSqlCellNameAs('email')
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('cellphone')
                ->setTitle(Fenix::lang("Телефон"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('cellphone')
                ->setSqlCellNameAs('cellphone')
        );

        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            //'title' => Fenix::lang("Удалить"),
            'url'   => 'emails/delete/id/{$data->id}'
        ));

        return $Table;
    }
}