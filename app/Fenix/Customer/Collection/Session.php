<?php
class Fenix_Customer_Collection_Session extends Fenix_Resource_Collection
{
    public function getCustomer($id = null)
    {
        if ($id == null) {
            $_customer  = Fenix::getModel('session/auth')->getUser();
        }
        else {
            $_customer  = Fenix::getModel('customer/backend_customer')->getCustomerById($id);
        }

        if ($_customer == null) {
            return false;
        }

        $Collection = Fenix::getCollection('customer/customer');
        $Collection ->setData($_customer->toArray());

        return $Collection;
    }
}