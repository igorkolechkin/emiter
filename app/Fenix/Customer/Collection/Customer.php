<?php
class Fenix_Customer_Collection_Customer extends Fenix_Object
{
    public function getAgency()
    {
        $agency   = Fenix::getModel('agencies/my')->getCustomerAgency($this->getData('id'));

        if ($agency == null) {
            return false;
        }

        $category = Fenix::getModel('agencies/backend_categories')->getCategoryByAgency($agency->a_id);
        return Fenix::getCollection('agencies/company')->load($agency, null);
    }

    public function getProfileUrl()
    {
        return Fenix::getUrl('seller/id/' . $this->getData('id'));
    }

    /**
     * Координаты клиента
     *
     * @return stdClass
     */
    public function getGeo()
    {
        return (object) array(
            'country' => $this->getData('geo_country'),
            'region'  => $this->getData('geo_region'),
            'city'    => $this->getData('geo_city'),
            'address' => ($this->getData('geo_region') != null ? $this->getData('geo_region') . ', ' : ' ') . $this->getData('geo_city')
        );
    }


    /**
     * Формирование полного имени пользователя
     *
     * @return mixed|null
     */
    public function getFullname()
    {
        if($this->getData('name')!=null){
            return $this->getData('name');
        }
        elseif ($this->getData('email')){
            return $this->getData('email');
        }
        /*elseif ($this->getData('cellphone')){
            return $this->getData('cellphone');
        }
        elseif ($this->getData('discount_card')){
            return $this->getData('discount_card');
        }*/

        return Fenix::lang('Личный кабинет');
    }

    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getPhoto()
    {
        if ($this->getData('photo') != null) {
            return HOME_DIR_URL . $this->getData('photo');
        }

        return false;
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function photoFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('photo') != null) {
            return Fenix_Image::frame($this->getData('photo'), $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function photoResize($width = null, $height = null)
    {
        if ($this->getData('photo') != null) {
            return Fenix_Image::resize($this->getData('photo'), $width, $height);
        }

        return false;
    }

    /**
     * Адаптируем изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function photoAdapt($width = null, $height = null)
    {
        if ($this->getData('photo') != null) {
            return Fenix_Image::adapt($this->getData('photo'), $width, $height);
        }

        return false;
    }

    public function getCellphone()
    {
        $_result = explode("\n", $this->getData('cellphone'));

        return array_filter($_result);
    }
}