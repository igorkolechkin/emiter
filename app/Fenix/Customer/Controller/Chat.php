<?php
class Fenix_Customer_Controller_Chat extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        Fenix::getModel('session/auth')->checkSession();
    }

    public function indexAction()
    {
        $Creator = Fenix::getCreatorUI();

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang('Профиль'))
                           ->fetch();

        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Личный кабинет"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Диалоги"),
                'uri'   => '',
                'id'    => 'dialogs'
            )
        ));

        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('customer/chat'),
            'title' => Fenix::lang('Диалоги'),
        ));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title
        ), array(
            $Creator->getView()->render('customer/block/chats.phtml')
        ), array(
            $Creator->loadPlugin('Events_Session')->fetch()
        ));
    }

    public function checkAction()
    {

    }
    public function sendAction()
    {

    }
}