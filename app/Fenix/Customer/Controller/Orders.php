<?php
class Fenix_Customer_Controller_Orders extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        Fenix::getModel('session/auth')->checkSession();
    }

    public function indexAction()
    {
        $user   = Fenix::getModel('session/auth')->getUser();
        $orders = Fenix::getModel('customer/orders')->getOrdersList($user->id);

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Личный кабинет"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Мои заказы"),
                'uri'   => '',
                'id'    => 'profile'
            )
        ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang('Мои заказы'))
                           ->fetch();

        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('customer'),
            'title' => Fenix::lang('Мои заказы'),
        ));

        $Creator ->getView()->assign(array(
            'ordersList' => $orders
        ));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title
        ), array(
            $Creator->getView()->render('customer/block/menu.phtml')
        ), array(
            $Creator->loadPlugin('Events_Session')->fetch(),
            $Creator->getView()->render('customer/orders.phtml')
        ));
    }

    public function detailsAction()
    {
        $orderId   = (int) $this->getRequest()->getParam('id');
        $orderInfo = Fenix::getCollection('checkout/order')->getOrderInfo($orderId);
        $products  = Fenix::getCollection('checkout/order')->getProducts($orderId);

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Личный кабинет"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Мои заказы"),
                'uri'   => Fenix::getUrl('customer/orders'),
                'id'    => 'profile'
            ),
            array(
                'label' => Fenix::lang("Детали заказа №" . $orderInfo->getId()),
                'uri'   => '',
                'id'    => 'profile'
            )
        ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Детали заказа №" . $orderInfo->getId()))
                           ->fetch();

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Детали заказа №" . $orderInfo->getId()));

        $Creator ->getView()->assign(array(
            'orderInfo' => $orderInfo,
            'products'  => $products
        ));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title
        ), array(
            $Creator->getView()->render('customer/block/menu.phtml')
        ), array(
            $Creator->loadPlugin('Events_Session')->fetch(),
            $Creator->getView()->render('customer/order-info.phtml'),
            $Creator->getView()->partial('checkout/table-info.phtml', array('products' => $products))
        ));
    }
}