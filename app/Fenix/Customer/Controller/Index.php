<?php
class Fenix_Customer_Controller_Index extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        Fenix::getModel('session/auth')->checkSession();
    }

    public function indexAction()
    {

        Fenix::redirect('customer/profile');
        $Creator = Fenix::getCreatorUI();

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang('Профиль'))
                           ->fetch();

        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('customer'),
            'title' => Fenix::lang('Личный кабинет'),
        ));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title
        ), array(
            $Creator->getView()->render('customer/block/profile.phtml')
        ), array(
            $Creator->loadPlugin('Events_Session')->fetch()
        ));
    }


}