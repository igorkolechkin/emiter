<?php
	
	class Fenix_Customer_Controller_Profile extends Fenix_Controller_Action {
		public function preDispatch() {
			Fenix::getModel('session/auth')->checkSession();
		}
		
		public function indexAction() {
			$Form = Fenix::getHelper('customer/profile')->getProfileForm();
			
			$Creator = Fenix::getCreatorUI();
			
			// Хлебные крошки
			$this->_helper->BreadCrumbs(array(
				array(
					'label' => Fenix::lang("Главная"),
					'uri'   => Fenix::getUrl(),
					'id'    => 'main',
				),
				array(
					'label' => Fenix::lang("Личный кабинет"),
					'uri'   => Fenix::getUrl('customer'),
					'id'    => 'customer',
				),
				array(
					'label' => Fenix::lang("Профиль"),
					'uri'   => '',
					'id'    => 'profile',
				),
			));
			
			$Creator->getView()
			        ->headTitle("Личный кабинет");
			
			$Creator->getView()->assign(array(
				'form'  => $Form,
				'title' => Fenix::lang('Личный кабинет'),
			));
			
			$Creator->setLayout()
			        ->render('customer/index.phtml');
			
		}
	}