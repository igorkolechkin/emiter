<?php
class Fenix_Customer_Controller_Admin_Emails extends Fenix_Controller_Action
{

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('customersAll');

    }
    public function indexAction()
    {
        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['cList'])) {
                foreach ((array) $rows['cList'] AS $_rowId) {
                    Fenix::getModel('customer/backend_emails')->sendMail($_rowId);
                }

                $count = count($rows['cList']);

                Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Отправленно " . $count . " " . Fenix::declination($count, array('писем', 'письмо', 'письма'))))
                    ->saveSession();

                Fenix::redirect('customer/emails');
            }
        }

        $customerList = Fenix::getModel('customer/backend_emails')->getEmailListAsSelect();
        $Table        = Fenix::getHelper('customer/backend_emails')->getTable($customerList);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("База Email"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Title   = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("База Email"))
            ->fetch();

        $Creator ->getView()
            ->headTitle(Fenix::lang("База Email"));

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Event->fetch(),
            $Table->fetch()
        ));

    }

    public function deleteAction()
    {
        $currentCustomer = Fenix::getModel('customer/backend_emails')->getEmailById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCustomer == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("E-mail не найден"))
                ->saveSession();

            Fenix::redirect('customer/emails');
        }

        Fenix::getModel('customer/backend_emails')->deleteEmail(
            $currentCustomer
        );

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_ERROR)
            ->setMessage(Fenix::lang("E-mail удалён"))
            ->saveSession();

        Fenix::redirect('customer/emails');
    }

}