<?php
class Fenix_Customer_Controller_Admin_Index extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('customer/customer')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('customer/chat')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('customer/chat_messages')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('customer/customer_countries')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('customer/customer_regions')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('customer/customer_cities')
                ->prepare()
                ->execute();
    }


    public function indexAction()
    {
        $customerList = Fenix::getModel('customer/backend_customer')->getCustomerListAsSelect();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление клиентами"),
                'uri'   => '',
                'id'    => 'customer'
            )
        ));

        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый клиент"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('customer/add') . '\'')
                                         ->fetch()
                             ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Управление клиентами"))
                           ->setButtonSet($Buttonset);

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Управление клиентами"));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('customer')
                           ->setTitle(Fenix::lang("Управление клиентами"))
                           ->setData($customerList)
                           ->setStandartButtonset();
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'lock',
            'title' => Fenix::lang("Изменить пароль"),
            'url'   => Fenix::getUrl('customer/password/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('customer/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('customer/delete/id/{$data->id}')
        ));

        // Рендер страницы
        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('customer/customer')
        ));
    }

    public function viewAction()
    {
        $currentCustomer = Fenix::getModel('customer/backend_customer')->getCustomerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCustomer == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Клиент не найден"))
                ->saveSession();

            Fenix::redirect('customer');
        }


        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление клиентами"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Карточка клиента"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Карточка клиента"));

        $Creator ->getView()->assign(array(
            'customer' => $currentCustomer
        ));

        $Creator ->setLayout();
        $Creator ->render('customer/card.phtml');
    }

    /**
     * Добавление профиля клиента
     */
    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление клиентами"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Новый клиент"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        // Источник
        $Form      ->setSource('customer/customer', 'default')
            ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $id = Fenix::getModel('customer/backend_customer')->addProfile(
                $Form,
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Профиль клиента создан"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('customer');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('customer/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('customer/edit/id/' . $id);
            }

            Fenix::redirect('customer');
        }

        // Тайтл страницы
        $Creator ->getView()
            ->headTitle(Fenix::lang("Добавить клиента"));

        $Creator ->setLayout()
            ->oneColumn($Form->fetch());
    }

    /**
     * Редактирование пароля клиента
     */
    public function passwordAction()
    {
        $currentCustomer = Fenix::getModel('customer/backend_customer')->getCustomerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCustomer == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Клиент не найден"))
                ->saveSession();

            Fenix::redirect('customer');
        }

        $Form = Fenix::getHelper('customer/backend_customer')->getPasswordForm($currentCustomer);

        if ($Form->ok()) {
            $id = Fenix::getModel('customer/backend_customer')->editProfilePassword(
                $currentCustomer,
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Пароль клиента изменен"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('customer');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('customer/password/id/' . $id);
            }

            Fenix::redirect('customer');
        }

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление клиентами"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Редактировать клиента"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator ->getView()
            ->headTitle(Fenix::lang("Редактировать клиента"));

        $Creator ->setLayout()->oneColumn(array(
            $Form->fetch()
        ));
    }

    /**
     * Редактирование профиля клиента
     */
    public function editAction()
    {
        $currentCustomer = Fenix::getModel('customer/backend_customer')->getCustomerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCustomer == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Клиент не найден"))
                ->saveSession();

            Fenix::redirect('customer');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление клиентами"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Редактировать клиента"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentCustomer->toArray())
            ->setData('current', $currentCustomer);

        // Источник
        $Form      ->setSource('customer/customer', 'default')
            ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $id = Fenix::getModel('customer/backend_customer')->editProfile(
                $Form,
                $currentCustomer,
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Профиль клиента отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('customer');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('customer/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('customer/edit/id/' . $id);
            }

            Fenix::redirect('customer');
        }

        // Тайтл страницы
        $Creator ->getView()
            ->headTitle(Fenix::lang("Редактировать клиента"));

        $Creator ->setLayout()
            ->oneColumn($Form->fetch());
    }

    /**
     * Удаление профиля клиента
     */
    public function deleteAction()
    {
        $currentCustomer = Fenix::getModel('customer/backend_customer')->getCustomerById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCustomer == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Клиент не найден"))
                ->saveSession();

            Fenix::redirect('customer');
        }

        Fenix::getModel('customer/backend_customer')->deleteProfile(
            $currentCustomer
        );

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Клиент удалён"))
            ->saveSession();

        Fenix::redirect('customer');
    }
}