<?php
class Fenix_Customer_Controller_Favorites extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        //Fenix::getModel('session/auth')->checkSession();
    }

    public function indexAction()
    {
        Fenix::getModel('session/auth')->checkSession();

        $user = Fenix::getModel('session/auth')->getUser();
        $list = Fenix::getModel('catalog/products_favorites')->getCustomerProductsList($user->id);

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Личный кабинет"),
                'uri'   => Fenix::getUrl('customer'),
                'id'    => 'customer'
            ),
            array(
                'label' => Fenix::lang("Избранные товары"),
                'uri'   => '',
                'id'    => 'favorites'
            )
        ));

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang('Избранные товары'))
                           ->fetch();

        $Creator ->getView()
                 ->headTitle("Избранные товары");

        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('customer/favorites'),
            'title' => Fenix::lang('Избранные товары'),
        ), $list);

        $Creator ->getView()->assign(array(
            'products' => $list
        ));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title
        ), array(
            $Creator->getView()->render('customer/block/menu.phtml')
        ), array(
            $Creator->loadPlugin('Events_Session')->fetch(),
            $Creator->getView()->render('customer/favorites.phtml')
        ));

    }

    public function sidebarAction(){
        $user = Fenix::getModel('session/auth')->getUser();
        if($user)
            $list = Fenix::getModel('catalog/products_favorites')->getCustomerProductsList($user->id);
        else
            $list = null;

        $Creator = Fenix::getCreatorUI();
        $Creator ->getView()->assign(array(
            'customer' => $user,
            'products' => $list
        ));

        echo $Creator->getView()->render('customer/favorites-sidebar.phtml');

    }
    public function processAction()
    {
        Fenix::getModel('session/auth')->checkSession();

        $user = Fenix::getModel('session/auth')->getUser();

        Fenix::getModel('catalog/products_favorites')->process(
            $user->id,
            $this->getRequest()->getPost('pid')
        );

        exit;
    }
}