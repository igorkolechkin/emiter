<?php
class Fenix_Customer_Controller_Register extends Fenix_Controller_Action
{
    public function indexAction()
    {
        $Form    = Fenix::getHelper('customer/profile')->getRegisterForm();

        if ($Form->ok()) {
            Fenix::getModel('customer/profile')->registerProfile(
               $Form
            );

            $res['success_msg'] = Fenix::getCreatorUI()
                                       ->loadPlugin('Events')
                                       ->setType(Creator_Events::TYPE_INFO)
                                       ->setMessage(Fenix::lang("Вы успешно зарегистрированны!"))
                                       ->fetch();

            $res['success_msg'] .= '<script>window.location="' . Fenix::getUrl('customer/register/success') . '"</script>';

            echo Zend_Json::encode($res);
            exit;
        }

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'   => Fenix::getUrl('customer/register'),
            'title' => Fenix::lang('Регистрация'),
        ));

        $Creator ->assign(array(
            'Form' => $Form
        ));

        $Creator ->setLayout()
                 ->render('customer/register.phtml');
    }

    public function activateAction()
    {
        $customer = Fenix::getModel('customer/profile')->getProfileByGuid(
            $this->getRequest()->getUrlSegment(3)
        );

        if($customer == null) {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Профиль пользователя с таким идентификатором не найден"))
                 ->saveSession();

            Fenix::redirect('customer/register');
        }

        Fenix::getModel('customer/profile')->activateProfile(
            $customer
        );

        /** Автоматически логинем пользователя */
        $redirect_q = $this->getRequest()->getQuery('redirect');
        if(Fenix::getModel('session/auth')->automaticLogin($customer)){
            if($redirect_q) {
                Fenix::getCreatorUI()
                     ->loadPlugin('Events_Session')
                     ->setType(Creator_Events::TYPE_OK)
                     ->setMessage(Fenix::lang("Вы успешно активировали свой профиль. Выполнен автоматический вход в Ваш личный кабинет"))
                     ->saveSession();

                $redirect = base64_decode($redirect_q);
                $redirect = str_replace(array('/' . ACP_NAME . '/', '/' . ACP_NAME), '', $redirect);

                Fenix::redirect($redirect);
            }
        } else {
            Fenix::redirect('session/login?redirect='.$redirect_q);
        }


	    $Creator = Fenix::getCreatorUI();
	    $msg = Fenix::getCreatorUI()
	                ->loadPlugin('Events')
	                ->setType(Creator_Events::TYPE_INFO)
	                ->setMessage(Fenix::lang("Вы успешно активировали свой профиль."))->fetch();

	    $Title = $Creator->loadPlugin('Title')
	                     ->setTitle('Активация аккаунта')
	                     ->fetch();


	    $Creator->getView()->assign(array(
            'title'   => $Title,
            'content' => $msg,
        ));

        $Creator->setLayout()->render('customer/activate.phtml');
    }

    public function successAction()
    {
        $Block   = Fenix::getModel('core/blocks')->getBlock('success_register');

        $Creator = Fenix::getCreatorUI();

        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle($Block->title)
                           ->fetch();

        $Creator ->getView()
                 ->headTitle($Block->title);

        $Creator ->setLayout()->oneColumn(array(
            $Title,
            $Block->content
        ));
    }

    public function failAction()
    {
        Fenix::dump('Ошибка при регистрации');
    }
}