<?php
class Fenix_Customer_Controller_Tracking extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        Fenix::getModel('session/auth')->checkSession();
    }

    public function indexAction()
    {

    }

    public function processAction()
    {
        $user = Fenix::getModel('session/auth')->getUser();

        Fenix::getModel('catalog/products_tracking')->process(
            $user->id,
            $this->getRequest()->getPost('pid')
        );

        exit;
    }
}