<?php
class Fenix_Search_Model_Search extends Fenix_Resource_Model
{
    /**
     * Поиск товаров
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findProducts($query)
    {
        $fields = array(
            'p.id_1c',
            'p.sku',
            'p.title_russian',
            'p.title',
            'p.title_ukrainian',
            'p.search_keywords'
        );

        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $like       = array();
        foreach ($fields AS $attribute)
            $like[] = $this->adapter()->quoteInto($attribute . ' LIKE ?', '%' . $query . '%');
        $like = implode(' OR ', $like);

        $cols       = $this->productColumns();
        $cols[]     = $match . ' AS relevance';

        $Select = $this->setTable('catalog_relations')
                       ->select()
                       ->setIntegrityCheck(false)
                       ->from(array(
                'r' => $this->getTable()
            ), null);

        //
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'p.id = r.product_id', $cols);

        $Select->where($match . ' OR ' . $like)
               ->where('p.is_public = ?', '1')
               ->where('p.is_visible = ?', '1')
               ->where('r.parent <> ?', '178');        //Категория (без категорий)

        if($this->getRequest()->getQuery('term')=='' || $this->getRequest()->getQuery('term') ==null)
            $Select->where('p.id = 0');

        $Select ->group('p.id');

        $Select ->order(new Zend_Db_Expr('p.in_stock = \'1\' desc'));
        $Select ->order('relevance desc');

        $perPage   = 150;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    /**
     * Поиск товаров
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findProductsFast($query)
    {
        $fields = array(
            'p.id_1c',
            'p.sku',
            'p.search_keywords',
            'p.title_ukrainian',
            'p.title_russian'

        );
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        $fields[] = 'p.title_' . $lang->name;

        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $like       = array();
        foreach ($fields AS $attribute)
            $like[] = $this->adapter()->quoteInto($attribute . ' LIKE ?', '%' . $query . '%');
        $like = implode(' OR ', $like);

        $cols       = Fenix_Catalog_Model_Products::productColumns();
        $cols[]     = $match . ' AS relevance';

        $Select = $this->setTable('catalog_products')
                       ->select()
                       ->from(array(
                           'p' => $this->getTable()
                       ), $cols)
                       ->where($match . ' OR ' . $like);
        $Select ->order(new Zend_Db_Expr('p.in_stock = \'1\' desc'));
        $Select ->order('relevance desc');

        $Select->where($match . ' OR ' . $like)
               ->where('p.is_public = ?', '1')
               ->where('p.is_visible = ?', '1');

        if($this->getRequest()->getQuery('term')=='' || $this->getRequest()->getQuery('term') ==null)
            $Select->where('p.id = 0');

        $Select ->group('p.id');

        $Select ->order(new Zend_Db_Expr('p.in_stock = \'1\' desc'));
        $Select ->order('relevance desc');

        $Select ->limit('5');

        return $this->fetchAll($Select);
    }
     /**
     * Поиск статей блога
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findBlogArticles($query)
    {
        $fields = array(
            'a.title_russian',
            'a.content_russian'
        );
        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/articles');

        $this   ->setTable('articles');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $cols = $Engine->getColumns(array('prefix' => 'a'));
        //$cols[]     = $match . ' AS relevance';

        $Select ->from(array(
            'a' => $this->_name
        ), $cols);

        $Select->where('a.is_public = ?', '1');

        $Select->where(
            $this->getAdapter()->quoteInto('title_russian LIKE ?',   '%' . $query . '%') . ' OR ' .
            $this->getAdapter()->quoteInto('content_russian LIKE ?', '%' . $query . '%')
        );


        if ($query == '' || $query == null)
            $Select->where('a.id = 0');

        //$Select ->order('relevance desc');

        $perPage   = 10;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    /** Поиск статей блога
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findClubArticles($query)
    {
        $fields = array(
            'a.title_russian',
            'a.content_russian'
        );
        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('club/club');

        $this   ->setTable('club');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $cols = $Engine->getColumns(array('prefix' => 'a'));
        //$cols[]     = $match . ' AS relevance';

        $Select ->from(array(
            'a' => $this->_name
        ), $cols);

        $Select->where('a.is_public = ?', '1');

        $Select->where(
            $this->getAdapter()->quoteInto('title_russian LIKE ?',   '%' . $query . '%') . ' OR ' .
            $this->getAdapter()->quoteInto('content_russian LIKE ?', '%' . $query . '%')
        );


        if ($query == '' || $query == null)
            $Select->where('a.id = 0');

        //$Select ->order('relevance desc');

        $perPage   = 10;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /** Поиск рубрик клуба
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findClubRubrics($query)
    {
        $fields = array(
            'a.title_russian',
            'a.content_russian'
        );
        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('club/rubric');

        $this->setTable('club_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));


        $Select->where('r.is_public = ?', '1');

        $Select->where(
            $this->getAdapter()->quoteInto('title_russian LIKE ?',   '%' . $query . '%') . ' OR ' .
            $this->getAdapter()->quoteInto('content_russian LIKE ?', '%' . $query . '%')
        );


        if ($query == '' || $query == null)
            $Select->where('r.id = 0');

        //$Select ->order('relevance desc');

        $perPage   = 10;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
 /** Поиск рубрик клуба
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findBlogRubrics($query)
    {
        $fields = array(
            'a.title_russian',
            'a.content_russian'
        );
        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/rubric');

        $this->setTable('articles_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));


        $Select->where('r.is_public = ?', '1');

        $Select->where(
            $this->getAdapter()->quoteInto('title_russian LIKE ?',   '%' . $query . '%') . ' OR ' .
            $this->getAdapter()->quoteInto('content_russian LIKE ?', '%' . $query . '%')
        );


        if ($query == '' || $query == null)
            $Select->where('r.id = 0');

        //$Select ->order('relevance desc');

        $perPage   = 10;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
    /** Поиск статей блога
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findPages($query)
    {
        $fields = array(
            'a.title_russian',
            'a.content_russian'
        );
        $match      = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/structure');

        $this   ->setTable('structure');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $cols = $Engine->getColumns(array('prefix' => 'a'));
        //$cols[]     = $match . ' AS relevance';

        $Select ->from(array(
            'a' => $this->_name
        ), $cols);

        $Select->where('a.is_public = ?', '1');

        $Select->where(
               $this->getAdapter()->quoteInto('title_russian LIKE ?',   '%' . $query . '%') . ' OR ' .
               $this->getAdapter()->quoteInto('content_russian LIKE ?', '%' . $query . '%')
        );


        if ($query == '' || $query == null)
            $Select->where('a.id = 0');

        //$Select ->order('relevance desc');

        $perPage   = 10;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);

        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }
}