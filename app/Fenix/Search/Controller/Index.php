<?php

class Fenix_Search_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        if (trim($this->getRequest()->getQuery('term') == '')) {
            $error = Fenix::lang('Введите название товара');
        }
        else {
            $productsList = Fenix::getModel('catalog/products')->findProducts(
                $this->getRequest()->getQuery('term')
            );

            $productsCollection = Fenix::getCollection('catalog/products')->setList(
                $productsList
            );
        }

        //$blogArticles = Fenix::getModel('search/search')->findBlogArticles($this->getRequest()->getQuery('term'));
        //$blogRubrics  = Fenix::getModel('search/search')->findBlogRubrics($this->getRequest()->getQuery('term'));
        //$pagesList    = Fenix::getModel('search/search')->findPages($this->getRequest()->getQuery('term'));

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::lang("Результаты поиска"),
            'uri'   => Fenix::getUrl('search'),
            'id'    => 'search'
        );


        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        if (empty($error)) {
            $this->setMeta($Creator, array(
                'url'   => Fenix::getUrl('search'),
                'title' => Fenix::lang("Результаты поиска"),
            ), $productsList);

            $Creator->getView()->assign(array(
                'products'  => $productsCollection,
                'paginator' => $productsList,

                //'blogArticles' => $blogArticles,
                //'blogRubrics'  => $blogRubrics,
                //'pagesList'    => $pagesList
            ));

            $Creator->setLayout()
                ->render('catalog/search.phtml');
        }
        else {
            $Creator->getView()->assign(array(
                'errorMessage' => $error
            ));

            $Creator->setLayout()
                ->render('catalog/error_search.phtml');
        }
    }

    public function fastAction()
    {

        $productsList = Fenix::getModel('search/search')->findProductsFast(
            $this->getRequest()->getQuery('term')
        );

        $data = array();

        foreach ($productsList AS $product) {
            $_product = Fenix::getCollection('catalog/products_product')->setProduct($product->toArray());
            $baseImage = $_product->getImageFrame(100, 51);
            //$this->view->partial('store/block/fast-search-item.phtml', array('product' => $product))
            $data[] = array(
                'imgsrc' => $baseImage,
                'sku'    => $_product->getSku(),
                'label'  => $_product->getTitle(),
                'value'  => $_product->getTitle(),
                'price'  => $_product->getPriceFormatted(),
                'uri'    => $_product->getUrl()
            );
        }

        echo Zend_Json::encode($data);

        exit;

    }
}