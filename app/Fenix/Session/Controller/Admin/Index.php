<?php
class Fenix_Session_Controller_Admin_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
    }

    /**
     * Контроллер авторизации
     *
     * @return void
     */
    public function loginAction()
    {
        $error = null;

        if ($this->getRequest()->isPost()) {
            $result = Fenix::getModel('session/backend_auth')->checkFormData(
                $this->getRequest()
            );

            if ($result === false) {
                $error = "Вы ввели не правильный логин или пароль";
            }
            else {
                $redirect = $this->getRequest()->getQuery('redirect');
                $redirect = base64_decode($redirect);
                $redirect = str_replace(array('/' . ACP_NAME . '/', '/' . ACP_NAME), '', $redirect);

                Fenix::redirect($redirect);
            }
        }

        $Creator = Fenix::getCreatorUI();

        $Creator ->assign(array(
            'error' => $error
        ));

        $Creator->setLayout('session/layout')
                ->render('session/login.phtml');
    }

    /**
     * Контроллер выхода из системы
     *
     * @return void
     */
    public function logoutAction()
    {
        Fenix::getModel('session/backend_auth')->logout();
        Fenix::redirect('');
    }
}