<?php

	class Fenix_Session_Controller_Index extends Fenix_Controller_Action {
		public function indexAction() {
			Fenix::redirect('session/login');
		}

		public function vkAction() {
			$_code = $this->getRequest()->getQuery('code');
			$accessToken = Fenix::getModel('session/vk')->getAccessToken($_code);
			$userProfile = Fenix::getModel('session/vk')->getUserProfile($accessToken);
			$userData = Fenix::getModel('session/vk')->updateUserProfile($userProfile['response'][0]);

			Fenix::getModel('session/vk')->saveSession($userData);
			echo '<script>window.opener.location.reload();self.close();</script>';
		}

		public function fbAction() {
			$_code = $this->getRequest()->getQuery('code');
			$accessToken = Fenix::getModel('session/fb')->getAccessToken($_code);

			$userProfile = Fenix::getModel('session/fb')->getUserProfile($accessToken);
			$userData = Fenix::getModel('session/fb')->updateUserProfile($userProfile);

			Fenix::getModel('session/fb')->saveSession($userData);
			echo '<script>window.opener.location.reload();self.close();</script>';
		}

		public function twAction() {
			$tw = Fenix::getModel('session/tw');

			$connection = $tw->getOauth(
				Fenix::getStaticConfig()->oauth->tw->id,
				Fenix::getStaticConfig()->oauth->tw->key
			);

			$_twOauthToken = $connection->getRequestToken(
				Fenix::getUrl('session/twauth')
			);

			$_SESSION['oauth_token'] = $token = $_twOauthToken['oauth_token'];
			$_SESSION['oauth_token_secret'] = $_twOauthToken['oauth_token_secret'];

			$url = $connection->getAuthorizeURL($token);
			header('Location: ' . $url);
		}

		public function twauthAction() {
			if($this->getRequest()->getQuery('denied')) {
				echo '<script>self.close();</script>';
				exit;
			}

			$tw = Fenix::getModel('session/tw');
			$connection = $tw->getOauth(
				Fenix::getStaticConfig()->oauth->tw->id,
				Fenix::getStaticConfig()->oauth->tw->key,
				$_SESSION['oauth_token'],
				$_SESSION['oauth_token_secret']
			);
			$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

			$connection = $tw->getOauth(
				Fenix::getStaticConfig()->oauth->tw->id,
				Fenix::getStaticConfig()->oauth->tw->key,
				$access_token['oauth_token'],
				$access_token['oauth_token_secret']
			);

			$data = Fenix::getModel('session/tw')->updateUserProfile(
				$connection->get('account/verify_credentials')
			);

			Fenix::getModel('session/tw')->saveSession($data);

			echo '<script>window.opener.location.reload();self.close();</script>';
		}

		public function loginAction() {
			if(Fenix::getModel('session/auth')->getUser() instanceof Zend_Db_Table_Row) {
				Fenix::redirect('customer');
			}

			// Хлебные крошки
			$_crumbs = array();
			$_crumbs[] = array(
				'label' => Fenix::lang("Главная"),
				'uri'   => Fenix::getUrl(),
				'id'    => 'main',
			);
			$_crumbs[] = array(
				'label' => Fenix::lang("Вход"),
				'uri'   => '',
				'id'    => 'login',
			);
			$this->_helper->BreadCrumbs($_crumbs);

			$Form = Fenix::getHelper('customer/profile')->getSessionForm();

			if($Form->ok()) {
				$Result = Fenix::getModel('session/auth')->checkFrontendFormData(
					$this->getRequest()
				);

				if($Result === true) {
					Fenix::getCreatorUI()
					     ->loadPlugin('Events_Session')
					     ->setType(Creator_Events::TYPE_OK)
					     ->setMessage(Fenix::lang("Добро пожаловать."))
					     ->saveSession();

					Fenix::redirect('customer/profile');
				} else {
					Fenix::getCreatorUI()
					     ->loadPlugin('Events_Session')
					     ->setType(Creator_Events::TYPE_ERROR)
					     ->setMessage(Fenix::lang("Вы ввели не правильный логин, пароль или не активировали свой профиль"))
					     ->saveSession();

					Fenix::redirect('session/login?email=' . $this->getRequest()->getPost('email'));
				}
			}

			$Creator = Fenix::getCreatorUI();
			$Creator->getView()
			        ->headTitle(Fenix::lang("Авторизация"));
			$Creator->setLayout()->oneColumn(array(
				$Form->fetch(),
			));
		}

		public function ajaxAction() {

			$Form = Fenix::getHelper('customer/profile')->getSessionForm();

			if($Form->ok()) {
				$Result = Fenix::getModel('session/auth')->checkUserAuth(
					$Form->getRequest()->getPost('email'),
					$Form->getRequest()->getPost('password')
				);

				if($Result === true) {
					$res['success_msg'] = Fenix::getCreatorUI()
					                           ->loadPlugin('Events')
					                           ->setType(Creator_Events::TYPE_INFO)
					                           ->setMessage(Fenix::lang("Добро пожаловать!"))
					                           ->fetch();

					$res['success_msg'] .= '<script>window.location="' . Fenix::getUrl('customer/profile') . '"</script>';
				} else {
					$res['error'] = Fenix::getCreatorUI()
					                     ->loadPlugin('Events')
					                     ->setType(Creator_Events::TYPE_ERROR)
					                     ->setMessage(Fenix::lang("Вы ввели не правильный логин/пароль, или не активировали свой профиль."))
					                     ->fetch();
					$res['error'] .= $Form->fetch();
				}
			} else {
				$res['error'] = $Form->fetch();
			}

			echo Zend_Json::encode($res);
			exit;
		}

		public function ajaxpopupAction() {

			$Form = Fenix::getHelper('customer/profile')->getPopupSessionForm();

			if($Form->ok()) {
				$Result = Fenix::getModel('session/auth')->checkUserAuth(
					$Form->getRequest()->getPost('email'),
					$Form->getRequest()->getPost('password')
				);

				if($Result === true) {
					$res['success_msg'] = Fenix::getCreatorUI()
					                           ->loadPlugin('Events')
					                           ->setType(Creator_Events::TYPE_INFO)
					                           ->setMessage(Fenix::lang("Добро пожаловать!"))
					                           ->fetch();

					$res['success_msg'] .= '<script>window.location="' . Fenix::getUrl('customer/profile') . '"</script>';
				} else {
					$res['error'] = Fenix::getCreatorUI()
					                     ->loadPlugin('Events')
					                     ->setType(Creator_Events::TYPE_ERROR)
					                     ->setMessage(Fenix::lang("Вы ввели не правильный логин/пароль, или не активировали свой профиль."))
					                     ->fetch();
					$res['error'] .= $Form->fetch();
				}
			} else {
				$res['error'] = $Form->fetch();
			}

			echo Zend_Json::encode($res);
			exit;
		}

		public function ajaxregisterAction() {
			$Form = Fenix::getHelper('customer/profile')->getRegisterForm();

			if($Form->ok()) {
				//Fenix::dump($this->getRequest()->getPost());
				$Result = Fenix::getModel('customer/profile')->registerProfile(
					$this->getRequest()
				);
				if($Result === true) {

					$res['success_msg'] = Fenix::getCreatorUI()
					                           ->loadPlugin('Events')
					                           ->setType(Creator_Events::TYPE_OK)
					                           ->setClass("alert fade in alert-info")
					                           ->setMessage(Fenix::lang('Вы успешно зарегистрировались. Проверьте почту и активируйте аккаунт'))
					                           ->fetch();

					//$res['success_msg'] .= '<script>window.location.reload()</script>';
				} else {
					$res['error'] = Fenix::getCreatorUI()
					                     ->loadPlugin('Events')
					                     ->setType(Creator_Events::TYPE_ERROR)
					                     ->setMessage('<label for="fullname" class="error">' . Fenix::lang('Данный email уже найден в базе!') . '</label>')
					                     ->fetch();
					$res['error'] .= $Form->fetch();
				}
			} else {
				$res['error'] = $Form->fetch();
			}

			echo Zend_Json::encode($res);
			exit;
		}

		public function ajaxprofileAction() {
			$res = array();
			$Form = Fenix::getHelper('customer/profile')->getProfileForm();

			if($Form->ok()) {
				Fenix::getModel('customer/profile')->saveProfile(
					Fenix::getModel('session/auth')->getUser(),
					$this->getRequest()
				);

				Fenix::getCreatorUI()
				     ->loadPlugin('Events_Session')
				     ->setType(Creator_Events::TYPE_OK)
				     ->setMessage(Fenix::lang("Информация успешно изменена"))
				     ->saveSession();
				$res['success_msg'] = Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();
			} else {
				$res['error'] = $Form->fetch();
			}

			echo Zend_Json::encode($res);
			exit;
		}

		/**
		 * Контроллер выхода из системы
		 *
		 * @return void
		 */
		public function logoutAction() {
			Fenix::getModel('session/auth')->logout();
			Fenix::redirect('');
		}
	}