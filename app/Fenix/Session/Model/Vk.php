<?php
class Fenix_Session_Model_Vk extends Fenix_Resource_Model
{
    private $_profileFields = array(
        'sex',
        'bdate',
        'city',
        'country',
        'photo_50',
        'photo_100',
        'photo_200_orig',
        'photo_200',
        'photo_400_orig',
        'photo_max',
        'photo_max_orig',
        'online',
        'online_mobile',
        'lists',
        'domain',
        'has_mobile',
        'contacts',
        'connectionsv',
        'site',
        'education',
        'universities',
        'schools',
        'can_post',
        'can_see_all_posts',
        'can_see_audio',
        'can_write_private_message',
        'status',
        'last_seen',
        'common_count',
        'relation',
        'relatives',
        'counters'
    );

    public function getAccessToken($code)
    {
        $curl = new Fenix_Curl();
        $curl ->get('https://api.vk.com/oauth/access_token', array(
            'client_id'     => Fenix::getStaticConfig()->oauth->vk->id,
            'client_secret' => Fenix::getStaticConfig()->oauth->vk->key,
            'redirect_uri'  => Fenix::getUrl('session/vk'),
            'code'          => $code
        ));
        return Zend_Json::decode($curl->response);
    }

    public function getUserProfile($accessToken)
    {
        $curl = new Fenix_Curl();
        $curl->get('https://api.vk.com/method/users.get', array(
            'user_id'       => $accessToken['user_id'],
            'v'             => 5.4,
            'access_token'  => $accessToken['access_token'],
            'fields'        => implode(',', $this->_profileFields)
        ));
        return Zend_Json::decode($curl->response);
    }

    public function updateUserProfile($userProfile)
    {
        $data = array(
            'oauth_login'   => 'vk-' . $userProfile['id'],
            'password'      => Fenix::getModel('customer/profile')->enctypePassword('vk-' . $userProfile['id'], null),
            'register_date' => new Zend_Db_Expr('NOW()'),
            'fullname'      => $userProfile['last_name'] . ' ' . $userProfile['first_name'],
            'is_active'     => '1'
        );

        $test = $this->setTable('customer')->fetchRow(
            $this->getAdapter()->quoteInto('oauth_login = ?', 'vk-' . $userProfile['id'])
        );

        if ($test == null) {
            Fenix::getModel('core/notify')->addNotify(array(
                'recipients' => 0,
                'sender'     => 0,
                'icon'       => 'timeline-indicator icon-group btn btn-warning no-hover green',
                'content'    => "Зарегистрировался новый клиент: " . $data['fullname'] . ' через Vkontakte',
                'options'    => array(
                    'widget' => array(
                        'container' => 'transparent',
                        'header' => 'hidden'
                    )
                )
            ));
            $this->insert($data);
        }
        else {
            $this->update($data, $this->getAdapter()->quoteInto('oauth_login = ?', 'vk-' . $userProfile['id']));
        }

        return $this->setTable('customer')->fetchRow(
            $this->getAdapter()->quoteInto('oauth_login = ?', 'vk-' . $userProfile['id'])
        );
    }

    public function saveSession($customer)
    {
        if ($customer == null)
            return;

        $req     = Fenix::getRequest();

        $session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . $req->getAccessLevel());

        $auth    = Zend_Auth::getInstance();
        $auth    ->setStorage($session);
        $auth    ->getStorage()
            ->write($customer);

        // Обновляем время входа
        $this->setTable('customer')->update(array(
            'login_date_last' => new Zend_Db_Expr('login_date'),
            'login_date'      => new Zend_Db_Expr('NOW()')
        ), $this->getAdapter()->quoteInto(
            'id = ?', $customer->id
        ));
    }
}