<?php

	class Fenix_Session_Model_Auth extends Fenix_Resource_Model {
		const SESSION_MAX_LIFETIME = 86400;

		protected $_authorizationPage = 'session/login';

		/**
		 * Пользователь из сессии системы
		 *
		 * @return Zend_Db_Table_Row Авторизованный пользователь
		 */
		public function getUser() {

			$auth = Zend_Auth::getInstance();
			$session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . Fenix::getRequest()->getAccessLevel());
			$auth->setStorage($session);
			$Data = $auth->getIdentity();

			if(Fenix::getRequest()->getAccessLevel() == 'frontend') {

				if($Data == null) {
					return false;
				}

				return Fenix::getModel('customer/backend_customer')->getCustomerById($Data->id);
			}

			return $Data;
		}

		/**
		 * Выход из системы
		 *
		 * @return void
		 */
		public function logout() {
			$auth = Zend_Auth::getInstance();
			$session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . Fenix::getRequest()->getAccessLevel());
			$auth->setStorage($session);
			$auth->clearIdentity();
			unset($_SESSION['editorProtectAuth']);

			Zend_Session::forgetMe();

			return;
		}

		/**
		 * Проверка данных из формы авторизации
		 *
		 * @param Fenix_Controller_Request_Http $req
		 *
		 * @return boolean
		 */
		public function checkFormData(Fenix_Controller_Request_Http $req) {
			// Создаем экземпляр класса с фильтром
			$filter = new Zend_Filter_StripTags();

			// Фильтруем введённые пользователем данные
			$login = $filter->filter($req->getPost('email'));
			$password = $filter->filter($req->getPost('password'));

			// Проверка на пустоту поля
			if($login == '' || $password == '') {
				return false;
			}

			// Устанавниваем таблицу с администраторами
			$this->setTable('admin_users');

			// Создаем экземпляр класса авторизатора
			$auth = Zend_Auth::getInstance();

			// Создаем экземпляр класса адаптера для проверки авторизации
			$authAdapter = new Zend_Auth_Adapter_DbTable(
				$this->getAdapter(),
				$this->getTable('admin_users'),
				'login',
				'password',
				"SHA1(?)"
			);

			// Устанавливаем колонки таблицы для проврки
			$authAdapter->setIdentity($login)
			            ->setCredential($password);

			// Загружаем адаптер в авторизатор
			$result = $auth->authenticate($authAdapter);

			// Проверяем введённые данные
			if($result->isValid()) {

				// Данные администратора
				$adminUserData = new Zend_Db_Table_Row(array(
					'data' => (array)$authAdapter->getResultRowObject(),
				));

				// Создаем сессию
				$session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . $req->getAccessLevel());

				$_SESSION['editorProtectAuth'] = true;

				// Устанавливаем время жизни сессии
				#$session ->setExpirationSeconds(
				#    self::SESSION_MAX_LIFETIME
				#);

				$auth->setStorage($session);

				// Записываем данные в хранилище
				$auth->getStorage()
				     ->write($adminUserData);

				// Сохраняем кукисы
				if($req->getPost('remember') == '1') {
					Zend_Session::rememberMe();
				}

				// Обновляем время входа
				//            $this->update(array(
				//                'login_date_last' => new Zend_Db_Expr('login_date'),
				//                'login_date'      => new Zend_Db_Expr('NOW()')
				//            ), $this->getAdapter()->quoteInto(
				//                'id = ?', $adminUserData->id
				//            ));

				return true;
			} else {
				return false;
			}
		}


		public function automaticLogin($customer) {
			if($this->checkUserAuth($customer->email, $customer->password, false)) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Проверка данных авторизации
		 *
		 * @param Fenix_Controller_Request_Http $req
		 *
		 * @return boolean
		 */
		public function checkUserAuth($login = null, $password = null, $encrypt_pass = true) {
			$req = Fenix::getRequest();

			// Создаем экземпляр класса с фильтром
			$filter = new Zend_Filter_StripTags();

			// Фильтруем введённые пользователем данные
			$login = $filter->filter($login);
			$password = $filter->filter($password);

			// Проверка на пустоту поля
			if(empty($login) || empty($password)) {
				return false;
			}

			if($encrypt_pass) {
				$password = Fenix::getModel('customer/profile')->enctypePassword($password, null);
			}

			// Устанавниваем таблицу с администраторами
			$this->setTable('customer');

			// Создаем экземпляр класса авторизатора
			$auth = Zend_Auth::getInstance();

			// Создаем экземпляр класса адаптера для проверки авторизации
			$authAdapter = new Zend_Auth_Adapter_DbTable($this->getAdapter(), $this->getTable('customer'), 'email', 'password');

			// Устанавливаем колонки таблицы для проврки
			$authAdapter->setIdentity($login)
			            ->setCredential($password);

			// Загружаем адаптер в авторизатор
			$result = $auth->authenticate($authAdapter);

			// Проверяем введённые данные
			if($result->isValid()) {

				// Проверка на активацию
				//            if(@$result->is_active == '0') {
				//                return false;
				//            }


				// Данные пользователя
				$customer = new Zend_Db_Table_Row(array(
					'data' => (array)$authAdapter->getResultRowObject(),
				));


				// Создаем сессию
				$session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . $req->getAccessLevel());

				// Устанавливаем время жизни сессии
//				$session->setExpirationSeconds(
//					self::SESSION_MAX_LIFETIME
//				);

				$auth->setStorage($session);

				// Записываем данные в хранилище
				$auth->getStorage()
				     ->write($customer);

				// Сохраняем кукисы
				if($req->getPost('remember') == '1') {
					Zend_Session::rememberMe();
				}

				// Обновляем время входа
				//            $this->update(array(
				//                'login_date_last' => new Zend_Db_Expr('login_date'),
				//                'login_date'      => new Zend_Db_Expr('NOW()'),
				//            ), $this->getAdapter()->quoteInto(
				//                'id = ?', $customer->id
				//            ));

				return true;
			} else {
				return false;
			}
		}

		/**
		 * Проверка сессии
		 *
		 * @return void
		 */
		public function checkSession() {
			if($this->notAuthRedirectPage()) {
				$auth = Zend_Auth::getInstance();

				$session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . Fenix::getRequest()->getAccessLevel());
				$auth->setStorage($session);

				$adminUser = $auth->getIdentity();

				if($adminUser === null) {
					$uri = Fenix::getRequest()->getRequestUri();
					Fenix::redirect($this->_authorizationPage . '?redirect=' . base64_encode($uri));
				}

				return;
			}
		}

		/**
		 * Проверка является ли страница обязательной для редиректа на авторизацию
		 * Например, если мы находимся на странице ввода логина и пароля, то без этой проверки
		 * мы получим циклическую переадресацию и бан от сервера!
		 *
		 * @return boolean
		 */
		public function notAuthRedirectPage() {
			$LoginPageTest = $this->getRequest()->getUriSegment(0) == 'session'
				&& $this->getRequest()->getUriSegment(1) == 'login';

			if(!$LoginPageTest) {
				return true;
			}

			return false;
		}
	}