<?php
class Fenix_Session_Model_Tw extends Fenix_Resource_Model
{
    private $_oauth = null;

    public function getOauth($id = null, $key = null, $access = null, $secret = null)
    {
        require_once LIB_DIR_ABSOLUTE . 'ThirdParty/Twitteroauth/twitteroauth.php';

        $this->_oauth = new TwitterOAuth(
            $id,
            $key,
            $access,
            $secret
        );

        return $this->_oauth;
    }


    public function updateUserProfile($userProfile)
    {
        if ($userProfile->id == null)
            return;

        $data = array(
            'oauth_login'   => 'tw-' . $userProfile->id,
            'password'      => Fenix::getModel('customer/profile')->enctypePassword('tw-' . $userProfile->id, null),
            'register_date' => new Zend_Db_Expr('NOW()'),
            'fullname'      => $userProfile->name,
            'is_active'     => '1'
        );

        $test = $this->setTable('customer')->fetchRow(
            $this->getAdapter()->quoteInto('oauth_login = ?', 'tw-' . $userProfile->id)
        );

        if ($test == null) {
            Fenix::getModel('core/notify')->addNotify(array(
                'recipients' => 0,
                'sender'     => 0,
                'icon'       => 'timeline-indicator icon-group btn btn-warning no-hover green',
                'content'    => "Зарегистрировался новый клиент: " . $data['fullname'] . ' через Twitter',
                'options'    => array(
                    'widget' => array(
                        'container' => 'transparent',
                        'header' => 'hidden'
                    )
                )
            ));

            $this->insert($data);
        }
        else {
            unset($data['fullname']);
            $this->update($data, $this->getAdapter()->quoteInto('oauth_login = ?', 'tw-' . $userProfile->id));
        }

        return $this->setTable('customer')->fetchRow(
            $this->getAdapter()->quoteInto('oauth_login = ?', 'tw-' . $userProfile->id)
        );
    }

    public function saveSession($customer)
    {
        if ($customer == null)
            return;

        $req     = Fenix::getRequest();

        $session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_' . $req->getAccessLevel());

        $auth    = Zend_Auth::getInstance();
        $auth    ->setStorage($session);
        $auth    ->getStorage()
            ->write($customer);

        // Обновляем время входа
        $this->setTable('customer')->update(array(
            'login_date_last' => new Zend_Db_Expr('login_date'),
            'login_date'      => new Zend_Db_Expr('NOW()')
        ), $this->getAdapter()->quoteInto(
            'id = ?', $customer->id
        ));
    }
}