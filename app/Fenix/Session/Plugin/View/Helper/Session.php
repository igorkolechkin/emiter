<?php
class Fenix_Session_Plugin_View_Helper_Session extends Zend_View_Helper_Abstract
{
    public function __construct(){}
    
    public function session()
    {
        return Fenix::getModel('session/auth')->getUser();
    }
}