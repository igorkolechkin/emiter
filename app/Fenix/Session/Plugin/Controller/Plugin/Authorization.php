<?php
class Fenix_Session_Plugin_Controller_Plugin_Authorization extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getAccessLevel() == 'backend') {
            Fenix::getModel('session/auth')->checkSession();
        }
    }
}