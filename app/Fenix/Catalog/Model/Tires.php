<?php
class Fenix_Catalog_Model_Tires extends Fenix_Resource_Model
{
    const TIRES_TABLE = 'podbor_shini_i_diski';
    /**
     * @return Zend_Db_Select
     */
    public function getTiresSelect()
    {
        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('*'));

        return $Select;
    }

    /**
     * Список производителей
     * @return mixed
     */
    public function getVendorsList(){

        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('vendor'));

        $Select->group('vendor');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список моделей
     * @return mixed
     */
    public function getCarsList($vendor = null){

        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('car'));

        if($vendor)
            $Select->where('vendor = ?', $vendor);

        $Select->group('car');

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список годов выпуска
     * @return mixed
     */
    public function getYearsList($car = null){

        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('year'));

        $cache = $this->getCache();

        if(isset($cache->vendor))
            $Select->where('vendor = ?', $cache->vendor);

        if($car)
            $Select->where('car = ?', $car);

        $Select->group('year');

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список модификаций
     * @return mixed
     */
    public function getModificationList($year = null){

        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('modification'));

        $cache = $this->getCache();

        if(isset($cache->vendor))
            $Select->where('vendor = ?', $cache->vendor);
        if(isset($cache->car))
            $Select->where('car = ?', $cache->car);

        if($year)
            $Select->where('year = ?', $year);

        $Select->group('modification');


        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getTires(){
        $Select = $this->setTable(Fenix_Catalog_Model_Tires::TIRES_TABLE)
                       ->select();

        $Select->from(array(
            't' => $this->_name
        ), array('*'));

        $cache = $this->getCache();

        if(isset($cache->vendor))
            $Select->where('vendor = ?', $cache->vendor);
        if(isset($cache->car))
            $Select->where('car = ?', $cache->car);
        if(isset($cache->year))
            $Select->where('year = ?', $cache->year);

        if(isset($cache->modification))
            $Select->where('modification = ?', $cache->modification);

        $Result = $this->fetchAll($Select);
//Fenix::dump($Select->assemble());
        return $Result;
    }

    public function prepareFilter(){

        $tires = $this->getTires();
        $data = array();
        $zavod = array();
        $zamen = array();
        $zavodDiski = array();
        $zamenDiski = array();
        foreach ($tires as $_tire){

            $buf = explode('|', $_tire->zavod_shini);
            foreach($buf as $i=>$item){
                $zavod = array_merge(explode('#', $item), $zavod);
            }
            $buf = explode('|', $_tire->zamen_shini);
            foreach($buf as $i=>$item){
                $zamen = array_merge(explode('#', $item), $zavod);
            }

            $buf = explode('|', $_tire->zavod_diskov);
            foreach($buf as $i=>$item){
                $zavodDiski = array_merge(explode('#', $item), $zavodDiski);
            }
            $buf = explode('|', $_tire->zamen_diskov);
            foreach($buf as $i=>$item){
                $zamenDiski = array_merge(explode('#', $item), $zamenDiski);
            }

        }
        $data = array_merge($zavod,$zamen,$zavodDiski,$zamenDiski);

        foreach($data as $i=>$item)
        {
            $data[$i] = trim($item);
        }
        $cache = $this->getCache();

        $cache->filter = $data;

        return $data;
    }
    public function prepareFilterSelect(Zend_Db_Table_Select $Select){
        $cache = $this->getCache();

        if(isset($cache->filter) && is_array($cache->filter) && count($cache->filter) > 0)

            $Select->where('label IN("' . implode('","', $cache->filter) . '")');

        return $Select;
    }
    public function getCache(){

        return new Zend_Session_Namespace('Fenix_Tyres_Cache');
    }
}