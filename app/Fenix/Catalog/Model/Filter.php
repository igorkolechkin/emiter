<?php

class Fenix_Catalog_Model_Filter extends Fenix_Resource_Model
{
    const FILTER_VALUE_SEPARATOR          = '-or-';
    const FILTER_ATTR_SEPARATOR           = '-with-';
    const FILTER_ATTR_SYS_TITLE_SEPARATOR = '=';

    /**
     * Информация о таблице
     */
    public function getTableInfo($_name)
    {
        if (Zend_Registry::isRegistered('FnxEngineTblInf_' . $_name)) {
            return Zend_Registry::get('FnxEngineTblInf_' . $_name);
        }

        //Оригинальная версия
        //$_result = $this->getAdapter()->describeTable($_name);

        $cache = Fenix_Cache::getCache('core/database_tableinfo');
        if ($cache !== false) {
            $cacheId = 'front_getTableInfo_' . $_name;

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->getAdapter()->describeTable($_name);
                $cache->save($data, $cacheId);
            }
            else {
            }
        }
        else {
            $data = $this->getAdapter()->describeTable($_name);
        }

        Zend_Registry::set('FnxEngineTblInf_' . $_name, $data);

        return $data;
    }

    /**
     * Список доступных атрибутов в категории через кеш
     *
     * @param $category
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAvailableAttributes($category = null)
    {
        //Fenix_Debug::log('app getAvailableAttributes begin');
        $categoryId = $category ? $category->getId() : '1';
        $cache = Fenix_Cache::getCache('catalog/products_attributes');
        if ($cache !== false) {
            $cacheId = 'front_getAvailableAttributes_' . $categoryId;

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->_getAvailableAttributes($category);
                $cache->save($data, $cacheId);
                //Fenix_Debug::log('app getAvailableAttributes end cache update ');
            }
            else {
                //Fenix_Debug::log('app getAvailableAttributes end cache load ');
            }
        }
        else {
            $data = $this->_getAvailableAttributes($category);

            //Fenix_Debug::log('app getAvailableAttributes end cache disabled');
        }

        return $data;
    }

    /**
     * Список доступных атрибутов в категории
     *
     * @param $category
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    private function _getAvailableAttributes($category)
    {
        //Fenix_Debug::log('app getAvailableAttributes query to bd');
        $systemList = $this->getAvailableSystemAttributes($category);
        $userList = $this->getAvailableUserAttributes($category);

        //Помечаем системные атрибуты
        $systemList = $systemList->toArray();
        $systemList = array_map(function ($value) {
            $value['attribute_source'] = Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM;

            return $value;
        }, $systemList);

        //Помечаем пользовательские атрибуты
        $userList = $userList->toArray();
        $userList = array_map(function ($value) {
            $value['attribute_source'] = Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER;

            return $value;
        }, $userList);

        $data = array_merge($systemList, $userList);

        //Сортируем по полю position
        usort($data, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                return 0;
            }

            return ($a['position'] < $b['position']) ? -1 : 1;
        });

        $Result = new Fenix_Object_Rowset(
            array(
                'data' => $data,
            )
        );

        //Fenix_Debug::log('app getAvailableAttributes query to bd end');

        return $Result;
    }

    /**
     * Список доступных значений атрибутов в зависимости от уже выбранных фильтров
     *
     * @param $category
     * @param $attribute
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAvailableValues($category, $attribute)
    {
        Fenix_Debug::log('app getAvailableValues begin');
        $lang = Fenix_Language::getInstance();

        if ($attribute->split_by_lang == '1') {
            $attributeName = $attribute->sys_title . '_' . $lang->getCurrentLanguage()->name;
        }
        else {
            $attributeName = $attribute->sys_title;
        }
        $cacheId = 'getAvailableValues_' . $attributeName . '_' . ($category ? $category->id : '0');
        if ($category == null) {
            $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $cacheId .= '_' . $_catalog->left . '_' . $_catalog->right;
        }
        else {
            $cacheId .= '_' . $category->getLeft() . '_' . $category->getRight();
        }

        $this->setTable('catalog_relations');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'r' => $this->getTable(),
        ), null);

        // Джоиним товарчеги
        $cols = array(
            'p.' . $attributeName,
            'p.' . $attributeName . ' AS value',
        );

        $Select->join(array(
            'p' => $this->getTable('catalog_products'),
        ), 'p.id = r.product_id', $cols);

        $Select->join(array(
            'c' => $this->getTable('catalog'),
        ), 'r.parent = c.id', $cols);

        $Select->group('p.id');

        $Select->where('c.is_public = ?', '1');
        $Select->where('p.is_visible = ?', '1');
        $Select->where('p.' . $attributeName . ' <> ?', '');
        $Select->where('p.' . $attributeName . ' <> ?', '0');


        if ($category == null) {
            $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('r.left >= ?', (int) $_catalog->left)
                ->where('r.right <= ?', (int) $_catalog->right);
        }
        else {
            $Select->where('r.left >= ?', (int) $category->getLeft())
                ->where('r.right <= ?', (int) $category->getRight());
        }

        $Select->group('p.' . $attributeName);


        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        /*

        if ($attributeName == 'age') {
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 1.5 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 2 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 4 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 6 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 8 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 10 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 12 лет\' desc'));
            $Select->order(new Zend_Db_Expr('p.' . $attributeName . ' = \'От 16 лет\' desc'));
        }
        else {
            $Select->order('p.' . $attributeName . ' asc');
        }*/

        $Select->order('p.' . $attributeName . ' asc');

        /*$this  ->prepareFilterSelect($Select, array(
            'prefix' => 'p',
            'except' => $attributeName
        ));*/

        $Result = $this->fetchAll($Select);

        $_clean = array();

        foreach ($Result AS $_tmp) {
            /*$value = $_tmp->{$attributeName};
            /*
             *
            *//*
            $_clean[$value] = array(
                $attributeName => $value,
                'value'        => $value,
                'unit'         => $attribute->unit
            );*/
            $_value = explode(self::FILTER_VALUE_SEPARATOR, $_tmp->{$attributeName});
            foreach ($_value AS $_tmpValue) {
                $_clean[$_tmpValue] = array(
                    $attributeName => $_tmpValue,
                    'value'        => $_tmpValue,
                    'unit'         => $attribute->unit,
                );
            }
        }

        $Result = new Zend_Db_Table_Rowset(array(
            'data' => array_values($_clean),
        ));

        Zend_Registry::set($cacheId, $Result);
        Fenix_Debug::log('app getAvailableValues end');

        return $Result;
    }

    public function getAllAvailableValues($attribute)
    {
        $this->setTable('catalog_products');

        if (array_key_exists($attribute, (array) $this->getTableInfo($this->_name))) {
            $Select = $this->select()
                ->from($this->_name, $attribute . ' AS value')
                ->where($attribute . ' <> ?', '')
                ->group($attribute);


            $Select->order($attribute . ' asc');

            $Result = $this->fetchAll($Select);

            return $Result;
        }

        return false;
    }

    /**
     * Подготовка условий к запросу в зависимости от выбранных фильтров
     *
     * @param Zend_Db_Table_Select $Select
     * @param array $options
     *
     * @return bool
     */
    public function prepareFilterSelect(Zend_Db_Table_Select $Select, $options = array())
    {
        Fenix_Debug::log('app prepareFilterSelect begin');
        $filter = $this->parseFilterString();
        if (count($filter) == 0) {
            Fenix_Debug::log('app prepareFilterSelect end filter empty');

            return true;
        }

        $lang = Fenix_Language::getInstance();

        $prefix = isset($options['prefix']) && $options['prefix'] != '' ? $options['prefix'] . '.' : null;
        $except = (isset($options['except']) && $options['except'] != '' ? $options['except'] : null);

        $this->setTable('catalog_products');

        $Select->where($prefix . 'is_public = \'1\'', null)
            ->where($prefix . 'is_visible = \'1\'', null);

        $tableMetadata = $this->getTableInfo($this->getTable());

        foreach ($filter AS $_attribute => $_values) {
            $column = 'content';
            //Обработка фильтра по категории
            if ($_attribute == 'category') {
                $Select->where('r.parent IN ("' . implode('","', $_values) . '")');
            }
            else {

                $attributeData = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                    $_attribute
                );
                if ($attributeData && $attributeData->split_by_lang == '1') {
                    $_attribute = $attributeData->sys_title . '_' . $lang->getCurrentLanguage()->name;
                    $column = $column . '_' . $lang->getCurrentLanguage()->name;
                }
                else {
                    //$_attribute = $attributeData->sys_title;
                }

                if ($except != $_attribute) {
                    $where = array();
                    $attributeData = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                        $_attribute
                    );
                    if ($attributeData && $attributeData->split_by_lang == '1') {
                        $_attribute = $attributeData->sys_title;// . '_' . $lang->getCurrentLanguage()->name;
                        $column = $column . '_' . $lang->getCurrentLanguage()->name;
                    }
                    else {
                        //$_attribute = $attributeData->sys_title;
                    }
                    if ($attributeData) {

                        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attributeData);
                        $attrSelect = $this->setTable($tableName)->select()->setIntegrityCheck(false)
                            ->from(array(
                                'avc' => $this->_name,
                            ), array())
                            ->join(array(
                                'avr' => $this->getTable('attr_values'),
                            ), 'avc.id = avr.value_id AND ' . $this->getAdapter()->quoteInto('avr.attribute_id = ?', $attributeData->id),
                                array('product_id')
                            );


                        if ($attributeData->is_range == '1') {
                            if (isset($_values[0]) && preg_match('/^\d+\.?\d*-\d+\.?\d*$/si', $_values[0])) {
                                list($from, $to) = explode('-', rawurldecode($_values[0]));

                                $where[] = $this->getAdapter()->quoteInto('avc.' . $column . ' >= ?', $from);
                                $where[] = $this->getAdapter()->quoteInto('avc.' . $column . ' <= ?', $to);
                                $attrSelect->where(implode(' AND ', $where));
                            }
                        }
                        else {
                            $column = 'url';
                            foreach ($_values AS $_value) {
                                $where[] = $this->getAdapter()->quoteInto('avc.' . $column . ' LIKE ?', '' . rawurldecode($_value) . '');
                            }

                            if (sizeof($where) > 0) {
                                $attrSelect->where(implode(' or ', $where));
                            }
                        }

                        if (sizeof($where) > 0) {
                            if (isset($options['getValues']) && $options['getValues'] == true) {
                                $Select->where('avr.product_id IN(' . $attrSelect->assemble() . ')');
                            }
                            else {
                                $Select->where('p.id IN(' . $attrSelect->assemble() . ')');
                            }
                        }
                    }
                    else {
                        if ($_attribute == 'price') {
                            if (isset($_values[0]) && preg_match('/^\d+\.?\d*-\d+\.?\d*$/si', $_values[0])) {
                                list($from, $to) = explode('-', rawurldecode($_values[0]));
                                $where[] = $this->getAdapter()->quoteInto($prefix . $_attribute . ' >= ?', $from);
                                $where[] = $this->getAdapter()->quoteInto($prefix . $_attribute . ' <= ?', $to);
                                $Select->where(implode(' AND ', $where));
                            }
                        }
                    }
                }

                if (isset($tableMetadata[$_attribute])) {

                }
            }
        }

        //Fenix::dump($Select->assemble());
        Fenix_Debug::log('app prepareFilterSelect end');
        //Fenix::dump($Select->assemble());
        /*if(Fenix::getRequest()->getParam('filter')=='tires'){
            $Select = Fenix::getModel('catalog/tires')->prepareFilterSelect($Select);
        }*/

        return true;
    }

    /**
     * Вычисление минимальных и маскимальных значений для диапазонных атрибутов
     *
     * @param $category
     * @param $attribute
     *
     * @return bool|null|Zend_Db_Table_Row_Abstract
     */
    public function getRangeData($category, $attribute)
    {
        if ($attribute->split_by_lang == '1') {
            return false;
        }
        // Если пользовательский атрибут используем другой алгоритм

        if ($attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER) {
            return $this->getRangeDataUserAttribute($category, $attribute);
        }

        $this->setTable('catalog_relations');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'r' => $this->getTable(),
        ), null);


        $cols = array(
            new Zend_Db_Expr('FLOOR(MIN(p.' . $attribute->sys_title . ')) AS min'),//Округление вниз 2.25 => 2
            new Zend_Db_Expr('CEIL(MAX(p.' . $attribute->sys_title . ')) AS max'), //Округление вверх 2.25 => 3
            new Zend_Db_Expr('\'\' AS selected_min'),
            new Zend_Db_Expr('\'\' AS selected_max'),
        );

        $Select->join(array(
            'p' => $this->getTable('catalog_products'),
        ), 'p.id = r.product_id', $cols);

        $Select->where('p.' . $attribute->sys_title . ' <> ?', '');

        if ($category == null) {
            $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('r.left >= ?', (int) $_catalog->left)
                ->where('r.right <= ?', (int) $_catalog->right);
        }
        else {
            $Select->where('r.left >= ?', (int) $category->getLeft())
                ->where('r.right <= ?', (int) $category->getRight());
        }

        $this->prepareFilterSelect($Select, array(
            'prefix' => 'p',
            'except' => $attribute->sys_title,
        ));

        $Result = $this->fetchRow($Select);

        $filter = $this->parseFilterString();

        if (array_key_exists($attribute->sys_title, $filter) && isset($filter[$attribute->sys_title][0]) && preg_match('/^\d+\.?\d*-\d+\.?\d*$/si', $filter[$attribute->sys_title][0])) {
            list($from, $to) = explode('-', $filter[$attribute->sys_title][0]);

            $Result->selected_min = $from;
            $Result->selected_max = $to;
        }
        else {
            $Result->selected_min = $Result->min;
            $Result->selected_max = $Result->max;

        }

        return $Result;
    }

    public function getRangeDataUserAttribute($category, $attribute)
    {


        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $Select = $this->setTable($tableName)
            ->select()
            ->setIntegrityCheck(false);
        $cols = array(
            new Zend_Db_Expr('ROUND(MIN(avc.content),2) AS min'),
            new Zend_Db_Expr('ROUND(MAX(avc.content),2) AS max'),
            new Zend_Db_Expr('\'\' AS selected_min'),
            new Zend_Db_Expr('\'\' AS selected_max'),
        );

        $Select->from(array(
            'avc' => $this->_name,
        ), $cols);

        if ($category) {
            $Select->join(array(
                'avr' => $this->getTable('attr_values'),
            ), 'avr.value_id = avc.id AND ' .
                $this->getAdapter()->quoteInto('avr.attribute_id = ?', $attribute->id), null);
            $Select->join(array(
                'r' => $this->getTable('catalog_relations'),
            ), 'r.product_id = avr.product_id AND ' .
                $this->getAdapter()->quoteInto('r.left  >= ?', $category->getLeft()) . ' AND ' .
                $this->getAdapter()->quoteInto('r.right  <=?', $category->getRight())
                , array());
        }
        $Select->where('avc.attribute_id = ?', $attribute->id);
        //$Select->group('avr.value_id');
        //$Select->group('avr.attribute_id');
        //$Select->group('avr.value_id');

        //Fenix::dump($Select->assemble());
        //if($attribute->sys_title =='color') Fenix::dump($Select->assemble(),$Result);
        $Result = $this->fetchRow($Select);

        $filter = $this->parseFilterString();

        if (array_key_exists($attribute->sys_title, $filter) && isset($filter[$attribute->sys_title][0]) && preg_match('/^\d+\.?\d*-\d+\.?\d*$/si', $filter[$attribute->sys_title][0])) {
            list($from, $to) = explode('-', $filter[$attribute->sys_title][0]);

            $Result->selected_min = $from;
            $Result->selected_max = $to;
        }
        else {
            $Result->selected_min = $Result->min;
            $Result->selected_max = $Result->max;

        }

        return $Result;
    }

    /**
     * Подготовка URL содержищий значения фильтра
     *
     * @param       $attribute
     * @param       $value
     * @param array $options
     *
     * @return string
     */
    public function prepareFilterUrl($attribute, $value, $options = array())
    {
        $url = $this->_getCleanUrl();
        $filter = $this->parseFilterString(null, $options);
        if ($attribute->sys_title == 'category') {
            //Обработка фильтра по категории
            if (array_key_exists($attribute->sys_title, $filter)) {
                if ( ! in_array($value, $filter[$attribute->sys_title])) {
                    $filter[$attribute->sys_title][] = $value;
                }
                else {
                    while (in_array($value, $filter[$attribute->sys_title])) {
                        unset($filter[$attribute->sys_title][array_search($value, $filter[$attribute->sys_title])]);
                    }
                }
            }
            else {
                $filter[$attribute->sys_title][] = $value;
            }


        }
        elseif ($attribute->is_range == '1') {
            if ( ! array_key_exists($attribute->sys_title, $filter)) {
                $filter[$attribute->sys_title] = $value->value;
            }
            else {
                if ($value->value == $filter[$attribute->sys_title][0]) {
                    unset($filter[$attribute->sys_title]);
                }
                else {
                    $filter[$attribute->sys_title] = $value->value;
                }
            }
        }
        else {
            if (array_key_exists($attribute->sys_title, $filter)) {
                if ( ! in_array($value->content, $filter[$attribute->sys_title])) {
                    $filter[$attribute->sys_title][] = $value->content;
                }
                else {
                    while (in_array($value->content, $filter[$attribute->sys_title])) {
                        unset($filter[$attribute->sys_title][array_search($value->content, $filter[$attribute->sys_title])]);
                    }
                }
            }
            else {
                $filter[$attribute->sys_title][] = urlencode($value->content);
            }
        }

        $filterString = $this->assembleFilterString($filter);
        if ($filterString != '') {
            $url = Fenix::getUrl($url . '/filter/' . $filterString);
        }
        else {
            $url = Fenix::getUrl($url);
        }

        return $url;
    }

    public function prepareSortingUrl($by)
    {
        $url = $this->_getCleanUrl();
        if (Fenix::getRequest()->getParam('filter') != null) {
            $url .= '/filter/' . Fenix::getRequest()->getParam('filter');
        }

        if (Fenix::getRequest()->getQuery('sort') != null) {
            $url .= '?sort=' . Fenix::getRequest()->getParam('sort');
        }
        else {
            $url .= '?sort=' . $by;
        }


        return Fenix::getUrl($url);
    }

    /**
     * Проверка выбрано ли одно из значений атрибута
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function isAttributeSelected($attribute, $value = null)
    {
        $filter = $this->parseFilterString();

        if (count($filter) > 0) {
            if (array_key_exists($attribute->sys_title, $filter)) {
                if ($value) {
                    return in_array($value->url, $filter[$attribute->sys_title]);
                }
                else {
                    return true;
                }
            }
        }

        return false;
    }

    public function isAttributeUsingInFilter($attribute)
    {
        $filter = $this->parseFilterString();

        if (count($filter) > 0) {
            if (array_key_exists($attribute->sys_title, $filter)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Ссылка на сброс всего фильтра
     *
     * @return string
     */
    public function flushFilterUrl()
    {
        $url = Fenix::getUrl($this->_getCleanUrl());
        if (Fenix::getRequest()->getQuery('sort') != null) {
            $url .= '?sort=' . Fenix::getRequest()->getQuery('sort');
        }

        return $url;
    }

    /**
     * Список уже выбранных атрибутов в фильтре
     *
     * @return Zend_Db_Table_Rowset
     */
    public function getSelectedAttributes()
    {
        $result = array();
        $filter = $this->parseFilterString();

        foreach ($filter AS $attrName => $_urls) {
            $attribute = Fenix::getModel('catalog/products_attributes')->getAttributeBySysTitle(
                $attrName
            );

            if ($attribute == null) {
                continue;
            }

            $attrValues = Fenix::getModel('catalog/products_attributes')->getAttributeValuesByUrl($attribute, $_urls);
            $attribute = $attribute->toArray();
            $attribute['selected_values'] = $attrValues;
            $result[] = $attribute;
        }

        return new Zend_Db_Table_Rowset(array(
            'data' => $result,
        ));
    }


    /**
     * Сборка URL для фильтра
     *
     * @param $filter
     *
     * @return string
     */
    public function assembleFilterString($filter)
    {
        $result = array();

        if (isset($filter['price'])) {
            $price = $filter['price'];
            unset($filter['price']);
            ksort($filter);
            $filter['price'] = $price;
        }
        else {
            ksort($filter);
        }


        foreach ($filter AS $_attribute => $_values) {
            if (sizeof($_values) > 0) {
                if (is_array($_values)) {
                    $buf = array();
                    foreach ($_values as $_value) {
                        if ($_value instanceof Object) {
                            $buf[] = urlencode($_value->value);
                        }
                        else {
                            $buf[] = urlencode($_value);
                        }

                    }
                    $string = implode(self::FILTER_VALUE_SEPARATOR, $buf);

                }
                else {
                    $string = urlencode($_values);
                }

                $result[] = $_attribute . self::FILTER_ATTR_SYS_TITLE_SEPARATOR . $string;
            }
        }

        return implode(self::FILTER_ATTR_SEPARATOR, $result);
    }

    /**
     * Разборка строки фильтра на массив
     *
     * @param null $filter
     * @param array $options
     *
     * @return array
     */
    public function parseFilterString($filter = null, $options = array())
    {
        $result = array();

        //Если строки для разбора нет, пробуем взять из урла
        if ($filter === null) {
            $filter = Fenix::getRequest()->getParam('filter');
        }

        //Если в фильтре ничего нет, возвращаем пустой массив
        if ($filter === null || $filter == '') {
            return $result;
        }

        //Создаем ключ для реестра и пробуем вернуть результат из него
        $regTitle = 'parseFilterString_' . md5($filter . serialize($options));
        if (Zend_Registry::isRegistered($regTitle)) {
            return Zend_Registry::get($regTitle);
        }
        //Разбиваем фильтр по разделителю атрибутов
        $filter = explode(self::FILTER_ATTR_SEPARATOR, urldecode($filter));

        //Обрабатываем каждый элемент
        foreach ($filter AS $params) {
            //Разделяем системное название от значений
            $valuesBegin = strpos($params, self::FILTER_ATTR_SYS_TITLE_SEPARATOR);
            $attrName = substr($params, 0, $valuesBegin);
            $attrValues = substr($params, $valuesBegin + strlen(self::FILTER_ATTR_SYS_TITLE_SEPARATOR));

            //Разбиваем значения между собой по разделителю
            $attrValues = explode(self::FILTER_VALUE_SEPARATOR, urldecode($attrValues));

            //Сохраняем
            $result[$attrName] = $attrValues;
        }

        //Сохраняем в реест
        Zend_Registry::set($regTitle, $result);

        return $result;
    }

    /**
     * URL без значений фильтра
     *
     * @return string
     */
    private function _getCleanUrl()
    {
        $req = Fenix::getRequest();
        $pathInfo = $req->getParsedInfo();
        $_segments = $pathInfo->segments;

        $_segments = array_map(function ($v) {
            return urldecode($v);
        }, $_segments);

        while (in_array('filter', $_segments)) {
            unset($_segments[array_search('filter', $_segments)]);
        }

        while (in_array(urldecode($req->getParam('filter')), $_segments)) {
            unset($_segments[array_search(urldecode($req->getParam('filter')), $_segments)]);
        }

        $result = implode('/', $_segments);

        return $result;
    }

    private function getAvailableSystemAttributes($category)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/system_attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name,
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset'),
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups'),
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes'),
        ), 'cag.id = caga.group_id AND caga.attribute_source = "system"', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_system_attributes'),
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a',
        )));

        $Select->where('a.is_in_filter = ?', '1');

        if ($category == null) {
            $rootCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('c.left >= ?', (int) $rootCategory->left)
                ->where('c.right <= ?', (int) $rootCategory->right);
        }
        else {
            $Select->where('c.left >= ?', (int) $category->getLeft())
                ->where('c.right <= ?', (int) $category->getRight());
        }
        $Select->order('caga.position asc');
        $Select->group('a.sys_title');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    private function getAvailableUserAttributes($category)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name,
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset'),
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups'),
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes'),
        ), 'cag.id = caga.group_id AND caga.attribute_source = "user"', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes'),
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a',
        )));

        $Select->where('a.is_in_filter = ?', '1');

        if ($category == null) {
            $rootCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('c.left >= ?', (int) $rootCategory->left)
                ->where('c.right <= ?', (int) $rootCategory->right);
        }
        else {
            $Select->where('c.left >= ?', (int) $category->getLeft())
                ->where('c.right <= ?', (int) $category->getRight());
        }
        $Select->order('caga.position asc');
        $Select->group('a.sys_title');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getAllAvailableValuesInCategory($currentCategory, $attribute)
    {
        //Fenix_Debug::log('app getAllAvailableValuesInCategory (' . $attribute . ') begin');

        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        $attributeSource = 'user';
        $attr = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attribute);

        if ($attr == null) {
            $attributeSource = 'system';
            $attr = Fenix::getModel('catalog/backend_system_attributes')->getAttributeBySysTitle($attribute);
        }

        if ($attr->split_by_lang == '1') {
            $attribute = $attribute . '_' . $lang->name;
        }

        if ($currentCategory == null) {
            $currentCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
        }


        $regTitle = 'getAllAvailableValuesInCategory_' . $attribute . '_' . $currentCategory->left . '_' . $currentCategory->right;

        /*if (Zend_Registry::isRegistered($regTitle))
        {
            Fenix_Debug::log('app getAllAvailableValuesInCategory (' . $attribute . ') end cached');

            return Zend_Registry::get($regTitle);
        }*/
        if ($attributeSource == 'system') {
            $this->setTable('catalog_products');
        }
        elseif ($attributeSource == 'user') {
            $this->setTable('catalog_attribute_values');
        }
        else {
            throw new Exception('Неизвестный источник данных "' . $attr->attribute_source . '"');
        }
        if (array_key_exists($attribute, (array) $this->getTableInfo($this->_name))) {
            //Привязка к категориям
            $this->setTable('catalog_relations');
            $Select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array(
                    'r' => $this->getTable('catalog_relations'),
                ), array());

            $cols = array();
            if ($attributeSource == 'system') {
                $cols[] = $attribute . ' AS value';
            }
            $Select->join(array(
                'p' => $this->getTable('catalog_products'),
            ), 'p.id = r.product_id', $cols);

            $cols = array();
            if ($attributeSource == 'user') {
                $cols[] = $attribute . ' AS value';
            }
            $Select->join(array(
                'av' => $this->getTable('catalog_attribute_values'),
            ), 'p.id = av.product_id', $cols);

            $Select->where('r.left >= ?', $currentCategory->left)
                ->where('r.right <= ?', $currentCategory->right);

            $Select->where('p.is_public = ?', '1');

            if ($attributeSource == 'system') {
                $Select->where('p.' . $attribute . ' <> ?', '')
                    ->group('p.' . $attribute);
                $Select->order('p.' . $attribute . ' asc');
            }
            elseif ($attributeSource == 'user') {
                $Select->where('av.' . $attribute . ' <> ?', '')
                    ->group('av.' . $attribute);
                $Select->order('av.' . $attribute . ' asc');
            }

            if ($attribute == 'color') {
                Fenix::dump($attribute, $attributeSource, $Select->assemble(), $attr);
            }
            $Result = $this->fetchAll($Select);


            $_clean = array();
            $_sort = array();
            foreach ($Result AS $_tmp) {

                $_value = explode(',', $_tmp->value);

                foreach ($_value AS $_tmpValue) {
                    $_clean[$_tmpValue] = array(
                        'value' => $_tmpValue,
                    );
                }
                //SORT_NATURAL
                ksort($_clean, 6);
            }

            $Result = new Zend_Db_Table_Rowset(array(
                'data' => array_values($_clean),
            ));

            Zend_Registry::set($regTitle, $Result);

            //Fenix_Debug::log('app getAllAvailableValuesInCategory (' . $attribute . ') end');

            return $Result;
        }

        //Fenix_Debug::log('app getAllAvailableValuesInCategory (' . $attribute . ') end');

        return false;
    }

    /**
     * @param      $attribute
     * @param null $category
     *
     * @return mixed|Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Exception
     */
    public function getAttributeValues($attribute, $category = null)
    {

        Fenix_Debug::log('....... app getAttributeValues ' . $attribute->sys_title . ' categoryId:' . ($category ? $category->getId() : '') . ' begin');

        $cacheId = 'Filter_getAttributeValues_' . $attribute->id . '_' . ($category ? $category->id : '0');
        if (Zend_Registry::isRegistered($cacheId)) {

            Fenix_Debug::log('app getAttributeValues ' . $attribute->sys_title . ' categoryId:' . ($category ? $category->getId() : '') . ' by Zend_Registry end');

            return Zend_Registry::get($cacheId);
        }

        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';
        }
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $Select = $this->setTable($tableName)
            ->select()
            ->setIntegrityCheck(false);

        $cols = $Engine->getFrontendColumns(array('prefix' => 'avc'));
        $Select->from(array(
            'avc' => $this->_name,
        ), $cols);

        if ($category) {
            $Select->join(array(
                'avr' => $this->getTable('attr_values'),
            ), 'avr.value_id = avc.id AND ' .
                $this->getAdapter()->quoteInto('avr.attribute_id = ?', $attribute->id), null);
            $Select->join(array(
                'r' => $this->getTable('catalog_relations'),
            ), 'r.product_id = avr.product_id AND ' .
                $this->getAdapter()->quoteInto('r.left  >= ?', $category->getLeft()) . ' AND ' .
                $this->getAdapter()->quoteInto('r.right  <=?', $category->getRight())
                , array(
                    'COUNT(DISTINCT avr.product_id) as products_count',
                ));
            $Select->join(array(
                'p' => $this->getTable('catalog_products'),
            ), 'r.product_id = p.id', null);

            $Select->group('avr.value_id');
            $Select->group('avr.attribute_id');
        }

        // Параметры
        $options = array();
        $options['except'] = $attribute->sys_title;
        $options['getValues'] = true;

        // Применяем фильтры
        $this->prepareFilterSelect($Select, $options);
        //        /Fenix::dump($cols);

        $Select->where('avc.attribute_id = ?', $attribute->id);
        $Select->having('content<> ""');
        $Select->order('avc.position asc');
        //$Select->group('avr.value_id');

        //        Fenix::dump($Select->assemble());
        //if($attribute->sys_title =='color') Fenix::dump($Select->assemble(),$Result);
        $Result = $this->fetchAll($Select);


        Zend_Registry::set($cacheId, $Result);

        Fenix_Debug::log('....... app getAttributeValues ' . $attribute->sys_title . ' categoryId:' . ($category ? $category->getId() : '') . ' end');

        return $Result;
    }

    public function setAttributeRange($filter, $attributeName, $attrValue)
    {
        $filter = $this->parseFilterString($filter);
        $filter[$attributeName] = array(
            $attrValue,
        );
        $newFilter = $this->assembleFilterString($filter);

        return $newFilter;
    }

    /**
     * Список атрибутов и их значений, которые индексируются, и используются в $category
     */
    public function getIndexesAttributesToCategory($category = null)
    {
        if ($category == null || ! is_object($category)) {
            return false;
        }

        $data = array();

        Fenix_Debug::log('Fenix_Catalog_Model_Filter getIndexesAttributesToCategory begin');

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));


        $Select->where('a.is_in_filter = ?', '1')
            ->where('a.is_active = ?', '1')
            ->where('a.is_index = ?', '1');

        $Select->where('c.left >= ?', (int) $category->left)
            ->where('c.right <= ?', (int) $category->right)
            ->where('c.in_sitemap = ?', '1')
            ->where('c.is_public = ?', '1');

        $Select->group('a.sys_title');

        $attributes = $this->fetchAll($Select);

        if ($attributes == null || $attributes->count() <= 0) {
            return false;
        }

        foreach ($attributes as $attribute) {

            $tableName = 'attr_values_' . strtolower($attribute->sql_type);
            if ($attribute->split_by_lang == '1') {
                $tableName .= '_lang';
            }

            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/' . $tableName);

            $Select = $this->setTable($tableName)
                ->select()
                ->setIntegrityCheck(false);

            $cols = $Engine->getFrontendColumns(array('prefix' => 'avc'));
            $Select->from(array(
                'avc' => $this->_name
            ), $cols);

            $Select->join(array(
                'avr' => $this->getTable('attr_values')
            ), 'avr.value_id = avc.id AND ' .
                $this->getAdapter()->quoteInto('avr.attribute_id = ?', $attribute->id), null);
            $Select->join(array(
                'r' => $this->getTable('catalog_relations')
            ), 'r.product_id = avr.product_id AND ' .
                $this->getAdapter()->quoteInto('r.left  >= ?', (int) $category->left) . ' AND ' .
                $this->getAdapter()->quoteInto('r.right  <=?', (int) $category->right)
                , false);
            $Select->join(array(
                'a' => $this->getTable('catalog_attributes')
            ), 'avr.attribute_id = a.id', array(
                'a.sys_title as sys_title'
            ));
            $Select->join(array(
                'p' => $this->getTable('catalog_products')
            ), 'r.product_id = p.id', null);

            $Select->group('avr.value_id');
            $Select->group('avr.attribute_id');
            $Select->where('avc.attribute_id = ?', $attribute->id);
            $Select->having('content<> ""');

            $Result = $this->fetchAll($Select)->toArray();

            $data = array_merge($data, $Result);
        }

        Fenix_Debug::log('Fenix_Catalog_Model_Filter getIndexesAttributesToCategory end');

        return new Zend_Db_Table_Rowset(array(
            'data' => $data,
        ));
    }
}