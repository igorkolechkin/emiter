<?php
class Fenix_Catalog_Model_Reviews extends Fenix_Resource_Model
{
    public function getReviewsListAsSelect()
    {
        $this->setTable('catalog_product_reviews');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ));

        $Select ->joinLeft(array(
            'p' => $this->getTable('catalog_products')
        ), 'r.product_id = p.id', array(
            'p.id_1c AS id_1c',
            'p.sku AS sku',
            'p.title_russian AS product',
        ));

        $Select ->order('r.id desc');

        return $Select;
    }

    public function getAverageRating($productId)
    {
        if (Fenix::isDev()){

            $this->setTable('catalog_product_reviews');

            $Select = $this->select();

            $Select ->from($this->_name, array(
                new Zend_Db_Expr('COUNT(id) AS count'),
                new Zend_Db_Expr('SUM(rating) AS sum'),
                new Zend_Db_Expr('\'0\' AS average')
            ));

            $Select ->where('product_id = ?', $productId);
            $Select ->where('is_active = ?', '1');

            $Result = $this->fetchRow($Select);

            if ($Result == null)
                $Result =  new Zend_Db_Table_Row(array(
                    'data'=>array(
                        'count'   => 0,
                        'sum'     => 0,
                        'average' => 0
                    )
                ));

            if ($Result->count > 0)
                $Result->average = Fenix::number($Result->sum / $Result->count,2);

            return $Result;
        }

        $this->setTable('catalog_product_reviews');

        $Select = $this->select();

        $Select ->from($this->_name, array(
            new Zend_Db_Expr('(SELECT COUNT(*) FROM ' . $this->_name . ' WHERE is_active = "1" AND product_id = ' . (int) $productId . ') AS count'),
            new Zend_Db_Expr('(SELECT SUM(rating) FROM ' . $this->_name . ' WHERE is_active = "1" AND product_id = ' . (int) $productId . ') AS sum'),
            new Zend_Db_Expr('\'0\' AS average')
        ));

        $Select ->where('product_id = ?', $productId);

        $Result = $this->fetchRow($Select);

        if ($Result == null)
            $Result =  new Zend_Db_Table_Row(array(
                'data'=>array(
                    'count'   => 0,
                    'sum'     => 0,
                    'average' => 0
                )
            ));

        if ($Result->count > 0)
            $Result->average = Fenix::number($Result->sum / $Result->count,2);

        return $Result;
    }

    public function getReviewById($id)
    {
        $this->setTable('catalog_product_reviews');

        return $this->fetchRow('id = ' . (int) $id);
    }

    /**
     * Список отзывов на страницу товара
     *
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getReviewsList($productId, $parent = 0)
    {
        $this->setTable('catalog_product_reviews');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from($this);
        $Select ->where('is_active = ?', '1');

        $Select ->where('product_id = ?', $productId);

        $Select ->where('parent = ?', $parent);

        $Select ->order('review_date desc');

        $result =  $this->fetchAll($Select);

        return $result;
    }
    /**
     * Список отзывов на страницу товара
     *
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getReviewsListByGroupId($groupId, $parent = 0)
    {
        $this->setTable('catalog_product_reviews');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'r'=>$this->_name
        ),array('*'));

        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'r.product_id = p.id AND p.group_id = ' . (int)$groupId, array());


        $Select ->where('r.is_active = ?', '1');
        $Select ->where('r.parent = ?', $parent);

        $Select ->order('r.review_date desc');

        $result =  $this->fetchAll($Select);

        return $result;
    }
    /**
     * Список отзывов на страницу товара
     *
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getLastReviewsList($limit = 3)
    {
        $this->setTable('catalog_product_reviews');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select->from($this);
        $Select->where('is_active = ?', 1);
        $Select->order('id desc');
        $Select->limit($limit);

        return $this->fetchAll($Select);
    }

    /**
     * Список отзывов для страницы отзывов
     *
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getReviewsListAll()
    {
        $this->setTable('catalog_product_reviews');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select->from($this);
        $Select->where('is_active = ?', 1);
        $Select->order('id desc');

        $Count     = clone $Select;
        $Count     ->reset(Zend_Db_Select::COLUMNS);
        $Count     ->columns(new Zend_Db_Expr('COUNT(*) AS _count'));
        $Count     = (int) $this->fetchRow($Count)->_count;

        $perPage = 20;

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter   ->setRowCount($Count);
        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);
        return $paginator;
    }


    /**
     * Добавить новый отзыв
     *
     * @param Fenix_Object $product
     * @param Fenix_Controller_Request_Http $req
     */
    public function addReview(Fenix_Object $product, Fenix_Controller_Request_Http $req)
    {
        $this->setTable('catalog_product_reviews');
        $this->insert(array(
            'parent'      => $req->getPost('parent') ? $req->getPost('parent') : 0,
            'product_id'  => $product->getId(),
            'user_id'     => 0,
            'name'        => $req->getPost('name')?$req->getPost('name'):'',
            'review'      => $req->getPost('review')?$req->getPost('review'):'',
            'benefits'    => $req->getPost('benefits')?$req->getPost('benefits'):'',
            'limitations' => $req->getPost('limitations')?$req->getPost('limitations'):'',
            'review_date' => new Zend_Db_Expr('NOW()'),
            'rating'      => $req->getPost('rating')?$req->getPost('rating'):0
        ));

        return;
    }

    public function editReview($current, Fenix_Controller_Request_Http $req)
    {
        $this->setTable('catalog_product_reviews');
        $this->update(array(
            'user_name'   => $req->getPost('user_name'),
            'is_active'   => $req->getPost('is_active'),
            'review'      => $req->getPost('review')
        ), 'id = ' . (int) $current->id);

        return $current->id;
    }

    public function deleteReview($current)
    {
        $this->setTable('catalog_product_reviews');
        $this->delete('id = ' . (int) $current->id);

        return $current->id;
    }

    public function updateProductsRating(){
        $this->setTable('catalog_product_reviews');

        $Select = $this->select();

        $Select ->from($this->_name, array(
            'product_id',
            new Zend_Db_Expr('COUNT(id) AS rating_count')
        ));
        $Select->where('is_active = ?', '1');
        $Select ->group('product_id');
        $Result = $this->fetchAll($Select);

        $countData = array();
        foreach ($Result as $productRating) {
            $countData[$productRating->product_id] = $productRating->rating_count;
        }
        $this->setTable('catalog_product_reviews');

        $Select = $this->select();

        $Select ->from($this->_name, array(
            'product_id',
            new Zend_Db_Expr('AVG(rating)-1 AS rating_value'),
            new Zend_Db_Expr('MIN(rating)-1 AS rating_min'),
            new Zend_Db_Expr('MAX(rating)-1 AS rating_max')
        ));
        $Select->where('rating > 0');
        $Select->where('is_active = ?', '1');
        $Select ->group('product_id');
        $Result = $this->fetchAll($Select);
        foreach ($Result as $productRating) {
            $data = array(
                'rating_count'     => $countData[$productRating->product_id],
                'rating_value'     => round($productRating->rating_value,2),
                'rating_min_value' => $productRating->rating_min,
                'rating_max_value' => $productRating->rating_max,
            );
            $this->setTable('catalog_products');
            $this->update($data, 'id = ' . (int)$productRating->product_id);
        }
    }
}