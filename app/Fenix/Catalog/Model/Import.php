<?php

class Fenix_Catalog_Model_Import extends Fenix_Resource_Model
{
    //Папка в которую загружают фотографии товара
    const PATH_TO_UPLOAD_IMAGES = 'var/upload/import_images';

    public function importProducts($import_file, $_parent)
    {
        //Основные настройки
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $attrSysTitle = 2; //Строка системного названия
        $memory_start = memory_get_usage();
        $time_start = microtime(true);
        $UserId = Fenix::getModel('session/auth')->getUser()->id;
        $Limit = 2; //Количество товаров обрабатываемых за один круг

        //Атрибуты
        $lang = Fenix_Language::getInstance();
        $attrList = array(); //Список атрибутов

        //Статистика импорта
        $cacheNew = array();
        $cacheUpdate = array();
        $log = array();

        //Создаем объект для импорта
        $filename = $import_file->tmp_name;
        $Excel = Fenix_Excel::newGetObject($filename);

        //Собираем базовые параметры
        $sheet = $Excel->getSheet();
        $allAttributes = $sheet->cells[$attrSysTitle];
        $importAttr = array();
        $category_key = '';
        $sql_types = array();

        //Удаляем системные строки
        unset($sheet->cells[1]);
        unset($sheet->cells[$attrSysTitle]);

        $updatedAttributeIDs = array();
        /**
         *Собираем массив всех аттрибутов
         */
        foreach ($allAttributes as $key => $attr) {
            $attr = trim($attr);
            $values = array();
            foreach ($lang->getLanguagesList() as $_lang) {
                if (substr($attr, -strlen($_lang->name)) == $_lang->name) {
                    $attrLangName = '_' . $_lang->name;
                    $attr = substr($attr, 0, -(strlen($_lang->name) + 1));
                    break;
                }
                else {
                    $attrLangName = '';
                }
            }

            $check = Fenix::getModel('catalog/backend_system_attributes')->getAttributeBySysTitle($attr);

            $checkId = '';
            $checkSqlType = '';
            $checkSplitByLang = '';

            if ($check == null) {
                $check = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attr);
                if ($check == null) {
                    $type = null;
                }
                else {
                    $checkId = $check->id;
                    $checkSqlType = $check->sql_type;
                    $checkSplitByLang = $check->split_by_lang ? 'lang' : '';
                    $type = '0';
                    $values_tmp = Fenix::getModel('catalog/backend_attributes')->getAttributeValues($check);
                    foreach ($values_tmp as $value) {
                        $values[$value->content] = $value->toArray();
                    }
                }
            }
            else {
                $checkId = $check->id;
                $checkSqlType = $check->sql_type;
                $checkSplitByLang = $check->split_by_lang ? 'lang' : '';
                $type = '1';
            }
            $importAttr[$key]['key'] = $key;
            $importAttr[$key]['id'] = $checkId;
            $importAttr[$key]['sys_title'] = $attr;
            $importAttr[$key]['sql_type'] = strtolower($checkSqlType);
            $importAttr[$key]['lang'] = $checkSplitByLang;
            $importAttr[$key]['lang_name'] = $attrLangName;
            $importAttr[$key]['is_system'] = $type;
            $importAttr[$key]['values'] = $values;
            $importAttr[$key]['object'] = $check;

            $updatedAttributeIDs[] = $checkId;
        }
//        Fenix::dd($importAttr, false);
        /**
         *Обработчик продуктов
         */
        foreach ($sheet->cells as $item) {
            $product = array();
            $productAttributes = array();
            $categories = array();
            $productId = '';
            $gallery = array();
            $image = '';
            $url = '';

            $product['create_id'] = $UserId;
            /**
             *Собираем массив атрибутов
             */
            foreach ($item as $key => $str) {
                $str = trim($str);
                //Разбиваем категории
                if ($importAttr[$key]['sys_title'] == 'title') {
                    $url = $str;
                }

                //Проверяем товар по артикулу
                if ($importAttr[$key]['sys_title'] == 'sku') {
                    $product['sku'] = $str;
                    $productTmp = Fenix::getModel('catalog/products')->getProductBySku($product['sku']);
                    if ($productTmp == null) {
                        $action = 'new';
                    }
                    else {
                        $productId = $productTmp->id;
                        $action = 'update';
                    }
                    continue;
                }

                //Разбиваем категории
                if ($importAttr[$key]['sys_title'] == 'categories') {
                    $categories = explode(';', $str);
                    $product['parent'] = trim($categories[0]);
                    continue;
                }

                //Изображение товара
                if ($importAttr[$key]['sys_title'] == 'image') {
                    if ($str != '') {
                        $image = $str;
                    }
                    continue;
                }

                //Галерея изображений товара
                if ($importAttr[$key]['sys_title'] == 'gallery') {
                    $explodeGallery = explode(';', $str);
                    foreach ($explodeGallery as $imageGallery) {
                        $gallery[] = trim($imageGallery);
                    }
                    continue;
                }

                //Системные атрибуты
                if ($importAttr[$key]['is_system'] == '1') {
                    $product[$importAttr[$key]['sys_title'] . $importAttr[$key]['lang_name']] = $str;
                    continue;
                }

                //Пользовательские атрибуты
                if ($importAttr[$key]['is_system'] == '0') {
                    $explodeAttributes = explode(';', $str);
                    foreach ($explodeAttributes as $explodeAttribute) {
                        $explodeAttribute = trim($explodeAttribute);
                        if ($explodeAttribute == '') {
                            continue;
                        }
                        $productAttributes[$key][] = $explodeAttribute;
                    }
                    continue;
                }

            }


            /**
             *Добавляем/Обновляем товар
             */
            if ($action == 'new') { //Добавление
                if ( ! isset($product['url_key'])) {
                    $product['url_key'] = Fenix::stringProtectUrl($url) . $product['sku'];
                }
                $productId = $this->setTable('catalog_products')
                    ->insert($product);
                $cacheNew[] = 'Добавление товара Артикул:' . $product['sku'];
            }
            elseif ($action == 'update') { //Обновление
                if ( ! isset($product['url_key'])) {
                    $product['url_key'] = Fenix::stringProtectUrl($url) . $product['sku'];
                }

                $this->setTable('catalog_products')
                    ->update($product, $this->getAdapter()->quoteInto('id = ?', $productId));
                $cacheUpdate[] = 'Обновление товара Артикул:' . $product['sku'];
            }
            else { //Ошибка
                $log[] = 'Ошибка добавления товара Артикул:' . $product['sku'];
            }

            /**
             *Обновляем связи с категориями
             */
            if (count($categories) > 0) {
                //Удаляем старые связи
                $this->setTable('catalog_relations');
                $this->delete($this->getAdapter()->quoteInto('product_id = ?', $productId));

                //Добавляем новые
                $importedCategories = array();
                foreach ($categories as $categoryId) {
                    $categoryId = trim($categoryId);
                    if (in_array($categoryId, $importedCategories)) {
                        continue;
                    } // Если такая запись уже добавлена, пропускаем

                    //Проверяем как внесли категории через ID или Названием
                    if (preg_match('/\A\d+\Z/i', $categoryId)) {
                        $category = Fenix::getModel('catalog/backend_categories')->getCategoryById($categoryId);
                    }
                    else {
                        $category = Fenix::getModel('catalog/backend_categories')->getCategoryByTitleRussian($categoryId);
                    }
                    if ($category) {
                        $categoryRelation = array();
                        $categoryRelation['parent'] = $categoryId;
                        $categoryRelation['product_id'] = $productId;
                        $categoryRelation['left'] = $category->left;
                        $categoryRelation['right'] = $category->right;

                        $this->setTable('catalog_relations');
                        $this->insert($categoryRelation);

                        $importedCategories[] = $categoryId;
                    }
                }
                if (count($importedCategories) == 0) {
                    $categoryId = Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT;
                    $category = Fenix::getModel('catalog/backend_categories')->getCategoryById($categoryId);
                    if ($category) {
                        $categoryRelation = array();
                        $categoryRelation['parent'] = $categoryId;
                        $categoryRelation['product_id'] = $productId;
                        $categoryRelation['left'] = $category->left;
                        $categoryRelation['right'] = $category->right;

                        $this->setTable('catalog_relations');
                        $this->insert($categoryRelation);
                    }
                }
            }

            /**
             *Обновляем значения атрибутов товара
             */
            foreach ($productAttributes as $key => $arrtibute) {
                //Удаляем старые связи
                $this->setTable('attr_values');
                $this->delete(
                    $this->getAdapter()->quoteInto('product_id = ?', $productId) . ' AND ' .
                    $this->getAdapter()->quoteInto('attribute_id = ?', $importAttr[$key]['id']));

                //Добавляем новые связи
                foreach ($arrtibute as $value) {
                    //Проверяем есть ли такое значение
                    if (array_key_exists($value, $importAttr[$key]['values'])) {
                        $data = array();
                        $data['attribute_id'] = $importAttr[$key]['values'][$value]['attribute_id'];
                        $data['product_id'] = $productId;
                        $data['value_id'] = $importAttr[$key]['values'][$value]['id'];

                        $this->setTable('attr_values');
                        $this->insert($data);
                    }
                    else {
                        $data = array();
                        $data[$importAttr[$key]['sys_title'] . $importAttr[$key]['lang_name']] = $value;
                        $valueId = Fenix::getModel('catalog/backend_products')->saveAttributeValue($importAttr[$key]['object'], $data);
                        $importAttr[$key]['values'][$value]['id'] = $valueId;
                        $importAttr[$key]['values'][$value]['attribute_id'] = $importAttr[$key]['id'];

                        $data = array();
                        $data['product_id'] = $productId;
                        $data['attribute_id'] = $importAttr[$key]['values'][$value]['attribute_id'];
                        $data['value_id'] = $importAttr[$key]['values'][$value]['id'];

                        $this->setTable('attr_values');
                        $this->insert($data);
                    }
                }
            }

            /**
             *Изображения товара
             */
            if (($image != '') || count($gallery) > 0) {
                $productImagesDir = HOME_DIR_ABSOLUTE . 'catalog/products/' . $productId . '/';
                if ( ! is_dir($productImagesDir)) {
                    mkdir($productImagesDir, 0777, true);
                }
            }

            if ($image != '') {
                $path = BASE_DIR . self::PATH_TO_UPLOAD_IMAGES . '/' . $image;
                if (file_exists($path)) {
                    if (copy($path, $productImagesDir . $image)) {
                        $imagePath = 'catalog/products/' . $productId . '/' . $image;
                        $this->setTable('catalog_products')
                            ->update(array('image' => $imagePath), $this->getAdapter()->quoteInto('id = ?', (int) $productId));
                    }
                }

            }

            //Галерея изображений товара
            if (count($gallery) > 0) {
                $imagesList = array();
                foreach ($gallery as $img) {
                    $path = BASE_DIR . self::PATH_TO_UPLOAD_IMAGES . '/' . $img;
                    if (file_exists($path)) {
                        if (copy($path, $productImagesDir . $img)) {
                            $imagePath = 'catalog/products/' . $productId . '/' . $img;
                            $imagesList[$imagePath] = array(
                                'image' => $imagePath
                            );
                        }
                    }
                }
                $this->setTable('catalog_products')
                    ->update(array('gallery' => serialize($imagesList)), $this->getAdapter()->quoteInto('id = ?', (int) $productId));
            }
        }

        $backendAttributes = Fenix::getModel('catalog/backend_attributes');

        $backendAttributes->fillEmptyValuesUrl();

        foreach ($updatedAttributeIDs as $attributeId) {
            $result = $backendAttributes->findAttributeById($attributeId);
            if ($result > 0) {
                $backendAttributes->updateAttributePosition($attributeId);
            }
        }

        //Fenix::dump(Fenix_Excel::report('Report', $time_start, $memory_start), $cacheNew, $cacheUpdate, $log);

        return (object) array(
            'update' => count($cacheUpdate),
            'new'    => count($cacheNew)
        );
    }

    public function importPrices($import_file, $_parent)
    {
        //Основные настройки
        ini_set('memory_limit', '512M');
        set_time_limit(120);
        $attrSysTitle = 2; //Строка системного названия
        $memory_start = memory_get_usage();
        $time_start = microtime(true);
        $UserId = Fenix::getModel('session/auth')->getUser()->id;
        $Limit = 2; //Количество товаров обрабатываемых за один круг
        $skuList = array();

        //Атрибуты
        $lang = Fenix_Language::getInstance();
        $attrList = array(); //Список атрибутов

        //Статистика импорта
        $cacheNew = array();
        $cacheUpdate = array();
        $log = array();

        //Создаем объект для импорта
        $filename = $import_file->tmp_name;
        $Excel = Fenix_Excel::newGetObject($filename);

        //Собираем базовые параметры
        $sheet = $Excel->getSheet();
        $allAttributes = $sheet->cells[$attrSysTitle];
        $importAttr = array();
        $category_key = '';
        $sql_types = array();

        //Удаляем системные строки
        unset($sheet->cells[1]);
        unset($sheet->cells[$attrSysTitle]);

        foreach ($allAttributes as $key => $attr) {
            $attr = trim($attr);
            if ($attr == 'sku') {
                $system[$key] = 'sku';
                continue;
            }
            if ($attr == 'group_sku') {
                $system[$key] = 'group_sku';
                continue;
            }
            if ($attr == 'type') {
                $system[$key] = 'type';
                continue;
            }
            if ($attr == 'price') {
                $system[$key] = 'price';
                continue;
            }
            if ($attr == 'price_old') {
                $system[$key] = 'price_old';
                continue;
            }
            $values = array();
            foreach ($lang->getLanguagesList() as $_lang) {
                if (substr($attr, -strlen($_lang->name)) == $_lang->name) {
                    $attrLangName = '_' . $_lang->name;
                    $attr = substr($attr, 0, -(strlen($_lang->name) + 1));
                    break;
                }
                else {
                    $attrLangName = '';
                }
            }

            $check = Fenix::getModel('catalog/backend_system_attributes')->getAttributeBySysTitle($attr);
            $checkId = '';
            $checkSqlType = '';
            $checkSplitByLang = '';

            if ($check == null) {
                $check = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attr);
                if ($check == null) {
                    $type = null;
                }
                else {
                    $checkId = $check->id;
                    $checkSqlType = $check->sql_type;
                    $checkSplitByLang = $check->split_by_lang ? 'lang' : '';
                    $type = '0';
                    $values_tmp = Fenix::getModel('catalog/backend_attributes')->getAttributeValues($check);
                    foreach ($values_tmp as $value) {
                        $values[$value->content] = $value->toArray();
                    }
                }
            }
            else {
                $checkId = $check->id;
                $checkSqlType = $check->sql_type;
                $checkSplitByLang = $check->split_by_lang ? 'lang' : '';
                $type = '1';
            }
            $importAttr[$key]['key'] = $key;
            $importAttr[$key]['id'] = $checkId;
            $importAttr[$key]['sys_title'] = $attr;
            $importAttr[$key]['sql_type'] = strtolower($checkSqlType);
            $importAttr[$key]['lang'] = $checkSplitByLang;
            $importAttr[$key]['lang_name'] = $attrLangName;
            $importAttr[$key]['is_system'] = $type;
            $importAttr[$key]['values'] = $values;
            $importAttr[$key]['object'] = $check->toArray();
        }

        foreach ($sheet->cells as $i => $item) {

            $product = '';
            $productId = '';
            $attributesData = '';
            /**
             *Собираем массив атрибутов
             */
            foreach ($item as $key => $str) {
                if (array_key_exists($key, $system)) {
                    $data[$system[$key]] = $str;
                    if ($system[$key] == 'sku') {
                        $productTmp = Fenix::getModel('catalog/products')->getProductBySku($str);
                        $productId = $productTmp->id;
                        if ( ! array_key_exists($str, $skuList)) {
                            Fenix::getModel('catalog/backend_configurable')->clearConfigurable($productId);
                            Fenix::getModel('catalog/backend_configurable')->clearConfigurableAttributes($productId);
                            $skuList[$str] = 'cleared';
                        }
                    }
                    continue;
                }

                if ( ! isset($importAttr[$key])) {
                    continue;
                }

                //Если значение существует все ок
                if (isset($importAttr[$key]['values'][$str])) {
                    $attributesData[$importAttr[$key]['id']] = $importAttr[$key]['values'][$str]['id'];
                }
                else {

                    //Если значение не существует - добавляем
                    $valueData[$importAttr[$key]['sys_title'] . $importAttr[$key]['lang_name']] = $str;
                    $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($importAttr[$key]['object']['id']);

                    $valueId = Fenix::getModel('catalog/backend_products')->saveAttributeValue($attribute, $valueData);

                    $attributesData[$importAttr[$key]['id']] = $valueId;
                }
            }
            //Если значение существует все ок
            if (isset($importAttr[$key]['values'][$str])) {
                $attributesData[$importAttr[$key]['id']] = $importAttr[$key]['values'][$str]['id'];

            }
            else {
                if (isset($importAttr[$key])) {
                    //Если значение не существует - добавляем
                    $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($importAttr[$key]['object']['id']);

                    $valueData[$importAttr[$key]['sys_title'] . $importAttr[$key]['lang_name']] = $str;
                    $valueId = Fenix::getModel('catalog/backend_products')->saveAttributeValue($attribute, $valueData);

                    $attributesData[$importAttr[$key]['id']] = $valueId;

                    //Добавляем в список значений данные о добавленом значении
                    $importAttr[$key]['values'][$str]['id'] = $valueId;

                }
            }
            /*if ($i == 376)
            {
                Fenix::dump($item, $productId, $data['group_sku'], $data['type'], $data['price'], $attributesData);
                Fenix::dump($sheet->cells);
            }*/

            Fenix::getModel('catalog/backend_configurable')->addConfigurable($productId, $data, $attributesData);
            $cacheUpdate[] = 1;
        }

        //Fenix::dump(Fenix_Excel::report('Report', $time_start, $memory_start), $cacheNew, $cacheUpdate, $log);

        return (object) array(
            'update' => count($cacheUpdate)
        );
    }
}