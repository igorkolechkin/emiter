<?php
class Fenix_Catalog_Model_Material extends Fenix_Resource_Model
{
    /**
     * Активный ли пункт меню слайдера в админке
     *
     * @return bool
     */
    static public function isMaterialActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'catalog' && Fenix::getRequest()->getUrlSegment(1) == 'material';
    }

    /**
     * Список слайдеров
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getMaterialListAsSelect($parent = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material');

        $this   ->setTable('catalog_material');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'm' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'm'
        )));
        if($parent!==null){
            $Select->where('m.parent = ?', $parent);
        }

        $Select ->order('m.position asc');

        return $Select;
    }

    /**
     * Список слайдов
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getItemListAsSelect($parent = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material_item');

        $this   ->setTable('catalog_material_item');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'mi' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'mi'
        )));

        if ($parent) {
            $Select->where('mi.parent = ?', (int)$parent);
        }

        $Select ->order('mi.position asc');

        return $Select;
    }

    /**
     * Список слайдов
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getMaterialList($parent = null)
    {
        $Select = $this->getMaterialListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список слайдов
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getItemList($parent)
    {
        $Select = $this->getItemListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список  опубликованых блоков
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getPublicItemList($parent, $productId = null)
    {
        $Select = $this->getItemListAsSelect($parent);
        if($productId){

            $Select->join(array(
                'mp' => $this->getTable('catalog_material_products')
            ),'mi.id = mp.material_item_id', null);

            $Select->where('mp.product_id = ?', $productId);

            $parentMaterial = $this->getMaterialById($parent);
            if($parentMaterial){
                $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($parentMaterial->attribute_id);

            }
        }
        $Select->where('mi.is_public = ?','1');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Слайдер по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getMaterialById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material');

        $this   ->setTable('catalog_material');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайдер по названию
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getMaterialByName($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material');

        $this->setTable('catalog_material');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('name = ?', (string) $name)
                ->where('is_public = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }


    /**
     * Слайд по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getItemById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material_item');

        $this   ->setTable('catalog_material_item');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function addItem($Form, $req){

        $id = $Form ->addRecord(
            $req
        );

        $data = array(
            'value_id' => $req->getPost('value_id')
        );

        $this->setTable('catalog_material_item')->update($data,
            $this->getAdapter()->quoteInto('id = ?', $id)
        );

        return $id;
    }

    public function editItem($Form, $currentItem, $req){

        $Form ->editRecord($currentItem, $req);

        $data = array(
            'value_id' => $req->getPost('value_id')
        );

        $result = $this->setTable('catalog_material_item')->update($data,
            $this->getAdapter()->quoteInto('id = ?', $currentItem->id)
        );

        return $result;
    }
    public function getProductMaterialList($productId, $parent = 0, $viewSection = null){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/material');
        $cols = $Engine ->getColumns(array('prefix'=>'m'));

        $Select = $this->getItemListAsSelect();

        $Select->reset(Zend_Db_Select::COLUMNS);

        $Select->join(array(
            'm' => $this->getTable('catalog_material')
        ),'m.id = mi.parent', $cols);

        $Select->join(array(
            'mp' => $this->getTable('catalog_material_products')
        ),'mi.id = mp.material_item_id', null);

        $Select->group('m.id');

        //$Select->where('m.parent = ?',$parent);

        $Select->where('mi.is_public = ?','1');
        $Select->where('mp.product_id = ?', $productId);
        $Select->order('m.position asc');

        $Result = $this->fetchAll($Select);

        $materialIdList=array();

        foreach($Result as $record){
            $materialIdList[] = $record->id;

                $materialIdList[] = $record->parent;
        }

        if(count($materialIdList)==0)
            return $Result;

        $Select = $this->getMaterialListAsSelect($parent);
        $Select->where('m.id IN ("' . implode('","', $materialIdList) . '")');
        if($viewSection){
            $Select->where('m.view_section = ? OR m.parent = 0', $viewSection);
        }
        $Result = $this->fetchAll($Select);
        /*if($viewSection !='tab-material')
        Fenix::dump($Select->assemble(),$Result );*/
        return $Result;
    }
}