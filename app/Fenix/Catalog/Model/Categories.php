<?php

class Fenix_Catalog_Model_Categories extends Fenix_Resource_Model
{
    public function setUrl($urlKey = null)
    {
        Fenix_Debug::log('categories setUrl begin');
        $req = Fenix::getRequest();
        $pathInfo = $req->getParsedInfo();
        $_segments = $pathInfo->segments;

        //Пустой URL
        if (count($_segments) == 0) {
            return false;
        }

        if ($urlKey == null) {
            $urlKey = $_segments[0];
        }

        $category = $this->getCategoryByUrlKey($urlKey);

        //По урлу не нашли категорию
        if ($category == null) {
            return false;
        }

        //Данные категории
        $currentCategory = Fenix::getCollection('catalog/category_category')->setCategory($category->toArray());

        $category = new Fenix_Object(array(
            'data' => array(
                'current'    => $currentCategory,
                'navigation' => Fenix::getCollection('catalog/categories')->getCategoryNavigationByCategory($currentCategory)
            )
        ));

        Fenix_Debug::log('categories setUrl end');

        return $category;
    }

    /**
     * Категория по Id
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCategoryById($id)
    {
        $cacheId = 'getCategoryById_' . $id;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getFrontendColumns());
        $Select->where('id = ?', (int) $id);
        $Select->where('is_public = ?', '1');

        $Select->limit('position asc');
        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Категория по url_key
     * @param $urlKey
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCategoryByUrlKey($urlKey)
    {
        $cacheId = 'getCategoryByUrlKey_' . $urlKey;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $this->setTable('catalog');

        $Select = $this->select();
        $Select->from(array(
            'c' => $this->_name
        ), $Engine->getFrontendColumns(array('prefix' => 'c')));
        $Select->where('c.url_key = ?', $urlKey);
        $Select->where('is_public = ?', '1');

        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Список категорий по родителю
     *
     * @param int $parent Идентификатор родительской категории
     * @return Zend_Db_Table_Rowset
     */
    public function getCategoriesList($parent = 1)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $cols = $Engine->getFrontendColumns();

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $cols);
        $Select->where('parent = ?', (int) $parent)
            ->where('is_public = ?', '1');
        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }


    /**
     * Список категорий для навигации
     *
     * @param int $parent
     * @return Zend_Db_Table_Rowset
     */
    public function getNavCategoriesList($parent = 1)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $cols = $Engine->getFrontendColumns(array(
            'columns' => array('id', 'parent', 'left', 'right', 'title', 'image', 'url_key', 'image_category')
        ));

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $cols);

        $Select->where('parent = ?', $parent);
        $Select->where('is_public = ?', Fenix::IS_PUBLIC);

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список категорий по параметрам
     *
     * @param $params
     * @return Zend_Db_Table_Rowset
     */
    public function getQueryCategoriesList($params = array())
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $cols = $Engine->getFrontendColumns();

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $cols);

        foreach ($params as $param => $value) {
            $Select->where($param . ' = ?', $value);
        }
        $Select->where('is_public = ?', '1');

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Цепочка родительских категорий
     *
     * TODO Переделать под один запрос
     *
     * @param $id
     * @return null|Zend_Db_Table_Rowset_Abstract
     */
    public function getCatalogNavigation($category)
    {
        // Список категорий в навигации
        if (Zend_Registry::isRegistered('Category_NAV_' . $category->left . '_' . $category->right)) {
            $Navigate = Zend_Registry::get('Category_NAV_' . $category->left . '_' . $category->right);
        }
        else {
            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/categories');
            $Select = $this->setTable('catalog')
                ->select();
            $params = array('columns' => array('id', 'parent', 'left', 'right', 'title', 'image', 'url_key'));
            $Select->from($this->_name, $Engine->getFrontendColumns($params));
            $Select->where('`left` <= ?', $category->left)
                ->where('`right` >= ?', $category->right)
                ->where('parent > 0');

            $Select->where('is_public = ?', '1');

            $Select->order('left asc');

            $Navigate = $this->fetchAll($Select);
            Zend_Registry::set('Category_NAV_' . $category->left . '_' . $category->right, $Navigate);
        }

        return $Navigate;
    }

    /**
     * Сборка урла для всех вложеностей
     *
     * @param $id
     * @return null|Zend_Db_Table_Rowset_Abstract
     */
    public function getCategoryUrl($id)
    {
        $navigation = $this->getCatalogNavigation($id);
        $url = array();
        foreach ($navigation AS $_nav) {
            $url[] = $_nav->url_key;
        }

        //Fenix::dump($id, $url);
        return Fenix::getUrl(implode('/', $url));
    }

    /**
     * Урл категории
     * @param $category
     * @return string
     */
    public function getUrl($category)
    {
        return Fenix::getUrl($category->url_key);
    }


    public function getCategoryNavigationById($categoryId)
    {
        Fenix_Debug::log('categories model getCategoryNavigationById begin');
        // Список категорий в навигации
        $cacheId = 'getCatalogNavigationId_' . $categoryId;
        if (Zend_Registry::isRegistered($cacheId)) {
            $Navigate = Zend_Registry::get($cacheId);

            Fenix_Debug::log('categories model getCategoryNavigationById end registry');

            return $Navigate;
        }
        else {
            $category = Fenix::getModel('catalog/categories')->getCategoryById($categoryId);

            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/categories');
            $Select = $this->setTable('catalog')
                ->select();
            $Select->from($this->_name, $Engine->getColumns());
            $Select->where('`left` <= ?', $category->left)
                ->where('`right` >= ?', $category->right)
                ->where('parent > 0');

            $Select->where('is_public = ?', '1');

            $Select->order('left asc');

            $Navigate = $this->fetchAll($Select);
            Zend_Registry::set($cacheId, $Navigate);
        }
        Fenix_Debug::log('categories model getCategoryNavigationById end');

        return $Navigate;
    }

    public function getCategoryNavigationByCategory($category)
    {
        Fenix_Debug::log('categories model getCategoryNavigationByCategory begin');
        // Список категорий в навигации
        $cacheId = 'getCatalogNavigationId_' . $category->id;
        if (Zend_Registry::isRegistered($cacheId)) {
            $Navigate = Zend_Registry::get($cacheId);

            Fenix_Debug::log('categories model getCategoryNavigationByCategory end registry');

            return $Navigate;
        }
        else {
            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/categories');
            $Select = $this->setTable('catalog')
                ->select();
            $Select->from($this->_name, $Engine->getColumns());
            $Select->where('`left` <= ?', $category->left)
                ->where('`right` >= ?', $category->right)
                ->where('parent > 0');

            $Select->where('is_public = ?', '1');

            $Select->order('left asc');

            $Navigate = $this->fetchAll($Select);
            Zend_Registry::set($cacheId, $Navigate);
        }
        Fenix_Debug::log('categories model getCategoryNavigationById end');

        return $Navigate;
    }

    /**
     * Количество товаров в категории по Id категории
     *
     * @param $catalog
     *
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCategoryProductsCountById($catalog)
    {
        $cacheId = 'getCategoryProductsCountById_' . $catalog->id;

        if (Zend_Registry::isRegistered($cacheId)) {
            Fenix::dump(Zend_Registry::get($cacheId));

            return Zend_Registry::get($cacheId);
        }

        $this->setTable('catalog_relations');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'r' => $this->getTable(),
        ), null);

        // Джоиним товарчеги
        $cols = Fenix::getModel('catalog/products')->productColumns();

        $Select->join(array(
            'p' => $this->getTable('catalog_products'),
        ), 'p.id = r.product_id', $cols);

        $Select->join(array(
            'cur' => $this->getTable('core_currency'),
        ), 'cur.name = p.currency', array('name AS currency_name', 'crosscourse'));

        $Select->where('r.left >= ?', (int) $catalog->left)
            ->where('r.right <= ?', (int) $catalog->right);

        $Select->where('r.parent <> ?', (int) Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT);
        //$Select->where('r.parent = ?', (int)$catalog->id);

        $Select->where('p.is_public = ?', '1')
            ->where('p.is_visible = ?', '1');

        $Select->columns(new Zend_Db_Expr(
            'CASE ' .
            'WHEN p.group_id = 0 ' .
            'THEN (select max(group_id) FROM ' . $this->getTable('catalog_products') . ') + p.id ' .
            'ELSE p.group_id END as product_group_id'));

        $catalogSelect = $this->select()->setIntegrityCheck(false)->from(array(
            'p' => new Zend_Db_Expr('(' . $Select->assemble() . ')'),
        ), array('product_group_id'));

        $catalogSelect->columns(new Zend_Db_Expr('COUNT(DISTINCT p.product_group_id) as products_count'));
        $catalogSelect->limit(1);
        $Result = $this->fetchRow($catalogSelect);

        $products_count = 0;

        if ($Result != null) {
            $products_count = $Result->products_count;

            Zend_Registry::set($cacheId, $products_count);
        }

        return $products_count;
    }
}