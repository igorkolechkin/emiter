<?php
class Fenix_Catalog_Model_Backend_Coupons extends Fenix_Resource_Model
{
    public function getCoupon($coupon)
    {
        $this->setTable('catalog_coupons');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('coupon = ?', $coupon);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getCouponById($id)
    {
        $this->setTable('catalog_coupons');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getCouponByName($name)
    {
        $this->setTable('catalog_coupons');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('name = ?', $name);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getCouponsListAsSelect()
    {
        $this->setTable('catalog_coupons');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ));

        return $Select;
    }

    public function addCoupon($Form, $req)
    {
        $couponId = $Form->addRecord($req);
        return $couponId;
    }

    public function addManyCoupon($Form, $req)
    {
        for ($i = 0 ; $i < $req->getPost('counter'); $i++) {
            $req->setPost('coupon',     Fenix::getRand(6, false, array(0,1,2,3,4,5,6,7,8,9)));
            $req->setPost('is_active',  '1');

            $Form->addRecord($req);
        }
        return;
    }

    public function editCoupon($Form, $current, $req)
    {
        $couponId  = $Form->editRecord($current, $req);
        return $couponId;
    }


    public function deleteCoupon($Form, $current)
    {
        return $Form->deleteRecord($current);
    }
}