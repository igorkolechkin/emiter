<?php
class Fenix_Catalog_Model_Backend_Promo extends Fenix_Resource_Model
{
    public function getBlockById($id)
    {
        $this->setTable('catalog_promo');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getBlockByName($name)
    {
        $this->setTable('catalog_promo');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('name = ?', $name);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getPromoListAsSelect()
    {
        $this->setTable('catalog_promo');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ));

        return $Select;
    }

    public function addBlock($Form, $req)
    {
        $req->setPost('last_date', $req->getPost('last_date') . ' ' . $req->getPost('last_time'));

        $blockId  = $Form->addRecord($req);

        $products = $req->getPost('_list');


        $this->setTable('catalog_promo_products');
        $this->delete('block_id = ' . (int) $blockId);

        foreach ((array) $products['promoProducts'] AS $_i => $_id) {
            $this->insert(array(
                'block_id'   => $blockId,
                'product_id' => $_id,
                'position'   => ($_i + 1)
            ));
        }
        return $blockId;
    }

    public function editBlock($Form, $current, $req)
    {
        $req->setPost('last_date', $req->getPost('last_date') . ' ' . $req->getPost('last_time'));

        $blockId  = $Form->editRecord($current, $req);



        $products = $req->getPost('_list');

        $this->setTable('catalog_promo_products');
        $this->delete('block_id = ' . (int) $blockId);

        foreach ((array) $products['promoProducts'] AS $_i => $_id) {
            $this->insert(array(
                'block_id'   => $blockId,
                'product_id' => $_id,
                'position'   => ($_i + 1)
            ));
        }
        return $blockId;
    }


    public function deleteBlock($Form, $current)
    {
        $blockId  = $Form->deleteRecord($current);

        $this->setTable('catalog_promo_products');
        $this->delete('block_id = ' . (int) $blockId);
    }

    public function getProductsListByBlockId($blockId)
    {
        $this->setTable('catalog_promo_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());

        $Select ->where('_p.block_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    public function getProductsBrowser($blockId)
    {
        return Fenix::getHelper('catalog/backend_browser')->getSimpleProductsFilter(array(
            'name'      => 'promoProducts',
            'url'       => Fenix::getUrl('catalog/products/find'),
            'products'  => Fenix::getModel('catalog/backend_promo')->getProductsListByBlockId($blockId)
        ));
    }

    public function addLastOrder($order){
        $block = self::getBlockByName('last-order');

        if($block){
            $blockId = (int)$block->id;
            $this->setTable('catalog_promo');
            $data = array(
                'last_date' => $order->create_date
            );
            $this->update($data, $this->getAdapter()->quoteInto('id = ?', $block->id) .' AND `last_date` < "'.$order->create_date.'"');

            $this->setTable('catalog_promo_products');

            foreach ($order->products AS $_i => $product) {
                //удаляем если такой товар уже есть в блоке
                $this->setTable('catalog_promo_products');
                $this->delete('block_id = ' . $blockId . ' AND product_id = ' . $product->id);

                //Добавляем товар в блок
                $this->setTable('catalog_promo_products');
                $this->insert(array(
                    'block_id'   => $blockId,
                    'product_id' => $product->id,
                    'position'   => ($_i + 1),
                    'modify_date'=> date('Y-m-d H:m:i')
                ));
            }
            //Удаляем товары если в блоке больше восьми товаров
            $this->setTable('catalog_promo_products');

            $Select = $this->select()
                           ->setIntegrityCheck(false);

            $Select ->from(array(
                '_p' => $this->getTable()
            ), null);

            // Джоиним товарчеги
            $Select ->join(array(
                'p' => $this->getTable('catalog_products')
            ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());
            $Select->where('_p.block_id = ?', (int)$blockId);
            $Select->order('_p.modify_date asc');
            $products = $this->fetchAll($Select);

            foreach ($products as $i => $_product) {
                if ($i > 7) {
                    //удаляем если такой товар уже есть в блоке
                    $this->setTable('catalog_promo_products');
                    $this->delete('block_id = ' . $blockId . ' AND product_id = ' . $product->id);
                }
            }
        }
    }
    public function getLastOrderDate(){
        $block = self::getBlockByName('last-order');

        if($block) {
            $blockId = $block->id;
            $this->setTable('catalog_promo_products');
            $Select = $this->select()
                           ->setIntegrityCheck(false);
            $Select->from(array(
                '_p' => $this->getTable()
            ), array('MAX(_p.modify_date) as last_date'));
            $Select->where('_p.block_id = ?', (int)$blockId);
            $Select->group('_p.block_id');
            $data = $this->fetchRow($Select);

            if($data){
                return $data->last_date;
            }
        }

        return false;
    }
}