<?php

class Fenix_Catalog_Model_Backend_Products extends Fenix_Resource_Model
{
    /**
     *
     */
    const PRODUCT_NO_CATEGORY_PARENT = 2;

    /**
     * @param $product
     * @param null $child
     * @return float
     */
    public function calculateSetPrice($product, $child = null)
    {
        /*        $price = Fenix::getModel('catalog/products')->getProductPrice($product);

                $setList = $this->getSetList($product);
                foreach ($setList AS $_product) {
                    $setPrice    = $_product->set_price;
                    $setDiscount = $_product->set_discount;
                    if ($setPrice == 0) {
                        $setPrice = Fenix::getModel('catalog/products')->getProductPrice($_product) - ($setDiscount * Fenix::getModel('catalog/products')->getProductPrice($_product) / 100);
                    }
                    $price+= $setPrice;
                }
        */
        $priceOld = Fenix::getModel('catalog/products')->getProductPrice($product) + Fenix::getModel('catalog/products')->getProductPrice($child);

        $setPrice = $child->set_price;
        $setDiscount = $child->set_discount;
        if ($setPrice == 0) {
            $setPrice = $priceOld - $priceOld * $setDiscount / 100;
        }


        return round($setPrice, 2);
    }

    /**
     * @param $product
     * @return float
     */
    public function calculateSet2Price($product)
    {
        $price = Fenix::getModel('catalog/products')->getProductPrice($product);

        $setList = $this->getSet2List($product);
        foreach ($setList AS $_product) {
            $setPrice = $_product->set_price;
            $setDiscount = $_product->set_discount;
            if ($setPrice == 0) {
                $setPrice = Fenix::getModel('catalog/products')->getProductPrice($_product) - ($setDiscount * Fenix::getModel('catalog/products')->getProductPrice($_product) / 100);
            }
            $price += $setPrice;
        }

        return ceil($price);
    }

    /**
     * @param $product
     * @return Zend_Db_Table_Rowset|Zend_Db_Table_Rowset_Abstract
     */
    public function getSetList($product)
    {
        if ($product === false) {
            return new Zend_Db_Table_Rowset(array(
                'data' => array()
            ));
        }

        $this->setTable('catalog_products_set');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), array(
            '_p.price AS set_price',
            '_p.discount AS set_discount'
        ));

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.product_id = ?', (int)$product->id);

        $Select->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * @param $product
     * @return Zend_Db_Table_Rowset|Zend_Db_Table_Rowset_Abstract
     */
    public function getSet2List($product)
    {
        if ($product === false) {
            return new Zend_Db_Table_Rowset(array(
                'data' => array()
            ));
        }

        $this->setTable('catalog_products_set2');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), array(
            '_p.price AS set_price',
            '_p.discount AS set_discount'
        ));

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.product_id = ?', (int)$product->id);

        $Select->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * @param $product
     * @return Zend_Db_Table_Rowset|Zend_Db_Table_Rowset_Abstract
     */
    public function getUpsellList($product)
    {
        if ($product === false) {
            return new Zend_Db_Table_Rowset(array(
                'data' => array()
            ));
        }

        $this->setTable('catalog_products_upsell');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.product_id = ?', (int)$product->id);

        $Select->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * @param $product
     * @return Zend_Db_Table_Rowset|Zend_Db_Table_Rowset_Abstract
     */
    public function getRelatedList($product)
    {
        if ($product === false) {
            return new Zend_Db_Table_Rowset(array(
                'data' => array()
            ));
        }

        $this->setTable('catalog_products_related');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.product_id = ?', (int)$product->id);

        $Select->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * @param $term
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function findProduct($term)
    {
        $fields = array(
            'title_russian',
            'sku',
            'id_1c',
        );

        $where = array();
        foreach ($fields AS $_field) {
            $where[] = $this->getAdapter()->quoteInto($_field . ' LIKE ?', '%' . $term . '%');
        }

        $this->setTable('catalog_products');

        $Select = $this->select();
        $Select->from($this, $this->productColumns(null));
        $Select->where(implode(' OR ', $where));

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getProductById($id)
    {
        $this->setTable('catalog_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'p' => $this->_name
        ), $this->productColumns());

        $Select->where('p.id = ?', $id);

        //$Select = $this->appendUserAttributesToSelect($Select);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * @param $category
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getProductsList($category)
    {
        $Select = $this->getProductsListAsSelect($category);

        $this->getTable('catalog_products');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * @param $category
     * @return Zend_Db_Select
     */
    public function getProductsListAsSelect($category = null)
    {
        /* $this->setTable('catalog');

         $Select = $this->select()
                        ->setIntegrityCheck(false);

         $Select ->from(array(
             'c' => $this->getTable()
         ), null);

         // Джоиним товарчеги
         $Select ->join(array(
             'p' => $this->getTable('catalog_products')
         ), 'c.id = p.parent', $this->productColumns());

         $Select ->where('c.left >= ?', (int) $category->left)
                 ->where('c.right <= ?', (int) $category->right);

         //$Select ->order('p.title_' . Fenix_Language::getInstance()->getCurrentLanguage()->name . ' asc');
         $Select ->order('p.position asc');*/


        $this->setTable('catalog_relations');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'r' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $cols = $this->productColumns();

        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'p.id = r.product_id', $cols);

        if ($category) {
            $Select->where('r.left >= ?', (int)$category->left)
                ->where('r.right <= ?', (int)$category->right);
        }

        $Select->group('p.id');

        //$Select = $this->appendUserAttributesToSelect($Select, array('prefix' => 'av'));
        return $Select;
    }

    /**
     * @param $Select
     * @param null $options
     * @return mixed
     */
    public function appendUserAttributesToSelect($Select, $options = null)
    {
        $prefix = isset($options['prefix']) ? $options['prefix'] : 'av';

        //Привязка к значениям
        $Select->joinLeft(array(
            $prefix => $this->getTable('attr_values')
        ), 'p.id = ' . $prefix . '.product_id', null);

        $langList = Fenix_Language::getInstance()->getLanguagesList();
        $attributes = Fenix::getModel('catalog/backend_attributes')->getAttributesList();

        foreach ($attributes AS $i => $_attribute) {
            $cols = array();
            $index = $_attribute->id;
            //Название таблицы для джойна
            $tableName = 'attr_values_' . strtolower($_attribute->sql_type);
            $column = 'content';
            if ($_attribute->split_by_lang == '1') {
                // добавляем признак языка к названию таблицы
                $tableName .= '_lang';

                //Колонки
                foreach ($langList as $lang) {
                    $cols[] = $prefix . $index . '.' . $column . '_' . $lang->name . ' AS ' . $_attribute->sys_title . '_' . $lang->name;
                    //$cols[] = $prefix . $i . '.' . $column .'_'. $lang->name . ' AS user_' . $_attribute->sys_title . '_' . $lang->name;
                }
            } else {
                $cols[] = $prefix . $index . '.' . $column . ' AS ' . $_attribute->sys_title;
                //$cols[] = $prefix . $i . '.' . $column . ' AS user_' . $_attribute->sys_title;
            }
            $Select->joinLeft(array(
                $prefix . $index => $this->getTable($tableName)
            ),
                $prefix . $index . '.id = ' . $prefix . '.value_id AND ' . $prefix . '.attribute_id = ' . $_attribute->id,
                $cols);

        }
        $Select->group('p.id');

        //Fenix::dump($Select->assemble(),$cols);
        return $Select;
    }

    /**
     * @param string $prefix
     * @return array
     */
    public function productColumns($prefix = 'p.')
    {
        $attributes = Fenix::getModel('catalog/backend_system_attributes')->getAttributesList();
        $result = array($prefix . 'id', $prefix . 'parent');
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();

        foreach ($attributes AS $_attribute) {
            if ($_attribute->split_by_lang == '1') {
                $result[] = $prefix . $_attribute->sys_title . '_' . $lang->name . ' AS ' . $_attribute->sys_title;
                foreach (Fenix_Language::getInstance()->getLanguagesList() As $_name => $_options) {
                    $result[] = $prefix . $_attribute->sys_title . '_' . $_name;
                }
            } else {
                if ($_attribute->type == 'image') {
                    $result[] = $prefix . $_attribute->sys_title;
                    $result[] = $prefix . $_attribute->sys_title . '_info';
                } else {
                    $result[] = $prefix . $_attribute->sys_title;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $prefix
     * @return array
     */
    public function productUserColumns($prefix = 'u_p.')
    {
        $attributes = Fenix::getModel('catalog/backend_attributes')->getAttributesList();
        $result = array();
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();

        foreach ($attributes AS $_attribute) {
            if ($_attribute->split_by_lang == '1') {
                $result[] = $prefix . $_attribute->sys_title . '_' . $lang->name . ' AS ' . $_attribute->sys_title;
                foreach (Fenix_Language::getInstance()->getLanguagesList() As $_name => $_options) {
                    $result[] = $prefix . $_attribute->sys_title . '_' . $_name;
                }
            } else {
                if ($_attribute->type == 'image') {
                    $result[] = $prefix . $_attribute->sys_title;
                    $result[] = $prefix . $_attribute->sys_title . '_info';
                } else {
                    $result[] = $prefix . $_attribute->sys_title;
                }
            }
        }

        return $result;
    }

    /**
     * @param $category
     * @param $req
     * @return mixed|void
     * @throws Exception
     */
    public function addProduct($category, $req)
    {
        $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            $category->attributeset_id
        );

        if ($attributeset == null) {
            return;
        }

        // Группы набора атрибутов
        $GroupsList = Fenix::getModel('catalog/backend_attributeset')->getGroupsList(
            $attributeset->id
        );

        $systemData = array();
        $systemData['parent'] = $req->getParam('parent');
        $systemData['create_id'] = Fenix::getModel('session/auth')->getUser()->id;
        $systemData['create_date'] = date('Y-m-d H:i:s');


        //Сохраняем системные
        $this->setTable('catalog_products');
        $productId = $this->insert($systemData);

        foreach ($GroupsList AS $_group) {
            $AttributesList = Fenix::getModel('catalog/backend_attributeset')->getGroupAttributesList($_group->id);

            foreach ($AttributesList AS $_attribute) {
                switch ($_attribute->type) {
                    case 'checkbox':
                        $value = $this->getPostCheckboxValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveCheckboxValue($_attribute, $value, $productId);
                        }
                        break;


                    case 'image':
                        $value = $this->getPostImageValue($_attribute, $req, $productId);

                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($systemData, $value);

                            // Добавляем информацию об изображении
                            $imageInfo = Fenix_ImageInfo::getImageInfo($systemData[$_attribute->sys_title]);
                            $imageInfoKey = $_attribute->sys_title . '_info';

                            $value[$imageInfoKey] = serialize($imageInfo);

                            $systemData = array_merge($systemData, $value);
                        } else {
                            $this->saveImageValue($_attribute, $value, $productId);

                            // Добавляем информацию об изображении
                            $imageInfo = Fenix_ImageInfo::getImageInfo($value[$_attribute->sys_title]);
                            $userData = serialize($imageInfo);

                            $this->saveImageValueUser($_attribute, $userData, $productId);
                        }
                        break;

                    case 'file':
                        $value = $this->getPostFileValue($_attribute, $req, $productId);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveFileValue($_attribute, $value, $productId);
                        }
                        break;
                    case 'textarea':
                        $value = $this->getPostTextareaValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextareaValue($_attribute, $value, $productId);
                        }
                        break;
                    case 'text':
                        $value = $this->getPostTextValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextValue($_attribute, $value, $productId);
                        }
                        break;

                    default:
                        $value = $this->getPostTextValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextValue($_attribute, $value, $productId);
                        }
                        break;
                }
            }
        }

        // Галерея
        $value = $this->getPostGalleryValue($req, $productId);
        $systemData['gallery'] = $value['gallery'];

        //Сохраняем системные
        $this->setTable('catalog_products');
        $this->update($systemData, $this->getAdapter()->quoteInto('id = ?', $productId));

        //Сохраняем категории товара
        $this->saveCategoriesRelations($productId, $req);

        // Обновляем сопутствующие
        if (Fenix::getStaticConfig()->catalog->modules->related_products == '1') {

            $related = $req->getPost('_list');
            $related = $related['related'];

            $this->setTable('catalog_products_related')
                ->delete('product_id = ' . (int)$productId);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $productId,
                    'child_id'   => $_id,
                    'position'   => ($_i + 1)
                ));
            }
        }

        // Обновляем похожие
        if (Fenix::getStaticConfig()->catalog->modules->upsell_products == '1') {

            $related = $req->getPost('_list');
            $related = $related['upsell'];

            $this->setTable('catalog_products_upsell')
                ->delete('product_id = ' . (int)$productId);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $productId,
                    'child_id'   => $_id,
                    'position'   => ($_i + 1)
                ));
            }
        }

        // Обновляем наборы
        if (Fenix::getStaticConfig()->catalog->modules->products_set == '1') {

            $related = $req->getPost('_list');
            $related = $related['set'];

            $price = $req->getPost('_price');
            $price = $price['set'];

            $discount = $req->getPost('_persent');
            $discount = $discount['set'];

            $this->setTable('catalog_products_set')
                ->delete('product_id = ' . (int)$productId);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $productId,
                    'child_id'   => $_id,
                    'price'      => $price[$_i],
                    'discount'   => $discount[$_i],
                    'position'   => ($_i + 1)
                ));
            }
        }
        // Обновляем мультинаборы
        if (Fenix::getStaticConfig()->catalog->modules->products_multiset == '1') {
            self::updateMultiset($productId, $req);
        }

        // Обновляем сложный товар
        if (Fenix::getStaticConfig()->catalog->modules->configurable_products == '1') {
            Fenix::getModel('catalog/backend_configurable')->saveConfigurable($productId, $req);
        }

        return $productId;
    }

    /**
     * @param $category
     * @param $current
     * @param $req
     * @throws Exception
     */
    public function editProduct($category, $current, $req)
    {
        $productId = $current->id;
        $langList = Fenix_Language::getInstance()->getLanguagesList();

        $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            $category->attributeset_id
        );

        if ($attributeset == null) {
            return;
        }

        // Группы набора атрибутов
        $GroupsList = Fenix::getModel('catalog/backend_attributeset')->getGroupsList(
            $attributeset->id
        );

        $systemData = array();

        $systemData['create_id'] = Fenix::getModel('session/auth')->getUser()->id;
        $systemData['create_date'] = date('Y-m-d H:i:s');

        foreach ($GroupsList AS $_group) {
            $AttributesList = Fenix::getModel('catalog/backend_attributeset')->getGroupAttributesList($_group->id);

            foreach ($AttributesList AS $_attribute) {
                switch ($_attribute->type) {
                    case 'checkbox':
                        $value = $this->getPostCheckboxValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveCheckboxValue($_attribute, $value, $productId);
                        }
                        break;
                    case 'image':
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $value = $this->getPostImageValue($_attribute, $req, $productId,
                                $current->{$_attribute->sys_title});
                        } else {
                            $value = $this->getPostImageValue($_attribute, $req, $productId);
                        }

                        if ($value) {
                            // Удаление информации об изображении
                            if ($value == 'delete') {
                                if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                                    $valueAttr = array($_attribute->sys_title => '');

                                    $systemData = array_merge($systemData, $valueAttr);

                                    // Удаление информации об изображении
                                    $imageInfoKey = $_attribute->sys_title . '_info';
                                    $valueAttr = array($imageInfoKey => '');

                                    $systemData = array_merge($systemData, $valueAttr);
                                } else {
                                    $valueAttr = array($_attribute->sys_title => '');
                                    $this->saveImageValue($_attribute, $valueAttr, $productId);

                                    // Удаление информации об изображении
                                    $this->delelteImageValueUser($_attribute, $productId);
                                }
                            } else {
                                // Системный атрибут
                                if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                                    $systemData = array_merge($systemData, $value);

                                    // Добавляем информацию об изображении
                                    $imageInfo = Fenix_ImageInfo::getImageInfo($systemData[$_attribute->sys_title]);
                                    $imageInfoKey = $_attribute->sys_title . '_info';

                                    $value[$imageInfoKey] = serialize($imageInfo);

                                    $systemData = array_merge($systemData, $value);
                                } else {
                                    $this->saveImageValue($_attribute, $value, $productId);

                                    // Добавляем информацию об изображении
                                    $imageInfo = Fenix_ImageInfo::getImageInfo($value[$_attribute->sys_title]);
                                    $userData = serialize($imageInfo);

                                    $this->saveImageValueUser($_attribute, $userData, $productId);
                                }
                            }
                        }
                        break;
                    case 'file':
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $value = $this->getPostFileValue($_attribute, $req, $productId,
                                $current->{$_attribute->sys_title});
                        } else {
                            $attrValue = Fenix::getModel('catalog/backend_attributes')->getAttributeValuesInfo($_attribute,
                                $productId);

                            if (sizeof($attrValue) == 0) {
                                $value = $this->getPostFileValue($_attribute, $req, $productId, '');
                            } else {
                                $value = $this->getPostFileValue($_attribute, $req, $productId, $attrValue[0]->content);
                            }
                        }

                        if ($value) {
                            if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                                if ($value == 'delete') {
                                    $value = array($_attribute->sys_title => '');
                                }

                                $systemData = array_merge($systemData, $value);
                            } else {
                                if ($value == 'delete') {
                                    $value = array($_attribute->sys_title => '');
                                }

                                $this->saveFileValue($_attribute, $value, $productId);
                            }
                        }
                        break;
                    case 'textarea':
                        $value = $this->getPostTextareaValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextareaValue($_attribute, $value, $productId);
                        }
                        break;
                    case 'text':
                        $value = $this->getPostTextValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextValue($_attribute, $value, $productId);
                        }
                        break;
                    default:
                        $value = $this->getPostTextValue($_attribute, $req);
                        if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                            $systemData = array_merge($value, $systemData);
                        } else {
                            $this->saveTextValue($_attribute, $value, $productId);
                        }
                        break;
                }
            }
        }

        // Галерея
        $value = $this->getPostGalleryValue($req, $productId);
        $systemData = array_merge($systemData, $value);

        //Сохраняем системные
        $this->setTable('catalog_products');
        $this->update($systemData, $this->getAdapter()->quoteInto('id = ?', $productId));

        // Формируем урл кей
        /*   if (array_key_exists('system_url_key', $systemData)) {
               if ($systemData['system_url_key'] == null) {
                   $systemData['system_url_key'] = $systemData['system_title_' . Fenix_Language::getInstance()->getCurrentLanguage()->name];
               }

               $systemData['system_url_key'] = Fenix::stringProtectUrl($systemData['system_url_key']);
           }

           //Отделяем данные пользовательских атрибутов от системных
           $userString       = Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER . '_';  //Префикс пользовательских атрибутов
           $systemString     = Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM . '_';//Префикс системных атрибутов
           $userAttributes   = array(); //Данные пользовательских атрибутов
           $systemAttributes = array(); //Данные системных атрибутов
           foreach($systemData as $attrName => $value){

               //Распределяем атрибуты по префиксу
               if (strpos($attrName, $userString) === 0) {
                   //Обработка пользовательских атрибутов
                   unset($systemData[$attrName]);
                   $attrName = substr($attrName, strlen($userString));
                   $userAttributes[$attrName] = $value;

               }elseif (strpos($attrName, $systemString) === 0){
                   //Обработка системных атрибутов
                   unset($systemData[$attrName]);
                   $attrName = substr($attrName, strlen($systemString));
                   $systemAttributes[$attrName] = $value;

               }else{
                   //Обработка системных атрибутов без префикса
                   unset($systemData[$attrName]);
                   $systemAttributes[$attrName] = $value;
               }
           }
   */
        //Сохраняем пользовательские
        /*        $this->setTable('catalog_attribute_values');
                $this->update($userAttributes,  $this->getAdapter()->quoteInto('product_id = ?', $productId));
        */
        //Fenix::dump($systemAttributes,$userAttributes);
        //Сохраняем категории товара
        $this->saveCategoriesRelations($productId, $req);


        Fenix::getModel('catalog/backend_options')->updateOptions($productId, $req);

        // Обновляем сопутствующие
        if (Fenix::getStaticConfig()->catalog->modules->related_products == '1') {

            $related = $req->getPost('_list');
            $related = $related['related'];

            $this->setTable('catalog_products_related')
                ->delete('product_id = ' . (int)$current->id);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $current->id,
                    'child_id'   => $_id,
                    'position'   => ($_i + 1)
                ));
            }
        }

        // Обновляем похожие
        if (Fenix::getStaticConfig()->catalog->modules->upsell_products == '1') {

            $related = $req->getPost('_list');
            $related = $related['upsell'];

            $this->setTable('catalog_products_upsell')
                ->delete('product_id = ' . (int)$current->id);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $current->id,
                    'child_id'   => $_id,
                    'position'   => ($_i + 1)
                ));
            }
        }

        // Обновляем наборы
        if (Fenix::getStaticConfig()->catalog->modules->products_set == '1') {

            $related = $req->getPost('_list');
            $related = $related['set'];

            $price = $req->getPost('_price');
            $price = $price['set'];

            $discount = $req->getPost('_persent');
            $discount = $discount['set'];

            $this->setTable('catalog_products_set')
                ->delete('product_id = ' . (int)$current->id);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $current->id,
                    'child_id'   => $_id,
                    'price'      => $price[$_i],
                    'discount'   => $discount[$_i],
                    'position'   => ($_i + 1)
                ));
            }


            $related = $req->getPost('_list');
            $related = $related['set2'];

            $price = $req->getPost('_price');
            $price = $price['set2'];

            $discount = $req->getPost('_persent');
            $discount = $discount['set2'];

            $this->setTable('catalog_products_set2')
                ->delete('product_id = ' . (int)$current->id);

            foreach ((array)$related AS $_i => $_id) {
                $this->insert(array(
                    'product_id' => $current->id,
                    'child_id'   => $_id,
                    'price'      => $price[$_i],
                    'discount'   => $discount[$_i],
                    'position'   => ($_i + 1)
                ));
            }
        }
        // Обновляем мультинаборы
        if (Fenix::getStaticConfig()->catalog->modules->products_multiset == '1') {
            self::updateMultiset($current->id, $req);
        }
        // Обновляем сложный товар
        if (Fenix::getStaticConfig()->catalog->modules->configurable_products == '1') {
            Fenix::getModel('catalog/backend_configurable')->saveConfigurable($productId, $req);
        }

        // Обновляем таблицу слежки за ценой
        $this->setTable('customer_tracking')->update(array(
            'price_new' => $req->getPost('price')
        ), 'product_id = ' . (int)$current->id);

        return $current->id;
    }

    /**
     * @param $current
     * @return true
     */
    public function deleteProduct($current)
    {
        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        /** NEW begin */
        if (is_object($current)) {
            $productId = (int)$current->id;
        } elseif (is_array($current)) {
            $productId = (int)$current['id'];
        } elseif ((is_int($current) || is_string($current)) && (int)$current > 0) {
            $productId = (int)$current;
        } else {
            return false;
        }

        return $this->deleteProducts(array($productId => $productId));

        /*$langList     = Fenix_Language::getInstance()->getLanguagesList();
        $category     = Fenix::getModel('catalog/backend_categories')->getCategoryById($current->parent);
        $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            $category->attributeset_id
        );

        if ($attributeset != null) {
            // Группы набора атрибутов
            $GroupsList = Fenix::getModel('catalog/backend_attributeset')->getGroupsList(
                $attributeset->id
            );
            foreach ($GroupsList AS $_group) {
                $AttributesList = Fenix::getModel('catalog/backend_attributeset')->getGroupAttributesList($_group->id);
                foreach ($AttributesList AS $_attribute) {
                    if ($_attribute->split_by_lang == '1') {
                        switch ($_attribute->type) {
                            case 'text':
                            break;
                            case 'textarea':
                            break;
                            default:
                            break;
                        }
                    }
                    else {
                        switch ($_attribute->type) {
                            case 'text':
                            break;
                            case 'textarea':
                            break;
                            case 'checkbox':
                            break;
                            case 'image':
                            break;
                            case 'gallery':
                                $_gallery = (array) unserialize($current->{$_attribute->sys_title});
                                foreach ($_gallery AS $_image) {
                                    if(trim($_image['image'])!='')
                                    if (file_exists(HOME_DIR_ABSOLUTE . $_image['image'])) {
                                        unlink(HOME_DIR_ABSOLUTE . $_image['image']);
                                    }
                                }
                            break;
                            default:
                            break;
                        }
                    }
                }
            }
        }*/

    }

    public function deleteGarbageProducts()
    {
        /** Выбираем товары у которых нет связей */
        $Select = $this->setTable('catalog_relations')
            ->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'r' => $this->getTable()
        ), 'product_id');
        $correctProductsSelect = $Select->assemble();

        $Select = $this->setTable('catalog_products')
            ->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'p' => $this->getTable()
        ), 'p.id AS id');
        $Select->where('id NOT IN(' . $correctProductsSelect . ')');

        $garbageProducts = $this->fetchAll($Select);
        if ($garbageProducts != null && count($garbageProducts) > 0) {
            $productsIDs = array();
            foreach ($garbageProducts as $_product) {
                $productsIDs[$_product->id] = $_product->id;
            }
        }

        /** Выбираем ID товаров из catalog_relations которых нет в catalog_products */
        $Select = $this->setTable('catalog_products')
            ->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'p' => $this->getTable()
        ), 'id');
        $correctProductsSelect = $Select->assemble();

        $Select = $this->setTable('catalog_relations')
            ->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'r' => $this->getTable()
        ), 'product_id AS id');
        $Select->where('product_id NOT IN(' . $correctProductsSelect . ')');

        $garbageProducts = $this->fetchAll($Select);
        if ($garbageProducts != null && count($garbageProducts) > 0) {
            $productsIDs = array();
            foreach ($garbageProducts as $_product) {
                $productsIDs[$_product->id] = $_product->id;
            }
        }

        if ( ! empty($productsIDs) && count($productsIDs) > 0) {
            $this->deleteProducts($productsIDs);
        }

        return true;
    }

    /**
     * @param $productIDs - массив ID удаляемых товаров
     * @return true
     */
    public function deleteProducts($productIDs = array())
    {
        if (empty($productIDs) || ! is_array($productIDs)) {
            return false;
        }

        $productIDsStr = implode(',', $productIDs);

        // Список товаров - удаляем товар
        $this->setTable('catalog_products');
        $this->delete('id IN(' . $productIDsStr . ')');

        // Мульти наборы - удаляем наборы и связи
        $this->setTable('catalog_products_multiset')
            ->delete('product_id IN(' . $productIDsStr . ')');
        $this->setTable('catalog_products_multiset_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Дополнительные опции к товару - удаляем связи
        $this->setTable('catalog_products_options_relations')
            ->delete('product_id IN(' . $productIDsStr . ')');

//        $this->setTable('catalog_products_sorting')
//            ->delete('product_id IN('.$productIDsStr.')');

        // Сопутствующие товары - удаляем связи
        $this->setTable('catalog_products_related')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Похожие товары - удаляем связи
        $this->setTable('catalog_products_upsell')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Отзывы к товару - удаляем все отзывы товара
        $this->setTable('catalog_product_reviews')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Промоблоки у товара - удаляем связи
        $this->setTable('catalog_promo_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Чистим связи с значениями атрибутов
        $this->setTable('attr_values')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('catalog_attribute_values')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Удаляем связи с материалами
        $this->setTable('catalog_material_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Чистим мультикатегории
        $this->setTable('catalog_relations')
            ->delete('product_id IN(' . $productIDsStr . ')');

        //  Чистим таблицы сложного товара
        $this->setTable('product_conf_attr')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('product_conf_groups')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('product_conf_values')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Удаляем связь товара с статьями
        $this->setTable('articles_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Удаляем комплекты
        $Select = $this->setTable('catalog_set')
            ->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'set' => $this->getTable()
        ), 'id');
        $Select->where('product_id IN(' . $productIDsStr . ')');

        $this->setTable('catalog_set_products')
            ->delete('set_id IN(' . $Select->assemble() . ')');

        $this->setTable('catalog_set_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('catalog_set')
            ->delete('product_id IN(' . $productIDsStr . ')');


        // Удаляем связь товара из списка желаний пользователей
        $this->setTable('customer_favorites')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Слежка за ценой
        $this->setTable('customer_tracking')
            ->delete('product_id IN(' . $productIDsStr . ')');

        // Скидки
        $this->setTable('sale_discount')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('sale_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('sale_products_from')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->setTable('sale_sticker_products')
            ->delete('product_id IN(' . $productIDsStr . ')');

        $this->afterDeleteProducts($productIDs);


        return true;
    }

    /** Действия после массового удаления товаров
     *  - удаление из корзины (неоформленные)
     *  - удаление папки с изображениями товара
     *
     * @param $productIDs - массив ID удаляемых товаров
     *
     * @return true
     */
    public function afterDeleteProducts($productIDs = array())
    {
        if (empty($productIDs) || ! is_array($productIDs)) {
            return false;
        }

        $productIDsStr = implode(',', $productIDs);


        /** Удаляем папку с картинками */
        foreach ($productIDs as $_id) {
            $images_path = HOME_DIR_ABSOLUTE . 'catalog/product' . $_id . '/';
            if (is_dir($images_path)) {
                Fenix::removeDirectory($images_path);
            }
        }

        /** Удаление товара из еще не оформленных заказов */
        $this->setTable('checkout_orders');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'co' => $this->getTable(),
        ), array(
            'co.id',
        ));
        $Select->join(array(
            'cp' => $this->getTable('checkout_products'),
        ), 'cp.order_id = co.id', null);
        $Select->where($this->getAdapter()->quoteInto('cp.product_id IN(?)',
                $productIDs) . ' OR ' . $this->getAdapter()->quoteInto('cp.parent_product IN(?)', $productIDs));
        $Select->where('co.abandoned = ?', '1');

        $orders = $this->fetchAll($Select);
        $ordersIDs = array();

        if ($orders != null) {
            foreach ($orders as $_order) {
                $ordersIDs[] = $_order->id;
            }

            if ( ! empty($ordersIDs)) {
                $this->setTable('checkout_products')
                    ->delete('(' . $this->getAdapter()->quoteInto('product_id IN(?)',
                            $productIDs) . ' OR ' . $this->getAdapter()->quoteInto('parent_product IN(?)',
                            $productIDs) . ')' . ' AND ' . $this->getAdapter()->quoteInto('order_id IN (?)',
                            $ordersIDs));

                foreach ($ordersIDs as $_id) {
                    Fenix::getModel('checkout/process')->updateOrderTotal($_id);
                }
            }
        }

        return true;
    }

    public function clearProducts($parent)
    {
        if (is_object($parent)) {
            $parentId = (int)$parent->id;
        } elseif (is_array($parent)) {
            $parentId = (int)$parent['id'];
        } elseif ((is_int($parent) || is_string($parent)) && (int)$parent > 0) {
            $parentId = (int)$parent;
        } else {
            return false;
        }

        $productsIDs = array();
        $productsStep = 100; // удаляем пачкой по 100 товаров
        $msg = '<hr>';

        $category = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($category != null) {
            $msg .= '<br>работаем категорией ' . $category->id;

            $productsSelect = $this->getProductsListAsSelect($category);
            $productsSelect->limit($productsStep);

            $products = $this->fetchAll($productsSelect);

            if ($products != null && count($products) > 0) {
                foreach ($products as $_product) {
                    $productsIDs[$_product->id] = $_product->id;
                }
            }

            /** Есть товары в категории? - удаляем */
            if ( ! empty($productsIDs) && count($productsIDs) > 0) {
                $msg .= '<br>удаление товаров ' . count($productsIDs);
                Fenix::getModel('catalog/backend_products')->deleteProducts($productsIDs);

                $msg = Fenix::isDev() ? $msg : '';

                print '<meta http-equiv="Refresh" content="0">';
                print 'Подождите...';
                print $msg;
                exit;
            }

        } else {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Категрия не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        return true;
    }

    /**
     * @param $product
     * @param null $in_stock
     * @return Zend_Db_Table_Rowset|Zend_Db_Table_Rowset_Abstract
     */
    public function getMultiSetList($product, $in_stock = null)
    {
        if ($product === false) {
            return new Zend_Db_Table_Rowset(array(
                'data' => array()
            ));
        }

        $this->setTable('catalog_products_multiset');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), array(
            '_p.id AS id',
            '_p.price AS set_price',
            '_p.discount AS set_discount',
            '_p.in_stock'
        ));

        /* // Джоиним товарчеги
         $Select ->join(array(
             'p' => $this->getTable('catalog_products')
         ), '_p.child_id = p.id', $this->productColumns());*/

        $Select->where('_p.product_id = ?', (int)$product->id);

        if ($in_stock) {
            $Select->where('_p.in_stock = ?', (string)$in_stock);
        }

        $Select->order('_p.position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getMultiSetById($id)
    {

        $this->setTable('catalog_products_multiset');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), array(
            '_p.id AS id',
            '_p.price AS set_price',
            '_p.discount AS set_discount'
        ));

        $Select->where('_p.id = ?', (int)$id);

        return $this->fetchRow($Select);
    }

    /**
     * @param $multisetId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getMultiSetProductsList($multisetId)
    {

        $this->setTable('catalog_products_multiset_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.multiset = ?', (int)$multisetId);

        return $this->fetchAll($Select);
    }

    /**
     * @param $multisetId
     * @return bool
     */
    public function checkMultiSet($multisetId)
    {
        //Собираем список артикулов из товаров комплекта
        $list = self::getMultiSetProductsList($multisetId);
        $skuList = array();
        foreach ($list as $_product) {
            $skuList[] = $_product->sku;
        }

        //Ищем товары у которых количество 0
        $this->setTable('catalog_stores_products');
        $Select = $this->select();
        $Select->from($this->_name, array(
            'sku',
            'SUM(qty) as sumqty'
        ));

        $Select->where('sku  IN ("' . implode('","', $skuList) . '")');
        $Select->having('sumqty = 0');
        $Select->group("sku");
        $productsList = $this->fetchAll($Select);

        if ($productsList->count() > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Сохраняем мультикомплекты
     * @param $current
     * @param $req
     */
    public function updateMultiset($currentProductId, $req)
    {

        $multisetList = $req->getPost('multiset_list_products');

        $price = $req->getPost('_price');
        $price = $price['multiset'];

        $discount = $req->getPost('_persent');
        $discount = $discount['multiset'];

        //Удаляем старые
        $this->setTable('catalog_products_multiset')
            ->delete('product_id = ' . (int)$currentProductId);
        $this->setTable('catalog_products_multiset_products')
            ->delete('product_id = ' . (int)$currentProductId);
        $multisetId = array();

        $_i = 0;
        foreach ((array)$multisetList AS $multisetProducts) {

            $this->setTable('catalog_products_multiset');
            $multisetId[$_i] = $this->insert(array(
                'product_id' => $currentProductId,
                'price'      => $price[$_i],
                //'discount'   => $discount[$_i],
                'discount'   => 0,
                'position'   => ($_i + 1)
            ));
            foreach ($multisetProducts as $_j => $product_id) {
                $this->setTable('catalog_products_multiset_products');
                $this->insert(array(
                    'multiset'   => $multisetId[$_i],
                    'product_id' => $currentProductId,
                    'child_id'   => $product_id
                ));
            }
            $_i++;
        }

    }

    /**
     * Обновляем связи товаров
     */
    public function updateProductsRelations()
    {
        // Удаляем несуществующие связи в catalog_relations
        $SelectCorrect = $this->setTable('catalog_relations')
            ->select();

        $SelectCorrect->from(array('_r' => $this->getTable()), '_r.id');
        $SelectCorrect->join(array(
            'c' => $this->getTable('catalog')
        ), 'c.id = _r.parent', null);

        $currentIds = array();
        $result = $this->fetchAll($SelectCorrect);
        if ($result != null && $result->count() > 0) {
            foreach ($result as $_item) {
                $currentIds[] = $_item->id;
            }
        }

        // влепить со вложенным запросом не получилось, зенд кидает ошибки (((((
        if ( ! empty($currentIds)) {
            $this->setTable('catalog_relations')->delete($this->getAdapter()->quoteInto('id NOT IN (?)', $currentIds));
        } else {
            $this->setTable('catalog_relations')->delete('id > 0');
        }


        // Поиск товаров привязанных к несуществующим категориям в catalog_products
        $productIDs = array();

        $SelectCorrect = $this->setTable('catalog_products')
            ->select();

        $SelectCorrect->from(array('_p' => $this->_name), '_p.id');
        $SelectCorrect->join(array(
            'c' => $this->getTable('catalog')
        ), 'c.id = _p.parent', null);

        $SelectBadRecords = $this->setTable('catalog_products')
            ->select();
        $SelectBadRecords->from(array('p' => $this->_name), 'p.id as product_id');
        $SelectBadRecords->where('p.id NOT IN(?)', new Zend_Db_Expr($SelectCorrect->assemble()));

        $resultProducts = $this->fetchAll($SelectBadRecords);
        if ($resultProducts->count() > 0) {
            foreach ($resultProducts as $_product) {
                $productIDs[] = $_product->product_id;
            }
        }
        if ( ! empty($productIDs)) {
            foreach ($productIDs as $_productId) {
                $Select = $this->setTable('catalog_relations')
                    ->select();
                $Select->from($this->getTable(), 'parent');
                $Select->where($this->getAdapter()->quoteInto('product_id = ?', $_productId));
                $row = $this->fetchRow($Select);

                if ($row != null && $row->parent != null) {
                    $this->setTable('catalog_products')
                        ->update(array(
                            'parent' => $row->parent,
                        ), $this->getAdapter()->quoteInto('id = ?', $_productId));
                } else {
                    $this->setTable('catalog_products')
                        ->update(array(
                            'parent' => Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT,
                        ), $this->getAdapter()->quoteInto('id = ?', $_productId));

                    $this->setTable('catalog_relations')
                        ->insert(array(
                            'product_id' => $_productId,
                            'parent'     => Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT,
                        ));
                }
            }
        }


        // Обновляем left и right для привязок товаров к категориям
        Fenix::getModel('core/common')->updateTreeIndex('catalog');

        $this->setTable('catalog');
        $Select = $this->select()->from($this->_name)->where('id > 0');

        $categoriesList = $this->fetchAll($Select);

        foreach ($categoriesList as $category) {
            $data = array(
                'left'  => $category->left,
                'right' => $category->right,
            );

            $this->setTable('catalog_relations')
                ->update($data, $this->getAdapter()->quoteInto('parent = ?', $category->id));
        }
    }

    /**
     * Список категорий по родителю
     *
     * @param int $parent Идентификатор родительской категории
     * @return Zend_Db_Table_Rowset
     */
    public function getCategoriesRelations($product)
    {
        $productId = ($product != null ? $product->id : 0);
        $cacheId = 'getCategoriesRelations_' . $productId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getColumns());

        // Джоиним связи
        $Select->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'fe_catalog.id = r.parent AND r.product_id = ' . $productId, null);

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * @param $currentProductId
     * @param $req
     */
    public function saveCategoriesRelations($currentProductId, $req)
    {

        // Удаляем старые
        $this->setTable('catalog_relations')
            ->delete($this->getAdapter()->quoteInto('product_id = ?', $currentProductId));

        // Обновляем категории

        //Проверяем выбранные категории
        $categories = $req->getPost('categories');
        if ($categories == null || ! is_array($categories) || count($categories) == 0) {
            // Если нет выбраной категории ставим как родительскую категорию(нет родителя)
            $categories = array(
                Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT
            );
        }

        //Добавляем новые
        foreach ($categories as $i => $categoryId) {
            $_category = Fenix::getModel('catalog/backend_categories')->getCategoryById($categoryId);
            if ($_category) {
                $this->setTable('catalog_relations');
                $this->insert(array(
                    'parent'     => $categoryId,
                    'product_id' => $currentProductId,
                    'left'       => $_category->left,
                    'right'      => $_category->right
                ));
            }

            if ($i == 0) { // Ставим первую категорию как родительскую
                $data = array('parent' => $categoryId);
                $this->setTable('catalog_products');
                $this->update($data, $this->getAdapter()->quoteInto('id = ?', $currentProductId));
            }
        }
    }


    /**
     * @param Zend_Db_Table_Row $attribute
     * @param array $value
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getAttributeValue(Zend_Db_Table_Row $attribute, array $value)
    {

        $cacheId = 'backend_getAttributeValue_' . $attribute->id . md5(serialize($value));
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $prefix = 'avc';
        $Select = $this->setTable($tableName)
            ->select()->setIntegrityCheck(false);
        $Select->from(array($prefix => $this->_name), $Engine->getColumns(array('prefix' => $prefix)));

        $column = 'content';

        if ($Engine->columnIsSplitByLang($column) && $attribute->split_by_lang == '1') {
            foreach (Fenix_Language::getInstance()->getLanguagesList() as $lang) {
                $_value = isset($value[$attribute->sys_title . '_' . $lang->name]) ? $value[$attribute->sys_title . '_' . $lang->name] : '';

                $Select->where($prefix . '.' . $column . '_' . $lang->name . ' = ?', $_value);
            }
        } else {
            $_value = isset($value[$attribute->sys_title]) ? $value[$attribute->sys_title] : '';

            $Select->where($prefix . '.' . $column . ' = ?', $_value);
        }

        $Select->where($prefix . '.attribute_id = ?', $attribute->id);
        $Select->limit(1);
        try {
            $Result = $this->fetchRow($Select);
        } catch (Exception $e) {
            Fenix::dump($Select->assemble(), $value, $attribute, $e);
        }

        if ($Result !== null) {
            Zend_Registry::set($cacheId, $Result);
        }

        return $Result;
    }

    /**
     * @param Zend_Db_Table_Row $attribute
     * @param array $value
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getProductAttributeValue(Zend_Db_Table_Row $attribute, $productId = null)
    {

        $cacheId = 'backend_getAttributeValue_' . $attribute->id . '_' . $productId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
//        Fenix::dump($tableName);
        $Engine->setSource('catalog/' . $tableName);


        $Select = $this->setTable('attr_values')
            ->select()->setIntegrityCheck(false);

        $Select->from(array('avr' => $this->_name), null);

        $prefix = 'avc';
        $Select->join(array(
            $prefix => $this->getTable($tableName)
        ), 'avr.value_id = ' . $prefix . '.id AND ' .
            $this->getAdapter()->quoteInto('avr.product_id = ?', $productId),
            $Engine->getColumns(array('prefix' => $prefix))
        );
        $Select->where('avr.attribute_id = ?', $attribute->id);
        $Select->limit(1);

        $Select->group('avr.value_id');
        $Result = $this->fetchRow($Select);
        /*if($attribute->id == 297)
            Fenix::dump($Select->assemble(),$Result);*/
        $Result = $this->fetchRow($Select);
        if ($Result) {
            $Result = $Result->toArray();
        } else {
            $Result = array();
        }

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * @param Zend_Db_Table_Row $attribute
     * @param array $value
     *
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getProductAttributeValues(Zend_Db_Table_Row $attribute, $productId = null)
    {

        $cacheId = 'backend_getAttributeValues_' . $attribute->id . '_' . $productId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);


        $Select = $this->setTable('attr_values')
            ->select()->setIntegrityCheck(false);

        $Select->from(array('avr' => $this->_name), null);

        $prefix = 'avc';
        $Select->join(array(
            $prefix => $this->getTable($tableName),
        ), 'avr.value_id = ' . $prefix . '.id AND ' .
            $this->getAdapter()->quoteInto('avr.product_id = ?', $productId) . ' AND ' .
            $this->getAdapter()->quoteInto('avc.attribute_id = ?', $attribute->id),
            $Engine->getColumns(array('prefix' => $prefix))
        );
        $Select->where('avr.attribute_id = ?', $attribute->id);

        $Select->group('avr.value_id');
        //$Result = $this->fetchRow($Select);
        /*if($attribute->id == 297)
			Fenix::dump($Select->assemble(),$Result);*/
        $Result = $this->fetchAll($Select);
        if ($Result != null) {
            $Result = $Result->toArray();
            Zend_Registry::set($cacheId, $Result);
        } else {
            $Result = array();
        }


        return $Result;
    }

    /**
     * @param Zend_Db_Table_Row $attribute
     * @param array $value
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getAttributeValueById(Zend_Db_Table_Row $attribute, $valueId)
    {

        $cacheId = 'backend_getAttributeValueById_' . $attribute->id . '_' . $valueId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);


        $Select = $this->setTable($tableName)
            ->select()->setIntegrityCheck(false);

        $Select->where('id = ?', $valueId);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * @param Zend_Db_Table_Row $attribute
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getAttributeValues(Zend_Db_Table_Row $attribute)
    {

        $cacheId = 'backend_getAttributeValues_' . $attribute->id;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';
        }
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $Select = $this->setTable($tableName)
            ->select();

        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('attribute_id = ?', $attribute->id);
        $Select->order('content asc');
        $Result = $this->fetchAll($Select);
        //Fenix::dump($Result);
        /*
         try{
             }catch (Exception $e){
            Fenix::dump($tableName, $Engine->columnIsSplitByLang($column), $value,$attribute->sys_title,$Select->assemble(),$e->getMessage());
        }*/
        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * @param Zend_Db_Table_Row $attribute
     * @param $value
     * @param null $lang
     * @return mixed|string
     */
    public function saveAttributeValue(Zend_Db_Table_Row $attribute, array $value)
    {
        $attrValue = $this->getAttributeValue($attribute, $value);

        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

        if ($attrValue) {
            $attrData = array();
            if (isset($value['url'])) {
                $attrData['url'] = $value['url'];
            } elseif ($attrValue->url == null) {
                if ($attribute->split_by_lang == '1') {
                    $def_lang = Fenix_Language::getInstance()->getDefaultLanguage();
                    $attrData['url'] = Fenix::stringProtectUrl($value[$attribute->sys_title . '_' . $def_lang->name]);
                } else {
                    $attrData['url'] = Fenix::stringProtectUrl($value[$attribute->sys_title]);
                }
            }

            if (isset($value['type'])) {
                $attrData['type'] = $value['type'];
            }

            if (isset($value['property'])) {
                $attrData['property'] = $value['property'];
            }

            if (count($attrData) > 0) {
                $this->setTable($tableName)->update($attrData,
                    $this->getAdapter()->quoteInto('id = ?', $attrValue->id));
            }

            return $attrValue->id;
        } else {

            $attrData = array(
                'attribute_id' => $attribute->id
            );
            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/' . $tableName);

            $column = 'content';
            if ($Engine->columnIsSplitByLang($column) && $attribute->split_by_lang == '1') {
                foreach (Fenix_Language::getInstance()->getLanguagesList() as $lang) {
                    $attrData[$column . '_' . $lang->name] = $value[$attribute->sys_title . '_' . $lang->name] ? $value[$attribute->sys_title . '_' . $lang->name] : '';
                }
            } else {
                $attrData[$column] = $value[$attribute->sys_title] ? $value[$attribute->sys_title] : '';
            }

            if (isset($value['url'])) {
                $attrData['url'] = $value['url'];
            } else {
                if ($attribute->split_by_lang == '1') {
                    $def_lang = Fenix_Language::getInstance()->getDefaultLanguage();
                    $attrData['url'] = Fenix::stringProtectUrl($value[$attribute->sys_title . '_' . $def_lang->name]);
                } else {
                    $attrData['url'] = Fenix::stringProtectUrl($value[$attribute->sys_title]);
                }
            }

            if (isset($value['type'])) {
                $attrData['type'] = $value['type'];
            }

            if (isset($value['property'])) {
                $attrData['property'] = $value['property'];
            }

            $id = $this->setTable($tableName)->insert($attrData);

            return $id;
        }
    }

    /**
     * Получаем данные text из пост запроса
     * @param $_attribute
     * @param $req
     * @return array
     */
    private function getPostTextValue($_attribute, $req)
    {
        $data = array();
        if ($_attribute->split_by_lang == '1') {
            $langList = Fenix_Language::getInstance()->getLanguagesList();
            foreach ($langList AS $_langName => $_langOptions) {
                $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title . '_' . $_langName;
                $colName = $_attribute->sys_title . '_' . $_langName;
                $data[$colName] = stripslashes($req->getPost($fieldName));
            }
        } else {
            $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title;
            $colName = $_attribute->sys_title;
            $data[$colName] = stripslashes($req->getPost($fieldName));
        }

        return $data;
    }

    /**
     * Получаем данные textarea из пост запроса
     * @param $_attribute
     * @param $req
     * @return array
     */
    private function getPostTextareaValue($_attribute, $req)
    {
        $data = array();
        if ($_attribute->split_by_lang == '1') {
            $langList = Fenix_Language::getInstance()->getLanguagesList();
            foreach ($langList AS $_langName => $_langOptions) {
                $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title . '_' . $_langName;
                $colName = $_attribute->sys_title . '_' . $_langName;
                $data[$colName] = stripslashes($req->getPost($fieldName));
            }
        } else {
            $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title;
            $colName = $_attribute->sys_title;
            $data[$colName] = stripslashes($req->getPost($fieldName));
        }

        return $data;
    }

    /**
     * Получаем данные checkbox из пост запроса
     * @param $_attribute
     * @param $req
     * @return array
     */
    private function getPostCheckboxValue($_attribute, $req)
    {
        $data = array();

        $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title;
        $colName = $_attribute->sys_title;

        $data[$colName] = ($req->getPost($fieldName) == null ? '0' : '1');

        return $data;
    }

    /**
     * Получаем путь к файлу
     * @param $_attribute
     * @param $req
     * @param $productId
     * @return mixed
     */
    private function getPostFileValue($_attribute, $req, $productId, $file = null)
    {
        $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title;
        $colName = $_attribute->sys_title;
        $data = array();
        $fileInfo = $req->getFiles($fieldName);

        // Удаление изображения
        if ($req->getPost($_attribute->attribute_source . '_' . $_attribute->sys_title . '_delete') == '1' && $file != null) {
            if (file_exists(HOME_DIR_ABSOLUTE . $file)) {
                unlink(HOME_DIR_ABSOLUTE . $file);
            }

            $remove = 1;
        } elseif ($fileInfo->tmp_name != null) {
            $fileInfo->name = Fenix::TranslitFileName($fileInfo);
            if ( ! is_dir(HOME_DIR_ABSOLUTE . 'catalog/product' . $productId . '/')) {
                mkdir(HOME_DIR_ABSOLUTE . 'catalog/product' . $productId . '/', 0777, true);
            }

            if (file_exists(HOME_DIR_ABSOLUTE . $file)) {
                unlink(HOME_DIR_ABSOLUTE . $file);
            }

            copy($fileInfo->tmp_name,
                HOME_DIR_ABSOLUTE . 'catalog/product' . $productId . '/' . $colName . '_' . $fileInfo->name);
            $file = 'catalog/product' . $productId . '/' . $colName . '_' . $fileInfo->name;
        }
        if ($file) {
            $data[$colName] = $file;
        }

        if (isset($remove)) {
            $data = 'delete';
        }


        return $data;
    }

    /**
     * Получаем путь к изображению
     * @param $_attribute
     * @param $req
     * @param $productId
     * @return mixed
     */
    private function getPostImageValue($_attribute, $req, $productId, $file = null)
    {
        $data = $this->getPostFileValue($_attribute, $req, $productId, $file);

        return $data;
    }


    private function getPostGalleryValue($req, $productId)
    {
        $data = array();

        if ($req->getPost('gallery')) {
            $gallery = Fenix::getModel('core/common')->uploadImages($req->getPost('gallery'),
                'catalog/product' . $productId . '/', null);
        } else {
            $gallery = array();
        }
        $data['gallery'] = serialize($gallery);

        return $data;
    }


    /**
     * Получаем запись связи между товаром и значением атрибута
     * @param $attributeId
     * @param $productId
     * @return null|Zend_Db_Table_Row_Abstract
     */
    private function getProductToValueRelation($attributeId, $productId, $valueId = null)
    {

        $Select = $this->setTable('attr_values')->select()
            ->from($this->_name)
            ->where('attribute_id = ?', $attributeId)
            ->where('product_id = ?', $productId);

        if ($valueId) {
            $Select->where('value_id = ?', $valueId);
        }

        $Select->limit(1);
        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Cохраняем значение атрибута для товара, только одно значение связано с товаром
     * @param $attribute
     * @param $value
     * @param $productId
     * @return mixed|string
     */
    private function saveOneValue($attribute, $value, $productId)
    {
        $valueId = $this->saveAttributeValue($attribute, $value);
        $data = array(
            'attribute_id' => $attribute->id,
            'product_id'   => $productId,
            'value_id'     => $valueId
        );

        $test = $this->getProductToValueRelation($attribute->id, $productId);

        if ($test) {
            //Удаляем другие значения, если существуют
            $this->setTable('attr_values')
                ->delete(
                    $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                    $this->getAdapter()->quoteInto('product_id = ?', $productId) . ' AND ' .
                    $this->getAdapter()->quoteInto('id <> ?', $test->id)
                );

            //Сохраняем текущее
            $this->setTable('attr_values')
                ->update($data, $this->getAdapter()->quoteInto('id = ?', $test->id));

            return $test->id;
        } else {
            //Сохраняем новое
            $id = $this->setTable('attr_values')->insert($data);

            return $id;
        }
    }

    /**
     * Cохраняем значения атрибута для товара, несколько значений связано с товаром
     *
     * @param $attribute
     * @param $value
     * @param $productId
     *
     * @return mixed|string
     */
    private function saveMultipleValues($attribute, $value, $productId)
    {
        //Удаляем старые связи
        $this->setTable('attr_values')
            ->delete(
                $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                $this->getAdapter()->quoteInto('product_id = ?', $productId)
            );

        /** формируем корректный массив для сохраниения значений чтоб понимал многоязычность (пока не на чем было проверить) */
        $update_values = array();
        foreach ($value as $sys_title => $values) {
            $_values = explode(';', $values);
            foreach ($_values as $k => $value) {
                $update_values[$k][$sys_title] = trim($value);
                if ( ! isset($update_values[$k]['url'])) {
                    $update_values[$k]['url'] = Fenix::stringProtectUrl($value);
                }
            }
        }

        /**
         *Обновляем значения атрибутов товара
         */
        if ( ! empty($update_values)) {
            foreach ($update_values as $value) {
                $valueId = $this->saveAttributeValue($attribute, $value);

                $data = array();
                $data['attribute_id'] = $attribute->id;
                $data['product_id'] = $productId;
                $data['value_id'] = $valueId;

                $this->setTable('attr_values');
                $this->insert($data);
            }

            return false;
        }

        return true;
    }

    /**
     * Сохраняем значение атрибута text для товара
     *
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveTextValue($attribute, $value, $productId)
    {
        if ($attribute->is_multiple) {
            $this->saveMultipleValues($attribute, $value, $productId);
        } else {
            $this->saveOneValue($attribute, $value, $productId);
        }
    }

    /**
     * Сохраняем значение атрибута textarea для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveTextareaValue($attribute, $value, $productId)
    {
        $this->saveOneValue($attribute, $value, $productId);
    }

    /**
     * Сохраняем значение атрибута file для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveFileValue($attribute, $value, $productId)
    {
        $this->saveOneValue($attribute, $value, $productId);
    }

    /**
     * Сохраняем значение атрибута image для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveImageValue($attribute, $value, $productId)
    {
        $this->saveOneValue($attribute, $value, $productId);
    }

    /**
     * Сохраняем значение атрибута image для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveImageValueUser($attribute, $value, $productId)
    {
        $data = array(
            'attribute_id' => (int)$attribute->id,
            'product_id'   => (int)$productId,
            'content'      => $value
        );

        $Select = $this->setTable('attr_image_info')->select()
            ->from($this->_name)
            ->where('attribute_id = ?', (int)$attribute->id)
            ->where('product_id = ?', (int)$productId);

        $Select->limit(1);
        $Result = $this->fetchRow($Select);

        if ($Result) {
            // Удаляем старую запись
            $this->setTable('attr_image_info')
                ->delete(
                    $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                    $this->getAdapter()->quoteInto('product_id = ?', $productId)
                );
        }

        //Сохраняем новое изображение
        $id = $this->setTable('attr_image_info')
            ->insert($data);

        return $id;
    }

    /**
     * Удаляем значение атрибута image для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function delelteImageValueUser($attribute, $productId)
    {
        $Select = $this->setTable('attr_image_info')->select()
            ->from($this->_name)
            ->where('attribute_id = ?', $attribute->id)
            ->where('product_id = ?', $productId);

        $Select->limit(1);
        $Result = $this->fetchRow($Select);

        if ($Result) {
            $this->setTable('attr_image_info')
                ->delete(
                    $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                    $this->getAdapter()->quoteInto('product_id = ?', $productId)
                );

            return $Result->id;
        }
    }

    /**
     * Сохраняем значение атрибута checkbox для товара
     * @param $attribute
     * @param $value
     * @param $productId
     */
    private function saveCheckboxValue($attribute, $value, $productId)
    {
        $this->saveOneValue($attribute, $value, $productId);
    }


    /**
     * Обновляем описание для товаров в группе, находим товар с описанием и копируем во все товары группы где нет описаний
     * @param $parent
     */
    public function updateDescriptionsProducts($parent)
    {
        $category = Fenix::getCollection('catalog/categories')->getCategoryById($parent);
        $Select = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect($category);
        $Select->reset(Zend_Db_Select::COLUMNS);
        $Select->columns(array('p.group_id', 'p.short_details', 'p.details_russian'));
        $Select->where('p.group_id <> ?', 0);
        $Select->where('p.description_is_main = ?', '1');
        $products = $this->fetchAll($Select);

        foreach ($products->toArray() as $i => $product):

            $data['short_details'] = $product['short_details'];
            $data['details_russian'] = $product['details_russian'];
            $this->setTable('catalog_products');
            $this->update(
                $data,
                $this->getAdapter()->quoteInto('group_id = ?', $product['group_id'])
                . ' AND ' .
                $this->getAdapter()->quoteInto('description_is_lock = ?', '0')
                . ' AND ' .
                '(details_russian IS NULL OR details_russian ="")'
            );

        endforeach;

        return $i++;
    }

    /**
     * Обновляем описание для товаров в группе, находим товар с описанием и копируем во все товары группы где нет описаний
     * @param $parent
     */
    public function updateAllDescriptionsProducts($parent)
    {

        $category = Fenix::getCollection('catalog/categories')->getCategoryById($parent);
        $Select = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect($category);
        $Select->reset(Zend_Db_Select::COLUMNS);
        $Select->columns(array('p.group_id', 'p.short_details', 'p.details_russian'));
        $Select->where('p.group_id <> ?', 0);
        $Select->where('p.description_is_main = ?', '1');
        $products = $this->fetchAll($Select);


        foreach ($products->toArray() as $i => $product):
            $data['short_details'] = $product['short_details'];
            $data['details_russian'] = $product['details_russian'];
            $this->setTable('catalog_products');
            $this->update(
                $data,
                $this->getAdapter()->quoteInto('group_id = ?', $product['group_id'])
                . ' AND ' .
                $this->getAdapter()->quoteInto('description_is_lock = ?', '0')
            );
        endforeach;

        return $products->count();
    }
}