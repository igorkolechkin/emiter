<?php
class Fenix_Catalog_Model_Backend_Set extends Fenix_Resource_Model
{

    public function getSetListForTableAsSelect($parentCategory = null)
    {
        $this->setTable('catalog_relations');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ));

        $Select->join(array(
            'set_p' => $this->getTable('catalog_set_products')
        ), 'r.product_id = set_p.product_id', array());

        $Select->join(array(
            'set' => $this->getTable('catalog_set')
        ), 'set_p.set_id= set.id', Fenix_Engine::getInstance()->setSource('catalog/set')->getColumns());

        if($parentCategory){
            $Select ->where('r.left >= ?', (int)$parentCategory->left)
                    ->where('r.right <= ?', (int)$parentCategory->right);
        }
        $Select->group('set.id');

        return $Select;
    }


    public function getSetListAsSelect($parent = null)
    {
        $this->setTable('catalog_set');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'cs' => $this->_name
        ));

        return $Select;
    }

    public function getSetById($setId)
    {

        $Select = $this->getSetListAsSelect();
        $Select ->where('cs.id = ?', $setId);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function addSet($Form, $req)
    {
        $req->setPost('create_date', date('Y-m-d H:i:s'));

        $setId  = $Form->addRecord($req);

        $products = $req->getPost('_list');

        $this->setTable('catalog_set_products');

        foreach ((array) $products['setProducts'] AS $_i => $_id) {
            $data = array(
                'set_id'     => $setId,
                'product_id' => $_id,
                'position'   => ($_i + 1)
            );
            $this->insert($data);
        }
        return $setId;
    }

    public function editSet($Form, $current, $req)
    {
        $req->setPost('modify_date', date('Y-m-d H:i:s'));

        $Form->editRecord($current, $req);

        $setId = $current->id;

        $products = $req->getPost('_list');

        $this->setTable('catalog_set_products');
        $this->delete($this->getAdapter()->quoteInto('set_id = ?', $setId));

        $this->setTable('catalog_set_products');
        foreach ((array) $products['setProducts'] AS $_i => $_id) {
            $data = array(
                'set_id'     => $setId,
                'product_id' => $_id,
                'set_price' => $products['set_price'][$_i],
                'set_discount' => $products['set_discount'][$_i],
                'position'   => ($_i + 1)
            );
            $this->insert($data);
        }
        return $setId;
    }

    public function deleteSet($Form, $current)
    {
        $setId  = $current->id;
        $Form->deleteRecord($current);

        $this->setTable('catalog_set_products');
        $this->delete($this->getAdapter()->quoteInto('set_id = ?', $setId));
    }

    public function getProductsListBySetId($blockId)
    {
        $this->setTable('catalog_set_products');

        $category = Fenix::getModel('catalog/backend_categories')->getCategoryById(1);
        $Select = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect($category);

        $Select->join(array(
            'set_p' => $this->getTable('catalog_set_products')
        ), 'set_p.product_id = p.id', array(
            'set_price',
            'set_discount'
        ));

        $Select ->where('set_p.set_id = ?', (int) $blockId);
        $Select ->order('set_p.position asc');

        return $this->fetchAll($Select);
    }

    public function getProductsBrowser($setId)
    {
        return Fenix::getHelper('catalog/backend_browser')->getSetFilter(array(
            'name'      => 'setProducts',
            'url'       => Fenix::getUrl('catalog/products/find'),
            'products'  => Fenix::getModel('catalog/backend_set')->getProductsListBySetId($setId)
        ));
    }

}