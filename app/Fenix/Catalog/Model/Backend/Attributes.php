<?php

class Fenix_Catalog_Model_Backend_Attributes extends Fenix_Resource_Model
{
    const ATTRIBUTE_SOURCE_SYSTEM = 'system';
    const ATTRIBUTE_SOURCE_USER   = 'user';


    public function getSqlDataTypeList()
    {
        return array(
            'varchar',
            'int',
            'tinyint',
            'double',
            'text',
            'longtext',
            'enum',
        );
    }


    /**
     *
     * Получение стандартных SQL параметров для атрибута по типу поля
     *
     *
     * @param $attribute_type
     * @throws Exception
     */
    public function getSqlDataByType($attribute_type)
    {
        $sql_data = array(
            'text'     => array(
                'sql_type'   => 'varchar',
                'sql_lenght' => '255'
            ),
            'textarea' => array(
                'sql_type'   => 'text',
                'sql_lenght' => ''
            ),
            'select'   => array(
                'sql_type'   => 'text',
                'sql_lenght' => ''
            ),
            'checkbox' => array(
                'sql_type'   => 'enum',
                'sql_lenght' => "['0', '1']"
            ),
            'image'    => array(
                'sql_type'   => 'varchar',
                'sql_lenght' => '255'
            ),
            'file'     => array(
                'sql_type'   => 'varchar',
                'sql_lenght' => '255'
            ),
            'gallery'  => array(
                'sql_type'   => 'text',
                'sql_lenght' => ''
            ),
        );

        if ( ! isset($sql_data[$attribute_type])) {
            throw new Exception('Тип данных для поля не найден, проверьте XML и getDataTypes в файле: ' . __FILE__);
        }

        return $sql_data[$attribute_type];
    }

    /**
     * Атрибут по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('id = ?', (int) $id);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Атрибут по системному имени
     *
     * @param $sys_title
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeBySysTitle($sys_title)
    {
        if (Zend_Registry::isRegistered('FenAttribute_' . $sys_title)) {
            return Zend_Registry::get('FenAttribute_' . $sys_title);
        }


        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('sys_title = ?', $sys_title);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set('FenAttribute_' . $sys_title, $Result);

        return $Result;
    }

    public function getAttributesListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns());

        $Select->order('a.title_russian asc');

        return $Select;
    }

    public function getAttributesList()
    {
        $Select = $this->getAttributesListAsSelect();
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getActiveAttributesList()
    {
        $Select = $this->getAttributesListAsSelect();
        $Select->where('a.is_active = ?', '1');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getActiveAttributesListForProductForm()
    {
        $Select = $this->getAttributesListAsSelect();
        $Select->where('a.is_active = ?', '1');

        $Result = $this->fetchAll($Select);
        $data = array();
        foreach ($Result as $attr) {
            $data[$attr->sys_title] = $attr;
        }

        return $data;
    }


    public function getConfigurableAttributesList($attributesetId = null)
    {
        $Select = $this->getAttributesListAsSelect();

        if ($attributesetId) {
            $Select->join(array(
                'g' => $this->getTable('catalog_attributeset_groups_attributes')
            ), 'a.id = g.attribute_id AND ' . $this->getAdapter()->quoteInto('attributeset_id = ?', $attributesetId));
        }

        $Select->where('a.is_active = ?', '1');
        $Select->where('a.is_configurable = ?', '1');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function addAttribute($Form, $type, $req)
    {
        $req->setPost('type', $type);

        $id = $Form->addRecord($req);

        return $id;
    }

    public function editAttribute($Form, $current, $req)
    {
        $id = $Form->editRecord($current, $req);

        return $id;
    }

    public function deleteAttribute($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator->loadPlugin('Form_Generator')
            ->setData('current', $current)
            ->setSource('catalog/attributes', $current->type)
            ->deleteRecord($current);

        /*        $this    ->deleteColumn($current,'catalog_attribute_values');

                if($current->is_configurable =='1')
                    $this    ->deleteColumn($current,'catalog_products_configurable');
        */

        return $current->id;
    }

    /**
     * Добавление колонки в таблицу атрибутов товара
     *
     * @param string $column Название колонки
     * @param string $sqlName Тип колонки
     * @param string $sqlLenght Длина колонки
     * @param boolean $splitByLang Разбить на языки
     * @return void
     */
    public function addColumn($column, $sqlName, $sqlLenght, $splitByLang = false, $tableName = 'catalog_attribute_values')
    {
        $query = array();

        $this->setTable($tableName);

        if ($splitByLang) {
            // Список языков
            $langList = Fenix_Language::getInstance()->getLanguagesList();

            foreach ($langList AS $langName => $langOptions) {
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
                            ADD ' . Fenix::stringProtect($column . '_' . $langName) . ' ' . $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ')' : '') . ' NOT NULL';
            }
        }
        else {
            $query[] = 'ALTER TABLE ' . $this->getTable() . '
                        ADD ' . Fenix::stringProtect($column) . ' ' . $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ')' : '') . ' NOT NULL';
        }

        foreach ($query AS $query)
            $this->getAdapter()->query($query);

        return;
    }

    /**
     * Редактирование колонки в таблицу атрибутов товара
     *
     * @param Zend_Db_Table_Row $currentAttribute Редактируемый атрибут
     * @param string $column Название колонки
     * @param string $sqlName Тип колонки
     * @param string $sqlLenght Длина колонки
     * @param boolean $splitByLang Разбить на языки
     * @return void
     */
    public function editColumn(Zend_Db_Table_Row $currentAttribute, $column, $sqlName, $sqlLenght, $splitByLang = false, $tableName = 'catalog_attribute_values')
    {
        $newSqlColumnType = $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ') NOT NULL' : '');
        $query = array();
        $langDefault = Fenix_Language::getInstance()->getDefaultLanguage();
        $langList = Fenix_Language::getInstance()->getLanguagesList();

        $this->setTable($tableName);
        $tableInfo = $this->getAdapter()
            ->describeTable($this->getTable());

        // Изменяем тип колонки
        if ($currentAttribute->split_by_lang) {
            foreach ($langList AS $langName => $langOptions) {
                if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                    $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
            					CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                }
                else {
                    $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
            					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                }
            }
        }

        foreach ($query AS $query)
            $this->getAdapter()->query($query);

        /**
         * Шаманим с колонками и языками
         */
        // Если юзверь поставил галочку разбить на языки или она там уже стояла
        if ($splitByLang) {
            $query = array();

            // Если атрибут уже разбит на языки, обновляем колонку
            if ($currentAttribute->split_by_lang) {
                $tableInfo = $this->getAdapter()->describeTable($this->getTable());
                foreach ($langList AS $langName => $langOptions) {
                    if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $column . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                    else {
                        $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
                					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
            else {
                // Изменяем тип колонки
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $currentAttribute->sys_title . '_' . $langDefault->name . ' ' . $newSqlColumnType;

                foreach ($langList AS $langName => $langOptions)
                    if ($langName != $langDefault->name) {
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                    }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);

                // Если системное название изменилось
                $query = array();
                foreach ($langList AS $langName => $langOptions) {
                    $query[] = 'ALTER TABLE ' . $this->getTable() . '
                                CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $column . '_' . $langName . ' ' . $newSqlColumnType;
                }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
        }
        else {
            // Если юзверь вдруг решил разбить колонки на языки
            $query = array();

            if ($currentAttribute->split_by_lang) {
                // Изменяем тип колонки
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . '_' . $langDefault->name . ' ' . $currentAttribute->sys_title . ' ' . $newSqlColumnType;

                foreach ($langList AS $langName => $langOptions)
                    if ($langName != $langDefault->name) {
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					DROP ' . $currentAttribute->sys_title . '_' . $langName;
                    }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);

                // Если системное название изменилось
                $query = array();
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $column . ' ' . $newSqlColumnType;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
            else {
                // Если системное название изменилось
                $query = array();

                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $column . ' ' . $newSqlColumnType;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
        }
    }

    /**
     * Удаление колонки в таблицу атрибутов товара
     *
     * @param Zend_Db_Table_Row $currentAttribute Удаляемый атрибут
     * @param string $tableName
     */
    public function deleteColumn(Zend_Db_Table_Row $currentAttribute, $tableName = 'catalog_attribute_values')
    {
        $query = array();

        $this->setTable($tableName);
        $tableInfo = $this->getAdapter()->describeTable($this->getTable());

        // Изменяем тип колонки
        if ($currentAttribute->split_by_lang) {
            $langList = Fenix_Language::getInstance()->getLanguagesList();

            foreach ($langList AS $langName => $langOptions) {
                if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                    $query[] = 'ALTER TABLE ' . $this->getTable() . '
            					DROP ' . $currentAttribute->sys_title . '_' . $langName;
                }
            }
        }
        else {
            $query[] = 'ALTER TABLE ' . $this->getTable() . '
    					DROP ' . $currentAttribute->sys_title;
        }

        // Выполняем запросы
        foreach ($query AS $query)
            $this->getAdapter()->query($query);
    }

    public function updateAttributesValuesTable($tableName = 'catalog_products')
    {
        $this->setTable($tableName);

        $AttributesList = $this->getAttributesList();
        $langList = Fenix_Language::getInstance()->getLanguagesList();

        $tableInfo = $this->getAdapter()
            ->describeTable($this->getTable($tableName));

        $query = array();

        foreach ($AttributesList AS $attribute) {
            if ($attribute->split_by_lang) {
                foreach ($langList AS $langName => $langInfo) {
                    if ( ! isset($tableInfo[$attribute->sys_title . '_' . $langName])) {
                        $newSqlColumnType = $attribute->sql_type . ($attribute->sql_lenght != null ? '(' . $attribute->sql_lenght . ') NOT NULL' : '');

                        $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
                					ADD ' . $attribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                }
            }
            else {
                if ($attribute->type == 'image') {
                    // Если не создался ранее атрубит изображения
                    if ( ! isset($tableInfo[$attribute->sys_title])) {
                        $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . ' ADD `' . $attribute->sys_title . '` LONGTEXT NOT NULL;';
                    }

                    // Если не создался ранее атрубит информации об изображении
                    if ( ! isset($tableInfo[$attribute->sys_title . '_info']) && substr($attribute->sys_title, 5) != '_info') {
                        $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . ' ADD `' . $attribute->sys_title . '_info` LONGTEXT NOT NULL AFTER `' . $attribute->sys_title . '`;';
                    }
                }
            }
        }

        foreach ($query AS $query) {
            $this->getAdapter()->query($query);
        }
    }

    /**
     * Формируем название таблицы для хранения значений атрибута
     * @param Zend_Db_Table_Row $attribute
     * @return string
     */
    public function getAttributeTable($attribute)
    {
        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';

            return $tableName;
        }

        return $tableName;
    }

    /**
     * Формируем названия колонок для хранения значений атрибута
     * @param Zend_Db_Table_Row $attribute
     * @return string
     */
    public function getAttributeContentColumns(Zend_Db_Table_Row $attribute)
    {
        $tableName = $this->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);
        $column = 'content';
        $cols = array();
        if ($Engine->columnIsSplitByLang($column) && $attribute->split_by_lang == '1') {
            foreach (Fenix_Language::getInstance()->getLanguagesList() as $lang) {
                $cols[] = $column . '_' . $lang->name;
            }
        }
        else {
            $cols[] = $column;
        }

        return $cols;
    }

    public function getAttributeValues($attribute)
    {

        $cacheId = 'Filter_getAttributeValues_' . $attribute->id;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';
        }
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $Select = $this->setTable($tableName)
            ->select();

        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('attribute_id = ?', $attribute->id);
        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    public function getAttributeValuesInfo($attribute, $productId = null)
    {
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

        $cols[] = new Zend_Db_Expr('COUNT(avr.id) as relations_count');
        $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
            ->from(array(
                'avc' => $this->getTable()
            ), $cols)
            ->joinLeft(array(
                'avr' => $this->getTable('attr_values')
            ), 'avr.value_id = avc.id AND '
                . 'avr.attribute_id=  avc.attribute_id', null);

        if ($productId) {
            $Select->where('avr.product_id = ?', $productId);
        }

        $Select->where('avc.attribute_id = ?', $attribute->id);
        $Select->group('avc.id');
        $Select->order('avc.position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function fillEmptyValuesUrl()
    {
        $attributesList = Fenix::getModel('catalog/filter')->getAvailableAttributes();
        foreach ($attributesList as $i => $attribute) {

            /*$attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById((int)$attributeId);
            if($attribute == null) return null;*/

            $valuesList = Fenix::getModel('catalog/backend_attributes')->getAttributeValuesInfo($attribute);
            foreach ($valuesList as $value) {
                if (trim($value->url) == '') {
                    $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

                    $data = array(
                        'url' => Fenix::stringProtectUrl($value->content)
                    );
                    $this->setTable($tableName)
                        ->update($data, $this->getAdapter()->quoteInto('id =?', $value->id));
                }
            }
            //$url   = Fenix::stringProtectUrl($value->content);
            //if ($_attr->sys_title == 'price') continue;
            //$attrValues = Fenix::getModel('catalog/filter')->getAttributeValues($_attr);
            //if($attrValues)
        }
    }

    /**
     * Returned sys_title the all active attributes
     * @return array('sys_title' => '');
     */
    public function getActiveAttributesNameListForProductForm()
    {
        $cacheId = 'Fenix_Catalog_Model_Backend_Attributes_getActiveAttributesNameListForProductForm';

        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $data = array();

        $Select = $this->getAttributesListAsSelect();
        $Select->where('a.is_active = ?', '1');
        $Select->reset(Zend_Db_Select::COLUMNS);
        $Select->columns(new Zend_Db_Expr('a.sys_title'));

        $Result = $this->fetchAll($Select);

        if ($Result != null) {
            foreach ($Result as $attr) {
                $data[$attr->sys_title] = ''; // the value not used
            }
        }

        if ( ! empty($data)) {
            Zend_Registry::set($cacheId, $data);
        }

        return $data;
    }

    /**
     * Returned the attribute values by input autocomplete jQuery UI Plugin
     * @param $sys_title - array sys_title values or string sys_title
     * @return null|Zend_Db_Table_Rowset
     */
    public function getAttributesListBySysTitle($sys_title)
    {
        $caheId = 'Fenix_Catalog_Model_Backend_Attributes_getAttributesListBySysTitle_';

        if (is_array($sys_title)) {
            $sys_title = array_unique($sys_title);
            sort($sys_title);
            $caheId .= implode('_', $sys_title);
        }
        else {
            $caheId .= $sys_title;
        }

        if (Zend_Registry::isRegistered($caheId)) {
            return Zend_Registry::get($caheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('sys_title IN(?)', $sys_title);


        $Result = $this->fetchAll($Select);

        if ($Result != null) {
            Zend_Registry::set($caheId, $Result);
        }

        return $Result;
    }

    /**
     * Returned the attribute values by input autocomplete jQuery UI Plugin
     * @param Zend_Db_Table_Row $attribute
     * @param string $value - search all values where like %$value% sting
     * @return array('value 1', 'value 2');
     */
    public function getAutocompleteData(Zend_Db_Table_Row $attribute, $value = '')
    {
        $cacheId = 'Fenix_Catalog_Model_Backend_Attributes_getAutocompleteData_' . $attribute->id . $value;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $langsList = Fenix_Language::getInstance()->getLanguagesList()->toArray();

        $autocompleteData = array();
        foreach ($langsList as $_lang) {
            $options = 'select_options';

            if ($attribute->split_by_lang == '1') {
                $options .= '_' . $_lang['name'];
            }

            $autocompleteData[$_lang['name']] = (array) explode("\n", $attribute->{$options});

            if ( ! empty($autocompleteData[$_lang['name']])) {
                foreach ($autocompleteData[$_lang['name']] as $k => $_item) {
                    $autocompleteData[$_lang['name']][$k] = str_replace(array(
                        "\t", "\r", "\n",
                    ), '', strip_tags($_item));
                }
            }

            if ($attribute->split_by_lang != '1') {
                break;
            }
        }

        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';
        }

        $autocompleteDataDB = array();
        foreach ($langsList as $_lang) {

            $column = 'content';

            if ($attribute->split_by_lang == '1') {
                $column .= '_' . $_lang['name'];
            }

            $Engine = Fenix_Engine::getInstance();
            $Engine->setSource('catalog/' . $tableName);

            $Select = $this->setTable($tableName)
                ->select();

            $Select->from($this->_name, 'DISTINCT(' . $column . ')');
            $Select->where('attribute_id = ?', $attribute->id);

            if ( ! empty($value)) {
                $Select->where($column . ' LIKE ?', '%' . $value . '%');
            }

            $Result = $this->fetchAll($Select);

            $autocompleteDataDB[$_lang['name']] = array();

            if ($Result != null) {
                foreach ($Result as $_item) {
                    if ($_item->{$column} != '') {
                        $autocompleteDataDB[$_lang['name']][] = str_replace(array("\t", "\r", "\n"), '', strip_tags($_item->{$column}));
                    }
                }

                $autocompleteData[$_lang['name']] = array_merge($autocompleteData[$_lang['name']], $autocompleteDataDB[$_lang['name']]);
            }

            $autocompleteData[$_lang['name']] = array_unique($autocompleteData[$_lang['name']]);

            if ($attribute->split_by_lang != '1') {
                break;
            }
        }

        if ( ! empty($autocompleteData)) {
            Zend_Registry::set($cacheId, $autocompleteData);
        }

        return $autocompleteData;
    }

    /**
     * Получить информацию об пользовательскм изображения
     *
     * @param $attributesetId
     * @param $productId
     * @return string;
     */
    public function getAttrImageInfo($attributesetId, $productId)
    {
        $Select = $this->setTable('attr_image_info')->select()
            ->from($this->_name)
            ->where('attribute_id = ?', (int) $attributesetId)
            ->where('product_id = ?', (int) $productId);

        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function editAttributeValues($Form, $current, $req)
    {
        $values_position = $req->getPost('values_position');
        $values_url = $req->getPost('values_url');
        $values_type = $req->getPost('values_type');
        $values_property = $req->getPost('values_property');
        if ($current->split_by_lang == '1') {
            $langList = Fenix_Language::getLanguagesList();
            foreach ($langList as $lang) {
                $content[$lang->name] = $req->getPost('values_content_' . $lang->name);
            }
        }
        else {
            $content = $req->getPost('values_content');
        }

        $values = array();
        if (
            /*$values_position != null
            &&*/
            $values_url != null
            && $values_type != null
            && $values_property != null
            && $content != null
        ) {
            //Собираем данные в один массив
            /*foreach ($values_position as $id => $position) {
                $values[$id]['position'] = $position;
            }*/
            foreach ($values_url as $id => $value) {
                $values[$id]['url'] = $value;
            }
            foreach ($values_type as $id => $value) {
                $values[$id]['type'] = $value;
            }
            foreach ($values_property as $id => $value) {
                $values[$id]['property'] = $value;
            }
            if ($current->split_by_lang == '1') {
                $langList = Fenix_Language::getLanguagesList();
                foreach ($langList as $lang) {
                    foreach ($content[$lang->name] as $id => $value) {
                        $values[$id]['content_' . $lang->name] = $value;
                    }
                }
            }
            else {
                foreach ($content as $id => $value) {
                    $values[$id]['content'] = $value;
                }
            }
        }

        //Сохраняем в бд
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($current);
        foreach ($values as $id => $data) {
            $this->setTable($tableName)
                ->update($data, $this->getAdapter()->quoteInto('id = ?', $id));
        }
    }

    public function sortingValues($n, $step, $attributeId = null)
    {
        $Select = $this->getAttributesListAsSelect();
        if ($attributeId) {
            $Select->where('id = ?', $attributeId);
        }
        else {
            $Select->limit($step, $n);
        }

        $resultList = $this->fetchAll($Select);
        $resultCount = $resultList->count();

        foreach ($resultList as $attribute) {
            $tableName = $this->getAttributeTable($attribute);

            $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

            $cols[] = new Zend_Db_Expr('COUNT(avr.id) as relations_count');
            $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
                ->from(array(
                    'avc' => $this->getTable()
                ), $cols)
                ->joinLeft(array(
                    'avr' => $this->getTable('attr_values')
                ), 'avr.value_id = avc.id AND '
                    . 'avr.attribute_id=  avc.attribute_id', null);

            $Select->where('avc.attribute_id = ?', $attribute->id);
            $Select->group('avc.id');

            if ($attribute->split_by_lang == '1') {
                $lang = Fenix_Language::getInstance()->getCurrentLanguage();
                $Select->order('avc.content_' . $lang->name . ' asc');
            }
            else {
                $Select->order('avc.content asc');
            }

            $sortedValues = $this->fetchAll($Select);

            $sortedValues = $this->additionalSorting($attribute->sql_type, $sortedValues);

            foreach ($sortedValues as $position => $value) {
                $data = array(
                    'position' => $position
                );
                $this->setTable($tableName);
                $this->update($data, $this->getAdapter()->quoteInto('id = ?', $value->id));
            }
        }
        if ($attributeId == null && $resultCount >= $step) {
            $url = Fenix::getUrl('catalog/attributes/values/processsorting/n/' . ($n + $step));
            print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
            print 'Подождите...';
            exit;
        }
    }

    public function dublicateValuesMerge($n, $step, $attributeId = null)
    {

        $Select = $this->getAttributesListAsSelect();
        if ($attributeId) {
            $Select->where('id = ?', $attributeId);
        }
        else {
            $Select->limit($step, $n);
        }

        $resultList = $this->fetchAll($Select);;
        $resultCount = $resultList->count();

        foreach ($resultList as $attribute) {
            $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

            $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

            $cols[] = new Zend_Db_Expr('COUNT(avr.id) as relations_count');
            $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
                ->from(array(
                    'avc' => $this->getTable()
                ), $cols)
                ->joinLeft(array(
                    'avr' => $this->getTable('attr_values')
                ), 'avr.value_id = avc.id AND '
                    . 'avr.attribute_id =  avc.attribute_id', array('COUNT(avc.id) as dublicate_count'));

            $Select->where('avc.attribute_id = ?', $attribute->id);

            //$Select->group('avc.id');
            $Select->group('avc.attribute_id');

            if ($attribute->split_by_lang == '1') {
                $lang = Fenix_Language::getInstance()->getCurrentLanguage();
                $Select->order('avc.content_' . $lang->name . ' asc');
                $Select->group('avc.content_' . $lang->name);
            }
            else {
                $Select->order('avc.content asc');
                $Select->group('avc.content');
            }
            $Select->having('dublicate_count > 1');
            $dublicateValues = $this->fetchAll($Select);

            $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();
            foreach ($dublicateValues as $position => $value) {
                $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
                    ->from(array(
                        'avc' => $this->getTable()
                    ), $cols);
                $Select->where('avc.attribute_id = ?', $attribute->id);
                if ($attribute->split_by_lang == '1') {
                    $lang = Fenix_Language::getInstance()->getCurrentLanguage();
                    $Select->where('avc.content_' . $lang->name . ' = ?', $value->content);
                }
                else {
                    $Select->where('avc.content = ?', $value->content);
                }
                $dublicateList = $this->fetchAll($Select);

                $mainRecord = null;
                foreach ($dublicateList as $valueRecord) {
                    if ($mainRecord == null) {
                        $mainRecord = $valueRecord;
                    }
                    else {
                        /**
                         * Заменяем связи товаров с дубликатом значения
                         */
                        $data = array(
                            'value_id' => $mainRecord->id
                        );
                        $this->setTable('attr_values')
                            ->update($data,
                                $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                                $this->getAdapter()->quoteInto('value_id = ?', $valueRecord->id)
                            );
                        /**
                         * Заменяем дубликаты в ценах варианта товара
                         */
                        $data = array(
                            'value_id' => $mainRecord->id
                        );
                        $this->setTable('product_conf_values')
                            ->update($data,
                                $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                                $this->getAdapter()->quoteInto('value_id = ?', $valueRecord->id)
                            );
                        /**
                         * Находим материалы соответсвие которых указывает на дубликат
                         */
                        $data = array(
                            'value_id' => $mainRecord->id
                        );
                        $materials = $this->findMaterialItemsByValueId($attribute->id, $valueRecord->id);
                        foreach ($materials as $materialItem) {
                            $this->setTable('catalog_material_item')
                                ->update($data, $this->getAdapter()->quoteInto('id = ?', $materialItem->id));
                        }
                        /**
                         * Удаляем сам дубликат
                         */
                        $this->setTable($tableName)
                            ->delete($this->getAdapter()->quoteInto('id = ?', $valueRecord->id));
                    }
                }
            }
        }
        if ($attributeId == null && $resultCount >= $step) {
            $url = Fenix::getUrl('catalog/attributes/values/dublicate/n/' . ($n + $step));
            print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
            print 'Подождите...';
            exit;
        }
    }

    public function updateAttributeValuesPosition($position)
    {

        $attribute = null;
        foreach ($position as $index => $valueData) {
            //Получаем объект атрибута если он пустой или id отличается от существующего
            if (
                isset($valueData['attribute_id']) && $attribute == null ||
                isset($valueData['attribute_id']) && $attribute->id != $valueData['attribute_id']
            ) {
                $attribute = $this->getAttributeById($valueData['attribute_id']);
            }
            if ($attribute && isset($valueData['value_id'])) {
                $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
                $data = array(
                    'position' => $index
                );

                $this->setTable($tableName)
                    ->update($data,
                        $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                        $this->getAdapter()->quoteInto('id = ?', $valueData['value_id'])
                    );
            }
        }
    }

    protected function additionalSorting($typeAttribute, $data = array())
    {

    }
}