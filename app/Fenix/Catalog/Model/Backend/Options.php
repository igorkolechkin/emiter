<?php
class Fenix_Catalog_Model_Backend_Options extends Fenix_Resource_Model
{

    /**
     * Список вариантов сложного товара
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getOptionsIdArray($productId)
    {
        $this->setTable('catalog_products_options');
        $Select = $this->select()
                       ->from($this->_name)
                       ->where('product_id = ?', (int)$productId);
        $list = $this->fetchAll($Select);

        $result = array();
        foreach ($list as $itemData) {
            $result[] = $itemData->material_item_id;
        }

        return $result;
    }

    /**
     * Сохраняем связи товара с материалами
     * @param $currentId
     * @param $req
     */
    public function updateOptions($currentId, $req){
        //Удаляем старые связи с материалами
        $this->setTable('catalog_products_options_relations')
             ->delete(
                 $this->getAdapter()->quoteInto('product_id = ?', $currentId)
             );

        //Добавляем новые
        $list = $req->getPost('_list');

        if(isset($list['options']))
        foreach($list['options'] as $key => $optionsId){

            $data = array(
                'product_id' => $currentId,
                'option_id'  => $optionsId,
            );
            $this->setTable('catalog_products_options_relations')
                 ->insert($data);

        }
    }


    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionsListAsSelect($parent = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/options');

        $this   ->setTable('catalog_products_options');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'o' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'o'
        )));
        if ($parent) {
            $Select->where('o.parent = ?', $parent);
        }

        return $Select;
    }

    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionsList($parent = null)
    {
        $Select = $this->getOptionsListAsSelect();

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionsListByProductId($productId)
    {
        $Select = $this->getOptionsListAsSelect();

        $Select->join(array(
            'cpr' => $this->getTable('catalog_products_options_relations')
        ), 'o.id = cpr.option_id', array());

        $Select->where('cpr.product_id = ?', $productId);

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function findOptions($term)
    {
        $Select = $this->getOptionsListAsSelect();
        $Select->where('o.title_russian LIKE ?', "%" . $term . "%");

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionById($id)
    {
        $Select = $this->getOptionsListAsSelect();

        $Select->where('id = ?',$id);
        $Select->limit(1);
        $Result = $this->fetchRow($Select);

        return $Result;
    }

}