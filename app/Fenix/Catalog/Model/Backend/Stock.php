<?php
class Fenix_Catalog_Model_Backend_Stock extends Fenix_Resource_Model
{
    public function isStock($data)
    {
        $this   ->setTable('catalog_stock');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('product_id = ?', (int) $data['product_id'])
                ->where('email      = ?', $data['email'])
                ->where('cellphone  = ?', $data['cellphone']);

        $Result = $this->fetchRow($Select);

        if($Result)
            return true;
        else
            return false;

        return $Result;
    }


    public function getStockListAsSelect()
    {
        $this->setTable('catalog_stock');

        $Select = $this->select();
                       //->setIntegrityCheck(false)

        $Select ->from($this->_name);

        return $Select;
    }

    /**
     * Новый профиль
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор клиента
     */
    public function addStock($_data)
    {
        $this->setTable('catalog_stock');

        // Формируем массив
        $data = array(
            'email'         => $_data['email'],
            'product_id'    => $_data['product_id'],
            'cellphone'     => $_data['cellphone']
        );

        // Вставляем в базу
        $this->insert($data);

        return true;
    }

    /**
     * Новый профиль
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор клиента
     */
    public function sendNotifyStock($id)
    {
        $Select = $this->getStockListAsSelect();
        $Select ->where('product_id = ?', (int)$id);

        $Result = $this->fetchAll($Select);
        foreach($Result AS $mail)
        {
            if($mail->email != null) {
                $product = Fenix::getModel('catalog/products')->getProductById($mail->product_id);
                $product = Fenix::getCollection('catalog/products_product')->setProduct($product->toArray());

                Fenix::getModel('core/mail')->sendUserMail(array(
                    'to'       => $mail->email,
                    'template' => 'catalog.notify.instock', // шаблон письма
                    'assign'   => array(
                        'product'     => (object)$product->toArray()
                    ),
                ));

                $this->deleteStock($mail);
            }
        }
        Fenix::dump();

        return true;
    }

    /**
     * Удаление профиля
     *
     * @param $current
     * @return int
     */
    public function deleteStock($current)
    {
        $this->setTable('catalog_stock');
        $this->delete('id = ' . (int) $current->id);

        return true;
    }
}