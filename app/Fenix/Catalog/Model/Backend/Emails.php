<?php
class Fenix_Catalog_Model_Backend_Emails extends Fenix_Resource_Model
{
    public function isEmail($data)
    {
        $this   ->setTable('email_base');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('email = ?', $data['email']);

        $Result = $this->fetchRow($Select);

        if($Result)
            return true;
        else
            return false;

        return $Result;
    }


    public function getEmailListAsSelect()
    {
        $this->setTable('email_base');

        $Select = $this->select();
                       //->setIntegrityCheck(false)

        $Select ->from($this->_name);

        return $Select;
    }

    /**
     * Новый профиль
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор клиента
     */
    public function addEmail($_data)
    {
        $this->setTable('email_base');

        // Формируем массив
        $data = array(
            'email'         => $_data['email'],
            'cellphone'     => isset($_data['cellphone']) ? $_data['cellphone'] : ''
        );

        // Вставляем в базу
        $this->insert($data);

        return true;
    }

}