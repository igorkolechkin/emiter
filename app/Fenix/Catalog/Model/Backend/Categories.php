<?php
class Fenix_Catalog_Model_Backend_Categories extends Fenix_Resource_Model
{
    public function getCategoryById($id)
    {
        $cacheId = 'getCategoryById_' . $id;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);


        $this->setTable('catalog');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);


        return $Result;
    }

    public function getCategoryByTitleRussian($title)
    {
        $cacheId = 'getCategoryByTitleRussian_' . $title;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);

        $this->setTable('catalog');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('title_russian = ?', $title);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }


    public function getNavigation($category)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');

        $Select  = $this->setTable('catalog')
                        ->select()
                        ->from($this, $Engine ->getColumns())
                        ->where('`left` <= ?', $category->left)
                        ->where('`right` >= ?', $category->right)
                        ->where('parent > 0')
                        ->order('left asc');
        $Result  = $this->fetchAll($Select);

        return $Result;
    }

    public function getCategoriesListAsSelect($parent = 1)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');

        $this->setTable('catalog');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'c' => $this->_name
        ), $Engine ->getColumns(array('prefix' => 'c')));

        $Select ->where('c.parent = ?', $parent);
        $Select ->order('c.position asc');

        return $Select;
    }

    public function getCategoriesList($parent = 1)
    {
        $Select = $this->getCategoriesListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function addCategory($Form, $req)
    {
        // Формируем урл кей
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', $req->getPost('title_' . Fenix_Language::getInstance()->getCurrentLanguage()->name));
        }
        $req->setPost('url_key', Fenix::stringProtectUrl($req->getPost('url_key')));

        $id = $Form->addRecord($req);

        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        return $id;
    }

    public function editCategory($Form, $currentCategory, $req)
    {
        // Формируем урл кей
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', $req->getPost('title_' . Fenix_Language::getInstance()->getCurrentLanguage()->name));
        }
        $req->setPost('url_key', Fenix::stringProtectUrl($req->getPost('url_key')));

        $id = $Form->editRecord($currentCategory, $req);

        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        return $id;
    }

    public function deleteCategory($current) {

	    $msg = '<hr>';

        /** Достаем дочерние */
	    $Select = $this->setTable('catalog')
	                   ->select()
	                   ->from($this)
	                   ->where('`left` >= ?', $current->left)
	                   ->where('`right` <= ?', $current->right)
	                   ->where('id != ?', $current->id)
	                   ->limit(1);

	    $category = $this->fetchRow($Select);

	    /** Дочерних нет - работаем с $current */
	    if($category == null) {
		    $category = Fenix::getModel('catalog/backend_categories')->getCategoryById($current->id);

		    $msg .= '<br>работаем с ридителем '.$category->id;
	    }


	    if($category->id != $current->id) {
		    $msg .= '<br>удаление дочерней категроии ' . $category->id;
		    $Creator = Fenix::getCreatorUI();
		    $Creator->loadPlugin('Form_Generator')
		            ->setSource('catalog/categories', 'default')
		            ->deleteRecord($category);
	    } else {
		    $msg .= '<br>удаление главной категории ' . $category->id;
		    $Creator = Fenix::getCreatorUI();
		    $Creator->loadPlugin('Form_Generator')
		            ->setSource('catalog/categories', 'default')
		            ->deleteRecord($category);

		    Fenix::getModel('catalog/backend_products')->updateProductsRelations();

		    return true;
	    }

	    $msg = Fenix::isDev() ? $msg : '';

	    print '<meta http-equiv="Refresh" content="0">';
	    print 'Подождите...';
	    print $msg;
	    exit;
    }
}