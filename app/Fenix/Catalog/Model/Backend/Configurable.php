<?php

class Fenix_Catalog_Model_Backend_Configurable extends Fenix_Resource_Model
{
    /**
     * Сохраняем Варианты товара
     * @param $productId
     * @param $req
     * @return int
     */
    public function saveConfigurable($productId, $req)
    {

        $count = 0;
        $configurable = $req->getPost('_configurable');

        //$parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($req->getParam('parent'));

        if (/*$parentCategory && */
        $productId /*&& isset($configurable['price'])*/) {

            $confAttrList = Fenix::getModel('catalog/backend_configurable_attributes')->getAttributesList($productId);

            //Fenix::dump($confAttrList->toArray());
            //Удаляем старые
            $this->setTable('product_conf_groups')
                ->delete($this->getAdapter()->quoteInto('product_id = ?', (int) $productId));
            $this->setTable('product_conf_values')
                ->delete($this->getAdapter()->quoteInto('product_id = ?', (int) $productId));

            $langList = Fenix_Language::getLanguagesList();

            //Добавляем новые
            $checkEmpty = '';
            foreach ($configurable['price'] as $i => $price) {

                $price = trim($price);
                $price = str_replace(',', '.', $price);
                $price = (double) str_replace(' ', '', $price);
                if ($price == 0) {
                    continue;
                }

                $priceOld = trim($configurable['price_old'][$i]);
                $priceOld = str_replace(',', '.', $priceOld);
                $priceOld = (double) str_replace(' ', '', $priceOld);

                $type = $configurable['type'][$i];
                $group_sku = $configurable['group_sku'][$i];
                $priceGroup = array(
                    'price'      => $price,
                    'price_old'  => $priceOld,
                    'type'       => $type,
                    'group_sku'  => $group_sku,
                    'product_id' => $productId
                );

                $groupId = $this->setTable('product_conf_groups')->insert($priceGroup);
                $groupData = array(
                    'group_id'   => $groupId,
                    'product_id' => $productId
                );

                foreach ($confAttrList as $attr) {
                    $attrValue = array();
                    if ($attr->split_by_lang == '1') {
                        foreach ($langList as $lang) {
                            $attrValue[$attr->sys_title . '_' . $lang->name] = trim($configurable[$attr->sys_title . '_' . $lang->name][$i]);
                        }
                    }
                    else {
                        $attrValue[$attr->sys_title] = trim($configurable[$attr->sys_title][$i]);
                    }

                    $valueId = Fenix::getModel('catalog/backend_products')->saveAttributeValue($attr, $attrValue);
                    $groupData['attribute_id'] = $attr->id;
                    $groupData['value_id'] = $valueId;
                    $this->setTable('product_conf_values')->insert($groupData);
                }

                //Пропускаем пустые строки
                /*if (trim($checkEmpty) == '')
                    continue;*/

                //Заносим в базу
                /*$this->setTable('catalog_products_configurable')
                     ->insert($data);*/
                $count++;
            }
            //Fenix::dump($count);
        }

        //Сохранем цвет в наличии
        /*Fenix::getModel('catalog/colors')->saveProductColors($productId, $req);*/

        return $count;
    }

    public function addConfigurable($productId, $data, $attributesData)
    {
        $dataKeyForSave = ['group_sku', 'type', 'price_old', 'price'];
        $priceGroup['product_id'] = $productId;

        foreach ($data as $key => $value) {
            if (in_array($key, $dataKeyForSave)) {
                $priceGroup[$key] = $value;
            }
        }

        $groupId = $this->setTable('product_conf_groups')->insert($priceGroup);

        $groupData = array(
            'group_id'   => $groupId,
            'product_id' => $productId
        );
        //Fenix::dump($productId, $groupSku, $type, $price, $attributesData);
        foreach ($attributesData as $attributeId => $valueId) {

            /*if($productId==3501)
                Fenix::dump($attributesData,$attributeId,$productId);*/
            Fenix::getModel('catalog/backend_configurable_attributes')->addAttributeToProduct($productId, $attributeId);

            $groupData['attribute_id'] = $attributeId;
            $groupData['value_id'] = $valueId;
            $this->setTable('product_conf_values')->insert($groupData);
        }
    }

    /**
     * Удаляем варианты цен
     * @param $productId
     */
    public function clearConfigurable($productId)
    {

        $this->setTable('product_conf_groups')
            ->delete($this->getAdapter()->quoteInto('product_id = ?', (int) $productId));
        $this->setTable('product_conf_values')
            ->delete($this->getAdapter()->quoteInto('product_id = ?', (int) $productId));

    }

    /**
     * Удаляем атрибуты
     * @param $productId
     */
    public function clearConfigurableAttributes($productId)
    {

        $this->setTable('product_conf_attr')
            ->delete($this->getAdapter()->quoteInto('product_id = ?', (int) $productId));
    }

    /**
     * Список вариантов сложного товара
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getConfigurable($productId, $fillData = true)
    {
        $confAttributes = Fenix::getModel('catalog/backend_configurable_attributes')->getAttributesList($productId);
        /*$this->setTable('product_conf_attr');
        $Select = $this->select()
                       ->from(array(
                           'pca' => $this->_name
                       ));

        $Select ->join(array(
            'pcv'=>$this->getTable('product_conf_values')
        ),'pcv.attribute_id = pca.attribute_id',null);

        $Select->where('pca.product_id = ?', $productId);

        $list = $this->fetchAll($Select);*/

        $cols = array();
        foreach ($confAttributes as $attr) {
            $cols[] = 'GROUP_CONCAT(IF(pcv.attribute_id = ' . $attr->id . ', pcv.value_id, null)) AS ' . $attr->sys_title;
            //Fenix::dump($confAttributes, $attr);
        }
        $Select = Fenix::getModel('catalog/backend_configurable_groups')->getGroupsListAsSelect();
        $this->setTable('product_conf_values');
        $Select->join(array(
            'pcv' => $this->getTable('product_conf_values')
        ), 'pcv.group_id = pcg.id AND ' .
            $this->getAdapter()->quoteInto('pcg.product_id = ?', $productId)
            , $cols);

        $Select->group('pcg.id');
        $list = $this->fetchAll($Select);

        return $list;
        //Fenix::dump($Select->assemble(),$list);//,$list);
        /*$configurables =array();
        foreach($list as $item){

        }
        if ($fillData) {
            //Заполняем недостающие данные по % скидки или по новой цене
            $configurables = array();
            foreach ($list as $i => $item) {
                $data = $item->toArray();
                //Указана скидка - заполняем цену со скидкой
                if ($item->discount > 0) {
                    $data['price'] = $item->base_price - ($item->base_price * $item->discount / 100);
                }
                //Указана цена -  заполняем скидку
                if ($item->price > 0) {
                    $data['discount'] = round($item->price / $item->base_price * 100,2);
                }
                $configurables[] = (object)$data;
            }
            return new Fenix_Object_Rowset(array(
                'data'     => $configurables,
                'rowClass' => false
            ));
        } else {
            return $list;
        }*/
    }

}