<?php
class Fenix_Catalog_Model_Backend_Material extends Fenix_Resource_Model
{

    /**
     * Список вариантов сложного товара
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getMaterialIdArray($productId)
    {
        $this->setTable('catalog_material_products');
        $Select = $this->select()
                       ->from($this->_name)
                       ->where('product_id = ?', (int)$productId);
        $list = $this->fetchAll($Select);

        $result = array();
        foreach($list as $itemData){
            $result[]=$itemData->material_item_id;
        }

        return $result;
    }

    /**
     * Сохраняем связи товара с материалами
     * @param $currentId
     * @param $req
     */
    public function updateMaterial($currentId, $req){
        //Удаляем старые связи с материалами
        $this->setTable('catalog_material_products')
             ->delete(
                 $this->getAdapter()->quoteInto('product_id = ?', $currentId)
             );

        //Добавляем новые
        $materialData = $req->getPost('_material');
        foreach($materialData as $itemId => $isActive){
            if($isActive =='1'){
                $data = array(
                    'product_id'       => $currentId,
                    'material_item_id' => $itemId,
                );
                $this->setTable('catalog_material_products')
                     ->insert($data);
            }
        }
    }

}