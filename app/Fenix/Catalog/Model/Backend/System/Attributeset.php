<?php
class Fenix_Catalog_Model_Backend_System_Attributeset extends Fenix_Resource_Model
{
    public function getAttributesetSelect($currentId)
    {
        $list = $this->getAttributesetList();

        $Select = Fenix::getCreatorUI()
                        ->loadPlugin('Form_Select')
                        ->setId('attributeset_id')
                        ->setName('attributeset_id')
                        ->setSelected($currentId);

        foreach ($list AS $_attributeset) {
            $Select->addOption($_attributeset->id, $_attributeset->title);
        }

        return $Select->fetch();
    }

    /**
     * Наболр атрибутов по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributesetById($id)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset');

        $this   ->setTable('catalog_attributeset');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getAttributesetListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset');

        $this   ->setTable('catalog_attributeset');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from($this->_name, $Engine ->getColumns());

        $Select ->order('title_russian asc');

        return $Select;
    }

    public function getAttributesetList()
    {
        $Select = $this->getAttributesetListAsSelect();
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function addAttributeset($Form, $req)
    {
        $id = $Form->addRecord($req);

        return $id;
    }

    public function editAttributeset($Form, $current, $req)
    {
        $id = $Form->editRecord($current, $req);

        return $id;
    }

    public function deleteAttributeset($current)
    {
        $Creator = Fenix::getCreatorUI();

        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('catalog/attributeset', 'default')
                 ->deleteRecord($current);

        $Groups = $this->getGroupsList($current->id);
        foreach ($Groups AS $_group) {
            $this->deleteGroup($_group);
        }

        return $current->id;
    }

    // --------------------- ГРУППЫ

    public function getGroupById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset_groups');

        $this   ->setTable('catalog_attributeset_groups');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function deleteGroup($current)
    {
        $Creator = Fenix::getCreatorUI();

        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('catalog/attributeset_groups', 'default')
                 ->deleteRecord($current);

        $this->setTable('catalog_attributeset_groups_attributes')
             ->delete('group_id = ' . (int) $current->id);

        return $current->id;
    }

    public function getGroupsList($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset_groups');

        $this   ->setTable('catalog_attributeset_groups');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('parent = ?', (int) $parent);
        $Select ->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }



    /**
     * Список атрибутов группы
     *
     * @param int $groupId Идентификатор группы
     * @return Zend_Db_Table_Rowset
     */
    public function getGroupAttributesList($groupId)
    {
        $List = $this->setTable('catalog_attributeset_groups_attributes')->fetchAll(
            $this->select()
                 ->from($this)
                 ->where('group_id = ?', $groupId)
                 ->order('position asc')
        );

        $Attributes = array();

        foreach ($List AS $list) {
            $Engine = Fenix_Engine::getInstance();
            $Engine ->setSource('catalog/attributes');

            $this->setTable('catalog_attributes');

            $Select = $this->select();
            $Select ->from($this->_name, $Engine ->getColumns());
            $Select ->where('id = ?', (int) $list->attribute_id);
            $Select ->limit(1);

            $Test   = $this->fetchRow($Select);

            if ($Test != null)
                $Attributes[] = $Test->toArray();
        }

        return new Zend_Db_Table_Rowset(array(
            'data' => $Attributes
        ));
    }

    /**
     * Список доступных атрибутов модуля
     *
     * @param Zend_Db_Table_Row $attributesetId Набор атрибутов
     * @return Zend_Db_Table_Rowset
     */
    public function getAvailableAttributesList($attributesetId)
    {
        $AllAttributes = Fenix::getModel('catalog/backend_system_attributes')->getAttributesList();
        $Result = array();

        foreach ($AllAttributes AS $attribute)
        {
            $Test = $this->setTable('catalog_attributeset_groups_attributes')->fetchRow(
                'attributeset_id = ' . (int) $attributesetId . ' AND ' .
                'attribute_id = ' . (int) $attribute->id . ' AND ' .
                'attribute_source = "system"'
            );

            if ($Test == null)
                $Result[] = $attribute->toArray();
        }

        return new Zend_Db_Table_Rowset(array(
            'data' => $Result
        ));

    }

    /**
     * Сохраняем группы и атрибуты
     *
     * @param Zend_Db_Table_Row $attributeset наболр атрибутов
     * @param array $post
     * @return void
     */
    public function saveAttributeset(Zend_Db_Table_Row $attributeset, array $post)
    {
        // Обновим сортировку групп атрибутов из набора
        if (isset($post['attributeGroup'])) {
            foreach ($post['attributeGroup'] AS $i => $groupId) {
                $this->setTable('catalog_attributeset_groups')
                     ->update(array(
                         'position' => ($i + 1)
                     ), 'id = ' . (int) $groupId);

                $this->setTable('catalog_attributeset_groups_attributes')
                    ->delete('group_id = ' . (int) $groupId);
            }
        }

        // Формируем массив для вставки в таблицу
        if (isset($post['attribute'])) {
            $result = array();
            $i = 1;
            foreach ($post['attribute'] AS $attributeId => $groupId) {
                $result[] = array(
                    'attributeset_id' => $attributeset->id,
                    'group_id'        => $groupId,
                    'attribute_id'    => $attributeId,
                    'position'        => $i
                );
                $i++;
            }

            foreach ($result AS $data) {
                $this->setTable('catalog_attributeset_groups_attributes')
                     ->insert($data);
            }
        }

        return;
    }

    public function copyAttributeset(Zend_Db_Table_Row $attributeset){
        /**
         * Создаем набор атрибутов
         */
        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        // Источник
        $Form      ->setSource('catalog/attributeset', 'default')
                   ->renderSource();
        // Компиляция
        $Form      ->compile();

        $req = $this->getRequest();

        $req->setPost('title_russian', $attributeset->title . '(копия)');
        $req->setPost('name', $attributeset->name . '_copy');
        $req->setPost('create_date', date('Y-m-d H:i:s'));

        $attributeset_id = Fenix::getModel('catalog/backend_attributeset')->addAttributeset($Form, $this->getRequest());

        $groups = Fenix::getModel('catalog/backend_attributeset')->getGroupsList($attributeset->id);
        foreach($groups as $i=> $_group){
            $attributes = Fenix::getModel('catalog/backend_attributeset')->getGroupAttributesList($_group->id);

            $groupId = Fenix::getModel('catalog/backend_groups')->addGroup(
                $attributeset_id,
                array('title_russian' => $_group->title),
                $_group->position
            );
            $result = array();
            foreach($attributes as $n=>$_attribute){
                $result[] = array(
                    'attributeset_id' => $attributeset_id,
                    'group_id'        => $groupId,
                    'attribute_id'    => $_attribute->id,
                    'position'        => $n
                );
            }

            foreach ($result AS $data) {
                $this->setTable('catalog_attributeset_groups_attributes')
                     ->insert($data);
            }
        }

        return $attributeset_id;
    }
}