<?php
class Fenix_Catalog_Model_Backend_System_Attributes extends Fenix_Resource_Model
{
    public function getSelectValues()
    {
        return 'asd';
    }

    /**
     * Атрибут по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeById($id)
    {
        $cacheId = 'FenSysAttributeById_' . $id;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/system_attributes');

        $this->setTable('catalog_system_attributes');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Атрибут по системному названию
     *
     * @param $sys_title
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeBySysTitle($sys_title)
    {
        $cacheId = 'FenSysAttributeBySysTitle_' . $sys_title;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);


        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/system_attributes');

        $this->setTable('catalog_system_attributes');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('sys_title = ?', $sys_title);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Стандартный Select для атрибутов
     * @return Zend_Db_Select
     */
    public function getAttributesListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/system_attributes');

        $this   ->setTable('catalog_system_attributes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine ->getColumns());

        $Select ->order('a.title_russian asc');

        return $Select;
    }

    /**
     * Список всех атрибутов
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributesList()
    {
        $Select = $this->getAttributesListAsSelect();
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список атрибутов помеченных как активные
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveAttributesList()
    {
        $Select = $this->getAttributesListAsSelect();
        $Select ->where('a.is_active = ?', '1');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список атрибутов помеченных как дл янастраиваемого товара
     * @param null $attributesetId Дополнительная фильтрация по Id набора
     * @return Zend_Db_Table_Rowset_Abstract
     */

    public function getConfigurableAttributesList($attributesetId = null)
    {
        $Select = $this->getAttributesListAsSelect();

        if($attributesetId)
        {
            $Select->join(array(
                'g'=>$this->getTable('catalog_system_attributeset_groups_attributes')
            ), 'a.id = g.attribute_id AND '. $this->getAdapter()->quoteInto('attributeset_id = ?', $attributesetId));
        }

        $Select ->where('a.is_active = ?', '1');
        $Select ->where('a.is_configurable = ?', '1');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Создаем новый атрибут
     * @param $Form
     * @param $type
     * @param $req
     * @return mixed
     */
    public function addAttribute($Form, $type,  $req)
    {
        $req ->setPost('type', $type);

        $this->addColumn(
            (string) $req->getPost('sys_title'),
            (string) $req->getPost('sql_type'),
            (string) stripcslashes($req->getPost('sql_lenght')),
            (bool)   $req->getPost('split_by_lang'),
            'catalog_products'
        );

        // Если это изображение - добавляем поле "{$req->getPost('sys_title'}_info" для сохранения инофрмации об изображении
        if($type == 'image'){
            $this->addColumn(
                (string) $req->getPost('sys_title').'_info',
                (string) $req->getPost('sql_type'),
                (string) stripcslashes($req->getPost('sql_lenght')),
                (bool)   $req->getPost('split_by_lang'),
                'catalog_products'
            );
        }

        $id = $Form->addRecord($req);

        return $id;
    }

    /**
     * Редактирование атрибута
     * @param $Form
     * @param $current
     * @param $req
     * @return mixed
     */
    public function editAttribute($Form, $current, $req)
    {
        $id = $Form->editRecord($current, $req);

        $this->editColumn(
            $current,
            (string) $req->getPost('sys_title'),
            (string) $req->getPost('sql_type'),
            (string) stripcslashes($req->getPost('sql_lenght')),
            (bool)   $req->getPost('split_by_lang'),
            'catalog_products'
        );

        return $id;
    }

    public function deleteAttribute($current)
    {
        $Creator = Fenix::getCreatorUI();

        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('catalog/system_attributes', $current->type)
                 ->deleteRecord($current);

        $this    ->deleteColumn($current,'catalog_products');

        if($current->is_configurable =='1')
            $this    ->deleteColumn($current,'catalog_products_configurable');

        return $current->id;
    }

    /**
     * Добавление колонки в таблицу атрибутов товара
     *
     * @param string $column Название колонки
     * @param string $sqlName Тип колонки
     * @param string $sqlLenght Длина колонки
     * @param boolean $splitByLang Разбить на языки
     * @return void
     */
    public function addColumn($column, $sqlName, $sqlLenght, $splitByLang = false, $tableName = 'catalog_products')
    {
        $query = array();

        $this->setTable($tableName);

        if ($splitByLang) {
            // Список языков
            $langList = Fenix_Language::getInstance()->getLanguagesList();

            foreach ($langList AS $langName => $langOptions) {
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
                            ADD ' . Fenix::stringProtect($column . '_' . $langName) . ' ' . $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ')' : '') . ' NOT NULL';
            }
        }
        else {
            $query[] = 'ALTER TABLE ' . $this->getTable() . '
                        ADD ' . Fenix::stringProtect($column) . ' ' . $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ')' : '') . ' NOT NULL';
        }

        foreach ($query AS $query)
            $this->getAdapter()->query($query);

        return;
    }

    /**
     * Редактирование колонки в таблицу атрибутов товара
     *
     * @param Zend_Db_Table_Row $currentAttribute   Редактируемый атрибут
     * @param string            $column             Название колонки
     * @param string            $sqlName            Тип колонки
     * @param string            $sqlLenght          Длина колонки
     * @param boolean           $splitByLang        Разбить на языки
     * @return void
     */
    public function editColumn(Zend_Db_Table_Row $currentAttribute, $column, $sqlName, $sqlLenght, $splitByLang = false, $tableName = 'catalog_products')
    {
        $newSqlColumnType = $sqlName . ($sqlLenght != null ? '(' . $sqlLenght . ') NOT NULL' : '');
        $query            = array();
        $langDefault      = Fenix_Language::getInstance()->getDefaultLanguage();
        $langList         = Fenix_Language::getInstance()->getLanguagesList();

        $this->setTable($tableName);
        $tableInfo        = $this->getAdapter()
            ->describeTable($this->getTable());

        // Изменяем тип колонки
        if ($currentAttribute->split_by_lang) {
            foreach ($langList AS $langName => $langOptions) {
                if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                    $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
            					CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                }
                else {
                    $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
            					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                }
            }
        }

        foreach ($query AS $query)
            $this->getAdapter()->query($query);

        /**
         * Шаманим с колонками и языками
         */
        // Если юзверь поставил галочку разбить на языки или она там уже стояла
        if ($splitByLang) {
            $query = array();

            // Если атрибут уже разбит на языки, обновляем колонку
            if ($currentAttribute->split_by_lang) {
                $tableInfo = $this->getAdapter()->describeTable($this->getTable());
                foreach ($langList AS $langName => $langOptions) {
                    if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $column . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                    else {
                        $query[] = 'ALTER TABLE ' . $this->getTable($tableName) . '
                					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
            else {
                // Изменяем тип колонки
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $currentAttribute->sys_title . '_' . $langDefault->name . ' ' . $newSqlColumnType;

                foreach ($langList AS $langName => $langOptions)
                    if ($langName != $langDefault->name)
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					ADD ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);

                // Если системное название изменилось
                $query = array();
                foreach ($langList AS $langName => $langOptions) {
                    $query[] = 'ALTER TABLE ' . $this->getTable() . '
                                CHANGE ' . $currentAttribute->sys_title . '_' . $langName . ' ' . $column . '_' . $langName . ' ' . $newSqlColumnType;
                }

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
        }
        else {
            // Если юзверь вдруг решил разбить колонки на языки
            $query = array();

            if ($currentAttribute->split_by_lang) {
                // Изменяем тип колонки
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . '_' . $langDefault->name . ' ' . $currentAttribute->sys_title . ' ' . $newSqlColumnType;

                foreach ($langList AS $langName => $langOptions)
                    if ($langName != $langDefault->name)
                        $query[] = 'ALTER TABLE ' . $this->getTable() . '
                					DROP ' . $currentAttribute->sys_title . '_' . $langName;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);

                // Если системное название изменилось
                $query = array();
                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $column . ' ' . $newSqlColumnType;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
            else {
                // Если системное название изменилось
                $query = array();

                $query[] = 'ALTER TABLE ' . $this->getTable() . '
        					CHANGE ' . $currentAttribute->sys_title . ' ' . $column . ' ' . $newSqlColumnType;

                // Выполняем запросы
                foreach ($query AS $query)
                    $this->getAdapter()->query($query);
            }
        }
    }

    /**
     * Удаление колонки в таблицу атрибутов товара
     *
     * @param Zend_Db_Table_Row $currentAttribute   Удаляемый атрибут
     * @return void
     */
    public function deleteColumn(Zend_Db_Table_Row $currentAttribute, $tableName = 'catalog_products')
    {
        $query = array();

        $this->setTable($tableName);
        $tableInfo = $this->getAdapter()->describeTable($this->getTable());

        // Изменяем тип колонки
        if ($currentAttribute->split_by_lang) {
            $langList = Fenix_Language::getInstance()->getLanguagesList();

            foreach ($langList AS $langName => $langOptions) {
                if (array_key_exists($currentAttribute->sys_title . '_' . $langName, $tableInfo)) {
                    $query[] = 'ALTER TABLE ' . $this->getTable() . '
            					DROP ' . $currentAttribute->sys_title . '_' . $langName;
                }
            }
        }
        else {
            $query[] = 'ALTER TABLE ' . $this->getTable() . '
    					DROP ' . $currentAttribute->sys_title;
        }

        // Выполняем запросы
        foreach ($query AS $query)
            $this->getAdapter()->query($query);
    }

    public function updateProductsTable()
    {
        $this->setTable('catalog_attributes');

        $AttributesList = $this->getAttributesList();
        $langList       = Fenix_Language::getInstance()->getLanguagesList();
        $tableInfo      = $this->getAdapter()
                               ->describeTable($this->getTable('catalog_products'));
        $query          = array();

        foreach ($AttributesList AS $attribute) {
            if ($attribute->split_by_lang) {
                foreach ($langList AS $langName => $langInfo) {
                    if (!isset($tableInfo[$attribute->sys_title . '_' . $langName])) {
                        $newSqlColumnType = $attribute->sql_type . ($attribute->sql_lenght != null ? '(' . $attribute->sql_lenght . ') NOT NULL' : '');

                        $query[] = 'ALTER TABLE ' . $this->getTable('catalog_products') . '
                					ADD ' . $attribute->sys_title . '_' . $langName . ' ' . $newSqlColumnType;
                    }
                }
            }
            else {
                if ($attribute->type == 'image') {
                    if (!isset($tableInfo[$attribute->sys_title . '_info'])) {
                        $query[] = 'ALTER TABLE ' . $this->getTable('catalog_products') . ' ADD `' . $attribute->sys_title . '_info` LONGTEXT NOT NULL AFTER `' . $attribute->sys_title . '`';
                    }
                }
            }

        }

        foreach ($query AS $query)
            $this->getAdapter()->query($query);
    }

    /**
     * Returned the attribute values by input autocomplete jQuery UI Plugin
     * @param Zend_Db_Table_Row $attribute
     * @param string $value - search all values where like %$value% sting
     * @return array('value 1', 'value 2');
     */
    public function getAutocompleteData(Zend_Db_Table_Row $attribute, $value = '') {

        $cacheId = 'Fenix_Catalog_Model_Backend_System_Attributes_getAutocompleteData_' . $attribute->id . '_' . $value;
        if(Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

	    $langsList = Fenix_Language::getInstance()->getLanguagesList()->toArray();

	    $autocompleteData = array();
	    foreach($langsList as $_lang) {

		    $options = 'select_options';

		    if($attribute->split_by_lang == '1') {
			    $options .= '_' . $_lang['name'];
		    }

		    $autocompleteData[ $_lang['name'] ] = (array)explode("\n", $attribute->{$options});

		    if(!empty($autocompleteData[ $_lang['name'] ])) {
			    foreach($autocompleteData[ $_lang['name'] ] as $k => $_item) {
				    $autocompleteData[ $_lang['name'] ][ $k ] = str_replace(array(
					    "\t", "\r", "\n",
				    ), '', strip_tags($_item));
			    }
		    }

		    if($attribute->split_by_lang != '1') {
			   break;
		    }
	    }

	    $autocompleteDataDB = array();
	    foreach($langsList as $_lang) {
		    $this->setTable('catalog_products');

		    $Select = $this->select()
		                   ->setIntegrityCheck(false);

		    $column = $attribute->sys_title;

		    if($attribute->split_by_lang == '1') {
			    $column .= '_' . $_lang['name'];
		    }

		    $Select->from(array(
			    'p' => $this->_name,
		    ), 'DISTINCT(p.' . $column . ')');

		    if(!empty($value)) {
			    $Select->where('p.' . $column . ' LIKE ?', '%' . $value . '%');
		    }

		    $Result = $this->fetchAll($Select);

		    $autocompleteDataDB[$_lang['name']] = array();

		    if($Result != null) {
			    foreach($Result as $_item) {
				    if($_item->{$column} != '') {
					    $autocompleteDataDB[$_lang['name']][] = $_item->{$column};
				    }
			    }

			    $autocompleteData[$_lang['name']] = array_merge($autocompleteData[$_lang['name']], $autocompleteDataDB[$_lang['name']]);
		    }

		    $autocompleteData[$_lang['name']] = array_unique($autocompleteData[$_lang['name']]);

		    if($attribute->split_by_lang != '1') {
			    break;
		    }
	    }

	    Zend_Registry::set($cacheId, $autocompleteData);


	    return $autocompleteData;
    }
}