<?php

class Fenix_Catalog_Model_Backend_Configurable_Groups extends Fenix_Resource_Model
{

    public function getGroupsListAsSelect($productId = null)
    {

        $this->setTable('product_conf_groups');
        $Select = $this->select()->setIntegrityCheck(false)
                       ->from(array(
                           'pcg' => $this->_name
                       ));
        if($productId)
            $Select->where('pcg.product_id = ?', $productId);

        return $Select;
    }

    public function getGroupsList($productId = null){
        $Select = $this->getGroupsListAsSelect($productId);
        $Result = $this->fetchAll($Select);

        return $Result;
    }
}