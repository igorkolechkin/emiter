<?php

/**
 * Class Fenix_Catalog_Model_Backend_Configurable_Attributes
 */
class Fenix_Catalog_Model_Backend_Configurable_Attributes extends Fenix_Resource_Model
{

    /**
     * @param $productId
     * @param $attributeId
     * @return mixed|string
     */
    public function addAttributeToProduct($productId, $attributeId)
    {
        $test = $this->getAttributeToProductRelation($productId, $attributeId);

        if ($test) {
            return $test->id;
        } else {
            $data = array(
                'product_id'   => $productId,
                'attribute_id' => $attributeId
            );
            $id = $this->setTable('product_conf_attr')
                       ->insert($data);
            /*if($productId==3501 && $attributeId==342)
                Fenix::dump($test,$id);*/
            return $id;
        }
    }

    /**
     * @param $productId
     * @param $attributeId
     */
    public function removeAttributeFromProduct($productId, $attributeId)
    {
        $test = $this->getAttributeToProductRelation($productId, $attributeId);
        if ($test) {
            $this->setTable('product_conf_attr')
                 ->delete($this->getAdapter()->quoteInto('id = ?', $test->id));
        }
    }

    /**
     * @param $productId
     * @param $attributeId
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeToProductRelation($productId, $attributeId)
    {

        $this->setTable('product_conf_attr');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('product_id = ?', $productId);
        $Select ->where('attribute_id = ?', $attributeId);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributesList($productId){

        $Select = Fenix::getModel('catalog/backend_attributes')->getAttributesListAsSelect();
        $this->setTable('product_conf_attr');
        $Select->join(array(
            'pca' => $this->getTable('product_conf_attr')
        ), 'a.id = pca.attribute_id', null);
        $Select->where('pca.product_id = ?', $productId);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributesIdList($productId){

        $Select = Fenix::getModel('catalog/backend_attributes')->getAttributesListAsSelect();
        $this->setTable('product_conf_attr');
        $Select->join(array(
            'pca' => $this->getTable('product_conf_attr')
        ),'a.id = pca.attribute_id', null);
        $Select ->where('pca.product_id = ?', $productId);

        $Result = $this->fetchAll($Select);
        $data   = array();
        foreach ($Result as $item) {
            $data[] = $item->attribute_id;
        }
        return $Result;
    }
}