<?php

class Fenix_Catalog_Model_Products extends Fenix_Resource_Model
{
    const DEFAULT_PER_PAGE = 9;
    const DEFAULT_VIEWED   = 5;
    const USE_FILTER_ON    = true;
    const USE_FILTER_OFF   = false;
    const USE_GROUP_ON     = true;
    const USE_GROUP_OFF    = false;

    /**
     * Сохранение в списке просмотренных товаров
     *
     * @param Fenix_Catalog_Collection_Products_Product $product
     */
    public function saveViewed(Fenix_Catalog_Collection_Products_Product $product)
    {
        $cache = new Zend_Session_Namespace('Fenix_Catalog');

        if (isset($cache->viewedProducts)) {
            unset($cache->viewedProducts);
        }

        if ($cache->viewedProductsId) {
            $index = array_search($product->getId(), $cache->viewedProductsId);
            if ($index >= 0) {
                unset($cache->viewedProductsId[$index]);
            }
        }

        $cache->viewedProductsId[] = $product->getId();
        $maxViewed = Fenix::getConfig('catalog_products_viewed') ? Fenix::getConfig('catalog_products_viewed') : self::DEFAULT_VIEWED;

        if (count($cache->viewedProductsId) > $maxViewed) {
            // Удаляем последний элемент при переполнении массива
            array_pop($cache->viewedProductsId);
        }

        return;
    }

    /**
     * Список просмотренных товаров
     *
     * @return Fenix_Object_Rowset
     */
    public function getViewed()
    {
        $cache = new Zend_Session_Namespace('Fenix_Catalog');
        $list = array_reverse((array)$cache->viewedProductsId);

        $productsId = array_slice($list, 1);
        $products = $this->getProductsListViewed($productsId);

        return $products;
    }

    /**
     * Запрос на получение просмотренных товаров
     *
     * @param array $productsId
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getProductsListViewed(array $productsId)
    {
        if (count($productsId) > 0) {
            $Select = $this->getProductsListAsSelect(null);

            $Select->where('p.id IN(?)', $productsId);
            $Select->where('p.is_public = ?', '1');
            $Select->where('p.is_visible = ?', '1');

            $result = $this->fetchAll($Select);

        } else {
            $result = new Zend_Db_Table_Rowset(array('data' => array()));
        }

        return $result;
    }

    /**
     * Список товаров
     *
     * @param $sale
     * @param $category
     * @return Zend_Paginator
     * @throws Zend_Db_Select_Exception
     * @throws Zend_Paginator_Exception
     */
    public function getSaleProductsList($sale, $category = null)
    {

        $category = null;
        Fenix_Debug::log('....... app getSaleProductsList getProductsListAsSelect');
        $Select = $this->getProductsListAsSelect($category);

        $Select->join(array(
            'sale_d' => $this->getTable('sale_discount')
        ), 'sale_d.product_id = p.id AND ' . $this->getAdapter()->quoteInto('sale_d.sale_id = ?', $sale->id), false);

        $maxGroupId = $this->getMaxGroupId();

        //Сколько товаров в категории
        $CountSelect = clone $Select;
        $CountSelect->reset(Zend_Db_Select::COLUMNS);
        $CountSelect->columns(
            array(
                new Zend_Db_Expr('COUNT(DISTINCT CASE ' .
                    'WHEN p.group_id = 0 ' .
                    //'THEN (select max(group_id) FROM ' . $this->getTable('catalog_products') . ') + p.id ' .
                    'THEN ' . $maxGroupId . ' + p.id ' .
                    'ELSE p.group_id END) as products_count')
            )
        );
        $CountSelect->reset(Zend_Db_Select::GROUP);
        $CountSelect->reset(Zend_Db_Select::ORDER);
        $CountSelect->limit(1);

        $countResult = $this->fetchRow($CountSelect);

        if ($countResult) {
            $Count = (int)$countResult->products_count;
        } else {
            $Count = 0;
        }


        Fenix_Debug::log('....... app getSaleProductsList $CountSelect end');

        Fenix_Debug::log('....... app getSaleProductsList $paginator');
        if (Fenix::getRequest()->getQuery('perPage')) {
            $perPage = Fenix::getRequest()->getQuery('perPage');
        } else {
            $perPage = Fenix::getConfig('catalog_products_per_page') ? Fenix::getConfig('catalog_products_per_page') : self::DEFAULT_PER_PAGE;
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter->setRowCount($Count);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber((int)Fenix::getRequest()->getQuery("page"))
            ->setItemCountPerPage($perPage);
        Fenix_Debug::log('....... app getSaleProductsList $paginator end');

        return $paginator;
    }

    /**
     * Список товаров
     *
     * @param $sale
     * @param $category
     * @return Zend_Paginator
     * @throws Zend_Db_Select_Exception
     * @throws Zend_Paginator_Exception
     */
    public function getSaleProductsListLight($sale, $category = null)
    {
        $category = null;
        Fenix_Debug::log('....... app getSaleProductsList getProductsListAsSelect');
        $Select = $this->getProductsListAsSelectCustom($category);

        $Select->join(array(
            'sale_p' => $this->getTable('sale_products')
        ), 'sale_p.product_id = p.id AND ' . $this->getAdapter()->quoteInto('sale_p.record_id = ?', $sale->id), false);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Подготовка запроса на список товаров
     *
     * @param $catalog
     * @param bool $useGroup
     * @param bool $useFilter
     * @return Zend_Db_Select
     */
    public function getProductsListAsSelectCustom($list)
    {

        $this->setTable('catalog_products');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'p' => $this->getTable()
        ), array(
            'title_russian AS title',
            'id',
            'is_sale',
            'is_sale',
            'is_top',
            'is_new',
            'group_id',
            'price_old',
            'price',
            'url_key',
            'image',
            'in_stock',
            'rating_count',
            'rating_value'
        ));
        $Select->where('p.is_public = 1');

        //Групируем по p.id что бы убрать дубли изза мультикатегорий
        $Select->group('p.id');

        return $Select;
    }

    /**
     * Список атрибутов для списка товаров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getListAttributes($parent)
    {
        $cache = Fenix_Cache::getCache('catalog/products');
        if ($cache !== false) {
            $cacheId = 'ProductListAttributes_' . $parent;

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->_getListAttributes($parent);
                $cache->save($data, $cacheId);
            }
        } else {
            $data = $this->_getListAttributes($parent);
        }

        return $data;
    }

    public function _getListAttributes($parent)
    {
        if (Zend_Registry::isRegistered('FnxCatalogPL_' . $parent)) {
            return Zend_Registry::get('FnxCatalogPL_' . $parent);
        }

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним каталожеги по волшебному дереву
        $Select->join(array(
            'n' => $this->getTable('catalog')
        ), 'c.left <= n.left AND c.right >= n.right', false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'n.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id', false);

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        $Select->where('c.id = ?', (int)$parent)
            ->where('a.is_in_list = ?', '1');
        $Select->group('a.id');
        $Select->order('cag.position asc')
            ->order('caga.position asc');

//        Fenix::dump($Select->assemble());
        $Result = $this->fetchAll($Select);
        Zend_Registry::set('FnxCatalogPL_' . $parent, $Result);

        return $Result;
    }

    /**
     * Список атрибутов для карты товаров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCardAttributes($parent)
    {
        if (Zend_Registry::isRegistered('getCardAttributes_' . $parent)) {
            return Zend_Registry::get('getCardAttributes_' . $parent);
        }

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним каталожеги по волшебному дереву
        $Select->join(array(
            'n' => $this->getTable('catalog')
        ), 'c.left <= n.left AND c.right >= n.right', false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'n.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id', array('attribute_source', 'position'));

        $Select->where('c.id = ?', (int)$parent);

        $Select->order('cag.position asc')
            ->order('caga.position asc');

        $System_Select = clone $Select;

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        $Engine->setSource('catalog/system_attributes');
        $System_Select->join(array(
            'sa' => $this->getTable('catalog_system_attributes')
        ), 'caga.attribute_id = sa.id', $Engine->getColumns(array(
            'prefix' => 'sa'
        )));

        $Select->where('a.is_in_card = ?', '1')
            ->group('a.id');
        $Select = $this->select()->setIntegrityCheck(false)->from(array('*' => $Select));

        $System_Select->where('sa.is_in_card = ?', '1')
            ->group('sa.id');

        $System_Select = $this->select()->setIntegrityCheck(false)->from(array('*' => $System_Select));


        $Select = $this->select()
            ->union(array($Select, $System_Select), Zend_Db_Table_Select::SQL_UNION_ALL);

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);


        Zend_Registry::set('getCardAttributes_' . $parent, $Result);

        return $Result;
    }

    /**
     * Список атрибутов для сео
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCardSeoAttributes($parent)
    {
        if (Zend_Registry::isRegistered('getCardSeoAttributes_' . $parent)) {
            return Zend_Registry::get('getCardSeoAttributes_' . $parent);
        }

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним каталожеги по волшебному дереву
        $Select->join(array(
            'n' => $this->getTable('catalog')
        ), 'c.left <= n.left AND c.right >= n.right', false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'n.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id', false);

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        $Select->where('c.id = ?', (int)$parent)
            ->where('a.in_seo_template = ?', '1');
        $Select->group('a.id');
        $Select->order('cag.position asc')
            ->order('caga.position asc');

        $Result = $this->fetchAll($Select);
        Zend_Registry::set('getCardSeoAttributes_' . $parent, $Result);

        return $Result;
    }


    /**
     * Список атрибутов для карты товаров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCardAttributesShort($parent)
    {
        if (Zend_Registry::isRegistered('getCardAttributesShort_' . $parent)) {
            return Zend_Registry::get('getCardAttributesShort_' . $parent);
        }


        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним каталожеги по волшебному дереву
        $Select->join(array(
            'n' => $this->getTable('catalog')
        ), 'c.left <= n.left AND c.right >= n.right', false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'n.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id', array('attribute_source', 'position'));

        $Select->where('c.id = ?', (int)$parent);

        $Select->order('cag.position asc')
            ->order('caga.position asc');

        $System_Select = clone $Select;

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        $Engine->setSource('catalog/system_attributes');
        $System_Select->join(array(
            'a' => $this->getTable('catalog_system_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        $Select->where('a.is_in_card_short = ?', '1')
            ->group('a.id');
        $Select = $this->select()->setIntegrityCheck(false)->from(array('*' => $Select));

        $System_Select->where('a.is_in_card_short = ?', '1')
            ->group('a.id');
        $System_Select = $this->select()->setIntegrityCheck(false)->from(array('*' => $System_Select));


        $Select = $this->select()
            ->union(array($Select, $System_Select), Zend_Db_Table_Select::SQL_UNION_ALL);

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set('getCardAttributesShort_' . $parent, $Result);

        return $Result;
    }

    /**
     * Список рекомендуемых товаров
     *
     * @param Fenix_Object $catalog
     * @return mixed
     */
    public function getCategoryRecommendProducts(Fenix_Object $catalog)
    {
        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'c.id = p.parent', $this->productColumns());

        $Select->where('c.left >= ?', (int)$catalog->getLeft())
            ->where('c.right <= ?', (int)$catalog->getRight())
            ->where('p.is_recommended_category = ?', '1');

        $Select->order('p.title_' . Fenix_Language::getInstance()->getCurrentLanguage()->name . ' asc');

        $productsCollection = Fenix::getCollection('catalog/products')->setList(
            $this->fetchAll($Select)
        );

        return $productsCollection;
    }

    /**
     * Проверка, товар ли это?
     *
     * @param null $urlKey
     * @return bool
     */
    public function setUrl($urlKey = null)
    {
        Fenix_Debug::log('product setUrl begin');

        if ($urlKey == null) {
            $urlKey = Fenix::getRequest()->getUrlSegment(0);
        }
        if ($urlKey == 'default') {
            Fenix_Debug::log('product setUrl end');

            return false;
        }

        if ( ! $urlKey) {
            Fenix_Debug::log('product setUrl end');

            return false;
        }

        $Select = $this->setTable('catalog_products')
            ->select();
        $Select->from($this, array('id', 'url_key', 'sku'));
        $Select->where('url_key = ?', (string)$urlKey);
        $Select->limit(1);
        $Result = $this->fetchRow($Select);

        if ($Result == null) {
            Fenix_Debug::log('product setUrl end');

            return false;
        }

        $Select = $this->setTable('catalog_products')
            ->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'p' => $this->_name
        ), $this->productColumns(null));

        //Связь с другими категориями
        $Select->joinLeft(array(
            'r' => $this->getTable('catalog_relations')
        ), 'p.id = r.product_id', null);

        //Скидки
        $Select->joinLeft(array(
            's' => $this->getTable('sale_discount')
        ), 'p.id = s.product_id AND NOW() BETWEEN s.sale_begin_date AND s.sale_end_date', array(
            'sale_discount',
            'sale_begin_date',
            'sale_end_date'
        ));

        $Select->group('p.id');

        $Select->where('p.url_key = ?', (string)$urlKey);

        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Fenix_Debug::log('product setUrl end');

        return $Result;
    }

    /**
     * Список колонок в таблице товаров
     *
     * @param string $prefix
     * @return array
     */
    static public function productColumns($prefix = 'p.')
    {
        $attributes = Fenix::getModel('catalog/system_attributes')->getActiveAttributesList();
        $result = array(
            $prefix . 'id',
            $prefix . 'parent',
            $prefix . 'modify_date',
        );
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        $list = Fenix_Language::getInstance()->getLanguagesList();

        foreach ($attributes AS $_attribute) {

            if ($_attribute->split_by_lang == '1') {
                foreach ($list AS $_langName => $_langOptions) {
                    if ($lang->name == $_langOptions->name) {
                        $result[] = $prefix . $_attribute->sys_title . '_' . $lang->name . ' AS ' . $_attribute->sys_title;
                    }
                }
            } else {
                if ($_attribute->type == 'image') {
                    $result[] = $prefix . $_attribute->sys_title;
                    $result[] = $prefix . $_attribute->sys_title . '_info';
                } else {
                    $result[] = $prefix . $_attribute->sys_title;
                }
            }
        }

        return $result;
    }

    /**
     * Список пользовательских колонк
     * @param string $prefix
     * @return array
     */
    public function productUserColumns($prefix = 'u_p.')
    {
        $attributes = Fenix::getModel('catalog/backend_attributes')->getAttributesList();
        $result = array();
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();

        foreach ($attributes AS $_attribute) {
            if ($_attribute->split_by_lang == '1') {
                $result[] = $prefix . $_attribute->sys_title . '_' . $lang->name . ' AS ' . $_attribute->sys_title;
                foreach (Fenix_Language::getInstance()->getLanguagesList() As $_name => $_options) {
                    $result[] = $prefix . $_attribute->sys_title . '_' . $_name;
                }
            } else {
                if ($_attribute->type == 'image') {
                    $result[] = $prefix . $_attribute->sys_title;
                    $result[] = $prefix . $_attribute->sys_title . '_info';
                } else {
                    $result[] = $prefix . $_attribute->sys_title;
                }
            }
        }

        return $result;
    }

    /**
     * Подготовка запроса на список товаров
     *
     * @param $catalog
     * @param bool $useGroup
     * @param bool $useFilter
     * @return Zend_Db_Select
     */
    public function getProductsListAsSelect($catalog, $useGroup = self::USE_GROUP_ON, $useFilter = self::USE_FILTER_ON)
    {

        $this->setTable('catalog_relations');
        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'r' => $this->getTable()
        ), null);

        // Товары
        $cols = $this->productColumns();

        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'p.id = r.product_id', $cols);

        //Скидки
//        $Select ->joinLeft(array(
//            's' => $this->getTable('sale_discount')
//        ), 'p.id = s.product_id AND NOW() BETWEEN s.sale_begin_date AND s.sale_end_date', array(
//            'sale_discount',
//            'sale_begin_date',
//            'sale_end_date'
//        ));
//Fenix::dump($Select->assemble());

        //Категории
//        if ($catalog == null) {
//            $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);
//
//            $Select ->where('r.left >= ?', (int) $_catalog->left)
//                    ->where('r.right <= ?', (int) $_catalog->right);
//        }
//        else {
//            $Select ->where('r.left >= ?', (int) $catalog->getLeft())
//                    ->where('r.right <= ?', (int) $catalog->getRight());
//        }


        //Фильтр выбранный пользователем
        //if ($useFilter) {
        //Fenix::dump($catalog);
        $Select->where('p.is_public = 1');
        if (Fenix::getRequest()->getParam('filter') || is_null($catalog)) {
            if ($catalog == null) {
                $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);

                $Select->where('r.left >= ?', (int)$_catalog->left)
                    ->where('r.right <= ?', (int)$_catalog->right);
            } else {
                $Select->where('r.left >= ?', (int)$catalog->getLeft())
                    ->where('r.right <= ?', (int)$catalog->getRight());
            }
            $Select->where('r.parent <> ?', Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT);
            Fenix::getModel('catalog/filter')->prepareFilterSelect($Select, array(
                'prefix' => 'p',
                'catalog' => $catalog
            ));
        } else {
            $Select->where('r.parent <> ?', Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT);
            $Select->where('r.parent = ?', $catalog->id);
        }

        //Групируем по p.id что бы убрать дубли изза мультикатегорий
        $Select->group('p.id');
        //Сортируем товары по цене по возростанию (для правильных групп)
        if ($useGroup) {
            $Select->order('p.price asc');
        }

        //Колонки для правильной групировки товаров по "группе" - group_id
        $cols = array('*');
        if ($useGroup) {
            $maxGroupId = $this->getMaxGroupId();
            $cols[] = new Zend_Db_Expr('COUNT(DISTINCT p.id) as products_in_group');
            $cols[] = new Zend_Db_Expr(
                'CASE ' .
                'WHEN p.group_id = 0 ' .
                //'THEN (select max(group_id) FROM ' . $this->getTable('catalog_products') . ') + p.id ' .
                'THEN ' . $maxGroupId . ' + p.id ' .
                'ELSE p.group_id END as product_group_id');
        }

        //Запрос для каталога сгрупированый по "группе" - group_id
        $catalogSelect = $this->select()->setIntegrityCheck(false)
            ->from(array(
                'p' => new Zend_Db_Expr('(' . $Select->assemble() . ')'
                )
            ), $cols);
        //Групировка по просчитаной "группы" - group_id в product_group_id
        if ($useGroup) {
            $catalogSelect->group('product_group_id');
        }

        //Сортировка выбранная пользователем
        switch (Fenix::getRequest()->getQuery('sort')) {
            case 'rating-asc':
                $catalogSelect->order('p.rating_value');
                break;

            case 'rating-desc':
                $Select->order('p.rating_value desc');
                break;

            case 'title_russian-asc':
                $catalogSelect->order('p.title_russian asc');
                break;

            case 'is_top-desc':
                $catalogSelect->order('p.is_top desc');
                break;

            case 'is_new-desc':
                $catalogSelect->order('p.is_new desc');
                break;

            case 'price-asc':
                $catalogSelect->order('p.price asc');
                break;

            case 'price-desc':
                $catalogSelect->order('p.price desc');
                break;

            default:
                //$catalogSelect ->order('p.price asc');
                $catalogSelect->order('p.position desc');
                break;
        }

        return $catalogSelect;
    }

    public function getMaxGroupId()
    {
        $this->setTable('catalog_products');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'p' => $this->getTable()
        ), array('MAX(group_id) as max_group_id'));
        $Select->limit(1);
        $Result = $this->fetchRow($Select);
        if ($Result && $Result->max_group_id) {
            return $Result->max_group_id;
        } else {
            return 0;
        }
    }

    /**
     * Список товаров
     *
     * @param $category
     * @return Zend_Paginator
     */
    public function getProductsList($category)
    {
        Fenix_Debug::log('....... app getProductsList getProductsListAsSelect');
        $Select = $this->getProductsListAsSelect($category);
        Fenix_Debug::log('....... app getProductsList getProductsListAsSelect end');

        Fenix_Debug::log('....... app getProductsList $CountSelect');

        $maxGroupId = $this->getMaxGroupId();
        //Сколько товаров в категории
        $CountSelect = clone $Select;
        $CountSelect->reset(Zend_Db_Select::COLUMNS);
        $CountSelect->columns(
            array(
                new Zend_Db_Expr('COUNT(DISTINCT CASE ' .
                    'WHEN p.group_id = 0 ' .
                    //'THEN (select max(group_id) FROM ' . $this->getTable('catalog_products') . ') + p.id ' .
                    'THEN ' . $maxGroupId . ' + p.id ' .
                    'ELSE p.group_id END) as products_count')
            )
        );
        $CountSelect->reset(Zend_Db_Select::GROUP);
        $CountSelect->reset(Zend_Db_Select::ORDER);
        $CountSelect->limit(1);
        //Fenix::dump($Select->assemble(),$CountSelect->assemble());
        $countResult = $this->fetchRow($CountSelect);
        //Fenix::dump($countResult);
        if ($countResult) {
            $Count = (int)$countResult->products_count;
        } else {
            $Count = 0;
        }

        //Fenix::dump($Count);

        Fenix_Debug::log('....... app getProductsList $CountSelect end');

        Fenix_Debug::log('....... app getProductsList $paginator');
        if (Fenix::getRequest()->getQuery('perPage')) {
            $perPage = Fenix::getRequest()->getQuery('perPage');
        } else {
            $perPage = Fenix::getConfig('catalog_products_per_page') ? Fenix::getConfig('catalog_products_per_page') : self::DEFAULT_PER_PAGE;
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter->setRowCount($Count);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber((int)Fenix::getRequest()->getQuery("page"))
            ->setItemCountPerPage($perPage);
        Fenix_Debug::log('....... app getProductsList $paginator end');

        return $paginator;
    }

    /**
     * Список товаров без пагинатора
     *
     * @param $catalog
     * @return Zend_Paginator
     */
    public function getProductsListAll($catalog)
    {
        $Select = $this->getProductsListAsSelect($catalog);

        return $this->fetchAll($Select);
    }

    /**
     * Список товаров из группы
     *
     * @param $groupId
     * @return Zend_Paginator
     */
    public function getGroupProductsList($groupId, $short = true)
    {
        if ($short) {
            $this->setTable('catalog_products');
            $Select = $this->select();
            $Select->from($this, array('id', 'url_key'));
            $Select->where('group_id = ?', $groupId);
        } else {
            $Select = $this->getProductsListAsSelect(null, self::USE_GROUP_OFF, self::USE_FILTER_OFF);
            $Select->where('p.group_id = ?', $groupId);
        }


        return $this->fetchAll($Select);
    }

    /**
     * Поиск товаров
     *
     * @param $query
     * @return Zend_Paginator;
     */
    public function findProducts($query)
    {
        //Fenix::dump($query);
        $fields = array(
            'p.id_1c',
            'p.sku',
            'p.search_keywords',
            'p.title_russian',
            'p.title_ukrainian'

        );
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        $fields[] = 'p.title_' . $lang->name;

        $match = $this->adapter()->quoteInto('MATCH(' . implode(', ', $fields) . ') AGAINST(?)', $query);

        $like = array();
        foreach ($fields AS $attribute) {
            $like[] = $this->adapter()->quoteInto($attribute . ' LIKE ?', '%' . $query . '%');
        }
        $like = implode(' OR ', $like);

        $cols = $this->productColumns();
        $cols[] = $match . ' AS relevance';

        $Select = $this->setTable('catalog_products')
            ->select()
            ->from(array(
                'p' => $this->getTable()
            ), $cols)
            ->where($match . ' OR ' . $like)
            ->where('p.is_public = ?', '1')
            ->where('p.is_visible = ?', '1');
        $Select->order(new Zend_Db_Expr('p.in_stock = \'1\' desc'));
        $Select->order('relevance desc');

        $perPage = Fenix::getConfig('catalog_products_per_page') ? Fenix::getConfig('catalog_products_per_page') : self::DEFAULT_PER_PAGE;

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter->setRowCount($this->fetchAll($Select)->count());
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber((int)Fenix::getRequest()->getQuery("page"))
            ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Товар по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */

    public function getProductById($id)
    {
        $this->setTable('catalog_products');

        $Select = $this->select()->setIntegrityCheck(false);

        $Select->from(array(
            'p' => $this->_name
        ), $this->productColumns(null));
        $Select->where('p.id = ?', $id)
            ->limit(1);

        //Скидки
        $Select->joinLeft(array(
            's' => $this->getTable('sale_discount')
        ), 'p.id = s.product_id AND NOW() BETWEEN s.sale_begin_date AND s.sale_end_date', array(
            'sale_discount',
            'sale_begin_date',
            'sale_end_date'
        ));

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Товар по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */

    public function getSetProductById($childId, $parent)
    {
        $this->setTable('catalog_products_set');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            '_p' => $this->getTable()
        ), array(
            '_p.price AS set_price',
            '_p.discount AS set_discount'
        ));

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select->where('_p.child_id = ?', (int)$childId);
        $Select->where('_p.product_id = ?', (int)$parent);

        $Select->order('_p.position asc');

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Товар по коду 1с
     *
     * @param $id_1с
     * @return null|Zend_Db_Table_Row_Abstract
     */

    public function getProductById1c($id_1с)
    {
        $this->setTable('catalog_products');

        $Select = $this->select();

        $Select->from($this, $this->productColumns(null));
        $Select->where('id_1c = ?', (string)$id_1с)
            ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getProductBySku($id_1с)
    {
        $this->setTable('catalog_products');

        $Select = $this->select();

        $Select->from($this, $this->productColumns(null));
        $Select->where('sku = ?', (string)$id_1с)
            ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getProductPrice($product)
    {
        if (is_array($product)) {
            $product = (object)$product;
        }

        $price = $product->price;

        return $price;
    }

    public function getPriceInfo($product, $params)
    {

        $basePrice = $product->price;

        //Цена в соответсвии с выбранными атрибутами
        $priceByAttributes = null;
        if (isset($params['attributes']) && count($params['attributes']) > 0) {
            $priceByAttributes = Fenix::getModel('catalog/products_configurable')->getProductPrice($product,
                $params['attributes']);
        }

        $priceByOptions = 0;
        if (isset($params['options']) && count($params['options']) > 0) {
            $priceByOptions = Fenix::getModel('catalog/products_options')->getProductOptionsPrice($product,
                $params['options']);
        }

        if ($priceByAttributes && $priceByAttributes->price > 0) {
            $resultData = $priceByAttributes->toArray();
            $resultData['price'] = $priceByAttributes->price + $priceByOptions;
            $resultData['price_old'] = $priceByAttributes->price_old + $priceByOptions;
        } else {
            $resultData = array(
                'group_sku' => $product->sku,
                'price'     => $basePrice + $priceByOptions,
                'price_old' => $basePrice + $priceByOptions
            );
        }

        if ($product->sale_discount > 0) {
            $resultData['price_old'] = $resultData['price'];
            $resultData['price'] = round($resultData['price'] - $resultData['price'] * $product->sale_discount / 100,
                0);
        }
        $resultPriceInfo = new Fenix_Object(array(
            'data' => $resultData
        ));

        return $resultPriceInfo;
    }

    /**
     * Топ товары
     *
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getTopProducts()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getColumns());
        $Select->where('is_top = ?', '1');
        $Select->limit('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Товары по запросу
     *
     * @param $query Параметр для поиска
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getWhereProduct($query = 'is_new')
    {
        $Select = $this->getProductsListAsSelect(null);
        $Select->where('p.' . $query . ' = ?', '1');
        $Select->limit(10);

        return $this->fetchAll($Select);
    }

    /**
     * Список категорий по родителю
     *
     * @param int $parent Идентификатор родительской категории
     * @return Zend_Db_Table_Rowset
     */
    public function getCategoriesRelations($product)
    {
        $productId = ($product != null ? $product->id : 0);
        $cacheId = 'getCategoriesRelations_' . $productId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getColumns());

        // Джоиним связи
        $Select->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'fe_catalog.id = r.parent AND r.product_id = ' . $productId, null);

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    public function getCategoriesIdList($productId)
    {

        $this->setTable('catalog_relations');

        $list = $this->fetchAll('product_id = ' . (int)$productId);
        $result = array();
        foreach ($list as $_record) {
            $result[$_record->parent] = $_record->parent;
        }

        return $result;
    }
}
