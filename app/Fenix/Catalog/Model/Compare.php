<?php

class Fenix_Catalog_Model_Compare extends Fenix_Resource_Model
{

    public function getCache()
    {
        $cache = new Zend_Session_Namespace('Fenix_Catalog_Compare');
        return $cache;
    }

    public function addProduct($id)
    {
        $cache                = self::getCache();
        $cache->products[$id] = $id;

        //Находим товар и сохраняем его категорию
        $_product = Fenix::getModel('catalog/products')->getProductById($id);
        $_category = Fenix::getModel('catalog/categories')->getCategoryById($_product->parent);
        if($_category->parent!=1){
            $_category = Fenix::getModel('catalog/categories')->getCategoryById($_category->parent);
        }
        if($_product){
            if(isset($cache->categories[$_category->id]))
                $cache->categories[$_category->id][$id]=$id;
            else
                $cache->categories[$_category->id] =array($id=>$id);
        }
    }

    public function deleteProduct($id)
    {
        $cache = self::getCache();

        if (isset($cache->products[$id]))
            unset($cache->products[$id]);

        //Находим товар и удаляем его категорию
        $_product = Fenix::getModel('catalog/products')->getProductById($id);
        $_category = Fenix::getModel('catalog/categories')->getCategoryById($_product->parent);
        if($_category->parent!=1){
            $_category = Fenix::getModel('catalog/categories')->getCategoryById($_category->parent);
        }
        if($_product){
            if(isset($cache->categories[$_category->id]))
                if(isset($cache->categories[$_category->id][$id]))
                    unset($cache->categories[$_category->id][$id]);

            if(count($cache->categories[$_category->id])==0)
                unset($cache->categories[$_category->id]);
        }
    }

  public function deleteCategory($id)
    {
        $cache = self::getCache();

        if(isset($cache->categories[$id])){
            foreach($cache->categories[$id] as $productId){
                unset($cache->products[$productId]);
            }
            unset($cache->categories[$id]);
        }
    }

    public function inCompare($id){
        $cache = self::getCache();
        if (isset($cache->products[$id]))
            return true;
        else
            return false;
    }
    public function getProductList()
    {
        $Select = Fenix::getModel('catalog/products')->getProductsListAsSelect(null);
    }

}