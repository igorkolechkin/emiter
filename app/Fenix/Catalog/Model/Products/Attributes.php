<?php

class Fenix_Catalog_Model_Products_Attributes extends Fenix_Resource_Model
{
    /**
     * Атрибут по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeById($id)
    {
        $cacheId = 'frontend_getAttributeById_' . $id;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getFrontendColumns());
        $Select->where('id = ?', (int) $id);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Атрибут по системному имени
     *
     * @param $sys_title
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeBySysTitle($sys_title)
    {
        $cacheId = 'frontend_getAttributeBySysTitle_' . $sys_title;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }


        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getFrontendColumns());
        $Select->where('sys_title = ?', $sys_title);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Значение атрибута по его атрибуту и ид
     * @param Zend_Db_Table_Row $attribute
     * @param array $value
     * @return mixed|null|Zend_Db_Table_Row_Abstract
     * @throws Zend_Exception
     */
    public function getAttributeValueById(Zend_Db_Table_Row $attribute, $valueId)
    {

        $cacheId = 'frontend_getAttributeValueById_' . $attribute->id . '_' . $valueId;
        if (Zend_Registry::isRegistered($cacheId)) {
            return Zend_Registry::get($cacheId);
        }
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/' . $tableName);

        $Select = $this->setTable($tableName)
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name, $Engine->getColumns());

        $Select->where('id = ?', $valueId);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Характеристики для списка
     *
     * @param $productId
     * @return Fenix_Object_Rowset
     */
    public function getCardList($productId, $category = null)
    {
        $attributesList = $this->_getCachedCardAttributes($category);

        $data = array();
        foreach ($attributesList as $attribute) {
            $values = $this->getAttributeValues($attribute, $productId);

            if ($values->count() > 0) {
                $attrData = array(
                    'id'        => $attribute->id,
                    'title'     => $attribute->title,
                    'sys_title' => $attribute->sys_title,
                    'values'    => $values
                );
                $data[$attribute->id] = $attrData;
            }

        }
        $list = new Fenix_Object_Rowset(array(
            'data' => $data
        ));

        return $list;
    }

    /**
     * Список атрибутов для списка товаров через кеширование
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    private function _getCachedCardAttributes($category = null)
    {
        $cache = Fenix_Cache::getCache('catalog/products_attributes');
        if ($cache !== false) {
            $cacheId = 'front_getCachedCardAttributes';

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->_getCardAttributes($category);
                $cache->save($data, $cacheId);
            }
        }
        else {
            $data = $this->_getCardAttributes($category);
        }

        return $data;
    }

    private function _getCardAttributes($category = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id AND caga.attribute_source = "user"', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        if ($category == null) {
            $rootCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('c.left >= ?', (int) $rootCategory->left)
                ->where('c.right <= ?', (int) $rootCategory->right);
        }
        else {
            $Select->where('c.left >= ?', (int) $category->left)
                ->where('c.right <= ?', (int) $category->right);
        }

        $Select->where('a.is_in_card = ?', '1');
        $Select->group('a.id');
        $Select->order('caga.position asc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }


    /**
     * Характеристики для списка
     *
     * @param $productId
     * @return Fenix_Object_Rowset
     */
    public function getShortCardList($productId)
    {
        $attributesList = $this->_getCachedShortCardAttributes();

        $data = array();
        foreach ($attributesList as $attribute) {
            $values = $this->getAttributeValues($attribute, $productId);

            if ($values->count() > 0) {
                $attrData = array(
                    'id'        => $attribute->id,
                    'title'     => $attribute->title,
                    'sys_title' => $attribute->sys_title,
                    'values'    => $values
                );
                $data[$attribute->id] = $attrData;
            }
        }
        $list = new Fenix_Object_Rowset(array(
            'data' => $data
        ));

        return $list;
    }

    /**
     * Список атрибутов для списка товаров через кеширование
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    private function _getCachedShortCardAttributes()
    {
        $cache = Fenix_Cache::getCache('catalog/products');
        if ($cache !== false) {
            $cacheId = 'front_getCachedShortCardAttributes';

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->_getShortCardAttributes();
                $cache->save($data, $cacheId);
            }
        }
        else {
            $data = $this->_getCardAttributes();
        }

        return $data;
    }

    private function _getShortCardAttributes($category = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id AND caga.attribute_source = "user"', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        if ($category == null) {
            $rootCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('c.left >= ?', (int) $rootCategory->left)
                ->where('c.right <= ?', (int) $rootCategory->right);
        }
        else {
            $Select->where('c.left >= ?', (int) $category->getLeft())
                ->where('c.right <= ?', (int) $category->getRight());
        }

        $Select->where('a.is_in_card_short = ?', '1');
        $Select->group('a.id');
        $Select->order('caga.position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    private function getDataTypes()
    {
        return array(
            'varchar',
            'text',
            'enum',
            'int',
            'double',
        );
    }

    /**
     * Характеристики для списка
     *
     * @param $productId
     * @return Fenix_Object_Rowset
     */
    public function getCategoryList($productId, $category = null)
    {
        $Engine = Fenix_Engine::getInstance()
            ->setSource('catalog/attributes');

        $attrColumns = $Engine->getColumns();
        $attrColumns[] = new Zend_Db_Expr('COUNT(avr.id) as values_count');
        $this->setTable('catalog_attributes');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'a' => $this->_name
        ), $attrColumns);

        $Select->join(array(
            'avr' => $this->getTable('attr_values')
        ), 'a.id = avr.attribute_id', array());

        $types = Fenix::getModel('catalog/backend_attributes')->getDataTypes();

        $langTypes = array();
        $langTypes [] = 'varchar_lang';
        $langTypes [] = 'text_lang';

        $contentColumns = array();
        $this->joinAttributeValues($Select, $contentColumns, $types);
        $this->joinAttributeValues($Select, $contentColumns, $langTypes);
        if ($category) {
            $attributesetId = $category->attributeset_id;
            if ($attributesetId) {
                $Select->join(array(
                    'caga' => $this->getTable('catalog_attributeset_groups_attributes')
                ), 'caga.attribute_id = avr.attribute_id AND caga.attribute_source ="user"', array());
                $Select->where('caga.attribute_source = ?', 'user');
                $Select->order('caga.position asc');
            }
        }

        $Select->group('a.id');

        $Select->where('a.is_in_list = ?', '1');
        $Select->where('avr.product_id = ?', $productId);

        $Select->where(implode(' NOT IN ("","0") OR ', $contentColumns) . ' NOT IN ("","0")');

        $result = $this->fetchAll($Select);

        return $result;

    }

    /**
     * Список атрибутов для списка товаров через кеширование
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    private function _getCachedCategoryAttributes($category = null)
    {
        $cache = Fenix_Cache::getCache('catalog/products');
        if ($cache !== false) {
            $cacheId = 'front_getCachedCategoryAttributes_' . $category->id;

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->_getCategoryAttributes($category);
                $cache->save($data, $cacheId);
            }
        }
        else {
            $data = $this->_getCategoryAttributes($category);
        }

        return $data;
    }

    private function _getCategoryAttributes($category = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'c' => $this->_name
        ), false);

        $Select->join(array(
            'ca' => $this->getTable('catalog_attributeset')
        ), 'c.attributeset_id = ca.id', false);

        $Select->join(array(
            'cag' => $this->getTable('catalog_attributeset_groups')
        ), 'ca.id = cag.parent', false);

        $Select->join(array(
            'caga' => $this->getTable('catalog_attributeset_groups_attributes')
        ), 'cag.id = caga.group_id AND caga.attribute_source = "user"', array('position'));

        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'caga.attribute_id = a.id', $Engine->getColumns(array(
            'prefix' => 'a'
        )));

        if ($category == null) {
            $rootCategory = Fenix::getModel('catalog/categories')->getCategoryById(1);
            $Select->where('c.left >= ?', (int) $rootCategory->left)
                ->where('c.right <= ?', (int) $rootCategory->right);
        }
        else {
            if ($category instanceof Zend_Db_Table_Row) {
                $Select->where('c.left >= ?', (int) $category->left)
                    ->where('c.right <= ?', (int) $category->right);
            }
            else {
                $Select->where('c.left >= ?', (int) $category->getLeft())
                    ->where('c.right <= ?', (int) $category->getRight());
            }
        }

        $Select->where('a.is_in_list = ?', '1');
        $Select->group('a.id');
        $Select->order('caga.position asc');

        $Result = $this->fetchAll($Select);

        return $Result;

    }

    public function getAttributeValue($attribute, $productId = null)
    {
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();


        $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
            ->from(array(
                'avc' => $this->getTable()
            ), $cols)
            ->join(array(
                'avr' => $this->getTable('attr_values')
            ), ' avr.value_id = avc.id');


        if ($productId) {
            $Select->where('avr.product_id = ?', $productId);
        }

        $Select->where('avr.attribute_id = ?', $attribute->id);

        if (in_array($attribute->sql_type, array('INT', 'DOUBLE'))) {
            $Select->having('content <> 0');
        }
        else {
            $Select->having('TRIM(content) <> ""');
        }

        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список значений атрибутов для списка товаров через кеширование
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    private function _getCachedAttributeValues($attribute, $productId = null)
    {
        $cache = Fenix_Cache::getCache('catalog/products_attributes');
        if ($cache !== false) {
            $cacheId = 'front_getCachedAttributeValues_' . $attribute->id . '_' . $productId;

            if ( ! $data = $cache->load($cacheId)) {
                $data = $this->getAttributeValues($attribute, $productId);
                $cache->save($data, $cacheId);
            }
        }
        else {
            $data = $this->getAttributeValues($attribute, $productId);
        }

        return $data;
    }

    public function getAttributeValues($attribute, $productId = null)
    {
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);
        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

        $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
            ->from(array(
                'avc' => $this->getTable()
            ), $cols)
            ->join(array(
                'avr' => $this->getTable('attr_values')
            ), ' avr.value_id = avc.id');

        if ($productId) {
            $Select->where('avr.product_id = ?', $productId);
        }

        $Select->where('avr.attribute_id = ?', $attribute->id);

        if (in_array($attribute->sql_type, array('INT', 'DOUBLE'))) {
            $Select->having('content <> 0');
        }
        else {
            $Select->having('TRIM(content) <> ""');
        }

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Значения аттрибута по url значений один URL или массив URL'ов
     * @param $attribute
     * @param $url
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributeValuesByUrl($attribute, $url)
    {
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getFrontendColumns();
        $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
            ->from(array(
                'avc' => $this->getTable()
            ), $cols)
            ->where('avc.attribute_id =?', $attribute->id);

        if (is_array($url)) {
            $Select->where('avc.url IN ("' . implode('","', $url) . '")');
        }
        else {
            $Select->where('avc.url= ?', $url);
        }

        if ($attribute->split_by_lang == '1') {
            $lang = Fenix_Language::getInstance()->getCurrentLanguage();
            $Select->order('avc.content_' . $lang->name . ' asc');
        }
        else {
            $Select->order('avc.content asc');
        }

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    private function joinAttributeValues(Zend_Db_Select $Select, &$contentColumns, $typeList)
    {
        foreach ($typeList as $dataType) {
            $dataType = strtolower($dataType);
            $tableName = 'attr_values_' . $dataType;
            $prefix = 'avc_' . $dataType;

            $Engine = Fenix_Engine::getInstance()
                ->setSource('catalog/' . $tableName);
            $tableColumns = $Engine->getColumns();

            //Удаляем колонку id таккак после склеивания она будет не коректной
            if (($idIndex = array_search('id', $tableColumns)) !== false) {
                unset($tableColumns[$idIndex]);
            }

            foreach ($tableColumns as $index => $columnName) {

                $asPosition = strpos($columnName, ' AS ');
                if ($asPosition !== false) {
                    $column = substr($columnName, 0, $asPosition);
                    $alias = substr($columnName, $asPosition + 4);
                    if ($alias == 'content') {
                        $tableColumns[$index] =
                            'GROUP_CONCAT(DISTINCT ' . $prefix . '.' . $column . ' ORDER BY ' . $prefix . '.' . $column . ' ASC SEPARATOR ", ")' . ' AS ' . $dataType . '_' . $alias;

                        $contentColumns[] = $prefix . '.' . $column;
                    }
                    else {
                        $tableColumns[$index] = $prefix . '.' . $column . ' AS ' . $dataType . '_' . $alias;
                    }
                }
                else {
                    if ($columnName == 'content') {
                        $tableColumns[$index] =
                            'GROUP_CONCAT(DISTINCT ' . $prefix . '.' . $columnName . ' ORDER BY ' . $prefix . '.' . $columnName . ' ASC SEPARATOR ", ")' . ' AS ' . $dataType . '_' . $columnName;

                        $contentColumns[] = $prefix . '.' . $columnName;
                    }
                    else {
                        $tableColumns[$index] = $prefix . '.' . $columnName . ' AS ' . $dataType . '_' . $columnName;
                    }

                }
            }

            $Select->joinLeft(array(
                $prefix => $this->getTable($tableName)
            ), 'a.id = ' . $prefix . '.attribute_id AND ' . $prefix . '.id = avr.value_id', $tableColumns);
        }
    }
}