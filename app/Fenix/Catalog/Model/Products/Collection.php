<?php
class Fenix_Catalog_Model_Products_Collection extends Fenix_Resource_Model
{
    const DEFAULT_PER_PAGE = 20;
    /**
     * Запрос товаров для товара-коллекции
     * @param $productId
     * @return Zend_Db_Select
     */
    public function getCollectionListAsSelect($productId)
    {
        $this->setTable('catalog_collection_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.child_id = p.id', $this->productColumns());

        $Select ->where('_p.product_id = ?', (int) $productId);

        $Select ->order('_p.position asc');

        return $Select;
    }

    /**
     * Список товаров для товара-коллекция
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCollectionProductsListAll($productId){
        $this->setTable('catalog_products');

        $Select    = Fenix::getModel('catalog/products')->getProductsListAsSelect(null);

        // Джоиним товарчеги
        $Select ->join(array(
            'ccp' => $this->getTable('catalog_collection_products')
        ), 'ccp.child_id = p.id', array());
        $Select ->where('ccp.product_id = ?', (int) $productId);

        $result = $this->fetchAll($Select);

        return $result ;
    }

    public function getCollectionProductsList($productId){

        $this->setTable('catalog_products');

        $Select    = Fenix::getModel('catalog/products')->getProductsListAsSelect(null);
        //$Select ->where('p.id = ?', (int) $productId);
        // Джоиним товарчеги
      $Select ->join(array(
            'ccp' => $this->getTable('catalog_collection_products')
        ), 'ccp.child_id = p.id', array());
        $Select ->where('ccp.product_id = ?', (int) $productId);

        $maxGroupId = Fenix::getModel('catalog/products')->getMaxGroupId();
        //Сколько товаров
        Fenix_Debug::log('....... app getCollectionList $CountSelect begin');
        $CountSelect = clone $Select;
        $CountSelect ->reset(Zend_Db_Select::COLUMNS);
        $CountSelect ->columns(
            array(
                new Zend_Db_Expr('COUNT(DISTINCT CASE ' .
                    'WHEN p.group_id = 0 ' .
                    //'THEN (select max(group_id) FROM ' . $this->getTable('catalog_products') . ') + p.id ' .
                    'THEN ' . $maxGroupId . ' + p.id ' .
                    'ELSE p.group_id END) as products_count')
            )
        );
        $CountSelect ->reset(Zend_Db_Select::GROUP);
        $CountSelect ->reset(Zend_Db_Select::ORDER);
        $CountSelect ->limit(1);

        $countResult = $this->fetchRow($CountSelect);
        if($countResult){
            $Count = (int) $countResult->products_count;
        }else{
            $Count = 0;
        }
        //Fenix::dump($CountSelect->assemble(),$Count);


        Fenix_Debug::log('....... app getCollectionList $CountSelect end');

        Fenix_Debug::log('....... app getCollectionList $paginator');
        $perPage = $this->getPerPage();

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter   ->setRowCount($Count);
        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);
        Fenix_Debug::log('....... app getCollectionList $paginator end');
        return $paginator;
    }

    /**
     * Сколько товаров на страницу
     * @return int
     */
    public function getPerPage()
    {
        if(Fenix::getRequest()->getQuery('perPage'))
            $perPage = Fenix::getRequest()->getQuery('perPage');
        else
            $perPage = Fenix::getConfig('catalog_collection_per_page') ? Fenix::getConfig('catalog_collection_per_page') : self::DEFAULT_PER_PAGE;

        return (int)$perPage;
    }

    /**
     * Запрос количества тооваров в товаре коллекции
     * @param $productId
     * @return Zend_Db_Select
     */
    public function getCount($productId)
    {
        $this->setTable('catalog_collection_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'ccp' => $this->getTable()
        ), array('COUNT(ccp.id) as products_count'));

        // Товары
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'ccp.child_id = p.id', array());

        // Категории
        $Select ->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'p.id = r.product_id', array());

        $Select->where('r.parent <> ?', Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT);

        $Select ->where('p.is_public = ?', '1');
        $Select ->where('ccp.product_id = ?', (int) $productId);
        $Select ->group('ccp.product_id');
        $result = $this->fetchRow($Select);

        if ($result)
            return $result->products_count;
        else
            return 0;
    }

    public function getProductCollection($productId){
        $this->setTable('catalog_collection_products');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'ccp' => $this->getTable()
        ), array());

        // Товары
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'ccp.product_id = p.id', array('url_key'));

        // Категории
        $Select ->join(array(
            'r' => $this->getTable('catalog_relations')
        ), 'p.id = r.product_id', array());

        $Select->where('r.parent <> ?', Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT);

        $Select ->where('p.is_public = ?', '1');
        $Select ->where('ccp.child_id = ?', (int) $productId);
        $Select ->group('ccp.product_id');
        $Select->limit(1);
        $result = $this->fetchRow($Select);

        return $result;
    }
}