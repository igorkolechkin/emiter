<?php
class Fenix_Catalog_Model_Products_Favorites extends Fenix_Resource_Model
{
    public function getCustomerProductsList($customerId)
    {
        $this->setTable('customer_favorites');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_f' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_f.product_id = p.id', Fenix::getModel('catalog/products')->productColumns());

        $Select ->where('_f.customer_id = ?', (int) $customerId);
        $Select ->order('_f.added_date desc');

        return $this->fetchAll($Select);
    }

    /**
     * Обработка товара в списке избранного
     *
     * @param $user
     * @param $id
     */
    public function process($user, $id)
    {
        $test = $this->setTable('customer_favorites')->fetchRow(
            'customer_id = ' . (int) $user . ' AND product_id = ' . (int) $id
        );

        if ($test == null) {
            $this->insert(array(
                'customer_id' => (int) $user,
                'product_id'  => (int) $id,
                'added_date'  => new Zend_Db_Expr('NOW()')
            ));
        }
        else {
            $this->delete(
                'customer_id = ' . (int) $user . ' AND product_id = ' . (int) $id
            );
        }

        return;
    }

    /**
     * Проверка есть ли этот товар в списке избранного клиента
     *
     * @param $userId
     * @param $productId
     * @return bool
     */
    public function isInFavorites($userId, $productId)
    {
        return $this->setTable('customer_favorites')->fetchRow(
            'customer_id = ' . (int) $userId . ' AND product_id = ' . (int) $productId
        ) != null;
    }
}