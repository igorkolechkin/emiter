<?php

class Fenix_Catalog_Model_Products_Configurable extends Fenix_Resource_Model
{

    public function getGroupsListAsSelect($productId = null)
    {
        $this->setTable('product_conf_groups');
        $Select = $this->select()->setIntegrityCheck(false)
            ->from(array(
                'pcg' => $this->_name
            ));
        if ($productId) {
            $Select->where('pcg.product_id = ?', $productId);
        }

        return $Select;
    }

    public function getGroupsList($productId = null)
    {
        $Select = $this->getGroupsListAsSelect($productId);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getGroupValues($groupId, $productId)
    {
        $Select = $this->getGroupsListAsSelect($productId);
        $Select->reset(Zend_Db_Select::COLUMNS);
        $Select->join(array(
            'pca' => $this->getTable('product_conf_values')
        ), 'pca.group_id = pcg.id', array('*'));

        $Select->where('pcg.id = ?', $groupId);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getPriceList($productId)
    {
        $groups = $this->getGroupsList($productId);
        foreach ($groups as $priceGroup) {
            $priceInfo = $priceGroup->toArray();
            $values = $this->getGroupValues($priceGroup->id, $productId);
            $valuesData = array();
            foreach ($values as $value) {
                Fenix::dump($value);
                $valuesData[$value->attribute_id] = (object)array(
                    'attribute_id' => $value->attribute_id,
                    'value_id'     => $value->id
                );
            }
            Fenix::dump($priceInfo);
        }
        Fenix::dump($groups);
    }

    public function getAttributesList($productId)
    {
        $this->setTable('product_conf_values');
        $Select = $this->select()->setIntegrityCheck(false)
            ->from(array(
                'pcv' => $this->getTable()
            ), null);
        $cols = Fenix_Engine::getInstance()->setSource('catalog/attributes_frontend')->getColumns();
        $Select->join(array(
            'a' => $this->getTable('catalog_attributes')
        ), 'a.id = pcv.attribute_id AND ' .
            $this->getAdapter()->quoteInto('pcv.product_id =?', $productId)
            , $cols);

        $Select->group('a.id');
        $attributesList = $this->fetchAll($Select);
        $data = array();

        foreach ($attributesList as $attribute) {
            $valuesList = $this->getAttributeValueList($attribute, $productId);
            $values = array();
            foreach ($valuesList as $value) {
                $values[$value->value_id] = (object)array(
                    'title'    => $value->content,
                    'value_id' => $value->value_id
                );
            }
            $data[$attribute->id] = array(
                'id'                       => $attribute->id,
                'title'                    => $attribute->title,
                'sys_title'                => $attribute->sys_title,
                'is_configurable_material' => $attribute->is_configurable_material,
                'values'                   => $values
            );
        }
        $Result = new Fenix_Object_Rowset(array(
            'data' => $data
        ));

        return $Result;
    }

    public function getAttributeValueList($attribute, $productId)
    {
        $this->setTable('product_conf_values');
        $Select = $this->select()->setIntegrityCheck(false)
            ->from(array(
                'pcv' => $this->getTable()
            ));
        $tableName = Fenix::getModel('catalog/attributes')->getAttributeTable($attribute);

        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getFrontendColumns(array('prefix' => 'avc'));
        $Select->join(array(
            'avc' => $this->getTable($tableName)
        ), 'avc.id = pcv.value_id AND ' .
            $this->getAdapter()->quoteInto('pcv.attribute_id = ?', $attribute->id) . ' AND ' .
            $this->getAdapter()->quoteInto('pcv.product_id = ?', $productId)
            , $cols);
        $Select->group('pcv.attribute_id');
        $Select->group('pcv.value_id');
        $Select->order('avc.position asc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function getProductPrice($product, $values)
    {

        //Fenix::dump($values);
        $this->setTable('product_conf_values');
        $Select = $this->select()->setIntegrityCheck(false)
            ->from(array(
                'pcv' => $this->getTable()
            ), array('group_id', 'COUNT(pcv.attribute_id) as match_attributes'));
        foreach ($values as $attributeId => $valueId) {
            $Select->orWhere(
                $this->getAdapter()->quoteInto('pcv.product_id = ?', $product->id) . ' AND ' .
                $this->getAdapter()->quoteInto('pcv.attribute_id = ?', $attributeId) . ' AND ' .
                $this->getAdapter()->quoteInto('pcv.value_id = ?', $valueId)
            );
        }

        $Select->join(array(
            'pcg' => $this->getTable('product_conf_groups')
        ), 'pcv.group_id = pcg.id AND ' .
            $this->getAdapter()->quoteInto('pcv.product_id = ?', $product->id)
            , array('*'));

        $Select->group('pcv.group_id');
        $Select->having('match_attributes = ?', count($values));
        $Select->limit(1);
        $priceParams = $this->fetchRow($Select);

        //Fenix::dump($values, $Select->assemble(),$priceParams);

        if ($priceParams) {
            $priceInfo = $this->calculateProductPrice($product, $priceParams);
        } else {
            $priceInfo = new Fenix_Object(array(
                'data' => array(
                    'group_sku' => $product->sku,
                    'price_old' => $product->price_old,
                    'price'     => $product->price
                )
            ));
        }

        //Fenix::dump($priceInfo,$priceParams);

        return $priceInfo;
    }

    public function calculateProductPrice($product, $priceParams)
    {

        $priceInfo = $priceParams->toArray();

        switch ($priceParams->type) {
            case '+':
                $priceInfo['price'] = (double)$product->price + $priceParams->price;
                $priceInfo['price_old'] = (double)$priceParams->price_old;
                break;
            case '-':

                $priceInfo['price'] = (double)$product->price - $priceParams->price;
                $priceInfo['price_old'] = (double)$priceParams->price_old;
                break;
            case '=':
            default:
                $priceInfo['price'] = (double)$priceParams->price;
                $priceInfo['price_old'] = (double)$priceParams->price_old;
        }

        $result = new Fenix_Object(array(
            'data' => $priceInfo
        ));

        //Fenix::dump($result);

        return $result;
    }

    public function getConfigurablePrices($productId)
    {
        $confAttributes = Fenix::getModel('catalog/configurable_attributes')->getAttributesList($productId);

        $cols = array();
        foreach ($confAttributes as $attr) {
            $cols[] = 'GROUP_CONCAT(IF(pcv.attribute_id = ' . $attr->id . ', pcv.value_id, null)) AS ' . $attr->sys_title;
            //Fenix::dump($confAttributes, $attr);
        }
        $Select = Fenix::getModel('catalog/configurable_groups')->getGroupsListAsSelect();
        $this->setTable('product_conf_values');
        $Select->join(array(
            'pcv' => $this->getTable('product_conf_values')
        ), 'pcv.group_id = pcg.id AND ' .
            $this->getAdapter()->quoteInto('pcg.product_id = ?', $productId)
            , $cols);

        $Select->group('pcg.id');
        $list = $this->fetchAll($Select);

        $data = array();
        foreach ($list as $item) {

            $key = '';
            foreach ($confAttributes as $attrIndex => $attr) {
                if ($attrIndex > 0) {
                    $key .= '-';
                }
                $key .= $item->{$attr->sys_title};
            }
            $data[$key] = $item;
        }

        return $data;
    }
}