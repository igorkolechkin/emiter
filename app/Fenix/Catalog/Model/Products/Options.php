<?php
class Fenix_Catalog_Model_Products_Options extends Fenix_Resource_Model
{

    /**
     * Одна запись
     *
     * @Id $шd
     * @return Zend_Db_Select
     */
    public function getOptionById($Id)
    {
        $Select = $this->getOptionsListAsSelect();
        $Select ->where('o.id = ?', (int) $Id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }


    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionsListByProductId($productId, $idListFilter = null)
    {
        $Select = $this->getOptionsListAsSelect();

        $Select->join(array(
            'cpr' => $this->getTable('catalog_products_options_relations')
        ), 'o.id = cpr.option_id', array());

        $Select->where('cpr.product_id = ?', $productId);

        if($idListFilter && is_array($idListFilter)){
            $Select->where('o.id IN("' . implode('","', $idListFilter) . '")');
        }

        $Result = $this->fetchAll($Select);

        return $Result;
    }


    /**
     * Список опций
     *
     * @param null $parent
     * @return Zend_Db_Select
     */
    public function getOptionsListAsSelect($parent = null)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/options');

        $this   ->setTable('catalog_products_options');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'o' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'o'
        )));
        if ($parent) {
            $Select->where('o.parent = ?', $parent);
        }

        return $Select;
    }

    public function getProductOptionsPrice($product, $options){
        $price = 0;
        $optionsList = $this->getOptionsListByProductId($product->id, $options);
        foreach($optionsList as $optionProduct){
            $price +=$optionProduct->price;
        }
        return $price;
    }
}