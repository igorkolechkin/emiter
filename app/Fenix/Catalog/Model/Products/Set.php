<?php
class Fenix_Catalog_Model_Products_Set extends Fenix_Resource_Model
{

    public function getSetListAsSelect($parent = null)
    {
        $this->setTable('catalog_set');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'cs' => $this->_name
        ));

        return $Select;
    }

    public function getSetById($setId)
    {

        $Select = $this->getSetListAsSelect();
        $Select ->where('cs.id = ?', (int) $setId);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }


    public function getSetListByProductId($productId)
    {

        $cacheId = 'getSetListByProductId_'.$productId;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);

        $this->setTable('catalog_set');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'set' => $this->_name
        ), Fenix_Engine::getInstance()->setSource('catalog/set')->getColumns());

        $Select->join(array(
            'set_p' => $this->getTable('catalog_set_products')
        ), 'set.id = set_p.set_id', array());

        $Select->where('set_p.product_id = ?',$productId);
        $Select->group('set.id');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    public function getSetBySku($setSku)
    {

        $Select = $this->getSetListAsSelect();
        $Select ->where('cs.sku = ?', $setSku);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getProductsListBySetId($setId, $currentProductId = null)
    {
        $cacheId = 'getProductsListBySetId_'.$setId;
        if (Zend_Registry::isRegistered($cacheId))
            return Zend_Registry::get($cacheId);

        $this->setTable('catalog_set_products');

        $category = Fenix::getModel('catalog/categories')->getCategoryById(1);
        $Select = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect($category);

        $Select->join(array(
            'set_p' => $this->getTable('catalog_set_products')
        ), 'set_p.product_id = p.id', array(
            'set_price',
            'set_discount'
        ));

        //Скидки
        $Select ->joinLeft(array(
            's' => $this->getTable('sale_discount')
        ), 'p.id = s.product_id AND NOW() BETWEEN s.sale_begin_date AND s.sale_end_date', array(
            'sale_discount',
            'sale_begin_date',
            'sale_end_date'
        ));

        $Select ->where('set_p.set_id = ?', (int) $setId);

        if($currentProductId){
            $Select->order(new Zend_Db_Expr('set_p.product_id = \'' . $currentProductId . '\' desc'));
        }

        $Select ->order('set_p.position asc');

        $Result = $this->fetchAll($Select);

        Zend_Registry::set($cacheId, $Result);

        return $Result;
    }

    /**
     * Возвращает стоимость товара с учетом настроек в комплекте
     * @param $product
     * @return float
     */
    public function getSetProductPriceOld($product, $priceInfo = null){
        if($priceInfo)
            return $priceInfo->price_old;

        return $product->price;
    }

    /**
     * Возвращает стоимость товара с учетом настроек в комплекте
     * @param $product
     * @return float
     */
    public function getSetProductPrice($product, $priceInfo = null) {
        if($priceInfo)
            return $priceInfo->price;

        if($product->set_price > 0){
            return $product->set_price;
        } elseif($product->sale_discount > 0) {
            return round($product->price - $product->price * $product->sale_discount/100);
        } elseif($product->set_discount > 0) {
            return round($product->price - $product->price * $product->set_discount/100);
        } else {
            return $product->price;
        }
    }

    /**
     * Возвращает % скидки товара с учетом настроек в комплекте
     * @param $product
     * @return float
     */
    public function getSetProductDiscount($product, $priceInfo = null){
        if($priceInfo)
            return $priceInfo->discount;

        if ($product->sale_discount > 0) {
            return $product->sale_discount;
        } elseif ($product->set_price > 0) {
            return $product->set_discount;
        } elseif ($product->set_price > 0) {
            return round(100 - $product->price / $product->set_price);
        } else {
            return 0;
        }
    }
    /**
     * Возвращает сумму скидки товара с учетом настроек в комплекте
     * @param $product
     * @return float
     */
    public function getSetProductDiscountAmount($product, $priceInfo = null){
        if($priceInfo)
            return $priceInfo->price_old - $priceInfo->price;

        return $product->price - $this->getSetProductPrice($product);
    }

    /**
     * Стоимость комплекта
     * @param $set
     * @return float|int
     */
    public function getSetPrice($set, $priceInfo = null){

        if($set->price >0)
            return $set->price;

        $productsList = $this->getProductsListBySetId($set->id);

        if($set->discount >0){
            $totalPrice    = 0;
            $totalNoDiscountPrice    = 0;

            foreach ($productsList as $product) {
                if ($priceInfo && $product->id == $priceInfo->product_id) {
                    $price    = $this->getSetProductPrice($product, $priceInfo);
                    $priceOld = $this->getSetProductPriceOld($product, $priceInfo);
                }
                else{
                    $price    = $this->getSetProductPrice($product);
                    $priceOld = $this->getSetProductPriceOld($product);
                }


                if ($priceOld > $price) {
                    $totalPrice += $price;
                } else {
                    $totalNoDiscountPrice += $price;
                }
            }

            return round($totalPrice + $totalNoDiscountPrice - $totalNoDiscountPrice * $set->discount / 100, 0);
        }

        $totalPrice    = 0;
        $totalPriceOld = 0;

        foreach ($productsList as $product) {
            if ($priceInfo && $product->id == $priceInfo->product_id) {
                $price    = $this->getSetProductPrice($product, $priceInfo);
                $priceOld = $this->getSetProductPriceOld($product, $priceInfo);
            }
            else{
                $price    = $this->getSetProductPrice($product);
                $priceOld = $this->getSetProductPriceOld($product);
            }

            $totalPrice += $price;
            $totalPriceOld += $priceOld;
        }

        return $totalPrice;
    }

    /**
     * Старая цена комплекта
     * @param $set
     * @return float|int
     */
    public function getSetPriceOld($set){
        $productsList = $this->getProductsListBySetId($set->id);

        $totalPriceOld = 0;

        foreach ($productsList as $product) {
            $totalPriceOld += $this->getSetProductPriceOld($product);
        }

        return $totalPriceOld;
    }

}