<?php
class Fenix_Catalog_Model_Products_Tracking extends Fenix_Resource_Model
{
    public function cron()
    {
        $Select = $this->setTable('customer_tracking')
                       ->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            '_t' => $this->_name
        ), array(
            '_t.id',
            '_t.price AS track_price',
            '_t.price_new AS track_price_new'
        ));

        $Select ->join(array(
            'c' => $this->getTable('customer')
        ), '_t.customer_id = c.id', array(
            'c.email',
            'c.fullname'
        ));

        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_t.product_id = p.id', array(
            'p.title_russian AS product'
        ));

        $Select ->where('_t.price <> price_new')
                ->where('_t.price_new > ?', 0);

        $Result = $this->fetchAll($Select);

        $this->setTable('customer_tracking');

        foreach ($Result AS $_track) {
            $result = Fenix::getModel('core/mail')->sendUserMail(array(
                'to'      => $_track->email,
                'subject' => 'Изменение цены в товаре ' . $_track->product,
                'body'    => 'Цена на отслеживаемый товар изменилась с ' . Fenix::getHelper('catalog/decorator')->decoratePrice($_track->track_price) . ' на ' . Fenix::getHelper('catalog/decorator')->decoratePrice($_track->track_price_new)
            ));

            $this->update(array(
                'price'     => new Zend_Db_Expr('price_new'),
                'price_new' => 0
            ), 'id = ' . (int) $_track->id);
        }
    }

    /**
     * Обработка товара в списке слежки
     *
     * @param $user
     * @param $id
     */
    public function process($user, $id)
    {
        $test = $this->setTable('customer_tracking')->fetchRow(
            'customer_id = ' . (int) $user . ' AND product_id = ' . (int) $id
        );

        if ($test == null) {
            $product = Fenix::getModel('catalog/products')->getProductById($id);
            $price   = Fenix::getModel('catalog/products')->getProductPrice($product);

            $this->insert(array(
                'customer_id' => (int) $user,
                'product_id'  => (int) $id,
                'price'       => (double) $price,
                'price_new'   => 0
            ));
        }
        else {
            $this->delete(
                'customer_id = ' . (int) $user . ' AND product_id = ' . (int) $id
            );
        }

        return;
    }

    /**
     * Проверка есть ли этот товар в списке слежки клиента
     *
     * @param $userId
     * @param $productId
     * @return bool
     */
    public function isTrack($userId, $productId)
    {
        return $this->setTable('customer_tracking')->fetchRow(
            'customer_id = ' . (int) $userId . ' AND product_id = ' . (int) $productId
        ) != null;
    }
}