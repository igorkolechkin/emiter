<?php
class Fenix_Catalog_Controller_New extends Fenix_Controller_Action
{
    /**
     * Новинки товаров страница
     */
    public function indexAction()
    {
        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::lang("Новинки"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'series'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, array(
            'url'         => Fenix::getUrl('catalog/new'),
            'title'       => Fenix::lang("Новинки"),
        ));

        $Creator ->setLayout()
                 ->render('catalog/new.phtml');
    }
}