<?php
class Fenix_Catalog_Controller_Ajax_Filter extends Fenix_Controller_Action
{
    public function setrangeAction()
    {
        $req          = Fenix::getRequest();
        $category     = $req->getPost('category_url');
        $filter       = $req->getPost('filter');
        $attrSysTitle = $req->getPost('attribute_sys_title');
        $attrValue    = $req->getPost('attribute_value');

        $filterString = Fenix::getModel('catalog/filter')->setAttributeRange($filter, $attrSysTitle, $attrValue);

        $data = array(
            'url' => Fenix::getUrl($category . '/filter/' . $filterString)
        );
        echo json_encode($data);
    }
}