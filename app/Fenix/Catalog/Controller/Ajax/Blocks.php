<?php
class Fenix_Catalog_Controller_Ajax_Blocks extends Fenix_Controller_Action
{

    public function hoverAction(){

        $productId  = Fenix::getRequest()->getParam('product_id');

        $Creator = Fenix::getCreatorUI();
        $Creator->assign(array(
            'productId'  => $productId,
        ));
        $Creator->render('catalog/products/ajax/block-hover.phtml');
    }
    public function groupsAction(){

        header('Cache-Control: max-age=3600');
        $productId    = Fenix::getRequest()->getParam('product_id');
        $attrSysTitle = Fenix::getRequest()->getParam('attr_sys_title');

       /* if (Fenix::isDev()){

            Fenix_Debug::log('cache render_groups begin');

            $cache = Fenix_Cache::getCache('core/render');
            $lang = Fenix_Language::getInstance()->getCurrentLanguage();
            if (Fenix::getRequest()->getAccessLevel() == 'backend') {
                $cache = false;
            }
            $load = true;
            if ($cache !== false) {
                $cacheId = md5('Render_groups_' . $productId . '_' . $lang->name);
                if (!$data = $cache->load($cacheId)) {
                    $data  = $this->_cachedGroups($productId,$attrSysTitle);
                    $cache ->save($data, $cacheId);
                    $load = false;
                }
            }
            else {
                $data  = $this->_cachedGroups($productId,$attrSysTitle);
                $load = false;
            }

            echo $data;
        }else {*/
            $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attrSysTitle);
            if ($attribute == null)
                //throw new Exception('Атрибут "' . $attrSysTitle . '" не найден');
                exit;

            $product = Fenix::getModel('catalog/products')->getProductById($productId);
            if ($product == null)
                //throw new Exception('Товар ID: "' . $productId . '" не найден');
                exit;

            $Creator = Fenix::getCreatorUI();
            $Creator->assign(array(
                'productId'  => $productId,
                'attribute'  => $attribute,
                'product'    => $product,
            ));

            $Creator->render('catalog/products/ajax/block-groups.phtml');
       // }

    }

    private function _cachedGroups($productId, $attrSysTitle)
    {
        $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attrSysTitle);
        if ($attribute == null)
            //throw new Exception('Атрибут "' . $attrSysTitle . '" не найден');
            exit;
        $product = Fenix::getModel('catalog/products')->getProductById($productId);
        if ($product == null)
            //throw new Exception('Товар ID: "' . $productId . '" не найден');
            exit;
        $Creator = Fenix::getCreatorUI();
        $Creator->assign(array(
            'productId' => $productId,
            'attribute' => $attribute,
            'product'   => $product,
        ));
        return $Creator->getView()->render('catalog/products/ajax/block-groups.phtml');
    }
}