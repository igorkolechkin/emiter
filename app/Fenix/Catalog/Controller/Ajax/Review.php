<?php

class Fenix_Catalog_Controller_Ajax_Review extends Fenix_Controller_Action
{
    public function addAction()
    {
        $product = Fenix::getModel('catalog/products')->getProductById(Fenix::getRequest()->getPost('product_id'));

        if ($product && $this->getRequest()->isPost()) {

            $productCollection = Fenix::getCollection('catalog/products_product')->setProduct(
                $product->toArray()
            );

            Fenix::getModel('catalog/reviews')->addReview(
                $productCollection,
                $this->getRequest()
            );
            $Event = Fenix::getCreatorUI()
                ->loadPlugin('Events')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Отзыв добавлен. После проверки модератором он появится на этой странице"));

            echo $Event->fetch();
        }
    }
}