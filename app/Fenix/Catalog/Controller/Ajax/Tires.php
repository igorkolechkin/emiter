<?php
class Fenix_Catalog_Controller_Ajax_Tires extends Fenix_Controller_Action
{
    public function indexAction()
    {
        Fenix::dump('index');
    }

    public function carAction()
    {

        $vendor = base64_decode($this->getRequest()->getParam('vendor'));
        $cache         = Fenix::getModel('catalog/tires')->getCache();
        $cache->vendor = $vendor;

        if ($vendor) {
            $list = $productsList = Fenix::getModel('catalog/tires')->getCarsList(
                $vendor
            );

            $data = array();
            $data[] = array(
                'option' => 'Выберите модель',
                'value'  => ''
            );
            foreach ($list AS $item) {
                $data[] = array(
                    'option' => $item->car,
                    'value'  => base64_encode($item->car)
                );
            }
        }else {
            $data = array();
            $data[] = array(
                'option' => '&nbsp;',
                'value'  => ''
            );
        }

        echo Zend_Json::encode($data);
        exit;
    }

    public function yearAction()
    {
        $car = base64_decode($this->getRequest()->getParam('car'));
        $cache      = Fenix::getModel('catalog/tires')->getCache();
        $cache->car = $car;

        if ($car) {
            $list = $productsList = Fenix::getModel('catalog/tires')->getYearsList(
                $car
            );

            $data = array();
            $data[] = array(
                'option' => 'Выберите год',
                'value'  => ''
            );
            foreach ($list AS $item) {
                $data[] = array(
                    'option' => $item->year,
                    'value'  => $item->year
                );
            }
        } else {
            $data   = array();
            $data[] = array(
                'option' => '&nbsp;',
                'value'  => ''
            );
        }
        echo Zend_Json::encode($data);
        exit;
    }
    public function modificationAction(){

        $year = $this->getRequest()->getParam('year');
        $cache       = Fenix::getModel('catalog/tires')->getCache();
        $cache->year = $year;

        if ($year) {
            $list = $productsList = Fenix::getModel('catalog/tires')->getModificationList(
                $year
            );
            $data = array();
            $data[] = array(
                'option' => 'Выберите модификацию',
                'value'  => ''
            );
            foreach ($list AS $item) {
                $data[] = array(
                    'option' => $item->modification,
                    'value'  => base64_encode($item->modification)
                );
            }
        } else {
            $data   = array();
            $data[] = array(
                'option' => '&nbsp;',
                'value'  => ''
            );
        }
//        echo $cache->vendor . ' ' . $cache->car . ' ' . $cache->year . ' ' . $cache->modification;
        echo Zend_Json::encode($data);
        exit;
    }

    public function prepareAction(){

        $modification = base64_decode($this->getRequest()->getParam('modification'));

        $cache = Fenix::getModel('catalog/tires')->getCache();
        $cache->modification = $modification;

        $filterData =  Fenix::getModel('catalog/tires')->prepareFilter();

        $data = array('result' => 'ok');
        //echo $cache->vendor . ' ' . $cache->car . ' ' . $cache->year . ' ' . $cache->modification;
        echo Zend_Json::encode($data);
        exit;
    }
    public function filterAction(){

        $modification = base64_decode($this->getRequest()->getParam('modification'));

        $cache = Fenix::getModel('catalog/tires')->getCache();
        $cache->modification = $modification;

        $filterData =  Fenix::getModel('catalog/tires')->prepareFilter();

        $data = array('result' => 'ok');

        echo Zend_Json::encode($data);
        exit;
    }
}