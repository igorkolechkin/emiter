<?php
class Fenix_Catalog_Controller_Ajax_Configurable extends Fenix_Controller_Action
{
    public function indexAction(){

    }
    public function setsliderAction(){


        $productId    = Fenix::getRequest()->getPost('product_id');

        $materials  = Fenix::getRequest()->getPost('materials');
        $attributes = Fenix::getRequest()->getPost('attributes');
        $options    = Fenix::getRequest()->getPost('options');

        $params = array(
            'materials'  => $materials,
            'attributes' => $attributes,
            'options'    => $options
        );
        $productRow = Fenix::getModel('catalog/products')->getProductById($productId);
        if($productRow == null){
            throw new Exception('Товар не существует');
        }

        $priceInfo = Fenix::getModel('catalog/products')->getPriceInfo($productRow, $params);


        //Товар
        $product   = Fenix::getCollection('catalog/products_product')->setProduct(
            $productRow->toArray(), $priceInfo
        );

        $Creator = Fenix::getCreatorUI();
        $Creator->assign('priceInfo', $priceInfo);
        $Creator->assign('current', $product);
        $Creator->assign(array(
            'product'    => $product,
            'productRow' => $productRow,
            'blockId'    => 'productSetSlider'
        ));
        $Creator->render('catalog/products/set-slider.phtml');
    }
    public function setlistAction(){


        $productId    = Fenix::getRequest()->getPost('product_id');

        $materials  = Fenix::getRequest()->getPost('materials');
        $attributes = Fenix::getRequest()->getPost('attributes');
        $options    = Fenix::getRequest()->getPost('options');

        $params = array(
            'materials'  => $materials,
            'attributes' => $attributes,
            'options'    => $options
        );
        $productRow = Fenix::getModel('catalog/products')->getProductById($productId);
        if($productRow == null){
            throw new Exception('Товар не существует');
        }

        $priceInfo = Fenix::getModel('catalog/products')->getPriceInfo($productRow, $params);


        //Товар
        $product   = Fenix::getCollection('catalog/products_product')->setProduct(
            $productRow->toArray(), $priceInfo
        );

        $Creator = Fenix::getCreatorUI();
        $Creator->assign('priceInfo', $priceInfo);
        $Creator->assign('current', $product);
        $Creator->assign(array(
            'product'    => $product,
            'productRow' => $productRow,
            'blockId'    => 'productSetSlider'
        ));
        $Creator->render('catalog/products/set-list.phtml');
    }
}