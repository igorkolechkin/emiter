<?php
class Fenix_Catalog_Controller_Ajax_Card extends Fenix_Controller_Action
{

    public function grouprewievAction(){
        $productId = Fenix::getRequest()->getPost('productId');
        $product = Fenix::getModel('catalog/products')->getProductById($productId);
        $product = Fenix::getCollection('catalog/products_product')->setProduct(
            $product->toArray()
        );
        $Creator = Fenix::getCreatorUI();

        $Creator->assign(array(
            'product'  => $product,
        ));

        echo $Creator->getView()->render('catalog/products/reviews/list.phtml');
    }

    public function conflistAction(){
        $productId = Fenix::getRequest()->getPost('productId');
        $product = Fenix::getModel('catalog/products')->getProductById($productId);
        $product = Fenix::getCollection('catalog/products_product')->setProduct(
            $product->toArray()
        );
        $group_products = Fenix::getModel('catalog/products')->getGroupProductsList($product->getGroupId());
        if($group_products->count()>1){
            $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle('sleeping_area');
            $opt = '<div class="title">Размер спального места:</div>';
            $opt.='<select name="sleeparea" id="" class="sleeparea">';
            foreach ($group_products as $i=>$group_product){
                $values = Fenix::getModel('catalog/products_attributes')->getAttributeValues($attribute, $group_product->id);
                foreach ($values as $value){
                    $select = ($product->url_key==$group_product->url_key) ? "selected":"";
                    $url = Fenix::getUrl($group_product->url_key);
                    $cont  =($value->content!="") ? $value->content: "Нет";
                    $opt.='<option '. $select .' value="'.$url.'">'.$cont.'</option>';
                }
            }
            $opt.=' </select>';
        }
        //Fenix::dump($opt);
        if(isset($opt)){
            echo $opt;
        }

    }
    public function characteristicsAction(){
        $productId  = Fenix::getRequest()->getPost('productId');
        $categoryId = Fenix::getRequest()->getPost('categoryId') ? Fenix::getRequest()->getPost('categoryId') : 0;

        $category   = Fenix::getModel('catalog/categories')->getCategoryById($categoryId);
        $attributes = Fenix::getModel('catalog/products_attributes')->getCardList($productId, $category);
        $i=0;
        $con = '<div class="block-title">Характеристики:</div>';
        $con .= '<ul>';
        foreach($attributes as $key=>$attribute){
            $i++;
            $values = '';
            foreach($attribute->values as $n=>$value){
                $values .= $value->content;
                if(($n+1)<$attribute->values->count()){
                    $values .= ', ';
                }
            }
            if($i % 2 == 0){
                $cl = 'class="second"';
            }
            else{
                $cl = '';
            }
            $con.='<li '.$cl.'><span class="title">'.$attribute->title.'</span> <span class="value">'.$values.'</span> </li>';
        }
        $con.='</ul>';
        echo $con;
    }
}