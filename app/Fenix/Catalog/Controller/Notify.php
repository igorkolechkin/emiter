<?php
class Fenix_Catalog_Controller_Notify extends Fenix_Controller_Action
{
    public function indexAction()
    {}

    public function processAction()
    {
        $product = Fenix::getModel('catalog/products')->getProductById(
            (int) $this->getRequest()->getPost('pid')
        );

        $_data = array(
            'product'   => $product,
            'email'     => $this->getRequest()->getPost('email'),
            'cellphone' => $this->getRequest()->getPost('cellphone')
        );

        Fenix::getModel('core/mail')->sendAdminMail(array(
            'from'     => $this->getRequest()->getPost('email'),
            'template' => 'catalog.notify', // шаблон письма
            'assign'   => $_data,
        ));

        $res['success_msg'] = Fenix::getCreatorUI()
                                   ->loadPlugin('Events')
                                   ->appendClass('alert-success')
                                   ->setMessage('Спасибо. Наш менеджер уведомит Вас о начилии товара.')
                                   ->fetch();

        $res['success_msg'] = $this->view->partial('catalog/products/stock-notify.phtml');

        echo Zend_Json::encode($res);
    }
}