<?php

class Fenix_Catalog_Controller_Reviews extends Fenix_Controller_Action
{
    public function indexAction()
    {

        $product = Fenix::getModel('catalog/products')->getProductById(Fenix::getRequest()->getPost('product_id'));

        if ($product && $this->getRequest()->isPost()) {
            $productCollection = Fenix::getCollection('catalog/products_product')->setProduct(
                $product->toArray()
            );

            $Form = Fenix::gethelper('catalog/reviews')->getReviewForm($productCollection->getId());
            if ($Form->ok()) {
                Fenix::getModel('catalog/reviews')->addReview(
                    $productCollection,
                    $Form->getRequest()
                );

                $Event = Fenix::getCreatorUI()
                    ->loadPlugin('Events')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Отзыв добавлен. После проверки модератором он появится на этой странице"));
                //$Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch()
                echo $Event->fetch();
                exit;
            }
            else {
                echo $Form->fetch();
                exit;
            }

        }
    }

    public function responseAction()
    {

        $product = Fenix::getModel('catalog/products')->getProductById(Fenix::getRequest()->getPost('product_id'));

        $review = Fenix::getModel('catalog/reviews')->getReviewById(Fenix::getRequest()->getPost('parent'));

        if ($product && $review && $this->getRequest()->isPost()) {
            $productCollection = Fenix::getCollection('catalog/products_product')->setProduct(
                $product->toArray()
            );


            $Form = Fenix::gethelper('catalog/reviews')->getResponseForm($productCollection, $review);
            if ($Form->ok()) {
                Fenix::getModel('catalog/reviews')->addReview(
                    $productCollection,
                    $Form->getRequest()
                );

                $Event = Fenix::getCreatorUI()
                    ->loadPlugin('Events')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Ответ добавлен. После проверки модератором он появится на этой странице"));
                //$Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch()
                echo $Event->fetch();
                exit;
            }
            else {
                echo $Form->fetch();
                exit;
            }

        }
    }
}