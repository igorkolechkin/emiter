<?php
class Fenix_Catalog_Controller_Admin_Options extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/options')
                ->prepare()
                ->execute();


        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/options_relations')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        if ($this->getRequest()->getParam('parent') == null) {
            Fenix::redirect('catalog/options/parent/1');
        }


        /*if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['productsList'])) {
                foreach ((array) $rows['productsList'] AS $_rowId) {
                    $currentProduct  = Fenix::getModel('catalog/backend_products')->getProductById(
                        $_rowId
                    );
                    Fenix::getModel('catalog/backend_products')->deleteProduct($currentProduct);
                }

                Fenix::redirect('catalog/products/parent/' . $this->getRequest()->getParam('parent'));
            }
        }*/

        $parentId       = (int) $this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);
        $productsList   = Fenix::getModel('catalog/backend_options')->getOptionsListAsSelect($parentId);
        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(

                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Добавить товар"))
                                         ->appendClass('btn-primary')
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/options/add/parent/' . $parentId) . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Управление опциями"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/options/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/options/parent/' . $_catalog->id)
            );
        }
        $this->_helper->BreadCrumbs($_crumb);

        $jTree = $Creator->loadPlugin('JTree');
        $jTree ->setId('structure_tree')
               ->setPlugins(array('themes', 'json_data', 'ui', 'unique'))
               ->setTable('catalog')
               ->setUrl(Fenix::getUrl('catalog/options/parent/{$node->id}'))
               ->setEditUrl(Fenix::getUrl('catalog/options/parent/{$node->id}'))
               ->setInitOpen($parentId);

        // Таблица
        $Table    = $Creator->loadPlugin('Table_Db_Generator')
                            ->setTablename('catalog_products')
                            ->setTableId('productsList')
                            ->setTitle(Fenix::lang("Управление опциями") . ($parentCategory->id > 1 ? ' категории "' . $parentCategory->title_russian . '"' : null))
                            ->setData($productsList)
                            ->setCheckall()
                            ->setStandartButtonset()
                            ->setCellCallback('image', function($value, $data, $column, $table){
                                if ($data->image == null)
                                    return;

                                //$info     = (object)unserialize($data->image_info);
                                $imageUrl = HOME_DIR_URL.$data->image;

                                return '<img src="' . $imageUrl . '" width="50" alt="" />';
                            })
                            /*->setCellCallback('create_date', function($value, $data, $column, $table){
                                return Fenix::getDate($value)->format('d.m.Y H:i:s');
                            })
                            ->setCellCallback('modify_date', function($value, $data, $column, $table){
                                if ($value != '0000-00-00 00:00:00')
                                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                                return;
                            })*/;

        $config = Fenix::getStaticConfig();

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/options/edit/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type' => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'  => Fenix::getUrl('catalog/options/delete/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Управление опции") . ($parentCategory->id > 1 ? ' категории "' . $parentCategory->title_russian . '"' : null));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
        ), array(
            $Creator->loadPlugin('Block')->setTitle(Fenix::lang("Дерево категорий"))->setContent($jTree->fetch())
        ), array(
            $Table->fetch('catalog/options')
        ));
    }

    public function addAction()
    {
        $parent = Fenix::getRequest()->getParam('parent');
        if ($parent == null) $parent = 0;
        $this->getRequest()->setPost('parent', $parent);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категории тканей"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Новый блок"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('catalog/options', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Создано"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/options/parent/' . $parent);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/options/add/parent/' . $parent);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/options/edit/id/' . $id . '/parent/' . $parent);
            }
            
            Fenix::redirect('catalog/options/parent/' . $parent);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentOption = Fenix::getModel('catalog/backend_options')->getOptionById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentOption == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Категории тканей не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/options');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление опциями"),
                'uri'   => Fenix::getUrl('catalog/options'),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Изменить"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $parent = Fenix::getRequest()->getParam('parent');
        if ($parent == null) $parent = 0;
        $this->getRequest()->setPost('parent', $parent);


        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentOption->toArray())
                   ->setData('current', $currentOption);
        
        // Источник
        $Form      ->setSource('catalog/options', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentOption, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Категории тканей отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/options');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/options/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/options/edit/id/' . $id);
            }
            
            Fenix::redirect('catalog/options');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentMaterial = Fenix::getModel('catalog/backend_options')->getOptionById(
            $this->getRequest()->getParam('id')
        );

        if ($currentMaterial == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Опция не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/options');
        }

        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('catalog/options', 'default')
                ->deleteRecord($currentMaterial);


        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Опция тканей удалён"))
                 ->saveSession();
            
        Fenix::redirect('catalog/options');
    }
}