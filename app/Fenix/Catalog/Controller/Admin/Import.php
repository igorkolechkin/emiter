<?php

class Fenix_Catalog_Controller_Admin_Import extends Fenix_Controller_Action
{
    public function indexAction()
    {
        /*
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(/*$Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Синхронизировать"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/sync/begin') . '\'')
                    ->fetch()*/
        ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Импорт товаров"))
            ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Импорт товаров"),
            'uri'   => Fenix::getUrl('catalog/sync'),
            'id'    => 'sync'
        );

        $this->_helper->BreadCrumbs($_crumb);

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Импорт товаров"));

        //Форма импорта
        $Form = Fenix::getHelper('catalog/backend_import')->getImportProductsForm();

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
            $Form->fetch()
        ));
    }

    public function beginAction()
    {
        Fenix::getModel('catalog/sync')->process(
            $this->getRequest(),
            $this->getResponse()
        );

        $Creator = Fenix::getCreatorUI();
        $Creator->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Данные синхронизированы"))
            ->saveSession();

        Fenix::redirect('catalog/sync');
    }

    public function productsAction()
    {
        //Форма импорта
        $Form = Fenix::getHelper('catalog/backend_import')->getImportProductsForm();
        $import_file = Fenix::getRequest()->getFiles('import_file');
        if ($import_file && $import_file->error == 0) {
            $result = Fenix::getModel('catalog/import')->importProducts($import_file, $Form->getRequest()->getPost('parent'));
            $Creator = Fenix::getCreatorUI();
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Данные синхронизированы. Новых товаров: " . $result->new . ', Обновление: ' . $result->update))
                ->saveSession();

            Fenix::redirect('catalog/import');
        }

    }

    public function priceAction()
    {
        /*
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array());

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Синхронизация цен"))
            ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Синхронизация цен"),
            'uri'   => Fenix::getUrl('catalog/sync'),
            'id'    => 'sync'
        );
        $this->_helper->BreadCrumbs($_crumb);

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Синхронизация цен"));

        //Форма импорта
        $Form = Fenix::getHelper('catalog/backend_import')->getImportPriceForm();

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
            $Form->fetch()
        ));
    }

    public function pricebeginAction()
    {
        //Форма импорта
        $Form = Fenix::getHelper('catalog/backend_import')->getImportPriceForm();
        $import_file = Fenix::getRequest()->getFiles('import_file');

        if ($import_file && $import_file->error == 0) {
            $result = Fenix::getModel('catalog/import')->importPrices($import_file, $Form->getRequest()->getPost('parent'));
            $Creator = Fenix::getCreatorUI();
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Данные синхронизированы. Обновление: " . $result->update))
                ->saveSession();

            Fenix::redirect('catalog/import/price');
        }

    }
}