<?php
class Fenix_Catalog_Controller_Admin_Coupons extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/coupons')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $couponsList = Fenix::getModel('catalog/backend_coupons')->getCouponsListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Пакетное добавление купонов"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/coupons/many') . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый купон"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/coupons/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Купоны"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Купоны"),
                'uri'   => Fenix::getUrl('catalog/coupons'),
                'id'    => 'seo'
            )
        ));
        
        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('couponsList')
                           ->setTitle(Fenix::lang("Купоны"))
                           ->setData($couponsList)
                           ->setStandartButtonset();
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/coupons/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/coupons/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Купоны"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('catalog/coupons')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Купоны"),
                'uri'   => Fenix::getUrl('catalog/coupons'),
                'id'    => 'seo'
            ),
            array(
                'label' => Fenix::lang("Новый купон"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('catalog/coupons', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = Fenix::getModel('catalog/backend_coupons')->addCoupon(
                $Form,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Купон создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/coupons');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/coupons/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/coupons/edit/id/' . $id);
            }
            
            Fenix::redirect('catalog/coupons');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый Купон"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function manyAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Купоны"),
                'uri'   => Fenix::getUrl('catalog/coupons'),
                'id'    => 'seo'
            ),
            array(
                'label' => Fenix::lang("Пакетное добавление купонов"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('catalog/coupons', 'many')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $id = Fenix::getModel('catalog/backend_coupons')->addManyCoupon(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Купоны созданы"))
                    ->saveSession();

            Fenix::redirect('catalog/coupons');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Пакетное добавление купонов"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentCoupons = Fenix::getModel('catalog/backend_coupons')->getCouponById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentCoupons == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Купон не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/coupons');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Купоны"),
                'uri'   => Fenix::getUrl('catalog/coupons'),
                'id'    => 'coupons'
            ),
            array(
                'label' => Fenix::lang("Редактировать Купон"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentCoupons->toArray())
                   ->setData('current', $currentCoupons);
        
        // Источник
        $Form      ->setSource('catalog/coupons', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = Fenix::getModel('catalog/backend_coupons')->editCoupon(
                $Form,
                $currentCoupons,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Купон отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/coupons');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/coupons/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/coupons/edit/id/' . $id);
            }
            
            Fenix::redirect('catalog/coupons');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать Купон"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentCoupons = Fenix::getModel('catalog/backend_coupons')->getCouponById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCoupons == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Купон не найден"))
                ->saveSession();

            Fenix::redirect('catalog/coupons');
        }

        $Creator = Fenix::getCreatorUI();

        $Form    = $Creator->loadPlugin('Form_Generator')
                           ->setSource('catalog/coupons', 'default');

        Fenix::getModel('catalog/backend_coupons')->deleteCoupon(
            $Form,
            $currentCoupons
        );

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Купон удалён"))
                 ->saveSession();
            
        Fenix::redirect('catalog/coupons');
    }
}