<?php
class Fenix_Catalog_Controller_Admin_Promo extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/promo')
                ->prepare()
                ->execute();
        $this->_engine = $Engine;

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/promo_products')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $promoList = Fenix::getModel('catalog/backend_promo')->getPromoListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('catalog/promo/add/attributeset/{$attributeset}')
        )));
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый блок"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('catalog/promo/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Промоблоки"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Промоблоки"),
                'uri'   => Fenix::getUrl('catalog/promo'),
                'id'    => 'seo'
            )
        ));
        
        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('promoList')
                           ->setTitle(Fenix::lang("Промоблоки"))
                           ->setData($promoList)
                           ->setStandartButtonset();
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/promo/edit/id/{$data->id}/attributeset/{$data->attributeset}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/promo/delete/id/{$data->id}/attributeset/{$data->attributeset}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Промоблоки"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Dialog->fetch(),
                     $Table->fetch('catalog/promo')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Промоблоки"),
                'uri'   => Fenix::getUrl('catalog/promo'),
                'id'    => 'seo'
            ),
            array(
                'label' => Fenix::lang("Новый промоблок"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('catalog/promo', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);

            $id = Fenix::getModel('catalog/backend_promo')->addBlock(
                $Form,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Промоблок создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/promo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/promo/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/promo/edit/attributeset/' . $attributeset.'/id/' . $id);
            }
            
            Fenix::redirect('catalog/promo');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый промоблок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentPromo = Fenix::getModel('catalog/backend_promo')->getBlockById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentPromo == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Промоблок не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/promo');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Промоблоки"),
                'uri'   => Fenix::getUrl('catalog/promo'),
                'id'    => 'promo'
            ),
            array(
                'label' => Fenix::lang("Редактировать промоблок"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        list($lastDate, $lastTime) = explode(' ', $currentPromo->last_date);

        $Defaults = $currentPromo->toArray();

        $Defaults['last_date'] = $lastDate;
        $Defaults['last_time'] = $lastTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentPromo);
        
        // Источник
        $Form      ->setSource('catalog/promo', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = Fenix::getModel('catalog/backend_promo')->editBlock(
                $Form,
                $currentPromo,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Промоблок отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/promo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/promo/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/promo/edit/attributeset/' . $attributeset.'/id/' . $id);
            }
            
            Fenix::redirect('catalog/promo');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать промоблок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentPromo = Fenix::getModel('catalog/backend_promo')->getBlockById(
            $this->getRequest()->getParam('id')
        );

        if ($currentPromo == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Промоблок не найден"))
                ->saveSession();

            Fenix::redirect('catalog/promo');
        }

        $Creator = Fenix::getCreatorUI();

        $Form    = $Creator->loadPlugin('Form_Generator')
                           ->setSource('catalog/promo', 'default');

        Fenix::getModel('catalog/backend_promo')->deleteBlock(
            $Form,
            $currentPromo
        );

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Промоблок удалён"))
                 ->saveSession();
            
        Fenix::redirect('catalog/promo');
    }
}