<?php
class Fenix_Catalog_Controller_Admin_Products extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/configurable')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/attr_values')
               ->prepare()
               ->execute();

        $dataTypes = array(
            'double',
            'enum',
            'int',
            'text',
            'text_lang',
            'varchar',
            'varchar_lang',
            'longtext',
            'longtext_lang'
        );
        foreach($dataTypes as $type) {
            $Engine = new Fenix_Engine_Database();
            $Engine->setDatabaseTemplate('catalog/attr_values_' . $type)
                   ->prepare()
                   ->execute();
        }

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/product_conf_attr')
               ->prepare()
               ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/product_conf_groups')
               ->prepare()
               ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/product_conf_values')
               ->prepare()
               ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/options')
               ->prepare()
               ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/reviews')
               ->prepare()
               ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/attr_image_info')
            ->prepare()
            ->execute();
    }

    public function indexAction()
    {
        if ($this->getRequest()->getParam('parent') == null) {
            Fenix::redirect('catalog/products/parent/1');
        }

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['productsList'])) {
                $productsIDs = array();
                // TODO множественное удаление товаров
                /*foreach ((array) $rows['productsList'] AS $_rowId) {
                    $productsIDs[$_rowId] = $_rowId;
                }

                if(!empty($productsIDs) && count($productsIDs) > 0){
                    Fenix::getModel('catalog/backend_products')->deleteProducts($productsIDs);

                    print '<meta http-equiv="Refresh" content="0">';
                    print 'Подождите...';
                    exit;
                }*/

                Fenix::redirect('catalog/products/parent/' . $this->getRequest()->getParam('parent'));
            }
        }

        $parentId       = (int) $this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);
        $productsList   = Fenix::getModel('catalog/backend_products')->getProductsListAsSelect($parentCategory);

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Удалить все товары в категории"))
                                         ->appendClass('btn-danger')
                                         ->setType('button')
                                         ->setOnclick('if(confirm(\'Удалить все товары в категории?\')) self.location.href=\'' . Fenix::getUrl('catalog/products/clear/parent/' . $parentId) . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                     ->setValue(Fenix::lang("Заполнить пустые описания товаров"))
                                     ->appendClass('btn-danger')
                                     ->setType('button')
                                     ->setOnclick('if(confirm(\'Заполнить пустые описания товаров?\')) self.location.href=\'' . Fenix::getUrl('catalog/products/updateDescriptions/parent/' . $parentId) . '\'')
                                     ->fetch(),
                                 $Creator->loadPlugin('Button')
                                     ->setValue(Fenix::lang("Заполнить описания товаров"))
                                     ->appendClass('btn-danger')
                                     ->setType('button')
                                     ->setOnclick('if(confirm(\'Заполнить все описания товаров?\')) self.location.href=\'' . Fenix::getUrl('catalog/products/filldescription/parent/' . $parentId) . '\'')
                                     ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Добавить товар"))
                                         ->appendClass('btn-primary')
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/products/add/parent/' . $parentId) . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Товары интернет магазина"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/products/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/products/parent/' . $_catalog->id)
            );
        }
        $this->_helper->BreadCrumbs($_crumb);

        $jTree = $Creator->loadPlugin('JTree');
        $jTree ->setId('structure_tree')
               ->setPlugins(array('themes', 'json_data', 'ui', 'unique'))
               ->setTable('catalog')
               ->setUrl(Fenix::getUrl('catalog/products/parent/{$node->id}'))
               ->setEditUrl(Fenix::getUrl('catalog/products/parent/{$node->id}'))
               ->setInitOpen($parentId);

        // Таблица
        $Table    = $Creator->loadPlugin('Table_Db_Generator')
                            ->setTablename('catalog_products')
                            ->setTableId('productsList')
                            ->setTitle(Fenix::lang("Управление товарами") . ($parentCategory->id > 1 ? ' категории "' . $parentCategory->title_russian . '"' : null))
                            ->setData($productsList)
                            //->setCheckall()
                            ->setStandartButtonset()
                            ->setCellCallback('image', function($value, $data, $column, $table){
                                if ($data->image == null)
                                    return;
                                return '<img src="' . HOME_DIR_URL . $data->image . '" width="120" alt="" />';
                            })
                            ->setCellCallback('create_date', function($value, $data, $column, $table){
                                return Fenix::getDate($value)->format('d.m.Y H:i:s');
                            })
                            ->setCellCallback('modify_date', function($value, $data, $column, $table){
                                if ($value != '0000-00-00 00:00:00')
                                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                                return;
                            });

        $config = Fenix::getStaticConfig();

        if ($config->catalog->modules->material == '1')
        {
            $Table ->addAction(array(
                'type'  => 'link',
                'icon'  => 'th',
                'title' => Fenix::lang("Материалы"),
                'url'   => Fenix::getUrl('catalog/products/materials/id/{$data->id}/parent/{$data->parent}')
            ));
        }

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/products/edit/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type' => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'  => Fenix::getUrl('catalog/products/delete/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Управление товарами") . ($parentCategory->id > 1 ? ' категории "' . $parentCategory->title_russian . '"' : null));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
        ), array(
            $Creator->loadPlugin('Block')->setTitle(Fenix::lang("Дерево категорий"))->setContent($jTree->fetch())
        ), array(
            $Table->fetch('catalog/products')
        ));
    }

    public function addAction()
    {
        // Тестирование родительской категории
        $parentId       = (int) $this->getRequest()->getParam('parent');
        //если категория не выбрана редиректим на "Товар без категории"
        if($parentId == 1){
            $parentId = Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT;
        }
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/products/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/products/parent/' . $_catalog->id)
            );
        }
        $_crumb[]   = array(
            'label' => Fenix::lang("Новый товар"),
            'uri'   => '',
            'id'    => 'add'
        );
        $this->_helper->BreadCrumbs($_crumb);

        // Url автоматом
        if ($this->getRequest()->getPost('system_url_key') == '') {
            $this->getRequest()->setPost('system_url_key',
                Fenix::stringProtectUrl($this->getRequest()->getPost('system_title_russian')));
        }


        $Form = Fenix::getHelper('catalog/backend_products')->getProductsForm(array(
            'category' => $parentCategory
        ));
        if ($Form->ok()) {
            $id = Fenix::getModel('catalog/backend_products')->addProduct(
                $parentCategory,
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Товар добавлен"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/products/add/parent/' . $parentCategory->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/products/edit/parent/' . $parentCategory->id . '/id/' . $id);
            }

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый товар"));

        $Creator ->setLayout()->oneColumn(array(
            $Form->fetch()
        ));
    }

    public function editAction()
    {
        // Тестирование родительской категории
        $parentId       = (int) $this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        // Тестирование товара
        $productId       = (int) $this->getRequest()->getParam('id');
        $currentProduct  = Fenix::getModel('catalog/backend_products')->getProductById($productId);

        if ($currentProduct == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный тован не найден"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );

        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/products/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/products/parent/' . $_catalog->id)
            );
        }
        $_crumb[]   = array(
            'label' => Fenix::lang("Редактировать товар"),
            'uri'   => '',
            'id'    => 'add'
        );
        $this->_helper->BreadCrumbs($_crumb);

        // Url автоматом
        if ($this->getRequest()->getPost('system_url_key') == '') {
            $this->getRequest()->setPost('system_url_key',
                Fenix::stringProtectUrl($this->getRequest()->getPost('system_title_russian')));
        }

        $Form = Fenix::getHelper('catalog/backend_products')->getProductsForm(array(
            'category' => $parentCategory,
            'current'  => $currentProduct
        ));

        if ($Form->ok()) {
            $id = Fenix::getModel('catalog/backend_products')->editProduct(
                $_catalog,
                $currentProduct,
                $this->getRequest()
            );

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Товар отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/products/parent/' . $_catalog->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/products/add/parent/' . $_catalog->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/products/edit/parent/' . $_catalog->id . '/id/' . $id);
            }

            Fenix::redirect('catalog/products/parent/' . $_catalog->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать товар"));
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        $Creator ->setLayout()->oneColumn(array(
            $Event->fetch(),
            $Form->fetch()
        ));
    }

    public function deleteAction()
    {
        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        // Тестирование родительской категории
        $parentId       = (int) $this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        // Тестирование товара
        $productId       = (int) $this->getRequest()->getParam('id');
        $currentProduct  = Fenix::getModel('catalog/backend_products')->getProductById($productId);

        if ($currentProduct == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный тован не найден"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        Fenix::getModel('catalog/backend_products')->deleteProduct($currentProduct);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Товар удален"))
            ->saveSession();


        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
    }

    public function deleteGarbageAction(){
        $this->getHelper('rules')->checkDevRedirect();

        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        Fenix::getModel('catalog/backend_products')->deleteGarbageProducts();

        Fenix::getCreatorUI()
             ->loadPlugin('Events_Session')
             ->setType(Creator_Events::TYPE_OK)
             ->setMessage(Fenix::lang("Мусорные товары и их записи удалены"))
             ->saveSession();


        Fenix::getModel('core/common')->updateTreeIndex('catalog');
        Fenix::getModel('catalog/backend_products')->updateProductsRelations();

        Fenix::redirect('catalog/products/parent/1');
    }

    public function findAction()
    {
        $resuls = $productsList = Fenix::getModel('catalog/backend_products')->findProduct(
            $this->getRequest()->getQuery('term')
        );


        $data = array();

        foreach ($resuls AS $_product) {
            $data[] = array(
                'id'     => $_product->id,
                'sku'    => $_product->sku,
                'id_1c'    => $_product->id_1c,
                'label'  => $_product->title,
                'price'  => $_product->price
            );
        }

        echo Zend_Json::encode($data);
        exit;
    }

    public function optionsAction()
    {

        $resuls = $productsList = Fenix::getModel('catalog/backend_options')->findOptions(
            $this->getRequest()->getQuery('term')
        );


        $data = array();

        foreach ($resuls AS $_product) {
            $data[] = array(
                'id'     => $_product->id,
                //'sku'    => $_product->sku,
                //'id_1c'    => $_product->id_1c,
                'label'  => $_product->title,
                //'price'  => $_product->price
            );
        }

        echo Zend_Json::encode($data);
        exit;
    }

    public function clearAction(){
        Fenix::getModel('catalog/backend_products')->clearProducts(Fenix::getRequest()->getParam('parent'));

        Fenix::getCreatorUI()
             ->loadPlugin('Events_Session')
             ->setType(Creator_Events::TYPE_OK)
             ->setMessage(Fenix::lang("Товары удалены"))
             ->saveSession();

        Fenix::redirect('catalog/products/parent/' . Fenix::getRequest()->getParam('parent'));
    }

    public function updateDescriptionsAction(){
        $count = Fenix::getModel('catalog/backend_products')->updateDescriptionsProducts(Fenix::getRequest()->getParam('parent'));
        //Fenix::dump(Fenix::getRequest()->getParam('parent'));
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Групы обновлены (" . $count . ")"))
            ->saveSession();

        Fenix::redirect('catalog/products/parent/' . Fenix::getRequest()->getParam('parent'));
    }

    /**
     *
     */
    public function filldescriptionAction(){

        $count = Fenix::getModel('catalog/backend_products')->updateAllDescriptionsProducts(Fenix::getRequest()->getParam('parent'));
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Групы обновлены (" . $count . ")"))
            ->saveSession();

        Fenix::redirect('catalog/products/parent/' . Fenix::getRequest()->getParam('parent'));
    }

    public function materialsAction()
    {
        // Тестирование родительской категории
        $parent   = (int)$this->getRequest()->getPost('parent');
        $parentId = $parent ? $parent : $this->getRequest()->getParam('parent');
        //Fenix::dump($parentId);
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Родительская категория не найдена"))
                 ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        // Тестирование товара
        $productId       = (int) $this->getRequest()->getParam('id');
        $currentProduct  = Fenix::getModel('catalog/backend_products')->getProductById($productId);

        if ($currentProduct == null) {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Выбранный тован не найден"))
                 ->saveSession();

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/products/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/products/parent/' . $_catalog->id)
            );
        }
        $_crumb[]   = array(
            'label' => Fenix::lang("Редактировать материалы товара"),
            'uri'   => '',
            'id'    => 'add'
        );
        $this->_helper->BreadCrumbs($_crumb);
        $Form = Fenix::getHelper('catalog/backend_products')->getMaterialForm(array(
            'category' => $parentCategory,
            'current'  => $currentProduct
        ));


        if ($Form->ok()) {
            // Обновляем мультинаборы
            if (Fenix::getStaticConfig()->catalog->modules->material == '1') {
                Fenix::getModel('catalog/backend_material')->updateMaterial($currentProduct->id,  $this->getRequest());
            }

            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Товар отредактирован"))
                 ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/products/add/parent/' . $parentCategory->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/products/materials/parent/' . $parentCategory->id . '/id/' . $currentProduct->id);
            }

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать материалы товара"));

        $Creator ->setLayout()->oneColumn(array(
            $Form->fetch()
        ));
    }
}