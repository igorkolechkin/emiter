<?php
class Fenix_Catalog_Controller_Admin_Material extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/material')
                ->prepare()
                ->execute();
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/material_item')
                ->prepare()
                ->execute();
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/material_products')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $parent = Fenix::getRequest()->getParam('parent');
        if ($parent == null) $parent = 0;

        $materialsList = Fenix::getModel('catalog/material')->getMaterialListAsSelect($parent);
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый блок"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/material/add/parent/' . $parent  . '\''))
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Категории тканей"))
                           //->setDetails(Fenix::lang("Можно использовать для создания различных Категории тканей блоков на разных страницах сайта"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Каталог"),
                'uri'   => Fenix::getUrl('catalog'),
                'id'    => 'catalog'
            ),
            array(
                'label' => Fenix::lang("Категории тканей"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('materialsList')
                           ->setTitle(Fenix::lang("Категории тканей"))
                           ->setData($materialsList)
                           ->setStandartButtonset();
        $Table->setCellCallback('title', function ($value, $data, $column, $table) {
            if ($data->parent == '0')
                return '<a href="' . Fenix::getUrl('catalog/material/parent/' . $data->id) . '">' . $value . '</a>';
            else
                return '<a href="' . Fenix::getUrl('catalog/material/item/sid/' . $data->id) . '">' . $value . '</a>';
        })
              ->setCellCallback('attribute_id', function ($value, $data, $column, $table) {

                  $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($value);

                  if ($attribute)
                      return $attribute->title . ' (' . $attribute->sys_title . ')';

                  return $value;
              });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/material/edit/id/{$data->id}/parent/' . $parent)
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/material/delete/id/{$data->id}/parent/' . $parent)
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Материалы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch(),
                     $Event->fetch(),
                     $Table->fetch('catalog/material')
                 ));
    }

    public function addAction()
    {
        $parent = Fenix::getRequest()->getParam('parent');
        if ($parent == null) $parent = 0;
        $this->getRequest()->setPost('parent', $parent);

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категории тканей"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Новый блок"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('catalog/material', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Категории тканей создано"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/material/parent/' . $parent);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/material/add/parent/' . $parent);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/material/edit/id/' . $id . '/parent/' . $parent);
            }
            
            Fenix::redirect('catalog/material/parent/' . $parent);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый блок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentMaterial = Fenix::getModel('catalog/material')->getMaterialById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentMaterial == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Категории тканей не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/material');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категории тканей"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Изменить Категории тканей"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $parent = Fenix::getRequest()->getParam('parent');
        if ($parent == null) $parent = 0;
        $this->getRequest()->setPost('parent', $parent);


        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentMaterial->toArray())
                   ->setData('current', $currentMaterial);
        
        // Источник
        $Form      ->setSource('catalog/material', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentMaterial, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Категории тканей отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/material');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/material/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/material/edit/id/' . $id);
            }
            
            Fenix::redirect('catalog/material');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать Категории тканей"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentMaterial = Fenix::getModel('catalog/material')->getMaterialById(
            $this->getRequest()->getParam('id')
        );

        if ($currentMaterial == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Категории тканей не найден"))
                ->saveSession();

            Fenix::redirect('catalog/material');
        }

        $slideList = Fenix::getModel('catalog/material')->getItemList($currentMaterial->id);

        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('catalog/material', 'default')
                ->deleteRecord($currentMaterial);

        foreach ($slideList AS $_slide) {
            $Creator->loadPlugin('Form_Generator')
                    ->setSource('catalog/material_item', 'default')
                    ->deleteRecord($_slide);
        }

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Категории тканей удалён"))
                 ->saveSession();
            
        Fenix::redirect('catalog/material');
    }
}