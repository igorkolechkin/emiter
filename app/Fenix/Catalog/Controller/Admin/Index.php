<?php

class Fenix_Catalog_Controller_Admin_Index extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/categories')
            ->prepare()
            ->execute();

        /*$Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/configurable')
                ->prepare()
                ->execute();*/

    }

    public function indexAction()
    {
        if ($this->getRequest()->getParam('parent') == null) {
            Fenix::redirect('catalog/parent/1');
        }

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['categoriesList'])) {
                foreach ((array)$rows['categoriesList'] AS $_rowId) {
                    $currentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById(
                        $_rowId
                    );
                    Fenix::getModel('catalog/backend_categories')->deleteCategory(
                        $currentCategory
                    );
                }

                Fenix::redirect('catalog/parent/' . $this->getRequest()->getParam('parent'));
            }
        }

        $parentId = (int)$this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);
        $categoriesList = Fenix::getModel('catalog/backend_categories')->getCategoriesListAsSelect($parentId);

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Новая категория"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/add/parent/' . $parentId) . '\'')
                    ->fetch()
            ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setImage(Fenix::getAppEtcUrl('icons/icon-auction-catalog.png', 'catalog'))
            ->setTitle(Fenix::lang("Категории интернет магазина"))
            ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/parent/' . $_catalog->id)
            );
        }
        $this->_helper->BreadCrumbs($_crumb);

        $jTree = $Creator->loadPlugin('JTree');
        $jTree->setId('structure_tree')
            ->setPlugins(array('themes', 'json_data', 'ui', 'unique'))
            ->setTable('catalog')
            ->setUrl(Fenix::getUrl('catalog/parent/{$node->id}'))
            ->setEditUrl(Fenix::getUrl('catalog/edit/id/{$node->id}'))
            ->setInitOpen($parentId);

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('categoriesList')
            ->setTitle(Fenix::lang("Управление категориями"))
            ->setData($categoriesList)
            //->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('image', function ($value, $data, $column, $table) {
                if ($data->image == null) {
                    return;
                }

                return '<img src="' . HOME_DIR_URL . $data->image . '" width="50" alt="" />';
            })
            ->setCellCallback('title', function ($value, $data, $column, $table) {
                return '<a href="' . Fenix::getUrl('catalog/parent/' . $data->id) . '">' . $value . '</a>';
            })
            ->setCellCallback('create_date', function ($value, $data, $column, $table) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function ($value, $data, $column, $table) {
                if ($value != '0000-00-00 00:00:00') {
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                }

                return;
            });

        if ($parentCategory->id > 1) {
            $Table->setUpLevel(Fenix::getUrl('catalog/parent/' . $parentCategory->parent));
        }

        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/edit/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/delete/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table->addAction(array(
            'type'    => 'sorting',
            'options' => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Категории"));

        $Creator->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
        ), array(
            $Creator->loadPlugin('Block')->setTitle(Fenix::lang("Дерево категорий"))->setContent($jTree->fetch())
        ), array(
            $Table->fetch('catalog/categories')
        ));
    }

    public function addAction()
    {
        // Тестирование родительской категории
        $parentId = (int)$this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog');
        }

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Каталог"),
                'uri'   => Fenix::getUrl('catalog'),
                'id'    => 'catalog'
            ),
            array(
                'label' => Fenix::lang("Новый каталог"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Url автоматом
        if ($this->getRequest()->getPost('url_key') == '') {
            $this->getRequest()->setPost('url_key',
                Fenix::stringProtectUrl($this->getRequest()->getPost('title_russian')));
        }

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('current', null)
            ->setData('parentCategory', $parentCategory);

        // Источник
        $Form->setSource('catalog/categories', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));

            $this->getRequest()->setPost('parent', $parentCategory->id);

            $id = Fenix::getModel('catalog/backend_categories')->addCategory(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Категория создана"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/add/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/edit/id/' . $id . '/parent/' . $parentCategory->id);
            }

            Fenix::redirect('catalog/parent/' . $parentCategory->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новая категория"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        // Тестирование родительской категории
        $parentId = (int)$this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog');
        }

        $currentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/parent/' . $parentCategory->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Каталог"),
                'uri'   => Fenix::getUrl('catalog'),
                'id'    => 'catalog'
            ),
            array(
                'label' => Fenix::lang("Редактировать каталог"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Url автоматом
        if ($this->getRequest()->getPost('url_key') == '') {
            $this->getRequest()->setPost('url_key',
                Fenix::stringProtectUrl($this->getRequest()->getPost('title_russian')));
        }

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentCategory->toArray())
            ->setData('current', $currentCategory)
            ->setData('parentCategory', $parentCategory);

        // Источник
        $Form->setSource('catalog/categories', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('modify_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_categories')->editCategory(
                $Form,
                $currentCategory,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Категория изменена"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/add/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/edit/id/' . $id . '/parent/' . $parentCategory->id);
            }

            Fenix::redirect('catalog/parent/' . $parentCategory->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать категорию"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        // Тестирование родительской категории
        $parentId = (int)$this->getRequest()->getParam('parent');
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog');
        }

        $currentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/parent/' . $parentCategory->id);
        }

        /** Защита от удаления корневой категории и категории "Товары без категории" */
        if ($currentCategory->id == 1 || $currentCategory->id == Fenix_Catalog_Model_Backend_Products::PRODUCT_NO_CATEGORY_PARENT) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Нельзя удалить данную категорию!"))
                ->saveSession();

            Fenix::redirect('catalog');
        }

        Fenix::getModel('catalog/backend_categories')->deleteCategory(
            $currentCategory
        );

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили категории"))
            ->saveSession();

        Fenix::redirect('catalog/parent/' . $parentCategory->id);
    }

    public function materialsAction()
    {
        // Тестирование родительской категории
        $parent = (int)$this->getRequest()->getPost('parent');
        $parentId = $parent ? $parent : $this->getRequest()->getParam('parent');
        //Fenix::dump($parentId);
        $parentCategory = Fenix::getModel('catalog/backend_categories')->getCategoryById($parentId);

        if ($parentCategory == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская категория не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/1');
        }

        // Тестирование товара
        $productId = (int)$this->getRequest()->getParam('id');
        $currentProduct = Fenix::getModel('catalog/backend_products')->getProductById($productId);

        if ($currentProduct == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный тован не найден"))
                ->saveSession();

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        // Хлебные крошки
        $navigation = Fenix::getModel('catalog/backend_categories')->getNavigation(
            $parentCategory
        );
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog/products/parent/1'),
            'id'    => 'catalog'
        );
        foreach ($navigation AS $_catalog) {
            $_crumb[] = array(
                'label' => $_catalog->title,
                'id'    => 'id_' . $_catalog->id,
                'uri'   => Fenix::getUrl('catalog/products/parent/' . $_catalog->id)
            );
        }
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать материалы товара"),
            'uri'   => '',
            'id'    => 'add'
        );
        $this->_helper->BreadCrumbs($_crumb);

        $Form = Fenix::getHelper('catalog/backend_products')->getMaterialForm(array(
            'category' => $parentCategory,
            'current'  => $currentProduct
        ));

        if ($Form->ok()) {

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Товар отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/products/add/parent/' . $parentCategory->id);
            } elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/products/edit/parent/' . $parentCategory->id . '/id/' . $id);
            }

            Fenix::redirect('catalog/products/parent/' . $parentCategory->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать материалы товара"));

        $Creator->setLayout()->oneColumn(array(
            $Form->fetch()
        ));
    }
}