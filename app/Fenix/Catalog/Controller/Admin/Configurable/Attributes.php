<?php
class Fenix_Catalog_Controller_Admin_Configurable_Attributes extends Fenix_Controller_Action
{
    public function preDispatch()
    {

    }
    
    public function indexAction()
    {

    }

    public function addAction()
    {
        $productId   = Fenix::getRequest()->getParam('product_id');
        $attributeId = Fenix::getRequest()->getParam('attribute_id');
        $return = base64_decode(Fenix::getRequest()->getParam('return'));

        if($productId==null || $attributeId == null){
            throw new Exception('Не указан id');
        }

        Fenix::getModel('catalog/backend_configurable_attributes')->addAttributeToProduct($productId, $attributeId);

        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Атрибут добавлен"))
                 ->saveSession();

        Fenix::redirect($return, true);
    }

    public function removeAction()
    {
        $productId   = Fenix::getRequest()->getParam('product_id');
        $attributeId = Fenix::getRequest()->getParam('attribute_id');
        $return = base64_decode(Fenix::getRequest()->getParam('return'));

        if ($productId == null || $attributeId == null) {
            throw new Exception('Не указан id');
        }

        Fenix::getModel('catalog/backend_configurable_attributes')->removeAttributeFromProduct($productId, $attributeId);

        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Атрибут удален"))
                 ->saveSession();

        Fenix::redirect($return, true);
    }
}