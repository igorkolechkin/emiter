<?php
class Fenix_Catalog_Controller_Admin_Reviews extends Fenix_Controller_Action
{

    public function preDispatch()
    {
        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('catalog/reviews')
               ->prepare()
               ->execute();
        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('likes/likes')
               ->prepare()
               ->execute();
    }

    public function indexAction()
    {
        $reviewsList = Fenix::getModel('catalog/reviews')->getReviewsListAsSelect();

        //Обновляем рейтинг на товарах
        Fenix::getModel('catalog/reviews')->updateProductsRating();
        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Отзывы"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Отзывы"),
                'uri'   => Fenix::getUrl('catalog/reviews'),
                'id'    => 'reviews'
            )
        ));

        // Таблица
        $Table = Fenix::getCreatorUI()->loadPlugin('Table_Db');


        $Table ->setRowCallback(function($table, $data){
            switch ($data->is_active) {
                case '0':
                    $table->appendStyle('background:#ffc0c0 !important;');
                    break;
                case '1':
                    $table->appendStyle('background:#96c787 !important;');
                    break;
            }
        });

        $Table ->setTableId('cList');
        $Table ->setClass('table table-bordered');
        $Table ->setTitle(Fenix::lang("Управление отзывами"));
        $Table ->setData($reviewsList);
        $Table ->setStandartButtonset();

        // Id
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('id')
                ->setTitle(Fenix::lang("ID"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'range',
                    'html' => 'text'
                ))
                ->setSqlCellName('id')
                ->setSqlCellNameAs('r.id')
                ->setHeaderAttributes(array(
                    'width' => '50'
                ))
        );

        // Дата
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('review_date')
                ->setTitle(Fenix::lang("Дата создания"))
                ->setSortable(true)

                ->setFilter(array(
                    'type' => 'range',
                    'html' => 'calendar'
                ))
                ->setSqlCellName('review_date')
                ->setSqlCellNameAs('r.review_date')
                ->setHeaderAttributes(array(
                    'width' => '150'
                ))
                ->setEditable(array(
                    'html'    => 'text'
                ))
                /*->setCellCallback(function($value){
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                })*/
        );

        // Артикул
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('sku')
                ->setTitle(Fenix::lang("Артикул"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('sku')
                ->setSqlCellNameAs('p.sku')
                ->setCellCallback(function($value){
                    return $value ? $value : 'Товар не найден (Удален)';
                })
        );

        // Артикул
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('rating')
                ->setTitle(Fenix::lang("Оценка"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('rating')
                ->setSqlCellNameAs('rating')
        );

        // Товар
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('product')
                ->setTitle(Fenix::lang("Товар"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('product')
                ->setSqlCellNameAs('p.title_russian')
        );

        // Клиент
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('user_name')
                ->setTitle(Fenix::lang("Клиент"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('name')
                ->setSqlCellNameAs('r.name')
        );

        // Отзыв
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('review')
                ->setTitle(Fenix::lang("Отзыв"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('review')
                ->setSqlCellNameAs('r.review')
        );

        // Отзыв
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('is_active')
                ->setTitle(Fenix::lang("Активность"))
                ->setSortable(true)
                ->setEditable(array(
                    'html'    => 'select',
                    'options' => array(
                        'option' => array(
                            array(
                                'name'   => '1',
                                '_value' => Fenix::lang("Да")
                            ),
                            array(
                                'name'   => '0',
                                '_value' => Fenix::lang("Нет")
                            )
                        )
                    )
                ))
                ->setSqlCellName('is_active')
                ->setSqlCellNameAs('r.is_active')
        );

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/reviews/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/reviews/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отзывы"));

        // Рендер страницы
        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch()
        ));
    }

    public function editAction()
    {
        $current = Fenix::getModel('catalog/reviews')->getReviewById(
            $this->getRequest()->getParam('id')
        );

        if ($current == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Отзыв не найден"))
                ->saveSession();

            Fenix::redirect('catalog/reviews');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Отзывы"),
                'uri'   => Fenix::getUrl('catalog/reviews'),
                'id'    => 'coupons'
            ),
            array(
                'label' => Fenix::lang("Редактировать отзыв"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $current->toArray();

        list($createDate, $createTime) = explode(' ', $current->review_date);

        $Defaults['review_date'] = $createDate;
        $Defaults['review_time'] = $createTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $current);

        // Источник
        $Form      ->setSource('catalog/reviews', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $req = $Form->getRequest();

            $review_date = $req->getPost('review_date') . ' ' . $req->getPost('review_time');

            $req->setPost('review_date', $review_date);

            $req->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = $Form->editRecord(
                $current,
                $req
            );
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Отзыв отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/reviews');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/reviews/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/reviews/edit/id/' . $id);
            }

            Fenix::redirect('catalog/reviews');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать отзыв"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $current = Fenix::getModel('catalog/reviews')->getReviewById(
            $this->getRequest()->getParam('id')
        );

        if ($current == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Отзыв не найден"))
                ->saveSession();

            Fenix::redirect('catalog/reviews');
        }

        $Creator = Fenix::getCreatorUI();

        Fenix::getModel('catalog/reviews')->deleteReview(
            $current
        );

        $Creator ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Отзыв удалён"))
            ->saveSession();

        Fenix::redirect('catalog/reviews');
    }
}