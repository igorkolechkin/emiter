<?php

class Fenix_Catalog_Controller_Admin_Attributes_Values extends Fenix_Controller_Action
{
    public function indexAction()
    {

        $currentAttribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttribute == null) {
            //Fenix::dump( $this->getRequest()->getParam('id'),$currentAttribute);
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributes');
        }

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/attributes'),
                'id'    => 'attributes'
            ),
            array(
                'label' => Fenix::lang("Редактировать атрибут"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentAttribute->toArray())
            ->setData('current', $currentAttribute);

        // Источник
        $Form->setSource('catalog/attributes_values', $currentAttribute->type)
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_attributes')->editAttributeValues(
                $Form,
                $currentAttribute,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Значение атрибута отредактированы"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributes');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributes/values/id/' . $currentAttribute->id . '/type/' . $currentAttribute->type);
            }

            Fenix::redirect('catalog/attributes');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать атрибут"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());

    }

    public function sortingAction()
    {

        $currentAttribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttribute == null) {
            //Fenix::dump( $this->getRequest()->getParam('id'),$currentAttribute);
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributes');
        }

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/attributes'),
                'id'    => 'attributes'
            ),
            array(
                'label' => Fenix::lang("Редактировать атрибут"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentAttribute->toArray())
            ->setData('current', $currentAttribute);

        // Источник
        $Form->setSource('catalog/attributes_values_sorting', $currentAttribute->type)
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('modify_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_attributes')->editAttributeValues(
                $Form,
                $currentAttribute,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Значение атрибута отредактированы"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributes');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributes/values/id/' . $currentAttribute->id . '/type/' . $currentAttribute->type);
            }

            Fenix::redirect('catalog/attributes');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать атрибут"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());

    }

    public function deleteAction()
    {
        $attrSysTitle = Fenix::getRequest()->getPost('sys_title');
        $valueId = Fenix::getRequest()->getPost('value_id');

        if ($attrSysTitle && $valueId) {
            Fenix::getModel('catalog/backend_attributes')->deleteAttributeValue($attrSysTitle, $valueId);
            echo 'ok';
        }
        else {
            throw new Exception('Ошибка нет данных');
        }
    }

    public function positionAction()
    {

        $position = Fenix::getRequest()->getPost('position');
        if ($position) {
            Fenix::getModel('catalog/backend_attributes')->updateAttributeValuesPosition($position);
            echo 'ok';
        }
        else {
            throw new Exception('Ошибка нет данных');
        }
    }

    public function processsortingAction()
    {
        $step = 10;
        $n = Fenix::getRequest()->getParam('n');
        $id = Fenix::getRequest()->getParam('id');
        if ($n == null) {
            $n = 0;
        }
        else {
            $n = (int) $n;
        }

        Fenix::getModel('catalog/backend_attributes')->sortingValues($n, $step, $id);

        if ($id) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Вы успешно отсортировали значения атрибута"))
                ->saveSession();
        }
        else {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Вы успешно отсортировали значения всех атрибутов"))
                ->saveSession();
        }

        if ($id) {
            Fenix::redirect('catalog/attributes/values/sorting/id/' . $id);
        }
        else {
            Fenix::redirect('catalog/attributes');
        }
    }

    public function dublicateAction()
    {

        if ($_SERVER['REMOTE_ADDR'] == "194.29.63.35") {

        }
        else {
            exit;
        }

        $step = 10;
        $n = Fenix::getRequest()->getParam('n');
        $id = Fenix::getRequest()->getParam('id');
        if ($n == null) {
            $n = 0;
        }
        else {
            $n = (int) $n;
        }

        Fenix::getModel('catalog/backend_attributes')->dublicateValuesMerge($n, $step, $id);

        if ($id) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Вы успешно проверили дубликаты значений атрибута"))
                ->saveSession();
        }
        else {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Вы успешно проверили дубликаты значений всех атрибутов"))
                ->saveSession();
        }

        if ($id) {
            Fenix::redirect('catalog/attributes/values/sorting/id/' . $id);
        }
        else {
            Fenix::redirect('catalog/attributes');
        }
    }
}