<?php
class Fenix_Catalog_Controller_Admin_Attributeset extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('catalogAll');

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('catalog/system_attributeset');

        $this->_engine->prepare()
             ->execute();
    }

    public function indexAction()
    {
        $attributesetList = Fenix::getModel('catalog/backend_system_attributeset')->getAttributesetListAsSelect();

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['attributesetList'])) {
                foreach ((array) $rows['attributesetList'] AS $_rowId) {
                    $currentAttributeset = Fenix::getModel('catalog/backend_system_attributeset')->getAttributesetById(
                        $_rowId
                    );
                    Fenix::getModel('catalog/backend_system_attributeset')->deleteAttributeset(
                        $currentAttributeset
                    );
                }

                Fenix::redirect('catalog/attributeset');
            }
        }

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наботы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Новый набор атрибутов"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Новый набор атрибутов"))
                                         ->appendClass('btn-primary')
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/attributeset/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Атрибуты товаров"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('attributesetList')
                           ->setTitle(Fenix::lang("Управление атрибутами товаров"))
                           ->setData($attributesetList)
                           ->setCheckall(array('name' => 'row'))
                           ->setStandartButtonset()
                           ->setCellCallback('title', function($value, $data, $column, $table){
                               return '<a href="' . Fenix::getUrl('catalog/attributeset/groups/aid/' . $data->id) . '">' . $value . '</a>';
                           })
                           ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'copy',
            'title' => Fenix::lang("Копировать"),
            'url'   => Fenix::getUrl('catalog/attributeset/copy/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/attributeset/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type' => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'  => Fenix::getUrl('catalog/attributeset/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Атрибуты"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('catalog/attributeset')
        ));

    }

    public function addAction()
    {
        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наботы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Новый набор атрибутов"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        // Источник
        $Form      ->setSource('catalog/attributeset', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_system_attributeset')->addAttributeset($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Набор атрибутов добавлен"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributeset');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributeset/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributeset/edit/id/' . $id);
            }

            Fenix::redirect('catalog/attributeset');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый набор атрибутов"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentAttributeset = Fenix::getModel('catalog/backend_system_attributeset')->getAttributesetById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttributeset == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами набор атрибутов не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset');
        }

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наботы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Редактировать набор атрибутов"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentAttributeset->toArray())
                   ->setData('current', $currentAttributeset);

        // Источник
        $Form      ->setSource('catalog/attributeset', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_system_attributeset')->editAttributeset(
                $Form, $currentAttributeset, $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Набор атрибутов изменен"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributeset');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributeset/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributeset/edit/id/' . $id);
            }

            Fenix::redirect('catalog/attributeset');
        }

        // Тайтл страницы
        $Creator ->getView()
            ->headTitle(Fenix::lang("Редактировать набор атрибутов"));

        $Creator ->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $currentAttributeset = Fenix::getModel('catalog/backend_system_attributeset')->getAttributesetById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttributeset == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами набор атрибутов не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset');
        }

        Fenix::getModel('catalog/backend_system_attributeset')->deleteAttributeset(
            $currentAttributeset
        );

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили набор атрибутов"))
            ->saveSession();

        Fenix::redirect('catalog/attributeset');
    }

    public function copyAction(){

        $currentAttributeset = Fenix::getModel('catalog/backend_system_attributeset')->getAttributesetById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttributeset == null) {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Выбранный Вами набор атрибутов не найден"))
                 ->saveSession();

            Fenix::redirect('catalog/attributeset');
        }

        $id = Fenix::getModel('catalog/backend_system_attributeset')->copyAttributeset(
            $currentAttributeset
        );

        Fenix::getCreatorUI()
             ->loadPlugin('Events_Session')
             ->setType(Creator_Events::TYPE_OK)
             ->setMessage(Fenix::lang("Вы успешно скопировали набор атрибутов"))
             ->saveSession();

        Fenix::redirect('catalog/attributeset/');
    }
}