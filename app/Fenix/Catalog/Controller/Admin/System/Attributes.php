<?php
/**
 * Управление системными атрибутами
 *
 * Class Fenix_Catalog_Controller_Admin_System_Attributes
 *
 */
class Fenix_Catalog_Controller_Admin_System_Attributes extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('catalog/system_attributes');

        $this->_engine->prepare()
             ->execute();

    }

    /**
     * Список системных атрибутов
     * @throws Exception
     */
    public function indexAction()
    {

        Fenix::getModel('catalog/backend_system_attributes')->updateProductsTable();

        $attributesList = Fenix::getModel('catalog/backend_system_attributes')->getAttributesListAsSelect();

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['attributesList'])) {
                foreach ((array) $rows['attributesList'] AS $_rowId) {
                    $currentAttribute = Fenix::getModel('catalog/backend_system_attributes')->getAttributeById(
                        $_rowId
                    );
                    if ($currentAttribute->is_system != '1') {
                        Fenix::getModel('catalog/backend_system_attributes')->deleteAttribute(
                            $currentAttribute
                        );
                    }
                }

                Fenix::redirect('catalog/system/attributes');
            }
        }

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор тип атрибута"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('catalog/system/attributes/add/type/{$attributeset}')
        )));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Системные атрибуты товаров"),
                'uri'   => '',
                'id'    => 'system_attributes'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Новый атрибут"))
                                         ->appendClass('btn-primary')
                                         ->setType('button')
                                         ->setOnclick($Dialog->toButton())
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Атрибуты товаров"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('attributesList')
                           ->setTitle(Fenix::lang("Управление системными атрибутами товаров"))
                           ->setData($attributesList)
                           ->setCheckall(array('name' => 'row'))
                           ->setStandartButtonset()
                           ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/system/attributes/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type' => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'  => Fenix::getUrl('catalog/system/attributes/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Атрибуты"));
        
        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('catalog/system_attributes')
        ));
    }

    /**
     * Создание нового системного атрибута
     * @throws Exception
     */
    public function addAction()
    {
        $attributeset = ($this->getRequest()->getParam('type') == null ? 'text' : $this->getRequest()->getParam('type'));

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/system/attributes'),
                'id'    => 'catalog'
            ),
            array(
                'label' => Fenix::lang("Новый атрибут"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        // Источник
        $Form      ->setSource('catalog/system_attributes', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_system_attributes')->addAttribute(
                $Form,
                $attributeset,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут добавлен"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/system/attributes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/system/attributes/add/type/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/system/attributes/edit/id/' . $id . '/type/' . $attributeset);
            }

            Fenix::redirect('catalog/system/attributes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактирование системного атрибута
     * @throws Exception
     */
    public function editAction()
    {
        $currentAttribute = Fenix::getModel('catalog/backend_system_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentAttribute == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/system/attributes');
        }
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/system/attributes'),
                'id'    => 'attributes'
            ),
            array(
                'label' => Fenix::lang("Редактировать атрибут"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentAttribute->toArray())
                   ->setData('current', $currentAttribute);
        
        // Источник
        $Form      ->setSource('catalog/system_attributes', $currentAttribute->type)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_system_attributes')->editAttribute(
                $Form,
                $currentAttribute,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут изменен"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/system/attributes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/system/attributes/add/type/' . $currentAttribute->type);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/system/attributes/edit/id/' . $id . '/type/' . $currentAttribute->type);
            }

            Fenix::redirect('catalog/system/attributes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }

    /**
     * Удаление системного атрибута
     * @throws Exception
     */
    public function deleteAction()
    {
        $currentAttribute = Fenix::getModel('catalog/backend_system_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttribute == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                ->saveSession();

            Fenix::redirect('catalog/system/attributes');
        }

        if ($currentAttribute->is_system == '1') {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Удаление системных атрибутов может привести к потери работоспособности системы"))
                ->saveSession();

            Fenix::redirect('catalog/system/attributes');
        }

        Fenix::getModel('catalog/backend_system_attributes')->deleteAttribute(
            $currentAttribute
        );
        
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили атрибут"))
            ->saveSession();
        
        Fenix::redirect('catalog/system/attributes');
    }
}