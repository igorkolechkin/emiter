<?php
class Fenix_Catalog_Controller_Admin_Attributeset_Groups extends Fenix_Controller_Action
{
    private $_aset = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('catalogAll');

        $currentAttributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            $this->getRequest()->getParam('aid')
        );

        if ($currentAttributeset == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами набор атрибутов не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset');
        }

        $this->_aset = $currentAttributeset;

        $this->view->assign('attributeset', $currentAttributeset);

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('catalog/attributeset_groups');

        $this->_engine->prepare()
             ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/attributeset_groups_attributes');

        $Engine->prepare()
               ->execute();
    }

    public function indexAction()
    {
        if ($this->getRequest()->isPost()) {
            Fenix::getModel('catalog/backend_attributeset')->saveAttributeset(
                $this->_aset,
                $this->getRequest()->getPost()
            );
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Набор атрибутов добавлен"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
        }

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наботы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Группы"),
                'uri'   => '',
                'id'    => 'groups'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Группы набора атрибутов"));
        $Creator ->setLayout();
        $Creator ->render('catalog/groups.phtml');
    }

    public function addAction()
    {
        if ($this->getRequest()->getPost('addattribute') != null) {
            Fenix::getModel('catalog/backend_attributeset')->addGroupAttribute(
                $this->_aset,
                $this->getRequest()
            );
        }

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наборы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Группы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset/groups/' . $this->_aset->id),
                'id'    => 'attributeset'
            ),
            array(
                'label' => Fenix::lang("Новая группа"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setData('attributeset', $this->_aset)
                   ->setData('current', null);

        // Источник
        $Form      ->setSource('catalog/attributeset_groups', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('parent',      $this->_aset->id);
            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));

            $id = $Form->addRecord($this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Набор атрибутов добавлен"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributeset/groups/add/aid/' . $this->_aset->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributeset/groups/edit/id/' . $id . '/aid/' . $this->_aset->id);
            }

            Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая группа"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentGroup = Fenix::getModel('catalog/backend_attributeset')->getGroupById(
            $this->getRequest()->getParam('id')
        );

        if ($currentGroup == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами группа не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
        }

        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Наборы атрибутов"),
                'uri'   => Fenix::getUrl('catalog/attributeset'),
                'id'    => 'attributeset'
            ),
            array(
                'label' => $this->_aset->title,
                'uri'   => Fenix::getUrl('catalog/attributeset/groups/aid/' . $this->_aset->id),
                'id'    => 'group'
            ),
            array(
                'label' => Fenix::lang("Редактировать группу"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentGroup->toArray())
                   ->setData('current',      $currentGroup)
                   ->setData('attributeset', $this->_aset);

        // Источник
        $Form      ->setSource('catalog/attributeset_groups', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = $Form->editRecord($currentGroup, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Группа атрибутов отредактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributeset/groups/add/aid/' . $this->_aset->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributeset/groups/edit/id/' . $id . '/aid/' . $this->_aset->id);
            }

            Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
        }

        // Тайтл страницы
        $Creator ->getView()
            ->headTitle(Fenix::lang("Редактировать группу атрибутов"));

        $Creator ->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $currentGroup = Fenix::getModel('catalog/backend_attributeset')->getGroupById(
            $this->getRequest()->getParam('id')
        );

        if ($currentGroup == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами группа не найдена"))
                ->saveSession();

            Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
        }

        Fenix::getModel('catalog/backend_attributeset')->deleteGroup(
            $currentGroup
        );

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили набор атрибутов"))
            ->saveSession();

        Fenix::redirect('catalog/attributeset/groups/aid/' . $this->_aset->id);
    }
}