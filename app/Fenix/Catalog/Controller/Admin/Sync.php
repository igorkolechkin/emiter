<?php
class Fenix_Catalog_Controller_Admin_Sync extends Fenix_Controller_Action
{
    public function indexAction()
    {
        /*
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Синхронизировать"))
                    ->appendClass('btn-primary')
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/sync/begin') . '\'')
                    ->fetch()
        ));

        // Заголовок страницы
        $Title      = $Creator->loadPlugin('Title')
                              ->setTitle(Fenix::lang("Синхронизация"))
                              ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb     = array();
        $_crumb[]   = array(
            'label' => Fenix::lang("Панель управления"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumb[]   = array(
            'label' => Fenix::lang("Синхронизация"),
            'uri'   => Fenix::getUrl('catalog/sync'),
            'id'    => 'sync'
        );
        $this->_helper->BreadCrumbs($_crumb);

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Синхронизация"));

        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch()
        ));
    }

    public function beginAction()
    {
        Fenix::getModel('catalog/sync')->process(
            $this->getRequest(),
            $this->getResponse()
        );

        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Данные синхронизированы"))
                 ->saveSession();

        Fenix::redirect('catalog/sync');
    }
}