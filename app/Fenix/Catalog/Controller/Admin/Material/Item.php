<?php
class Fenix_Catalog_Controller_Admin_Material_Item extends Fenix_Controller_Action
{
    private $_material;
    private $_attribute;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        // Проверки
        $this->_material = Fenix::getModel('catalog/material')->getMaterialById(
            (int) $this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_material == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Категория не найден"))
                ->saveSession();

            Fenix::redirect('catalog/material');
        }

        $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($this->_material->attribute_id);

        if ($attribute == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("У категории неправильно указано системное название атрибута"))
                ->saveSession();

            Fenix::redirect('catalog/material/parent/' . $this->_material->parent);
        }

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('catalog/material_item')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $slideList = Fenix::getModel('catalog/material')->getItemListAsSelect($this->_material->id);
        $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($this->_material->attribute_id);
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый элемент"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/material/item/add/sid/' . $this->_material->id) . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Назад"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/material') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Категория / " . $this->_material->title))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категория"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => $this->_material->title,
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('materialsList')
                           ->setTitle(Fenix::lang("Категория / " . $this->_material->title))
                           ->setData($slideList)
                           ->setStandartButtonset();
        $Table->setCellCallback('image', function($value, $data, $column, $table){
                if ($data->image == null)
                    return;

                $image = Fenix::getUploadImagePath($data->image);
                $url = Fenix_Image::resize($image, 100,100);

                return '<img src="' . $url . '" width="50" alt="" />';
            })
            ->setCellCallback('value_id', function($value, $data, $column, $table){
                if ($data->value_id == null)
                    return;

                return $value;
                //$value  = Fenix::getModel('catalog/backend_products')->getAttributeValueById($attribute, $value);
                if($value)
                    return $value->content;
                else
                    return '';
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/material/item/edit/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('catalog/material/item/delete/sid/{$data->parent}/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Категория / " . $this->_material->title));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('catalog/material_item')
                 ));
    }

    public function addAction()
    {


        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категория"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => $this->_material->title,
                'uri'   => Fenix::getUrl('catalog/material/item/sid/' . $this->_material->id),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Добавить элемент"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        $Form       ->setData('material', $this->_material)
                    ->setData('current', null);
        // Источник
        $Form      ->setSource('catalog/material_item', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_material->id);

            $id = Fenix::getModel('catalog/material')->addItem($Form,  $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Категория создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/material/item/sid/' . $this->_material->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/material/item/add/sid/' . $this->_material->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/material/item/edit/sid/' . $this->_material->id . '/id/' . $id);
            }

            Fenix::redirect('catalog/material/item/sid/' . $this->_material->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый элемент"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {

        $currentItem = Fenix::getModel('catalog/material')->getItemById(
            $this->getRequest()->getParam('id')
        );


        if ($currentItem == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Слайд не найден"))
                    ->saveSession();

            Fenix::getUrl('catalog/material/item/sid/' . $this->_material->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Категория"),
                'uri'   => Fenix::getUrl('catalog/material'),
                'id'    => 'material'
            ),
            array(
                'label' => $this->_material->title,
                'uri'   => Fenix::getUrl('catalog/material/item/sid/' . $this->_material->id),
                'id'    => 'material'
            ),
            array(
                'label' => Fenix::lang("Редактировать элемент"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentItem->toArray())
                   ->setData('current', $currentItem)
                   ->setData('material',  $this->_material);
        
        // Источник
        $Form      ->setSource('catalog/material_item', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            //$id = $Form ->editRecord($currentItem, $this->getRequest());
             Fenix::getModel('catalog/material')->editItem($Form, $currentItem, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Категория отредактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/material/item/sid/' . $this->_material->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/material/item/add/sid/' . $this->_material->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/material/item/edit/sid/' . $this->_material->id . '/id/' . $currentItem->id);
            }

            Fenix::redirect('catalog/material/item/sid/' . $this->_material->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать элементер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentItem = Fenix::getModel('catalog/material')->getItemById(
            $this->getRequest()->getParam('id')
        );

        if ($currentItem == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('catalog/material/item/sid/' . $this->_material->id);
        }

        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('catalog/material_item', 'default')
                 ->deleteRecord($currentItem);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Слайд удалён"))
                 ->saveSession();

        Fenix::redirect('catalog/material/item/sid/' . $this->_material->id);
    }
}