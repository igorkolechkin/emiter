<?php
class Fenix_Catalog_Controller_Admin_Attributes extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('catalog/attributes');

        $this->_engine->prepare()
             ->execute();
    }

    public function indexAction()
    {
        Fenix::getModel('catalog/backend_attributes')->updateAttributesValuesTable();

        $attributesList = Fenix::getModel('catalog/backend_attributes')->getAttributesListAsSelect();

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['attributesList'])) {
                foreach ((array) $rows['attributesList'] AS $_rowId) {
                    $currentAttribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById(
                        $_rowId
                    );
                    if ($currentAttribute->is_system != '1') {
                        Fenix::getModel('catalog/backend_attributes')->deleteAttribute(
                            $currentAttribute
                        );
                    }
                }

                Fenix::redirect('catalog/attributes');
            }
        }

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор тип атрибута"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('catalog/attributes/add/type/{$attributeset}')
        )));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты товаров"),
                'uri'   => '',
                'id'    => 'attributes'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Новый атрибут"))
                                         ->appendClass('btn-primary')
                                         ->setType('button')
                                         ->setOnclick($Dialog->toButton())
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Атрибуты товаров"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('attributesList')
                           ->setTitle(Fenix::lang("Управление атрибутами товаров"))
                           ->setData($attributesList)
                           ->setCheckall(array('name' => 'row'))
                           ->setStandartButtonset()
                           ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('split_by_lang', function($value, $data, $column, $table){
                               if ($value == '1')
                                   return 'Да';
                               else
                                   return 'Нет';
                           });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'sort-by-attributes',
            'title' => Fenix::lang("Сортировка значений"),
            'url'   => Fenix::getUrl('catalog/attributes/values/sorting/id/{$data->id}')
        ));

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'list',
            'title' => Fenix::lang("Значения"),
            'url'   => Fenix::getUrl('catalog/attributes/values/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('catalog/attributes/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type' => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'  => Fenix::getUrl('catalog/attributes/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Атрибуты"));
        
        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('catalog/attributes')
        ));
    }
    
    public function addAction()
    {
        $attributeset = ($this->getRequest()->getParam('type') == null ? 'text' : $this->getRequest()->getParam('type'));

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/attributes'),
                'id'    => 'catalog'
            ),
            array(
                'label' => Fenix::lang("Новый атрибут"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        // Источник
        $Form      ->setSource('catalog/attributes', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_attributes')->addAttribute(
                $Form,
                $attributeset,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут добавлен"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributes/add/type/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributes/edit/id/' . $id . '/type/' . $attributeset);
            }

            Fenix::redirect('catalog/attributes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentAttribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttribute == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                    ->saveSession();
            
            Fenix::redirect('catalog/attributes');
        }
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('catalog/attributes'),
                'id'    => 'attributes'
            ),
            array(
                'label' => Fenix::lang("Редактировать атрибут"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentAttribute->toArray())
                   ->setData('current', $currentAttribute);
        
        // Источник
        $Form      ->setSource('catalog/attributes', $currentAttribute->type)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            
            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('catalog/backend_attributes')->editAttribute(
                $Form,
                $currentAttribute,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут изменен"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('catalog/attributes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('catalog/attributes/add/type/' . $currentAttribute->type);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('catalog/attributes/edit/id/' . $currentAttribute->id . '/type/' . $currentAttribute->type);
            }

            Fenix::redirect('catalog/attributes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentAttribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById(
            $this->getRequest()->getParam('id')
        );

        if ($currentAttribute == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранный Вами атрибут не найден"))
                ->saveSession();

            Fenix::redirect('catalog/attributes');
        }

        if ($currentAttribute->is_system == '1') {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Удаление системных атрибутов может привести к потери работоспособности системы"))
                ->saveSession();

            Fenix::redirect('catalog/attributes');
        }

        Fenix::getModel('catalog/backend_attributes')->deleteAttribute(
            $currentAttribute
        );
        
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили атрибут"))
            ->saveSession();
        
        Fenix::redirect('catalog/attributes');
    }
}