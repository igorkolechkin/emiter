<?php
class Fenix_Catalog_Controller_Set extends Fenix_Controller_Action
{
    public function indexAction($categoryInfo = null)
    {
        Fenix_Debug::log('index controller begin');
        /**
         * Обработка пост запросов
         */
/*        if ($this->getRequest()->isXmlHttpRequest()) {
            switch ($this->getRequest()->getPost('action')) {
                case 'updateFilter':
                    $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                        $this->getRequest()->getPost('attribute')
                    );

                    if ($attribute == null)
                        exit;

                    echo Fenix::getModel('catalog/filter')->prepareFilterUrl(
                        $attribute,
                        (object) array(
                            'value' => $this->getRequest()->getPost('min') . '-' . $this->getRequest()->getPost('max')
                        )
                    );
                    break;
                case 'load-more':
                    if ($categoryInfo) {
                        $currentCategory   = $categoryInfo->getCurrent();
                    } else {
                        $currentCategory   = null;
                    }

                    //Список товаров
                    $productsList = Fenix::getModel('catalog/products')->getProductsList($currentCategory);
                    $Creator = Fenix::getCreatorUI();
                    $Creator->getView()->assign(array(
                        'category'     => $currentCategory,
                        'productsList' => $productsList
                    ));
                    //Список товаров
                    echo $Creator ->getView()
                             ->render('catalog/load-more.phtml');
                    break;
            }
            exit;
        }*/

        if($categoryInfo){
            $currentCategory   = $categoryInfo->getCurrent();
            $currentNavigation = $categoryInfo->getNavigation();
        }else{
            $currentCategory   = null;
            $currentNavigation = null;
        }

        //Список комплектов
        $setList = Fenix::getModel('catalog/products_set')->getCategorySetList($currentCategory);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        if ($currentNavigation) {
            foreach ($currentNavigation AS $_category) {
                $_crumbs[] = array(
                    'label' => $_category->getTitle(),
                    'uri'   => $_category->getUrl(),
                    'id'    => 'category_' . $_category->getId()
                );
            }
        } else {
            $_crumbs[] = array(
                'label' => Fenix::lang("Каталог"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'catalog'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $currentCategory, $setList); //TODO требуется проверка

        $Creator->getView()->assign(array(
            'category'   => $currentCategory,
            'navigation' => $currentNavigation,
            'collection' => $categoryInfo,
            'setList'   => $setList
        ));

        if($categoryInfo)
        {
            //Список товаров
            $Creator ->setLayout()
                     ->render('catalog/list-set.phtml');
        }
        else
        {
            if(Fenix::getRequest()->getUriSegment(1)=='reviews')
            {
                //Список отзывов на товары
                $Creator ->setLayout()
                         ->render('catalog/reviews-list.phtml');
            }
            else{
                //Корень каталога
                $Creator ->setLayout()
                         ->render('catalog/root.phtml');
            }
        }
        Fenix_Debug::log('index controller end');
    }

    public function cardAction($set = null)
    {
        Fenix_Debug::log('index controller begin');
        /**
         * Обработка пост запросов
         */
        /*        if ($this->getRequest()->isXmlHttpRequest()) {
                    switch ($this->getRequest()->getPost('action')) {
                        case 'updateFilter':
                            $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                                $this->getRequest()->getPost('attribute')
                            );

                            if ($attribute == null)
                                exit;

                            echo Fenix::getModel('catalog/filter')->prepareFilterUrl(
                                $attribute,
                                (object) array(
                                    'value' => $this->getRequest()->getPost('min') . '-' . $this->getRequest()->getPost('max')
                                )
                            );
                            break;
                        case 'load-more':
                            if ($categoryInfo) {
                                $currentCategory   = $categoryInfo->getCurrent();
                            } else {
                                $currentCategory   = null;
                            }

                            //Список товаров
                            $productsList = Fenix::getModel('catalog/products')->getProductsList($currentCategory);
                            $Creator = Fenix::getCreatorUI();
                            $Creator->getView()->assign(array(
                                'category'     => $currentCategory,
                                'productsList' => $productsList
                            ));
                            //Список товаров
                            echo $Creator ->getView()
                                     ->render('catalog/load-more.phtml');
                            break;
                    }
                    exit;
                }*/

/*        if($categoryInfo){
            $currentCategory   = $categoryInfo->getCurrent();
            $currentNavigation = $categoryInfo->getNavigation();
        }else{
            $currentCategory   = null;
            $currentNavigation = null;
        }*/

        //Список комплектов
        //$setList = Fenix::getModel('catalog/products_set')->getCategorySetList($currentCategory);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        /*$_crumbs[] = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl('catalog'),
            'id'    => 'main'
        );*/
        $_crumbs[] = array(
            'label' => $set->title,
            'uri'   => Fenix::getUrl($set->url_key),
            'id'    => 'main'
        );
/*        if ($currentNavigation) {
            foreach ($currentNavigation AS $_category) {
                $_crumbs[] = array(
                    'label' => $_category->getTitle(),
                    'uri'   => $_category->getUrl(),
                    'id'    => 'category_' . $_category->getId()
                );
            }
        } else {
            $_crumbs[] = array(
                'label' => Fenix::lang("Каталог"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'catalog'
            );
        }*/

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();
        /*if ($currentCategory) {

            //Title страницы
            $Creator ->getView()
                     ->headTitle($currentCategory->getSeo()->getTitle());

            // Ключевые слова
            $keywords = $currentCategory->getSeo()->getKeywords();
            if ($keywords != null)
                $this->view->headMeta()->appendName("keywords", $keywords);

            // Описание
            $description = $currentCategory->getSeo()->getDescription();
            if ($description != null)
                $this->view->headMeta($description, "description");
        }
        else {
            //Сео шаблон для корня каталога
            $seo = Fenix::getModel('core/seo')->getSeoByName('catalog');
            if ($seo) {
                //Title страницы
                $Creator->getView()
                        ->headTitle(Fenix::getModel('core/seo')->parseString($seo->seo_title, array()));
                // Ключевые слова
                $keywords = Fenix::getModel('core/seo')->parseString($seo->seo_keywords, array());
                if ($keywords != null)
                    $this->view->headMeta()->appendName("keywords", $keywords);
                // Описание
                $description = Fenix::getModel('core/seo')->parseString($seo->seo_description, array());
                if ($description != null)
                    $this->view->headMeta($description, "description");
            } else {
                //Title страницы
                $Creator->getView()
                        ->headTitle(Fenix::lang("Каталог"));
            }
        }*/

        $setProducts = Fenix::getModel('catalog/products_set')->getProductsListBySetId($set->id);

        $Creator->getView()->assign(array(
            //'category'   => $currentCategory,
            //'navigation' => $currentNavigation,
            //'collection' => $categoryInfo,
            'setProducts' => $setProducts,
            'set'         => $set
        ));

        //Корень каталога
        $Creator ->setLayout()
                 ->render('catalog/products/card-set.phtml');
    }
}