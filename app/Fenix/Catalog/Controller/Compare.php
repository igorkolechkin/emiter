<?php
class Fenix_Catalog_Controller_Compare extends Fenix_Controller_Action
{
    public function indexAction(){
        $cache = Fenix::getModel('catalog/compare')->getCache();

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::lang("Сравнение"),
            'uri'   => Fenix::getUrl('catalog/compare'),
            'id'    => 'compare'
        );


        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        $Creator->getView()
                ->headTitle(Fenix::lang("Каталог"));

        if(Fenix::getRequest()->getParam('category') && isset($cache->categories[(int)Fenix::getRequest()->getParam('category')])){
            $Creator ->getView()->assign(array(
                'productsIdList'   => $cache->categories[(int)Fenix::getRequest()->getParam('category')]
            ));
        }else {
            Fenix::redirect('/');
            exit;/*$Creator ->getView()->assign(array(
                'productsIdList'   => array()
            ));*/
        }

        $Creator ->setLayout()
                 ->render('catalog/compare.phtml');
    }
    public function addAction(){

        $id = Fenix::getRequest()->getPost('id');
        Fenix::getModel('catalog/compare')->addProduct($id);

        echo 'ok';
    }
    public function deleteAction(){

        $id = Fenix::getRequest()->getPost('id');
        if($id){
            Fenix::getModel('catalog/compare')->deleteProduct($id);
        }

        $id = Fenix::getRequest()->getPost('category');
        if($id)
            Fenix::getModel('catalog/compare')->deleteCategory($id);


        echo 'ok';
    }
    public function removeAction(){

        $id = Fenix::getRequest()->getParam('id');

        Fenix::getModel('catalog/compare')->deleteProduct($id);

        echo 'ok';

        //Fenix::redirect('/catalog/compare/category/'.Fenix::getRequest()->getParam('category'));
    }
    public function sidebarAction(){
        echo $this->view->partial('catalog/compare-sidebar.phtml', array());
    }
}