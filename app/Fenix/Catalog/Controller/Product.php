<?php

class Fenix_Catalog_Controller_Product extends Fenix_Controller_Action
{
    public function indexAction($product)
    {
        $productCollection = Fenix::getCollection('catalog/products_product')->setProduct(
            $product->toArray()
        );

        Fenix::getModel('catalog/products')->saveViewed(
            $productCollection
        );

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        $currentCategory = null;
        foreach ($productCollection->getNavigation() AS $_category) {
            $_crumbs[] = array(
                'label' => $_category->getTitle(),
                'uri'   => $_category->url_key,
                'id'    => 'category_' . $_category->getId()
            );
            if (is_null($currentCategory)) {
                $currentCategory = $_category;
            }
        }

        $_crumbs[] = array(
            'label' => $productCollection->getTitle(),
            'uri'   => '',
            'id'    => 'product'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $productCollection);

        $this->view->assign(array(
            'product'    => $productCollection,
            'productRow' => $product,
            'category'   => $currentCategory,
            'utpItems'   => Fenix::getHelper('core/service_utp')->setUtpName('utp.on.hover')->getUtpItems()
        ));

        $videoReview = Fenix::getModel('articles/articles')->getArticleByProductId($productCollection->id);

        if ($videoReview instanceof Zend_Db_Table_Row) {
            $this->view->assign(array(
                'videoReview' => (object)$videoReview->toArray()
            ));
        }

        $Creator->setLayout()
            ->render('catalog/products/card.phtml');
    }
}