<?php

class Fenix_Catalog_Collection_Promo extends Fenix_Resource_Collection
{
    public function load($block)
    {
        if ($block == null) {
            return false;
        }

        $block = Fenix::getModel('catalog/repository_promo')->findBlockByName($block);

        if ($block == null) {
            return false;
        }

        return Fenix::getCollection('catalog/promo_block')->load($block->toArray());
    }
}