<?php

/**
 * Class Fenix_Catalog_Collection_Products
 */
class Fenix_Catalog_Collection_Products extends Fenix_Resource_Collection
{
    /**
     * @param Zend_Db_Table_Row $product
     * @return string
     */
    public function getUrl(Zend_Db_Table_Row $product)
    {

        return Fenix::getUrl($product->url_key);
    }

    /**
     * @param Zend_Db_Table_Row $product
     * @return string
     */
    public function getBuyUrl(Zend_Db_Table_Row $product)
    {

        return Fenix::getUrl('checkout/process/add/pid/' . $product->id);
    }

    /**
     * @param $products
     * @return Fenix_Object_Rowset
     */
    public function setList($products)
    {
        $data = array();

        foreach ($products AS $_product) {
            $_product = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());

            $data[] = $_product;
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }
}