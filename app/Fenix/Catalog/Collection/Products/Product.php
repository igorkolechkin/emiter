<?php

class Fenix_Catalog_Collection_Products_Product extends Fenix_Object
{
    use Fenix_Traits_ImageCollection;
    use Fenix_Traits_SeoCollection;

    public function setProduct($_product, $priceInfo = null)
    {
        $_product['url'] = Fenix::getUrl($_product['url_key']);
        $_product['url_relative'] = $_product['url_key'];
        $_product['buy'] = Fenix::getUrl('checkout/process/add/pid/' . $_product['id'] . '/');

        // Цены
        $discount = (double)$_product['discount'] ? (double)$_product['discount'] : (isset($_product['sale_discount']) ? (double)$_product['sale_discount'] : 0);
        $priceOld = Fenix::getModel('catalog/products')->getProductPrice($_product);
        if ($priceInfo) {
            $discount = $priceInfo->discount;
            $priceOld = $priceInfo->price_old;
        }

        $_product['unit_price'] = $priceOld;
        $_product['price'] = $priceOld - round($priceOld * $discount / 100);

        if ($discount > 0) {
            $_product['price_old'] = $priceOld;
        }

        $_product['price_formatted'] = str_replace(',', '.',
            Fenix::getHelper('catalog/decorator')->decoratePrice($_product['price']));
        if ($_product['price_old'] > 0) {
            $_product['price_old_formatted'] = Fenix::getHelper('catalog/decorator')->decoratePrice($_product['price_old']);
        }

        if (fmod($_product['price'], 1) > 0) {
            $_product['price'] = round($_product['price'], 2);
        }

        // Навигация по категориям
        $_product['navigation'] = Fenix::getCollection('catalog/categories')->getCategoryNavigationById(
            (int)$_product['parent']
        );

        $this->setData($_product);


        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {

        return Fenix::getUrl($this->getData('url_key'));
    }

    /**
     * @return string
     */
    public function getBuyUrl()
    {

        return Fenix::getUrl('checkout/process/add/pid/' . $this->getData('id'));
    }

    public function getGallery()
    {
        $_gallery = (array)unserialize($this->getData('gallery'));

        $data = array();
        foreach ($_gallery AS $_image) {
            if ($_image != '') {
                $data[] = Fenix::getCollection('catalog/products_gallery')->setImage($_image);
            }
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }

    /**
     * Сопутствующие товары
     *
     * @return Fenix_Object_Rowset
     */
    public function getRelated()
    {
        $_list = Fenix::getModel('catalog/backend_products')->getRelatedList(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }

    /**
     * Похожие
     *
     * @return Fenix_Object_Rowset
     */
    public function getUpsell()
    {
        $_list = Fenix::getModel('catalog/backend_products')->getUpsellList(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }

    /**
     * Наборы
     *
     * @return Fenix_Object_Rowset
     */
    public function getSetList()
    {
        $_list = Fenix::getModel('catalog/backend_products')->getSetList(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }

    public function getSet2List()
    {
        $_list = Fenix::getModel('catalog/backend_products')->getSet2List(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }

    /**
     * Характеристики для списка
     *
     * @return Fenix_Object_Rowset
     */
    public function getListFeatures()
    {
        $features = Fenix::getModel('catalog/products')->getListAttributes(
            $this->getData('parent')
        );

        $Result = array();
        $Lang = Fenix_Language::getInstance();

        foreach ($features AS $_feature) {

            $attributeName = ($_feature->split_by_lang == '1' ? $_feature->sys_title . '_' . $Lang->getCurrentLanguage()->name : $_feature->sys_title);

            if (isset($_feature->attribute_source) && $_feature->attribute_source == 'system') {
                $value = $this->getData($attributeName);
                if (empty($value)) {
                    continue;
                }
            } else {
                $value_record = Fenix::getModel('catalog/products_attributes')->getAttributeValue($_feature,
                    $this->getData('id'));
                if (empty($value_record)) {
                    continue;
                }

                $value = Fenix::getModel('catalog/products_attributes')->getAttributeValueById($_feature,
                    $value_record->value_id);
                $value = $value->content;
            }

            $Result[] = array(
                'name'    => $_feature->title,
                'details' => $_feature->details,
                'value'   => $value,
                'image'   => $_feature->image
            );
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }

    /**
     * Характеристики для страницы товара
     *
     * @return Fenix_Object_Rowset
     */

    public function getCardFeatures()
    {
        $features = Fenix::getModel('catalog/products')->getCardAttributes(
            $this->getData('parent')
        );

        $Result = array();
        $Lang = Fenix_Language::getInstance();

        foreach ($features AS $_feature) {
            $attributeName = ($_feature->split_by_lang == '1' ? $_feature->sys_title . '_' . $Lang->getCurrentLanguage()->name : $_feature->sys_title);
            if ($_feature->attribute_source == 'system') {
                $value = $this->getData($attributeName);
                if (empty($value)) {
                    continue;
                }
            } else {
                $value_record = Fenix::getModel('catalog/products_attributes')->getAttributeValue($_feature,
                    $this->getData('id'));
                if (empty($value_record)) {
                    continue;
                }

                $value = Fenix::getModel('catalog/products_attributes')->getAttributeValueById($_feature,
                    $value_record->value_id);
                $value = $value->content;
            }

            $Result[] = array(
                'sys_title' => $_feature->sys_title,
                'title'     => $_feature->title,
                'details'   => $_feature->details,
                'unit'      => $_feature->unit,
                'value'     => $value
            );
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }

    /**
     * Характеристики для страницы товара
     *
     * @return Fenix_Object_Rowset
     */
    public function getCardFeaturesShort()
    {
        $features = Fenix::getModel('catalog/products')->getCardAttributesShort(
            $this->getData('parent')
        );

        $Result = array();
        $Lang = Fenix_Language::getInstance();

        foreach ($features AS $_feature) {
            $attributeName = ($_feature->split_by_lang == '1' ? $_feature->sys_title . '_' . $Lang->getCurrentLanguage()->name : $_feature->sys_title);
            if ($_feature->attribute_source == 'system') {
                $value = $this->getData($attributeName);
            } else {
                $value_record = Fenix::getModel('catalog/products_attributes')->getAttributeValue($_feature,
                    $this->getData('id'));
                if (empty($value_record)) {
                    continue;
                }

                $value = Fenix::getModel('catalog/products_attributes')->getAttributeValueById($_feature,
                    $value_record->value_id);
                $value = $value->content;
            }

            $Result[] = array(
                'sys_title' => $_feature->sys_title,
                'title'     => $_feature->title,
                'details'   => $_feature->details,
                'unit'      => $_feature->unit,
                'value'     => $value
            );
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }

    /**
     * Тейтинг товара
     *
     * @return Fenix_Object
     */
    public function getRating()
    {
        $average = Fenix::getModel('catalog/reviews')->getAverageRating(
            $this->getData('id')
        );

        $Result = new Fenix_Object(array(
            'data' => ($average == null ? array() : $average->toArray())
        ));

        return $Result;
    }

    public function getReviews()
    {

        if ($this->getData('group_id') && $this->getData('group_id') != 0) {
            $reviews = Fenix::getModel('catalog/reviews')->getReviewsListByGroupId(
                $this->getData('group_id')
            );
        } else {
            $reviews = Fenix::getModel('catalog/reviews')->getReviewsList(
                $this->getData('id')
            );
        }

        return $reviews;
    }

    /**
     * Сео штучки для товара
     */
    public function getSeo($_product = null)
    {
        if ($_product == null) {
            $_product = $this->toArray();
        }

        $navigation = $_product['navigation'];

        //берем первую категорию из навигации
        $root = $navigation->offsetGet(0);

        //получаем тайтл родителя
        $category = $navigation->offsetGet($navigation->count() - 1);


        $_product['root_title'] = $root->title;
        $_product['category_title'] = $category->title;

        $this->getSeoAttributes($_product);

        return $this->getSeoMetadata($_product, 'catalog_products');
    }


    /**
     *
     * Добавляем значения пользовательских аттрибутов которые используются в сео фильтре
     *
     * @param $_product
     */
    private function getSeoAttributes(&$_product)
    {
        $attributes = Fenix::getModel('catalog/products')->getCardSeoAttributes($_product['parent']);

        foreach ($attributes AS $_attribute) {
            $value_record = Fenix::getModel('catalog/products_attributes')->getAttributeValue($_attribute,
                $this->getData('id'));
            if (empty($value_record)) {
                continue;
            }
            $_value = Fenix::getModel('catalog/products_attributes')->getAttributeValueById($_attribute,
                $value_record->value_id);
            $value = $_value->content;


            $_product[$_attribute->sys_title] = $value;
        }
    }


    public function getMultiSetList($in_stock = null)
    {

        $_list = Fenix::getModel('catalog/backend_products')->getMultiSetList(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )), $in_stock);
        $result = $_list;

// Проверка наличия товаров в комплекте
        /*   $_list = $_list->toArray();
           foreach($_list as $i => $_set){
               $inStock = Fenix::getModel('catalog/backend_products')->checkMultiSet($_set['id']);
               if($inStock ==false){
                   unset($_list[$i]);
               }
           }

           $result = new Zend_Db_Table_Rowset(array(
               'data'=>$_list
           ));
   */

        /*$_list = Fenix::getModel('catalog/backend_products')->getSet2List(new Zend_db_Table_Row(array(
            'data' => $this->getData()
        )));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));*/

        return $result;
    }

    public function getMultiSetProducts($multisetId)
    {
        $_list = Fenix::getModel('catalog/backend_products')->getMultiSetProductsList($multisetId);
        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }

    public function getSetPrice($set, $products)
    {
        if ($set->set_price != 0) {
            return $set->set_price;
        }

        $totalDiscount = 0;
        $totalNoDiscount = 0;

        if ($this->getDiscount() > 0) {
            $totalDiscount += $this->getPrice();
        } else {
            $totalNoDiscount += $this->getPrice();
        }

        foreach ($products as $_product) {
            if ($_product->getDiscount() > 0) {
                $totalDiscount += $_product->getPrice();

            } else {
                $totalNoDiscount += $_product->getPrice();
            }
        }

        if ($set->set_discount != 0) {
            return round($totalDiscount + $totalNoDiscount - $totalNoDiscount * $set->set_discount / 100);
        } else {
            $price = $totalDiscount + $totalNoDiscount;

            if ($totalNoDiscount >= Fenix::getConfig('multiset_discount_limit_1')) {
                $price = round($totalDiscount + $totalNoDiscount - $totalNoDiscount * Fenix::getConfig('multiset_discount_percent_1') / 100);
            }

            if ($totalNoDiscount >= Fenix::getConfig('multiset_discount_limit_2')) {
                $price = round($totalDiscount + $totalNoDiscount - $totalNoDiscount * Fenix::getConfig('multiset_discount_percent_2') / 100);
            }

            if ($totalNoDiscount >= Fenix::getConfig('multiset_discount_limit_3')) {
                $price = round($totalDiscount + $totalNoDiscount - $totalNoDiscount * Fenix::getConfig('multiset_discount_percent_3') / 100);
            }
        }

        //Fenix::dump($cache, $price, $totalDiscount ,$totalNoDiscount ,$totalNoDiscount);
        return $price;
    }

    public function getSetPriceOld($set, $products)
    {


        $totalDiscount = 0;
        $totalNoDiscount = 0;

        if ($this->getDiscount() > 0) {
            $totalDiscount += (int)$this->getPriceOld();
        } else {
            $totalNoDiscount += (int)$this->getPrice();
        }

        foreach ($products as $_product) {
            if ($_product->getDiscount() > 0) {
                $totalDiscount += (int)$_product->getPriceOld();
            } else {
                $totalNoDiscount += (int)$_product->getPrice();
            }
        }

        $total = $totalDiscount + $totalNoDiscount;

        return $total;
    }

    /**
     * Цена комплекта для корзины и заказа
     * @param $set
     * @param $products
     * @return float
     */
    public function getCheckoutSetPrice($set, $products)
    {
        if ($set->set_price != 0) {
            return $set->set_price;
        }

        $total = $this->getPrice();

        foreach ($products as $_product) {
            $total += $_product->getPrice();
        }

        if ($set->set_discount != 0) {
            $price = round($total - $total * $set->set_discount / 100);
        } else {
            $price = $total;
        }

        return $price;
    }

    /**
     *
     * Получение формы отзыва для товара
     *
     * @return mixed
     */
    public function getReviewForm()
    {
        return Fenix::getHelper('catalog/reviews')->getReviewForm($this->getData('id'))->fetch();
    }
}