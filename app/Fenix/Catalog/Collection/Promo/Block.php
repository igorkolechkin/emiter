<?php

class Fenix_Catalog_Collection_Promo_Block extends Fenix_Object
{
    public function load($_block)
    {
        $this->setData($_block);

        return $this;
    }

    /**
     * Товары
     *
     * @return Fenix_Object_Rowset
     */
    public function getProducts()
    {
        $_list = Fenix::getModel('catalog/repository_promo')->findProductsByBlockId($this->getData('id'));

        $products = array();
        foreach ($_list AS $_product) {
            $products[] = Fenix::getCollection('catalog/products_product')->setProduct($_product->toArray());
        }

        $Result = new Fenix_Object_Rowset(array(
            'data'     => $products,
            'rowClass' => ''
        ));

        return $Result;
    }
}