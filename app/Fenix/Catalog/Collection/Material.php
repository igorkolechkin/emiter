<?php
class Fenix_Catalog_Collection_Material extends Fenix_Resource_Collection
{
    public function getMaterial($name)
    {
        $Material = Fenix::getModel('catalog/material')->getMaterialByName($name);

        if ($Material == null) {
            return false;
        }

        $_material = $Material->toArray();
        $_material['item_list'] = $this->_getItemsList($Material);

        return new Fenix_Object(array(
            'data' => $_material
        ));
    }
    public function getMaterialbyId($id)
    {
        $Material = Fenix::getModel('catalog/material')->getMaterialById($id);

        if ($Material == null) {
            return false;
        }

        $_material = $Material->toArray();
        $_material['item_list'] = $this->_getItemsList($Material);

        return new Fenix_Object(array(
            'data' => $_material
        ));
    }
    private function _getItemsList($Material, $productId = null)
    {
        $blockList = Fenix::getModel('catalog/material')->getPublicItemList($Material->id, $productId);

        $Result    = array();
        foreach ($blockList AS $_slide) {
            $_tmp          = $_slide->toArray();

            if ($_slide->image != '') {
                //$image         = HOME_DIR_URL . $_tmp['image'];
                //Fenix::dump($_tmp['image']);
                $image         = Fenix_Image::adapt(HOME_DIR_ABSOLUTE . $_slide->image,100,100);
                $_tmp['image'] = $image;
            }

            if ($_slide->image) {
                //$image         = HOME_DIR_URL . $_tmp['image_zoom'];
                //$image         = HOME_DIR_URL . $_tmp['image'];
                $image         = Fenix_Image::adapt(HOME_DIR_ABSOLUTE . $_slide->image,200,200);
                $_tmp['image_zoom'] = $image;
            }


            $Result[]           = $_tmp;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }
    public function loadProductMaterialList($productId, $parent = 0, $viewSection = null){

        //Список опубликованых материалов привязаных к товару
        $materialList = Fenix::getModel('catalog/material')->getProductMaterialList($productId, $parent, $viewSection);

        $Result = array();

        foreach($materialList as $material){

            $_materialData = $material->toArray();
            $_materialData['item_list'] = $this->_getItemsList($material, $productId);

            $Result[] = new Fenix_Object(array(
                'data' => $_materialData
            ));
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result,
            'rowClass'=>false
        ));
    }
}