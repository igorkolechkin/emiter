<?php
// TODO Сделать нормальную работу коллекций, как в товарах
class Fenix_Catalog_Collection_Categories extends Fenix_Resource_Collection
{
    public function getCategoryNavigationById($id)
    {
        Fenix_Debug::log('categories collection getCategoryNavigationById begin');

        $nav = Fenix::getModel('catalog/categories')->getCategoryNavigationById($id);

        $navigation = array();
        $url        = '';

        foreach ($nav AS $_category) {
            $url             = $_category['url_key'];

            $_category        = $_category->toArray();
            $_category['url'] = Fenix::getUrl($url);

            $navigation[] = $_category;
        }

        $Result = new Fenix_Object_Rowset(array(
            'data' => $navigation
        ));
        Fenix_Debug::log('categories collection getCategoryNavigationById end');

        return $Result;
    }
    public function getCategoryNavigationByCategory($category)
    {
        Fenix_Debug::log('categories collection getCategoryNavigationByCategory begin');

        $nav = Fenix::getModel('catalog/categories')->getCategoryNavigationByCategory($category);

        $navigation = array();

        $url        = '';
        foreach ($nav AS $_category) {
            $url = $_category['url_key'];

            $_category        = $_category->toArray();
            $_category['url'] = Fenix::getUrl($url);

            $navigation[] = $_category;
        }

        Fenix_Debug::log('categories collection getCategoryNavigationByCategory end');

        return new Fenix_Object_Rowset(array(
            'data' => $navigation
        ));
    }


    public function getCategoryById($id)
    {
        $_category = Fenix::getModel('catalog/categories')->getCategoryById($id);

        $_category = Fenix::getCollection('catalog/category_category')->setCategory($_category->toArray());

        return $_category;
    }

    public function getCategoriesList($parent = 1)
    {

        $this->_setSource('catalog/categories');
        $this->setParent($parent);
        $this->setIsPublic('1');
        $this->_setOrder('position asc');

        $result = $this->_fetchAll();
        $data   = array();

        foreach ($result AS $_category) {
            $data[] = Fenix::getCollection('catalog/category_category')->setCategory($_category->toArray());;
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }

    public function getCategoriesListOnMain($parent = 1)
    {

        $this->_setSource('catalog/categories');
        $this->setParent($parent);
        $this->setIsPublic('1');
        $this->setOnMain('1');
        $this->_setOrder('position asc');

        $result = $this->_fetchAll();
        $data = array();

        foreach ($result AS $_category) {
            $data[] = Fenix::getCollection('catalog/category_category')->setCategory($_category->toArray());;
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }
}