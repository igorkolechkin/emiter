<?php
class Fenix_Catalog_Collection_Category_Category extends Fenix_Object
{
    use Fenix_Traits_ImageCollection;
    use Fenix_Traits_SeoCollection;

    /**
     * Данные категории для колекции
     * @param $category
     * @return $this
     */
    public function setCategory($category)
    {
        $category['seo'] = $this->getSeoMetadata($category);
        $category['url'] = Fenix::getUrl($category['url_key']);

        $this->setData($category);

        return $this;
    }

    /**
     * Галерея
     * @return Fenix_Object_Rowset
     */
    public function getGallery()
    {
        $_gallery = (array) unserialize($this->getData('gallery'));

        $data = array();
        foreach ($_gallery AS $_image) {
            $data[] = Fenix::getCollection('catalog/products_gallery')->setImage($_image);
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }

    /**
     * Подготовка сео полей
     * @param array $page
     * @return Fenix_Object
     */
    public function getSeoMetadata(array $page)
    {
        $return = array();
        $seo = null;
	
	    // присутствуют ли какие либо $_GET параметры
	    $getParams = Fenix::getRequest()->getQuery();
	    $isGetParams = !empty($getParams);
	    
        /** Приоритет второй - то что указано в категори */
        $title = ($page['seo_title']) ? trim($page['seo_title']) : trim($page['title']);
        $h1 = ($page['seo_h1']) ? trim($page['seo_h1']): trim($page['title']);
        $keywords = trim($page['seo_keywords']);
        $description = trim($page['seo_description']);
        $seo_text = trim($page['seo_text']);

        /** Приоритет первый - если уаказано в СЕО Фильтре */
        $filter = Fenix::getRequest()->getParam('filter');

        if($filter !== null) {
            $seo = Fenix::getModel('seo/seo')->getActiveSeo($filter, $page['id']);
            //Если не нашли шаблон для значения, ищем общий для атрибута
            if($seo == null) {
                $filter_arr = Fenix::getModel('catalog/filter')->parseFilterString($filter);
                $filterAttributesSysTitle = array_keys($filter_arr);
                $firstAttrSysTitle = array_shift($filterAttributesSysTitle);
                $firstAttrValues = array_shift($filter_arr);
                if(count($firstAttrValues) == 1) {
                    $seo = Fenix::getModel('seo/attributes')->getAttributeSeo($firstAttrSysTitle, $page['id']);
                    if($seo) {
                        $filter_value = '';
                        $selectedAttributes = Fenix::getModel('catalog/filter')->getSelectedAttributes();
                        foreach($selectedAttributes as $attr) {
                            foreach($attr->selected_values AS $_value) {
                                /** доп параметр автозамены по атрибуту */
                                $page[$attr->sys_title] = $_value->content;

                                if($filter_value != '') {
                                    $filter_value .= ', ';
                                }
                                $filter_value .= $_value->content;
                            }
                        }
                        $page['filter_value'] = $filter_value;
                    }
                }
            }
        }

        /** Если есть информация высшего приоритета - заполняем */
        if($seo != null) {
            if(!empty($seo->seo_title)) {
                $title = $seo->seo_title;
            }
            if(!empty($seo->seo_h1)) {
                $h1 = $seo->seo_h1;
            }
            if(!empty($seo->seo_keywords)) {
                $keywords = $seo->seo_keywords;
            }
            if(!empty($seo->seo_description)) {
                $description = $seo->seo_description;
            }
            // настройки для СЕО текста ниже
        }

        /** Последний (самый низкий) приоритет - значения по умолчанию для категории */
        if(Fenix::getRequest()->getUriSegment(1) == 'multiset') {
            //Сео шаблон для ХЗ - оставил так
            $seo_def = Fenix::getModel('core/seo')->getSeoByName('catalog_multiset');
        } else {
            //если нет СЕО для категории то берем общее сео для каталога или корня
            $seo_def = Fenix::getModel('core/seo')->getSeoByCategoryId($page['id']);
            if($seo_def == null){
                if($page['id'] == 1){
                    //Сео шаблон для корня
                    $seo_def = Fenix::getModel('core/seo')->getSeoByName('catalog_root');
                }else{
                    //Сео шаблон для каталога
                    $seo_def = Fenix::getModel('core/seo')->getSeoByName('catalog_categories');
                }
            }
        }

        /** Если что-то осталось пустое - заполняем значениями "по умолчанию" */
        if($seo_def != null) {
            if(empty($title) && !empty($seo_def->seo_title)) {
                $title = $seo_def->seo_title;
            }
            if(empty($h1) && !empty($seo_def->seo_h1)) {
                $h1 = $seo_def->seo_h1;
            }
            if(empty($keywords) && !empty($seo_def->seo_keywords)) {
                $keywords = $seo_def->seo_keywords;
            }
            if(empty($description) && !empty($seo_def->seo_description)) {
                $description = $seo_def->seo_description;
            }
            // настройки для СЕО текста ниже
        }


        /** Если присутствуют какие-либо GET параметры*/
	    if(!$isGetParams) {
		    /** Если находимся на странице фильтра и для данного фильтра назначен свой SEO текст - выводим
		     *  В противном случае убираем SEO текст категории исключая дубли
		     */
		    if($filter) {
			    if($seo != null && !empty($seo->seo_text)) {
				    $seo_text = $seo->seo_text;
			    } else {
				    $seo_text = '';
			    }
		    } /** Если мы в категории и СЕО текст пустой - выводим "по умолчанию" для категории */ elseif(empty($seo_text) && $seo_def && !empty($seo_def->seo_text)) {
			    $seo_text = $seo_def->seo_text;
		    }
		
	    } else {
		    $seo_text = '';
	    }

        /** Доп параметры автозамены - %title% */
        if($page['parent'] > 1) {
            $main_category = Fenix::getModel('catalog/categories')->getCategoryById((int)$page['parent']);
            $page['main_category'] = ($main_category->seo_h1 != '') ? $main_category->seo_h1 : $main_category->title;
        } else {
            $page['main_category'] = '';
        }


        /** Формируем результатирующие данные */
        $return['title'] = Fenix::getModel('core/seo')->parseString($title, $page);
        $return['h1'] = Fenix::getModel('core/seo')->parseString($h1, $page);
        $return['keywords'] = Fenix::getModel('core/seo')->parseString($keywords, $page);
        $return['description'] = Fenix::getModel('core/seo')->parseString($description, $page);
        $return['seo_text'] = Fenix::getModel('core/seo')->parseString($seo_text, $page);

        return new Fenix_Object(array(
            'data' => $return
        ));
    }
}