<?php
class Fenix_Catalog_Helper_Reviews extends Fenix_Catalog_Helper_Decorator
{
    private $required = '<span class="required"></span>';

    public function getResponseForm($product, $review)
    {

        $Form= Fenix::getCreatorUI()->loadPlugin('Form');
        $Form->setId('reviewForm'.$review->id)
             ->enableAjax(true)
             ->setAction('/catalog/reviews/response');


        if ($Form->getRequest()->getQuery('email') != null) {
            $Form ->setDefaults(array(
                'email' => $Form->getRequest()->getQuery('email')
            ));
        }
        $Content = array();

        // Това на который отвечают
        $Field = $Form->loadPlugin('Form_Text')
                      ->setName("product_id")
                      ->setType("hidden")
                      ->setId("hidden")
                      ->setValue($product->getId())
                      ->fetch();

        $Content[] = $Field;

        // Комментарий на который отвечают
        $Field = $Form->loadPlugin('Form_Text')
                      ->setName("parent")
                      ->setType("hidden")
                      ->setId("parent")
                      ->setValue($review->id)
                      ->fetch();

        $Content[] = $Field;

        // Комментарий
        $Field     = $Form->loadPlugin('Form_Textarea')
                          ->setName("review")
                          ->setId("review")
                          ->setStyle("height:80px;")
                          ->setValue($Form->getRequest()->getPost('review'))
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Ваш ответ"). ':'.$this->required)
                          ->setContent($Field)
                          ->fetch();
        // Ваше имя
        $Field     = $Form->loadPlugin('Form_Text')
                          ->setName("name")
                          ->setId("name")
                          ->setValue($Form->getRequest()->getPost('name'))
                          ->setValidator(new Zend_Validate_NotEmpty())
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Ваше имя") . ':'. $this->required)
                          ->setContent($Field)
                          ->fetch();
        // E-mail
        $Field     = $Form->loadPlugin('Form_Text')
                          ->setType("email")
                          ->setName("email")
                          ->setId("email")
                          ->setValue($Form->getRequest()->getPost('email'))
                          ->setValidator(new Zend_Validate_NotEmpty())
                          ->setValidator(new Zend_Validate_EmailAddress())
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("E-mail") . ':'. $this->required)
                          ->setContent($Field)
                          ->fetch();

        // Кнопка
        $Field     = $Form->loadPlugin('Button')
                          ->setType("submit")
                          ->setClass('btn green form-send')
                          ->setValue(Fenix::lang("Добавить ответ"))
                          ->fetch();
        $Content[] = $Form->loadPlugin('Container')
                          ->setClass('buttons')
                          ->setContent($Field)
                          ->fetch();

        $Form ->setContent($Content);
        $Form ->compile();

        return $Form;
    }

    public function getReviewForm($product_id)
    {

        $Form= Fenix::getCreatorUI()->loadPlugin('Form');
        $Form->setId('reviewForm')
             ->enableAjax(true);


        if ($Form->getRequest()->getQuery('email') != null) {
            $Form ->setDefaults(array(
                'email' => $Form->getRequest()->getQuery('email')
            ));
        }

        $Content = array();

        //id товара
        $Field = $Form->loadPlugin('Form_Text')
            ->setName('product_id')
            ->setValue($product_id)
            ->setType('hidden')
            ->fetch();

        $Content[] = $Form->loadPlugin('Row')
            ->setContent($Field)
            ->fetch();

        //оценка
        $Field = $Form->loadPlugin('Form_Rating')
            ->setName('rating')
            ->setId('rating')
            ->setStars(5)
            ->fetch();

        $Content[] = $Form->loadPlugin('Row')
            ->setClass('rating-block')
            ->setLabel($this->required . Fenix::lang("E-mail"))
            ->setContent($Field)
            ->setLabel(Fenix::lang('Ваша оценка'))
            ->fetch();

        // Достоинства
        /*$Field     = $Form->loadPlugin('Form_Text')
                          ->setName("benefits")
                          ->setId("benefits")
                          ->setValue($Form->getRequest()->getPost('benefits'))
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Достоинства:"))
                          ->setContent($Field)
                          ->fetch();*/
        // Недостатки
        /*$Field     = $Form->loadPlugin('Form_Text')
                          ->setName("limitations")
                          ->setId("limitations")
                          ->setValue($Form->getRequest()->getPost('limitations'))
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Недостатки:"))
                          ->setContent($Field)
                          ->fetch();*/

        // Комментарий
        $Field     = $Form->loadPlugin('Form_Textarea')
                          ->setName("review")
                          ->setId("review")
                          ->setStyle("height:80px;")
                          ->setValue($Form->getRequest()->getPost('review'))
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Комментарий"). ':'. $this->required)
                          ->setContent($Field)
                          ->fetch();
        // Ваше имя
        $Field     = $Form->loadPlugin('Form_Text')
                          ->setName("name")
                          ->setId("name")
                          ->setValue($Form->getRequest()->getPost('name'))
                          ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorNotEmpty())
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("Ваше имя") . ':'. $this->required)
                          ->setContent($Field)
                          ->fetch();
        // E-mail
        $Field     = $Form->loadPlugin('Form_Text')
                          ->setName("email")
                          ->setId("email")
                          ->setValue($Form->getRequest()->getPost('email'))
                          ->setValidator(Fenix::getHelper('core/validation')->getFieldValidatorEmailAddress())
                          ->fetch();

        $Content[] = $Form->loadPlugin('Row')
                          ->setLabel(Fenix::lang("E-mail") . ':'. $this->required)
                          ->setContent($Field)
                          ->fetch();

        // Кнопка
        $Field     = $Form->loadPlugin('Button')
                          ->setType("submit")
                          ->setClass('btn green form-send')
                          ->setValue(Fenix::lang("Добавить отзыв"))
                          ->fetch();
        $Content[] = $Form->loadPlugin('Container')
                          ->setClass('buttons')
                          ->setContent($Field)
                          ->fetch();

        $Form ->setContent($Content);
        $Form ->compile();

        return $Form;
    }
}