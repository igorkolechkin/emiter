<?php
class Fenix_Catalog_Helper_Material extends Fenix_Catalog_Helper_Decorator
{
    public function getMaterialValueSelect($materialId, $materialCategoryId){
        $itemCategory = Fenix::getModel('catalog/material')->getMaterialById(
            (int) $this->getRequest()->getParam('sid')
        );
        if($itemCategory == null)
            return 'Не найдена родительская категория материала';



            $item         = Fenix::getModel('catalog/material')->getItemById($materialId);


            $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById($itemCategory->attribute_id);

            $values    = Fenix::getModel('catalog/backend_products')->getAttributeValues($attribute);

            $Content = array();
            $Field = Fenix::getCreatorUI()
                ->loadPlugin('Form_Select')
                ->setId('value_id')
                ->setName('value_id');

            foreach ($values as $value){
                $Field->addOption($value->id,$value->content);
            }

            if($item){
                $Field->setSelected($item->value_id);
            }

            return $Field->fetch();

    }

    public function getMaterialAttributeSelect($materialGroupId, $attribute_id)
    {
        $materialGroup         = Fenix::getModel('catalog/material')->getMaterialById($materialGroupId);


        $attributesList = Fenix::getModel('catalog/backend_attributes')->getAttributesList();

        $Field   = Fenix::getCreatorUI()
                        ->loadPlugin('Form_Select')
                        ->setId('attribute_id')
                        ->setName('attribute_id');
        foreach ($attributesList as $attribute) {
            $Field->addOption($attribute->id, $attribute->title);
        }
        if ($materialGroup) {

            $Field->setSelected($materialGroup->attribute_id);
        }
        return $Field->fetch();
    }
}