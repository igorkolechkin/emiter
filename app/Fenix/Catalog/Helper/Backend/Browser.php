<?php
class Fenix_Catalog_Helper_Backend_Browser extends Fenix_Resource_Helper
{
    public function getSimpleProductsFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/browser.phtml', $options);
    }
    public function getSetProductsFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/set.phtml', $options);
    }
    public function getMultiSetProductsFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/multiset.phtml', $options);
    }

    public function getOptionsFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/products/options.phtml', $options);
    }

    public function getSetFilter(array $options)
    {
        return Fenix::getCreatorUI()->getView()->partial('catalog/set_browser.phtml', $options);
    }
}