<?php
class Fenix_Catalog_Helper_Backend_Options extends Fenix_Resource_Helper
{
    /**
     * Таблица редактирования сложного товара
     * @param $currentProduct
     * @return string
     */
    public function getOptionsTable($currentId, $attributesetId = 0)
    {
        $language = Fenix_Language::getInstance();
        $optionsList = Fenix::getModel('catalog/backend_options')->getOptionsList();

        $activeOptionsIds = Fenix::getModel('catalog/backend_material')->getMaterialIdArray($currentId);

        $Creator = Fenix::getCreatorUI();

        $Creator = Fenix::getCreatorUI();

        //Готовим массив input'ов для атрибутов
        $inputList = array();
        foreach(Fenix_Language::getLanguagesList() as $lang){
            $inputList['title_'.$lang->name] = $Creator->loadPlugin('Form_Text')->setName('_configurable[' . 'title_'.$lang->name . '][]');
        }
       /* foreach($confAttrList as $attr){
            if($attr->split_by_lang =='1'){
                foreach($language->getLanguagesList() as $_lang){
                    $inputList[$attr->sys_title . '_' . $_lang->name] = $Creator->loadPlugin('Form_Text')->setName('_configurable[' . $attr->sys_title. '_' . $_lang->name . '][]');
                }
            }else{
                $inputList[$attr->sys_title] = $Creator->loadPlugin('Form_Text')->setName('_configurable[' . $attr->sys_title . '][]');
            }
        }*/

        //input для цен и скидок
        $inputGroupSku = $Creator->loadPlugin('Form_Text')->setName('_configurable[group_sku][]');
        $inputPrice     = $Creator->loadPlugin('Form_Text')->setName('_configurable[price][]');
        $slectType = $Creator->loadPlugin('Form_Select')
                             ->setName('_configurable[type][]')
                             ->addOption('+','+')
                             ->addOption('=','=')
                             ->addOption('-','-');

        //Размеры товаров и их цена
        $Table      = $Creator->loadPlugin('Table')->setId('configurableTable')->setStyle('width:100%');

        //Заголовки таблицы
       /* foreach($confAttrList as $attr){
            if($attr->split_by_lang =='1'){
                foreach($language->getLanguagesList() as $_lang){
                    $Table->addHeader($attr->sys_title . '_' . $_lang->name,
                        $attr->title . '(' . $_lang->label . ')' . ($attr->unit != '' ? ', ' . $attr->unit : ''));
                }
            }else{
                $Table->addHeader($attr->sys_title, $attr->title . ($attr->unit != '' ? ', ' . $attr->unit : ''));
            }

        }*/
        $Table->addHeader('type','Опция');
        $Table->addHeader('price','Цена, грн');

    /*    foreach ($configurableList as $i => $config) {
            foreach($confAttrList as $attr){
                $attrValue = Fenix::getModel('catalog/backend_products')->getAttributeValueById($attr, $config->{$attr->sys_title});

                if($attr->split_by_lang =='1'){
                    foreach($language->getLanguagesList() as $_lang){

                        $Table->addCell('row-' . $i,
                            $attr->sys_title . '_' . $_lang->name,
                            $inputList[$attr->sys_title . '_' . $_lang->name]->setValue($attrValue->{'content_' . $_lang->name})->fetch());
                    }
                }else{
                    $attrValue = Fenix::getModel('catalog/backend_products')->getAttributeValueById($attr, $config->{$attr->sys_title});

                    $Table->addCell('row-' . $i, $attr->sys_title,     $inputList[$attr->sys_title]->setValue($attrValue->content)->fetch());
                }

            }
            $Table->addCell('row-' . $i, 'type', $slectType->setSelected($config->type)->fetch());
            $Table->addCell('row-' . $i, 'price', $inputPrice->setValue($config->price)->fetch());
            $Table->addCell('row-' . $i, 'group_sku', $inputGroupSku->setValue($config->group_sku)->fetch());
        }*/





        $Creator->getView()->assign(array(
            'currentId'         => $currentId,
            'optionsList'       => $optionsList,
            'activeOptionsIds'  => $activeOptionsIds
        ));

        $Result = $Creator->getView()->render('catalog/products/options.phtml');

        return $Result;
    }
}