<?php
class Fenix_Catalog_Helper_Backend_Import extends Fenix_Resource_Helper
{
   public function getImportProductsForm(){
       $Form = Fenix::getCreatorUI()->loadPlugin('Form');
       $Form->setAction('/acp/catalog/import/products');
           $Row = array();

           // Событие
           $Row[] = $Form->loadPlugin('Events_Session')->fetch();
          /* $Row[] = $Form->loadPlugin('Row')
                         ->appendClass('form-title')
                         ->setLabel('<strong>Импорт товаров:</strong>')
                         ->fetch();*/

           // Имя
           $Field = $Form->loadPlugin('Form_File')
                         ->setName('import_file')
                         ->setId('import_file')
                         //->setValue($Form->getRequest()->getFiles('import_file'))
                         //->setValidator(new Zend_Validate_NotEmpty())
                         ->fetch();
           $Row[] = $Form->loadPlugin('Row')
                         ->setLabel(Fenix::lang("Файл товаров *.xls"))
                         ->setContent($Field)
                         ->fetch();

       // Родитель
       /*
       $categories = Fenix::getModel('catalog/backend_categories')->getCategoriesList(1);

       $Field = $Form->loadPlugin('Form_Select')
                        ->setId('parent')
                        ->setName('parent')
                        ->setSelected($Form->getRequest()->getPost('parent'));
       foreach ($categories AS $_category) {
           $Field->addOption($_category->id, $_category->title,'level-0');

           $subCategories = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_category->id);
           foreach ($subCategories AS $_subCategory) {
               $Field->addOption($_subCategory->id, '' . $_subCategory->title,'level-1');
               $subCategories2 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory->id);
               foreach ($subCategories2 AS $_subCategory2) {
                   $Field->addOption($_subCategory2->id, '' . $_subCategory2->title,'level-2');
               }
           }
       }
       $Row[] = $Form->loadPlugin('Row')
                     ->setLabel(Fenix::lang("Категория"))
                     ->setContent($Field->fetch())
                     ->fetch();

           //->setValue($Form->getRequest()->getFiles('import_file'))
           //->setValidator(new Zend_Validate_NotEmpty())
                     //->fetch();
       */


        // Кнопуля
           $Field = $Form->loadPlugin('Button')
                         ->setValue(Fenix::lang("Отправить"))
                         ->setType('submit')
                         ->fetch();
           $Row[] = $Form->loadPlugin('Row')
                         ->setLabel('&nbsp;')
                         ->setContent($Field)
                         ->fetch();

           // Контейнер
           $Form->setContent($Row);
           $Form->compile();

           return $Form;

   }
    public function getImportPriceForm(){
       $Form = Fenix::getCreatorUI()->loadPlugin('Form');
       $Form->setAction('/acp/catalog/import/pricebegin');
           $Row = array();

           // Событие
           $Row[] = $Form->loadPlugin('Events_Session')->fetch();

           // Файл
           $Field = $Form->loadPlugin('Form_File')
                         ->setName('import_file')
                         ->setId('import_file')
                         //->setValue($Form->getRequest()->getFiles('import_file'))
                         //->setValidator(new Zend_Validate_NotEmpty())
                         ->fetch();
           $Row[] = $Form->loadPlugin('Row')
                         ->setLabel(Fenix::lang("Файл товаров *.xls"))
                         ->setContent($Field)
                         ->fetch();

        // Кнопка
           $Field = $Form->loadPlugin('Button')
                         ->setValue(Fenix::lang("Отправить"))
                         ->setType('submit')
                         ->fetch();
           $Row[] = $Form->loadPlugin('Row')
                         ->setLabel('&nbsp;')
                         ->setContent($Field)
                         ->fetch();

           // Контейнер
           $Form->setContent($Row);
           $Form->compile();

           return $Form;

   }
}