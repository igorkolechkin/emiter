<?php
class Fenix_Catalog_Helper_Backend_Categories extends Fenix_Resource_Helper
{

    public function getCategoriSelect($parent){
        $categories = Fenix::getModel('catalog/backend_categories')->getCategoriesList(1);
        $Creator = Fenix::getCreatorUI();
        $Field = $Creator->loadPlugin('Form_Select')
                         ->setId('parent')
                         ->setName('parent')
                         ->setSelected($Creator->getRequest()->getParam('parent'));
        $Field->addOption(1, 'Корень','level-0');
        foreach ($categories AS $_category) {
            $Field->addOption($_category->id, $_category->title,'level-0');
            $subCategories = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_category->id);
            foreach ($subCategories AS $_subCategory) {
                $Field->addOption($_subCategory->id, '' . $_subCategory->title, 'level-1');
                $subCategories2 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory->id);
                foreach ($subCategories2 AS $_subCategory2) {
                    $Field->addOption($_subCategory2->id, '' . $_subCategory2->title,'level-2');
                    $subCategories3 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory2->id);
                    foreach ($subCategories3 AS $_subCategory3) {
                        $Field->addOption($_subCategory3->id, '' . $_subCategory3->title,'level-3');
                        //if((int)$_subCategory3->id == 1103)Fenix::dump($_subCategory3->id);
                        $subCategories4 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory3->id);
                        foreach ($subCategories4 AS $_subCategory4) {

                            $Field->addOption($_subCategory4->id, '' . $_subCategory4->title,'level-4');
                            $subCategories5 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory4->id);
                            foreach ($subCategories5 AS $_subCategory5) {
                                $Field->addOption($_subCategory5->id, '' . $_subCategory5->title,'level-5');
                                $subCategories6 = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_subCategory5->id);
                                foreach ($subCategories6 AS $_subCategory6) {
                                    $Field->addOption($_subCategory6->id, '' . $_subCategory6->title,'level-6');
                                }
                            }
                        }
                    }
                }
            }
        }

        return $Field->fetch();
    }
}