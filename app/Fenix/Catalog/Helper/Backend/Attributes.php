<?php

class Fenix_Catalog_Helper_Backend_Attributes extends Fenix_Resource_Helper
{
    public function getAttributeValuesTable($attributeId)
    {
        $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById((int)$attributeId);
        if ($attribute == null) {
            return null;
        }
        $page = Fenix::getRequest()->getQuery('page');

        $_values = Fenix::getModel('catalog/backend_attributes')->getAttributeValuesInfoPaginator($attribute, null,
            $page);

        $Creator = Fenix::getCreatorUI();

        //Таблица
        $Table = $Creator->loadPlugin('Table');
        $Table->setId('attributeValuesList');

        //Заголовки для таблицы
        $Table->addHeader('col-id', 'ID');
        $Table->addHeader('col-products', 'Товаров')->setStyle('text-align:center;');;
        if ($attribute->split_by_lang == '1') {
            $langList = Fenix_Language::getLanguagesList();
            foreach ($langList as $lang) {
                $Table->addHeader('col-content-' . $lang->name, 'Значение (' . $lang->title . ')');
            }
        } else {
            $Table->addHeader('col-content', 'Значение');
        }

        $Table->addHeader('col-content', 'Значение');
        $Table->addHeader('col-url', 'URL');
        $Table->addHeader('col-type', 'Тип');
        $Table->addHeader('col-property', 'Свойство');
        //$Table->addHeader('col-position', 'Сортировка');
        $Table->addHeader('col-delete', 'Удаление');

        //Селект для выбора типа
        $selectType = $Creator->loadPlugin('Form_Select');
        $selectType->addOption('none', 'По умолчанию');
        $selectType->addOption('image', 'Изображение');
        $selectType->addOption('color', 'Цвет');
        //Заполняем строки таблицы
        foreach ($_values as $i => $value) {

            $inputContent = $Creator->loadPlugin('Form_Text');
            $inputUrl = $Creator->loadPlugin('Form_Text')->setName('values_url[' . $value->id . ']');
            $selectType->setName('values_type[' . $value->id . ']');
            $inputProperty = $Creator->loadPlugin('Form_Text')->setName('values_property[' . $value->id . ']');
            $inputPosition = $Creator->loadPlugin('Form_Text')->setName('values_position[' . $value->id . ']')->setType('text');


            $inputPosition->setValue($value->position);
            $Table->addCell('row-' . $i, 'col-id', $value->id);
            $Table->addCell('row-' . $i, 'col-products', $value->relations_count)->setStyle('text-align:center;');

            if ($attribute->split_by_lang == '1') {
                $langList = Fenix_Language::getLanguagesList();
                foreach ($langList as $lang) {
                    $inputContent->setName('values_content_' . $lang->name . '[' . $value->id . ']')
                        ->setValue($value->{'content_' . $lang->name});
                    $Table->addCell('row-' . $i, 'col-content-' . $lang->name, $inputContent->fetch());
                }
            } else{
                $inputContent->setName('values_content[' . $value->id . ']')
                    ->setValue($value->content);
                $Table->addCell('row-' . $i, 'col-content', $inputContent->fetch());
            }
            $label = '';
            if ($value->url != '') {
                $url = $value->url;

            } else {
                $url = Fenix::stringProtectUrl($value->content);
                if ($value->content != '')
                    $label = '</br><span style="font-size: 12px;color:red;">*автозаполнение, необходимо сохранить</span>';
            }

            $inputUrl->setValue($url);
            $Table->addCell('row-' . $i, 'col-url', $inputUrl->fetch() . $label);

            $selectType->setId('ol-type-select' . $value->id)
                ->setSelected($value->type);
            $Table->addCell('row-' . $i, 'col-type', $selectType->fetch());

            $inputProperty->setValue($value->property);
            $Table->addCell('row-' . $i, 'col-property', $inputProperty->fetch());

            //$Table->addCell('row-' . $i, 'col-position', '<span class="attr-edit-plus" onclick="attrEditPositionMoveUp(this)">Вверх</span><span class="attr-edit-minus" onclick="attrEditPositionMoveDown(this)">Вниз</span>');
            $Table->addCell('row-' . $i, 'col-delete',
                '<span class="attr-edit-delete" onclick="attrEditDelete(this, \'' . $attribute->sys_title . '\',' . $value->id . ',\'' . Fenix::getRequest()->getPathInfo() . '?page=' . $page . '\')">Удалить</span>');
            //$List->addItem($content);
        }
        $script = '
        <script type="text/javascript">
            var attrColorHighLight ="#BEEBBE";
            function attrEditHighlightTr($tr){
                var color = $tr.find("td").eq(0).css("background");
                $tr.find("td").css("background", attrColorHighLight);
                setTimeout(function(){
                    $tr.find("td").css("background",color);
                },500);
            }
            function attrEditPositionMoveUp(elem){
                var $elem = $(elem);
                var $tr = $elem.closest("tr");
                var currentIndex = $("#attributeValuesList tr").index($tr);
                if(currentIndex>1){
                    var $positionInput = $("#attributeValuesList tr").eq(currentIndex  - 1).find("td").eq(0).find("input");
                    $positionInput.val(parseInt($positionInput.val())+1);

                    var $positionInput = $tr.find("td").eq(0).find("input");
                    $positionInput.val(parseInt($positionInput.val())-1);


                    $tr.insertBefore($("#attributeValuesList tr").eq(currentIndex - 1));
                    attrEditHighlightTr($tr);
                }
            }
            function attrEditPositionMoveDown(elem){
                var $elem = $(elem);
                var $tr = $elem.closest("tr");
                var currentIndex = $("#attributeValuesList tr").index($tr);
                console.log(currentIndex);
                console.log($("#attributeValuesList tr").length);
                console.log(currentIndex<$("#attributeValuesList tr").length-1);
                if(currentIndex<$("#attributeValuesList tr").length-1){

                    var $positionInput = $("#attributeValuesList tr").eq(currentIndex +1).find("td").eq(0).find("input");
                    $positionInput.val(parseInt($positionInput.val())-1);

                    var $positionInput = $tr.find("td").eq(0).find("input");
                    $positionInput.val(parseInt($positionInput.val())+1);

                    $tr.insertAfter($("#attributeValuesList tr").eq(currentIndex +1));
                    attrEditHighlightTr($tr);
                }
            }
            function attrEditDelete(elem, sys_title,value_id){
                $.ajax({
                    url : "' . Fenix::getUrl('catalog/attributes/values/delete') . '",
                    type: "POST",
                    data: {
                        sys_title:sys_title,
                        value_id:value_id
                    },
                    success: function(data){
                        var $elem = $(elem);
                        var $tr = $elem.closest("tr");
                        $tr.remove();
                    },
                    error  : function(){

                    }
                });
            }
        </script>';

        $paginator = Fenix::getCreatorUI()->getView()->paginationControl(
            $_values,
            'Elastic',
            'creator/paginator.phtml', array(
            'url'            => Fenix::getUrl('catalog/attributes/values/id/' . $attributeId) . '?page=%s',
            'paginator'      => $_values,
            'total'          => $_values->getTotalItemCount(),
            'currentPerPage' => 25
        ));

        return $Table->fetch() . $script . $paginator;
    }

    public function getAttributeValuesSorting($attributeId)
    {
        $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeById((int)$attributeId);
        if ($attribute == null) return null;
        $page = Fenix::getRequest()->getQuery('page');

        $_values = Fenix::getModel('catalog/backend_attributes')->getAttributeValuesInfo($attribute, null, $page);

        $liList = array();
        foreach ($_values as $value) {
            if ($value->content) {
                $liContent = '<span class="span4">' . $value->content . '</span>';
            } else {
                $liContent = '<span class="span4">(Пустое значение)</span>';
            }

            if (Fenix::isDev()) {
                $liContent .= '<span class="span2">' . $value->id . '</span>';
            }
            $liList[] = '<li data-value_id="' . $value->id . '" data-attribute_id="' . $attribute->id . '" class="" style="cursor:pointer; overflow:hidden;">' . $liContent . '</li>';
        }
        $resultHtml = '<div class="row-fluid">'
            . '<span class="span4">Значение</span>';

        if (Fenix::isDev()) {
            $resultHtml .= '<span class="span2">ID</span>';
        }
        $resultHtml .= '</div>'
            . '<ul class="item-list clearfix" id="valuesSortingList">' . implode('', $liList) . '</ul>';

        $script = '<script>
        $(function(){
            $("#valuesSortingList").sortable({
                    opacity:0.8,
                    revert:true,
                    forceHelperSize:true,
                    placeholder: "draggable-placeholder",
                    forcePlaceholderSize:true,
                    tolerance:"pointer",
                    axis: "y",
                    stop: function( event, ui ) {
                        $(ui.item).css("z-index", "auto");
                        console.log(ui.item);
                        var position = {};
                        $("#valuesSortingList li").each(function(index){
                            position[index]={
                                attribute_id: $(this).data("attribute_id"),
                                value_id: $(this).data("value_id"),
                            }
                        });

                         $.ajax({
                            url : "' . Fenix::getUrl('catalog/attributes/values/position') . '",
                            type: "POST",
                            data: {
                               position: position
                            },
                            beforeSend:function(){
                                $(ui.item).css("opacity","0.5");
                            },
                            success: function(data){
                                $(ui.item).css("opacity","1");
                            },
                            error  : function(){
                                $(ui.item).css("opacity","1");
                                $(ui.item).css("border","1px solid red");
                            }
                        });
                    }
                }
            );
        });
        function savePosition(attribute_id, value_id){

        }
        </script>';
        return $resultHtml . $script;

    }

    /**
     *
     * Формируем блок выбора типа данных ток для админов!!!!
     *
     * @param $type
     * @param null $current
     */
    public function getAttributeSqlFields($type, $sql_type = null, $sql_lenght = null)
    {
        $Creator = Fenix::getCreatorUI();
        $Fieldset = $Creator->loadPlugin('Fieldset');
        $Fieldset->setLabel('Настройки базы данных');

        //если зашел не разработчик то прячем поле и отображаем дефолтные значения
        if ( ! Fenix::isDev()) {
            $Fieldset->setStyle('display:none;');
        }

        $data_types_list = Fenix::getModel('catalog/backend_attributes')->getSqlDataTypeList();

        if ($sql_type == null) {
            $default_values = Fenix::getModel('catalog/backend_attributes')->getSqlDataByType($type);

            $sql_type = $default_values['sql_type'];
            $sql_lenght = $default_values['sql_lenght'];
        }


        $Select = $Creator->loadPlugin('Form_Select')
            ->setName('sql_type')
            ->setSelected(strtoupper($sql_type));

        //вносим типы данных в селект
        foreach ($data_types_list as $_data_type) {
            $_data_type = strtoupper($_data_type);
            $Select->addOption($_data_type, $_data_type);
        }

        $Row = array();
        $Row[] = $Creator->loadPlugin('Row')
            ->setLabel('Тип колонки')
            ->setContent($Select->fetch())
            ->fetch();

        $lenght_input = $Creator->loadPlugin('Form_Text')
            ->setName('sql_lenght')
            ->setValue($sql_lenght);

        $Row[] = $Creator->loadPlugin('Row')
            ->setLabel('Длина')
            ->setContent($lenght_input->fetch())
            ->fetch();

        return $Fieldset->setContent($Row)->fetch();

    }
}