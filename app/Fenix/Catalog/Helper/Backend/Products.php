<?php

class Fenix_Catalog_Helper_Backend_Products extends Fenix_Resource_Helper
{
    public function getProductsForm(array $options)
    {
        $isEdit = array_key_exists('current', $options);

        $Creator = Fenix::getCreatorUI();

        $Form = $Creator->loadPlugin('Form');
        $languageList = Fenix_Language::getInstance()->getLanguagesList();
        if ($isEdit) {
            $defaults = array();
            $productData = $options['current']->toArray();
            $productId = $productData['id'];

            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm getActiveAttributesListForProductForm begin');
            $userAttributes = Fenix::getModel('catalog/backend_attributes')->getActiveAttributesNameListForProductForm();
            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm getActiveAttributesListForProductForm end');

            //Обрабатываем поля товара и добавляем префиксы для атрибутов системный/пользовательский
            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm array_merge($productData, $userAttributes) begin');
            $attributes_list = array_merge($productData, $userAttributes);
            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm array_merge($productData, $userAttributes) end');

            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm foreach($attributes_list as $key => $value)  begin');

            $attributes_list_arr = array();
            foreach ($attributes_list as $attrName => $value) {
                foreach ($languageList as $lang) {
                    $pos = strpos($attrName, '_' . $lang->name);
                    if ($pos > 0) {
                        $attrName = substr($attrName, 0, $pos);
                        break;
                    }
                }
                $attributes_list_arr[] = $attrName;
            }
            $attributes = Fenix::getModel('catalog/backend_attributes')->getAttributesListBySysTitle($attributes_list_arr);

            foreach ($attributes as $attribute) {
                $key = $attribute->sys_title;

                if (isset($userAttributes[$attribute->sys_title]) && $attribute->is_multiple == 1) {
                    $values = Fenix::getModel('catalog/backend_products')->getProductAttributeValues($attribute, $productId);
                    $valuesArr = array();

                    if (!empty($values)) {
                        foreach ($values as $value) {
                            if ($attribute->split_by_lang == '1') {
                                foreach ($languageList as $lang) {
                                    $valuesArr[$lang->name][] = (isset($value['content_' . $lang->name]) ? $value['content_' . $lang->name] : '');
                                    $defaults[Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER . '_' . $key . '_' . $lang->name] = implode(';', $valuesArr[$lang->name]);
                                }
                            } else {
                                $valuesArr['def'][] = (isset($value['content']) ? $value['content'] : '');
                                $defaults[Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER . '_' . $key] = implode(';', $valuesArr['def']);
                            }
                        }
                    }

                } else {

                    $value = Fenix::getModel('catalog/backend_products')->getProductAttributeValue($attribute, $productId);

                    if ($attribute->split_by_lang == '1') {
                        foreach ($languageList as $lang) {
                            $valueContent = (isset($value['content_' . $lang->name]) ? $value['content_' . $lang->name] : '');
                            $defaults[Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER . '_' . $key . '_' . $lang->name] = $valueContent;
                        }
                    } else {
                        $valueContent = (isset($value['content']) ? $value['content'] : '');
                        $defaults[Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_USER . '_' . $key] = $valueContent;
                    }
                }

                unset($attributes_list[$key]);

            }

            foreach ($attributes_list as $key => $value) {
                $defaults[Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM . '_' . $key] = $value;
            }

            Fenix_Debug::log('Fenix_Catalog_Helper_Backend_Products getProductsForm foreach($attributes_list as $key => $value)  end');

            $Form->setDefaults($defaults);
        } else {
            $Form->setDefaults(array(
                'is_public' => '1',
                'is_visible' => '1',
                'in_stock' => '1'
            ));
        }

        /*
         * Заголовок формы
         */
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Сохранить"))
                    ->appendClass('btn-success')
                    ->setType('submit')
                    ->setName('save')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Сохранить и создать"))
                    ->appendClass('btn-primary')
                    ->setType('submit')
                    ->setName('save_add')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Применить"))
                    ->appendClass('btn-warning')
                    ->setType('submit')
                    ->setName('apply')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Отменить"))
                    ->appendClass('btn-danger')
                    ->setType('reset')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Назад"))
                    ->appendClass('btn-inverse')
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/products/parent/' . $options['category']->id) . '\'')
                    ->fetch()
            ));

        if ($isEdit) {
            $pageTitle = Fenix::lang("Редактировать товар (%s)", $options['current']->title);
        } else {
            $pageTitle = Fenix::lang("Новый товар");
        }

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle($pageTitle)
            ->setButtonset($Buttonset->fetch());

        $Form->setContent($Title->fetch());

        // Выбранный набор атрибутов
        $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            $options['category']->attributeset_id
        );

        if ($attributeset == null) {
            $Form->setContent(
                $Creator->loadPlugin('Events')
                    ->appendClass('alert-danger')
                    ->setMessage(Fenix::lang("Выбранной категории не задан набор атрибутов"))
                    ->fetch()
            );
        } else {
            // Группы набора атрибутов
            $GroupsList = Fenix::getModel('catalog/backend_attributeset')->getGroupsList(
                $attributeset->id
            );

            $GroupsTabs = $Creator->loadPlugin('Tabs');//->setNavPosition('left')

            foreach ($GroupsList AS $i => $_group) {
                $Attributes = array();
                $AttributesList = Fenix::getModel('catalog/backend_attributeset')->getGroupAttributesList($_group->id);

                foreach ($AttributesList AS $_attribute) {
                    $Field = null;
                    $req = null;

                    $fieldName = $_attribute->attribute_source . '_' . $_attribute->sys_title;
                    switch ($_attribute->type) {
                        case 'text':
                        default:
                            $Field = $Creator->loadPlugin('Form_Text')
                                ->setName($fieldName)
                                ->setValue($Form->getRequest()->getPost($fieldName));

                            //для урла устанавливаем транслит на основе тайтла
                            if ($fieldName == 'system_url_key') {
                                $current = isset($options['current']) ? $options['current']->id : null;
                                $this->prepareUrlField($Field, $current);
                            }

                            if ($_attribute->is_autocomplete_in_admin == "1" || (isset($_attribute->is_multiple) && $_attribute->is_multiple == "1")) {
                                if ($_attribute->attribute_source == 'user') {
                                    $Field->setAutocompleteData(Fenix::getModel('catalog/backend_attributes')->getAutocompleteData($_attribute));
                                } else {
                                    $Field->setAutocompleteData(Fenix::getModel('catalog/backend_system_attributes')->getAutocompleteData($_attribute));
                                }
                            }

                            break;

                        case 'image':
                            $Field = $Creator->loadPlugin('Form_Image')
                                ->setId($fieldName)
                                ->setName($fieldName);

                            if ($isEdit) {
                                $Field->setImage($Form->getRequest()->getPost($fieldName));

                                if ($_attribute->attribute_source == Fenix_Catalog_Model_Backend_Attributes::ATTRIBUTE_SOURCE_SYSTEM) {
                                    $Field->setImageInfo($Form->getRequest()->getPost($fieldName . '_info'));
                                } else {
                                    $imageInfo = Fenix::getModel('catalog/backend_attributes')->getAttrImageInfo($_attribute->id, $productId);
                                    if ($imageInfo) {
                                        $Field->setImageInfo($imageInfo->content);
                                    }
                                }
                            }
                            $Field->setStorage('filesystem');
                            break;

                        case 'file':
                            $Field = $Creator->loadPlugin('Form_File')
                                ->setId($fieldName)
                                ->setName($fieldName)
                                ->setDetails($Form->getRequest()->getPost($fieldName));

                            if ($isEdit) {
                                $Field->setFile($Form->getRequest()->getPost($fieldName));
                            }
                            break;

                        case 'gallery':
                            $Field = $Creator->loadPlugin('Gallery')
                                ->setId($fieldName)
                                ->setName($fieldName);
                            $Field->addDropZone()
                                ->addPreviewsContainer()
                                ->addImagesContainer($Form->getRequest()->getPost($fieldName));
                            //$Field ->setImages($Form->getRequest()->getPost($_attribute->sys_title));
                            break;

                        case 'checkbox':
                            $Field = $Creator->loadPlugin('Form_Checkbox')
                                ->setName($fieldName)
                                ->setDetails($_attribute->title);
                            if ($Form->getRequest()->getPost($fieldName) == '1') {
                                $Field->setChecked('checked');
                            }

                            break;

                        case 'select':
                            $Field = $Creator->loadPlugin('Form_Select')
                                ->setId($fieldName)
                                ->setName($fieldName)
                                ->setStyle('width:150px;')
                                ->setSelected($Form->getRequest()->getPost($fieldName));

                            $values = (array)explode("\n", $_attribute->select_options);
                            $Field->addOption('', '');

                            foreach ($values AS $_value) {
                                $_value = trim($_value);
                                if ($_value != null) {
                                    $Field->addOption($_value, $_value);
                                }
                            }

                            break;

                        case 'textarea':
                            if ($_attribute->use_wysiwyg != '1') {
                                $Field = $Creator->loadPlugin('Form_Textarea')
                                    ->setName($fieldName)
                                    ->setValue($Form->getRequest()->getPost($fieldName));
                            } else {
                                $Field = $Creator->loadPlugin('Form_Wysiwyg')
                                    ->setName($fieldName)
                                    ->setValue($Form->getRequest()->getPost($fieldName));

                            }

                            $style = null;

                            if ($_attribute->field_width > 0) {
                                $style .= 'width:' . $_attribute->field_width . 'px;';
                            }
                            if ($_attribute->field_height > 0) {
                                $style .= 'height:' . $_attribute->field_height . 'px;';
                            }

                            $Field->setStyle($style);
                            break;
                    }

                    if ($_attribute->split_by_lang == '1') {
                        $Field->setSplitByLang();
                    }
                    if ($_attribute->is_required == '1') {
                        $req = ' *';
                        $Field->setValidator(new Zend_Validate_NotEmpty());
                    }
                    if (isset($_attribute->is_multiple) && $_attribute->is_multiple == '1') {
                        $Field->setMultiple(1);
                    }

                    $label = ($_attribute->type != 'checkbox' ? $req . $_attribute->title : '&nbsp;');

                    if ($_attribute->unit != null) {
                        $label .= ' (' . $_attribute->unit . ')';
                    }

                    $Row = $Creator->loadPlugin('Row')
                        ->setLabel($label)
                        ->setContent(($Field != null ? $Field->fetch() : null));

                    $Attributes[] = $Row->fetch();
                }


                $GroupsTabs->addTab('group_' . $_group->id, $_group->title, $Attributes);

                //Добавляем вкладку выбор категории, после первой вкладки "Общие"
                if ($i == 0) {
                    $GroupsTabs->addTab('group_' . $_group->id . '-2', 'Категории', self::renderCategoriesMultiSelect(($isEdit ? $options['current'] : false)));
                }
            }

            // Маркетинговые примочки
            $config = Fenix::getStaticConfig();
            if ($isEdit) {
                // Сложный товар
                if ($config->catalog->modules->configurable_products == '1') {
                    $current = isset($options['current']) ? $options['current'] : null;
                    $GroupsTabs->addTab(
                        'configurable',
                        Fenix::lang("Варианты товара"),
                        Fenix::getHelper('catalog/backend_configurable')->getConfigurableTable($isEdit ? $current->id : false, $attributeset->id)
                    );
                }
                if ($config->catalog->modules->configurable_products == '1') {
                    $current = isset($options['current']) ? $options['current'] : null;
                    $GroupsTabs->addTab(
                        'options',
                        Fenix::lang("Дополнительные опции"),
                        Fenix::getHelper('catalog/backend_browser')->getOptionsFilter(array(
                            'name' => 'options',
                            'url' => Fenix::getUrl('catalog/products/options'),
                            'products' => Fenix::getModel('catalog/backend_options')->getOptionsListByProductId(($isEdit ? $options['current']->id : false))
                        ))
                    );
                }
            }
            // Сопутствующие товары
            if ($config->catalog->modules->related_products == '1') {
                $GroupsTabs->addTab(
                    'related',
                    Fenix::lang("Сопутствующие"),
                    Fenix::getHelper('catalog/backend_browser')->getSimpleProductsFilter(array(
                        'name' => 'related',
                        'url' => Fenix::getUrl('catalog/products/find'),
                        'products' => Fenix::getModel('catalog/backend_products')->getRelatedList(($isEdit ? $options['current'] : false))
                    ))
                );
            }

            // Похожие товары
            if ($config->catalog->modules->upsell_products == '1') {
                $GroupsTabs->addTab(
                    'upsell',
                    Fenix::lang("Похожие"),
                    Fenix::getHelper('catalog/backend_browser')->getSimpleProductsFilter(array(
                        'name' => 'upsell',
                        'url' => Fenix::getUrl('catalog/products/find'),
                        'products' => Fenix::getModel('catalog/backend_products')->getUpsellList(($isEdit ? $options['current'] : false))
                    ))
                );
            }

            // Наборы товаров
            if ($config->catalog->modules->products_set == '1') {
                $GroupsTabs->addTab(
                    'set',
                    Fenix::lang("Комплекты"),
                    Fenix::getHelper('catalog/backend_browser')->getSetProductsFilter(array(
                        'name' => 'set',
                        'url' => Fenix::getUrl('catalog/products/find'),
                        'products' => Fenix::getModel('catalog/backend_products')->getSetList(($isEdit ? $options['current'] : false))
                    ))
                );
                /*  $GroupsTabs->addTab(
                      'set2',
                      Fenix::lang("Набор 2"),
                      Fenix::getHelper('catalog/backend_browser')->getSetProductsFilter(array(
                          'name'      => 'set2',
                          'url'       => Fenix::getUrl('catalog/products/find'),
                          'products'  => Fenix::getModel('catalog/backend_products')->getSet2List(($isEdit ? $options['current'] : false))
                      ))
                  );*/

            }

            $Form->setContent($GroupsTabs->fetch());
        }

        $Form->setContent($Buttonset->fetch());

        // Компиляция формы
        $Form->compile();

        return $Form;
    }

    /**
     * Рендер дерева каталога как мульти выбор
     *
     * @param $product
     * @return string
     */
    public function renderCategoriesMultiSelect($product) {

        $relationsIndex = array();
        if($product) {
            $relations = Fenix::getModel('catalog/backend_products')->getCategoriesRelations($product);
            if($relations->count() > 0) {
                foreach($relations as $categories) {
                    $relationsIndex[] = $categories->id;
                }
            }
        }
        $categories = Fenix::getModel('catalog/backend_categories')->getCategoriesList(1);

        $content = '';
        $content .= '<div class="row-fluid categories-multiselect">';
        $content .= '<div class="span6">';
        $content .= '<ul id="groups-categories" class="item-list groups-list">';

        $content .= $this->getCategoriesMultiSelectRecursion($categories, $relationsIndex);

        $content .= '</ul>';
        $content .= '</div>';
        $content .= '</div>';
        $content .= '<script>
                function toggleCategory(e,elem){
                    e.stopPropagation();
                    $(elem).find("input").not($(elem).find(".groups-categories input")).trigger("click");
                }
             </script>';

        return $content;
    }

    /**
     * Рендер дерева каталога как мульти выбор - доп рекурсия
     *
     * @param $categories
     * @param $relationsIndex
     * @return string
     */
    private function getCategoriesMultiSelectRecursion($categories, $relationsIndex) {
        $content = '';
        foreach($categories as $i => $_category) {
            $content .= '<li class="clearfix" style="position: relative;cursor: pointer;" onclick="toggleCategory(event,this);">';
            $content .= '<input type="checkbox" name="categories[]" value="' . $_category->id . '" ' . (in_array($_category->id, $relationsIndex) ? 'checked="checked"' : '') . ' class="gui-form-checkbox"/><span class="lbl"></span>';
            $content .= '<span style="margin-left:5px;">' . $_category->title . '</span>';
            $content .= '</li>';
            $subCategories = Fenix::getModel('catalog/backend_categories')->getCategoriesList($_category->id);

            if($subCategories->count() > 0) {
                $content .= '<li class="clearfix">';
                $content .= '<ul id="groups-categories" class="item-list groups-list">';
                foreach($subCategories as $j => $_subCategory) {
                    $content .= $this->getCategoriesMultiSelectRecursion($subCategories, $relationsIndex);
                }
                $content .= '</ul>';
                $content .= '</li>';
            }
        }

        return $content;
    }

    /**
     * Рендер дерева каталога как мульти выбор для сео шаблонов
     *
     * @param $product
     * @return string
     */
    public function renderSeoCategoriesMultiSelect($template_id)
    {
        $relationsIndex = array();
        $relations = Fenix::getModel('core/seo')->getTemplateCategoriesById($template_id);
        if($relations->count() > 0) {
            foreach($relations as $categories) {
                $relationsIndex[] = $categories->category_id;
            }
        }

        $categories = Fenix::getModel('catalog/backend_categories')->getCategoriesList(1);

        $content = '';
        $content .= '<div class="row-fluid categories-multiselect">';
        $content .= '<div class="span6">';
        $content .= '<ul id="groups-categories" class="item-list groups-list">';

        $content .= $this->getCategoriesMultiSelectRecursion($categories, $relationsIndex);

        $content .= '</ul>';
        $content .= '</div>';
        $content .= '</div>';
        $content .= '<script>
                function toggleCategory(e,elem){
                    e.stopPropagation();
                    $(elem).find("input").not($(elem).find(".groups-categories input")).trigger("click");
                }
             </script>';

        return $content;
    }

    public function getMaterialForm(array $options)
    {
        $isEdit = array_key_exists('current', $options);


        $Creator = Fenix::getCreatorUI();

        $Form = $Creator->loadPlugin('Form');

        if ($isEdit) {
            $defaults = $options['current']->toArray();

            $Form->setDefaults($defaults);
        } else {
            $Form->setDefaults(array(
                'is_public' => '1',
                'is_visible' => '1',
                'in_stock' => '1',
                'use_sitemap' => '1',
                'use_sitemap_images' => '1'
            ));
        }

        /*
         * Заголовок формы
         */
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Сохранить"))
                    ->appendClass('btn-success')
                    ->setType('submit')
                    ->setName('save')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Сохранить и создать"))
                    ->appendClass('btn-primary')
                    ->setType('submit')
                    ->setName('save_add')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Применить"))
                    ->appendClass('btn-warning')
                    ->setType('submit')
                    ->setName('apply')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Отменить"))
                    ->appendClass('btn-danger')
                    ->setType('reset')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Назад"))
                    ->appendClass('btn-inverse')
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('catalog/products/parent/' . $options['category']->id) . '\'')
                    ->fetch()
            ));

        if ($isEdit) {
            $pageTitle = Fenix::lang("Редактировать товар (" . $options['current']->sku . ") - " . $options['current']->title);
        } else {
            $pageTitle = Fenix::lang("Новый товар");
        }

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle($pageTitle)
            ->setButtonset($Buttonset->fetch());

        $Form->setContent($Title->fetch());

        // Выбранный набор атрибутов
        $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetById(
            (isset($options['category']) ? $options['category']->attributeset_id : 1)
        );

        if ($attributeset == null) {
            $Form->setContent(
                $Creator->loadPlugin('Events')
                    ->appendClass('alert-danger')
                    ->setMessage(Fenix::lang("Выбранной категории не задан набор атрибутов"))
                    ->fetch()
            );
        } else {
            $config = Fenix::getStaticConfig();

            $GroupsTabs = $Creator->loadPlugin('Tabs');
            // Материалы
            if ($config->catalog->modules->material == '1') {
                $current = isset($options['current']) ? $options['current'] : null;
                $GroupsTabs->addTab(
                    'material',
                    Fenix::lang("Материалы"),
                    Fenix::getHelper('catalog/backend_material')->getMaterialTable($isEdit ? $current->id : false, $attributeset->id)
                );
            }

            $Form->setContent($GroupsTabs->fetch());
        }

        $Form->setContent($Buttonset->fetch());

        // Компиляция формы
        $Form->compile();

        return $Form;
    }


    private function prepareUrlField($Field, $current = null)
    {
        $Field->setTranslit();
        $Field->setTranslitSource('system_title_russian');
        $Field->setUniqueChecker('catalog_products', 'url_key', $current);
    }
}