<?php
class Fenix_Catalog_Helper_Backend_Menu extends Fenix_Resource_Helper
{
    public function isRootActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'catalog';
    }
    public function isCatalogActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'catalog' && (
            Fenix::getRequest()->getUrlSegment(1) == 'parent' ||
            Fenix::getRequest()->getUrlSegment(1) == 'add' ||
            Fenix::getRequest()->getUrlSegment(1) == 'edit'
        );
    }
}