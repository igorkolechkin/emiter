<?php
class Fenix_Catalog_Helper_Backend_Emails extends Fenix_Resource_Helper
{
    public function getTable($customerList)
    {
        $Table = Fenix::getCreatorUI()->loadPlugin('Table_Db');
        $Table ->setTableId('cList');
        $Table ->setTitle(Fenix::lang("База E-mail"));
        $Table ->setData($customerList);

        // Id
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                  ->setId('id')
                  ->setTitle(Fenix::lang("ID"))
                  ->setSortable(true)
                  ->setFilter(array(
                      'type' => 'range',
                      'html' => 'text'
                  ))
                  ->setSqlCellName('id')
                  ->setSqlCellNameAs('id')
                  ->setHeaderAttributes(array(
                      'width' => '50'
                  ))
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('email')
                ->setTitle(Fenix::lang("Email"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('email')
                ->setSqlCellNameAs('email')
        );

        // Email
        $Table ->addColumn(
            $Table->loadPlugin('Table_Db_Column')
                ->setId('cellphone')
                ->setTitle(Fenix::lang("Телефон"))
                ->setSortable(true)
                ->setFilter(array(
                    'type' => 'single',
                    'html' => 'text'
                ))
                ->setSqlCellName('cellphone')
                ->setSqlCellNameAs('cellphone')
        );

        return $Table;
    }
}