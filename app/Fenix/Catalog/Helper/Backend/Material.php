<?php
class Fenix_Catalog_Helper_Backend_Material extends Fenix_Resource_Helper
{
    /**
     * Таблица редактирования сложного товара
     * @param $currentProduct
     * @return string
     */
    public function getMaterialTable($currentId, $attributesetId = 0)
    {
        $language = Fenix_Language::getInstance();
        $materialList = Fenix::getModel('catalog/material')->getMaterialList(0);

        $activeMaterialIds = Fenix::getModel('catalog/backend_material')->getMaterialIdArray($currentId);

        $Creator = Fenix::getCreatorUI();


        $Creator->getView()->assign(array(
            'currentId'         => $currentId,
            'materialList'      => $materialList,
            'activeMaterialIds' => $activeMaterialIds
        ));

        $Result = $Creator->getView()->render('catalog/products/material.phtml');

        return $Result;
    }
}