<?php

class Fenix_Catalog_Helper_Backend_Configurable extends Fenix_Resource_Helper
{
    /**
     * Таблица редактирования сложного товара
     * @param $currentProduct
     * @return string
     */
    public function getConfigurableTable($currentId, $attributesetId = 1)
    {
        $language = Fenix_Language::getInstance();
        //Список атрибутов для настраивомого товара
        //$confAttrList = Fenix::getModel('catalog/backend_attributes')->getConfigurableAttributesList($attributesetId);
        $confAttrList = Fenix::getModel('catalog/backend_configurable_attributes')->getAttributesList($currentId);

        //Список вариантов настраивомого товара
        $configurableList = Fenix::getModel('catalog/backend_configurable')->getConfigurable($currentId, false);

        $Creator = Fenix::getCreatorUI();

        //Готовим массив input'ов для атрибутов
        $inputList = array();

        foreach ($confAttrList as $attr) {
            if ($attr->split_by_lang == '1') {
                foreach ($language->getLanguagesList() as $_lang) {
                    $inputList[$attr->sys_title . '_' . $_lang->name] = $Creator->loadPlugin('Form_Text')->setName('_configurable[' . $attr->sys_title . '_' . $_lang->name . '][]');
                }
            }
            else {
                $inputList[$attr->sys_title] = $Creator->loadPlugin('Form_Text')->setName('_configurable[' . $attr->sys_title . '][]');
            }
        }
        //input для цен и скидок
        $inputGroupSku = $Creator->loadPlugin('Form_Text')->setName('_configurable[group_sku][]');
        $inputOldPrice = $Creator->loadPlugin('Form_Text')->setName('_configurable[price_old][]');
        $inputPrice = $Creator->loadPlugin('Form_Text')->setName('_configurable[price][]');
        $slectType = $Creator->loadPlugin('Form_Select')
            ->setName('_configurable[type][]')
            ->addOption('+', '+')
            ->addOption('=', '=')
            ->addOption('-', '-');

        //Размеры товаров и их цена
        $Table = $Creator->loadPlugin('Table')->setId('configurableTable')->setStyle('width:100%');

        //Заголовки таблицы
        foreach ($confAttrList as $attr) {
            if ($attr->split_by_lang == '1') {
                foreach ($language->getLanguagesList() as $_lang) {
                    $Table->addHeader($attr->sys_title . '_' . $_lang->name,
                        $attr->title . '(' . $_lang->label . ')' . ($attr->unit != '' ? ', ' . $attr->unit : ''));
                }
            }
            else {
                $Table->addHeader($attr->sys_title, $attr->title . ($attr->unit != '' ? ', ' . $attr->unit : ''));
            }

        }
        $Table->addHeader('type', 'Операция');
        $Table->addHeader('price_old', 'Старая цена, грн');
        $Table->addHeader('price', 'Цена, грн');
        $Table->addHeader('sku', 'Артикул');
        $Table->addHeader('position', 'Позиция');
        $Table->addHeader('remove', 'Удаление');


        foreach ($configurableList as $i => $config) {
            foreach ($confAttrList as $attr) {
                //Fenix::dump($config,$configurableList);
                if ($config->{$attr->sys_title}) {
                    try {
                        $attrValue = Fenix::getModel('catalog/backend_products')->getAttributeValueById($attr, $config->{$attr->sys_title});
                    }
                    catch (Exception $e) {

                        Fenix::dump($e->getMessage(), $config, $attr->sys_title);
                    }

                }
                else {
                    $attrValue = array();
                    foreach ($language->getLanguagesList() as $_lang) {
                        $attrValue['content_' . $_lang->name] = '';
                    }
                    $attrValue = (object) $attrValue;
                }


                if ($attr->split_by_lang == '1') {
                    foreach ($language->getLanguagesList() as $_lang) {

                        $Table->addCell('row-' . $i,
                            $attr->sys_title . '_' . $_lang->name,
                            $inputList[$attr->sys_title . '_' . $_lang->name]->setValue($attrValue->{'content_' . $_lang->name})->fetch());
                    }
                }
                else {
                    if ($config->{$attr->sys_title}) {
                        try {
                            $attrValue = Fenix::getModel('catalog/backend_products')->getAttributeValueById($attr, $config->{$attr->sys_title});
                        }
                        catch (Exception $e) {

                            Fenix::dump($e->getMessage(), $config, $attr->sys_title);
                        }

                    }
                    else {
                        $attrValue = (object) array('content' => '');
                    }

                    $Table->addCell('row-' . $i, $attr->sys_title, $inputList[$attr->sys_title]->setValue($attrValue->content)->fetch());
                }

            }

            $Table->addCell('row-' . $i, 'type', $slectType->setSelected($config->type)->fetch());
            $Table->addCell('row-' . $i, 'price_old', $inputOldPrice->setValue($config->price_old)->fetch());
            $Table->addCell('row-' . $i, 'price', $inputPrice->setValue($config->price)->fetch());
            $Table->addCell('row-' . $i, 'group_sku', $inputGroupSku->setValue($config->group_sku)->fetch());
            $Table->addCell('row-' . $i, 'col-position', '<span class="attr-edit-plus" onclick="attrEditPositionMoveUp(this)">Вверх</span><span class="attr-edit-minus" onclick="attrEditPositionMoveDown(this)">Вниз</span>');
            $Table->addCell('row-' . $i, 'col-delete', '<span class="attr-edit-delete" onclick="attrEditDelete(this)">Удалить</span>');
        }

        $Creator->getView()->assign(array(
            'currentId'        => $currentId,
            'attribute_set_id' => $attributesetId,
            'configurableList' => $configurableList,
            'confAttrList'     => $confAttrList,
            'table'            => $Table
        ));

        $Result = $Creator->getView()->render('catalog/configurable.phtml');

        return $Result;
    }

}