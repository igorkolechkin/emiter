<?php

class Fenix_Catalog_Helper_Decorator extends Fenix_Resource_Helper
{
    /**
     * Округление цены
     *
     * @var const
     */
    const CURRENCY_PRECISION = 2;

    protected $currency;

    /**
     * Украшение цены
     *
     * @param float $price Цена
     * @param string $code Код валюты
     * @return string
     */
    public function decoratePrice($price, $code = null)
    {
        if ($price != null) {

            if ($this->currency === null) {
                $this->setCurrency(new Zend_Currency([
                    'display' => Zend_Currency::USE_SHORTNAME,
                    'symbol'  => $code ? $code : '<span class="currency">руб</span>'
                ]));
            }

            if (fmod($price, 1) > 0) {
                $this->currency->setFormat(['precision' => self::CURRENCY_PRECISION]);
            } else {
                $this->currency->setFormat(['precision' => 0]);
            }
            $this->currency->setValue($price);

            return $this->currency->toString();
        }
    }

    protected function setCurrency(Zend_Currency $zendCurrency)
    {
        if ($this->currency === null) {
            $this->currency = $zendCurrency;
        }

        return $this;
    }
}