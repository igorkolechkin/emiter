<?php
class Fenix_Articles_Collection_Rubrics extends Fenix_Resource_Collection
{

    /**
     * Загрузка существующих рубрик
     * @return Fenix_Object_Rowset
     */
    public function getRubricList()
    {
        $_rubrics = Fenix::getModel('articles/rubric')->getRubricsList();


        $_data = array();
        foreach ($_rubrics as $_rubric) {
            $_data[]= Fenix::getCollection('articles/rubric_rubric')->setRubric($_rubric);
        }
        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}