<?php
class Fenix_Articles_Collection_Gallery extends Fenix_Object
{
    use Fenix_Traits_ImageCollection;

    public function setImage($_image)
    {
        $this->setData($_image);

        return $this;
    }

    public function getTitle()
    {
        return $this->getData('meta_title');
    }

    public function getAlt()
    {
        return $this->getData('meta_alt');
    }
}