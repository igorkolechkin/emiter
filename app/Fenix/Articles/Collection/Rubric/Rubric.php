<?php
class Fenix_Articles_Collection_Rubric_Rubric extends Fenix_Object
{
    use Fenix_Traits_SeoCollection;

    /**
     * Загрузка рубрики
     * @param $rubric
     * @return bool|Fenix_Object
     */
    public function setRubric($rubric)
    {
        if ($rubric == null) {
            return false;
        }


        $_data        = $rubric->toArray();
        $_data['active'] = Fenix::getRequest()->getUrlSegment(1) == $_data['url_key']?'true':false;
        $_data['seo'] = $this->getSeoMetadata($_data, 'articles_rubric');
        $_data['url'] = Fenix::getUrl('articles/' . $_data['url_key']);

        $this->setData($_data);

        return $this;
    }
}