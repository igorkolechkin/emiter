<?php
class Fenix_Articles_Collection_Article extends Fenix_Object
{
    use Fenix_Traits_ImageCollection;
    use Fenix_Traits_SeoCollection;

    /**
     * Загрузка статьи с учетом рубрики
     * @param $article
     * @param $rubric
     * @return $this
     */
    public function load($article, $rubric)
    {
        if ($article instanceof Zend_Db_Table_Row) {
            $article = $article->toArray();
        }

        $article['seo']     = $this->getSeoMetadata($article, 'articles_article');
        $article['url']     = Fenix::getUrl('articles/' . ($rubric != null ? $rubric->url_key . '/' : null) . $article['url_key']);

        $this->setData($article);

        return $this;
    }

    /**
     * Галлерея статьи
     * @return Fenix_Object_Rowset
     */
    public function getGallery()
    {
        $_gallery = (array) unserialize($this->getData('gallery'));

        $data = array();
        foreach ($_gallery AS $_image) {
            $data[] = Fenix::getCollection('articles/gallery')->setImage($_image);
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }

    /**
     *
     * Получаем дату создания в нужном формате
     */
    public function getCreateDate()
    {
        if ($this->getData('create_date') != null)
            if (Fenix::getConfig('questions/general/date_format') != '') {
                return date(Fenix::getConfig('questions/general/date_format'), strtotime($this->getData('create_date')));
            }else{
                return $this->getData('create_date');
            }
    }
}