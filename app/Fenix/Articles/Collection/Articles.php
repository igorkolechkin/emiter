<?php
class Fenix_Articles_Collection_Articles extends Fenix_Resource_Collection
{
    /**
     * Загрузка статей рубрики
     * @param $rubric
     * @return Fenix_Object_Rowset
     */
    public function load($rubric)
    {
        $_articles = Fenix::getModel('articles/articles')->getArticlesList($rubric);

        $_articles = $_articles->getCurrentItems()->toArray();

        $_data = array();
        foreach ($_articles AS $_article) {
            $_data[] = Fenix::getCollection('articles/article')->load($_article, $rubric);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }

    /**
     * Загрузка "последних" статей
     * @return Fenix_Object_Rowset
     */
    public function getLastArticles()
    {
        $_articles = Fenix::getModel('articles/articles')->getLastArticles();

        $_data = array();
        foreach ($_articles AS $_article) {
            $_data[] = Fenix::getCollection('articles/article')->load($_article, null);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}