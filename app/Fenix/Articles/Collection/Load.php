<?php

class Fenix_Articles_Collection_Load extends Fenix_Resource_Collection
{
    public function load($rubric = null)
    {
        return new Fenix_Object(
            array(
                'data' => array(
                    'rubric' => Fenix::getCollection('articles/rubric_rubric')->setRubric($rubric),
                    'articles' => Fenix::getCollection('articles/articles')->load($rubric)
                )
            ));
    }
}