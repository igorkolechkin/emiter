<?php
class Fenix_Articles_Collection_Rubric extends Fenix_Resource_Collection
{
    /**
     * Загрузка рубрики
     * @param $rubric
     * @return bool|Fenix_Object
     */
    public function load($rubric)
    {
        if ($rubric == null) {
            return false;
        }

        $_data        = $rubric->toArray();
        $_data['seo'] = $this->getSeoMetadata($_data);
        $_data['url'] = Fenix::getUrl('articles/' . $_data['url_key']);

        return new Fenix_Object(array(
            'data' => $_data
        ));
    }

    /**
     * Готовим сео поля рубрики
     * @param array $page
     * @return Fenix_Object
     */
    public function getSeoMetadata(array $page)
    {
        /** Приоритет первый - то что указано в "странице" */
        $seo_title =  trim($page['seo_title']);
        $seo_h1 =  trim($page['seo_h1']);
        $seo_keywords =  trim($page['seo_keywords']);
        $seo_description =  trim($page['seo_description']);

        /** SEO атрибуты для изображения */
        $seo_image_title =  '';
        $seo_image_alt =  '';

        /** Приоритет второй - то что указано в "по умолчанию" в СЕО шаблонах */
        $seo_def = Fenix::getModel('core/seo')->getSeoByName('articles_rubric');

        if($seo_def != null) {
            if(empty($seo_title) && !empty($seo_def->seo_title)) {
                $seo_title = $seo_def->seo_title;
            }
            if(empty($seo_h1) && !empty($seo_def->seo_h1)) {
                $seo_h1 = $seo_def->seo_h1;
            }
            if(empty($seo_keywords) && !empty($seo_def->seo_keywords)) {
                $seo_keywords = $seo_def->seo_keywords;
            }
            if(empty($seo_description) && !empty($seo_def->seo_description)) {
                $seo_description = $seo_def->seo_description;
            }
            if(empty($seo_image_title) && !empty($seo_def->seo_image_title)) {
                $seo_image_title = $seo_def->seo_image_title;
            } elseif(empty($seo_def->seo_image_title)) {
                $seo_image_title = '%title%'; // значение по умолчанию
            }
            if(empty($seo_image_alt) && !empty($seo_def->seo_image_alt)) {
                $seo_image_alt = $seo_def->seo_image_alt;
            }elseif(empty($seo_def->seo_image_alt)) {
                $seo_image_alt = '%title%'; // значение по умолчанию
            }
        }

        /** Формируем результатирующие данные */
        $return['title'] = Fenix::getModel('core/seo')->parseString($seo_title, $page);
        $return['h1'] = Fenix::getModel('core/seo')->parseString($seo_h1, $page);
        $return['keywords'] = Fenix::getModel('core/seo')->parseString($seo_keywords, $page);
        $return['description'] = Fenix::getModel('core/seo')->parseString($seo_description, $page);
        $return['image_title'] = Fenix::getModel('core/seo')->parseString($seo_image_title, $page);
        $return['image_alt'] = Fenix::getModel('core/seo')->parseString($seo_image_alt, $page);

        return new Fenix_Object(array(
            'data' => $return
        ));
    }

    /**
     * Загрузка существующих рубрик
     * @return Fenix_Object_Rowset
     */
    public function loadRubricList()
    {
        $_rubrics = Fenix::getModel('articles/rubric')->getRubricsList();


        $_data = array();
        foreach ($_rubrics as $_rubric) {
            $_data[]= Fenix::getCollection('articles/rubric')->load($_rubric);
        }
        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}