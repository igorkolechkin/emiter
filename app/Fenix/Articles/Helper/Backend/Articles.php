<?php
class Fenix_Articles_Helper_Backend_Articles extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'articles' && Fenix::getRequest()->getUrlSegment(1) != 'rubric';
    }
}