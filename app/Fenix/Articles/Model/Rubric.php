<?php
class Fenix_Articles_Model_Rubric extends Fenix_Resource_Model
{
    /**
     * Рубрика по URL
     *
     * @param $url
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRubric($url)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/rubric');

        $this->setTable('articles_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select->where('r.url_key = ?', $url);
        $Select->where('r.is_public = ?', '1');
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список рубрик запросом
     *
     * @return Zend_Db_Select
     */
    public function getRubricsAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/rubric');

        $this->setTable('articles_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->order('r.position asc');

        return $Select;
    }

    /**
     * Список рубрик
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getRubricsList()
    {
        $Select = $this->getRubricsAsSelect();

        $Select->where('r.is_public = ?', '1');

        return $this->fetchAll($Select);
    }


    /**
     * Список рубрик статьи
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getArticleRubricsList($articleId)
    {
        $Select = $this->getRubricsAsSelect();

        $Select->join(array(
            'ar'=>$this->getTable('articles_relations')
        ),'r.id = ar.rubric_id');

        $Select->where('ar.article_id = ?', $articleId);
        $Select->where('ar.is_public = ?', '1');

        return $this->fetchAll($Select);
    }
}