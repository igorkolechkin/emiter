<?php
class Fenix_Articles_Model_Backend_Articles extends Fenix_Resource_Model
{
    /**
     * Статья по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getArticleById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/articles');

        $this->setTable('articles');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    //получаем список продуктов
    public function getProductsBrowser($blockId)
    {
        return Fenix::getHelper('catalog/backend_browser')->getSimpleProductsFilter(array(
            'name'      => 'promoProducts',
            'url'       => Fenix::getUrl('catalog/products/find'),
            'products'  => Fenix::getModel('articles/backend_articles')->getProductsListByBlockId($blockId)
        ));
    }

    //получаем id продуктов,которые добавлены
    public function getProductsListByBlockId($blockId)
    {
        $this->setTable('articles_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            '_p' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), '_p.product_id = p.id', Fenix::getModel('catalog/backend_products')->productColumns());

        $Select ->where('_p.block_id = ?', (int) $blockId);
        $Select ->order('_p.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getArticlesAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/articles');

        $this->setTable('articles');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.create_date desc');

        return $Select;
    }

    /**
     * Новая статья
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addArticle($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->addRecord($req);
        $this->setTable('articles_products');
        $this->delete('block_id = ' . (int) $id);
        $products = $req->getPost('_list');
        foreach ((array) $products['promoProducts'] AS $_i => $_id) {
            $this->insert(array(
                'block_id'   => $id,
                'product_id' => $_id,
                'position'   => ($_i + 1)
            ));
        }
        // Удаляем все старые связи
        $this->setTable('articles_relations')
            ->delete('article_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'article_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }

        return (int) $id;
    }

    /**
     * Редактировать статью
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editArticle($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->editRecord($current, $req);
        //обновляем данные товар-категория
        $this->setTable('articles_products');
        $this->delete('block_id = ' . (int) $current->id);
        $products = $req->getPost('_list');
        foreach ((array) $products['promoProducts'] AS $_i => $_id) {
            $this->insert(array(
                'block_id'   => $current->id,
                'product_id' => $_id,
                'position'   => ($_i + 1)
            ));
        }
        // Удаляем все старые связи
        $this->setTable('articles_relations')
             ->delete('article_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'article_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }

        return (int) $id;
    }

    /**
     * Удаление статьи
     *
     * @param $current
     */
    public function deleteArticle($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('articles/articles', $current->attributeset)
                 ->deleteRecord($current);
        $this->setTable('articles_products');
        $this->delete('block_id = ' . (int) $current->id);
        $this->setTable('articles_relations')
             ->delete('article_id = ' . (int) $current->id);
    }
}