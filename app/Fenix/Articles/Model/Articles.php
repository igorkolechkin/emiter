<?php
class Fenix_Articles_Model_Articles extends Fenix_Resource_Model
{
    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 12;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;

    /**
     *
     * Получение последних новостей
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getLastArticles()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/articles');

        $this   ->setTable('articles');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('articles/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('articles/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     * @param null $page
     * @return Zend_Paginator
     */
    public function getArticlesList($rubric = null, $page = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('articles/articles');

        $this->setTable('articles');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('articles_relations')
            ), 'a.id = _r.article_id', false);
            $Select->join(array(
                'r' => $this->getTable('articles_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select->where('r.id = ?', $rubric->id);
        }

        $Select->order('a.create_date desc');

        $perPage = (int)(Fenix::getConfig('articles/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('articles/general/per_page'));

        if($page === null){
            $page = Fenix::getRequest()->getQuery("page");
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter->setRowCount($this->fetchAll($Select)->count());

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber((int)$page)
            ->setPageRange(5)
            ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Статья по url
     *
     * @param $url
     * @param $rubric
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getArticle($url, $rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('articles/articles');

        $this   ->setTable('articles');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', urldecode($url))
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('articles_relations')
            ), 'a.id = _r.article_id', false);
            $Select->join(array(
                'r' => $this->getTable('articles_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}