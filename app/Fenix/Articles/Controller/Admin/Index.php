<?php
class Fenix_Articles_Controller_Admin_Index extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('articlesAll');

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('articles/articles')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('articles/relations')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('articles/rubric')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('articles/products')
            ->prepare()
            ->execute();
    }

    /**
     * Управление статьями
     */
    public function indexAction()
    {
        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentArticle = Fenix::getModel('articles/backend_articles')->getArticleById($_rowId);
                    Fenix::getModel('articles/backend_articles')->deleteArticle($currentArticle);
                }

                Fenix::redirect('articles');
            }
        }

        $articlesList  = Fenix::getModel('articles/backend_articles')->getArticlesAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('articles/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая статья"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('articles/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Статьи"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление статьями"))
            ->setData($articlesList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('articles/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('articles/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Статьи"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('articles/articles')
        ));
    }

    /**
     * Новая статья
     */
    public function addAction()
    {
        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'articles',
            'uri'   => Fenix::getUrl('articles')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'create_date' => date('Y-m-d'),
            'create_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Url автоматом
        $url_key = trim($this->getRequest()->getPost('url_key'));
        if ($url_key == '') {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl(str_replace(' ', '-', $url_key)));

        // Источник
        $Form      ->setSource('articles/articles', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('articles/backend_articles')->addArticle($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Статья создана"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('articles');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('articles/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('articles/edit/attributeset/' . $attributeset . '/id/' . $id);
            }

            Fenix::redirect('articles');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая статья"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать статью
     */
    public function editAction()
    {
        $currentArticle = Fenix::getModel('articles/backend_articles')->getArticleById(
            $this->getRequest()->getParam('id')
        );

        if ($currentArticle == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Статья не найдена"))
                ->saveSession();

            Fenix::redirect('articles');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'articles',
            'uri'   => Fenix::getUrl('articles')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentArticle->toArray();

        list($createDate, $createTime) = explode(' ', $currentArticle->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentArticle);

        // Url автоматом
        $url_key = trim($this->getRequest()->getPost('url_key'));
        if ($url_key == '') {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl(str_replace(' ', '-', $url_key)));

        // Источник
        $Form      ->setSource('articles/articles', $currentArticle->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('articles/backend_articles')->editArticle($Form, $currentArticle, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Статья отредактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('articles');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('articles/add/attributeset/' . $currentArticle->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('articles/edit/attributeset/' . $currentArticle->attributeset . '/id/' . $id);
            }

            Fenix::redirect('articles');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать статью"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить статью
     */
    public function deleteAction()
    {
        $currentArticle = Fenix::getModel('articles/backend_articles')->getArticleById(
            $this->getRequest()->getParam('id')
        );

        if ($currentArticle == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Статья не найдена"))
                ->saveSession();

            Fenix::redirect('articles');
        }

        Fenix::getModel('articles/backend_articles')->deleteArticle($currentArticle);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Статья удалена"))
            ->saveSession();

        Fenix::redirect('articles');
    }
}