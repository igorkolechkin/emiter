<?php
class Fenix_Articles_Controller_Admin_Rubric extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('articlesAll');

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('articles/rubric')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('articles/relations')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('articles/articles')
                ->prepare()
                ->execute();
    }

    /**
     * Управление рубриками
     */
    public function indexAction()
    {
        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentRubric = Fenix::getModel('articles/backend_rubric')->getRubricById($_rowId);
                    Fenix::getModel('articles/backend_rubric')->deleteRubric($currentRubric);
                }

                Fenix::redirect('articles/rubric');
            }
        }

        $rubricList  = Fenix::getModel('articles/backend_rubric')->getRubricsAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новая рубрика"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('articles/rubric/add') . '\'')
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Управление рубриками"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'articles',
            'uri'   => Fenix::getUrl('articles')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('articles/rubric')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление рубриками"))
            ->setData($rubricList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('articles/rubric/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('articles/rubric/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Рубрики статей"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('articles/rubric')
        ));
    }

    /**
     * Новая рубрика
     */
    public function addAction()
    {
        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'articles',
            'uri'   => Fenix::getUrl('articles')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('articles/rubric')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('articles/rubric', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('articles/backend_rubric')->addRubric($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Рубрика создана"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('articles/rubric');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('articles/rubric/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('articles/rubric/edit/id/' . $id);
            }

            Fenix::redirect('articles/rubric');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая рубрика"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать статью
     */
    public function editAction()
    {
        $currentRubric = Fenix::getModel('articles/backend_rubric')->getRubricById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRubric == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Рубрика не найдена"))
                ->saveSession();

            Fenix::redirect('articles/rubric');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Статьи"),
            'id'    => 'articles',
            'uri'   => Fenix::getUrl('articles')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Рубрики"),
            'id'    => 'rubric',
            'uri'   => Fenix::getUrl('articles/rubric')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults($currentRubric->toArray())
                   ->setData('current', $currentRubric);

        // Источник
        $Form      ->setSource('articles/rubric', 'default')
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('articles/backend_rubric')->editRubric($Form, $currentRubric, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Рубрика отредактирована"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('articles/rubric');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('articles/rubric/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('articles/rubric/edit/id/' . $id);
            }

            Fenix::redirect('articles/rubric');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать рубрику"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить рубрику
     */
    public function deleteAction()
    {
        $currentRubric = Fenix::getModel('articles/backend_rubric')->getRubricById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRubric == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Рубрика не найдена"))
                ->saveSession();

            Fenix::redirect('articles/rubric');
        }

        Fenix::getModel('articles/backend_rubric')->deleteRubric($currentRubric);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Рубрика удалена"))
            ->saveSession();

        Fenix::redirect('articles/rubric');
    }
}