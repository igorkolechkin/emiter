<?php
class Fenix_Articles_Controller_Index extends Fenix_Controller_Action
{
    /**
     * Роутинг статей
     *
     * @throws Exception
     */

    public function preDispatch()
    {
        //показать еще
        if ($this->getRequest()->isXmlHttpRequest()) {
            return $this->ajaxAction();
        }

        $first = $this->getRequest()->getUrlSegment(1);
        $second = $this->getRequest()->getUrlSegment(2);

        //выбрана и рубрика и статья
        if ($first != null && $second != null) {
            $rubric = Fenix::getModel('articles/rubric')->getRubric($first);
            if ($rubric == null) {
                throw new Exception();
            } else {
                $article = Fenix::getModel('articles/articles')->getArticle($second, $rubric);
                if ($article == null) {
                    throw new Exception();
                } else {
                    $this->articleAction($article, $rubric);
                }
            }
        } else {
            if ($first != null && $second == null) {
                $article = Fenix::getModel('articles/articles')->getArticle($first);
                if ($article == null) {
                    $rubric = Fenix::getModel('articles/rubric')->getRubric($first);
                    if ($rubric == null) {
                        throw new Exception();
                    } else {
                        $this->_indexAction($rubric);
                    }
                } else {
                    $this->articleAction($article);
                }
            } else {
                $this->_indexAction();
            }
        }
    }

    /**
     * Нужен для работы диспетчера
     */
    public function indexAction(){}

    /**
     *
     * Показать еще аяксом
     *
     * @return bool
     */
    private function ajaxAction()
    {
        if(($rubric = $this->getRequest()->getPost('rubric')) == ''){
            $rubric = null;
        }else{
            $rubric = Fenix::getModel('articles/rubric')->getRubric($rubric);
        }

        //если не передали страницу то не знаем что возвращать
        if(($page = $this->getRequest()->getPost('page')) == null)
            return false;

        $_articles = Fenix::getModel('articles/articles')->getArticlesList($rubric, $page);

        $collection = array();
        foreach ($_articles as $_article) {
            $collection[] = Fenix::getCollection('articles/article')->load($_article, $rubric);
        }

        $collection = new Fenix_Object_Rowset(array('data' => $collection, 'rowClass' => false));

        $Creator = Fenix::getCreatorUI();

        $Creator->getView()->assign(array(
            'articles' => $collection
        ));

        echo $Creator->getView()->render('articles/blocks/show-more.phtml');

        exit();
    }

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     */
    private function _indexAction($rubric = null)
    {
        $collection = Fenix::getCollection('articles/load')->load($rubric);
        $_rubric = $collection->getRubric();
        $_articles = Fenix::getModel('articles/articles')->getArticlesList($rubric);


        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri' => Fenix::getUrl(),
            'id' => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('articles/seo/h1', Fenix::lang("Новости")),
            'uri' => Fenix::getUrl('articles'),
            'id' => 'articles'
        );
        if ($_rubric !== false) {
            $_crumbs[] = array(
                'label' => $_rubric->getTitle(),
                'uri' => Fenix::getUrl('articles/' . $_rubric->getUrlKey()),
                'id' => 'rubric'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        if($_rubric !== false) {
            $this->setMeta($Creator, $_rubric, $_articles);
        } else {
            $this->setMeta($Creator, array(
                'url'         => Fenix::getUrl('articles'),
                'title'       => Fenix::getConfig('articles/seo/title'),
                'keywords'    => Fenix::getConfig('articles/seo/keywords'),
                'description' => Fenix::getConfig('articles/seo/description'),
            ), $_articles);
        }

        $Creator->getView()->assign(array(
            'rubric' => $_rubric,
            'articles' => $_articles,
            'collection' => $collection
        ));

        $Creator->setLayout()
            ->render('articles/list.phtml');
    }

    /**
     * Читаем статью
     *
     * @param $article
     * @param null $rubric
     */
    private function articleAction($article, $rubric = null)
    {
        $_article = Fenix::getCollection('articles/article')->load($article, $rubric);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('articles/seo/h1', Fenix::lang("Новости")),
            'uri'   => Fenix::getUrl('articles'),
            'id'    => 'articles'
        );
        if ($rubric != null) {
            $_crumbs[] = array(
                'label' => $rubric->title,
                'uri'   => Fenix::getUrl('articles/' . $rubric->url_key),
                'id'    => 'rubric'
            );
        }

        $_crumbs[] = array(
            'label' => $_article->getTitle(),
            'uri'   => $_article->getUrl(),
            'id'    => 'article'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $_article);

        $productList = Fenix::getModel('articles/backend_articles')->getProductsListByBlockId($_article->id);

        $Creator ->getView()->assign(array(
            'rubric'  => $rubric,
            'article' => $_article,
            'products' => $productList
        ));
        $Creator ->setLayout()
                 ->render('articles/read.phtml');
    }
}