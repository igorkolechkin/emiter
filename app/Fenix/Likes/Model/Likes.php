<?php
class Fenix_Likes_Model_Likes extends Fenix_Resource_Model
{
    /**
     * Основной запрос к таблице лайков
     * @return Zend_Db_Select
     */
    public function getLikesListAsSelect()
    {
        $this->setTable('likes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'l' => $this->_name
        ));

        return $Select;
    }

    /**
     * Добавление лайка
     * @param Fenix_Controller_Request_Http $req
     * @return bool|object
     */
    public function addLike(Fenix_Controller_Request_Http $req)
    {
        $id = $req->getPost('question_id');

        $this->setTable('catalog_product_questions');

        $likeType = $req->getPost('like_type');
        $data = array(
            'create_date' => new Zend_Db_Expr('NOW()'),
            'positive'   => $req->getPost('positive') ? '1' : '0',
            'negative'   => $req->getPost('negative') ? '1' : '0',
        );

        $fieldList = array(
            'item_id',
            'item_type',
            'user_id',
            'user_ip'
        );
        foreach($fieldList as $field){
            if($req->getPost($field))
                $data[$field] = $req->getPost($field);
        }

        if (isset($data['item_id']) && isset($data['item_type'])) {
            $this->saveLikeType($data['item_id'], $data['item_type'], $likeType);
            $likeId = $this->setTable('likes')->insert($data);
            $data['like_id'] = $likeId;
            return (object) $data;
        }
        return false;
    }

    /**
     * Создаем строковый id like для cookie
     * @param $itemId
     * @param $itemType
     * @return string
     */
    public function getLikeId($itemId, $itemType)
    {
        return 'like-' . $itemType . $itemId;
    }

    /**
     * Сохраняем like в cookie
     * @param $itemId
     * @param $itemType
     * @param $likeType
     * @return bool
     */
    public function saveLikeType($itemId, $itemType, $likeType){
        $likeId = $this->getLikeId($itemId, $itemType);

        setcookie($likeId, $likeType, null, '/');

        return true;
    }

    /**
     * Получаем выбор вида like пользователя из cookie
     * @param $itemId
     * @param $itemType
     * @return bool
     */
    public function getLikeType($itemId, $itemType){
        $likeId = $this->getLikeId($itemId, $itemType);
        if (isset($_COOKIE[$likeId])) {
            return $_COOKIE[$likeId];
        }

        return false;
    }

    /**
     * Получаем выбор вида like пользователя из cookie
     * @param $itemId
     * @param $itemType
     * @return bool
     */
    public function getLikeCounter($itemId, $itemType){
        $Select = $this->getLikesListAsSelect($itemId, $itemType);

        $Select->reset(Zend_Db_Select::COLUMNS);
        $Select->columns(array('SUM(positive) as positive', 'SUM(negative) as negative'));

        $Select->where('l.item_id = ?', $itemId);
        $Select->where('l.item_type = ?', $itemType);

        $Result = $this->fetchRow($Select);
        return $Result;
    }
}