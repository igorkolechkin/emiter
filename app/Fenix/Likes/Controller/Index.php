<?php
class Fenix_Likes_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        if (Fenix::getRequest()->isPost()) {
            $req = Fenix::getRequest();
            if($req->getPost('like_type')=='positive'){
                Fenix::getRequest()->setPost('positive','1');
                Fenix::getRequest()->setPost('negative','0');
            }else {
                Fenix::getRequest()->setPost('positive','0');
                Fenix::getRequest()->setPost('negative','1');
            }
            Fenix::getModel('likes/likes')->addLike(Fenix::getRequest());


            Fenix::getCreatorUI()->getView()->assign(array(
                'item_id'   => Fenix::getRequest()->getPost('item_id'),
                'item_type' => Fenix::getRequest()->getPost('item_type')
            ));
            echo Fenix::getCreatorUI()->getView()->render('likes/like.phtml');
        } else {
            throw new Exception();
        }
    }
}