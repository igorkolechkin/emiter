<?php
class Fenix_Seo_Helper_Backend_Seo extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'seo' && Fenix::getRequest()->getUrlSegment(1) != 'rubric';
    }


    public function getAttributesTable($id){
        
        $record = Fenix::getModel('seo/backend_seo')->getRecordById($id);

        $Creator= Fenix::getCreatorUI();

        $Table = $Creator->loadPlugin('Table')->setId('table');
        $Table->addHeader('title-1','Атрибут');
        $Table->addHeader('title-2','Значение');


        $attributesList = Fenix::getModel('catalog/filter')->getAvailableAttributes();
        foreach($attributesList as $i => $_attr){

            if ($_attr->sys_title == 'price') continue;

            $attrValues = Fenix::getModel('catalog/filter')->getAttributeValues($_attr);

            $Select = $Creator->loadPlugin('Form_Select')
                ->setId('attr_value_select_'.$_attr->id)
                ->setName('attr_value[' . $_attr->sys_title . ']');

            $Select->addOption('', '[Не используется]');
            foreach($attrValues as $value){
                $Select->addOption($value->url, $value->content);
            }

            if ($record) {
                $value = Fenix::getModel('seo/backend_seo')->getAttributeValue($id, $_attr->sys_title);
                $Select->setSelected($value);
            } else {
                $value = '';
                $Select->setSelected('');
            }
            //$Text = $Creator->loadPlugin('Form_Text')->setName('attr_value[' . $_attr->sys_title . ']')->setValue($value)->setStyle('width:90%');
            $Table->addCell('row-'.$i, 'title-1', $_attr->title);
            $Table->addCell('row-'.$i, 'title-2', $Select->fetch());
        }

        return $Table->fetch();
    }
/*
    public function getCategoriesBlock($record){

        $categoriesList = Fenix::getModel('catalog/categories')->getCategoriesList(1);

        $Creator = Fenix::getCreatorUI();

        $Select = $Creator->loadPlugin('Form_Select')
                          ->setStyle('margin-bottom:10px;')
                          ->setName('category')
                          ->setId('category')
                          ->setSelected(Fenix::getRequest()->getPost('category'));

        $Select->addOption('','Любой');
        foreach ($categoriesList  AS $_category) {
            $Select->addOption($_category->title,$_category->title);
        }

        return $Select->fetch();
    }
    public function getAttributesetBlock($record){

        $attributesetList = Fenix::getModel('catalog/backend_attributeset_categories')->getCategoriesList();

        $Creator = Fenix::getCreatorUI();

        $Select = $Creator->loadPlugin('Form_Select')
                          ->setStyle('margin-bottom:10px;')
                          ->setName('filter_attributeset')
                          ->setId('filter_attributeset')
                          ->setSelected(Fenix::getRequest()->getPost('filter_attributeset'));

        $Select->addOption('','Любой');
        foreach ($attributesetList  AS $_attributeset) {
            $Select->addOption($_attributeset->title,$_attributeset->title);
        }

        return $Select->fetch();
    }
    public function getCityBlock($record){

        $cityList = Fenix::getModel('geo/geo')->getRiaCityList(11);

        $Creator = Fenix::getCreatorUI();

        $Select = $Creator->loadPlugin('Form_Select')
                          ->setStyle('margin-bottom:10px;')
                          ->setName('city')
                          ->setId('city')
                          ->setSelected(Fenix::getRequest()->getPost('city'))
                          ->loadIn(array(
                              'id' => 'district',
                              'url' => '/geo/district/json/1/id/%s',
                              'selected' => Fenix::getRequest()->getPost('district')
                          ));

        $Select->addOption('','Любой');
        foreach ($cityList  AS $_city) {
            $Select->addOption($_city->id,$_city->name);
        }
        $result = $Select->fetch();
        return $result;
    }
    public function getDistrictBlock($record){

        $city = Fenix::getRequest()->getPost('city');
        $districtList = Fenix::getModel('geo/geo')->getRiaCityList(11);
        $districtList = Fenix::getModel('geo/geo')->getRiaDistricts(11, 'Район');
        $Creator = Fenix::getCreatorUI();

        $Select = $Creator->loadPlugin('Form_Select')
                          ->setStyle('margin-bottom:10px;')
                          ->setName('district')
                          ->setId('district')
                          ->setSelected(Fenix::getRequest()->getPost('district'));
        $Select->addOption('','Любой');
        foreach ($districtList  AS $_district) {
            $Select->addOption($_district->id,$_district->name);
        }

        return $Select->fetch();
    }
*/
}