<?php
class Fenix_Seo_Model_Catalog extends Fenix_Resource_Model
{
    public function getFilterSeo($urlKeyFilter, $category_id = null){
        if($urlKeyFilter ==null)
            return null;

        //$value = array_shift($urlKeyFilter);
        //Fenix::dump($urlKeyFilter);
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');



        $this->setTable('seo');
        $Select = $this->select()
                       ->setIntegrityCheck(false)
                       ->from(array(
                           's' => $this->_name
                       ), $Engine->getColumns(array('prefix' => 's')));
        $attributesList = Fenix::getModel('catalog/backend_attributes')->getFilterAttributesList();

        foreach($attributesList as $i => $_attr) {
            if ($_attr->sys_title == 'price') continue;
            $Select->join(array(
                'a' . $i => $this->getTable('seo_attributes')
            ), 's.id = a' . $i . '.record_id AND a' . $i . '.sys_title="' . $_attr->sys_title . '"', array('attr_value as ' . $_attr->sys_title));
        }

        $Select->where('s.category_id = ?', $category_id);
        foreach ($attributesList as $i => $_attr) {
            if ($_attr->sys_title == 'price') continue;
            if(!isset($urlKeyFilter[$_attr->sys_title])):
                $urlKeyFilter[$_attr->sys_title] = '';
            endif;
            $Select->where(
                $this->getAdapter()->quoteInto('a' . $i . '.sys_title = ? AND ', $_attr->sys_title) .
                $this->getAdapter()->quoteInto('a' . $i . '.attr_value = ? ', urldecode($urlKeyFilter[$_attr->sys_title]))
            );
            /*
            if ($_attr->sys_title == 'inset') {
                $Select->where(
                    $this->getAdapter()->quoteInto('a' . $i . '.sys_title = ? AND ', $_attr->sys_title) .
                    $this->getAdapter()->quoteInto('a' . $i . '.attr_value = ? ', $value)
                );
            } else {
                $Select->where(
                    $this->getAdapter()->quoteInto('a' . $i . '.sys_title = ? AND ', $_attr->sys_title) .
                    $this->getAdapter()->quoteInto('a' . $i . '.attr_value = ? ', '')
                );
            }
            */
        }
        $Select->limit(1);
        //Fenix::dump($this->fetchRow($Select));

        $Result = $this->fetchRow($Select);
        //Fenix::dump($Select->assemble());
        return $Result;

    }
    public function getAttributeUrlKeyList($attribute, $catalog = null)
    {
        $regTitle = 'getAttributeUrlKeyList_' . $attribute;
        if (Zend_Registry::isRegistered($regTitle))
            return Zend_Registry::get($regTitle);


        if (is_string($attribute)) {
            $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($attribute);
        }
        //Название колонки в таблице товаров
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        if($attribute->split_by_lang){
            $sys_title = $attribute->sys_title . '_' . $lang->name;
        }else{
            $sys_title = $attribute->sys_title;
        }
        //Получаем возможные значения
        $Select = $this->productsAsSelect($catalog, array(
            $sys_title
        ));
        $Select->reset(Zend_Db_Select::GROUP);
        $Select->where($sys_title . ' <> ""');
        $Select->group($sys_title);
        $_valuesList = $this->fetchAll($Select);

        //Готовим результат url-key => значение

        $valuesList = array();
        foreach ($_valuesList as $key => $value) {
            $value               = $value->{$sys_title};
            $urlKey              = Fenix::filterProtect($value);
            $valuesList[$urlKey] = $value;
        }

        Zend_Registry::set($regTitle, $valuesList);

        return $valuesList;
    }

    /**
     * Select для товаров по категории
     * @param $catalog
     * @return \Zend_Db_Select
     */
    public  function productsAsSelect($catalog, $cols = null){

        $this->setTable('catalog');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'c' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select ->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'c.id = p.parent', $this->productColumns());


        if ($catalog == null) {
            $_catalog = Fenix::getModel('catalog/categories')->getCategoryById(1);

            $Select ->where('c.left >= ?', (int) $_catalog->left)
                ->where('c.right <= ?', (int) $_catalog->right)
                ->where('c.is_public = ?', '1');
        }
        else {
            $Select ->where('c.left >= ?', (int) $catalog->getLeft())
                ->where('c.right <= ?', (int) $catalog->getRight())
                ->where('c.is_public = ?', '1');
        }

        return $Select;
    }
    public function getCategoriesSelect($category_id){

        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId('category_id')
                      ->setName('category_id');
        if($category_id)
        {
            $Field->setSelected($category_id);
        }
        $Field->addOption(0, 'Все категории');
        $categories = Fenix::getCollection('catalog/categories')->getCategoriesList();
        foreach ($categories as $_category){
            $Field->addOption($_category->getId(), $_category->getTitle(),'level-0');
            $subCategories = Fenix::getCollection('catalog/categories')->getCategoriesList($_category->getId());
            foreach($subCategories as $subCategory){
                $Field->addOption($subCategory->getId(), $subCategory->getTitle(),'level-1');
                $subCategories2 = Fenix::getCollection('catalog/categories')->getCategoriesList($subCategory->getId());
                foreach($subCategories2 as $subCategory2){
                    $Field->addOption($subCategory2->getId(), $subCategory2->getTitle(),'level-2');
                }
            }
        }

        return $Field->fetch();
    }
}