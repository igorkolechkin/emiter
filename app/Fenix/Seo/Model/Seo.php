<?php
class Fenix_Seo_Model_Seo extends Fenix_Resource_Model
{
    /**
     * По умолчанию статей на страницу
     */
    const DEFAULT_PER_PAGE = 10;

    /**
     * По умолчанию количество последний статей
     */
    const DEFAULT_LAST_ARTICLES = 1;





    public function checkActiveSeo($domain = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($domain != null) {
            $Select ->where('a.domain = ?', $domain);
        }
        $Select ->limit('1');
        return $this->fetchRow($Select);
    }

    public function getActiveSeo($filter, $categoryId)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->where('a.is_public = ?', '1');
        $Select->where('a.filter_string = ?', $filter);
        $Select->where('a.category_id = ?', $categoryId);

        $Select->limit('1');
        $Result = $this->fetchRow($Select);

        //Если не нашли в привязке по категории, ищем общий
        if($Result == null){
            $Select->reset(Zend_Db_Select::WHERE);
            $Select->where('a.is_public = ?', '1');
            $Select->where('a.filter_string = ?', $filter);
            $Select->where('a.category_id = ?', 0);
            $Result = $this->fetchRow($Select);
        }
        return $Result;
    }


    public function getLastSeo()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');
        $Select ->order('a.create_date desc');

        $Select ->limit(
            (int) (Fenix::getConfig('seo/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('seo/general/last_count'))
        );

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     * @return Zend_Paginator
     */
    public function getSeoList($rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.is_public = ?', '1');

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('seo_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('seo_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Select ->order('a.create_date desc');

        $perPage   = (int) (Fenix::getConfig('seo/general/per_page') <= 0 ? self::DEFAULT_PER_PAGE : Fenix::getConfig('seo/general/per_page'));

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($Select);


        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) Fenix::getRequest()->getQuery("page"))
                   ->setItemCountPerPage($perPage);

        return $paginator;
    }

    /**
     * Статья по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecord($url, $rubric = null)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.url_key = ?', $url)
                ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        if ($rubric != null) {
            $Select->join(array(
                '_r' => $this->getTable('seo_relations')
            ), 'a.id = _r.record_id', false);
            $Select->join(array(
                'r' => $this->getTable('seo_rubric')
            ), '_r.rubric_id = r.id', false);

            $Select ->where('r.id = ?', $rubric->id);
        }

        $Result = $this->fetchRow($Select);

        return $Result;
    }
    /**
     * Статья по url
     *
     * @param $url
     * @param $rubric рубрика
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSeoRecord($req)
    {
        $segmentIndex = 1;
        $operation =null;
        $operation_uri = urldecode($req->getUriSegment($segmentIndex++));
        if ($operation_uri) {
            $operation = Fenix::getModel('catalog/backend_attributeset_categories')->getCategoryByUrl($operation_uri);
        }
        $attributeset     = null;
        $attributeset_uri = urldecode($req->getUriSegment($segmentIndex++));
        if ($attributeset_uri) {
            $attributeset = Fenix::getModel('catalog/backend_attributeset')->getAttributesetByUrl($attributeset_uri);
        }
        $city     = null;
        $city_uri = urldecode($req->getUriSegment($segmentIndex++));
        if ($city_uri) {
            $city = Fenix::getModel('geo/geo')->getCityByName($city_uri);
        }
        $district     = null;
        $district_uri = urldecode($req->getUriSegment($segmentIndex++));

        if ($district_uri && $city) {

            $district = Fenix::getModel('geo/geo')->getRiaDistrictByName($city->id, $district_uri);

        }

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this   ->setTable('seo');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));
        if ($operation) {
            $Select->where('a.filter_attributeset = ?', $operation_uri);
        } else {
            $Select->where('a.filter_attributeset = ?', '');
        }
        if ($attributeset) {
            $Select->where('a.category = ?', $attributeset_uri);
        } else {
            $Select->where('a.category = ?', '');
        }
        if ($city) {
            $Select->where('a.city = ?', $city->id);
        } else {
            $Select->where('a.city = ?', '');
        }
        if ($district) {
            $Select->where('a.district = ?', $district->id);
        } else {
            $Select->where('a.district = ?', '');
        }
        $Select ->where('a.is_public = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getSeoAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/seo');

        $this->setTable('seo');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        return $Select;
    }

}