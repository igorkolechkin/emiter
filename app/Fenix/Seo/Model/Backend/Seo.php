<?php

class Fenix_Seo_Model_Backend_Seo extends Fenix_Resource_Model
{
    /**
     * Значения для атрибутов
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecordAttributesValues()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('seo/seo');

        $this->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        //$Select ->where('a.id = ?', (int) $id);
        //$Select ->limit(1);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Статья по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecordById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('seo/seo');

        $this->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->where('a.id = ?', (int)$id);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }


    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getSeoAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('seo/seo');

        $this->setTable('seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->order('a.create_date desc');

        return $Select;
    }

    /**
     * Новая страница
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addRecord($Form, $req)
    {
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', (string)$req->getPost('title_russian'));
        }

        $req->setPost('url_key', (string)Fenix::stringProtectUrl($req->getPost('url_key')));

        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        //Сохраняем выбор в одну строку фильтра
        $attrValues = (array)$req->getPost('attr_value');
        $filter = array();
        foreach ($attrValues as $attrName => $value) {
            if (trim($value) != '')
                $filter[$attrName] = array($value);
        }
        $filter = Fenix::getModel('catalog/filter')->assembleFilterString($filter);
        $req->setPost('filter_string', $filter);

        $id = $Form->addRecord($req);

        // Удаляем все старые связи
        $this->setTable('seo_relations')
            ->delete('record_id = ' . (int)$id);

        // Добавляем новые
        foreach ((array)$req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'record_id' => (int)$id,
                'rubric_id' => (int)$_rubricId
            ));
        }
        //сохраняем значения атрибутов
        self::saveAttributeValues($Form, $id, $req);


        return (int)$id;
    }

    /**
     * Редактировать страницу
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editRecord($Form, $current, $req)
    {
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', (string)$req->getPost('title_russian'));
        }
        $req->setPost('url_key', (string)Fenix::stringProtectUrl($req->getPost('url_key')));

        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        //Сохраняем выбор в одну строку фильтра
        $attrValues = (array)$req->getPost('attr_value');
        $filter = array();
        foreach ($attrValues as $attrName => $value) {
            if (trim($value) != '')
                $filter[$attrName] = array($value);
        }
        $filter = Fenix::getModel('catalog/filter')->assembleFilterString($filter);
        $req->setPost('filter_string', $filter);

        $id = $Form->editRecord($current, $req);

        // Удаляем все старые связи
        $this->setTable('seo_relations')
            ->delete('record_id = ' . (int)$id);

        // Добавляем новые
        foreach ((array)$req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'record_id' => (int)$id,
                'rubric_id' => (int)$_rubricId
            ));
        }
        //сохраняем значения атрибутов
        self::saveAttributeValues($Form, $id, $req);

        return (int)$id;
    }

    /**
     * Удаление страницы
     *
     * @param $current
     */
    public function deleteRecord($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator->loadPlugin('Form_Generator')
            ->setSource('seo/seo', $current->attributeset)
            ->deleteRecord($current);

        $this->setTable('seo_relations')
            ->delete('record_id = ' . (int)$current->id);
    }

    /**
     *  Копирование фильтра
     *  @param $current
     *
     * @return integer Copied record ID
     */
    public function copyRecord($current) {
        if($current == null) {
            return false;
        }

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('current', null);

        // Источник
        $Form->setSource('seo/seo', $current->attributeset)
             ->renderSource();
        // Компиляция
        $Form->compile();

        $req = $this->getRequest();

        $currentArr = $current->toArray();

        foreach($currentArr as $_property => $_value) {
            $req->setPost($_property, $_value);
        }

        $req->setPost('id', '');
        $req->setPost('title', $current->title . Fenix::lang('(копия)'));
        $req->setPost('create_date', date('Y-m-d H:i:s'));
        $req->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);


	    $attr_value = array();
	    $filter = Fenix::getModel('catalog/filter')->parseFilterString($current->filter_string);
	    if($filter != null){
		    foreach($filter as $_attribute => $_valueArr){
			    $attr_value[$_attribute] = $_valueArr[0];
		    }
	    }
	    $req->setPost('attr_value', $attr_value);

	    return $this->addRecord($Form, $req);
    }

    public function saveAttributeValues($Form, $id, $req)
    {

        //Удаляем старые значение
        $this->setTable('seo_attributes')
            ->delete('record_id = ' . (int)$id);

        $attrValues = (array)$req->getPost('attr_value');
        // Добавляем новые
        foreach ($attrValues AS $sys_title => $value) {
            $this->setTable('seo_attributes');
            $this->insert(array(
                'record_id' => (int)$id,
                'sys_title' => $sys_title,
                'attr_value' => $value
            ));
        }
    }

    public function getAttributeValue($recordId, $sys_title)
    {
        $this->setTable('seo_attributes');
        $Select = $this->select()
            ->from(
                array(
                    'sa' => $this->getTable()
                ),
                array('*')
            );
        $Select->where('record_id = ?', $recordId);
        $Select->where('sys_title = ?', $sys_title);
        $Select->limit(1);
        $result = $this->fetchRow($Select);
        if ($result)
            return $result->attr_value;
        else
            return null;
    }
}