<?php
class Fenix_Seo_Model_Backend_Attributes extends Fenix_Resource_Model
{

    /**
     * Значения для атрибутов
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecordAttributesValues(){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/by_attributes');

        $this->setTable('seo_by_attributes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        //$Select ->where('a.id = ?', (int) $id);
        //$Select ->limit(1);

        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Статья по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRecordById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/by_attributes');

        $this->setTable('seo_by_attributes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getAttributesAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/by_attributes');

        $this->setTable('seo_by_attributes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.create_date desc');

        return $Select;
    }

    /**
     * Новая страница
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addRecord($Form, $req)
    {
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', (string) $req->getPost('title_russian'));
        }

        $req->setPost('url_key', (string) Fenix::stringProtectUrl($req->getPost('url_key')));

        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->addRecord($req);

        return (int) $id;
    }

    /**
     * Редактировать страницу
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editRecord($Form, $current, $req)
    {
        if ($req->getPost('url_key') == null) {
            $req->setPost('url_key', (string) $req->getPost('title_russian'));
        }
        $req->setPost('url_key', (string) Fenix::stringProtectUrl($req->getPost('url_key')));

        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $Form->editRecord($current, $req);

        return (int)  $current->id;
    }

    /**
     * Удаление страницы
     *
     * @param $current
     */
    public function deleteRecord($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('seo/by_attributes', $current->attributeset)
                 ->deleteRecord($current);

    }

    /**
     *  Копирование фильтра
     *  @param $current
     *
     * @return integer Copied record ID
     */
    public function copyRecord($current) {
        if($current == null) {
            return false;
        }

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('current', null);

        // Источник
        $Form->setSource('seo/by_attributes', $current->attributeset)
             ->renderSource();
        // Компиляция
        $Form->compile();

        $req = $this->getRequest();

        $currentArr = $current->toArray();

        foreach($currentArr as $_property => $_value) {
            $req->setPost($_property, $_value);
        }

        $req->setPost('id', '');
        $req->setPost('title', $current->title . Fenix::lang('(копия)'));
        $req->setPost('create_date', date('Y-m-d H:i:s'));
        $req->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);

        return $this->addRecord($Form, $req);
    }

    public function getAttributesSelect($attributeSysTitle){
        $Field = Fenix::getCreatorUI()
                      ->loadPlugin('Form_Select')
                      ->setId('attribute_sys_title')
                      ->setName('attribute_sys_title');
        if($attributeSysTitle)
        {
            $Field->setSelected($attributeSysTitle);
        }
        $attributesList = Fenix::getModel('catalog/filter')->getAvailableAttributes();
        foreach($attributesList as $i => $_attr) {
            if ($_attr->sys_title == 'price') continue;

            $Field->addOption($_attr->sys_title, $_attr->title, 'level-0');
        }

        return $Field->fetch();
    }

}