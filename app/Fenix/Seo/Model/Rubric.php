<?php
class Fenix_Seo_Model_Rubric extends Fenix_Resource_Model
{
    /**
     * Рубрика по URL
     *
     * @param $url
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRubric($url)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('seo/rubric');

        $this->setTable('seo_rubric');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'r' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'r')));

        $Select ->where('r.url_key = ?', $url);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}