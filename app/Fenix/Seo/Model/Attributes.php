<?php
class Fenix_Seo_Model_Attributes extends Fenix_Resource_Model
{
    public function getAttributeSeo($attributeSysTitle, $categoryId)
    {

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('seo/by_attributes');
        $this->setTable($Engine->getTable());
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));

        $Select->where('s.is_public = ?', '1');
        $Select->where('s.attribute_sys_title = ?', $attributeSysTitle);
        $Select->where('s.category_id = ?', $categoryId);
        $Select->limit('1');
        $Result = $this->fetchRow($Select);

        //Если не нашли в привязке по категории, ищем общий
        if ($Result == null) {
            $Select->reset(Zend_Db_Select::WHERE);
            $Select->where('s.is_public = ?', '1');
            $Select->where('s.attribute_sys_title = ?', $attributeSysTitle);
            $Select->where('s.category_id = ?', 0);
            $Result = $this->fetchRow($Select);
        }
        return $Result;
    }
}