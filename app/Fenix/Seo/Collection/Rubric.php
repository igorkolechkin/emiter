<?php
class Fenix_Seo_Collection_Rubric extends Fenix_Resource_Collection
{
    use Fenix_Traits_SeoCollection;

    public function load($rubric)
    {
        if ($rubric == null) {
            return false;
        }

        $_data        = $rubric->toArray();
        $_data['seo'] = $this->getSeoMetadata($_data, 'seo_rubric');
        $_data['url'] = Fenix::getUrl('seo/' . $_data['url_key']);

        return new Fenix_Object(array(
            'data' => $_data
        ));
    }
}