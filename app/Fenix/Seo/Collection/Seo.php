<?php
class Fenix_Seo_Collection_Seo extends Fenix_Resource_Collection
{
    public function load($rubric)
    {
        $_seo = Fenix::getModel('seo/seo')->getSeoList($rubric);

        $_seo = $_seo->getCurrentItems()->toArray();

        $_data = array();
        foreach ($_seo AS $_record) {
            $_data[] = Fenix::getCollection('seo/record')->load($_record, $rubric);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data
        ));
    }

    public function getLastSeo()
    {
        $_seo = Fenix::getModel('seo/seo')->getLastSeo();

        $_data = array();
        foreach ($_seo AS $_record) {
            $_data[] = Fenix::getCollection('seo/record')->load($_record, null);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}