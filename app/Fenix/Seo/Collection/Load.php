<?php
class Fenix_Seo_Collection_Load extends Fenix_Resource_Collection
{
    public function load($rubric = null)
    {
        return new Fenix_Object(array(
            'data' => array(
                'rubric'  => Fenix::getCollection('seo/rubric')->load($rubric),
                'seo' => Fenix::getCollection('seo/seo')->load($rubric)
            )
        ));
    }
}