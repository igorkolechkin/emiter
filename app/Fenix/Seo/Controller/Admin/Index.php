<?php
class Fenix_Seo_Controller_Admin_Index extends Fenix_Controller_Action
{
    private $_engine = null;

    public function preDispatch()
    {
        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('seo/seo')
                       ->prepare()
                       ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('seo/relations')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('seo/rubric')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('seo/attributes')
                ->prepare()
                ->execute();

        Fenix::getModel('catalog/backend_attributes')->fillEmptyValuesUrl();
    }

    /**
     * Управление страницами
     */
    public function indexAction()
    {
        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentRecord = Fenix::getModel('seo/backend_seo')->getRecordById($_rowId);
                    Fenix::getModel('seo/backend_seo')->deleteRecord($currentRecord);
                }

                Fenix::redirect('seo');
            }
        }

        $seoList  = Fenix::getModel('seo/backend_seo')->getSeoAsSelect();

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('seo/add/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый SEO фильтр по значениям"))
                    ->setType('button')
                    ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('seo/add') . '\'' : $Dialog->toButton()))
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("SEO фильтры по значениям"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("SEO фильтры по значениям"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );
        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('pagesList')
            ->setTitle(Fenix::lang("Управление SEO фильтрами по значениям"))
            ->setData($seoList)
            ->setCheckall()
            ->setStandartButtonset()
            ->setCellCallback('create_date', function($value) {
                return Fenix::getDate($value)->format('d.m.Y H:i:s');
            })
            ->setCellCallback('modify_date', function($value){
                if ($value != '0000-00-00 00:00:00')
                    return Fenix::getDate($value)->format('d.m.Y H:i:s');
                return;
            })
            ->setCellCallback('city', function($value){
                $city = Fenix::getModel('geo/geo')->getCityById($value);
                if($city)
                    return $city->name;
                else
                    return $value;
            })
            ->setCellCallback('district', function($value){
                $city = Fenix::getModel('geo/geo')->getDistrictById($value);
                if($city)
                    return $city->name;
                else
                    return $value;
            });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'copy',
            'title' => Fenix::lang("Копировать"),
            'url'   => Fenix::getUrl('seo/copy/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('seo/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('seo/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("SEO фильтры по значениям"));

        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch(),
            $Table->fetch('seo/seo')
        ));
    }

    /**
     * Новая страница
     */
    public function addAction()
    {
        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("SEO фильтры по значениям"),
            'id'    => 'seo',
            'uri'   => Fenix::getUrl('seo')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Form      ->setDefaults(array(
            'create_date' => date('Y-m-d'),
            'create_time' => date('H:i')
        ));
        $Form      ->setData('current', null);

        // Источник
        $Form      ->setSource('seo/seo', $attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);

            $id = Fenix::getModel('seo/backend_seo')->addRecord($Form, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("SEO фильтр создан"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('seo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('seo/add/attributeset/' . $attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('seo/edit/attributeset/' . $attributeset . '/id/' . $id);
            }

            Fenix::redirect('seo');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый SEO фильтры по значениям"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать страницу
     */
    public function editAction()
    {
        $currentRecord = Fenix::getModel('seo/backend_seo')->getRecordById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRecord == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("SEO фильтр не найден"))
                ->saveSession();

            Fenix::redirect('seo');
        }

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("SEO фильтры по значениям"),
            'id'    => 'seo',
            'uri'   => Fenix::getUrl('seo')
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Редактировать"),
            'id'    => 'edit',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        // Работа с формой
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');

        $Defaults  = $currentRecord->toArray();

        list($createDate, $createTime) = explode(' ', $currentRecord->create_date);

        $Defaults['create_date'] = $createDate;
        $Defaults['create_time'] = $createTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentRecord);

        // Источник
        $Form      ->setSource('seo/seo', $currentRecord->attributeset)
                   ->renderSource();

        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date',  date('Y-m-d H:i:s'));

            $id = Fenix::getModel('seo/backend_seo')->editRecord($Form, $currentRecord, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("SEO фильтр отредактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('seo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('seo/add/attributeset/' . $currentRecord->attributeset);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('seo/edit/attributeset/' . $currentRecord->attributeset . '/id/' . $id);
            }

            Fenix::redirect('seo');
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Отредактировать SEO фильтр по значениям"));

        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Удалить страницу
     */
    public function deleteAction()
    {
        $currentRecord = Fenix::getModel('seo/backend_seo')->getRecordById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRecord == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Статья не найдена"))
                ->saveSession();

            Fenix::redirect('seo');
        }

        Fenix::getModel('seo/backend_seo')->deleteRecord($currentRecord);

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Статья удалена"))
            ->saveSession();

        Fenix::redirect('seo');
    }

    /**
     * Копировать страницу
     */
    public function copyAction(){
        $currentRecord = Fenix::getModel('seo/backend_seo')->getRecordById(
            $this->getRequest()->getParam('id')
        );

        if ($currentRecord == null) {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("SEO фильтр не найден"))
                 ->saveSession();

            Fenix::redirect('seo');
        }

        $id = Fenix::getModel('seo/backend_seo')->copyRecord(
            $currentRecord
        );

        if($id > 0){
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Вы успешно скопировали SEO фильтр"))
                 ->saveSession();
        } else {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_WARNING)
                 ->setMessage(Fenix::lang("SEO фильтр не скопирован. Возникла ошибка"))
                 ->saveSession();
        }

        Fenix::redirect('seo');
    }
}