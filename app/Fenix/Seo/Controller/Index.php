<?php
class Fenix_Seo_Controller_Index extends Fenix_Controller_Action
{
    /**
     * Роутинг статей
     *
     * @throws Exception
     */
    public function preDispatch()
    {
        $first  = $this->getRequest()->getUrlSegment(1);
        $second = $this->getRequest()->getUrlSegment(2);

        if ($first != null && $second != null) {
            $rubric  = Fenix::getModel('seo/rubric')->getRubric($first);
            if ($rubric == null) {
                throw new Exception();
            }
            else {
                $record = Fenix::getModel('seo/seo')->getRecord($second, $rubric);
                if ($record == null) {
                    throw new Exception();
                }
                else {
                    $this->recordAction($record, $rubric);
                }
            }
        }
        else {
            if ($first != null && $second == null) {
                $record = Fenix::getModel('seo/seo')->getRecord($first);
                if ($record == null) {
                    $rubric = Fenix::getModel('seo/rubric')->getRubric($first);
                    if ($rubric == null) {
                        throw new Exception();
                    }
                    else {
                        $this->_indexAction($rubric);
                    }
                }
                else {
                    $this->recordAction($record);
                }
            }
            else {
                $this->_indexAction();
            }
        }
    }

    /**
     * Нужен для работы диспетчера
     */
    public function indexAction(){}

    /**
     * Список статей в рубрике или общий список статей
     *
     * @param null $rubric
     */
    private function _indexAction($rubric = null)
    {
        $collection = Fenix::getCollection('seo/load')->load($rubric);
        $_rubric    = $collection->getRubric();
        $_seo  = Fenix::getModel('seo/seo')->getSeoList($rubric);


        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('seo/seo/h1', Fenix::lang("Новости")),
            'uri'   => Fenix::getUrl('seo'),
            'id'    => 'seo'
        );
        if ($_rubric !== false) {
            $_crumbs[] = array(
                'label' => $_rubric->getTitle(),
                'uri'   => Fenix::getUrl('seo/' . $_rubric->getUrlKey()),
                'id'    => 'rubric'
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        if($_rubric !== false) {
            $this->setMeta($Creator, $_rubric, $_seo);
        } else {
            $this->setMeta($Creator, array(
                'url'         => Fenix::getUrl('seo'),
                'title'       => Fenix::getConfig('seo/seo/title'),
                'keywords'    => Fenix::getConfig('seo/seo/keywords'),
                'description' => Fenix::getConfig('seo/seo/description'),
            ), $_seo);
        }

        $Creator ->getView()->assign(array(
            'rubric'     => $_rubric,
            'seo'   => $_seo,
            'collection' => $collection
        ));

        $Creator ->setLayout()
                 ->render('seo/list.phtml');
    }

    /**
     * Читаем страницу
     *
     * @param $record
     * @param null $rubric
     */
    private function recordAction($record, $rubric = null)
    {
        $_record = Fenix::getCollection('seo/record')->load($record, $rubric);

        // Хлебные крошки
        $_crumbs   = array();
        $_crumbs[] = array(
            'label' => Fenix::getConfig('general/project/name'),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );
        $_crumbs[] = array(
            'label' => Fenix::getConfig('seo/seo/h1', Fenix::lang("Новости")),
            'uri'   => Fenix::getUrl('seo'),
            'id'    => 'seo'
        );
        if ($rubric != null) {
            $_crumbs[] = array(
                'label' => $rubric->title,
                'uri'   => Fenix::getUrl('seo/' . $rubric->url_key),
                'id'    => 'rubric'
            );
        }

        $_crumbs[] = array(
            'label' => $_record->getTitle(),
            'uri'   => $_record->getUrl(),
            'id'    => 'record'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        $Creator ->getView()
                 ->headTitle($_record->getSeo()->getTitle());

        // Ключевые слова
        $keywords = $_record->getSeo()->getKeywords();
        if ($keywords != null)
            $this->view->headMeta()->appendName("keywords", $keywords);

        // Описание
        $description = $_record->getSeo()->getDescription();
        if ($description != null)
            $this->view->headMeta($description, "description");


        $Creator ->getView()->assign(array(
            'rubric'  => $rubric,
            'record' => $_record
        ));
        $Creator ->setLayout()
                 ->render('seo/read.phtml');
    }
}