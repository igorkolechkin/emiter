<?php
class Fenix_Core_Model_Images_Optimize extends Fenix_Resource_Model
{
    public function setImageViewed($imagePath)
    {
        //$imagePath = $imagePath;
        $this->setTable('optimize_images_viewed');

        $data = array(
            'image'          => basename(BASE_DIR . $imagePath),
            'view'           => new Zend_Db_Expr('view + 1'),
            'last_view_date' => new Zend_Db_Expr('NOW()')
        );
        $updateCount = $this->update($data, $this->getAdapter()->quoteInto('image_path = ?', $imagePath));
        if ($updateCount == 0) {
            $data['image_path'] = $imagePath;
            $data['view']       = 1;
            $this->insert($data);
        }
    }

    public function getViewedImages($step = 10)
    {
        $this->setTable('optimize_images_viewed');
        $select = $this->select();
        $select->where('is_optimized = ?', '0');
        $select->order('last_view_date desc');
        $select->order('view desc');

        $select->limit($step);
        $result = $this->fetchAll($select);
        return $result;
    }
    public function getViewedImage($imageUrl)
    {
        $this->setTable('optimize_images_viewed');
        $select = $this->select();
        $select->where('image_path = ?',$imageUrl);
        $select->limit(1);
        $result = $this->fetchRow($select);
        return $result;
    }

    public function checkIsOptimized($imageUrl){

        $this->setTable('optimize_images_optimized');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ));
        $Select ->where('a.image = ?', pathinfo($imageUrl, PATHINFO_BASENAME))
                ->where('a.image_path = ?', $imageUrl);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);
        if($Result)
            return true;
        else
            return false;
    }

    public function setIsOptimized($imageUrl){

        $this->setTable('optimize_images_optimized');
        $data = array(
            'image'         => pathinfo($imageUrl, PATHINFO_BASENAME),
            'image_path'    => $imageUrl,
            'optimize_date' => new Zend_Db_Expr('NOW()')
        );
        $id = $this->insert($data);

        $viewedImage = $this->getViewedImage($imageUrl);
        if($viewedImage){
            $data = array(
                'is_optimized' => '1'
            );

            $this->setTable('optimize_images_viewed')
                 ->update($data, $this->getAdapter()->quoteInto('id = ?', (int)$viewedImage->id));
        }

        return $id;
    }


    public function clearOptimizedLog()
    {
        $this->setTable('optimize_images_optimized');
        $deleteCount = $this->delete('id > 0');

        $data = array('is_optimized' => '0');
        $this->setTable('optimize_images_viewed')
             ->update($data, 'id > 0');

        return $deleteCount;
    }

    public function getFiles($dirPath, $limit  = null){

        $files = array();
        $dirPathAbsolute = BASE_DIR . $dirPath;

        if ($handle = opendir($dirPathAbsolute)) {
            while (false !== ($item = readdir($handle))) {
                if ($item != "." && $item != "..") {
                    $currentItemPath = $dirPath. '/' . $item;
                    if (is_dir($dirPathAbsolute. '/' . $item)){
                        $subDirFiles = $this->getFiles($currentItemPath, $limit);
                        $files = array_merge($files, $subDirFiles);
                    }

                    $extension = strtolower(pathinfo($item, PATHINFO_EXTENSION));
                    if (in_array($extension, array('jpg', 'jpeg', 'png')) && $this->checkIsOptimized($currentItemPath) == false) {
                        $files[] = $currentItemPath;
                    }
                    if($limit && $limit < count($files))
                        return $files;
                }
            }
        }

        return $files;
    }
/*    public function processOptimizationOld(){

//Fenix::dump('asdf');
        $cache        = Fenix::getModel('core/images_transfer')->getCache();
        $cache->token = Fenix::getModel('core/images_transfer')->createToken();
        if($cache->token == false)
        {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Ошибка авторизации сервиса. Проверьте API ключ"))
                 ->saveSession();

            return false;
        }

        $step = 100;
        $n = (int)(Fenix::getRequest()->getParam('n') ? Fenix::getRequest()->getParam('n') : 0);
        if($n == 0) {
            $cache->count = 0;
            $cache->successCount = 0;
            $cache->error = array();
            $cache->actionsLog = array();
        }
        $files = $this->getFiles('tmp/cache', $step);
        //$files = $this->getFiles('var/upload', $step);

        $resultCount = count($files);
        $successLog = array();
        $failLog = array();
        foreach($files as $originalImage){
            //$localFilename = pathinfo($originalImage,PATHINFO_BASENAME);
            $cache->actionsLog[] = 'Оптимизация: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            $success = false;

            //Создаем backup файла
            $backupFile = 'optimize/' . pathinfo($originalImage, PATHINFO_BASENAME);

            copy(BASE_DIR . $originalImage, TMP_DIR_ABSOLUTE . $backupFile);


            //Fenix::dump(BASE_URL . $originalImage, BASE_URL . $originalUrl);
            //Оптимизируем
            $remoteImageUrl = Fenix::getModel('core/images_transfer')->sendImage($cache->token, BASE_URL . $originalImage);

            if($remoteImageUrl){
                echo $originalImage . ' > ' . $remoteImageUrl . '<br/>';

                $copyResult = copy($remoteImageUrl, $originalImage);

                if($copyResult == false)
                {
                    $cache->actionsLog[] = 'не удалось скопировать(copy) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $ch = curl_init($remoteImageUrl);
                    $fp = fopen($originalImage, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                    if(file_exists($originalImage))
                    {
                        $cache->actionsLog[] = 'файл сохранен через curl: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success =  true;
                    }
                    else
                    {
                        $cache->actionsLog[] = 'не удалось скопировать(curl) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success = false;
                    }
                }else {
                   // $cache->actionsLog[] = 'файл сохранен через copy: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $success = true;
                }
            }else {
                $cache->actionsLog[] = '*** Ошибка оптимизации: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
                $cache->actionsLog[] = '*** Backup файла: <a href="' . TMP_DIR_URL . $backupFile . '" target="_blank">' . $backupFile . '</a>';
            }
            if ($success) {
                $successLog [] = $originalImage;
                $this->setIsOptimized($originalImage);
                unlink(TMP_DIR_ABSOLUTE . $backupFile);
            } else{
                $failLog[] = 'не удалось оптимизировать файл: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            }
        }
        $cache->count        = $cache->count + $resultCount;
        $cache->successCount = $cache->successCount + count($successLog);
        $cache->error        = array_merge($cache->error, $failLog);
        if ($resultCount >= $step) {
           /// if($n < 200){
                $url = Fenix::getUrl('core/images/optimize/process/n/' . ($n + $step));
                print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                print 'Подождите...<br /><br />';
                exit;
            //}
        }
    }*/
    public function processOptimization(){

//Fenix::dump('asdf');
        $cache        = Fenix::getModel('core/images_transfer')->getCache();
        $cache->token = Fenix::getModel('core/images_transfer')->createToken();
        if($cache->token == false)
        {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Ошибка авторизации сервиса. Проверьте API ключ"))
                 ->saveSession();

            return false;
        }


        $limit = 10;

        $files = Fenix::getModel('core/images_files')->getFilesForOptimization($limit);
        $successLog = array();
        $errorLog = array();
        $counter = 0;
        foreach($files as $file){
            $counter++;
            $originalImage = $file->path;

            //$localFilename = pathinfo($originalImage,PATHINFO_BASENAME);
            $cache->actionsLog[] = 'Оптимизация: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            $success = false;

            //Создаем backup файла
            $backupFile = 'optimize/' . pathinfo($originalImage, PATHINFO_BASENAME);

            copy(BASE_DIR . $originalImage, TMP_DIR_ABSOLUTE . $backupFile);

            //Fenix::dump(BASE_URL . $originalImage, BASE_URL . $originalUrl);
            //Оптимизируем
            $remoteImageUrl = Fenix::getModel('core/images_transfer')->sendImage($cache->token, BASE_URL . $originalImage);

            if($remoteImageUrl){
                $cache->actionsLog[]= $originalImage . ' > ' . $remoteImageUrl . '<br/>';

                $copyResult = copy($remoteImageUrl, $originalImage);

                if($copyResult == false)
                {
                    $cache->actionsLog[] = 'не удалось скопировать(copy) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $ch = curl_init($remoteImageUrl);
                    $fp = fopen($originalImage, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                    if(file_exists($originalImage))
                    {
                        $cache->actionsLog[] = 'файл сохранен через curl: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success =  true;
                    }
                    else
                    {
                        $cache->actionsLog[] = 'не удалось скопировать(curl) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success = false;
                    }
                }else {
                    // $cache->actionsLog[] = 'файл сохранен через copy: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $success = true;
                }
            }else {
                $cache->actionsLog[] = '*** Ошибка оптимизации: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
                $cache->actionsLog[] = '*** Backup файла: <a href="' . TMP_DIR_URL . $backupFile . '" target="_blank">' . $backupFile . '</a>';
            }
            if ($success) {
                $successLog [(int)$file->id] = $originalImage;
                //$this->setIsOptimized($originalImage);
                unlink(TMP_DIR_ABSOLUTE . $backupFile);
            } else{
                $errorLog[] = (int)$file->id;
                $cache->failLog[] = 'не удалось оптимизировать файл: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            }
        }

        Fenix::getModel('core/images_files')->setFilesOptimized(array_keys($successLog));
        Fenix::getModel('core/images_files')->setFilesError($errorLog);
        //Fenix::dump($cache->actionsLog);
        /*$cache->count        = $cache->count + $resultCount;
        $cache->successCount = $cache->successCount + count($successLog);
        $cache->error        = array_merge($cache->error, $failLog);
        if ($resultCount >= $step) {
            /// if($n < 200){
            $url = Fenix::getUrl('core/images/optimize/process/n/' . ($n + $step));
            print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
            print 'Подождите...<br /><br />';
            exit;
            //}
        }*/
    }

    public function processViewedOptimization(){
        $cache        = Fenix::getModel('core/images_transfer')->getCache();
        $cache->token = Fenix::getModel('core/images_transfer')->createToken();
        if($cache->token == false)
        {
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_ERROR)
                 ->setMessage(Fenix::lang("Ошибка авторизации сервиса. Проверьте API ключ"))
                 ->saveSession();

            return false;
        }

        $step = 100;
        $n = (int)(Fenix::getRequest()->getParam('n') ? Fenix::getRequest()->getParam('n') : 0);
        if($n == 0) {
            $cache->count = 0;
            $cache->successCount = 0;
            $cache->error = array();
            $cache->actionsLog = array();
        }
        $files = $this->getViewedImages($step);
        $resultCount = $files->count();
        $successLog = array();
        $failLog = array();

        foreach($files as $imageInfo){
            $originalImage= $imageInfo->image_path;
            //$localFilename = pathinfo($originalImage,PATHINFO_BASENAME);
            $cache->actionsLog[] = 'Оптимизация: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            $success = false;

            //Создаем backup файла
            $backupFile = 'optimize/' . pathinfo($originalImage, PATHINFO_BASENAME);
            copy(BASE_DIR . $originalImage, TMP_DIR_ABSOLUTE . $backupFile);

            //Оптимизируем
            $remoteImageUrl = Fenix::getModel('core/images_transfer')->sendImage($cache->token, BASE_URL . $originalImage);


            if($remoteImageUrl){
                echo $originalImage . ' > ' . $remoteImageUrl . '<br/>';

                $copyResult = copy($remoteImageUrl, $originalImage);

                if($copyResult == false)
                {
                    $cache->actionsLog[] = 'не удалось скопировать(copy) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $ch = curl_init($remoteImageUrl);
                    $fp = fopen($originalImage, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                    if(file_exists($originalImage))
                    {
                        $cache->actionsLog[] = 'файл сохранен через curl: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success =  true;
                    }
                    else
                    {
                        $cache->actionsLog[] = 'не удалось скопировать(curl) файл: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                        $success = false;
                    }
                }else {
                    // $cache->actionsLog[] = 'файл сохранен через copy: <a href="' . $remoteImageUrl . '" target="_blank">' . $remoteImageUrl . '</a>';
                    $success = true;
                }
            }else {
                $cache->actionsLog[] = '*** Ошибка оптимизации: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
                $cache->actionsLog[] = '*** Backup файла: <a href="' . TMP_DIR_URL . $backupFile . '" target="_blank">' . $backupFile . '</a>';
            }
            if ($success) {
                $successLog [] = $originalImage;
                //Fenix::dump(BASE_URL . $originalImage, $remoteImageUrl );
                //$this->setIsOptimized($originalImage);
                unlink(TMP_DIR_ABSOLUTE . $backupFile);
            } else{
                $failLog[] = 'не удалось оптимизировать файл: <a href="' . BASE_URL . $originalImage . '" target="_blank">' . $originalImage . '</a>';
            }
        }
        $cache->count        = $cache->count + $resultCount;
        $cache->successCount = $cache->successCount + count($successLog);
        $cache->error        = array_merge($cache->error, $failLog);
        if ($resultCount >= $step) {
            //if($n < 200){
                $url = Fenix::getUrl('core/images/optimize/process/mode/viewed/n/' . ($n + $step));
                print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                print 'Подождите...<br /><br />';
                exit;
            //}
        }
    }
}