<?php
class Fenix_Core_Model_Images_Files extends Fenix_Resource_Model
{
    const RECURSIVE_SEARCH = true;
    const NOT_RECURSIVE_SEARCH = false;
    public function getFiles($dirPath){

        $files = array();
        $dirs = array();
        $dirPathAbsolute = BASE_DIR . $dirPath;

        if ($handle = opendir($dirPathAbsolute)) {
            while (false !== ($item = readdir($handle))) {
                if ($item != "." && $item != "..") {
                    $currentItemPath = $dirPath. '/' . $item;
                    if (!is_dir($dirPathAbsolute. '/' . $item)){
/*                        $dirs = array();
                        $subDirFiles = $this->getFiles($currentItemPath, $limit);
                        $files = array_merge($files, $subDirFiles);
*/

                        $extension = strtolower(pathinfo($item, PATHINFO_EXTENSION));
                        //if (in_array($extension, array('jpg', 'jpeg', 'png')) && $this->checkIsOptimized($currentItemPath) == false) {
                        if (in_array($extension, array('jpg', 'jpeg', 'png'))) {
                            $files[] = $currentItemPath;
                        }
                    }


                    /*if($limit && $limit < count($files))
                        return $files;*/
                }
            }
        }

        return $files;
    }

    public function getDirectoriesList($dirPathAbsolute, $directoryId = null, $recursive = false, $level)
    {
        $dirList = array();
        $currentCount = 0;
        if ($handle = opendir(BASE_DIR . $dirPathAbsolute)) {
            while (false !== ($item = readdir($handle))) {
                if ($item != "." && $item != "..") {
                    $currentItemPath = $dirPathAbsolute . '/' . $item;
                    if (is_dir(BASE_DIR . $dirPathAbsolute . '/' . $item)) {
                        $currentCount++;
                        $directoryId = $this->addDirectory($currentItemPath, $directoryId,$level);
                        $dirList[]      = $dirPathAbsolute . '/' . $item;
                        if($recursive)
                        {
                            $subDirectories = $this->getDirectoriesList($currentItemPath, $directoryId, $recursive,$level+1);
                            $dirList = array_merge($dirList, $subDirectories);
                        }
                    }
                    //$this->setScanFinished($directoryId);
                }
            }
        }
        //Fenix::dump($currentCount);
        /*if($currentCount == 0){
            $this->setScanFinished($directoryId);
        }else {
            $status = $this->getDirectoriesStatus($directoryId);
            //Fenix::dump($status);
            if($status->scan_finished_count == $status->scan_total_count){
                $this->setScanFinished($directoryId);
            }
        }*/

        return $dirList;
    }

    public function addDirectory($path, $directoryId = null, $level = 0)
    {

        $data = array(
            'parent' => ($directoryId ? $directoryId : 0),
            'path'   => $path,
            'level'  => $level
        );

        $id = $this->setTable('optimize_directories')
                   ->insert($data);

        return $id;
    }
    public function addFile($path, $directoryId = null)
    {

        $data = array(
            'parent' => ($directoryId ? $directoryId : 0),
            'path'   => $path
        );

        $id = $this->setTable('optimize_files')
                   ->insert($data);

        return $id;
    }

    public function setScanFinished($directoryId){

        $data = array(
            'scan_finished' => '1'
        );

        $id = $this->setTable('optimize_directories')
                   ->update($data, $this->getAdapter()->quoteInto('id = ?', (int) $directoryId));

        return $id;
    }

    public function clearDirectories(){

        $this->setTable('optimize_directories')
             ->delete('id > 0');

        $this->getAdapter()->query('ALTER TABLE ' . $this->getTable('optimize_directories') . ' AUTO_INCREMENT = 1');

        return true;
    }
    public function clearFiles(){

        $this->setTable('optimize_files')
             ->delete('id > 0');

        $this->getAdapter()->query('ALTER TABLE ' . $this->getTable('optimize_files') . ' AUTO_INCREMENT = 1');

        return true;
    }
    public function getRootCategories($limit){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/optimize_directories');

        $this->setTable('optimize_directories');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'od' => $this->_name
        ),$Engine->getColumns());
        $Select->where('scan_finished = ?', '0');
        $Select->where('parent = ?', '0');
        $Select->order('level desc');
        $Select->limit($limit);
        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getCategoriesForScan($limit){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/optimize_directories');

        $this->setTable('optimize_directories');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'od' => $this->_name
        ),$Engine->getColumns());

        $Select->where('od.scan_finished = ?', '0');

        $Select->limit($limit);
        $Result = $this->fetchAll($Select);

        return $Result;
    }
    public function getFilesForOptimization($limit){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/optimize_files');

        $this->setTable('optimize_files');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'od' => $this->_name
        ),$Engine->getColumns());

        $Select->where('od.is_optimized = ?', '0');
        $Select->where('od.error = ?', '0');
        $Select->order('od.id asc');

        $Select->limit($limit);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function setFilesOptimized(array $ids){

        $this->setTable('optimize_files');
        $data = array(
            'is_optimized' =>'1'
        );
        $r = $this->update($data, 'id IN("' . implode('","', $ids) . '")');

    }
    public function setFilesError(array $ids){

        $this->setTable('optimize_files');
        $data = array(
            'error' =>'1'
        );
        $r = $this->update($data, 'id IN("' . implode('","', $ids) . '")');

    }

    public function prepareDirectoryTree($dirPath = null)
    {
        if($dirPath == null)
        {
            $limit = 100;
            $root = $this->getRootCategories($limit);

            foreach($root as $directory){
                $this->getDirectoriesList($directory->path, null, self::RECURSIVE_SEARCH, 1);
            }
        }
        //Fenix::dump($root);
    }


    public function getDirectoriesStatus($parentId = null){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');

        $this->setTable('optimize_directories');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'od' => $this->_name
        ),array(
            'COALESCE(SUM(IF(od.scan_finished = "1", 1, 0)),0) as scan_finished_count',
            'COALESCE(SUM(IF(od.scan_finished = "0", 1, 0)),0) as scan_not_finished_count',
            'COUNT(id) as scan_total_count'
        ));

        if ($parentId)
            $Select->where('parent = ?', (int)$parentId);

        $Select->where('id > 0');
        $Result = $this->fetchRow($Select);

        return $Result;
    }
    public function getFilesStatus($parentId = null){
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/optimize_files');

        $this->setTable('optimize_files');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'od' => $this->_name
        ),array(
            'COALESCE(SUM(IF(od.is_optimized = "1", 1, 0)),0) as is_optimized_count',
            'COALESCE(SUM(IF(od.error = "1", 1, 0)),0) as error_count',
            'COUNT(id) as total_count'
        ));

        if ($parentId)
            $Select->where('parent = ?', (int)$parentId);

        $Select->where('id > 0');
        $Result = $this->fetchRow($Select);

        return $Result;
    }
    public function scanFiles(){
        $limit= 200;

        $root = $this->getCategoriesForScan($limit);

        foreach($root as $directory){
            $files = $this->getFiles($directory->path);

            foreach ($files as $filePath){
                $this->addFile($filePath);
            }
            $this->setScanFinished($directory->id);
        }
    }
}