<?php
class Fenix_Core_Model_Images_Transfer extends Fenix_Resource_Model
{
    const API_BASE_URL           = 'http://serverapi.fnx.dp.ua';
    const API_AUTH_URL           = '/api/json/auth';
    const API_IMAGE_OPTIMIZE_URL = '/api/json/image/optimize';

    public function createToken(){

        $request = array(
            'api_key' => Fenix::getConfig('optimize_api_key')
        );

        $ch          = curl_init();
        $url = self::API_BASE_URL . self::API_AUTH_URL;
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $responseData = curl_exec($ch);

        if (curl_errno($ch)) {
            //return curl_error($ch);
            return false;
        }
        curl_close($ch);

        $response = json_decode($responseData);
        if ($response && $response->error == false && $response->success == '1')
            return $response->data->token;
        else
            return false;
    }

    public function sendImage($token, $imageUrl){
        $request = array(
            'token' => $token,
            'image' => $imageUrl
        );

        $ch  = curl_init();
        $url = self::API_BASE_URL . self::API_IMAGE_OPTIMIZE_URL . '?' . http_build_query($request);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $responseData = curl_exec($ch);

        if (curl_errno($ch)) {
            //return curl_error($ch);
            return false;
        }
        curl_close($ch);

        $response = json_decode($responseData);
        if ($response && $response->error == '0' && $response->success == '1')
            return $response->data->image;
        else
            return false;
    }
    public function getCache(){
        return new Zend_Session_Namespace('Fenix_Images_Optimize_Transfer');
    }
}