<?php
class Fenix_Core_Model_Themes extends Fenix_Resource_Model
{
    /**
     * Генерация фавиконки
     *
     * @return string
     */
    public function getFavicon()
    {
        if(Fenix::getTheme()->favicon)
            return HOME_DIR_URL . Fenix::getTheme()->favicon;
        else
            return null;
    }

    /**
     * Генерация логотипа
     *
     * @return string
     */
    public function getLogo()
    {
        if(Fenix::getTheme()->logo)
            return HOME_DIR_URL . Fenix::getTheme()->logo;
        else
            return null;
    }
    /**
     * Генерация логотипа
     *
     * @return string
     */
    public function getWatermark()
    {
        if(Fenix::getTheme()->watermark)
            return HOME_DIR_URL . Fenix::getTheme()->watermark;
        else
            return null;
    }

    /**
     * Генерация фона
     *
     * @return string
     */
    public function getBackground()
    {
        if(Fenix::getTheme()->background)
            return HOME_DIR_URL . Fenix::getTheme()->background;
        else
            return null;
    }

    /**
     * Тема по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getThemesById($id)
    {
        $this->setTable('core_themes');
        
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    public function getThemesByName($name)
    {
        $this->setTable('core_themes');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('name = ?', $name);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Тема по умолчанию
     *
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getDefaultTheme()
    {
        $this->setTable('core_themes');
        
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('is_default = ?', '1');
        $Select ->limit(1);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    /**
     * Список тем
     *
     * @return Zend_Db_Select
     */
    public function getThemesListAsSelect()
    {
        $this->setTable('core_themes');
        
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        
        $Select ->from(array(
            's' => $this->_name
        ));
        
        return $Select;
    }

    /**
     * Обновление темы по умолчанию
     *
     * @param $id
     * @param $isDefault
     */
    public function updateDefaultTheme($id, $isDefault)
    {
        if ($isDefault == '1') {
            $this->setTable('core_themes');
            
            $this->update(array(
                'is_default' => '0'
            ), null);
            
            $this->update(array(
                'is_default' => '1'
            ), 'id = ' . (int) $id);
        }
        
        return;
    }
}