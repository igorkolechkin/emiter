<?php

class Fenix_Core_Model_Common extends Fenix_Resource_Model
{
    const VALID_FILE_TYPES_GALLERY = "jpg,jpeg,png";

    /**
     * Получаем форматы документов разрешенных  к загрузке
     *
     * @return array
     */
    public function getValidFileTypesGallery()
    {
        $typesList = explode(',', self::VALID_FILE_TYPES_GALLERY);
        return $typesList;
    }

    /**
     * Обновление изображений галереи
     *
     * @param array $filesArray Массив с файлами
     * @param string $dir Папка для загрузки файлов
     * @param string $prefix Префикс файлов
     *
     * @return array
     */
    public function uploadImages($filesArray, $dir, $prefix)
    {
        $imageslist = array();

        // Загружаем новые
        if (isset($filesArray['new'][0])) {
            $destDir = HOME_DIR_ABSOLUTE . $dir;

            if (!is_dir($destDir)) {
                mkdir($destDir, 0777, true);
            }

            $validTypes = $this->getValidFileTypesGallery();

            foreach ($filesArray['new'] AS $image) {
                if (file_exists(TMP_DIR_ABSOLUTE . 'uploads/' . $image) && $image != null) {
                    $from = TMP_DIR_ABSOLUTE . 'uploads/' . $image;

                    $infoFile = new SplFileInfo($from);
                    $fileType = strtolower($infoFile->getExtension());

                    // Проверяем разрешено ли грузить файл согласно списку доступных расширений
                    if (array_search($fileType, $validTypes) !== false) {
                        $image = str_replace(array('[', ']'), null, $image);
                        $to = $destDir . $prefix . $image;

                        $copy = copy($from, $to);

                        if ($copy) {
                            $imageslist[$dir . $prefix . $image] = array(
                                'image' => $dir . $prefix . $image,
                            );

                            if (!empty($filesArray['meta'][$dir . $prefix . $image])) {
                                foreach ($filesArray['meta'][$dir . $prefix . $image] as $name => $value) {
                                    $imageslist[$dir . $prefix . $image][$name] = $value;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Обновляем старые
        if (isset($filesArray['old'][0])) {
            foreach ($filesArray['old'] AS $image) {
                $imageslist[$image] = array('image' => $image);

                if (!empty($filesArray['meta'][$image])) {
                    foreach ($filesArray['meta'][$image] as $name => $value) {
                        $imageslist[$image][$name] = $value;
                    }
                }
            }
        }

        // Удаляем отмеченные
        if (array_key_exists('delete', $filesArray) && is_array($filesArray['delete'])) {
            foreach ($filesArray['delete'] AS $file => $true) {
                if (isset($imageslist[$file]) && file_exists(HOME_DIR_ABSOLUTE . $file)) {
                    unset($imageslist[$file]);
                    unlink(HOME_DIR_ABSOLUTE . $file);
                }
            }
        }

        $imageslist = array_values($imageslist);

        return $imageslist;
    }

    /**
     * Удаление изображений
     *
     * @param array $imagesArray Массив изображений
     */
    public function deleteImages($imagesArray)
    {
        if (sizeof($imagesArray) <= 0) {
            return;
        }

        foreach ($imagesArray AS $image) {
            if (UPLOAD_IMAGES_DIR . $image['image']) {
                unlink(UPLOAD_IMAGES_DIR . $image['image']);
            }
        }

        return;
    }

    /**
     * Обновляем left right для указаной таблицы
     *
     * @param $table
     */
    public function updateTreeIndex($table)
    {
        //Получаем данные структуры
        $Select = $this->setTable($table)
            ->select()
            ->from($this, array(
                'id',
                'parent',
                'left',
                'right',
            ))
            ->where('id > 0');

        $Result = $this->fetchAll($Select);
        $items = $Result->toArray();
        //Обновляем данные
        $this->updateTreeIndexies($items);

        //Сохраняем изменения в бд
        $count = count($items);
        for ($i = 0; $i < $count; $i++) {
            $this->update($items[$i], $this->getAdapter()->quoteInto('id = ?', $items[$i]['id']));
        }
    }


    /**
     * Находит корень и запускает обновление left right в массиве
     *
     * @param $items
     */
    public function updateTreeIndexies(&$items)
    {
        $index = 0;
        $count = count($items);
        for ($i = 0; $i < $count; $i++) {
            //Находим корневой елемент и начинаем проставлять left right
            if ($items[$i]['id'] == 1) {
                $this->setTreeIndex($items, $items[$i], $index);
            }
        }
    }

    /**
     * Обновляет left right  для записми и запускает обновления для детей
     *
     * @param $items
     * @param $item
     * @param $index
     */
    public function setTreeIndex(&$items, &$item, &$index)
    {
        $index++;
        $item['left'] = $index;

        $count = count($items);
        for ($i = 0; $i < $count; $i++) {
            $current = $items[$i];
            if ($current['parent'] == $item['id']) {
                $this->setTreeIndex($items, $current, $index);
                $items[$i] = $current;
            }
        }

        $index++;
        $item['right'] = $index;
    }

    public function checkUniqueValue($table, $field, $value, $current = null)
    {
        $this->setTable($table);

        $Select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('t' => $this->getTable($table)), array($field))
            ->where($field . ' = ?', $value)
            ->where('id <> ?', (int)$current)
            ->limit(1);
        return $this->fetchRow($Select);
    }
}