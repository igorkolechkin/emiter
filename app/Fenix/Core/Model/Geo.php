<?php
class Fenix_Core_Model_Geo extends Fenix_Resource_Model
{
    /**
     * Список стран
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCountriesList()
    {
        $Select = $this->setTable('geo_countries')
                       ->select()
                       ->from($this)
                       ->order(new Zend_Db_Expr('`country` = \'Украина\' desc, `country` = \'Россия\' desc, `country` asc'));
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список областей
     *
     * @param int $countryId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getRegionsList($countryId = 62)
    {
        $Select = $this->setTable('geo_region')
                       ->select()
                       ->from($this)
                       ->where('country_id = ?', (int) $countryId)
                       ->order(new Zend_Db_Expr('`name` = \'Днепропетровская обл.\' desc, `name` = \'Киевская обл.\' desc, `name` = \'Запорожская обл.\' desc, `name` asc'));

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список городов
     *
     * @param $regionId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getCityList($regionId)
    {
        $Select = $this->setTable('geo_city')
                       ->select()
                       ->from($this)
                       ->where('region_id = ?', (int) $regionId)
                       ->order('name asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список всех городов, областей и стран связанных в одну таблицу
     *
     * @return Zend_Db_Select
     */
    public function getGeoList()
    {
        $Select = $this->setTable('geo_city')
                       ->select()
                       ->setIntegrityCheck(false);

        // Джоиним таблицу с городами
        $Select ->from(array(
            'c' => $this->getTable('geo_city')
        ), array(
            'c.id AS city_id',
            'c.region_id AS city_region_id',
            'c.district_id AS city_district_id',
            'c.lon AS city_lon',
            'c.lat AS city_lat',
            'c.prefix AS city_prefix',
            'c.name AS city_name',
            'c.type AS city_type',
            'c.tz AS city_tz',
            'c.timezone AS city_timezone',
            'c.timezone2 AS city_timezone2'
        ));

        // Джоиним страны
        $Select->joinLeft(array(
            'cn' => $this->getTable('geo_countries')
        ), 'cn.id = c.country_id', array(
            'cn.id AS country_id',
            'cn.country AS country_name'
        ));

        // Джоиним регионы
        $Select->joinLeft(array(
            'r' => $this->getTable('geo_region')
        ), 'r.id = c.region_id', array(
            'r.country_id AS region_country_id',
            'r.name AS region_name'
        ));

        $Select ->order('cn.country asc')
                ->order('r.name asc')
                ->order('c.name asc');

        return $Select;
    }
}