<?php

	class Fenix_Core_Model_Currency extends Fenix_Resource_Model {
		public $exchange_url = 'http://bank-ua.com/export/currrate.xml';
		public $exchange_xml = null;

		/**
		 * Валюта по идентификатору
		 *
		 * @param int $id
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getCurrencyById($id) {
			$cacheId = 'model_Currency_getCurrencyById_' . $id;

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}

			$this->setTable('core_currency');

			$Select = $this->select();
			$Select->from($this->_name);
			$Select->where('id = ?', (int)$id);
			$Select->limit(1);

			$Result = $this->fetchRow($Select);
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Валюта по названию
		 *
		 * @param int $id
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getCurrencyByName($name) {
			$cacheId = 'model_Currency_getCurrencyByName_' . $name;

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}

			$this->setTable('core_currency');

			$Select = $this->select();
			$Select->from($this->_name);
			$Select->where('name LIKE ?', $name);
			$Select->limit(1);

			$Result = $this->fetchRow($Select);
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Валюта по коду
		 *
		 * @param int $id
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 */
		public function getCurrencyByCode($code) {
			$cacheId = 'model_Currency_getCurrencyByCode_' . $code;

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}

			$this->setTable('core_currency');

			$Select = $this->select();
			$Select->from($this->_name);
			$Select->where('code LIKE ?', $code);
			$Select->limit(1);

			$Result = $this->fetchRow($Select);
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Возвращает основную валюту
		 *
		 * @return null|Zend_Db_Table_Row_Abstract
		 *
		 */
		public function getDefaultCurrency() {

			$cacheId = 'model_Currency_getDefaultCurrency';

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}

			$this->setTable('core_currency');

			$Select = $this->getCurrencyListAsSelect();
			$Select->where('is_main = ?', '1');
			$Select->limit(1);

			$Result = $this->fetchRow($Select);
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Список валют
		 *
		 * @return Zend_Db_Select
		 */
		public function getCurrencyListAsSelect() {

			$this->setTable('core_currency');
			$Engine = Fenix_Engine::getInstance();
			$Engine->setSource('core/currency');

			$Select = $this->select()
			               ->setIntegrityCheck(false);

			$Select->from(array(
				's' => $this->_name,
			), $Engine->getColumns(array('prefix' => 's')));

			return $Select;
		}

		/**
		 * Список активных валют
		 *
		 * @return Zend_Db_Table_Rowset_Abstract
		 */
		public function getCurrencyList() {

			$cacheId = 'model_Currency_getCurrencyList';

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}


			$Select = $this->getCurrencyListAsSelect();
			$Select->where('s.is_public = ?', '1');

			$Result = $this->fetchAll($Select);
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Список всех валют (активных и не активных)
		 *
		 * @return Zend_Db_Table_Rowset_Abstract
		 */
		public function getAllCurrencyList() {
			$cacheId = 'model_Currency_getAllCurrencyList';

			if(Zend_Registry::isRegistered($cacheId)) {
				return Zend_Registry::get($cacheId);
			}

			$Result = $this->fetchAll($this->getCurrencyListAsSelect());
			Zend_Registry::set($cacheId, $Result);

			return $Result;
		}

		/**
		 * Синхронизация с нацбанков Украины
		 */
		public function sync() {
			try {
				$result = file_get_contents($this->exchange_url);
				$array = explode("\n", $result);
				$xmlDef = array_shift($array);
				array_unshift($array, '<root>');
				array_unshift($array, $xmlDef);
				$array[] = '</root>';
				$result = implode("\n", $array);
				unset($array);

				$this->exchange_xml = new Zend_Config_Xml($result, null, false);
			} catch(Exception $e) {
				return false;
			}

			$Select = $this->setTable('core_currency')
			               ->select();
			$Select->from($this);
			$Select->where('is_sync = ?', '1');
			$Result = $this->fetchAll($Select);

			foreach($Result AS $_currency) {
				$BankRate = $this->getExchangeRateByChar3($_currency->code);

				if($BankRate !== false) {
					$crosscourse = $BankRate->rate / $BankRate->size;

					$this->update(array(
						'crosscourse' => $crosscourse,
					), 'id = ' . (int)$_currency->id);
				}
			}

			return;
		}

		/**
		 * Значение курса по коду валюты
		 *
		 * @param $char3
		 *
		 * @return bool
		 */
		public function getExchangeRateByChar3($char3) {
			if($this->exchange_xml !== false) {
				foreach($this->exchange_xml->chapter->item AS $item) {
					if($item->char3 == $char3) {
						return $item;
						break;
					}
				}
			}

			return false;
		}
	}