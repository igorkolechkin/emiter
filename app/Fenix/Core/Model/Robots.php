<?php
class Fenix_Core_Model_Robots extends Fenix_Resource_Model
{
    public function getRobots(){
        $data = file_get_contents(BASE_DIR."robots.txt");
        return $data;
    }

    public function saveRobots($data){
        file_put_contents(BASE_DIR."robots.txt", $data);
    }
}