<?php

class Fenix_Core_Model_Menu extends Fenix_Resource_Model {

	/**
	 * Активный ли пункт меню слайдера в админке
	 *
	 * @return bool
	 */
	static public function isMenuActive()
	{
		return Fenix::getRequest()->getUrlSegment(0) == 'core' && (
			Fenix::getRequest()->getUrlSegment(1) == 'menu'
		);
	}

	/**
	 * Навигация для хлебных крошек
	 *
	 * @param Zend_Db_Table_Row $page
	 *
	 * @return Zend_Db_Table_Rowset
	 */
	public function getNavigation(Zend_Db_Table_Row $page) {
		$result_arr = array();
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$Select = $this->setTable('menu_item')->select()->from($this, $Engine->getColumns())/* ->where('`left` <= ?', $page->left)
				 ->where('`right` >= ?', $page->right)
				 ->where('parent > 0');*/
		/**  updateTreeIndex для данной таблицы не работает!!! */
		               ->where('id = ?', $page->id);

		$Result = $this->fetchRow($Select);
		$result_arr[] = $Result;

		if(!empty($page->parent)) {
			$result_arr = array_merge($result_arr, $this->getNavigationRecursive($page->parent));
		}

		return $result_arr;
	}

	public function getNavigationRecursive($parent_id) {
		$result_arr = array();
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$Select = $this->setTable('menu_item')->select()->from($this, $Engine->getColumns())->where('id = ?', $parent_id);

		$Result = $this->fetchRow($Select);
		$result_arr[] = $Result;

		if(isset($Result->parent) && !empty($Result->parent) && $Result->parent > 0) {
			return array_merge($result_arr, $this->getNavigationRecursive($Result->parent));
		}

		return $result_arr;
	}

	/**
	 * Добавление нового пункта меню
	 *
	 * @param $Form
	 * @param $req
	 *
	 * @return mixed
	 */
	public function addItem($Form, $req) {
		$id = $Form->addRecord($req);

		/*$this->setTable('structure')
			 ->getAdapter()
			 ->query('CALL updateTreeIndex(\'' . $this->getTable() . '\')');*/
		Fenix::getModel('core/common')->updateTreeIndex('menu_item');

		return $id;
	}

	/**
	 * Редактирование пункта меню
	 *
	 * @param $Form    Объект формы. Для работы с шаблоном XML
	 * @param $current Редактируемая строка
	 * @param $req     Объект запроса
	 *
	 * @return int
	 */
	public function editItem($Form, $current, $req) {
		$id = $Form->editRecord($current, $req);

		/*$this->setTable('structure')
			 ->getAdapter()
			 ->query('CALL updateTreeIndex(\'' . $this->getTable() . '\')');*/

		Fenix::getModel('core/common')->updateTreeIndex('menu_item');

		return $id;
	}

	/**
	 * Проверяем открыт ли этот пункт меню
	 *
	 * @param $menuItem
	 *
	 * @return bool
	 */
	public function isCurrent($menuItem) {
		$currentUrl = Fenix::getRequest()->getPathInfo(); //Текущая страница

		$slash = (substr($menuItem->url_key, 0, 1) == "/" ? null : '/');
		$menuUrl = $slash . $menuItem->url_key;

		$slash = (substr($currentUrl, 0, 1) == "/" ? null : '/');
		$currentUrl = $slash . $currentUrl;


		//Готовим массивы для проверки
		$currentUrlSegments = explode('/', $currentUrl);
		$menuUrlSegments = explode('/', $menuUrl);

		//Удаляем приставку языка
		$lang = Fenix_Language::getInstance()->getCurrentLanguage();


		if(isset($currentUrlSegments[1]) && trim($currentUrlSegments[1]) == $lang->code) {
			if(count($currentUrlSegments) > 2) {
				unset($currentUrlSegments[1]);
				sort($currentUrlSegments);
			} else {
				$currentUrlSegments[1] = '';
			}
		}


		//Сравниваем по частям
		$count_menuUrlSegments = count($menuUrlSegments) - 1;
		foreach($menuUrlSegments as $k => $url_key) {
			/** идем по сегментам вплоть до последнего в массиве элемента меню
			 * если последний сегмент совпадает с структурой текущего УРЛа то этот пункт считаеться активным
			 */
			if(isset($currentUrlSegments[ $k ]) && $currentUrlSegments[ $k ] == $url_key && $k == $count_menuUrlSegments) {
				return true;
			} /** Если сегменты не совпали - не активен */ elseif(isset($currentUrlSegments[ $k ]) && $currentUrlSegments[ $k ] != $url_key && $k <= $count_menuUrlSegments) {
				return false;
			}
		}

		//Прошли все проверки = ок
		return true;
	}

	public function getMenuListAsSelect() {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu');

		$this->setTable('menu');

		$Select = $this->select()->setIntegrityCheck(false);

		$Select->from(array(
			's' => $this->_name,
		), $Engine->getColumns(array(
			'prefix' => 's',
		)));

		$Select->order(new Zend_Db_Expr("s.position = 0, s.position asc"));

		return $Select;
	}

	public function getItemsListAsSelect($parent) {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$this->setTable('menu_item');

		$Select = $this->select()->setIntegrityCheck(false);

		$Select->from(array(
			's' => $this->_name,
		), $Engine->getColumns(array(
			'prefix' => 's',
		)));

		$Select->where('s.parent = ?', (int)$parent);
		$Select->order(new Zend_Db_Expr("s.position = 0, s.position asc"));

		return $Select;
	}

	public function getItemsForMenu($parent, $parent_item) {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$this->setTable('menu_item');

		$Select = $this->select()->setIntegrityCheck(false);

		$Select->from(array(
			's' => $this->_name,
		), $Engine->getColumns(array(
			'prefix' => 's',
		)));

		$Select->where('s.parent = ?', (int)$parent);
		$Select->where('s.parent_item = ?', (int)$parent_item);
		$Select->order(new Zend_Db_Expr("s.position = 0, s.position asc"));
		$Result = $this->fetchAll($Select);

		return $Result;
	}

	/**
	 * Список слайдов
	 *
	 * @param int $parent
	 *
	 * @return Zend_Db_Select
	 */
	public function getListForParent($selected) {
		$request = $this->getRequest()->getPost();

		$parent = isset($request['parent']) ? $request['parent'] : null;
		$id = (isset($request['id'])) ? $request['id'] : null;
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$this->setTable('menu_item');

		$Select = $this->select()->setIntegrityCheck(false);

		$Select->from(array(
			's' => $this->_name,
		), $Engine->getColumns(array(
			'prefix' => 's',
		)));
		if($parent) {
			$Select->where('s.parent = ?', (int)$parent);
		} else {
			$Select->where('s.parent = 5');
		}

		if($id) {
			$Select->where('s.id <> ?', (int)$id);
		}
		$Select->order(new Zend_Db_Expr("s.position = 0, s.position asc"));
		$Result = $this->fetchAll($Select);
		$Select = Fenix::getCreatorUI()->loadPlugin('Form_Select')->setSelected($selected)->setName('parent_item')->setId('parent_item');
		if($Result->count() > 0) {
			$Select->addOption(0, "Корень");
			foreach($Result as $_value) {
				$Select->addOption($_value->id, $_value->title);
			}
		}

		return $Select->fetch();
	}

	/**
	 * Список слайдеров
	 *
	 * @param $parent
	 *
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function getItemsList($parent) {
		$Select = $this->getItemsListAsSelect($parent);
		$Result = $this->fetchAll($Select);

		return $Result;
	}

	/**
	 * Слайдер по идентификатору
	 *
	 * @param $id
	 *
	 * @return null|Zend_Db_Table_Row_Abstract
	 */
	public function getMenuById($id) {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu');

		$this->setTable('menu');

		$Select = $this->select();
		$Select->from($this->_name, $Engine->getColumns());
		$Select->where('id = ?', (int)$id);
		$Select->limit(1);

		$Result = $this->fetchRow($Select);

		return $Result;
	}

	/**
	 * Слайдер по названию
	 *
	 * @param $name
	 *
	 * @return null|Zend_Db_Table_Row_Abstract
	 */
	public function getMenuByName($name) {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu');

		$this->setTable('menu');

		$Select = $this->select();
		$Select->from($this->_name, $Engine->getColumns());
		$Select->where('name = ?', (string)$name)->where('is_public = ?', '1');
		$Select->limit(1);

		$Result = $this->fetchRow($Select);

		return $Result;
	}

	/**
	 * Слайд по идентификатору
	 *
	 * @param $id
	 *
	 * @return null|Zend_Db_Table_Row_Abstract
	 */
	public function getItemById($id) {
		$Engine = Fenix_Engine::getInstance();
		$Engine->setSource('core/menu_item');

		$this->setTable('menu_item');

		$Select = $this->select();
		$Select->from($this->_name, $Engine->getColumns());
		$Select->where('id = ?', (int)$id);
		$Select->limit(1);

		$Result = $this->fetchRow($Select);

		return $Result;
	}
	
	public function deleteItemById($itemId){
		$currentSlide = $this->getItemById($itemId);
		
		if($currentSlide == null) {
			return false;
		}
		
		$Creator = Fenix::getCreatorUI();
		
		$Creator->loadPlugin('Form_Generator')
		        ->setSource('core/menu_item', 'default')
		        ->deleteRecord($currentSlide);
		
		return true;
	}
}