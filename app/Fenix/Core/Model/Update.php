<?php
class Fenix_Core_Model_Update extends Fenix_Resource_Model
{
    /**
     * Процесс обновления системы
     *
     * @param $id
     * @return bool
     */
    public function process($id)
    {
        $this->setTable('core_updates');
        $Select = $this->select()
                       ->from($this)
                       ->where('id = ?', (int) $id)
                       ->order('check_date desc');
        $Result = $this->fetchRow($Select);

        try {
            $_xml = file_get_contents($Result->definition);
            $_xml = new Zend_Config_Xml($_xml);

            // Создаем папку для резервных копий
            $backupDir = TMP_DIR_ABSOLUTE . 'updates/backup/';
            if (!is_dir($backupDir))
                mkdir($backupDir, 0777, true);

            // Создаем папку резервных копий обновления
            $updateBackupDir = $backupDir . $Result->sys_title . '/backup/';
            if (!is_dir($updateBackupDir))
                mkdir($updateBackupDir, 0777, true);

            // Список файлов обновления
            $files = $this->_getFiles($_xml);

            // Список файлов которые необходимо скопировать и забекапить
            foreach ($files['copy'] AS $_file) {
                $_pathinfo = pathinfo($_file);
                // Создаем директорию бекапинга
                if (file_exists(BASE_DIR . $_file)) {
                    if (!is_dir($updateBackupDir . 'copy/' . $_pathinfo['dirname'])) {
                        mkdir($updateBackupDir . 'copy/' . $_pathinfo['dirname'], 0777, true);
                    }
                    copy(BASE_DIR . $_file, $updateBackupDir . 'copy/' . $_pathinfo['dirname'] . '/' . $_pathinfo['basename']);
                }

                // Создаем необходимую директорию для копирования файла
                if (!is_dir(BASE_DIR . $_pathinfo['dirname'])) {
                    mkdir(BASE_DIR . $_pathinfo['dirname'], 0777, true);
                }

                // Создаем файл
                $_defPathinfo = pathinfo($Result->definition);
                $_fileUrl     = $_defPathinfo['dirname'] . '/files/' . $_file;

                $fp           = fopen($_fileUrl, "r");
                $_content     = null;
                while (!feof($fp)) {
                    $_content.= fgets($fp, 10240);
                }

                $_f = fopen(BASE_DIR . $_pathinfo['dirname'] . '/' . $_pathinfo['basename'], 'w');
                fwrite($_f, $_content);
                fclose($_f);

                // Чмодим созданную директорию
                chmod(BASE_DIR . $_pathinfo['dirname'] . '/', 0755);
            }

            // Удалем файлы
            foreach ($files['remove'] AS $_file) {
                $_pathinfo = pathinfo($_file);
                // Создаем директорию бекапинга
                if (file_exists(BASE_DIR . $_file)) {
                    if (!is_dir($updateBackupDir . 'remove/' . $_pathinfo['dirname'])) {
                        mkdir($updateBackupDir . 'remove/' . $_pathinfo['dirname'], 0777, true);
                    }
                    copy(BASE_DIR . $_file, $updateBackupDir . 'remove/' . $_pathinfo['dirname'] . '/' . $_pathinfo['basename']);

                    // Удаляем файл
                    unlink(BASE_DIR . $_file);
                }
            }

            // Применяем запросы
            $Db        = Zend_Registry::get('db');
            $DbConfig  = $Db->getConfig('prefix');

            $Smarty    = Fenix_Smarty::getInstance();
            $Smarty    ->assign(array(
                'prefix' => $DbConfig['prefix']
            ));

            foreach ($files['sql'] AS $_sql) {
                $_sql = $Smarty->fetchString($_sql);

                try {
                    $this->getAdapter()->query($_sql);
                }
                catch(Exception $e) {
                }
            }

            $this->update(array(
                'is_updated'   => '1',
                'execute_date' => new Zend_Db_Expr('NOW()')
            ), 'id = ' . (int) $Result->id);
        }
        catch (Exception $e) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Список обновлений повреждён: %s", $e->getMessage()))
                ->saveSession();

            return false;
        }
    }

    /**
     * Формируем списое файлов копирования, удаления и списка запросов к базе
     *
     * @param $_xml
     * @return array
     */
    protected function _getFiles($_xml)
    {
        $_copy = array();
        if (isset($_xml->scheme->files->copy->file->{0})) {
            $_copy = $_xml->scheme->files->copy->file;
        }
        else {
            $_copy[] = $_xml->scheme->files->copy->file;
        }

        $_remove = array();
        if (isset($_xml->scheme->files->remove->file->{0})) {
            $_remove = $_xml->scheme->files->remove->file;
        }
        else {
            $_remove[] = $_xml->scheme->files->remove->file;
        }

        $_sql = array();
        if (isset($_xml->scheme->db->sql->{0})) {
            $_sql = $_xml->scheme->db->sql;
        }
        else {
            $_sql[] = $_xml->scheme->db->sql;
        }

        return array(
            'copy'   => $_copy,
            'remove' => $_remove,
            'sql'    => $_sql
        );
    }

    /**
     * Напоминать или нет про обновление
     *
     * @param $id
     * @param $disabled
     */
    public function remind($id, $disabled)
    {
        $this->setTable('core_updates');
        $this->update(array(
            'is_disabled' => ($disabled == '1' ? '0' : '1')
        ), 'id = ' . (int) $id);

        return;
    }

    /**
     * Список всех обновлений
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getUpdates()
    {
        $this   ->setTable('core_updates');
        $Select = $this->select()
                       ->from($this)
                       ->order('check_date desc');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Есть ли новые обновления?
     *
     * @return bool
     */
    public function isHaveUpdates()
    {
        $this   ->setTable('core_updates');
        $Select = $this->select()
                       ->from($this)
                       ->where('is_disabled = ?', '0')
                       ->where('is_updated = ?', '0')
                       ->limit(1);
        $Result = $this->fetchRow($Select);

        return $Result != null;
    }

    /**
     * Проверка обновлений по тихому
     */
    public function checkUpdatesSilence()
    {
/*        try {
            $url  = Fenix::getStaticConfig()->update->url;
            $_xml = file_get_contents($url);
            $_xml = new Zend_Config_Xml($_xml);

            $_updates = array();
            if (isset($_xml->updates->update->{0})) {
                $_updates = $_xml->updates->update;
            }
            else {
            	if (isset($_xml->updates->update))
                	$_updates[] = $_xml->updates->update;
            }

            $_newCounter = 0;

            foreach ($_updates AS $_update) {
            	if ($_update == null) {
		    		continue;
		    	}

                $Result = $this->setTable('core_updates')->fetchRow(
                    $this->getAdapter()->quoteinto('sys_title = ?', $_update->code)
                );

                if ($Result == null) {
                    $_newCounter++;
                    $this->insert(array(
                        'sys_title'    => $_update->code,
                        'details'      => $_update->details,
                        'definition'   => $_update->definition,
                        'check_date'   => new Zend_Db_Expr('NOW()'),
                        'update_date'  => $_update->date,
                        'is_disabled'  => '0',
                        'is_updated'   => '0'
                    ));
                }
            }
        }
        catch (Exception $e) {
        }*/
    }

    /**
     * Проверка обновлений обычная
     */
    public function checkUpdates()
    {
       /* try {
            $url  = Fenix::getStaticConfig()->update->url;
            $_xml = file_get_contents($url);
            $_xml = new Zend_Config_Xml($_xml);

            $_updates = array();
            if (isset($_xml->updates->update->{0})) {
                $_updates = $_xml->updates->update;
            }
            else {
            	if (isset($_xml->updates->update))
                	$_updates[] = $_xml->updates->update;
            }

            $_newCounter = 0;

		    foreach ($_updates AS $_update) {
		    	if ($_update == null) {
		    		continue;
		    	}
		    	
                $Result = $this->setTable('core_updates')->fetchRow(
                    $this->getAdapter()->quoteinto('sys_title = ?', $_update->code)
                );

                if ($Result == null) {
                    $_newCounter++;
                    $this->insert(array(
                        'sys_title'    => $_update->code,
                        'details'      => $_update->details,
                        'definition'   => $_update->definition,
                        'check_date'   => new Zend_Db_Expr('NOW()'),
                        'update_date'  => $_update->date,
                        'is_disabled'  => '0',
                        'is_updated'   => '0'
                    ));

                    Fenix::getCreatorUI()
                        ->loadPlugin('Events_Session')
                        ->setType(Creator_Events::TYPE_WARNING)
                        ->setMessage(Fenix::lang("Найдены новые обновления. Рекомендуется обновить прямо сейчас."))
                        ->saveSession();
                }
            }
	        if ($_newCounter == 0) {
                Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_INFO)
                    ->setMessage(Fenix::lang("Новых обновлений нет"))
                    ->saveSession();
            }
        }
        catch (Exception $e) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Невозможно проверить обновления из-за проблемы с сервером. Ответ сервера: %s", $e->getMessage()))
                ->saveSession();
        }*/
    }
}