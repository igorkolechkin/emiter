<?php
class Fenix_Core_Model_Cache extends Fenix_Resource_Model
{
    /**
     * Очиска кеша
     *
     * @param $cacheName
     * @return bool
     */
    public function cleanCache($cacheName)
    {
        $cache     = new Fenix_Cache();
        $list      = $cache->getCacheList();
        
        $cacheName = Fenix::fromCamelCase($cacheName);
        $cacheInfo = $list->cache->{$cacheName};
        
        $path      = CACHE_DIR_ABSOLUTE;
        $cacheDir = $path . $cacheInfo->backendOptions->cache_dir;
        
        if (!is_dir($cacheDir))
            return false;

        $Dir = new Fenix_Dir();
        $Dir ->removeDirectory($cacheDir, true);
        return;
    }

    /**
     * Изменение статуса кеша
     *
     * @param $cache
     * @param $status
     */
    public function updateStatus($cache, $status)
    {
        $cache = Fenix::fromCamelCase($cache);

        $this->setTable('core_cache')
             ->update(array(
                'status' => $status
             ), $this->getAdapter()->quoteInto('name = ?', $cache));
        
        return;
    }

    /**
     * Обновление списка кеша
     *
     * @param $list
     */
    public function updateCacheTable($list)
    {
        $this->setTable('core_cache');
        
        foreach ($list->cache AS $cacheName => $cacheOptions) {
            $tableCache = $this->fetchRow($this->getAdapter()->quoteInto('name = ?', $cacheName));
            if ($tableCache == null) {
                $this->insert(array(
                    'name'   => $cacheName,
                    'status' => $cacheOptions->status
                ));
            }
        }
        
        $testArray = $list->cache->toArray();
        
        // Подчищаем удаленные записи кеша
        $tableCacheList = $this->fetchAll();
        foreach ($tableCacheList AS $tableCache) {
            if (!isset($testArray[$tableCache->name])) {
                $this->delete($this->getAdapter()->quoteInto('id = ?', $tableCache->id));
            }
        }
        
        return;
    }
}