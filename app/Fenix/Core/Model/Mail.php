<?php

class Fenix_Core_Model_Mail extends Fenix_Resource_Model
{
    /**
     * Шаблон письма по идентификатору
     *
     * @param $id
     *
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getMailById($id)
    {
        $this->setTable('core_mail');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('id = ?', (int) $id);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Шаблон письма по имени
     *
     * @param $name
     *
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getMail($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('checkout/mail');

        $this->setTable('core_mail');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('name = ?', $name);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список шаблонов писем
     *
     * @return Zend_Db_Select
     */
    public function getMailListAsSelect()
    {
        $this->setTable('core_mail');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select->from(array(
            'b' => $this->_name,
        ));

        return $Select;
    }

    /**
     * Отравка письма с использованием Smarty
     *
     * @param array $options
     *
     * @return bool
     */
    public function smartySendMail(array $options)
    {
        $Smarty = Fenix_Smarty::getInstance();

        if ( ! isset($options['from']) || empty($options['from'])) {
            $options['from'] = array(
                'info@' . $_SERVER['HTTP_HOST'],
                Fenix::getConfig('general/project/name/mail'),
            );
        }

        $assignData = $this->getDefaultAssignData();
        if (isset($options['assign']) && ! empty($options['assign'])) {
            $assignData = array_merge($assignData, $options['assign']);
        }

        if ( ! empty($assignData)) {
            $Smarty->assign($assignData);
        }

        if ( ! isset($options['template'])) {
            $subject = $Smarty->fetchString($options['subject']);
            $body = $Smarty->fetchString($options['body']);
        }
        else {
            $template = $this->getMail($options['template']);

            $subject = $Smarty->fetchString($template->title, true);
            $body = $Smarty->fetchString($template->content, true);
        }

        return $this->sendMail($options['to'], $options['from'], $subject, $body);
    }

    /**
     * Отправка письма
     *
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     *
     * @return bool
     */
    public function sendMail($to, $from, $subject, $body)
    {
        try {
            // Отправляем письмо с уведомлением
            $mail = new Zend_Mail('UTF-8');

            // Переключение на base64
            $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

            if (is_array($from)) {
                $mail->setFrom($from[0], $from[1]);
            }
            else {
                $mail->setFrom($from);
            }

            if (is_array($to)) {
                $mail->addTo($to[0], $to[1]);
            }
            else {
                $mail->addTo($to);
            }

            $mail->setSubject($subject);
            $mail->setBodyHtml($body);
            $mail->send();

            return true;
        }
        catch (Zend_Mail_Exception $e) {
            return false;
        }
    }

    /**
     * Рассылка писем на множественные адреса
     */
    public function sendMailByEmailsList(array $emailsList = array(), $options)
    {
        if (empty($emailsList)) {
            return false;
        }

        $result = true;

        foreach ($emailsList as $_email) {
            $options['to'] = trim($_email);

            if ( ! $this->smartySendMail($options)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Рассылка писем на адреса Админа
     */
    public function sendAdminMail($options)
    {
        //Список email'ов админов
        $emailsList = Fenix::getConfig('general/email');
        $emailsList = explode("\n", $emailsList);

        return $this->sendMailByEmailsList($emailsList, $options);
    }

    /**
     * Рассылка писем на адрес пользователя
     */
    public function sendUserMail($options)
    {
        if ( ! isset($options['to']) || empty($options['to'])) {
            $user = Fenix::getModel('session/auth')->getUser();

            if ($user == null) {
                return false;
            }
            else {
                $options['to'] = $user->email;
            }
        }

        return $this->smartySendMail($options);
    }

    /**
     *   Возвращает предопределенные переменные для шаблонов писем
     */
    public function getDefaultAssignData()
    {
        return array(
            'url'  => BASE_URL_CLEAN,
            'logo' => '<img src="' . Fenix::getModel('core/themes')->getLogo() . '" />',
        );
    }

    /**
     * Возвращает массив сообщений по умолчанию для форм.
     */
    public function getDefaultMessages()
    {
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Спасибо! Наш менеджер свяжется с Вами в ближайшее время."))
            ->saveSession();
        $success_msg = Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();

        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_ERROR)
            ->setMessage(Fenix::lang("Произошла ошибка. Письмо или часть писем не была отправлена!"))
            ->saveSession();
        $error_msg = Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();

        return array(
            'success_msg' => $success_msg,
            'error_msg'   => $error_msg,
        );
    }
}