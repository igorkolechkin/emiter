<?php
class Fenix_Core_Model_Utp extends Fenix_Resource_Model
{
    /**
     * Активный ли пункт меню слайдера в админке
     *
     * @return bool
     */
    static public function isUtpActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'utp';
    }

    /**
     * Список слайдеров
     *
     * @return Zend_Db_Select
     */
    public function getUtpListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/utp');

        $this   ->setTable('core_utp');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдов
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getBlockListAsSelect($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/utp_content');

        $this   ->setTable('core_utp_content');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->where('s.parent = ?', (int) $parent);
        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдеров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getBlockList($parent)
    {
        $Select = $this->getBlockListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }
    /**
     * Список  опубликованых блоков
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getPublicBlockList($parent)
    {
        $Select = $this->getBlockListAsSelect($parent);
        $Select->where('s.is_public = ?','1');
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Слайдер по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getUtpById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/utp');

        $this   ->setTable('core_utp');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайдер по названию
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getUtpByName($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/utp');

        $this->setTable('core_utp');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select->where('system_name = ?', (string) $name)
                ->where('is_public = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайд по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getBlockById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/utp_content');

        $this   ->setTable('core_utp_content');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}