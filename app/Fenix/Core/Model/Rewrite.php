<?php
class Fenix_Core_Model_Rewrite extends Fenix_Resource_Model
{
    /**
     * Правило перезапись URL по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getRewriteById($id)
    {
        $this   ->setTable('core_rewrite');
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    /**
     * Список правил
     *
     * @return Zend_Db_Select
     */
    public function getRewriteListAsSelect()
    {
        $this->setTable('core_rewrite');
        
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        
        $Select ->from(array(
            'b' => $this->_name
        ));
        
        return $Select;
    }
}