<?php
class Fenix_Core_Model_Filter extends Fenix_Resource_Model
{
    /**
     * Активный ли пункт меню слайдера в админке
     *
     * @return bool
     */
    static public function isFilterActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'filter';
    }

    /**
     * Список слайдеров
     *
     * @return Zend_Db_Select
     */
    public function getFilterListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter');

        $this   ->setTable('core_filter');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдов
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getSlideListAsSelect($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter_content');

        $this   ->setTable('core_filter_content');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->where('s.parent = ?', (int) $parent);
        $Select ->order('s.position asc');

        return $Select;
    }
   /**
     * Список слайдов c атрибута
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getSlideWithAttributesListAsSelect($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter_content');

        $this   ->setTable('core_filter_content');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));


        $Engine ->setSource('catalog/attributes');
        $attrCols = $Engine ->getColumns();

        $Select->join(array(
           'a'=>$this->getTable('catalog_attributes')
        ),'s.attribute_id = a.id',$attrCols);

        $Select ->where('s.parent = ?', (int) $parent);
        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдеров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSlideList($parent)
    {
        $Select = $this->getSlideListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список слайдеров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSlideWithAttributesList($parent)
    {
        $Select = $this->getSlideWithAttributesListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Слайдер по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getFilterById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter');

        $this   ->setTable('core_filter');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайдер по названию
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getFilterByName($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter');

        $this->setTable('core_filter');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('name = ?', (string) $name)
                ->where('is_public = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список слайдер
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getFilterList()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter');

        $this->setTable('core_filter');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('is_public = ?', '1');
        $Select ->order('position asc');


        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Слайд по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSlideById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/filter_content');

        $this   ->setTable('core_filter_content');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}