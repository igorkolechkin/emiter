<?php
class Fenix_Core_Model_Structure extends Fenix_Resource_Model
{
    /**
     * Проверка активного пункта меню
     * TODO Переделать на регулярки
     * @return bool
     */
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && (
                Fenix::getRequest()->getUrlSegment(1) == 'structure'
                || Fenix::getRequest()->getUrlSegment(1) == 'blocks'
                || Fenix::getRequest()->getUrlSegment(1) == 'menu'
                || Fenix::getRequest()->getUrlSegment(1) == 'slider'
                || Fenix::getRequest()->getUrlSegment(1) == 'social'
                || Fenix::getRequest()->getUrlSegment(1) == 'utp'
                || Fenix::getRequest()->getUrlSegment(1) == 'sitemap'
        ) || Fenix::getRequest()->getUrlSegment(0) == 'banners';
    }

    /**
     * Активный пункт меню "страницы"
     *
     * @return bool
     */
    static public function isStructureActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'structure';
    }

    /**
     * Страница по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getPageById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/structure');

        $this   ->setTable('structure');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список страниц в родительской категории
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getPagesListAsSelect($parent = 1)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/structure');

        $this   ->setTable('structure');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'c' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'c'
        )));

        $Select ->where('c.parent = ?', $parent);
        $Select ->order('c.position asc');

        return $Select;
    }

    /**
     * Список страниц по запросу
     *
     * @param str $query like 'parent=1'
     * @return Zend_Db_Select
     */
    public function getPagesListByQuery($query = 'parent=1')
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/structure');

        $this   ->setTable('structure');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select ->from(array(
            'c' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 'c'
        )));

        $Select ->where('c.'.$query)
            ->where('c.id>1');
        $Select ->order('c.position asc');

        return $this->fetchAll($Select);
    }

    /**
     * Список товаров
     *
     * @param $catalog
     * @return Zend_Paginator
     */
    public function getPagesList($parent = 1)
    {
        $Select    = $this->getPagesListAsSelect($parent);

        return $this->fetchAll($Select);
    }

    /**
     * Навигация для хлебных крошек
     *
     * @param Zend_Db_Table_Row $page
     * @return Zend_Db_Table_Rowset
     */
    public function getNavigation(Zend_Db_Table_Row $page)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/structure');

        $Select  = $this->setTable('structure')
                        ->select()
                        ->from($this, $Engine ->getColumns())
                        ->where('`left` <= ?', $page->left)
                        ->where('`right` >= ?', $page->right)
                        ->where('parent > 0')
                        ->order('left asc');
        $Result  = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Добавление новой страницы
     *
     * @param $Form
     * @param $req
     * @return mixed
     */
    public function addPage($Form, $req)
    {
        $id = $Form->addRecord($req);

        Fenix::getModel('core/common')->updateTreeIndex('structure');

        return $id;
    }

    /**
     * Редактирование страницы
     *
     * @param $Form Объект формы. Для работы с шаблоном XML
     * @param $current Редактируемая строка
     * @param $req Объект запроса
     * @return int
     */
    public function editPage($Form, $current, $req)
    {
        $id = $Form->editRecord($current, $req);

        Fenix::getModel('core/common')->updateTreeIndex('structure');

        return $id;
    }

    /**
     * Удаление страницы и всех дочерних страниц
     *
     * @param $current
     */
    public function deletePage($current)
    {
        $Creator = Fenix::getCreatorUI();

        $Select  = $this->setTable('structure')
                        ->select()
                        ->from($this)
                        ->where('`left` >= ?', $current->left)
                        ->where('`right` <= ?', $current->right);
        $Result  = $this->fetchAll($Select);

        foreach ($Result AS $_node) {
            $Creator ->loadPlugin('Form_Generator')
                     ->setSource('core/structure', $_node->attributeset)
                     ->deleteRecord($_node);
        }

        Fenix::getModel('core/common')->updateTreeIndex('structure');

        return;
    }
}