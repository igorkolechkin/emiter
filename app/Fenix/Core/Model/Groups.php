<?php
class Fenix_Core_Model_Groups extends Fenix_Resource_Model
{
    /**
     * Группа администратора по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getGroupById($id)
    {
        $this   ->setTable('admin_groups');
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    /**
     * Список групп
     *
     * @return Zend_Db_Select
     */
    public function getGroupsListAsSelect()
    {
        $this   ->setTable('admin_groups');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'g' => $this->_name
        ));
        $Select ->order('g.title asc');
        
        return $Select;
    }

    /**
     * Список групп
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getGroupsList()
    {
        $Select = $this->getGroupsListAsSelect();
        return $this->fetchAll($Select);
    }

    /**
     * Новая группа
     *
     * @param $req
     * @return mixed
     */
    public function addGroup($req)
    {
        $user = Fenix::getModel('session/auth')->getUser();
        
        $this->setTable('admin_groups');
        
        $data = array(
            'title'       => $req->getPost('title'),
            'name'        => $req->getPost('name'),
            'create_date' => new Zend_Db_Expr('NOW()'),
            'create_id'   => $user->id
        );
        
        $id = $this->insert($data);
        $this->updateRules($id, $req->getPost('rule'));
        
        return $id;
    }

    /**
     * Редактирование группы
     *
     * @param $current
     * @param $req
     * @return mixed
     */
    public function editGroup($current, $req)
    {
        $user = Fenix::getModel('session/auth')->getUser();
        
        $this->setTable('admin_groups');
        
        $data = array(
            'title'       => $req->getPost('title'),
            'name'        => $req->getPost('name'),
            'modify_date' => new Zend_Db_Expr('NOW()'),
            'modify_id'   => $user->id
        );
        
        $this->update($data, 'id = ' . (int) $current->id);
        $this->updateRules($current->id, $req->getPost('rule'));
        
        return $current->id;
    }

    /**
     * Удаление группы
     *
     * @param $current
     */
    public function deleteGroup($current)
    {
        $this->setTable('admin_groups');
        $this->delete('id = ' . (int) $current->id);

        $this->setTable('admin_groups_relations');
        $this->delete('group_id = ' . (int) $current->id);
    }

    /**
     * Обновление списка правил доступа группы
     *
     * @param $groupId
     * @param $rules
     */
    public function updateRules($groupId, $rules)
    {
        $this->setTable('admin_groups_rules');
        $this->delete('group_id = ' . (int) $groupId);
        foreach ((array) $rules AS $_rule) {
            $this->insert(array(
                'group_id' => $groupId,
                'rule'     => $_rule
            ));
        }
        
        return;
    }
}