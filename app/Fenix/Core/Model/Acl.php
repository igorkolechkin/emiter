<?php
class Fenix_Core_Model_Acl extends Fenix_Resource_Model
{
    /**
     * Список правил доступа пользователя
     *
     * @return mixed|Zend_Db_Table_Rowset_Abstract
     */
    public function getUserRules()
    {
        if (Zend_Registry::isRegistered('FenixEngineAdminRules')) {
            return Zend_Registry::get('FenixEngineAdminRules');
        }
        
        $user   = Fenix::getModel('session/auth')->getUser();
        
        $Select = $this->setTable('admin_groups_relations')
                       ->select()
                       ->setIntegrityCheck(false);
        
        $Select ->from(array(
            'gr' => $this->_name
        ), false);
        
        $Select ->join(array(
            'g' => $this->getTable('admin_groups')
        ), 'gr.group_id = g.id', false);
        
        $Select ->join(array(
            'r' => $this->getTable('admin_groups_rules')
        ), 'g.id = r.group_id', 'rule');
        
        $Select ->where('gr.admin_id = ?', (int) $user->id);
        
        $Result = $this->fetchAll($Select);
        
        Zend_Registry::set('FenixEngineAdminRules', $Result);
        
        return $Result;
    }
}