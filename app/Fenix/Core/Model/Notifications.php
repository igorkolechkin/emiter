<?php
class Fenix_Core_Model_Notifications extends Fenix_Resource_Model
{
    /**
     * Проверка на активный пункт меню
     *
     * @return bool
     */
    static public function isStructureActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'notifications';
    }

    /**
     * Статический бло по названию
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     * @throws Exception
     */
    public function getNotification($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/notifications');

        $Select = $this->setTable($Engine->getTable())
                       ->select();

        $Select ->from($this, $Engine->getColumns());
        $Select ->where('name = ?', $name);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        if ($Result == null) {
            throw new Exception('Блок ' . $name . ' не найден');
        }

        return $Result;
    }

    /**
     * Статический блок по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getNotificationById($id)
    {
        $this->setTable('notifications');
        
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);
        
        $Result = $this->fetchRow($Select);
        return $Result;
    }

    /**
     * Список блоков
     *
     * @return Zend_Db_Select
     */
    public function getNotificationsListAsSelect()
    {
        $this->setTable('notifications');
        
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        
        $Select ->from(array(
            'b' => $this->_name
        ));
        
        return $Select;
    }
}