<?php

	class Fenix_Core_Model_Sitemap extends Fenix_Resource_Model {
		const MAX_SITEMAP_SIZE   = 40000000; //максимальный вес файла карты сайта 50МБ, вбил 40МБ для предосторожности
		const MAX_SITEMAP_ROWS   = 8000; //максимальное кол-во ссылок в карте сайта 10000, вбил 8000 для предосторожности
		const SITEMAP_FILES_PATH = 'sitemaps';

		public function getHtml() {
			$xml = null;

			// Текстовые страницы
			$this->setTable('structure');
			$RootStructure = $this->fetchRow('id = ' . 1);

			$Select = $this->select()
			               ->from($this)
			               ->where('`left` > ?', $RootStructure->left)
			               ->where('`right` < ?', $RootStructure->right)
			               ->where('in_sitemap = ?', '1');
			$Result = $this->fetchAll($Select);

			$xml .= '<ul>';
			$xml .= "\t" . '<li>' . "\n";
			$xml .= "\t\t" . '<a href="' . str_replace('acp/', '', Fenix::getUrl()) . '">Главная</a>' . "\n";
			$xml .= "\t" . '</li>' . "\n";
			foreach($Result AS $_node) {
				if($_node->url_key != 'default') {
					$_navigation = Fenix::getModel('core/structure')->getNavigation($_node);

					$url = array();
					foreach($_navigation AS $_item) {
						$url[] = $_item->url_key;
					}

					$xml .= "\t" . '<li>' . "\n";
					$xml .= "\t\t" . '<a href="' . str_replace('acp/', '', Fenix::getUrl(implode('/', $url))) . '">' . $_node->title_russian . '</a>' . "\n";
					$xml .= "\t" . '</li>' . "\n";
				}
			}

			$xml .= "\t" . '<li><a href="' . str_replace('acp/', '', Fenix::getUrl('catalog/series')) . '">Серии</a><ul>' . "\n";
			$this->setTable('catalog');
			$RootStructure = $this->fetchRow('id = ' . 1);

			$Select = $this->select()
			               ->from($this)
			               ->where('`left` > ?', $RootStructure->left)
			               ->where('`right` < ?', $RootStructure->right)
			               ->where('in_sitemap = ?', '1');
			$Result = $this->fetchAll($Select);

			foreach($Result AS $_node) {
				$_navigation = Fenix::getModel('catalog/categories')->getCatalogNavigation($_node->id);

				$url = array();
				foreach($_navigation AS $_item) {
					$url[] = $_item->url_key;
				}

				$xml .= "\t" . '<li>' . "\n";
				$xml .= "\t\t" . '<a href="' . str_replace('acp/', '', Fenix::getUrl(implode('/', $url))) . '">' . $_node->title_russian . '</a>' . "\n";
				$xml .= "\t" . '</li>' . "\n";

				//Товары
				$this->setTable('catalog_products');
				$_products = $this->fetchAll('parent = ' . $_item->id);

				$xml .= '<ul>';
				foreach($_products AS $_product) {
					$xml .= "\t" . '<li>' . "\n";
					$xml .= "\t\t" . '<a href="' . str_replace('acp/', '', Fenix::getUrl($_product->url_key . '-' . $_product->sku)) . '">' . $_product->title_russian . '</a>' . "\n";
					$xml .= "\t" . '</li>' . "\n";
				}
				$xml .= '</ul>';
			}
			$xml .= "\t" . '</ul></li>' . "\n";

			$xml .= '</ul>';

			return $xml;
		}

		public function generateSitemap() {
			/** очищаем директорию от старых файлов */
			$this->clearSitemapDirectory();

			/** массив созданных файлов */
			$created_files = array();

			/** Формируем массивы по категориям разбивки карты сайта */
			$pages = array(); // ссылки на статические страницы, новости и т.п.
			$categories = array(); // ссылки на категории
			$categories_filters = array(); // ссылки на категрии с фильтрацией. только индексируемые атрибуты фильтра
			$products = array(); // массив продуктов

			/** заполняем будующий pages.xml */
			// Главная
			$pages[] = array(
				'loc'        => BASE_URL,
				'changefreq' => 'hourly',
				'priority'   => '1',
				'lastmod'    => false,
			);

			// Текстовые страницы
			$this->setTable('structure');
			$RootStructure = $this->fetchRow('id = ' . 1);

			$Select = $this->select()
			               ->from($this)
			               ->where('`left` > ?', $RootStructure->left)
			               ->where('`right` < ?', $RootStructure->right)
			               ->where('in_sitemap = ?', '1')
			               ->where('is_public = ?', '1');

			$Result = $this->fetchAll($Select);

			foreach($Result AS $_node) {
				// исключаем главную страницу - она добавляется выше
				if($_node->url_key != 'default' && $_node->url_key != '/') {
					$url = str_replace('acp/', '', Fenix::getUrl($_node->url_key));

					$pages[] = array(
						'loc'        => $url,
						'changefreq' => 'daily',
						'priority'   => '0.7',
						'lastmod'    => $this->getLastModDate($_node),
					);
				}
			}

			// запись данных в xml и получение списка созданных файлов
			if(!empty($pages)) {
				$this->margeCreatedFiles($created_files, $this->createXMLFiles($pages, 'pages'));
			}
			// освобождаем память
			unset($pages);
			/** pages.xml КОНЕЦ */


			/** заполняем будующий articles.xml */
			$articles = array();

			// articles
			$articles[] = array(
				'loc'        => str_replace('acp/', '', Fenix::getUrl('articles')),
				'changefreq' => 'hourly',
				'priority'   => '0.7',
				'lastmod'    => false,
			);

			// рубирки
			$this->setTable('articles_rubric');
			$Select = $this->select()
			               ->from($this)
			               ->where('in_sitemap = ?', '1')
			               ->where('is_public = ?', '1');

			$Result = $this->fetchAll($Select);
			if($Result != null) {
				foreach($Result AS $_node) {
					$url = str_replace('acp/', '', Fenix::getUrl('articles/' . $_node->url_key));

					$articles[] = array(
						'loc'        => $url,
						'changefreq' => 'daily',
						'priority'   => '0.7',
						'lastmod'    => $this->getLastModDate($_node),
					);
				}
			}

			// статьи
			$this->setTable('articles');
			$Select = $this->select()
			               ->from($this)
			               ->where('in_sitemap = ?', '1')
			               ->where('is_public = ?', '1');

			$Result = $this->fetchAll($Select);

			if($Result != null) {
				foreach($Result AS $_node) {
					$url = str_replace('acp/', '', Fenix::getUrl('articles/' . $_node->url_key));

					$articles[] = array(
						'loc'        => $url,
						'changefreq' => 'daily',
						'priority'   => '0.7',
						'lastmod'    => $this->getLastModDate($_node),
					);
				}
			}

			// запись данных в xml и получение списка созданных файлов
			if(!empty($articles)) {
				$this->margeCreatedFiles($created_files, $this->createXMLFiles($articles, 'articles'));
			}
			// освобождаем память
			unset($articles);
			/** articles.xml КОНЕЦ */


			/** заполняем будующий categories.xml */
			// список главных категорий, которые должны попасть в sitemap. Подкатегории подтянутся сами.
			// из этих-же категорий будет строиться sitemap для продуктов
			$current_categories = array(
				'1', // по умолчанию тянем все подкатегории кореневого каталога и все товары
			);

			foreach($current_categories as $category_id) {

				$this->setTable('catalog');
				$RootStructure = $this->fetchRow('id = ' . (int)$category_id);

				$Select = $this->select()
				               ->from($this)
				               ->where('`left` > ?', $RootStructure->left)
				               ->where('`right` < ?', $RootStructure->right)
				               ->where('in_sitemap = ?', '1')
				               ->where('is_public = ?', '1');
				$Result = $this->fetchAll($Select);

				if($Result != null) {
					foreach($Result AS $_node) {
						$url = str_replace('acp/', '', Fenix::getUrl($_node->url_key));

						$categories[] = array(
							'loc'        => $url,
							'changefreq' => 'daily',
							'priority'   => '0.8',
							'lastmod'    => $this->getLastModDate($_node),
						);


						/** заполняем будующий categories_filters.xml */

						/** ВНИМАНИЕ для работы данного функционала у Атрибутов (системных и пользовательских)
						 * должен быть параметр is_index означающий, что данный атрибут индексируется */

						$this->getCategoryFilters($_node, $url, $categories_filters);

						/** categories_filters.xml КОНЕЦ */
					}
				}

			}

			// запись данных в xml и получение списка созданных файлов
			if(!empty($categories)) {
				$this->margeCreatedFiles($created_files, $this->createXMLFiles($categories, 'categories'));
			}
			if(!empty($categories_filters)) {
				$this->margeCreatedFiles($created_files, $this->createXMLFiles($categories_filters, 'categories_filters'));
			}
			// освобождаем память
			unset($categories);
			unset($categories_filters);
			/** categories.xml КОНЕЦ */


			/** заполняем будующий products.xml */
			// список ID категорий, из которых будут тянуться товары, и разбивать xml-ку на
			// products_%category_name%_%file_number%.xml
			$products_categories = $current_categories;

			/** Формируем шаблончик для  $Select, чтоб не нагружать зря систему */
			$this->setTable('catalog');

			$Select = $this->select()
			               ->setIntegrityCheck(false);

			$Select->from(array(
				'c' => $this->_name,
			), false);

			$Select->join(array(
				'n' => $this->getTable('catalog'),
			), 'c.left <= n.left AND c.right >= n.right', false);

			$Select->join(array(
				'p' => $this->getTable('catalog_products'),
			), 'n.id = p.parent', array(
				'id',
				'url_key',
				'title_russian',
				'modify_date',
				'create_date',
			));

			foreach($products_categories as $category_id) {
				$products = array(); // для каждой категории новый массив продуктов

				$this->setTable('catalog');
				$CategoryStructure = $this->fetchRow('id = ' . (int)$category_id);
				$Select->reset(Zend_Db_Select::WHERE);

				$Select->where('c.id = ?', (int)$category_id)
				       ->where('c.is_public = ?', '1')
				       ->where('c.in_sitemap = ?', '1');

				$Select->where('p.is_public = ?', '1');
				$Select->where('p.in_sitemap = ?', '1');

				$Result = $this->fetchAll($Select);

				if($Result != null) {
					foreach($Result AS $_product) {
						$url = str_replace('acp/', '', Fenix::getUrl($_product->url_key));

						$products[] = array(
							'loc'        => $url,
							'changefreq' => 'daily',
							'priority'   => '0.7',
							'lastmod'    => $this->getLastModDate($_product),
						);
					}
				}

				// запись данных в xml и получение списка созданных файлов
				if(!empty($products)) {
					$products_file_name = 'products_' . $this->getSitemapCategoryName($CategoryStructure);
					$this->margeCreatedFiles($created_files, $this->createXMLFiles($products, $products_file_name));
				}

				// освобождаем память
				unset($products);
			}

			/** products.xml КОНЕЦ */


			/** Формируем sitemap.xml */
			if(!empty($created_files)) {
				/** Коды текущих языковых версий */
				foreach($created_files as $langCode => $_filesByLang) {
					$xml = '';
					if($langCode == 'default') {
						$siteMapName = 'sitemap.xml';
					} else {
						$siteMapName = "sitemap_{$langCode}.xml";
					}

					/** удаляем старый */
					if(file_exists(BASE_DIR . $siteMapName)) {
						unlink(BASE_DIR . $siteMapName);
					}

					/** Формируем XML */
					$xml .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
					$xml .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

					foreach($_filesByLang as $file) {
						$xml .= "\t" . '<sitemap>' . "\n";
						$xml .= "\t\t" . '<loc>' . BASE_URL . self::SITEMAP_FILES_PATH . DS . $file . '</loc>' . "\n";
						$xml .= "\t\t" . '<lastmod>' . $this->getLastModDate(time()) . '</lastmod>' . "\n";
						$xml .= "\t" . '</sitemap>' . "\n";
					}

					$xml .= '</sitemapindex>';


					/** Запись в файл */
					$_f = fopen(BASE_DIR . $siteMapName, 'w') or die('Sitemap generator error!!!');
					fputs($_f, $xml);
					fclose($_f);
				}
			}

			/** sitemap.xml КОНЕЦ */

			return true;
		}

		public function getLastModDate($_node = null) {
			if(is_object($_node)) {
				if(isset($_node->modify_date) && strtotime($_node->modify_date) > 0) {
					$lastmod_timestamp = strtotime($_node->modify_date);
				} elseif(isset($_node->create_date) && strtotime($_node->create_date) > 0) {
					$lastmod_timestamp = strtotime($_node->create_date);
				} else {
					return false;
				}
			} elseif(is_array($_node)) {
				if(isset($_node['modify_date']) && strtotime($_node['modify_date']) > 0) {
					$lastmod_timestamp = strtotime($_node['modify_date']);
				} elseif(isset($_node['create_date']) && strtotime($_node['create_date']) > 0) {
					$lastmod_timestamp = strtotime($_node['create_date']);
				} else {
					return false;
				}
			} elseif(is_integer($_node)) { // на случай если передать timestamp
				$lastmod_timestamp = $_node;
			} else {
				$lastmod_timestamp = time();
			}

			return date('Y-m-d\TH:i:sP', $lastmod_timestamp);
		}

		public function getCategoryFilters($category, $category_url, &$categories_filters) {
			$attributes = Fenix::getModel('catalog/filter')->getIndexesAttributesToCategory($category);

			if(count($attributes) > 0 && is_object($attributes)) {
				foreach($attributes as $attribute) {
					if((is_object($attribute) && $attribute->url == null) || (is_array($attribute) && empty($attribute['url']))) {
						continue;
					}

					$row = array(
						'changefreq' => 'daily',
						'priority'   => '0.7',
						'lastmod'    => false,
					);

					if(is_object($attribute)) {
						$row['loc'] = $category_url . '/filter/' . $attribute->sys_title . Fenix_Catalog_Model_Filter::FILTER_ATTR_SYS_TITLE_SEPARATOR . $attribute->url;
					}

					if(is_array($attribute)) {
						$row['loc'] = $category_url . '/filter/' . $attribute['sys_title'] . Fenix_Catalog_Model_Filter::FILTER_ATTR_SYS_TITLE_SEPARATOR . $attribute['url'];
					}

					$categories_filters[] = $row;
				}
			}
		}

		public function getSitemapCategoryName($category) {
			if($category->id == 1 || (empty($category->url_key) && empty($category->title_russian))) {
				return 'root';
			}

			if(empty($category->url_key)) {
				$url_key = Fenix::stringProtectUrl($category->title_russian);
			} else {
				$url_key = Fenix::stringProtectUrl($category->url_key); //просто я параноик))))
			}

			$url_key = str_replace(array(
				'*', '@', '`', '~', '!', '#', '$', '%', '^', '&',
				'(', ')', '=', '+', "'", '"', '/', '\\', '.', ',',
				'<', '>', '|', '№', ';', ':', '?', ' ', "\n", "\r",
			), '', $url_key);

			$url_key = str_replace('-', '_', $url_key);

			if(strlen($url_key) > 0) {
				return $url_key;
			}

			return 'root';
		}

		public function createXMLFiles($data, $xml_name) {
			if(empty($data)) {
				return false;
			}

			/** массив созданных файлов */
			$created_files = array();

			/** Коды текущих языковых версий */
			$languages = $this->getLanguagesArray();

			foreach($languages as $_language) {
				$languagePrefix = '';
				$languageCode = '';

				if($_language != 'default') {
					$languagePrefix = '_' . $_language;
					$languageCode = $_language;
				}

				$file_index = 1;
				$xml = '';

				$file_size = 0;

				foreach($data as $i => $_row) {
					if(!is_array($_row) || empty($_row) || !isset($_row['loc']) || empty($_row['loc'])) {
						continue;
					}

					if(!empty($languageCode)) {
						$clearLink = BASE_URL . $languageCode . '/' . str_replace(BASE_URL, '', $_row['loc']);
						$_row['loc'] = rtrim($clearLink, '/');
					}

					$xml_url_section = '';
					$xml_url_section .= "\t" . '<url>' . "\n";
					$xml_url_section .= "\t\t" . '<loc>' . $_row['loc'] . '</loc>' . "\n";
					$xml_url_section .= ($_row['lastmod'] != false) ? "\t\t" . '<lastmod>' . $_row['lastmod'] . '</lastmod>' . "\n" : "";
					$xml_url_section .= "\t\t" . '<changefreq>' . $_row['changefreq'] . '</changefreq>' . "\n";
					$xml_url_section .= "\t\t" . '<priority>' . $_row['priority'] . '</priority>' . "\n";
					$xml_url_section .= "\t" . '</url>' . "\n";

					$file_size += strlen($xml_url_section);

					if($i > self::MAX_SITEMAP_ROWS || $file_size > self::MAX_SITEMAP_SIZE) {
						$created_files[ $_language ] = $this->createXMLFile($xml, $xml_name . $languagePrefix . '_' . $file_index . '.xml');

						//освобождаем память
						unset($xml);

						$file_size = 0;
						$xml = ''; // создаем новую
					}

					$xml .= $xml_url_section;
				}

				$created_files[ $_language ][] = $this->createXMLFile($xml, $xml_name . $languagePrefix . '_' . $file_index . '.xml');
			}

			return $created_files;
		}

		function createXMLFile($xml, $file_name) {
			$_f = fopen(BASE_DIR . self::SITEMAP_FILES_PATH . DS . $file_name, 'w') or die('Sitemap generator error!!!');

			fputs($_f, '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n" . $xml . '</urlset>');

			fclose($_f);

			return $file_name;
		}


		/** Удаление файлов из папки
		 *  принимает значение $file_pattern - строка, возможные значения:
		 *  null - будут удалены все файлы в категории
		 *  'file.xml' - полное имя файла - будет удален только этот файл
		 *  '/^categories_/' - шаблон имени файла - будут удалены файлы, совпадающие с шаблоном.
		 */
		function clearSitemapDirectory($file_pattern = null) {

			if($file_pattern != null & preg_match('/\.(xml)$/', $file_pattern)) {
				return unlink(BASE_DIR . self::SITEMAP_FILES_PATH . DS . $file_pattern);
			}

			$files = glob(BASE_DIR . self::SITEMAP_FILES_PATH . DS . "*");

			if(count($files) > 0) {
				foreach($files as $file) {
					if($file_pattern != null && preg_match($file_pattern, basename($file))) {
						unlink($file);
					} elseif($file_pattern == null && file_exists($file)) {
						unlink($file);
					}
				}
			}

			return true;
		}

		private function getLanguagesArray() {
			$result = array(
				'default',
			);
			$languages = Fenix_Language::getInstance()->getLanguagesList();
			if($languages != null && $languages->count() > 1) {
				foreach($languages as $_language) {
					if($_language->default_site == '1') {
						continue;
					}
					$result[] = $_language->code;
				}
			}

			return $result;
		}

		private function margeCreatedFiles(&$margeArr, $dataArr) {
			if(!empty($margeArr)) {
				foreach($margeArr as $langCode => &$_data) {
					if(!isset($dataArr[ $langCode ])) {
						continue;
					}
					$_data = array_merge($_data, $dataArr[ $langCode ]);
				}
			} else {
				$margeArr = $dataArr;
			}

			return true;
		}
	}