<?php
class Fenix_Core_Model_Administrators extends Fenix_Resource_Model
{
    /**
     * Администратор по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAdministratorById($id)
    {
        $this->setTable('admin_users');
        
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    /**
     * Список администраторов
     *
     * @return Zend_Db_Select
     */
    public function getAdministratorsListAsSelect()
    {
        $this->setTable('admin_users');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ));
        return $Select;
    }

    /**
     * Список администраторов
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAdministratorsList()
    {
        $Select = $this->getAdministratorsListAsSelect();
        return $this->fetchAll($Select);
    }

    /**
     * Добавление администраторов
     *
     * @param $req
     * @return mixed
     */
    public function addAdministrator($req)
    {
        $user = Fenix::getModel('session/auth')->getUser();
        
        $this ->setTable('admin_users');
        $data = array(
            'login'       => $req->getPost('login'),
            'password'    => sha1($req->getPost('password')),
            'create_date' => new Zend_Db_Expr('NOW()'),
            'create_id'   => $user->id,
            'fullname'    => (string) $req->getPost('fullname'),
            'job'         => (string) $req->getPost('job'),
            'image'       => (file_exists($req->getFiles('image')->tmp_name) ? file_get_contents($req->getFiles('image')->tmp_name) : ''),
            'image_info'  => (file_exists($req->getFiles('image')->tmp_name) ? serialize(array('file' => (object) $req->getFiles('image'), 'imagesize' => (object) getimagesize($req->getFiles('image')->tmp_name))) : '')
        );
        
        $adminId = $this->insert($data);

        // Обновление групп
        $this->updateGroups($adminId, $req->getPost('group'));
        
        return $adminId;
    }

    /**
     * Редактирование администратора
     *
     * @param $current
     * @param $req
     * @return mixed
     */
    public function editAdministrator($current, $req)
    {
        $user = Fenix::getModel('session/auth')->getUser();
        
        $this ->setTable('admin_users');
        $pass = $current->password;
        
        if ($req->getPost('password') != null) {
            $pass = sha1($req->getPost('password'));
        }
        
        $image = $current->image;
        
        if ($req->getPost('image_delete') == '1') {
            $image = '';
            $current->image_info = '';
        }
        
        $data = array(
            'login'       => $req->getPost('login'),
            'password'    => $pass,
            'modify_date' => new Zend_Db_Expr('NOW()'),
            'modify_id'   => $user->id,
            'fullname'    => (string) $req->getPost('fullname'),
            'job'         => (string) $req->getPost('job'),
            'image'       => (file_exists($req->getFiles('image')->tmp_name) ? file_get_contents($req->getFiles('image')->tmp_name) : $image),
            'image_info'  => (file_exists($req->getFiles('image')->tmp_name) ? serialize(array('file' => (object) $req->getFiles('image'), 'imagesize' => (object) getimagesize($req->getFiles('image')->tmp_name))) : $current->image_info)
        );
        
        $this->update($data, 'id = ' . (int) $current->id);
        
        $this->updateGroups($current->id, $req->getPost('group'));
        return $current->id;
    }

    /**
     * Обновление списка групп администратора
     *
     * @param $userId
     * @param $groups
     */
    public function updateGroups($userId, $groups)
    {
        $this->setTable('admin_groups_relations');
        $this->delete('admin_id = ' . (int) $userId);
        
        foreach ((array) $groups AS $group) {
            $this->insert(array(
                'admin_id' => $userId,
                'group_id' => $group
            ));
        }
        
        return;
    }

    /**
     * Удаление администратора
     *
     * @param $current
     */
    public function deleteAdministrator($current)
    {
        $this->setTable('admin_users');
        $this->delete('id = ' . (int) $current->id);

        $this->setTable('admin_groups_relations');
        $this->delete('admin_id = ' . (int) $current->id);
    }
}