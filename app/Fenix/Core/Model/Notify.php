<?php
class Fenix_Core_Model_Notify extends Fenix_Resource_Model
{
    /**
     * Подсчет количества непрочитанных уведомлений
     *
     * @return int
     */
    public function countUnreadNotify()
    {
        $user   = Fenix::getModel('session/auth')->getUser();

        $Select = $this->setTable('core_notify')
                       ->select();

        $Select ->from(array(
            'e' => $this->_name
        ), array(
            new Zend_Db_Expr('COUNT(*) AS _count')
        ));

        $Select ->where('e.recipient_id = ? OR e.recipient_id = 0', $user->id)
                ->where('e.unread = ?', '1');

        $Result = $this->fetchRow($Select);

        return (int) $Result->_count;
    }

    /**
     * Список непрочитанных уведомлений
     *
     * @param int $limit
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getUnreadNotify($limit = 4)
    {
        $user   = Fenix::getModel('session/auth')->getUser();
        $Select = $this->setTable('core_notify')
                       ->select();

        $Select ->from(array(
            'e' => $this->_name
        ), array(
            '*'
        ));

        $Select ->where('e.recipient_id = ? OR e.recipient_id = 0', $user->id)
                ->where('e.unread = ?', '1');
        $Select ->order('e.event_date desc');
        $Select ->limit((int) $limit);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Список уведомлений
     *
     * @param $user
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getNotifyList($user)
	{
		$Select = $this->setTable('core_notify')
					   ->select();
		
		$Select ->from(array(
			'e' => $this->_name
		), array(
			'*',
			new Zend_Db_Expr('DATE_FORMAT(e.event_date, \'%Y-%m-%d\') AS date')
		));
		
		$Select ->where('e.recipient_id = ? OR e.recipient_id = 0', $user->id);
		$Select ->order('e.event_date desc');
		
		$Result = $this->fetchAll($Select);
		
		return $Result;
	}

    /**
     * Подготовка списка уведомлений в ленте
     *
     * @return array
     */
    public function prepareForCreator()
	{
		$user       = Fenix::getModel('session/auth')->getUser();
		$notifyList = $this->getNotifyList($user);
		$_data      = array();
		
		foreach ($notifyList AS $_notify) {

            // Пометка уведомления как прочитанного
            if ($_notify->unread == '1') {
                $this->setTable('core_notify')->update(array(
                    'unread' => '0'
                ), 'id = ' . (int) $_notify->id);
            }

			$_tmp = array();
			
			$_tmp['date'] = $_notify->event_date;
			$_tmp = array_merge($_tmp, (array) unserialize($_notify->options));
						
			if ($_notify->icon != null) {
				$_tmp['icon'] = $_notify->icon;
			}			
						
			if ($_notify->title != null) {
				$_tmp['content']['title'] = $_notify->title;
			}			
						
			if ($_notify->content != null) {
				$_tmp['content']['content'] = $_notify->content;
			}			
						
			if ($_notify->more_url != null) {
				$_tmp['content']['more'] = $_notify->more_url;
			}		
			
			$_data[$_notify->date][] = $_tmp;
		}
		
		return $_data;
	}

    /**
     * Добавление уведомления
     *
     * @param array $options
     */
    public function addNotify(array $options)
	{
		$_recipients = $options['recipients'];
		unset($options['recipients']);
		
		$_data = array();
		if (is_array($_recipients)) {
			foreach ($_recipients AS $_recipient) {
				$_data[] = array(
					'unread' 		=> '1',
					'event_date' 	=> new Zend_Db_Expr('NOW()'),
					'sender_id'  	=> (isset($options['sender']) ? (int) $options['sender'] : 0),
					'recipient_id'  => $_recipient,
					'icon'			=> (isset($options['icon']) ? $options['icon'] : ''),
					'title'			=> (isset($options['title']) ? $options['title'] : ''),
					'content'		=> (isset($options['content']) ? $options['content'] : ''),
					'more_url'		=> (isset($options['more_url']) ? $options['more_url'] : ''),
					'options'		=> (isset($options['options']) ? serialize($options['options']) : '')
				);
			}
		}
		else {
			$_data[] = array(
				'unread' 		=> '1',
				'event_date' 	=> new Zend_Db_Expr('NOW()'),
				'sender_id'  	=> (isset($options['sender']) ? (int) $options['sender'] : 0),
				'recipient_id'  => (int) $_recipients,
				'icon'			=> (isset($options['icon']) ? $options['icon'] : ''),
				'title'			=> (isset($options['title']) ? $options['title'] : ''),
				'content'		=> (isset($options['content']) ? $options['content'] : ''),
				'more_url'		=> (isset($options['more_url']) ? $options['more_url'] : ''),
				'options'		=> (isset($options['options']) ? serialize($options['options']) : '')
			);
		}
		
		$this->setTable('core_notify');
		
		foreach ($_data AS $_data) {
			$this->insert($_data);
		}
	}
}