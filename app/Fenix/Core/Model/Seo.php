<?php

class Fenix_Core_Model_Seo extends Fenix_Resource_Model
{
    /**
     * SEO шаблон по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSeoById($id)
    {
        $this->setTable('core_seo');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('id = ?', (int)$id);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список SEO шаблонов
     *
     * @return Zend_Db_Select
     */
    public function getSeoListAsSelect()
    {
        $this->setTable('core_seo');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            's' => $this->_name
        ));

        return $Select;
    }

    /**
     * Список SEO шаблонов
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSeoList()
    {
        $this->setTable('core_seo');
        $Select = $this->getSeoListAsSelect();
        $Select->where('s.is_public = ?', '1');

        return $this->fetchAll($Select);
    }

    /**
     * SEO шаблон по имени
     *
     * @param $name
     * @return mixed|null|Zend_Db_Table_Row|Zend_Db_Table_Row_Abstract
     */
    public function getSeoByName($name)
    {
        if (Zend_Registry::isRegistered('FenixEngine_Seo_' . $name)) {
            return Zend_Registry::get('FenixEngine_Seo_' . $name);
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/seo');

        $this->setTable('core_seo');
        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('name = ?', $name);
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        if ($Result == null) {
            $Result = new Zend_Db_Table_Row(array(
                'data' => array(
                    'seo_title' => '',
                    'seo_h1' => '',
                    'seo_keywords' => '',
                    'seo_description' => ''
                )
            ));
        }

        Zend_Registry::set('FenixEngine_Seo_' . $name, $Result);

        return $Result;
    }

    /**
     * Парсинг строки SEO шаблона.
     * Для подстановки переменных
     *
     * @param $string
     * @param array $data
     * @return mixed
     */
    public function parseString($string, array $data)
    {
        foreach ($data AS $_key => $_value) {
            if (!is_string($_value)) continue;
            $string = str_replace('%' . $_key . '%', $_value, $string);
        }
        //удаляем теги %tag% если после замены чтото осталось не замененным
        $string = preg_replace('/%(.*)%/U', '', $string);
        //удаляем двоные пробелы
        $string = preg_replace('/\s{2,}/', ' ', $string);
        //удаляем пробелы по краям
        $string = trim($string);

        return $string;
    }

    /**
     *
     * Получение СЕО шаблона по ID категории
     *
     * @param $category_id
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSeoByCategoryId($category_id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/seo');

        $this->setTable('core_seo_categories_relations');

        $Select = $this->select()->from(array('sca' => $this->_name), null)->setIntegrityCheck(false);

        $Select->join(array('s' => $this->getTable('core_seo')),
            'sca.template_id = s.id', $Engine->getColumns());

        $Select->where('sca.category_id = ?', (int)$category_id);
        $Select->limit(1);

        return $this->fetchRow($Select);
    }

    /**
     *
     * Получение списка категорий выбранных в сео шаблоне
     *
     * @param $template_id
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getTemplateCategoriesById($template_id)
    {
        $this->setTable('core_seo_categories_relations');

        $Select = $this->select();
        $Select->where('template_id = ?', $template_id);

        return $this->fetchAll($Select);
    }

    public function removeCategoriesRelation($template_id)
    {
        $this->setTable('core_seo_categories_relations')
            ->delete($this->getAdapter()->quoteInto('template_id = ?', (int) $template_id));
    }

    public function saveCategoriesRelations($template_id)
    {
        //удаляем старые связи
        $this->removeCategoriesRelation($template_id);

        $this->setTable('core_seo_categories_relations');

        $categories = Fenix::getRequest()->getPost('categories');
        //если категории были выбраны то сохраняем их
        if($categories != null){
            foreach ($categories as $category_id) {
                //удаляем все записи с текущей категорией если она есть в другом шаблоне
                $this->delete($this->getAdapter()->quoteInto('category_id = ?', (int) $category_id));
                $this->insert(array('template_id' => $template_id, 'category_id' => $category_id));
            }
        }
    }
}