<?php
class Fenix_Core_Model_Rules extends Fenix_Resource_Model
{
    /**
     * Список правил доступа к админ панели
     *
     * @return array
     */
    public function getRulesList()
    {
        $xml   = Fenix::assembleXml('acl.xml');
        $Rules = array();
        
        foreach ($xml['acl'] AS $_ruleName => $_ruleDetails) {
            $Rules[$_ruleName] = $_ruleDetails;
        }
        
        ksort($Rules);
        
        return $Rules;
    }
}