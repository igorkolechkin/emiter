<?php
class Fenix_Core_Model_Optiimg extends Fenix_Resource_Model
{

    public function getDoneImagesList()
    {
        $this->setTable('images_optimisation');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ));
        $Select ->where('a.create_date = ?',date('Y-m-00'));
        return $this->fetchAll($Select);
    }

    public function checkDoneImage($image,$image_path)
    {
        $this->setTable('images_optimisation');
        $Select = $this->select()
            ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ));
        $Select ->where('a.create_date = ?',date('Y-m-00'))
                ->where('a.image = ?',$image)
                ->where('a.image_path = ?',$image_path);
        return $this->fetchAll($Select);
    }


    public function getFiles($dir = null,$url = null){
        $i = 0;
        $files = array();
        if($dir == null){
            $dir = TMP_DIR_ABSOLUTE.'cache/images/';
        }
        if($url == null){
            $url = TMP_DIR_URL.'cache/images/';
        }
        //Fenix::dump($dir);
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if(strripos($file, '.jpg') || strripos($file, '.JPG') || strripos($file, '.jpeg') || strripos($file, '.JPEG')){
                        $check = $this->checkDoneImage($file,$dir);
                        if($check->count()==0){
                            $files[$i]['path'] = $dir;
                            $files[$i]['url'] = $url;
                            $files[$i]['name'] = $file;
                            $files[$i]['type'] = 'jpg';
                            $i++;
                        }
                    }
                    elseif(strripos($file, '.png') || strripos($file, '.PNG')){
                        $check = $this->checkDoneImage($file,$dir);
                        if($check->count()==0){
                            $files[$i]['path'] = $dir;
                            $files[$i]['url'] = $url;
                            $files[$i]['name'] = $file;
                            $files[$i]['type'] = 'png';
                            $i++;
                        }
                    }
                    /*elseif(!is_dir($dir.$file)){
                    }*/
                    else{
                        ///Fenix::dump();
                        if ($handle2 = opendir($dir.$file)) {
                            while (false !== ($file2 = readdir($handle2))) {
                                if ($file2 != "." && $file2 != "..") {
                                    if(strripos($file2, '.jpg') || strripos($file2, '.JPG') || strripos($file2, '.jpeg') || strripos($file2, '.JPEG')){
                                        $check = $this->checkDoneImage($file2,$dir . $file . '/');
                                        if($check->count()==0){
                                            $files[$i]['path'] = $dir . $file . '/';
                                            $files[$i]['url'] = $url . $file . '/';
                                            $files[$i]['name'] = $file2;
                                            $files[$i]['type'] = 'jpg';
                                            $i++;
                                        }

                                    } elseif(strripos($file2, '.png') || strripos($file2, '.PNG')){
                                        $check = $this->checkDoneImage($file2,$dir . $file . '/');
                                        if($check->count()==0) {
                                            $files[$i]['path'] = $dir . $file . '/';
                                            $files[$i]['url'] = $url . $file . '/';
                                            $files[$i]['name'] = $file2;
                                            $files[$i]['type'] = 'png';
                                            $i++;
                                        }
                                    }
                                    /*elseif(!is_dir($dir.$file2)){
                                    }*/
                                    else {
                                        if ($handle3 = opendir($dir . $file . '/' . $file2)) {
                                            while (false !== ($file3 = readdir($handle3))) {
                                                if ($file3 != "." && $file3 != "..") {
                                                    if(strripos($file3, '.jpg') || strripos($file3, '.JPG')  || strripos($file3, '.jpeg') || strripos($file3, '.JPEG')){
                                                        $check = $this->checkDoneImage($file3,$dir . $file . '/' . $file2 . '/');
                                                        if($check->count()==0) {
                                                            $files[$i]['path'] = $dir . $file . '/' . $file2 . '/';
                                                            $files[$i]['url'] = $url . $file . '/' . $file2 . '/';
                                                            $files[$i]['name'] = $file3;
                                                            $files[$i]['type'] = 'jpg';
                                                            $i++;
                                                        }
                                                    }
                                                    elseif(strripos($file3, '.png') || strripos($file3, '.PNG')){
                                                        $check = $this->checkDoneImage($file3,$dir . $file . '/' . $file2 . '/');
                                                        if($check->count()==0) {
                                                            $files[$i]['path'] = $dir . $file . '/' . $file2 . '/';
                                                            $files[$i]['url'] = $url . $file . '/' . $file2 . '/';
                                                            $files[$i]['name'] = $file3;
                                                            $files[$i]['type'] = 'png';
                                                            $i++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
        return $files;
    }

   /* public function optiJPG($file){
        $stat = stat($file['path'].$file['name']);
        $old_size = $stat['size'];
        $url = $file['url'].$file['name'];
        //Fenix::dump($file);
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'http://fixup.com.ua/Myapi/myapi.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "url=".$url);
            $out = curl_exec($curl);
            curl_close($curl);
            $ff = 'http://fixup.com.ua/Myapi/images/'.$file['name'];
            $newfile = $file['path'].$file['name'];
            if(copy($ff, $newfile)){
                $this ->setTable('images_optimisation');
                $data = array(
                    'create_date' => date('Y-m-00'),
                    'image'       => $file['name'],
                    'image_path'  => $file['path']
                );
                $this->insert($data);
            }
            else{
            }
        }

        $new_stat = stat($file['path'].$file['name']);
        $new_size = $new_stat['size'];
        $size = $old_size-$new_size;
        return $size;
    }*/

    public function optiPNG($file){
        $this ->setTable('images_optimisation');
        $data = array(
            'create_date' => date('Y-m-00'),
            'image'       => $file['name'],
            'image_path'  => $file['path']
        );
        $this->insert($data);
    }
}