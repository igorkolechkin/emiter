<?php
class Fenix_Core_Model_Social extends Fenix_Resource_Model
{
    /**
     * Блок соцсети по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSocialById($id)
    {
        $this   ->setTable('core_social');
        
        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);
        
        $Result = $this->fetchRow($Select);
        
        return $Result;
    }

    /**
     * Список соц. сетей
     *
     * @return Zend_Db_Select
     */
    public function getSocialListAsSelect()
    {
        $this   ->setTable('core_social');
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/social');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        
        $Select ->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array('prefix' => 's')));
        
        return $Select;
    }

    /**
     * Список соц. сетей
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSocialList()
    {
        $Select = $this->getSocialListAsSelect();
        $Select ->where('s.is_public = ?', '1');
        $Select ->order('s.position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }
}