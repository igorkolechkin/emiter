<?php
class Fenix_Core_Model_Countdown extends Fenix_Resource_Model
{
    /**
     * Активный ли пункт меню слайдера в админке
     *
     * @return bool
     */
    static public function isCountdownActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'countdown';
    }

    /**
     * Список слайдеров
     *
     * @return Zend_Db_Select
     */
    public function getCountdownListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/countdown');

        $this   ->setTable('core_countdown');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдов
     *
     * @param int $parent
     * @return Zend_Db_Select
     */
    public function getSlideListAsSelect($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/countdown_content');

        $this   ->setTable('core_countdown_content');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            's' => $this->_name
        ), $Engine ->getColumns(array(
            'prefix' => 's'
        )));

        $Select ->where('s.parent = ?', (int) $parent);
        $Select ->order('s.position asc');

        return $Select;
    }

    /**
     * Список слайдеров
     *
     * @param $parent
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getSlideList($parent)
    {
        $Select = $this->getSlideListAsSelect($parent);
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * Слайдер по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCountdownById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/countdown');

        $this   ->setTable('core_countdown');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайдер по названию
     *
     * @param $name
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getCountdownByName($name)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/countdown');

        $this->setTable('core_countdown');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('name = ?', (string) $name)
                ->where('is_public = ?', '1');
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Слайд по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getSlideById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('core/countdown_content');

        $this   ->setTable('core_countdown_content');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}