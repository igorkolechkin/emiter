<?php

class Fenix_Core_Collection_Menu extends Fenix_Resource_Collection {
	public function getMenu($name) {
		$Menu = Fenix::getModel('core/menu')->getMenuByName($name);

		if($Menu == null) {
			return new Fenix_Object_Rowset(array(
				'data' => array(),
			));
		}

		$_slider = $Menu->toArray();
		$_slider['item_list'] = $this->_getItemsList($Menu);

		return new Fenix_Object(array(
			'data' => $_slider,
		));
	}

	private function _getItemsList($Menu) {
		$menuList = Fenix::getModel('core/menu')->getItemsList($Menu->id);

		$Result = array();
		foreach($menuList AS $_menu) {
			//$_url               = substr($_menu->url_key, 0, 1) != '/' ? '/' . $_menu->url_key : $_menu->url_key;


			if(!empty($_menu->category_id)) {
				$category = Fenix::getModel('catalog/categories')->getCategoryById($_menu->category_id);
				$_tmp['category'] = $category;
				$_url = ($category != null) ? Fenix::getUrl($category->url_key) : '';
			} elseif($_menu->structure_id) {
				$structure = Fenix::getModel('core/structure')->getPageById($_menu->structure_id);
				$_tmp['structure'] = $structure;
				$_url = ($structure != null) ? Fenix::getUrl($structure->url_key) : '';
			} else {
				$_url = Fenix::getUrl($_menu->url_key);
			}

			$_tmp = $_menu->toArray();
			$_tmp['image'] = $_menu->image ? HOME_DIR_URL . $_menu->image : null;
			$_tmp['url'] = $_url;
			//$_tmp['active']     = Fenix::getModel('core/menu')->isCurrent($_menu);

			$Result[] = $_tmp;
		}

		return new Fenix_Object_Rowset(array(
			'data' => $Result,
		));
	}
}