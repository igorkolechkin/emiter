<?php

class Fenix_Core_Collection_Slider extends Fenix_Resource_Collection
{
    public function getSlider($name)
    {
        $Slider = Fenix::getModel('core/slider')->getSliderByName($name);

        if ($Slider == null) {
            return null;
        }

        $_slider = $Slider->toArray();
        $_slider['slide_list'] = $this->getSlideList($Slider);

        return new Fenix_Object(array(
            'data' => $_slider
        ));
    }

    private function getSlideList($Slider)
    {
        $slideList = Fenix::getModel('core/slider')->getPublicSlideList($Slider->id);

        if (Fenix_Language::getInstance()->getLanguagesList()->count() > 1) {
            //если больше 1-го языка сайта для разных языковых версий указываем название поля в таблице БД
            $field = 'image_' . Fenix_Language::getInstance()->getCurrentLanguage()->name;
        } else {
            $field = 'image';
        }

        $Result = array();
        foreach ($slideList AS $_slide) {

            $_tmp = $_slide->toArray();

            $_tmp['image'] = trim($_tmp[$field]);

            if ($_tmp['image'] == '') {
                continue;
            }

            if ( ! file_exists(HOME_DIR_ABSOLUTE . $_tmp[$field])) {
                continue;
            }

            $_tmp['image'] = HOME_DIR_URL . $_tmp[$field];

            $Result[] = $_tmp;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }
}