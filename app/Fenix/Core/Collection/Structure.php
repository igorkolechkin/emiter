<?php

class Fenix_Core_Collection_Structure extends Fenix_Resource_Collection
{
    use Fenix_Traits_ImageInfoCollection;
    use Fenix_Traits_SeoCollection;

    public function setUrl($url = null)
    {
        $this->_setSource('core/structure');
        $req = Fenix::getRequest();

        if ($url == null) {
            $pathInfo = $req->getParsedInfo();
        } else {
            $pathInfo = $req->preparePathinfo($url);
        }

        /*
         * Далее несколько алгоритмов работы
         * 1. Если только один сегмент урла - Извлекаем родительскую страницу
         * 3. Если 2 и более - применяем лефты, райты и тд
         */
        $columns = $this->_getDb()->getColumnsList(array(
            'all_columns' => false,
        ));

        if ($pathInfo->sizeof === 1) {
            $this->setParent(1)
                ->setIsPublic('1')
                ->setUrlKey($pathInfo->segments[0])
                ->_setOrder('left asc');

            $this->_setLimit(1);

            $Data = $this->_fetchAll();
        } else {
            $_seg = $pathInfo->segments;
            $last = array_pop($_seg);
            unset($_seg);

            $Select = clone $this->_getSelect();
            $Select->reset(Zend_Db_Table::COLUMNS);
            $Select->columns(array(
                'left', 'right',
            ));
            $Select->where('is_public = ?', '1')
                ->where('url_key = ?', $last)
                ->order('left asc')
                ->limit(1);

            $Result = $this->_fetchRow($Select);

            if ($Result->count() == 0) {
                $Data = new Fenix_Object_Rowset(array('data' => array()));
            } else {
                // Список страниц
                $this->setParent('0', '<>')
                    ->setIsPublic('1')
                    ->setLeft($Result->left, '<=')
                    ->setRight($Result->right, '>=')
                    ->_setOrder('left asc');
                $Data = $this->_fetchAll();
            }

            // Проверяем по сегментами
            foreach ($Data AS $i => $_result) {
                if ($pathInfo->segments[$i] != $_result->url_key) {
                    $Data = new Fenix_Object_Rowset(array('data' => array()));
                    break;
                }
            }
        }

        if ($Data->count() == 0) {
            return false;
        }

        $Data = $Data->getData();



        // Выбранная страница
        $_current = $Data[sizeof($Data) - 1];

        // URL
        if($_current['url_key'] != 'default'){
            $page       = Fenix::getModel('core/structure')->getPageById($_current['id']);
            $navigation = Fenix::getModel('core/structure')->getNavigation($page);
            foreach($navigation AS $_nav) {
                $url[] = $_nav->url_key;
            }
            $_current['url'] = Fenix::getUrl(implode('/', $url));
        } else {
            $_current['url'] = Fenix::getUrl('/');
        }

        $_current['seo'] = $this->getSeoMetadata($_current, 'structure');
        $_current['gallery'] = $this->getGallery($_current['gallery']);
        if ($_current['image'] != null) {
            //				$info = (object)unserialize($_current['image_info']);
            //				$_current['image'] = Fenix::createImageFromStreamInfo($_current['image'], $info);

            $_current['image_path'] = HOME_DIR_ABSOLUTE . $_current['image'];
            $_current['image'] = HOME_DIR_URL . $_current['image'];
        }
        $selectedPage = new Fenix_Object(array(
            'data' => $_current,
        ));

        // Хлебные крошки к странице
        $navigation = array();
        foreach ($Data AS $_page) {
            $navigation[] = new Fenix_Object(array(
                'data' => $_page,
            ));
        }

        return new Fenix_Object(array(
            'data' => array(
                'current' => $selectedPage,
                'navigation' => new Fenix_Object_Rowset(array(
                    'data' => $navigation,
                    'rowClass' => '',
                )),
            ),
        ));
    }

    public function pageList($id = null)
    {
        $Data = Fenix::getModel('core/structure')->getPagesList($id);

        $data = array();
        foreach ($Data AS $_data) {
            $_data = Fenix::getCollection('core/pages_page')->setPage($_data->toArray());

            $data[] = $_data;
        }

        $result = new Fenix_Object_Rowset(array(
            'data' => $data,
            'rowClass' => false,
        ));

        return $result;
    }

    public function getGallery($gallery)
    {
        $gallery = (array)unserialize($gallery);
        $gallery = array_map(function ($value) {
            $value['path'] = BASE_DIR . 'home/' . str_replace('/var', 'var', $value['image']);

            //$value['url']  = BASE_URL . $value['url'];
            return $value;
        }, $gallery);

        return new Fenix_Object_Rowset(array('data' => $gallery));
    }
}