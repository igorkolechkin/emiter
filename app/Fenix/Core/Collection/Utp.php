<?php
class Fenix_Core_Collection_Utp extends Fenix_Resource_Collection
{
    public function getUtp($name)
    {
        $Utp = Fenix::getModel('core/utp')->getUtpByName($name);

        if ($Utp == null) {
            return false;
        }

        $_utp = $Utp->toArray();
        $_utp['block_list'] = $this->_getBlockList($Utp);

        return new Fenix_Object(array(
            'data' => $_utp
        ));
    }

    private function _getBlockList($Utp)
    {
        $blockList = Fenix::getModel('core/utp')->getPublicBlockList($Utp->id);

        $Result    = array();
        foreach ($blockList AS $_slide) {
            $_tmp          = $_slide->toArray();

            if (file_exists(HOME_DIR_ABSOLUTE . $_tmp['image']))
                $image = HOME_DIR_URL . $_tmp['image'];
            else
                $image = '';

            $_tmp['image'] = $image;

            $Result[]           = $_tmp;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }
}