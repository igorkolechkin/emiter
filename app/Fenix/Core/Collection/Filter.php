<?php
class Fenix_Core_Collection_Filter extends Fenix_Resource_Collection
{
    public function getFilter($name)
    {
        $Filter = Fenix::getModel('core/filter')->getFilterByName($name);

        if ($Filter == null) {
            return null;
        }

        $_filter = $Filter->toArray();
        $_filter['slide_list'] = $this->_getSlideList($Filter);

        return new Fenix_Object(array(
            'data' => $_filter
        ));
    }

    private function _getSlideList($Filter)
    {
        $slideList = Fenix::getModel('core/filter')->getSlideWithAttributesList($Filter->id);

        $Result    = array();
        foreach ($slideList AS $_slide) {
            $_tmp          = $_slide->toArray();
            $image         = HOME_DIR_URL . $_tmp['image'];
            $_tmp['image'] = $image;

            $Result[]           = $_tmp;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }
}