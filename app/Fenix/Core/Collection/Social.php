<?php
class Fenix_Core_Collection_Social extends Fenix_Resource_Collection
{
    public function getList()
    {
        $_social = Fenix::getModel('core/social')->getSocialList();

        $_data   = array();
        foreach ($_social AS $social) {
            $_array  = $social->toArray();

            if ($_array['image'] != null) {
                $image = HOME_DIR_URL . $_array['image'];
                $_array['image'] = $image;
            }

            $_data[] = $_array;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $_data
        ));
    }
}