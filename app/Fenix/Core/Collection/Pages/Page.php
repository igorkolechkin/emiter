<?php
class Fenix_Core_Collection_Pages_Page extends Fenix_Object
{
    use Fenix_Traits_ImageCollection;
    use Fenix_Traits_SeoCollection;

    public function setPage($_page)
    {
        // $_page['seo'] = $this->getSeoMetadata($_page, 'structure');
        $_page['url_relative'] = $_page['url_key'];

        $page       = Fenix::getModel('core/structure')->getPageById($_page['id']);
        $navigation = Fenix::getModel('core/structure')->getNavigation($page);
        foreach($navigation AS $_nav) {
            $url[] = $_nav->url_key;
        }

        $_page['url'] = Fenix::getUrl(implode('/', $url));

        $this->setData($_page);

        return $this;
    }

    public function getGallery()
    {
        $_gallery = (array) unserialize($this->getData('gallery'));

        $data = array();
        foreach ($_gallery AS $_image) {
            $data[] = Fenix::getCollection('core/pages_gallery')->setImage($_image);
        }

        $result = new Fenix_Object_Rowset(array(
            'data'     => $data,
            'rowClass' => false
        ));

        return $result;
    }
}