<?php
class Fenix_Core_Collection_Countdown extends Fenix_Resource_Collection
{
    public function getCountdown($name)
    {
        $Countdown = Fenix::getModel('core/countdown')->getCountdownByName($name);

        if ($Countdown == null) {
            return null;
        }

        $_countdown = $Countdown->toArray();
        $_countdown['slide_list'] = $this->_getSlideList($Countdown);

        return new Fenix_Object(array(
            'data' => $_countdown
        ));
    }

    private function _getSlideList($Countdown)
    {
        $slideList = Fenix::getModel('core/countdown')->getSlideList($Countdown->id);

        $Result    = array();
        foreach ($slideList AS $_slide) {
            $_tmp          = $_slide->toArray();
            $image         = HOME_DIR_URL . $_tmp['image'];
            $_tmp['image'] = $image;

            $Result[]           = $_tmp;
        }

        return new Fenix_Object_Rowset(array(
            'data' => $Result
        ));
    }
}