<?php
class Fenix_Core_Controller_Admin_Images_Optimize extends Fenix_Controller_Action
{

    const IMAGES_CHECK_LIMIT = 200;
    public function preDispatch()
    {
        //$this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/optimize_images_viewed')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/optimize_images_optimized')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/optimize_directories')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/optimize_files')
                ->prepare()
                ->execute();
        $dir = TMP_DIR_ABSOLUTE . 'optimize';

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true) or die('Не удалось создать');
        }

    }
    /**
     * Оптимизация изображений
     */
    public function indexAction()
    {

        ini_set('memory_limit','1024M');

        /*
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Оптимизация изображений"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Оптимизация изображений"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        //$files = Fenix::getModel('core/images_optimize')->getFiles('tmp/cache',self::IMAGES_CHECK_LIMIT);
        //$imagesCount = count($files);
       // $files2 = Fenix::getModel('core/optiimg')->getFiles(VAR_DIR_ABSOLUTE.'upload/',VAR_DIR_URL.'upload/');


        $Form = Fenix::getHelper('core/images_optimize')->getForm();

        if ($Form->ok()) {
            //Fenix::redirect('core/images/optimize/process');
            //Fenix::getModel('core/images_files')->clearDirectories();
            //Fenix::getModel('core/images_files')->clearFiles();
            //Fenix::getModel('core/images_files')->addDirectory('tmp/cache/images');

            $progress = $this->view->render('creator/progress.phtml');
            $Form = $Form->fetch().$progress;
        } else {
            $Form = $Form->fetch();
        }
        $cache = Fenix::getModel('core/images_transfer')->getCache();
        $info = '<br/>';
        if(count($cache->error)>0){
            $info .= count($cache->error).' '.Fenix::declination(count($cache->error), array('ошибок', 'ошибка', 'ошибки')).'<br />';
            $info .= implode("<br />", $cache->error);
            $info .= '<br />';
            $info .= '<br />';
            $info .= implode("<br />", $cache->actionsLog);

            $cache->error      = array();
            $cache->actionsLog = array();
        }
        $cache->error = array();
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Оптимизация изображений"));


        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Form.$info
                 ));
    }

    /**
     * Оптимизация
     */
    public function processAction()
    {

      /*  $mode = Fenix::getRequest()->getParam('mode');
        if ($mode == 'viewed') {
            Fenix::getModel('core/images_optimize')->processViewedOptimization();
        } else {
            Fenix::getModel('core/images_optimize')->processOptimization();
        }

        $cache = Fenix::getModel('core/images_transfer')->getCache();
        Fenix::getCreatorUI()
             ->loadPlugin('Events_Session')
             ->setType(Creator_Events::TYPE_OK)
             ->setMessage(Fenix::lang(
                 "Обработано " . $cache->count . ' ' . Fenix::declination($cache->count, array('изображений', 'изображение', 'изображения')) . ', Успешно: ' . $cache->successCount
             ))
             ->saveSession();

        Fenix::redirect('core/images/optimize');*/
        $cache = Fenix::getModel('core/images_transfer')->getCache();
        $mode = Fenix::getRequest()->getParam('mode');
        if ($mode == 'viewed') {
            Fenix::getModel('core/images_optimize')->processViewedOptimization();
            Fenix::getCreatorUI()
                 ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang(
                     "Обработано " . $cache->count . ' ' . Fenix::declination($cache->count, array('изображений', 'изображение', 'изображения')) . ', Успешно: ' . $cache->successCount
                 ))
                 ->saveSession();

            Fenix::redirect('core/images/optimize');

        } else {
            Fenix::getModel('core/images_optimize')->processOptimization();
        }

        $info = '<br/>';
        /*if(count($cache->error)>0){
            $info .= count($cache->error).' '.Fenix::declination(count($cache->error), array('ошибок', 'ошибка', 'ошибки')).'<br />';
            $info .= implode("<br />", $cache->error);
            $info .= '<br />';
            $info .= '<br />';
            $info .= implode("<br />", $cache->actionsLog);
            $info .= implode("<br />", $cache->failLog);

            $cache->error      = array();
            $cache->actionsLog = array();
            $cache->failLog = array();
        }*/
        $info .= count($cache->error).' '.Fenix::declination(count($cache->error), array('ошибок', 'ошибка', 'ошибки')).'<br />';
        $info .= implode("<br />", $cache->error);
        $info .= '<br />';
        $info .= '<br />';
        if(isset($cache->actionsLog) && is_array($cache->actionsLog))
            $info .= implode("<br />", $cache->actionsLog);
        if(isset($cache->failLog) && is_array($cache->failLog))
            $info .= implode("<br />", $cache->failLog);

        $cache->error      = array();
        $cache->actionsLog = array();
        $cache->failLog = array();

        $status = Fenix::getModel('core/images_files')->getFilesStatus();
        $result = array(
            'is_optimized_count' => (int)$status->is_optimized_count,
            'error_count'        => (int)$status->error_count,
            'total_count'        => (int)$status->total_count,
            'log'                => $info
        );
        echo json_encode($result);
        if (Fenix::isDev()){

        }

    }

    public function clearAction(){

        Fenix::getModel('core/images_files')->clearDirectories();
        Fenix::getModel('core/images_files')->clearFiles();
        Fenix::getModel('core/images_files')->addDirectory('tmp/cache/images');

        $count = Fenix::getModel('core/images_optimize')->clearOptimizedLog();
        Fenix::getCreatorUI()
             ->loadPlugin('Events_Session')
             ->setType(Creator_Events::TYPE_OK)
             ->setMessage(Fenix::lang("Список оптимизированых изображений очищен. " . $count. ' изображений'))
             ->saveSession();

        Fenix::redirect('core/images/optimize');
    }

}