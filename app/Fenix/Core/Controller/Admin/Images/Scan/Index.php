<?php
class Fenix_Core_Controller_Admin_Images_Scan_Index extends Fenix_Controller_Action
{
    public function indexAction(){
        Fenix::dump('adsf');
    }

    public function directoriesAction()
    {
        Fenix::getModel('core/images_files')->prepareDirectoryTree();
        $result = Fenix::getModel('core/images_files')->getDirectoriesStatus();

        echo json_encode(array(
            'scan_finished_count'     => (int)$result->scan_finished_count,
            'scan_not_finished_count' => (int)$result->scan_not_finished_count,
            'scan_total_count'        => (int)$result->scan_total_count
        ));
    }

    public function filesAction()
    {
        Fenix::getModel('core/images_files')->scanFiles();
        $result = Fenix::getModel('core/images_files')->getDirectoriesStatus();

        echo json_encode(array(
            'scan_finished_count'     => (int)$result->scan_finished_count,
            'scan_not_finished_count' => (int)$result->scan_not_finished_count,
            'scan_total_count'        => (int)$result->scan_total_count
        ));
    }

}