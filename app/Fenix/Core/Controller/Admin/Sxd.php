<?php

	class Fenix_Core_Controller_Admin_Sxd extends Fenix_Controller_Action {
		const SXD_PATH = 'var/sxd/';

		public function preDispatch() {
			$this->getHelper('rules')->checkDevRedirect();
		}

		public function indexAction() {
			$dbConfig = Zend_Registry::get('db')->getConfig();

			// Хлебные крошки
			$this->_helper->BreadCrumbs(array(
				array(
					'label' => Fenix::lang("Панель управления"),
					'uri'   => Fenix::getUrl(),
					'id'    => 'main',
				),
				array(
					'label' => Fenix::lang("Sypex Dumper (бэкапы)"),
					'uri'   => '',
					'id'    => 'last',
				),
			));

			/**
			 * Отображение
			 */
			$Creator = Fenix::getCreatorUI();

			// Форма
			$Form = $Creator->loadPlugin('Form');
			$Form->setAction('/var/sxd/index.php')
			     ->setTarget('_blank');

			$Form->setDefaults(array(
				'user' => $dbConfig['username'],
				'pass' => $dbConfig['password'],
			));

			$Form->setContent('<input type="hidden" name="user" value="' . $this->getRequest()->getPost('user') . '" />')
			     ->setContent('<input type="hidden" name="pass" value="' . $this->getRequest()->getPost('pass') . '" />');

			// Кнопули
			$Buttonset = $Creator->loadPlugin('Buttonset')
			                     ->setContent(array(
				                     $Creator->loadPlugin('Button')
				                             ->appendClass('btn-primary')
				                             ->setValue("Перейти к Sypex Dumper")
				                             ->setType('submit')
				                             ->fetch(),
			                     ));

			// Заголовок формы
			$Title = $Creator->loadPlugin('Title')
			                 ->setImage(Fenix::getAppEtcUrl('icons/icon-database.png', 'core'))
			                 ->setTitle(Fenix::lang("Управление бэкапами"));
			$Form->setContent($Title->fetch());

			$Form->setContent($Buttonset->fetch());

			// Компиляция
			$Form->compile();

			// Тайтл страницы
			$Creator->getView()
			        ->headTitle(Fenix::lang("Управление бэкапами"));


			/** Список Бэкапов */
			$Table = Fenix::getCreatorUI()->loadPlugin('Table');
			$Table->setTableId('cList');
			$Table->setTitle(Fenix::lang("Бэкапы"));
			
			$Table->addHeader('file', 'Файл');
			$Table->addHeader('url', 'Скачать');
			$Table->addHeader('delete', 'Удалить');
			
			$backups_list = scandir(BASE_DIR . self::SXD_PATH . 'backup');
			foreach($backups_list as $k => $file) {
				$path_info = pathinfo($file);

				if('gz' != $path_info['extension']) {
					unset($backups_list[ $k ]);
				} else {
					$Table->addColumn(array(
						'file'   => $file,
						'url'    => '<a href="' . BASE_URL . self::SXD_PATH . 'backup/' . $file . '">' . $file . '</a>',
						'delete' => $Creator->loadPlugin('Button')
						                    ->setValue(Fenix::lang("Удалить"))
						                    ->appendClass('btn-danger icon-trash')
						                    ->setType('button')
						                    ->setOnclick('if(confirm(\'Удалить файл бекапа '.$file.' ?\')) self.location.href=\'' . Fenix::getUrl('core/sxd/delete/' . $file) . '\'')
						                    ->fetch(),
					));
				}
			}
			

			// Событие
			$Event     = $Creator->loadPlugin('Events_Session');

			// Рендер страницы
			$Creator->setLayout()
			        ->oneColumn(array(
				        $Form->fetch(),
				        $Event->fetch(),
				        $Table->fetch(),
			        ));
		}

		public function deleteAction() {
			$file  = $this->getRequest()->getParam('delete');
			$file_path = BASE_DIR . self::SXD_PATH . 'backup/' . $file;

			if(file_exists($file_path)){
				unlink($file_path);

				Fenix::getCreatorUI()
				     ->loadPlugin('Events_Session')
				     ->setType(Creator_Events::TYPE_OK)
				     ->setMessage(str_replace('%file%', $file_path, Fenix::lang("Файл бекапа удален") .' <b>%file%</b>'))
				     ->saveSession();

				Fenix::redirect('core/sxd');
			} else {

				Fenix::getCreatorUI()
				     ->loadPlugin('Events_Session')
				     ->setType(Creator_Events::TYPE_ERROR)
				     ->setMessage(str_replace('%file%', $file_path, Fenix::lang("Файл бекапа не найден") .' <b>%file%</b>'))
				     ->saveSession();

				Fenix::redirect('core/sxd');
			}
		}
	}