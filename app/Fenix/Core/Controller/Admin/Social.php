<?php
class Fenix_Core_Controller_Admin_Social extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemSocial');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/social')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $blocksList = Fenix::getModel('core/social')->getSocialListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Социальные сети"),
                'uri'   => Fenix::getUrl('core/social'),
                'id'    => 'mail'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Добавить соцсеть"))
                                         ->setType('button')
                                         ->appendClass('btn-primary')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/social/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Социальные сети"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('mailsList')
                           ->setTitle(Fenix::lang("Социальные сети"))
                           ->setData($blocksList)
                           ->setStandartButtonset();
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/social/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/social/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Социальные сети"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/social')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Социальные сети"),
                'uri'   => Fenix::getUrl('core/social'),
                'id'    => 'mail'
            ),
            array(
                'label' => Fenix::lang("Новая соцсеть"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/social', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Соцсеть добавлена"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/social');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/social/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/social/edit/id/' . $id);
            }
            
            Fenix::redirect('core/social');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая соцсеть"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentSocial = Fenix::getModel('core/social')->getSocialById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSocial == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Соцсеть не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/social');
        }
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Социальные сети"),
                'uri'   => Fenix::getUrl('core/social'),
                'id'    => 'mail'
            ),
            array(
                'label' => Fenix::lang("Редактировать соцсеть"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentSocial->toArray())
                   ->setData('current', $currentSocial);
        
        // Источник
        $Form      ->setSource('core/social', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentSocial, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Соцсеть отредактирована"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/social');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/social/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/social/edit/id/' . $id);
            }
            
            Fenix::redirect('core/social');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать соцсеть"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentSocial = Fenix::getModel('core/social')->getSocialById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSocial == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Соцсеть не найдена"))
                ->saveSession();

            Fenix::redirect('core/social');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('core/social', 'default')
                 ->deleteRecord($currentSocial);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Соцсеть удалена"))
                 ->saveSession();
            
        Fenix::redirect('core/social');
    }
}