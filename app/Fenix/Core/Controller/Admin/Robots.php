<?php
class Fenix_Core_Controller_Admin_Robots extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');
    }
    /**
     * Редактирование robots.txt
     */
    public function indexAction()
    {
        ini_set('memory_limit','1024M');

        /*
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();


        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Редактирование robots.txt"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Редактирование robots.txt"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        $data = Fenix::getModel('core/robots')->getRobots();
        $Form = Fenix::getHelper('core/robots')->getForm($data);

        if($Form->ok()){
            Fenix::getModel('core/robots')->saveRobots(Fenix::getRequest()->getPost('data'));
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Файл успешно отредактирован"))
                ->saveSession();
        }

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактирование robots.txt"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Form->fetch()
                     /*

                     $Info->fetch()*/
                 ));
    }

    /**
     * Сохранение
     */
    public function saveAction()
    {
    }

}