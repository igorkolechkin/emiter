<?php

class Fenix_Core_Controller_Admin_Common extends Fenix_Controller_Action
{
    public function indexAction()
    {
    }

    public function uniquecheckerAction()
    {
        $table = $this->getRequest()->getPost('table');
        $field = $this->getRequest()->getPost('field');
        $value = $this->getRequest()->getPost('value');
        $current = $this->getRequest()->getPost('current');
        $current = empty($current)?null:$current;
        $response = array();
        if (!empty($table) && !empty($field) && !empty($value)) {
            $Result = Fenix::getModel('core/common')->checkUniqueValue($table, $field, $value, $current);
            if ($Result == null) {
                $response = array('type' => 'alert-success', 'message' => Fenix::lang('Данный') . ' ' . $field . ' ' . Fenix::lang('свободен'));
            } else {
                $response = array('type' => 'alert-danger', 'message' => Fenix::lang('Данный') . ' ' . $field . ' ' . Fenix::lang('уже занят'));
            }
        } else {
            if (empty($table)) {
                $response = array('type' => 'alert-danger', 'message' => Fenix::lang('Таблица не указана'));
            } elseif (empty($field)) {
                $response = array('type' => 'alert-danger', 'message' => Fenix::lang('Поле не указано'));
            }elseif (empty($value)) {
                $response = array('type' => 'alert-danger', 'message' => Fenix::lang('Значение не указано'));
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}