<?php
class Fenix_Core_Controller_Admin_Filter extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('catalogAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/filter')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $filtersList = Fenix::getModel('core/filter')->getFilterListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый атрибут"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/filter/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Атрибуты"))
                           ->setDetails(Fenix::lang("Можно использовать для создания различных атрибутов баннеров на разных страницах сайта"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('filtersList')
                           ->setTitle(Fenix::lang("Атрибуты"))
                           ->setData($filtersList)
                           ->setStandartButtonset();

        $Table   ->setCellCallback('title', function($value, $data, $column, $table){
            return '<a href="' . Fenix::getUrl('core/filter/content/sid/' . $data->id) . '">' . $value . '</a>';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/filter/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/filter/delete/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Сладеры"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/filter')
                 ));
    }

    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('core/filter'),
                'id'    => 'filter'
            ),
            array(
                'label' => Fenix::lang("Новый атрибут"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/filter', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/filter');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/filter/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/filter/edit/id/' . $id);
            }
            
            Fenix::redirect('core/filter');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentFilter = Fenix::getModel('core/filter')->getFilterById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentFilter == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Атрибут не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/filter');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Атрибуты"),
                'uri'   => Fenix::getUrl('core/filter'),
                'id'    => 'filter'
            ),
            array(
                'label' => Fenix::lang("Изменить атрибут"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentFilter->toArray())
                   ->setData('current', $currentFilter);
        
        // Источник
        $Form      ->setSource('core/filter', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentFilter, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Атрибут отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/filter');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/filter/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/filter/edit/id/' . $id);
            }
            
            Fenix::redirect('core/filter');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать атрибут"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentFilter = Fenix::getModel('core/filter')->getFilterById(
            $this->getRequest()->getParam('id')
        );

        if ($currentFilter == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Атрибут не найден"))
                ->saveSession();

            Fenix::redirect('core/filter');
        }

        $slideList = Fenix::getModel('core/filter')->getSlideList($currentFilter->id);

        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/filter', 'default')
                ->deleteRecord($currentFilter);

        foreach ($slideList AS $_slide) {
            $Creator->loadPlugin('Form_Generator')
                    ->setSource('core/filter_content', 'default')
                    ->deleteRecord($_slide);
        }

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Атрибут удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/filter');
    }
}