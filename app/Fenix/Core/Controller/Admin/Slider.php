<?php
class Fenix_Core_Controller_Admin_Slider extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/slider')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/slider_content')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $slidersList = Fenix::getModel('core/slider')->getSliderListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый слайдер"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/slider/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Слайдеры"))
                           ->setDetails(Fenix::lang("Можно использовать для создания различных слайдеров баннеров на разных страницах сайта"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('slidersList')
                           ->setTitle(Fenix::lang("Слайдеры"))
                           ->setData($slidersList)
                           ->setStandartButtonset();

        $Table   ->setCellCallback('title', function($value, $data, $column, $table){
            return '<a href="' . Fenix::getUrl('core/slider/content/sid/' . $data->id) . '">' . $value . '</a>';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/slider/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/slider/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Сладеры"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/slider')
                 ));
    }

    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/slider'),
                'id'    => 'slider'
            ),
            array(
                'label' => Fenix::lang("Новый слайдер"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/slider', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/slider');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/slider/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/slider/edit/id/' . $id);
            }
            
            Fenix::redirect('core/slider');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый слайдер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentSlider = Fenix::getModel('core/slider')->getSliderById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSlider == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Слайдер не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/slider');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/slider'),
                'id'    => 'slider'
            ),
            array(
                'label' => Fenix::lang("Изменить слайдер"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentSlider->toArray())
                   ->setData('current', $currentSlider);
        
        // Источник
        $Form      ->setSource('core/slider', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentSlider, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/slider');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/slider/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/slider/edit/id/' . $id);
            }
            
            Fenix::redirect('core/slider');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать слайдер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentSlider = Fenix::getModel('core/slider')->getSliderById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlider == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайдер не найден"))
                ->saveSession();

            Fenix::redirect('core/slider');
        }

        $slideList = Fenix::getModel('core/slider')->getSlideList($currentSlider->id);

        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/slider', 'default')
                ->deleteRecord($currentSlider);

        foreach ($slideList AS $_slide) {
            $Creator->loadPlugin('Form_Generator')
                    ->setSource('core/slider_content', 'default')
                    ->deleteRecord($_slide);
        }

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Слайдер удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/slider');
    }
}