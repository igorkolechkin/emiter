<?php
class Fenix_Core_Controller_Admin_Filter_Content extends Fenix_Controller_Action
{
    private $_filter;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('catalogAll');


        // Проверки
        $this->_filter = Fenix::getModel('core/filter')->getFilterById(
            (int) $this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_filter == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайдер не найден"))
                ->saveSession();

            Fenix::redirect('core/filter');
        }

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/filter_content')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $slideList = Fenix::getModel('core/filter')->getSlideListAsSelect($this->_filter->id);
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый слайд"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/filter/content/add/sid/' . $this->_filter->id) . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Назад"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/filter') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Слайдеры / " . $this->_filter->title))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/filter'),
                'id'    => 'filter'
            ),
            array(
                'label' => $this->_filter->title,
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('filtersList')
                           ->setTitle(Fenix::lang("Слайдеры / " . $this->_filter->title))
                           ->setData($slideList)
                           ->setStandartButtonset();
        $Table   ->setCellCallback('image', function($value, $data, $column, $table){
            if ($data->image == null)
                return;

            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 100,100);

            return '<img src="' . $url . '" width="50" alt="" />';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/filter/content/edit/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/filter/content/delete/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Слайдеры / " . $this->_filter->title));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/filter_content')
                 ));
    }

    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/filter'),
                'id'    => 'filter'
            ),
            array(
                'label' => $this->_filter->title,
                'uri'   => Fenix::getUrl('core/filter/content/sid/' . $this->_filter->id),
                'id'    => 'filter'
            ),
            array(
                'label' => Fenix::lang("Добавить слайд"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        $Form       ->setData('filter', $this->_filter)
                    ->setData('current', null);
        // Источник
        $Form      ->setSource('core/filter_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_filter->id);

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/filter/content/sid/' . $this->_filter->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/filter/content/add/sid/' . $this->_filter->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/filter/content/edit/sid/' . $this->_filter->id . '/id/' . $id);
            }

            Fenix::redirect('core/filter/content/sid/' . $this->_filter->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый слайд"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentSlide = Fenix::getModel('core/filter')->getSlideById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Слайд не найден"))
                    ->saveSession();

            Fenix::getUrl('core/filter/content/sid/' . $this->_filter->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/filter'),
                'id'    => 'filter'
            ),
            array(
                'label' => $this->_filter->title,
                'uri'   => Fenix::getUrl('core/filter/content/sid/' . $this->_filter->id),
                'id'    => 'filter'
            ),
            array(
                'label' => Fenix::lang("Редактировать слайд"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentSlide->toArray())
                   ->setData('current', $currentSlide)
                   ->setData('filter',  $this->_filter);
        
        // Источник
        $Form      ->setSource('core/filter_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentSlide, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер отредактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/filter/content/sid/' . $this->_filter->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/filter/content/add/sid/' . $this->_filter->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/filter/content/edit/sid/' . $this->_filter->id . '/id/' . $id);
            }

            Fenix::redirect('core/filter/content/sid/' . $this->_filter->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать слайдер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentSlide = Fenix::getModel('core/filter')->getSlideById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('core/filter/content/sid/' . $this->_filter->id);
        }

        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('core/filter_content', 'default')
                 ->deleteRecord($currentSlide);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Слайд удалён"))
                 ->saveSession();

        Fenix::redirect('core/filter/content/sid/' . $this->_filter->id);
    }
}