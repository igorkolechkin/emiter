<?php

	class Fenix_Core_Controller_Admin_Logging extends Fenix_Controller_Action {
		public function preDispatch() {
			$this->getHelper('rules')->checkDevRedirect();
		}

		public function indexAction() {
			/**
			 * Отображение
			 */
			$Creator = Fenix::getCreatorUI();

			// Заголовок страницы
			$Title = $Creator->loadPlugin('Title')
			                 ->setTitle(Fenix::lang("Error log"));

			// Хлебные крошки
			$_crumb = array();
			$_crumb[] = array(
				'label' => Fenix::lang("Панель управления"),
				'uri'   => Fenix::getUrl(),
				'id'    => 'main',
			);
			$_crumb[] = array(
				'label' => Fenix::lang("Error log"),
				'uri'   => '',
				'id'    => 'errorLog',
			);
			$this->_helper->BreadCrumbs($_crumb);

			// Тайтл страницы
			$Creator->getView()
			        ->headTitle(Fenix::lang("Error log"));

			$GroupsTabs = $Creator->loadPlugin('Tabs');

			/** Наполните список файлов-логов */
			$files = array(
				'Error Log'  => '../../logs/' . $_SERVER['HTTP_HOST'] . '.error.log',
				'Access Log' => '../../logs/' . $_SERVER['HTTP_HOST'] . '.access.log',
			);

			if(count($files) > 0) {
				foreach($files as $tab_name => $file) {
					if(file_exists(BASE_DIR . $file)) {
						$GroupsTabs->addTab(
							md5($file),
							$tab_name,
							'<pre>' . file_get_contents(BASE_DIR . $file) .'</pre>'
						);
					}
				}
			}

			$Creator->setLayout()
			        ->oneColumn(array(
				        $Title->fetch(),
				        $GroupsTabs->fetch(),
			        ));
		}
	}