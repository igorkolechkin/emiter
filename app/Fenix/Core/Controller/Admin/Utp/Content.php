<?php
class Fenix_Core_Controller_Admin_Utp_Content extends Fenix_Controller_Action
{
    private $_utp;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        // Проверки
        $this->_utp = Fenix::getModel('core/utp')->getUtpById(
            (int) $this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_utp == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("УТП не найден"))
                ->saveSession();

            Fenix::redirect('core/utp');
        }

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/utp_content')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $slideList = Fenix::getModel('core/utp')->getBlockListAsSelect($this->_utp->id);
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый элемент"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/utp/content/add/sid/' . $this->_utp->id) . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Назад"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/utp') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("УТП / " . $this->_utp->title))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => Fenix::getUrl('core/utp'),
                'id'    => 'utp'
            ),
            array(
                'label' => $this->_utp->title,
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('utpsList')
                           ->setTitle(Fenix::lang("УТП / " . $this->_utp->title))
                           ->setData($slideList)
                           ->setStandartButtonset();
        $Table   ->setCellCallback('image', function($value, $data, $column, $table){
            if ($data->image == null)
                return;

            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 100,100);

            return '<img src="' . $url . '" width="50" alt="" />';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/utp/content/edit/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/utp/content/delete/sid/{$data->parent}/id/{$data->id}')
        ));

        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("УТП / " . $this->_utp->title));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/utp_content')
                 ));
    }

    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => Fenix::getUrl('core/utp'),
                'id'    => 'utp'
            ),
            array(
                'label' => $this->_utp->title,
                'uri'   => Fenix::getUrl('core/utp/content/sid/' . $this->_utp->id),
                'id'    => 'utp'
            ),
            array(
                'label' => Fenix::lang("Добавить элемент"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        $Form       ->setData('utp', $this->_utp)
                    ->setData('current', null);
        // Источник
        $Form      ->setSource('core/utp_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_utp->id);

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("УТП создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/utp/content/sid/' . $this->_utp->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/utp/content/add/sid/' . $this->_utp->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/utp/content/edit/sid/' . $this->_utp->id . '/id/' . $id);
            }

            Fenix::redirect('core/utp/content/sid/' . $this->_utp->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый элемент"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentBlock = Fenix::getModel('core/utp')->getBlockById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentBlock == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Слайд не найден"))
                    ->saveSession();

            Fenix::getUrl('core/utp/content/sid/' . $this->_utp->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => Fenix::getUrl('core/utp'),
                'id'    => 'utp'
            ),
            array(
                'label' => $this->_utp->title,
                'uri'   => Fenix::getUrl('core/utp/content/sid/' . $this->_utp->id),
                'id'    => 'utp'
            ),
            array(
                'label' => Fenix::lang("Редактировать элемент"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentBlock->toArray())
                   ->setData('current', $currentBlock)
                   ->setData('utp',  $this->_utp);
        
        // Источник
        $Form      ->setSource('core/utp_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentBlock, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("УТП отредактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/utp/content/sid/' . $this->_utp->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/utp/content/add/sid/' . $this->_utp->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/utp/content/edit/sid/' . $this->_utp->id . '/id/' . $id);
            }

            Fenix::redirect('core/utp/content/sid/' . $this->_utp->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать элементер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentBlock = Fenix::getModel('core/utp')->getBlockById(
            $this->getRequest()->getParam('id')
        );

        if ($currentBlock == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('core/utp/content/sid/' . $this->_utp->id);
        }

        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('core/utp_content', 'default')
                 ->deleteRecord($currentBlock);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Слайд удалён"))
                 ->saveSession();

        Fenix::redirect('core/utp/content/sid/' . $this->_utp->id);
    }
}