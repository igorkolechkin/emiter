<?php
class Fenix_Core_Controller_Admin_Countdown_Content extends Fenix_Controller_Action
{
    private $_countdown;

    public function preDispatch()
    {
        // Проверки
        $this->_countdown = Fenix::getModel('core/countdown')->getCountdownById(
            (int) $this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_countdown == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайдер не найден"))
                ->saveSession();

            Fenix::redirect('core/countdown');
        }

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/countdown_content')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $slideList = Fenix::getModel('core/countdown')->getSlideListAsSelect($this->_countdown->id);
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый слайд"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/countdown/content/add/sid/' . $this->_countdown->id) . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Назад"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/countdown') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Слайдеры / " . $this->_countdown->title))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/countdown'),
                'id'    => 'countdown'
            ),
            array(
                'label' => $this->_countdown->title,
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('countdownsList')
                           ->setTitle(Fenix::lang("Слайдеры / " . $this->_countdown->title))
                           ->setData($slideList)
                           ->setStandartButtonset();
        $Table   ->setCellCallback('image', function($value, $data, $column, $table){
            if ($data->image == null)
                return;

            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 100,100);

            return '<img src="' . $url . '" width="50" alt="" />';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/countdown/content/edit/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/countdown/content/delete/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Слайдеры / " . $this->_countdown->title));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/countdown_content')
                 ));
    }

    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/countdown'),
                'id'    => 'countdown'
            ),
            array(
                'label' => $this->_countdown->title,
                'uri'   => Fenix::getUrl('core/countdown/content/sid/' . $this->_countdown->id),
                'id'    => 'countdown'
            ),
            array(
                'label' => Fenix::lang("Добавить слайд"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        $Form       ->setData('countdown', $this->_countdown)
                    ->setData('current', null);
        // Источник
        $Form      ->setSource('core/countdown_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_countdown->id);

            $req = Fenix::getRequest();
            $req->setPost('end_date', Fenix::getRequest()->getPost('end_date') . ' ' . Fenix::getRequest()->getPost('end_time'));
            $req->setPost('parent', $this->_countdown->id);




            $id = $Form ->addRecord(
                $req
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/countdown/content/sid/' . $this->_countdown->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/countdown/content/add/sid/' . $this->_countdown->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/countdown/content/edit/sid/' . $this->_countdown->id . '/id/' . $id);
            }

            Fenix::redirect('core/countdown/content/sid/' . $this->_countdown->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый слайд"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentSlide = Fenix::getModel('core/countdown')->getSlideById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Слайд не найден"))
                    ->saveSession();

            Fenix::getUrl('core/countdown/content/sid/' . $this->_countdown->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/countdown'),
                'id'    => 'countdown'
            ),
            array(
                'label' => $this->_countdown->title,
                'uri'   => Fenix::getUrl('core/countdown/content/sid/' . $this->_countdown->id),
                'id'    => 'countdown'
            ),
            array(
                'label' => Fenix::lang("Редактировать слайд"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');


        $Defaults  = $currentSlide->toArray();

        list($endDate, $endTime) = explode(' ', $currentSlide->end_date);

        $Defaults['end_date'] = $endDate;
        $Defaults['end_time'] = $endTime;

        $Form      ->setDefaults($Defaults)
                   ->setData('current', $currentSlide)
                   ->setData('countdown',  $this->_countdown);
        
        // Источник
        $Form      ->setSource('core/countdown_content', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            $req = Fenix::getRequest();
            $req->setPost('end_date', Fenix::getRequest()->getPost('end_date') . ' ' . Fenix::getRequest()->getPost('end_time'));


            $id = $Form ->editRecord($currentSlide,$req);
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Слайдер отредактирован"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/countdown/content/sid/' . $this->_countdown->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/countdown/content/add/sid/' . $this->_countdown->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/countdown/content/edit/sid/' . $this->_countdown->id . '/id/' . $id);
            }

            Fenix::redirect('core/countdown/content/sid/' . $this->_countdown->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать слайдер"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentSlide = Fenix::getModel('core/countdown')->getSlideById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('core/countdown/content/sid/' . $this->_countdown->id);
        }

        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('core/countdown_content', 'default')
                 ->deleteRecord($currentSlide);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Слайд удалён"))
                 ->saveSession();

        Fenix::redirect('core/countdown/content/sid/' . $this->_countdown->id);
    }
}