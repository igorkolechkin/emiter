<?php

class Fenix_Core_Controller_Admin_Slider_Content extends Fenix_Controller_Action
{
    private $_slider;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        // Проверки
        $this->_slider = Fenix::getModel('core/slider')->getSliderById(
            (int)$this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_slider == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайдер не найден"))
                ->saveSession();

            Fenix::redirect('core/slider');
        }

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('core/slider_content')
            ->prepare()
            ->execute();
    }

    public function indexAction()
    {
        $slideList = Fenix::getModel('core/slider')->getSlideListAsSelect($this->_slider->id);

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый слайд"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/slider/content/add/sid/' . $this->_slider->id) . '\'')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Назад"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/slider') . '\'')
                    ->fetch()
            ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Слайдеры / " . $this->_slider->title))
            ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/slider'),
                'id'    => 'slider'
            ),
            array(
                'label' => $this->_slider->title,
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('slidersList')
            ->setTitle(Fenix::lang("Слайдеры / " . $this->_slider->title))
            ->setData($slideList)
            ->setStandartButtonset();
        $Table->setCellCallback('image', function ($value, $data, $column, $table) {
            if ($data->image == null) {
                return;
            }
            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 200, 200);

            return '<img src="' . $url . '" alt="" />';
        });

        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/slider/content/edit/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/slider/content/delete/sid/{$data->parent}/id/{$data->id}')
        ));
        $Table->addAction(array(
            'type'    => 'sorting',
            'options' => array(
                'html' => 'text'
            )
        ));
        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Слайдеры / " . $this->_slider->title));

        // Рендер страницы
        $Creator->setLayout()
            ->oneColumn(array(
                $Title->fetch(),
                $Event->fetch(),
                $Table->fetch('core/slider_content')
            ));
    }

    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/slider'),
                'id'    => 'slider'
            ),
            array(
                'label' => $this->_slider->title,
                'uri'   => Fenix::getUrl('core/slider/content/sid/' . $this->_slider->id),
                'id'    => 'slider'
            ),
            array(
                'label' => Fenix::lang("Добавить слайд"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('slider', $this->_slider)
            ->setData('current', null);
        // Источник
        $Form->setSource('core/slider_content', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_slider->id);

            $id = $Form->addRecord(
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Слайдер создан"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/slider/content/sid/' . $this->_slider->id);
            } elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/slider/content/add/sid/' . $this->_slider->id);
            } elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/slider/content/edit/sid/' . $this->_slider->id . '/id/' . $id);
            }

            Fenix::redirect('core/slider/content/sid/' . $this->_slider->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новый слайд"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentSlide = Fenix::getModel('core/slider')->getSlideById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('core/slider/content/sid/' . $this->_slider->id);
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Слайдеры"),
                'uri'   => Fenix::getUrl('core/slider'),
                'id'    => 'slider'
            ),
            array(
                'label' => $this->_slider->title,
                'uri'   => Fenix::getUrl('core/slider/content/sid/' . $this->_slider->id),
                'id'    => 'slider'
            ),
            array(
                'label' => Fenix::lang("Редактировать слайд"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentSlide->toArray())
            ->setData('current', $currentSlide)
            ->setData('slider', $this->_slider);

        // Источник
        $Form->setSource('core/slider_content', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->editRecord($currentSlide, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Слайдер отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/slider/content/sid/' . $this->_slider->id);
            } elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/slider/content/add/sid/' . $this->_slider->id);
            } elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/slider/content/edit/sid/' . $this->_slider->id . '/id/' . $id);
            }

            Fenix::redirect('core/slider/content/sid/' . $this->_slider->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать слайдер"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $currentSlide = Fenix::getModel('core/slider')->getSlideById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Слайд не найден"))
                ->saveSession();

            Fenix::getUrl('core/slider/content/sid/' . $this->_slider->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Creator->loadPlugin('Form_Generator')
            ->setSource('core/slider_content', 'default')
            ->deleteRecord($currentSlide);

        $Creator->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Слайд удалён"))
            ->saveSession();

        Fenix::redirect('core/slider/content/sid/' . $this->_slider->id);
    }
}