<?php
class Fenix_Core_Controller_Admin_Settings extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemSettings');
    }

    public function indexAction()
    {
        $Config  = Fenix::getConfig();
        
        $this    ->view
                 ->assign('Config', $Config);
        
        $Creator = Fenix::getCreatorUI();
        
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-settings.png', 'core'))
                           ->setTitle(Fenix::lang("Настройки системы"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Настройки"),
                'uri'   => '',
                'id'    => 'settings'
            )
        ));

        $Creator ->getView()
                 ->headTitle(Fenix::lang("Настройки системы"));
        
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $this->view->render('creator/settings/panes.phtml')
                 ));
    }
    
    public function sectionAction()
    {
        //Fenix::dump(132);
        $Config  = Fenix::getConfig();
        
        $Section = $Config->getSection(
            $this->getRequest()->getParam('name')
        );
        
        // Источник формы
        $Source  = $Config->getXmlSection(
            $this->getRequest()->getParam('name')
        );
        
        $Creator = Fenix::getCreatorUI();

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-success')
                                         ->setValue("Применить")
                                         ->setType('submit')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-warning')
                                         ->setValue("Отменить")
                                         ->setType('reset')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-default')
                                         ->setValue("Назад")
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/settings') . '\'')
                                         ->fetch()
                             ));
        
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-settings.png', 'core'))
                           ->setTitle(Fenix::lang("Раздел параметров &laquo;%s&raquo;", $Section->getLabel()))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Настройки"),
                'uri'   => Fenix::getUrl('core/settings'),
                'id'    => 'settings'
            ),
            array(
                'label' => $Section->getLabel(),
                'uri'   => '',
                'id'    => 'section'
            )
        ));


        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        $Form      ->setDefaults($Config->getDefaultsForForm());
        
        $Form      ->setContent($Title->fetch());
        $Form      ->setContent($Event->fetch());
        
        
        // Источник
        $Form      ->setXmlSource($Source)
                   ->renderSource();
        
        $Form      ->setContent($Buttonset->fetch());
        
        // Компиляция
        $Form      ->compile();

        if ($Form->ok()) {
            $Config ->saveSettings(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Настройки сохранены"))
                    ->saveSession();

            Fenix::getModel('core/cache')->cleanCache('settings');
            Fenix::redirect('core/settings/section/name/' . $Section->getName());
        }
        
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Раздел параметров «%s»", $Section->getLabel()));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
}