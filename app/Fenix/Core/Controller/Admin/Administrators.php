<?php
class Fenix_Core_Controller_Admin_Administrators extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemAdminAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/administrators')
                ->prepare()
                ->execute();
        $Engine ->setDatabaseTemplate('core/administrators_groups')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $adminList = Fenix::getModel('core/administrators')->getAdministratorsListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый администратор"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/administrators/add') . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Группы"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/administrators/groups') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-administrator.png', 'core'))
                           ->setTitle(Fenix::lang("Администраторы системы"))
                           ->setButtonset($Buttonset->fetch());
        
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('adminList')
                           ->setTitle(Fenix::lang("Управление администраторами"))
                           ->setData($adminList)
                           ->setStandartButtonset()
                           ->setCellCallback('image', function($value, $data, $column, $table){
                               if ($data->image == null)
                                   return;
                               $info     = (object)unserialize($data->image_info);
                               $imageUrl = Fenix::createImageFromStreamInfo($data->image, $info);
                               return '<img src="' . $imageUrl . '" width="50" alt="" />';
                           })
                           ->setCellCallback('login_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('create_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/administrators/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/administrators/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Администраторы системы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/administrators')
                 ));
    }
    
    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => Fenix::getUrl('core/administrators'),
                'id'    => 'administrators'
            ),
            array(
                'label' => Fenix::lang("Новый администратор"),
                'uri'   => '',
                'id'    => 'new'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/administrators', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            $adminId = Fenix::getModel('core/administrators')->addAdministrator(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Администратор создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/administrators');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/administrators/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/administrators/edit/id/' . $adminId);
            }
            
            Fenix::redirect('core/administrators');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый администратор"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        // Проверки
        $administrator = Fenix::getModel('core/administrators')->getAdministratorById(
            (int) $this->getRequest()->getParam('id')
        );
        
        $Creator = Fenix::getCreatorUI();
        
        if ($administrator == null) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Администратор не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators');
        }
        
        $user = Fenix::getModel('session/auth')->getUser();
        
        if ($administrator->id == 1 && $user->id != 1) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Нельзя редактировать профиль главного админа"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => Fenix::getUrl('core/administrators'),
                'id'    => 'administrators'
            ),
            array(
                'label' => Fenix::lang("Редактировать администратора"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        $Form      ->setDefaults($administrator->toArray())
                   ->setData('current', $administrator);
        
        // Источник
        $Form      ->setSource('core/administrators', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            Fenix::getModel('core/administrators')->editAdministrator(
                $administrator,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Администратор отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/administrators');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/administrators/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/administrators/edit/id/' . $administrator->id);
            }
            
            Fenix::redirect('core/administrators');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактирование администратора"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $administrator = Fenix::getModel('core/administrators')->getAdministratorById(
            (int) $this->getRequest()->getParam('id')
        );
        
        $Creator = Fenix::getCreatorUI();
        
        if ($administrator == null) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Администратор не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators');
        }
        
        $user = Fenix::getModel('session/auth')->getUser();
        
        if ($administrator->id == $user->id) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Нельзя самоубиваться"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators');
        }
        
        Fenix::getModel('core/administrators')->deleteAdministrator(
            $administrator
        );
        
        $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Администратор удалён"))
                ->saveSession();

        Fenix::redirect('core/administrators');
    }
}