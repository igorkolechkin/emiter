<?php
class Fenix_Core_Controller_Admin_Denied extends Fenix_Controller_Action
{
    public function indexAction()
    {
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-denied.png', 'core'))
                           ->setTitle(Fenix::lang("Доступ запрещён"));
        
        $Error   = $Creator->loadPlugin('Events')
                           ->appendClass(Creator_Events::TYPE_ERROR)
                           ->setMessage("Вам запрещён доступ к данной функции");
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Доступ запрещён"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Error->fetch()
                 ));
    }
}