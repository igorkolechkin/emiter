<?php
class Fenix_Core_Controller_Admin_Cache extends Fenix_Controller_Action
{
    private $_cache     = null,
            $_cacheList = array();
    
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemCache');

        $this->_cache = new Fenix_Cache();
        $this->_cacheList = $this->_cache
                                 ->getCacheList();

        Fenix::getModel('core/cache')
                ->updateCacheTable($this->_cacheList);        
    }
    
    public function indexAction()
    {
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-cache.png', 'core'))
                           ->setTitle(Fenix::lang("Управление кешированием"));


        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Кеширование"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));
        
        $Table   = Fenix::getHelper('core/cache')->getCacheTable($this->_cacheList);
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Управление кешированием"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch()
                 ));
    }
    
    public function onAction()
    {
        Fenix::getModel('core/cache')->updateStatus($this->getRequest()->getParam('name'), 1);
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Вы успешно включили кеш"))
                 ->saveSession();
        
        Fenix::redirect("core/cache");
    }
    
    public function offAction()
    {
        Fenix::getModel('core/cache')->updateStatus($this->getRequest()->getParam('name'), 0);
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Вы успешно выключили кеш"))
                 ->saveSession();
        
        Fenix::redirect("core/cache");
    }
    
    public function cleanAction()
    {
        Fenix::getModel('core/cache')->cleanCache($this->getRequest()->getParam('name'));
        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Вы успешно очистили кеш"))
                 ->saveSession();
        
        Fenix::redirect("core/cache");
    }    
}