<?php
class Fenix_Core_Controller_Admin_Seo extends Fenix_Controller_Action
{

    private $_engine;
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemSeo');

        $this->_engine = new Fenix_Engine_Database();

        $this->_engine->setDatabaseTemplate('core/seo_categories_relations')
            ->prepare()
            ->execute();

        $this->_engine ->setDatabaseTemplate('core/seo')
            ->prepare()
            ->execute();
    }
    
    public function indexAction()
    {
        $blocksList = Fenix::getModel('core/seo')->getSeoListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите тип шаблона"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('core/seo/add/attributeset/{$attributeset}')
        )));


        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый seo шаблон"))
                                         ->setType('button')
                                         ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('core/seo/add') . '\'' : $Dialog->toButton()))
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-seo.png', 'core'))
                           ->setTitle(Fenix::lang("SEO шаблоны"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("SEO шаблоны"),
                'uri'   => Fenix::getUrl('core/seo'),
                'id'    => 'seo'
            )
        ));
        
        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('seosList')
                           ->setTitle(Fenix::lang("SEO шаблоны"))
                           ->setData($blocksList)
                           ->setStandartButtonset();
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/seo/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/seo/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("SEO шаблоны"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->twoColumnsLeft(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Dialog->fetch(),
                     $Table->fetch('core/seo')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("SEO шаблоны"),
                'uri'   => Fenix::getUrl('core/seo'),
                'id'    => 'seo'
            ),
            array(
                'label' => Fenix::lang("Новый seo шаблон"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');

        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Источник
        $Form      ->setSource('core/seo', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);

            $id = $Form ->addRecord(
                $this->getRequest()
            );

            Fenix::getModel('core/seo')->saveCategoriesRelations($id);
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("SEO шаблон создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/seo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/seo/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/seo/edit/id/' . $id);
            }
            
            Fenix::redirect('core/seo');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый SEO шаблон"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentSeo = Fenix::getModel('core/seo')->getSeoById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSeo == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("SEO шаблон не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/seo');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("SEO шаблоны"),
                'uri'   => Fenix::getUrl('core/seo'),
                'id'    => 'seo'
            ),
            array(
                'label' => Fenix::lang("Редактировать seo шаблон"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentSeo->toArray())
                   ->setData('current', $currentSeo);

        $attributeset = ($currentSeo->attributeset == null ? 'default' : $currentSeo->attributeset);

        // Источник
        $Form      ->setSource('core/seo', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentSeo, $this->getRequest());
            Fenix::getModel('core/seo')->saveCategoriesRelations($id);

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("SEO шаблон отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/seo');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/seo/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/seo/edit/id/' . $id);
            }
            
            Fenix::redirect('core/seo');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать seo шаблон"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentSeo = Fenix::getModel('core/seo')->getSeoById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentSeo == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("SEO шаблон не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/seo');
        }
        
        $Creator = Fenix::getCreatorUI();

        Fenix::getModel('core/seo')->removeCategoriesRelation($currentSeo->id);
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/seo', 'default')
                ->deleteRecord($currentSeo);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Seo шаблон удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/seo');
    }
}