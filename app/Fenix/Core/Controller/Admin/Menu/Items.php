<?php

class Fenix_Core_Controller_Admin_Menu_Items extends Fenix_Controller_Action
{
    protected $_menu;

    protected $helperMenu;

    public function preDispatch()
    {
        $this->helperMenu = Fenix::getHelper('core/menu_item');

        $this->getHelper('rules')->checkRedirect('contentAll');

        // Проверки
        $this->_menu = Fenix::getModel('core/backend_menu')->getMenuById(
            (int) $this->getRequest()->getParam('sid')
        );

        $Creator = Fenix::getCreatorUI();

        if ($this->_menu == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Меню не найдено"))
                ->saveSession();

            Fenix::redirect('core/menu');
        }

        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate('core/menu_item')
            ->prepare()
            ->execute();

        parent::preDispatch();
    }

    public function indexAction()
    {
        /** многоуровневое меню */
        $parentId = (int) $this->getRequest()->getParam('parent');

        $slideList = Fenix::getModel('core/backend_menu')->getItemsListAsSelect($this->_menu->id, $parentId);

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['menusList'])) {
                foreach ((array) $rows['menusList'] AS $_rowId) {
                    Fenix::getModel('core/backend_menu')->deleteItemById(
                        $_rowId
                    );
                }

                Fenix::redirect('core/menu/items/sid/' . $this->getRequest()->getParam('sid'));
            }
        }

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        if ($parentId) {
            $parentItem = Fenix::getModel('core/backend_menu')->getItemById($parentId);

            $slideList = Fenix::getModel('core/backend_menu')->getItemsListAsSelect($parentItem->parent, $parentItem->id);

            $navigation = Fenix::getModel('core/backend_menu')->getNavigation(
                $parentItem
            );
        }
        else {
            $parentId = 0;
        }


        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый пункт меню"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/menu/items/add/sid/' . $this->_menu->id) . '/parent/' . $parentId . '/\'')
                    ->fetch(),
                $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Назад"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/menu') . '\'')
                    ->fetch(),
            ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Меню / " . $this->_menu->title))
            ->setButtonset($Buttonset->fetch());


        // Хлебные крошки
        $_crumb = array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => Fenix::getUrl('core/menu'),
                'id'    => 'slider',
            ),
        );

        if (isset($navigation)) {
            $_crumb[] = array(
                'label' => $this->_menu->title,
                'uri'   => Fenix::getUrl('core/menu/sid/' . $this->_menu->id),
                'id'    => 'id_' . $this->_menu->id,
            );

            $count_nav = count($navigation);
            $navigation = array_reverse($navigation);

            foreach ($navigation AS $i => $_page) {
                if ( ! empty($_page)) {
                    if (($i + 1 < $count_nav)) {
                        $uri = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $_page->id);
                    }
                    else {
                        $uri = '';
                    }

                    $_crumb[] = array(
                        'label' => $_page->title,
                        'id'    => 'id_' . $_page->id,
                        'uri'   => $uri,
                    );
                }
            }
        }
        else {
            $_crumb[] = array(
                'label' => $this->_menu->title,
                'uri'   => '',
                'id'    => 'last',
            );
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        $menu_id = $this->_menu->id;
        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('menusList')
            ->setCheckAll()
            ->setTitle(Fenix::lang("Меню / " . $this->_menu->title))
            ->setData($slideList)
            ->setStandartButtonset()
            ->setCellCallback('title', function ($value, $data, $column, $table) use ($menu_id) {
                return '<a href="' . Fenix::getUrl('core/menu/items/sid/' . $menu_id . '/parent/' . $data->id) . '">' . $value . '</a>';
            });

        $Table->setCellCallback('image', function ($value, $data, $column, $table) {
            if ($data->image == null) {
                return;
            }

            $image = Fenix::getUploadImagePath($data->image);
            $url = Fenix_Image::resize($image, 100, 100);

            return '<img src="' . $url . '" width="50" alt="" />';
        });

        if (isset($parentItem) && $parentItem->id > 1) {
            if ($parentItem->parent != 0) {
                $Table->setUpLevel(Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parentItem->id));
            }
            else {
                $Table->setUpLevel(Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id));
            }
        }


        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/menu/items/edit/sid/' . $this->_menu->id . '/parent/' . $parentId . '/id/{$data->id}'),
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/menu/items/delete/sid/' . $this->_menu->id . '/parent/' . $parentId . '/id/{$data->id}'),
        ));

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Меню / " . $this->_menu->title));

        $Creator->setLayout()
            ->oneColumn(array(
                $Title->fetch(),
                $Event->fetch(),
                $Table->fetch('core/menu_item'),
            ));
    }

    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => Fenix::getUrl('core/menu'),
                'id'    => 'slider',
            ),
            array(
                'label' => $this->_menu->title,
                'uri'   => Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id),
                'id'    => 'menu',
            ),
            array(
                'label' => Fenix::lang("Добавить пункт меню"),
                'uri'   => '',
                'id'    => 'add',
            ),
        ));

        if ($this->getRequest()->getParam('parent')) {
            $parent_id = $this->getRequest()->getParam('parent');
        }
        else {
            $parent_id = $this->_menu->id;
        }

        if ($parent_id != $this->_menu->id) {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }
        else {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        // Url автоматом
        $url_key = $this->getRequest()->getPost('url_key');
        if (empty($url_key)) {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl(str_replace(' ', '-', $url_key)));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('menu', $this->_menu)
            ->setData('current', null)
            ->setData('button_beck', $buttonBecklink);

        // Источник
        $Form->setSource('core/menu_item', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {
            Fenix::dump($this->getRequest()->getPost());
            $this->getRequest()->setPost('parent', $parent_id);

            $id = Fenix::getModel('core/backend_menu')->addItem(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Пункт меню создан"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/menu/items/add/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/menu/items/edit/sid/' . $this->_menu->id . '/id/' . $id . '/parent/' . $parent_id);
            }

            Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новый пункт меню"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentSlide = Fenix::getModel('core/backend_menu')->getItemById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Пункт меню не найден"))
                ->saveSession();

            Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        if ($this->getRequest()->getParam('parent')) {
            $parent_id = $this->getRequest()->getParam('parent');
        }
        else {
            $parent_id = $this->_menu->id;
        }

        if ($parent_id != $this->_menu->id) {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }
        else {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        // Url автоматом
        $url_key = $this->getRequest()->getPost('url_key');
        if (empty($url_key)) {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl($url_key));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => Fenix::getUrl('core/menu'),
                'id'    => 'menus',
            ),
            array(
                'label' => $this->_menu->title,
                'uri'   => Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id),
                'id'    => 'menu',
            ),
            array(
                'label' => Fenix::lang("Редактировать пункт меню"),
                'uri'   => '',
                'id'    => 'edit',
            ),
        ));

        $Creator = Fenix::getCreatorUI();


        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentSlide->toArray())
            ->setData('current', $currentSlide)
            ->setData('menu', $this->_menu)
            ->setData('button_beck', $buttonBecklink);

        // Источник
        $Form->setSource('core/menu_item', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = Fenix::getModel('core/backend_menu')->editItem(
                $Form,
                $currentSlide,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Пункт меню отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/menu/items/add/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/menu/items/edit/sid/' . $this->_menu->id . '/id/' . $id . '/parent/' . $parent_id);
            }

            Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать пункт меню"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function deleteAction()
    {
        $currentMenu = Fenix::getModel('core/backend_menu')->getItemById(
            $this->getRequest()->getParam('id')
        );

        $Creator = Fenix::getCreatorUI();

        if ($currentMenu == null) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Элемент не найден"))
                ->saveSession();

            Fenix::redirect('core/menu/items/sid/' . $this->getRequest()->getParam('sid'));
        }

        $children = Fenix::getModel('core/backend_menu')->findChildren($currentMenu->id);

        if ($children->count() > 0) {
            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Пункт меню содержит дочерние элементы!"))
                ->saveSession();

            Fenix::redirect('core/menu/items/sid/' . $this->getRequest()->getParam('sid'));
        }

        $Creator->loadPlugin('Form_Generator')
            ->setSource('core/menu_item', 'default')
            ->deleteRecord($currentMenu);

        $Creator->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Элемент удалён"))
            ->saveSession();

        Fenix::redirect('core/menu/items/sid/' . $this->getRequest()->getParam('sid'));
    }
}