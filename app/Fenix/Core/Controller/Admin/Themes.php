<?php
class Fenix_Core_Controller_Admin_Themes extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemThemes');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/themes')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $blocksList = Fenix::getModel('core/themes')->getThemesListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый шаблон"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/themes/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-themes.png', 'core'))
                           ->setTitle(Fenix::lang("Шаблоны сайта"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны сайта"),
                'uri'   => Fenix::getUrl('core/themes'),
                'id'    => 'themes'
            )
        ));
        
        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('themessList')
                           ->setTitle(Fenix::lang("Шаблоны сайта"))
                           ->setData($blocksList)
                           ->setStandartButtonset();
        
        $Table   ->setCellCallback('is_default', function($value, $data, $column, $table){
            if ($value == '1')
                return Fenix::lang("Да");
                
            return Fenix::lang("Нет");
        });
        
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/themes/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/themes/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Шаблоны сайта"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/themes')
                 ));
    }

    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны сайта"),
                'uri'   => Fenix::getUrl('core/themes'),
                'id'    => 'themes'
            ),
            array(
                'label' => Fenix::lang("Новый шаблон"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/themes', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            Fenix::getModel('core/themes')->updateDefaultTheme($id, $this->getRequest()->getPost('is_default'));
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Шаблон сайта создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/themes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/themes/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/themes/edit/id/' . $id);
            }
            
            Fenix::redirect('core/themes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый themes шаблон"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentthemes = Fenix::getModel('core/themes')->getThemesById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentthemes == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("themes шаблон не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/themes');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны сайта"),
                'uri'   => Fenix::getUrl('core/themes'),
                'id'    => 'themes'
            ),
            array(
                'label' => Fenix::lang("Редактировать шаблон"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentthemes->toArray())
                   ->setData('current', $currentthemes);
        
        // Источник
        $Form      ->setSource('core/themes', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentthemes, $this->getRequest());
            
            Fenix::getModel('core/themes')->updateDefaultTheme($id, 
                $this->getRequest()->getPost('is_default')
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Шаблон сайта отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/themes');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/themes/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/themes/edit/id/' . $id);
            }
            
            Fenix::redirect('core/themes');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать шаблон сайта"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentTheme = Fenix::getModel('core/themes')->getThemesById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentTheme == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Шаблон сайта не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/themes');
        }
        
        if ($currentTheme->is_default == '1') {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Нельзя удалять тему по-умолчанию"))
                    ->saveSession();
            
            Fenix::redirect('core/themes');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/themes', 'default')
                ->deleteRecord($currentTheme);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Шаблон сайта удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/themes');
    }
}