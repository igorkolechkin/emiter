<?php
class Fenix_Core_Controller_Admin_Update extends Fenix_Controller_Action
{
    /**
     * Главная обновлений
     */
    public function indexAction()
    {
        $updatesList = Fenix::getModel('core/update')->getUpdates();
        $Table       = Fenix::getHelper('core/update')->getTable($updatesList);

        /*
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                ->setValue(Fenix::lang("Проверить"))
                ->setType('button')
                ->appendClass('btn-primary')
                ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/update/check') . '\'')
                ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Обновления системы"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Обновления системы"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Обновления системы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table
                 ));
    }

    /**
     * Проверка обновлений
     */
    public function checkAction()
    {
        Fenix::getModel('core/update')->checkUpdates();
        Fenix::redirect('core/update');
    }

    /**
     * Изменение парамена "Не напоминать"
     */
    public function remindAction()
    {
        Fenix::getModel('core/update')->remind(
            $this->getRequest()->getParam('id'),
            $this->getRequest()->getParam('disabled')
        );
        Fenix::redirect('core/update');
    }

    /**
     * Процесс обновления
     */
    public function processAction()
    {
        $result = Fenix::getModel('core/update')->process(
            $this->getRequest()->getParam('id')
        );

        if ($result !== false) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_INFO)
                ->setMessage(Fenix::lang("Система успешно обновлена"))
                ->saveSession();
        }
        Fenix::redirect('core/update');
    }
}