<?php
class Fenix_Core_Controller_Admin_Notifications extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/notifications')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $notificationsList = Fenix::getModel('core/notifications')->getNotificationsListAsSelect();

        $check = '
<script>
    function notifyMe() {
        if (!("Notification" in window)) {
            alert("Ваш браузер не поддерживает оповещения");
        }
        else if (Notification.permission === "granted") {
            var notification = new Notification("У вас настроены оповещения для этого браузера");
        }
        else if (Notification.permission !== "denied") {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    var notification = new Notification("Вы настроили оповещения для своего браузера");
                    setTimeout(function() {new Notification("Оформлен новый заказ");}, 1000);
                    setTimeout(function() {new Notification("Оформлен новый заказ");}, 2000);
                    setTimeout(function() {new Notification("Зарегистрирован новый пользователь");}, 2500);
                }
            });
        }
    }
</script>
        ';


        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 /*$Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый блок"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/notifications/add') . '\'')
                                         ->fetch(),*/
                                 $Creator->loadPlugin('Button')
                                     ->appendClass('btn-primary')
                                     ->setValue(Fenix::lang("Проверить оповещения"))
                                     ->setType('button')
                                     ->setOnclick('notifyMe()')
                                     ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-notifications.png', 'core'))
                           ->setTitle(Fenix::lang("Управление оповещениями"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление оповещениями"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('notificationsList')
                           ->setTitle(Fenix::lang("Управление оповещениями"))
                           ->setData($notificationsList)
                           ->setStandartButtonset()
                            ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/notifications/edit/id/{$data->id}')
        ));
        /*$Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/notifications/delete/id/{$data->id}')
        ));*/
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Группы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/notifications').$check
                 ));
    }

    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление оповещениями"),
                'uri'   => Fenix::getUrl('core/notifications'),
                'id'    => 'notifications'
            ),
            array(
                'label' => Fenix::lang("Новое оповещение"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/notifications', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));
            
            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Статический блок создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/notifications');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/notifications/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/notifications/edit/id/' . $id);
            }
            
            Fenix::redirect('core/notifications');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый статический блок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentNotification = Fenix::getModel('core/notifications')->getNotificationById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentNotification == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Статический блок не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/notifications');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление оповещениями"),
                'uri'   => Fenix::getUrl('core/notifications'),
                'id'    => 'notifications'
            ),
            array(
                'label' => Fenix::lang("Редактировать блок"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentNotification->toArray())
                   ->setData('current', $currentNotification);
        
        // Источник
        $Form      ->setSource('core/notifications', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));
                
            $id = $Form ->editRecord($currentNotification, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Статический блок отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/notifications');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/notifications/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/notifications/edit/id/' . $id);
            }
            
            Fenix::redirect('core/notifications');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать статический блок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        /*$currentNotification = Fenix::getModel('core/notifications')->getNotificationById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentNotification == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Статический блок не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/notifications');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/notifications', 'default')
                ->deleteRecord($currentNotification);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Статический блок удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/notifications');*/
    }
}