<?php
class Fenix_Core_Controller_Admin_Sitemap extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');
    }
    /**
     * Главная генерация карты сайта
     */
    public function indexAction()
    {
        ini_set('memory_limit','1024M');

        Fenix::getModel('core/sitemap')->generateSitemap();

        /*
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events')->appendClass('alert-success')->setMessage("Вы успешно сгерненировали карту сайта");
        $Info      = $Creator->loadPlugin('Events')->appendClass('alert-info')->setMessage("При просмотре новой карты сайта в браузере может отображаться ранее созданная.  Это кеш браузера, достаточно нажать комбинацию клавиш ctrl+R для обновления кеша карты сайта");

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("Генерация карты сайта"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Генерация карты сайта"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Тайтл страницы
        $Creator ->getView()
            ->headTitle(Fenix::lang("Генерация карты сайта"));

        // Рендер страницы
        $Creator ->setLayout()
            ->oneColumn(array(
                $Title->fetch(),
                $Event->fetch(),
                $Info->fetch()
            ));
    }

    /**
     * Проверка обновлений
     */
    public function checkAction()
    {
        Fenix::getModel('core/update')->checkUpdates();
        Fenix::redirect('core/update');
    }

    /**
     * Изменение парамена "Не напоминать"
     */
    public function remindAction()
    {
        Fenix::getModel('core/update')->remind(
            $this->getRequest()->getParam('id'),
            $this->getRequest()->getParam('disabled')
        );
        Fenix::redirect('core/update');
    }

    /**
     * Процесс обновления
     */
    public function processAction()
    {
        $result = Fenix::getModel('core/update')->process(
            $this->getRequest()->getParam('id')
        );

        if ($result !== false) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_INFO)
                ->setMessage(Fenix::lang("Система успешно обновлена"))
                ->saveSession();
        }
        Fenix::redirect('core/update');
    }
}