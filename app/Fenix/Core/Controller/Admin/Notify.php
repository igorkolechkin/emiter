<?php
class Fenix_Core_Controller_Admin_Notify extends Fenix_Controller_Action
{
    /**
     * Лента уведомлений
     */
    public function indexAction()
    {
    	$notifyList = Fenix::getModel('core/notify')->prepareForCreator();
    	
        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Уведомления"),
                'uri'   => Fenix::getUrl('core/notify'),
                'id'    => 'mail'
            )
        ));

        // Событие
        $Event   = $Creator->loadPlugin('Events_Session');

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Уведомления"));

        $Feed    = $Creator->loadPlugin('Feed');
        $Feed    ->addItems($notifyList);

        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Уведомления"));

        // Рендер страницы
        $Creator ->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Feed->fetch()
        ));
    }
}