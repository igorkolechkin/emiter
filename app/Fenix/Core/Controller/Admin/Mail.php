<?php
class Fenix_Core_Controller_Admin_Mail extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemMail');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/mail')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $blocksList = Fenix::getModel('core/mail')->getMailListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны писем"),
                'uri'   => Fenix::getUrl('core/mail'),
                'id'    => 'mail'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue(Fenix::lang("Новый шаблон письма"))
                                         ->setType('button')
                                         ->appendClass('btn-primary')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/mail/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-mail.png', 'core'))
                           ->setTitle(Fenix::lang("Шаблоны писем"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('mailsList')
                           ->setTitle(Fenix::lang("Шаблоны писем"))
                           ->setData($blocksList)
                           ->setStandartButtonset();
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/mail/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/mail/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Шаблоны писем"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/mail')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны писем"),
                'uri'   => Fenix::getUrl('core/mail'),
                'id'    => 'mail'
            ),
            array(
                'label' => Fenix::lang("Новый шаблон"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/mail', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Шаблон письма создан"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/mail');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/mail/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/mail/edit/id/' . $id);
            }
            
            Fenix::redirect('core/mail');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый шаблон письма"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentMail = Fenix::getModel('core/mail')->getMailById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentMail == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Шаблон письма не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/mail');
        }
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Шаблоны писем"),
                'uri'   => Fenix::getUrl('core/mail'),
                'id'    => 'mail'
            ),
            array(
                'label' => Fenix::lang("Редактировать шаблон"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentMail->toArray())
                   ->setData('current', $currentMail);
        
        // Источник
        $Form      ->setSource('core/mail', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentMail, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Шаблон письма отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/mail');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/mail/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/mail/edit/id/' . $id);
            }
            
            Fenix::redirect('core/mail');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать шаблон письма"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentMail = Fenix::getModel('core/mail')->getMailById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentMail == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Шаблон письма не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/mail');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/mail', 'default')
                ->deleteRecord($currentMail);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Шаблон письма удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/mail');
    }
}