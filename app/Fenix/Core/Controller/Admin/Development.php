<?php
class Fenix_Core_Controller_Admin_Development extends Fenix_Controller_Action
{
    public function indexAction()
    {
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-denied.png', 'core'))
                           ->setTitle(Fenix::lang("Идет разработка"));
        
        $Error   = $Creator->loadPlugin('Events')
                           ->appendClass(Creator_Events::TYPE_INFO)
                           ->setMessage("Внимание! Сейчас ведутся работы над системой.");
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Идет разработка"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Error->fetch()
                 ));
    }
}