<?php
class Fenix_Core_Controller_Admin_Currency extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemCurrency');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/currency')
                ->prepare()
                ->execute();
    }

    /**
     * Контроллер управления валютами
     */
    public function indexAction()
    {
        $currencyList = Fenix::getModel('core/currency')->getCurrencyListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Валюты"),
                'uri'   => Fenix::getUrl('core/currency'),
                'id'    => 'currency'
            )
        ));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Синхронизировать"))
                    ->setType('button')
                    ->setTitle(Fenix::lang('C Нацбанком Украины'))
                    ->appendClass('btn-info')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/currency/sync') . '\'')
                    ->fetch(),
            $Creator->loadPlugin('Button')
                    ->setValue(Fenix::lang("Добавить валюту"))
                    ->setType('button')
                    ->appendClass('btn-primary')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/currency/add') . '\'')
                    ->fetch()
        ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Валюты"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('cL')
                           ->setTitle(Fenix::lang("Валюты"))
                           ->setData($currencyList)
                           ->setStandartButtonset();

        $Table   ->setCellCallback('image', function($value, $data, $column, $table){
            if ($data->image == null)
                return;
            $info     = (object)unserialize($data->image_info);
            $imageUrl = Fenix::createImageFromStreamInfo($data->image, $info);
            return '<img src="' . $imageUrl . '" width="50" alt="" />';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/currency/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/currency/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Валюты"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/currency')
                 ));
    }

    /**
     * Новая валюта
     */
    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Валюты"),
                'uri'   => Fenix::getUrl('core/currency'),
                'id'    => 'currency'
            ),
            array(
                'label' => Fenix::lang("Новая соцсеть"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/currency', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Валюта добавлена"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/currency');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/currency/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/currency/edit/id/' . $id);
            }
            
            Fenix::redirect('core/currency');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая валюта"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    /**
     * Редактировать валюту
     */
    public function editAction()
    {
        $currentCurrency = Fenix::getModel('core/currency')->getCurrencyById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentCurrency == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Валюта не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/currency');
        }
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Валюты"),
                'uri'   => Fenix::getUrl('core/currency'),
                'id'    => 'currency'
            ),
            array(
                'label' => Fenix::lang("Редактировать соцсеть"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentCurrency->toArray())
                   ->setData('current', $currentCurrency);
        
        // Источник
        $Form      ->setSource('core/currency', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentCurrency, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Валюта отредактирована"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/currency');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/currency/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/currency/edit/id/' . $id);
            }
            
            Fenix::redirect('core/currency');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать валюту"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }

    /**
     * Удалить валюту
     */
    public function deleteAction()
    {
        $currentCurrency = Fenix::getModel('core/currency')->getSocialById(
            $this->getRequest()->getParam('id')
        );

        if ($currentCurrency == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Валюта не найдена"))
                ->saveSession();

            Fenix::redirect('core/currency');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('core/currency', 'default')
                 ->deleteRecord($currentCurrency);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Валюта удалена"))
                 ->saveSession();
            
        Fenix::redirect('core/currency');
    }

    /**
     * Синхронизация с Нацбанком Украины
     */
    public function syncAction()
    {
        Fenix::getModel('core/currency')->sync();

        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Курсы синхронизированы"))
                 ->saveSession();

        Fenix::redirect('core/currency');
    }
}