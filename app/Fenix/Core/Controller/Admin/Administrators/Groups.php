<?php
class Fenix_Core_Controller_Admin_Administrators_Groups extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemAdminAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/groups')
                ->prepare()
                ->execute();
        
        $Engine ->setDatabaseTemplate('core/groups_rules')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $groupsList = Fenix::getModel('core/groups')->getGroupsListAsSelect();
        
        /**
         * Отображение
         */

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => Fenix::getUrl('core/administrators'),
                'id'    => 'administrators'
            ),
            array(
                'label' => Fenix::lang("Группы"),
                'uri'   => '',
                'id'    => 'groups'
            )
        ));

        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-inverse')
                                         ->setValue(Fenix::lang("Назад"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/administrators') . '\'')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Добавить группу"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/administrators/groups/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-groups.png', 'core'))
                           ->setTitle(Fenix::lang("Группы"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('adminList')
                           ->setTitle(Fenix::lang("Управление группами"))
                           ->setData($groupsList)
                           ->setStandartButtonset()
                            ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/administrators/groups/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/administrators/groups/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Группы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/groups')
                 ));
    }
    
    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => Fenix::getUrl('core/administrators'),
                'id'    => 'administrators'
            ),
            array(
                'label' => Fenix::lang("Группы"),
                'uri'   => Fenix::getUrl('core/administrators/groups'),
                'id'    => 'groups'
            ),
            array(
                'label' => Fenix::lang("Создать"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/groups', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            $id = Fenix::getModel('core/groups')->addGroup(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Группа создана"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/administrators/groups');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/administrators/groups/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/administrators/groups/edit/id/' . $id);
            }
            
            Fenix::redirect('core/administrators/groups');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый группа"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        // Проверки
        $group = Fenix::getModel('core/groups')->getGroupById(
            (int) $this->getRequest()->getParam('id')
        );
        
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Администраторы системы"),
                'uri'   => Fenix::getUrl('core/administrators'),
                'id'    => 'administrators'
            ),
            array(
                'label' => Fenix::lang("Группы"),
                'uri'   => Fenix::getUrl('core/administrators/groups'),
                'id'    => 'groups'
            ),
            array(
                'label' => Fenix::lang("Редактировать"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        if ($group == null) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Группа не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators/groups');
        }
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        $Form      ->setDefaults($group->toArray())
                   ->setData('current', $group);
        
        // Источник
        $Form      ->setSource('core/groups', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            Fenix::getModel('core/groups')->editGroup(
                $group,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Группа отредактирована"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/administrators/groups');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/administrators/groups/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/administrators/groups/edit/id/' . $group->id);
            }
            
            Fenix::redirect('core/administrators/groups');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактирование группы"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }

    public function deleteAction()
    {
        // Проверки
        $group = Fenix::getModel('core/groups')->getGroupById(
            (int) $this->getRequest()->getParam('id')
        );
        
        $Creator = Fenix::getCreatorUI();
        
        if ($group == null) {
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Группа не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/administrators/groups');
        }
        
        Fenix::getModel('core/groups')->deleteGroup(
            $group
        );
        
        $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Группа удалёна"))
                ->saveSession();

        Fenix::redirect('core/administrators/groups');
    }
}