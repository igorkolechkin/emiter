<?php
class Fenix_Core_Controller_Admin_Utp extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/utp')
                ->prepare()
                ->execute();

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/utp_content')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $utpsList = Fenix::getModel('core/utp')->getUtpListAsSelect();
        
        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новый блок"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/utp/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("УТП"))
                           ->setDetails(Fenix::lang("Можно использовать для создания различных УТП блоков на разных страницах сайта"))
                           ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('utpsList')
                           ->setTitle(Fenix::lang("УТП"))
                           ->setData($utpsList)
                           ->setStandartButtonset();

        $Table   ->setCellCallback('title', function($value, $data, $column, $table){
            return '<a href="' . Fenix::getUrl('core/utp/content/sid/' . $data->id) . '">' . $value . '</a>';
        });

        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/utp/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/utp/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Сладеры"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/utp')
                 ));
    }

    public function addAction()
    {
        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => Fenix::getUrl('core/utp'),
                'id'    => 'utp'
            ),
            array(
                'label' => Fenix::lang("Новый блок"),
                'uri'   => '',
                'id'    => 'add'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/utp', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("УТП создано"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/utp');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/utp/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/utp/edit/id/' . $id);
            }
            
            Fenix::redirect('core/utp');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новый блок"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentUtp = Fenix::getModel('core/utp')->getUtpById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentUtp == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("УТП не найден"))
                    ->saveSession();
            
            Fenix::redirect('core/utp');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => Fenix::getUrl('core/utp'),
                'id'    => 'utp'
            ),
            array(
                'label' => Fenix::lang("Изменить УТП"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentUtp->toArray())
                   ->setData('current', $currentUtp);
        
        // Источник
        $Form      ->setSource('core/utp', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $id = $Form ->editRecord($currentUtp, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("УТП отредактирован"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/utp');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/utp/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/utp/edit/id/' . $id);
            }
            
            Fenix::redirect('core/utp');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать УТП"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentUtp = Fenix::getModel('core/utp')->getUtpById(
            $this->getRequest()->getParam('id')
        );

        if ($currentUtp == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("УТП не найден"))
                ->saveSession();

            Fenix::redirect('core/utp');
        }

        $slideList = Fenix::getModel('core/utp')->getBlockList($currentUtp->id);

        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/utp', 'default')
                ->deleteRecord($currentUtp);

        foreach ($slideList AS $_slide) {
            $Creator->loadPlugin('Form_Generator')
                    ->setSource('core/utp_content', 'default')
                    ->deleteRecord($_slide);
        }

        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("УТП удалён"))
                 ->saveSession();
            
        Fenix::redirect('core/utp');
    }
}