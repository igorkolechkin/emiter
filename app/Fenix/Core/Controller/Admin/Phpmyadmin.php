<?php
class Fenix_Core_Controller_Admin_Phpmyadmin extends Fenix_Controller_Action
{
    public function preDispatch() {
        $this->getHelper('rules')->checkDevRedirect();
    }

    public function indexAction()
    {
        $dbConfig = Zend_Registry::get('db')->getConfig();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Управление базой данных"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();

        // Форма
        $Form       = $Creator->loadPlugin('Form');
        $Form       ->setAction('/var/phpmyadmin/index.php')
                    ->setTarget('_blank');
        
        $Form ->setDefaults(array(
            'pma_username' => $dbConfig['username'],
            'pma_password' => $dbConfig['password']
        ));
        
        $Form       ->setContent('<input type="hidden" name="pma_username" value="' . $this->getRequest()->getPost('pma_username') . '" />')
                    ->setContent('<input type="hidden" name="pma_password" value="' . $this->getRequest()->getPost('pma_password') . '" />');
        
        // Кнопули
        $Buttonset  = $Creator->loadPlugin('Buttonset')
                              ->setContent(array(
                                  $Creator->loadPlugin('Button')
                                          ->appendClass('btn-primary')
                                          ->setValue("Перейти к администрированию")
                                          ->setType('submit')
                                          ->fetch()
                              ));

        // Заголовок формы
        $Title    = $Creator->loadPlugin('Title')
                            ->setImage(Fenix::getAppEtcUrl('icons/icon-database.png', 'core'))
                            ->setTitle(Fenix::lang("Управление базой данных"));
        $Form     ->setContent($Title->fetch());

        $Form      ->setContent($Buttonset->fetch());
        
        // Компиляция
        $Form      ->compile();
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Управление базой данных"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Form->fetch()
                 ));
    }
}