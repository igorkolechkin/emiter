<?php

class Fenix_Core_Controller_Admin_Menu extends Fenix_Controller_Action {

	public function preDispatch() {
		$this->getHelper('rules')->checkRedirect('contentAll');

		$Engine = new Fenix_Engine_Database();
		$Engine->setDatabaseTemplate('core/menu')->prepare()->execute();
	}

	public function indexAction() {
		$slidersList = Fenix::getModel('core/menu')->getMenuListAsSelect();

		/**
		 * Отображение
		 */
		$Creator = Fenix::getCreatorUI();

		// Событие
		$Event = $Creator->loadPlugin('Events_Session');

		// Кнопули
		$Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
				$Creator->loadPlugin('Button')->appendClass('btn-primary')->setValue(Fenix::lang("Новое меню"))->setType('button')->setOnclick('self.location.href=\'' . Fenix::getUrl('core/menu/add') . '\'')->fetch(),
			));

		// Заголовок страницы
		$Title = $Creator->loadPlugin('Title')->setTitle(Fenix::lang("Управление меню"))->setButtonset($Buttonset->fetch());

		// Хлебные крошки
		$this->_helper->BreadCrumbs(array(
			array(
				'label' => Fenix::lang("Панель управления"), 'uri' => Fenix::getUrl(), 'id' => 'main',
			), array(
				'label' => Fenix::lang("Управление меню"), 'uri' => '', 'id' => 'last',
			),
		));

		// Таблица
		$Table = $Creator->loadPlugin('Table_Db_Generator')->setTableId('menusList')->setTitle(Fenix::lang("Управление меню"))->setData($slidersList)->setStandartButtonset();

		$Table->setCellCallback('title', function($value, $data, $column, $table) {
			return '<a href="' . Fenix::getUrl('core/menu/items/sid/' . $data->id) . '">' . $value . '</a>';
		});

		$Table->addAction(array(
			'type' => 'link', 'icon' => 'edit', 'title' => Fenix::lang("Редактировать"),
			'url'  => Fenix::getUrl('core/menu/edit/id/{$data->id}'),
		));
		$Table->addAction(array(
			'type' => 'confirm', 'icon' => 'trash', 'title' => Fenix::lang("Удалить"),
			'url'  => Fenix::getUrl('core/menu/delete/id/{$data->id}'),
		));

		// Тайтл страницы
		$Creator->getView()->headTitle(Fenix::lang("Управление меню"));

		// Рендер страницы
		$Creator->setLayout()->oneColumn(array(
				$Title->fetch(), $Event->fetch(), $Table->fetch('core/menu'),
			));
	}

	public function addAction() {
		// Хлебные крошки
		$this->_helper->BreadCrumbs(array(
			array(
				'label' => Fenix::lang("Панель управления"), 'uri' => Fenix::getUrl(), 'id' => 'main',
			), array(
				'label' => Fenix::lang("Управление меню"), 'uri' => Fenix::getUrl('core/menu'), 'id' => 'menu',
			), array(
				'label' => Fenix::lang("Новое меню"), 'uri' => '', 'id' => 'add',
			),
		));

		$Creator = Fenix::getCreatorUI();

		// Форма
		$Form = $Creator->loadPlugin('Form_Generator');

		// Источник
		$Form->setSource('core/menu', 'default')->renderSource();

		// Компиляция
		$Form->compile();

		if($Form->ok()) {

			$id = $Form->addRecord($this->getRequest());

			$Creator->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_OK)->setMessage(Fenix::lang("Меню создано"))->saveSession();

			if($this->getRequest()->getPost('save')) {
				Fenix::redirect('core/menu');
			} elseif($this->getRequest()->getPost('save_add')) {
				Fenix::redirect('core/menu/add');
			} elseif($this->getRequest()->getPost('apply')) {
				Fenix::redirect('core/menu/edit/id/' . $id);
			}

			Fenix::redirect('core/menu');
		}

		// Тайтл страницы
		$Creator->getView()->headTitle(Fenix::lang("Новое меню"));

		$Creator->setLayout()->oneColumn($Form->fetch());
	}

	public function editAction() {
		$currentSlider = Fenix::getModel('core/menu')->getMenuById($this->getRequest()->getParam('id'));

		if($currentSlider == null) {
			Fenix::getCreatorUI()->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_ERROR)->setMessage(Fenix::lang("Меню не найдено"))->saveSession();

			Fenix::redirect('core/menu');
		}

		// Хлебные крошки
		$this->_helper->BreadCrumbs(array(
			array(
				'label' => Fenix::lang("Панель управления"), 'uri' => Fenix::getUrl(), 'id' => 'main',
			), array(
				'label' => Fenix::lang("Управление меню"), 'uri' => Fenix::getUrl('core/menu'), 'id' => 'menu',
			), array(
				'label' => Fenix::lang("Изменить меню"), 'uri' => '', 'id' => 'edit',
			),
		));

		$Creator = Fenix::getCreatorUI();

		// Форма
		$Form = $Creator->loadPlugin('Form_Generator');

		$Form->setDefaults($currentSlider->toArray())->setData('current', $currentSlider);

		// Источник
		$Form->setSource('core/menu', 'default')->renderSource();

		// Компиляция
		$Form->compile();

		if($Form->ok()) {

			$id = $Form->editRecord($currentSlider, $this->getRequest());

			$Creator->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_OK)->setMessage(Fenix::lang("Слайдер отредактирован"))->saveSession();

			if($this->getRequest()->getPost('save')) {
				Fenix::redirect('core/menu');
			} elseif($this->getRequest()->getPost('save_add')) {
				Fenix::redirect('core/menu/add');
			} elseif($this->getRequest()->getPost('apply')) {
				Fenix::redirect('core/menu/edit/id/' . $id);
			}

			Fenix::redirect('core/menu');
		}

		// Тайтл страницы
		$Creator->getView()->headTitle(Fenix::lang("Редактировать меню"));

		$Creator->setLayout()->oneColumn($Form->fetch());
	}

	public function deleteAction() {
		$currentSlider = Fenix::getModel('core/menu')->getMenuById($this->getRequest()->getParam('id'));

		if($currentSlider == null) {
			Fenix::getCreatorUI()->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_ERROR)->setMessage(Fenix::lang("Меню не найдено"))->saveSession();

			Fenix::redirect('core/menu');
		}

		$slideList = Fenix::getModel('core/menu')->getItemsList($currentSlider->id);

		$Creator = Fenix::getCreatorUI();

		$Creator->loadPlugin('Form_Generator')->setSource('core/menu', 'default')->deleteRecord($currentSlider);

		foreach($slideList AS $_slide) {
			$Creator->loadPlugin('Form_Generator')->setSource('core/menu_item', 'default')->deleteRecord($_slide);
		}

		$Creator->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_OK)->setMessage(Fenix::lang("Меню удалёно"))->saveSession();

		Fenix::redirect('core/menu');
	}
}