<?php
class Fenix_Core_Controller_Admin_Structure extends Fenix_Controller_Action
{
    use Fenix_Traits_ImageInfoCollection;

    private $_engine = null;

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('contentAll');

        $this->_engine = new Fenix_Engine_Database();
        $this->_engine ->setDatabaseTemplate('core/structure');

        $this->_engine->prepare()
                      ->execute();
    }
    
    public function indexAction()
    {
        if ($this->getRequest()->getParam('parent') == null) {
            Fenix::redirect('core/structure/parent/1');
        }

        if ($rows = $this->getRequest()->getQuery('row')) {
            if (isset($rows['pagesList'])) {
                foreach ((array) $rows['pagesList'] AS $_rowId) {
                    $currentPage = Fenix::getModel('core/structure')->getPageById(
                        $_rowId
                    );
                    Fenix::getModel('core/structure')->deletePage(
                        $currentPage
                    );
                }
                
                Fenix::redirect('core/structure/parent/' . $this->getRequest()->getParam('parent'));
            }
        }

        $parentId   = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);
        $pagesList  = Fenix::getModel('core/structure')->getPagesListAsSelect($parentId);

        /**
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Окно с набором атрибутов
        $Dialog    = $Creator->loadPlugin('Dialog');
        $Dialog    ->setTitle(Fenix::lang("Выберите набор атрибутов"))->setContent($this->_engine->getAttributesetListFormatted(array(
            'url' => Fenix::getUrl('core/structure/add/parent/' . $parentId . '/attributeset/{$attributeset}')
        )));

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новая страница"))
                                         ->setType('button')
                                         ->setOnclick(($this->_engine->getAttributesetList()->count() == '1' ? 'self.location.href=\'' . Fenix::getUrl('core/structure/add/parent/' . $parentId) . '\'' : $Dialog->toButton()))
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Статические страницы"))
                           ->setButtonset($Buttonset->fetch());
        
        $navigation = Fenix::getModel('core/structure')->getNavigation(
            $parentPage
        );

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Страницы"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );

        foreach ($navigation AS $i => $_page) {
            $_crumb[] = array(
                'label' => $_page->title,
                'id'    => 'id_' . $_page->id,
                'uri'   => ($navigation->count() > 0 ? Fenix::getUrl('core/structure/parent/1') : null)
            );
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);

        $jTree = $Creator->loadPlugin('JTree');
        $jTree ->setId('structure_tree')
               ->setPlugins(array('themes', 'json_data', 'ui', 'unique'))
               ->setTable('structure')
               ->setUrl(Fenix::getUrl('core/structure/parent/{$node->id}'))
               ->setEditUrl(Fenix::getUrl('core/structure/edit/id/{$node->id}'))
               ->setInitOpen($parentId);
        
        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('pagesList')
                           ->setTitle(Fenix::lang("Управление статическими страницами"))
                           ->setData($pagesList)
                           ->setCheckall()
                           ->setStandartButtonset()
                           ->setCellCallback('image', function($value, $data, $column, $table){
                               if ($data->image == null)
                                   return;

                                $info     = (object)unserialize($data->image_info);
                               $imageUrl = Fenix::createImageFromStreamInfo($data->image, $info);
                               
                               return '<img src="' . $imageUrl . '" width="50" alt="" />';
                           })
                           ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('title', function($value, $data, $column, $table){
                               return '<a href="' . Fenix::getUrl('core/structure/parent/' . $data->id) . '">' . $value . '</a>';
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });
                           
        if ($parentPage->id > 1)
            $Table ->setUpLevel(Fenix::getUrl('core/structure/parent/' . $parentPage->parent));
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/structure/edit/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить. Будут удалены все дочерние страницы."),
            'url'   => Fenix::getUrl('core/structure/delete/id/{$data->id}/parent/{$data->parent}')
        ));
        $Table ->addAction(array(
            'type'      => 'sorting',
            'options'   => array(
                'html' => 'text'
            )
        ));
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Статические страницы"));
        
        $Creator ->setLayout()->twoColumnsLeft(array(
            $Title->fetch(),
            $Event->fetch(),
            $Dialog->fetch()
        ), array(
            $Creator->loadPlugin('Block')->setTitle(Fenix::lang("Дерево страниц"))->setContent($jTree->fetch())
        ), array(
            $Table->fetch('core/structure')
        ));
    }
    
    public function addAction()
    {
        // Тестирование родительской страницы
        $parentId   = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);
        
        if ($parentPage == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Родительская страница не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/structure/parent/1');
        }

        $navigation = Fenix::getModel('core/structure')->getNavigation(
            $parentPage
        );

        // Хлебные крошки
        $_crumb   = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Страницы"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );

        foreach ($navigation AS $i => $_page) {
            $_crumb[] = array(
                'label' => $_page->title,
                'id'    => 'id_' . $_page->id,
                'uri'   => ($navigation->count() > 0 ? Fenix::getUrl('core/structure/parent/1') : null)
            );
        }

        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Работа с формой
        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setData('current',    null)
                   ->setData('parentPage', $parentPage);
        
        // Источник
        $Form      ->setSource('core/structure', $attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id',    Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date',  date('Y-m-d H:i:s'));
            $this->getRequest()->setPost('parent',       $parentPage->id);
            
            $id = Fenix::getModel('core/structure')->addPage(
                $Form,
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Страница создана"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/structure/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/structure/add/attributeset/' . $attributeset . '/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/structure/edit/attributeset/' . $attributeset . '/id/' . $id . '/parent/' . $parentPage->id);
            }
            
            Fenix::redirect('core/structure/attributeset/' . $attributeset . '/parent/' . $parentPage->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новая статическая страница"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        // Тестирование родительской страницы
        $parentId   = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);

        if ($parentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/1');
        }

        // Редактируемая страница
        $currentPage = Fenix::getModel('core/structure')->getPageById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentPage == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Выбранная Вами страница не найдена"))
                    ->saveSession();
            
            Fenix::redirect('core/structure/parent/' . $parentPage->id);
        }
        
        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentPage->toArray())
                   ->setData('current',    $currentPage)
                   ->setData('parentPage', $parentPage);
        
        // Источник
        $Form      ->setSource('core/structure', $currentPage->attributeset)
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $id = Fenix::getModel('core/structure')->editPage(
                $Form,
                $currentPage,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Статическая страница изменена"))
                    ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/structure/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/structure/add/attributeset/' . $currentPage->attributeset . '/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/structure/edit/attributeset/' . $currentPage->attributeset . '/id/' . $id . '/parent/' . $parentPage->id);
            }

            Fenix::redirect('core/structure/attributeset/' . $currentPage->attributeset . '/parent/' . $parentPage->id);
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать статическую страницу"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        // Тестирование родительской страницы
        $parentId   = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);

        if ($parentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/1');
        }

        // Редактируемая страница
        $currentPage = Fenix::getModel('core/structure')->getPageById(
            $this->getRequest()->getParam('id')
        );

        if ($currentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/' . $parentPage->id);
        }
        
        Fenix::getModel('core/structure')->deletePage(
            $currentPage
        );
        
        Fenix::getCreatorUI()
            ->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage(Fenix::lang("Вы успешно удалили статическую страницу"))
            ->saveSession();

        Fenix::redirect('core/structure/parent/' . $parentPage->id);
    }
}