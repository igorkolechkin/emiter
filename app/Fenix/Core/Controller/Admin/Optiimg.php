<?php
class Fenix_Core_Controller_Admin_Optiimg extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        //$this->getHelper('rules')->checkRedirect('contentAll');

        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/optiimg')
            ->prepare()
            ->execute();
    }
    /**
     * Оптимизация изображений
     */
    public function indexAction()
    {
        ini_set('memory_limit','1024M');

        /*
         * Отображение
         */
        $Creator   = Fenix::getCreatorUI();

        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setTitle(Fenix::lang("Оптимизация изображений"));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Оптимизация изображений"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        $files = Fenix::getModel('core/optiimg')->getFiles();
        $files2 = Fenix::getModel('core/optiimg')->getFiles(VAR_DIR_ABSOLUTE.'upload/',VAR_DIR_URL.'upload/');


        $files = array_merge($files,$files2);

        //Fenix::dump($files);

        $Form = Fenix::getHelper('core/optiimg')->getForm('');


        if($files==null){
            $Form = 'У вас нет не оптимизированных изображений';
        }
        else{
            if($Form->ok()){
                foreach($files as $i => $file){
                    if($i==100){
                        break;
                    }
                    if($file['type']=='jpg'){
                        Fenix::getModel('core/optiimg')->optiJPG($file);
                    }
                    else{
                        Fenix::getModel('core/optiimg')->optiPNG($file);
                    }
                }
                $url = Fenix::getUrl('core/optiimg');
                print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                exit;
            }
            elseif(Fenix::getRequest()->getParam('mode')=='package'){
                foreach($files as $i => $file){
                    if($i==100){
                        $url = Fenix::getUrl('core/optiimg/mode/package');
                        print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                        print 'Подождите... Осталось оптимизировать еще '.(count($files)-$i).' Изображений';
                        exit;
                    }
                    if($file['type']=='jpg'){
                        Fenix::getModel('core/optiimg')->optiJPG($file);
                    }
                    else{
                        Fenix::getModel('core/optiimg')->optiPNG($file);
                    }
                }
                $url = Fenix::getUrl('core/optiimg');
                print '<meta http-equiv="Refresh" content="0; url=' . $url . '">';
                exit;
            }
            $Form = $Form->fetch();
            $Form .= '<br/>У вас '.count($files).' не оптимизированных изображений';
        }


        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Оптимизация изображений"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Form
                 ));
    }

    /**
     * Сохранение
     */
    public function saveAction()
    {
    }

}