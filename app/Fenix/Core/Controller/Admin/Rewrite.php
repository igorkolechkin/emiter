<?php
class Fenix_Core_Controller_Admin_Rewrite extends Fenix_Controller_Action
{
    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('systemUrlRewrite');
        $Engine = new Fenix_Engine_Database();
        $Engine ->setDatabaseTemplate('core/rewrite')
                ->prepare()
                ->execute();
    }
    
    public function indexAction()
    {
        $rewriteList = Fenix::getModel('core/rewrite')->getRewriteListAsSelect();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Переопределение URL"),
                'uri'   => '',
                'id'    => 'rewrite'
            )
        ));

        /**
         * Отображение
         */     
        $Creator   = Fenix::getCreatorUI();
        
        // Событие
        $Event     = $Creator->loadPlugin('Events_Session');
        
        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->appendClass('btn-primary')
                                         ->setValue(Fenix::lang("Новое правило"))
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/rewrite/add') . '\'')
                                         ->fetch()
                             ));

        // Заголовок страницы
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-rewrite.png', 'core'))
                           ->setTitle(Fenix::lang("Правила перезаписи URL"))
                           ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table   = $Creator->loadPlugin('Table_Db_Generator')
                           ->setTableId('rewriteList')
                           ->setTitle(Fenix::lang("Правила перезаписи URL"))
                           ->setData($rewriteList)
                           ->setStandartButtonset()
                            ->setCellCallback('create_date', function($value, $data, $column, $table){
                               return Fenix::getDate($value)->format('d.m.Y H:i:s');
                           })
                           ->setCellCallback('modify_date', function($value, $data, $column, $table){
                               if ($value != '0000-00-00 00:00:00')
                                   return Fenix::getDate($value)->format('d.m.Y H:i:s');
                               return;
                           });
                           
        $Table ->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/rewrite/edit/id/{$data->id}')
        ));
        $Table ->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/rewrite/delete/id/{$data->id}')
        ));
               
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Группы"));
        
        // Рендер страницы
        $Creator ->setLayout()
                 ->oneColumn(array(
                     $Title->fetch(),
                     $Event->fetch(),
                     $Table->fetch('core/rewrite')
                 ));
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Переопределение URL"),
                'uri'   => Fenix::getUrl('core/rewrite'),
                'id'    => 'rewrite'
            ),
            array(
                'label' => Fenix::lang("Новое правило"),
                'uri'   => '',
                'id'    => 'rewrite'
            )
        ));

        // Форма
        $Form       = $Creator->loadPlugin('Form_Generator');
        
        // Источник
        $Form      ->setSource('core/rewrite', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {

            $this->getRequest()->setPost('create_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));
            
            $id = $Form ->addRecord(
                $this->getRequest()
            );
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Правило перезаписи создано"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/rewrite');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/rewrite/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/rewrite/edit/id/' . $id);
            }
            
            Fenix::redirect('core/rewrite');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Новое правило перезаписи URL"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());
    }
    
    public function editAction()
    {
        $currentRewrite = Fenix::getModel('core/rewrite')->getRewriteById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentRewrite == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Правило перезаписи не найдено"))
                    ->saveSession();
            
            Fenix::redirect('core/rewrite');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("Переопределение URL"),
                'uri'   => Fenix::getUrl('core/rewrite'),
                'id'    => 'rewrite'
            ),
            array(
                'label' => Fenix::lang("Редактировать правило"),
                'uri'   => '',
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        // Форма
        $Form      = $Creator->loadPlugin('Form_Generator');
        
        $Form      ->setDefaults($currentRewrite->toArray())
                   ->setData('current', $currentRewrite);
        
        // Источник
        $Form      ->setSource('core/rewrite', 'default')
                   ->renderSource();
        
        // Компиляция
        $Form      ->compile();
        
        if ($Form->ok()) {
            
            $this->getRequest()->setPost('modify_id',   Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));
                
            $id = $Form ->editRecord($currentRewrite, $this->getRequest());
            
            $Creator->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_OK)
                    ->setMessage(Fenix::lang("Правило перезаписи отредактировано"))
                    ->saveSession();
            
            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/rewrite');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/rewrite/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/rewrite/edit/id/' . $id);
            }
            
            Fenix::redirect('core/rewrite');
        }
        
        // Тайтл страницы
        $Creator ->getView()
                 ->headTitle(Fenix::lang("Редактировать правило перезаписи"));
        
        $Creator ->setLayout()
                 ->oneColumn($Form->fetch());        
    }
    
    public function deleteAction()
    {
        $currentRewrite = Fenix::getModel('core/rewrite')->getRewriteById(
            $this->getRequest()->getParam('id')
        );
        
        if ($currentRewrite == null) {
            Fenix::getCreatorUI()
                    ->loadPlugin('Events_Session')
                    ->setType(Creator_Events::TYPE_ERROR)
                    ->setMessage(Fenix::lang("Правило перезаписи не найдено"))
                    ->saveSession();
            
            Fenix::redirect('core/rewrite');
        }
        
        $Creator = Fenix::getCreatorUI();
        
        $Creator->loadPlugin('Form_Generator')
                ->setSource('core/rewrite', 'default')
                ->deleteRecord($currentRewrite);
        
        $Creator ->loadPlugin('Events_Session')
                 ->setType(Creator_Events::TYPE_OK)
                 ->setMessage(Fenix::lang("Правило перезаписи удалено"))
                 ->saveSession();
            
        Fenix::redirect('core/rewrite');
    }
}