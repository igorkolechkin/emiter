<?php
class Fenix_Core_Controller_Geo extends Fenix_Controller_Action
{
    public function indexAction()
    {
        throw new Exception();
    }
    
    public function regionsAction()
    {
        $countryId = $this->getRequest()->getParam('countryId');
        
        $regionsList = Fenix::getModel('core/geo')->getRegionsList(
            $countryId
        );
        
        $json = array();

        $json[] = array(
            'value'  => null,
            'option' => 'Область'
        );

        foreach ($regionsList AS $region)
        {
            $json[] = array(
                'value'  => $region->id,
                'option' => $region->name
            );
        }

    	$this->getResponse()
             ->setHeader('Content-Type', 'text/json')
        	 ->setBody(Zend_Json::encode($json))
        	 ->sendResponse();
    	exit;
    }
    
    public function cityAction()
    {
        $regionId = $this->getRequest()->getParam('regionId');
        
        $cityList = Fenix::getModel('core/geo')->getCityList(
            $regionId
        );
        
        $json   = array();
        foreach ($cityList AS $city) {
            $json[] = array(
                'value'  => $city->id,
                'option' => $city->name
            );
        }

    	$this->getResponse()
             ->setHeader('Content-Type', 'text/json')
        	 ->setBody(Zend_Json::encode($json))
        	 ->sendResponse();
    	exit;
    }
}