<?php
class Fenix_Core_Helper_Cache extends Fenix_Resource_Helper
{
    public function getCacheTable($list)
    {
        $Creator = Fenix::getCreatorUI();
        
        $Table = $Creator->loadPlugin('Table')
                         ->setStyle('margin-bottom:10px;');
        
        $Table ->addHeader('title',   "Название")
                    ->setWidth(200)
               ->addHeader('name',    "Системное название")
                    ->setWidth(200)
               ->addHeader('details', "Описание")
               ->addHeader('status',  "Статус")
                    ->setWidth(50)
               ->addHeader('actions', "Действия")
                    ->setWidth(50);
        
        foreach ($list->cache AS $cacheName => $cacheOptions)
        {
            $cleanButton = $Creator->loadPlugin('Button')
                                   ->appendClass('btn-danger')
                                   ->setOnclick('if (confirm(\'Подтвердите действие\')) self.location.href=\'' . Fenix::getUrl('core/cache/clean/name/' . $cacheName) . '\'')
                                   ->setValue("Очистить")
                                   ->setType('button')
                                   ->fetch();
            
            $status = Fenix_Cache::getStatus($cacheName);
            
            $Table ->addCell($cacheName, 'name',     $cacheOptions->name)
                   ->addCell($cacheName, 'sys_name', $cacheName)
                   ->addCell($cacheName, 'details',  $cacheOptions->details)
                   ->addCell($cacheName, 'clean',    $this->getStatusButton($cacheName, $cacheOptions))
                   ->addCell($cacheName, 'status',   $cleanButton);
        }
        
        return $Table;
    }
    

    public function getStatusButton($cacheName, $cacheOptions)
    {
        if (isset($cacheOptions->type) && $cacheOptions->type == 'simple_cleaner')
            return '';
        
        $status = Fenix_Cache::getStatus($cacheName);
        
        if ($status)
        {
            return Fenix::getCreatorUI()
                                ->loadPlugin('Button')
                                ->appendClass('btn-warning')
                                ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/cache/off/name/' . $cacheName) . '\'')
                                ->setValue("Выключить")
                                ->setType('button')
                                ->fetch();
        }
        else
        {
            return Fenix::getCreatorUI()
                                ->loadPlugin('Button')
                                ->appendClass('btn-success')
                                ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/cache/on/name/' . $cacheName) . '\'')
                                ->setValue("Включить")
                                ->setType('button')
                                ->fetch();
        }
    }
    public function renderCache($alias, $path, $id = null)
    {
        Fenix_Debug::log('renderCache '.$path.' begin');

        $cache = Fenix_Cache::getCache($alias);
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        if (Fenix::getRequest()->getAccessLevel() == 'backend') {
            $cache = false;
        }
        $load = true;
        if ($cache !== false) {
            $cacheId = md5('Render_' . $path . '_' . $id . '_' . $lang->name);
            if (!$data = $cache->load($cacheId)) {
                $data  = Fenix::getCreatorUI()->getView()->render($path);
                $cache ->save($data, $cacheId);
                $load = false;
            }
        }
        else {
            $data  = Fenix::getCreatorUI()->getView()->render($path);
            $load = false;
        }
        if ($load)
            Fenix_Debug::log('partialCache ' . $path . ' end load');
        else
            Fenix_Debug::log('partialCache ' . $path . ' end render');

        return $data;
    }
    public function partialCache($alias, $path, $id = null, $options = null)
    {
        //Fenix::dump($options);
        Fenix_Debug::log('partialCache '.$path.' begin');
        $cache = Fenix_Cache::getCache($alias);
        $lang = Fenix_Language::getInstance()->getCurrentLanguage();
        if (Fenix::getRequest()->getAccessLevel() == 'backend') {
            $cache = false;
        }
        $load = true;
        if ($cache !== false) {
            $cacheId = md5('Render_' . $path . '_' . $id . '_' . $lang->name);
            if (!$data = $cache->load($cacheId)) {
                if($options)
                    Fenix::getCreatorUI()->getView()->assign($options);

                $data  = Fenix::getCreatorUI()->getView()->render($path);
                $cache ->save($data, $cacheId);
                $load = false;
            }
        }
        else {
            /*if($id =='slider_top_home_page')
            Fenix::dump($id, $options);*/
            if($options)
                Fenix::getCreatorUI()->getView()->assign($options);

            $data  = Fenix::getCreatorUI()->getView()->render($path);
            $load = false;
        }
        if ($load)
            Fenix_Debug::log('partialCache ' . $path . ' end load');
        else
            Fenix_Debug::log('partialCache ' . $path . ' end render');

        return $data;
    }
}