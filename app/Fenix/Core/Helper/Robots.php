<?php
class Fenix_Core_Helper_Robots extends Fenix_Resource_Helper
{

    public function getForm($data)
    {
        if(Fenix::getRequest()->getPost('data')!=null){
            $data = Fenix::getRequest()->getPost('data');
        }

        $Creator = Fenix::getCreatorUI();
        $Form    = $Creator->loadPlugin('Form');
        /*
         * Кнопули
         */
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setStyle('float:right')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Сохранить")
                                         ->setType('submit')
                                         ->fetch()
                             ));

        $Row      = array();
        $Field    = $Creator->loadPlugin('Form_Textarea')
                            ->setName('data')
                            ->setStyle('height:400px')
                            ->setValue($data);
                            //->setValidator(new Zend_Validate_NotEmpty());
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel(Fenix::lang("Данные в файле robots.txt:"))
                            ->setContent($Field->fetch())
                            ->fetch();

        $Form    ->setContent($Row);

        $Form ->setContent($Buttonset->fetch());
        
        return $Form->compile();
    }
}