<?php

class Fenix_Core_Helper_Backend_Menu extends Fenix_Resource_Helper
{

    public function getCategoriesSelect($parent = 1)
    {
        $Creator = Fenix::getCreatorUI();
        $Field = $Creator->loadPlugin('Form_Select')->setId('category_id')->setName('category_id')->setSelected($Creator->getRequest()->getParam('category_id'));
        $Field->addOption(0, 'Не выбрано', '');

        self::getCategoriesOptionsRecursive($Field, $parent);

        $Field->setDetails('Выберите категорию, чтоб отобразить ее в меню и подпункты');

        return $Field->fetch();
    }

    private function getCategoriesOptionsRecursive(&$Field, $parent, $level = 0, $maxLevel = null)
    {
        if ($maxLevel != null && $maxLevel >= $parent) {
            return null;
        }

        $categories = Fenix::getModel('catalog/backend_categories')->getCategoriesList($parent);

        foreach ($categories as $_category) {
            $Field->addOption($_category->id, '' . $_category->title, 'level-' . $level);
            self::getCategoriesOptionsRecursive($Field, $_category->id, $level + 1, $maxLevel);
        }

        return true;
    }

    public function getStructureSelect($parent = 1)
    {
        $Creator = Fenix::getCreatorUI();
        $Field = $Creator->loadPlugin('Form_Select')->setId('structure_id')->setName('structure_id')->setSelected($Creator->getRequest()->getParam('structure_id'));
        $Field->addOption(0, 'Не выбрано', '');
        self::getStructureOptionsRecursive($Field, $parent, 0);

        $Field->setDetails('Выберите страницу, чтоб отобразить ее в меню и подпункты');

        return $Field->fetch();
    }

    private function getStructureOptionsRecursive(&$Field, $parent, $level = 0, $maxLevel = null)
    {
        if ($maxLevel != null && $maxLevel >= $parent) {
            return null;
        }

        $pages = Fenix::getModel('core/structure')->getPagesList($parent);

        foreach ($pages as $_page) {
            $Field->addOption($_page->id, '' . $_page->title, 'level-' . $level);
            self::getStructureOptionsRecursive($Field, $_page->id, $level + 1, $maxLevel);
        }

        return true;
    }

    public function getMenuItems()
    {
        $Creator = Fenix::getCreatorUI();

        $results = Fenix::getModel('core/menu')->findAllMenuItem($Creator->getRequest()->getParam('sid'));
        $menuItems = array();

        foreach ($results as $result) {
            $menuItems[$result->parent_menu_id][] = array('id'   => $result->id,
                                                          'name' => $result->title);
        }

        $Field = $Creator->loadPlugin('Form_Select')->setId('parent_menu_id')->setName('parent_menu_id')->setSelected($Creator->getRequest()->getParam('parent_menu_id'));
        $Field->addOption(0, 'Корень', '');

        $menuOptions = $this->buildRecursiveMenuForSelect($menuItems, 0, (int) $Creator->getRequest()->getParam('id'));

        if (null !== $menuOptions) {
            foreach ($menuOptions as $menuOption) {
                $Field->addOption($menuOption['id'], $menuOption['name'], '');
            }
        }

        return $Field->fetch();
    }

    private function buildRecursiveMenuForSelect($cats, $parentId = 0, $currentId = 0)
    {

        static $level = 0;
        static $results = array();

        $level++;

        if ($currentId > 0 && $currentId == $parentId) {
            $level--;

            return null;
        }

        if (is_array($cats) && isset($cats[$parentId])) {

            foreach ($cats[$parentId] as $cat) {

                if ($cat['id'] == $currentId) {
                    continue;
                }

                $results[] = array(
                    'id'   => $cat['id'],
                    'name' => str_repeat("-", $level) . ' ' . $cat['name']
                );

                $this->buildRecursiveMenuForSelect($cats, $cat['id'], $currentId);
            }

        }
        else {
            $level--;

            return null;
        }

        $level--;

        return $results;
    }
}