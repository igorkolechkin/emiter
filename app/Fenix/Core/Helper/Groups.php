<?php
class Fenix_Core_Helper_Groups extends Fenix_Resource_Helper
{
    static public function checkActiveGroups()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core'
                    && Fenix::getRequest()->getUrlSegment(1) == 'administrators'
                    && Fenix::getRequest()->getUrlSegment(2) == 'groups';
    }

    public function getGroupsTable($userId)
    {
        $GroupsList = Fenix::getModel('core/groups')->getGroupsList();
        
        $Creator = Fenix::getCreatorUI();
        
        $Table = $Creator->loadPlugin('Table')
                         ->setStyle('margin-bottom:10px;');
        
        $Table ->addHeader('rule',      "Название группы")
                    ->setColspan(2)
               ->addHeader('sys_title', "Системное название");
        
        $Model = Fenix::getModel()->setTable('admin_groups_relations');
        
        $_postGroups = (array) Fenix::getRequest()->getPost('group');
        
        foreach ($GroupsList AS $_group) {
            
            $Checkbox = $Creator->loadPlugin('Form_Checkbox')
                                ->setId('group_list_' . $_group->id)
                                ->setName('group[]')
                                ->setValue($_group->id);
            
            if ($userId != null) {
                $checked = $Model->fetchRow(
                    'admin_id = ' . (int) $userId . ' AND group_id = ' . (int) $_group->id
                );
                
                if ($checked != null) {
                    $Checkbox->setChecked('checked');
                }
            }
            
            if (in_array($_group->id, $_postGroups)) {
                $Checkbox->setChecked('checked');
            }
            
            $Table->addCell('group_' . $_group->id, 'checkbox', '<label>' . $Checkbox->fetch() . '<span class="lbl"></span></label>')
                     ->setStyle('padding:5px 10px;width:1px;')
                  ->addCell('group_' . $_group->id, 'rule',      $_group->title)
                     ->setStyle('padding-top:5px;padding-bottom:5px;')
                  ->addCell('group_' . $_group->id, 'sys_title', $_group->name)
                     ->setStyle('padding-top:7px;padding-bottom:7px;font-weight:bold;');
        }
        
        return $Table->fetch();
    }
}