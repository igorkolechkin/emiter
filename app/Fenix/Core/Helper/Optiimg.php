<?php
class Fenix_Core_Helper_Optiimg extends Fenix_Resource_Helper
{

    public function getForm($data)
    {
        if(Fenix::getRequest()->getPost('data')!=null){
            $data = Fenix::getRequest()->getPost('data');
        }

        $Creator = Fenix::getCreatorUI();
        $Form    = $Creator->loadPlugin('Form');
        /*
         * Кнопули
         */
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setStyle('float:right')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Оптимизировать")
                                         ->setType('submit')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                     ->setValue("Пакетная оптимизация")
                                     ->setStyle('margin-left:10px; background:red')
                                     //->setType('submit')
                                     ->setOnclick('self.location.href=\'/acp/core/optiimg/mode/package\'')
                                     ->fetch()
                             ));
        $Row      = array();
        $Row[]    = $Creator->loadPlugin('Container')
            ->setContent(Fenix::lang("Во избежание подвисания системы мы не рекомендуем оптимизировать более 100 изображений за раз. <br/>Пакетная оптимизация может занять долгое время, в случае большого количества не оптимизированных изображений. При использовании рекомендуется отойти от компьютера и не трогать его до полного завершения процесса оптимизации."))
            ->setStyle('margin-bottom:20px')
            ->fetch();

        $Form    ->setContent($Row);

        $Row      = array();
        $Field    = $Creator->loadPlugin('Form_Text')
                            ->setName('quality')
                            //->setStyle('height:400px')
                            ->setValue('100');
                            //->setValidator(new Zend_Validate_NotEmpty());
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel(Fenix::lang("Качество:"))
                            ->setContent($Field->fetch())
                            ->fetch();
        $Form    ->setContent($Row);

        $Row      = array();
        $Field    = $Creator->loadPlugin('Form_Text')
            ->setName('width')
            ->setValue('');
        $Row[]    = $Creator->loadPlugin('Row')
            ->setLabel(Fenix::lang("Подогнать по ширине:"))
            ->setContent($Field->fetch())
            ->fetch();
        $Form    ->setContent($Row);

        $Row      = array();
        $Field    = $Creator->loadPlugin('Form_Text')
            ->setName('height')
            ->setValue('');
        $Row[]    = $Creator->loadPlugin('Row')
            ->setLabel(Fenix::lang("Подогнать по высоте:"))
            ->setContent($Field->fetch())
            ->fetch();
        $Form    ->setContent($Row);

        $Form ->setContent($Buttonset->fetch());
        
        return $Form->compile();
    }
}