<?php
class Fenix_Core_Helper_System extends Fenix_Resource_Helper
{
    static public function checkRootActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && (
                Fenix::getRequest()->getUrlSegment(1) == 'settings' ||
                Fenix::getRequest()->getUrlSegment(1) == 'rewrite' ||
                Fenix::getRequest()->getUrlSegment(1) == 'cache' ||
                Fenix::getRequest()->getUrlSegment(1) == 'themes' ||
                Fenix::getRequest()->getUrlSegment(1) == 'modules' ||
                Fenix::getRequest()->getUrlSegment(1) == 'seo' ||
                Fenix::getRequest()->getUrlSegment(1) == 'update' ||
                Fenix::getRequest()->getUrlSegment(1) == 'currency' ||
                Fenix::getRequest()->getUrlSegment(1) == 'sitemap' ||
                Fenix::getRequest()->getUrlSegment(1) == 'mail' ||
                Fenix::getRequest()->getUrlSegment(1) == 'robots'
        );
    }

    static public function checkSettingsActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'settings';
    }

    static public function checkRewriteActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'rewrite';
    }

    static public function checkCacheActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'cache';
    }

    static public function checkThemesActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'themes';
    }

    static public function checkModulesActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'modules';
    }

    static public function checkSeoActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'seo';
    }

    static public function checkMailActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'mail';
    }


}