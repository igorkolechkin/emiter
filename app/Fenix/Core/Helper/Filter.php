<?php
class Fenix_Core_Helper_Filter extends Fenix_Resource_Helper
{
    public function renderAttributesSelect($value)
    {

        $attributes = Fenix::getModel('catalog/backend_attributes')->getAttributesList();

        $Field = Fenix::getCreatorUI()->loadPlugin('Form_Select')
                 ->setId('attribute_id')
                 ->setName('attribute_id');


        if($value)
             $Field->setSelected($value);
        else{
            $parent = Fenix::getModel('core/filter')->getFilterById(Fenix::getRequest()->getParam('sid'));
            if($parent){
                $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle($parent->name);
                if($attribute){
                    $Field->setSelected($attribute->id);
                }
            }

        }
        $Field->addOption('', 'Выберите атрибут');
        foreach ($attributes as $_attr){
            $Field->addOption($_attr->id, $_attr->title);
        }

        return $Field->fetch();
    }
}