<?php
class Fenix_Core_Helper_Developer extends Fenix_Resource_Helper
{
    static public function checkRootActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && (
                Fenix::getRequest()->getUrlSegment(1) == 'phpmyadmin' ||
                Fenix::getRequest()->getUrlSegment(1) == 'sxd' ||
                Fenix::getRequest()->getUrlSegment(1) == 'logging'
        ) || Fenix::getRequest()->getUrlSegment(0) == 'xhprof'
          || Fenix::getRequest()->getUrlSegment(0) == 'install';
    }

    static public function checkPhpmyadminActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'phpmyadmin';
    }

    static public function checkSxdActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'sxd';
    }

    static public function checkLoggingActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'logging';
    }

    static public function checkXhprofActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'xhprof';
    }

    static public function checkInstallActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'install';
    }
}