<?php
class Fenix_Core_Helper_Update extends Fenix_Resource_Helper
{
    /**
     * Таблица обновлений
     *
     * @param $updatesList
     * @return mixed
     */
    public function getTable($updatesList)
    {
        $Creator = Fenix::getCreatorUI();

        $Table = $Creator->loadPlugin('Table');

        $Table ->addHeader('details',     "Описание")
               ->addHeader('sys_title',   "Системное название")
                    ->setWidth(150)
               ->addHeader('updatedate',  "Дата выпуска")
                    ->setWidth(150)
               ->addHeader('checkdate',   "Дата проверки")
                    ->setWidth(150)
               ->addHeader('executedate', "Дата обновления")
                    ->setWidth(150)
               ->addHeader('action',      "Действия")
                    ->setColspan(2);

        foreach ($updatesList AS $_update) {

            $Table->addCell('update_' . $_update->id, 'details',        $_update->details)
                  ->addCell('update_' . $_update->id, 'sys_title',      $_update->sys_title)
                  ->addCell('update_' . $_update->id, 'updatedate',     $_update->update_date)
                  ->addCell('update_' . $_update->id, 'checkdate',      $_update->check_date)
                  ->addCell('update_' . $_update->id, 'executedate',    $_update->execute_date)
                  ->addCell('update_' . $_update->id, 'remind',         $this->getRemindButton($_update))
                    ->setWidth(140)
                  ->addCell('update_' . $_update->id, 'update',         $this->getUpdateButton($_update))
                    ->setWidth(90)
                    ;
        }

        return $Table->fetch();
    }

    /**
     * Формирование кнопки обновить
     *
     * @param $update
     * @return mixed
     */
    public function getUpdateButton($update)
    {
        if ($update->is_updated == '1') {
            return Fenix::getCreatorUI()
                ->loadPlugin('Button')
                ->appendClass('btn-info')
                ->appendClass('btn-block')
                ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/update/details/id/' . $update->id) . '\'')
                ->setValue("Детали")
                ->setType('button')
                ->fetch();
        }

        return Fenix::getCreatorUI()
                    ->loadPlugin('Button')
                    ->appendClass('btn-success')
                    ->appendClass('btn-block')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/update/process/id/' . $update->id) . '\'')
                    ->setValue("Обновить")
                    ->setType('button')
                    ->fetch();
    }

    /**
     * Формирование кнопки Напоминать/Нет
     *
     * @param $update
     * @return mixed
     */
    public function getRemindButton($update)
    {
        if ($update->is_updated == '1') {
            return;
        }

        if ($update->is_disabled == '0') {
            return Fenix::getCreatorUI()
                        ->loadPlugin('Button')
                        ->appendClass('btn-danger')
                        ->appendClass('btn-block')
                        ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/update/remind/id/' . $update->id . '/disabled/' . $update->is_disabled) . '\'')
                        ->setValue("Не напоминать")
                        ->setType('button')
                        ->fetch();
        }
        else {
            return Fenix::getCreatorUI()
                        ->loadPlugin('Button')
                        ->appendClass('btn-success')
                        ->appendClass('btn-block')
                        ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/update/remind/id/' . $update->id . '/disabled/' . $update->is_disabled) . '\'')
                        ->setValue("Включить")
                        ->setType('button')
                        ->fetch();
        }
    }
}