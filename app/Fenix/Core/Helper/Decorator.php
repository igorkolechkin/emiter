<?php

class Fenix_Core_Helper_Decorator extends Fenix_Resource_Helper
{
    /**
     * @param $index int индекс блока
     * @param $perLine int блоков в строке
     * @return string first-of-type | last-of-type
     */
    public function getBlockType($index, $perLine)
    {
        $index   = (int)$index + 1;
        $perLine = (int)$perLine;

        if (!$perLine) return '';

        if ($index == 1 || ($index - 1) % $perLine == 0) {
            $blockClass = 'first-of-type';
        } else {
            if ($index % $perLine == 0) {
                $blockClass = 'last-of-type';
            } else {
                $blockClass = '';
            }
        }
        return $blockClass;
    }
}