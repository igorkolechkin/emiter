<?php
	
	class Fenix_Core_Helper_Validation extends Fenix_Resource_Helper {

		static public function getFieldValidatorNotEmpty($msg_arr = array()) {
			$Field_validator__NotEmpty = new Zend_Validate_NotEmpty();
			$Field_validator__NotEmpty->setMessages(
				array(
					Zend_Validate_NotEmpty::IS_EMPTY => (isset($msg_arr['IS_EMPTY'])) ? $msg_arr['IS_EMPTY'] : Fenix::lang('Значение является обязательным и не может быть пустым'),
				)
			);

			return $Field_validator__NotEmpty;
		}

		static public function getFieldValidatorName($msg_arr = array()) {
			$Field_validator__Name = new Zend_Validate_Regex(array('pattern' => '/^([A-Za-zА-Яа-яЁё]+[\s\,\.\-]*)+$/u'));
			$Field_validator__Name->setMessages(
				array(
					Zend_Validate_Regex::INVALID   => (isset($msg_arr['INVALID'])) ? $msg_arr['INVALID'] : Fenix::lang("Указан недопустимый тип. Ожидается строка, целое число или число с плавающей точкой"),
					Zend_Validate_Regex::NOT_MATCH => (isset($msg_arr['NOT_MATCH'])) ? $msg_arr['NOT_MATCH'] : Fenix::lang("Введите корректное имя"),
					Zend_Validate_Regex::ERROROUS  => (isset($msg_arr['ERROROUS'])) ? $msg_arr['ERROROUS'] : Fenix::lang("There was an internal error while using the pattern") . " '%pattern%'",
				)
			);

			return $Field_validator__Name;
		}

		static public function getFieldValidatorEmailAddress($msg_arr = array()) {
			$Field_validator__EmailAddress = new Zend_Validate_EmailAddress();
			$Field_validator__EmailAddress->setMessages(
				array(
					Zend_Validate_EmailAddress::INVALID            => (isset($msg_arr['INVALID'])) ? $msg_arr['INVALID'] : Fenix::lang('Введите строку'),
					Zend_Validate_EmailAddress::INVALID_FORMAT     => (isset($msg_arr['INVALID_FORMAT'])) ? $msg_arr['INVALID_FORMAT'] : "%value% " . Fenix::lang('недействительный адрес электронной почты'),
					Zend_Validate_EmailAddress::INVALID_HOSTNAME   => (isset($msg_arr['INVALID_HOSTNAME'])) ? $msg_arr['INVALID_HOSTNAME'] : "'%hostname%' " . Fenix::lang("не является допустимым именем узла адреса электронной почты") . " '%value%'",
					Zend_Validate_EmailAddress::INVALID_MX_RECORD  => (isset($msg_arr['INVALID_MX_RECORD'])) ? $msg_arr['INVALID_MX_RECORD'] : "'%hostname%' " . Fenix::lang("не имеет правильную запись MX для адреса электронной почты") . " '%value%'",
					Zend_Validate_EmailAddress::INVALID_SEGMENT    => (isset($msg_arr['INVALID_SEGMENT'])) ? $msg_arr['INVALID_SEGMENT'] : "'%hostname%' " . Fenix::lang("is not in a routable network segment. The email address should not be resolved from public network"),
					Zend_Validate_EmailAddress::DOT_ATOM           => (isset($msg_arr['DOT_ATOM'])) ? $msg_arr['DOT_ATOM'] : "'%localPart%' " . Fenix::lang("can not be matched against dot-atom format"),
					Zend_Validate_EmailAddress::QUOTED_STRING      => (isset($msg_arr['QUOTED_STRING'])) ? $msg_arr['QUOTED_STRING'] : "'%localPart%' " . Fenix::lang("can not be matched against quoted-string format"),
					Zend_Validate_EmailAddress::INVALID_LOCAL_PART => (isset($msg_arr['INVALID_LOCAL_PART'])) ? $msg_arr['INVALID_LOCAL_PART'] : "'%localPart%' " . Fenix::lang("недействительная локальная часть адреса электронной почты для") . " '%value%'",
					Zend_Validate_EmailAddress::LENGTH_EXCEEDED    => (isset($msg_arr['LENGTH_EXCEEDED'])) ? $msg_arr['LENGTH_EXCEEDED'] : "'%value%' " . Fenix::lang("превышает допустимую длину"),
				)
			);

			return $Field_validator__EmailAddress;
		}

		static public function getFieldValidatorDbNoRecordExists($args, $msg_arr = array()) {
			$Field_validator = new Zend_Validate_Db_NoRecordExists($args);
			$Field_validator->setMessages(
				array(
					Zend_Validate_Db_NoRecordExists::ERROR_NO_RECORD_FOUND => (isset($msg_arr['ERROR_NO_RECORD_FOUND'])) ? $msg_arr['ERROR_NO_RECORD_FOUND'] : Fenix::lang('Значение не совпадает'),
					Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND    => (isset($msg_arr['ERROR_RECORD_FOUND'])) ? $msg_arr['ERROR_RECORD_FOUND'] : Fenix::lang('Найдено совпадение'),
				)
			);

			return $Field_validator;
		}

		static public function getFieldValidatorDbRecordExists($args, $msg_arr = array()) {
			$Field_validator = new Zend_Validate_Db_RecordExists($args);
			$Field_validator->setMessages(
				array(
					Zend_Validate_Db_RecordExists::ERROR_NO_RECORD_FOUND => (isset($msg_arr['ERROR_NO_RECORD_FOUND'])) ? $msg_arr['ERROR_NO_RECORD_FOUND'] : Fenix::lang('Значение не совпадает'),
					Zend_Validate_Db_RecordExists::ERROR_RECORD_FOUND    => (isset($msg_arr['ERROR_RECORD_FOUND'])) ? $msg_arr['ERROR_RECORD_FOUND'] : Fenix::lang('Найдено совпадение'),
				)
			);

			return $Field_validator;
		}

		static public function getFieldValidatorIdentical($args, $msg_arr = array()) {
			$Field_validator = new Zend_Validate_Identical($args);
			$Field_validator->setMessages(
				array(
					Zend_Validate_Identical::NOT_SAME      => (isset($msg_arr['NOT_SAME'])) ? $msg_arr['NOT_SAME'] : Fenix::lang('Значения не совпадают'),
					Zend_Validate_Identical::MISSING_TOKEN => (isset($msg_arr['MISSING_TOKEN'])) ? $msg_arr['MISSING_TOKEN'] : Fenix::lang('Значение для сравнения не найдено'),
				)
			);

			return $Field_validator;
		}

		static public function getFieldValidatorBetween($args, $msg_arr = array()) {
			$Field_validator = new Zend_Validate_Between($args);
			$Field_validator->setMessages(
				array(
					Zend_Validate_Between::NOT_BETWEEN        => (isset($msg_arr['NOT_BETWEEN'])) ? $msg_arr['NOT_BETWEEN'] : "'%value%' is not between '%min%' and '%max%', inclusively",
					Zend_Validate_Between::NOT_BETWEEN_STRICT => (isset($msg_arr['NOT_BETWEEN_STRICT'])) ? $msg_arr['NOT_BETWEEN_STRICT'] : "'%value%' is not strictly between '%min%' and '%max%'",
				)
			);

			return $Field_validator;
		}

		static public function getFieldValidatorStringLength($args, $msg_arr = array()) {
			$Field_validator = new Zend_Validate_StringLength($args);
			$Field_validator->setMessages(
				array(
					Zend_Validate_StringLength::INVALID   => (isset($msg_arr['INVALID'])) ? $msg_arr['INVALID'] : "Неверный формат данных",
					Zend_Validate_StringLength::TOO_SHORT => (isset($msg_arr['TOO_SHORT'])) ? $msg_arr['TOO_SHORT'] : Fenix::lang('Значение меньше') . " %min% " . Fenix::lang('символов'),
					Zend_Validate_StringLength::TOO_LONG  => (isset($msg_arr['TOO_LONG'])) ? $msg_arr['TOO_LONG'] : Fenix::lang('Значение больше') . " %max% " . Fenix::lang('символов'),
				)
			);

			return $Field_validator;
		}
	}