<?php
class Fenix_Core_Helper_Administrators extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'administrators';
    }

    static public function checkActiveManagement()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'core' && Fenix::getRequest()->getUrlSegment(1) == 'administrators' && Fenix::getRequest()->getUrlSegment(2) == '';
    }

    public function getForm()
    {
        $Creator = Fenix::getCreatorUI();
        
        $Form    = $Creator->loadPlugin('Form');
        
        /*
         * Кнопули
         */
        $Buttonset = $Creator->loadPlugin('Buttonset')
                             ->setContent(array(
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Сохранить")
                                         ->setType('submit')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Сохранить и создать")
                                         ->setType('submit')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Применить")
                                         ->setType('submit')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Отменить")
                                         ->setType('reset')
                                         ->fetch(),
                                 $Creator->loadPlugin('Button')
                                         ->setValue("Назад")
                                         ->setType('button')
                                         ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/administrators') . '\'')
                                         ->fetch()
                             ));

        /*
         * Заголовок формы
         */
        $Title   = $Creator->loadPlugin('Title')
                           ->setImage(Fenix::getAppEtcUrl('icons/icon-administrator.png', 'core'))
                           ->setTitle(Fenix::lang("Новый администратор"))
                           ->setButtonset($Buttonset->fetch());
        $Form    ->setContent($Title->fetch());
        
        /*
         * Хлебные крошки
         */
        $Bread   = $Creator->loadPlugin('Breadcrumbs')
                           ->addCrumb(array(
                               'url'   => Fenix::getUrl(),
                               'label' => Fenix::lang("Панель управления")
                           ))
                           ->addCrumb(array(
                               'url'   => Fenix::getUrl('core/administrators'),
                               'label' => Fenix::lang("Администраторы системы")
                           ))
                           ->addCrumb(array(
                               'label' => Fenix::lang("Новый администратор")
                           ));
        $Form     ->setContent($Bread->fetch());
        
        /*
         * Данные авторизации
         */
        $Row      = array();
        
        // Логин
        $Field    = $Creator->loadPlugin('Form_Text')
                            ->setName('login')
                            ->setValue($Form->getRequest()->getPost('login'))
                            ->setValidator(new Zend_Validate_NotEmpty())
                            ->setValidator(new Zend_Validate_Db_NoRecordExists(array(
                                'table'   => Fenix::getModel()->getTable('admin_users'),
                                'field'   => 'login',
                            )));
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel(Fenix::lang("Логин"))
                            ->setContent($Field->fetch())
                            ->fetch();
        
        // Пароль
        $Field    = array();
                
        $Field[]  = $Creator->loadPlugin('Form_Text')
                            ->setName('password')
                            ->setType('password')
                            ->setStyle('margin-bottom:5px;')
                            ->setValue($Form->getRequest()->getPost('password'))
                            ->setValidator(new Zend_Validate_NotEmpty())
                            ->setValidator(new Zend_Validate_Identical($Form->getRequest()->getPost('password_confirm')))
                            ->fetch();
        
        // Подтверждение пароля
        $Field[]  = $Creator->loadPlugin('Form_Text')
                            ->setDetails(Fenix::lang("Подтверждение пароля"))
                            ->setName('password_confirm')
                            ->setType('password')
                            ->setValue($Form->getRequest()->getPost('password_confirm'))
                            ->setValidator(new Zend_Validate_NotEmpty())
                            ->fetch();
        
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel(Fenix::lang("Пароль"))
                            ->setContent($Field)
                            ->fetch();
        
        $Fieldset = $Creator->loadPlugin('Fieldset')
                            ->setLegend("Данные авторизации")
                            ->setContent($Row);
        $Form ->setContent($Fieldset->fetch());
        
        /*
         * Профиль
         */
        $Row      = array();
        
        // Полное имя
        $Field    = $Creator->loadPlugin('Form_Text')
                            ->setName('fullname')
                            ->setValue($Form->getRequest()->getPost('fullname'));
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel(Fenix::lang("Полное имя"))
                            ->setContent($Field->fetch())
                            ->fetch();
        
        // Изображение
        $Field    = $Creator->loadPlugin('Form_Image')
                            ->setId('image')
                            ->setName('image')
                            ->setImage($Form->getRequest()->getPost('image'));
        $Row[]    = $Creator->loadPlugin('Row')
                            ->setLabel("Изображение")
                            ->setContent($Field->fetch())
                            ->fetch();
        
        $Fieldset = $Creator->loadPlugin('Fieldset')
                            ->setLegend("Профиль")
                            ->setContent($Row);
        $Form ->setContent($Fieldset->fetch());
        
        // Баттонсет
        $Form ->setContent($Buttonset->fetch());
        
        return $Form;
    }
}