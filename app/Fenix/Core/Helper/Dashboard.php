<?php
class Fenix_Core_Helper_Dashboard extends Fenix_Resource_Helper
{
    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == null;
    }

    static public function checkActiveInterface()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'interface';
    }

    public function assembleDashboard()
    {
        $xml = Fenix::assembleXml('dashboard.xml');
        
        $dashboard = array();
        
        
        if (isset($xml['dashboard'])) {
            foreach ($xml['dashboard'] AS $_group => $_groupOptions) {
                
                $panes    = $_groupOptions['panes']; unset($_groupOptions['panes']);
                $newPanes = array();
                foreach ($panes AS $_pane => $_paneOptions) {
                    $newPanes[$_paneOptions['offset']] = $_paneOptions;
                }
                ksort($newPanes);
                $_groupOptions['panes'] = array_values($newPanes);
                
                $dashboard[$_groupOptions['offset']] = $_groupOptions;
            }
            ksort($dashboard);
            
            $dashboard = array_values($dashboard);
        }
        
        return $dashboard;
    }
    
    
    public function assembleMenu()
    {
        $xml  = Fenix::assembleXml('menu.xml');

        $menu = array();
        $devMenu = array();

        if (isset($xml['menu'])) {
            foreach ($xml['menu'] AS $_rootName => $_rootOptions) {
                
                if (isset($_rootOptions['sub'])) {
                    $sub    = $_rootOptions['sub']; unset($_rootOptions['sub']);
                    $newSub = array();
                    
                    foreach ($sub AS $_sub) {

                        $newSub[$_sub['offset']] = $_sub;
                    }
                    
                    ksort($newSub);
                    $_rootOptions['sub'] = array_values($newSub);
                    
                }

                if('developer' == $_rootName && Fenix::isDev()){
                    $devMenu[$_rootOptions['offset']] = $_rootOptions;
                } elseif('developer' != $_rootName) {
                    $menu[$_rootOptions['offset']] = $_rootOptions;
                }
            }
        }
        
        ksort($menu);
        $menu = array_values($menu);

        if(!empty($devMenu)){
            foreach($devMenu as $_item) {
                $menu[] = $_item;
           }
        }
        
        return $menu;
    }
}