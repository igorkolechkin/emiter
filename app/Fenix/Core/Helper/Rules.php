<?php
class Fenix_Core_Helper_Rules extends Fenix_Resource_Helper
{
    public function getRulesTable($groupId)
    {
        $RulesList = Fenix::getModel('core/rules')->getRulesList();
        
        $Creator = Fenix::getCreatorUI();
        
        $Table = $Creator->loadPlugin('Table')
                         ->setStyle('margin-bottom:10px;');
        
        $Table ->addHeader('rule',      "Правило доступа")
                    ->setWidth('30%')
                    ->setColspan(2)
               ->addHeader('sys_title', "Системное название")
                    ->setWidth('30%')
               ->addHeader('detals',    "Описание правила");
        
        $Model = Fenix::getModel()->setTable('admin_groups_rules');
       
        $_postRules = (array) Fenix::getRequest()->getPost('rule');
        
        foreach ($RulesList AS $_rule => $_details) {
            
            $Checkbox = $Creator->loadPlugin('Form_Checkbox')
                                ->setId('rule_list_' . $_rule)
                                ->setName('rule[]')
                                ->setValue($_rule);
            
            if ($groupId != null) {
                $checked = $Model->fetchRow(
                    'group_id = ' . (int) $groupId . ' AND rule = \'' . $_rule . '\''
                );
                
                if ($checked != null) {
                    $Checkbox->setChecked('checked');
                }
            }
            
            if (in_array($_rule, $_postRules)) {
                $Checkbox->setChecked('checked');
            }
            
            $Table->addCell('rule_' . $_rule, 'checkbox',  '<label>' . $Checkbox->fetch() . '<span class="lbl"></span></label>')
                     ->setStyle('padding:5px 0 5px 5px;width:30px;')
                  ->addCell('rule_' . $_rule, 'rule',      $_details['label'])
                     ->setStyle('padding-top:5px;padding-bottom:5px;')
                  ->addCell('rule_' . $_rule, 'sys_title', $_rule)
                     ->setStyle('padding-top:7px;padding-bottom:7px;font-weight:bold;')
                  ->addCell('rule_' . $_rule, 'detals',    $_details['details'])
                     ->setStyle('padding-top:5px;padding-bottom:5px;');
        }
        
        return $Table->fetch();
    }
}