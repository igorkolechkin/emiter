<?php
class Fenix_Core_Plugin_Controller_Plugin_Settings extends Zend_Controller_Plugin_Abstract
{    
    public function preDispatch()
    {
        if (Fenix::getRequest()->getAccessLevel() == 'backend') {
            Fenix::getConfig()->updateSettings();
        }
        
        Fenix::getConfig()->loadAll();
    }
}