<?php
class Fenix_Core_Plugin_Controller_Plugin_Theme extends Zend_Controller_Plugin_Abstract
{    
    public function preDispatch()
    {
        if ($this->getRequest()->getAccessLevel() == 'frontend') {
            //            $domain = str_replace('.','',$_SERVER['HTTP_HOST']);
            //            $domain = str_replace('nafrontcomua','',$domain);
            //            if($domain==''){$domain='default';}
            //            $domain='default';
            //
            //            $defaultTheme = Fenix::getModel('core/themes')->getThemesByName($domain);

            $defaultTheme = Fenix::getModel('core/themes')->getDefaultTheme();

            Zend_Registry::set('FenixEngine_Theme', $defaultTheme);

            $View = Zend_Registry::get('getView');

            $View->addScriptPath(
                THEMES_DIR_ABSOLUTE . $defaultTheme->name . DS . "design" . DS
            );
        }
    }
}