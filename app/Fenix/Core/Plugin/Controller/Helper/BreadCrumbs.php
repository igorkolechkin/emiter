<?php

class Fenix_Core_Plugin_Controller_Helper_BreadCrumbs extends Zend_Controller_Action_Helper_Abstract
{
    private $navigation = array();

    public function direct($navigation)
    {
        if ($navigation instanceof Fenix_Engine_Rowset) {
            $this->navigation[] = array(
                'label' => Fenix::lang("Главная"),
                'uri' => Fenix::getUrl(),
                'id' => 'row_0'
            );

            foreach ($navigation AS $row) {
                $this->navigation[] = array(
                    'label' => $row->title,
                    'uri' => $row->getUrl(),
                    'id' => 'row_' . $row->id
                );
            }
        } else {
            $this->navigation = $navigation;
        }
    }

    public function getCrumbs()
    {
        return $this->navigation;
    }

    public function append(array $crumbs)
    {
        if (isset($crumbs[0]) && is_array($crumbs[0])) {
            $this->navigation = array_merge($this->navigation, $crumbs);
        }else{
            $this->navigation[] = $crumbs;
        }
    }

    public function postDispatch()
    {
        $breadcrumbs = new Zend_Navigation($this->navigation);

        $this->getActionController()
            ->view
            ->navigation($breadcrumbs)
            ->breadcrumbs()
            ->setSeparator(Fenix::getConfig('systemBreadcrumbsSeparator'));
    }
}