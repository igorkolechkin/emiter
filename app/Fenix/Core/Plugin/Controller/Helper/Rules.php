<?php

	class Fenix_Core_Plugin_Controller_Helper_Rules extends Zend_Controller_Action_Helper_Abstract {
		public function check($rule) {
			$userRules = Fenix::getModel('core/acl')->getUserRules();

			foreach($userRules AS $_rule) {
				if($_rule->rule == $rule) {
					return true;
				}
			}

			return false;
		}

		public function checkRedirect($rule) {
			if($this->check($rule) === false) {
				Fenix::redirect('core/denied');
			}
		}

		public function checkDevRedirect(){
			if(!Fenix::isDev()){
				Fenix::redirect('core/denied');
			}
		}
	}