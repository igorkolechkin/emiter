<?php
class Fenix_Core_Plugin_Controller_Helper_ViewRenderer extends Zend_Controller_Action_Helper_ViewRenderer
{
    public function getModuleDirectory()
    {
        return THEMES_DIR_ABSOLUTE;
    }
}