<?php
class Fenix_Core_Plugin_View_Helper_GetBanner extends Zend_View_Helper_Abstract
{
    private $_fenix = null;
    
    public function __construct()
    {
         
    }
    
    public function getBanner($sys_title)
    {
        return Fenix::getCollection('banners/banners')->load($sys_title);
    }
    
}