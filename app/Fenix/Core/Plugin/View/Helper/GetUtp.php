<?php
class Fenix_Core_Plugin_View_Helper_GetUtp extends Zend_View_Helper_Abstract
{
    private $_fenix = null;
    
    public function __construct()
    {
         
    }
    
    public function getUtp($sys_title)
    {
        return Fenix::getCollection('core/utp')->getUtp($sys_title);
    }

}