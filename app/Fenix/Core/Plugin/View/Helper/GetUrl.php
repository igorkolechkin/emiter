<?php
class Fenix_Core_Plugin_View_Helper_GetUrl extends Zend_View_Helper_Abstract
{
    public function __construct(){}
    
    public function getUrl($url = null)
    {
        return Fenix::getUrl($url);
    }
}