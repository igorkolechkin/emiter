<?php
class Fenix_Core_Plugin_View_Helper_GetSkinUrl extends Zend_View_Helper_Abstract
{
    public function __construct(){}
    
    public function getSkinUrl($url)
    {
        return Fenix::getSkinUrl($url);
    }
}