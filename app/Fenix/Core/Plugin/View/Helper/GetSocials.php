<?php
class Fenix_Core_Plugin_View_Helper_GetSocials extends Zend_View_Helper_Abstract
{
    
    public function __construct()
    {
         
    }
    
    public function getSocials()
    {
        return Fenix::getCollection('core/social')->getList();
    }
}