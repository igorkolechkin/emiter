<?php
class Fenix_Core_Plugin_View_Helper_GetSlider extends Zend_View_Helper_Abstract
{
    
    public function __construct()
    {
         
    }
    
    public function getSlider($system_name)
    {
        return Fenix::getCollection('core/slider')->getSlider($system_name);
    }
}