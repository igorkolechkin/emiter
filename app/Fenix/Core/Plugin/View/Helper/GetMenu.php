<?php
class Fenix_Core_Plugin_View_Helper_GetMenu extends Zend_View_Helper_Abstract
{
    private $_fenix = null;

    public function __construct()
    {

    }

    public function getMenu($sys_title)
    {
        return Fenix::getCollection('core/menu')->getMenu($sys_title);
    }
}