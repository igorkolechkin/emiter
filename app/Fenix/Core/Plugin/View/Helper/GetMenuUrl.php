<?php
class Fenix_View_Helper_GetMenuUrl extends Zend_View_Helper_Abstract
{
    public function __construct(){}
    
    public function getMenuUrl($menu)
    {
        return Fenix::getModel('core/menu')->getMenuUrl($menu);
    }
}