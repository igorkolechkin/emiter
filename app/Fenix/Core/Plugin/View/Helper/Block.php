<?php
class Fenix_Core_Plugin_View_Helper_Block extends Zend_View_Helper_Abstract
{
    private $_fenix = null;
    
    public function __construct()
    {
         
    }
    
    public function block($sys_title)
    {
        return Fenix::getModel('core/blocks')->getBlock($sys_title);
    }
    
    public function __toString()
    {
        return Fenix::getModel('core/blocks')->getBlock($sys_title)->content;
    }
}