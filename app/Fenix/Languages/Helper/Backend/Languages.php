<?php
class Fenix_Languages_Helper_Backend_Languages extends Fenix_Resource_Helper
{
    static public function getTranslateEditor($languageId){

        $Creator = Fenix::getCreatorUI();

        $Table = $Creator->loadPlugin('Table')
                         ->setStyle('margin-bottom:10px;')
                         ->setId('languageEditTable');

        $Table->addHeader('id', "#");
        $Table->addHeader('base', "Строка");
        $Table->addHeader('translation', "Перевод");

        if ($languageId) {
            $language = Fenix::getModel('languages/backend_languages')->getLanguageById($languageId);

            if ($language) {
                $translations = Fenix::getModel('languages/backend_translations')->getTranslationsList($language->code);
                foreach ($translations as $i => $record) {
                    $inputBase        = '<textarea type="text" name="base_string[]" class="translation-base">' . $record->base_string . '</textarea>';
                    $inputTranslation = '<textarea type="text" name="translation[]" class="translation-translation">' . $record->translation . '</textarea>';
                    $Table->addCell('record_' . $record->id, 'id', $record->id . '<input type="hidden" name="translate_id[]" value="' . $record->id . '" />')
                          ->setStyle('padding:5px;');
                    $Table->addCell('record_' . $record->id, 'base', $inputBase)
                          ->setStyle('padding:5px;');
                    $Table->addCell('record_' . $record->id, 'translation', $inputTranslation)
                          ->setStyle('padding:5px;');
                }
            }
        }

        $addButton = $Creator->loadPlugin('Button')
                ->appendClass('btn-primary')
                ->setValue(Fenix::lang("Добавить строку"))
                ->setType('button')
                ->setOnclick("languageEditTableAddRow()")
                ->fetch();
        $Script ='
        <script>
        function languageEditTableAddRow(){
            //Шаблон новой строки
            $("#languageEditTable").append("<tr><td><input type=\'hidden\' name=\'translate_id[]\' value=\'\' /></td><td><textarea type=\'text\' name=\'base_string[]\' class=\'translation-base\' ></textarea></td><td><textarea type=\'text\' name=\'translation[]\' class=\'translation-translation\' ></textarea></td></tr>");
        }
        </script>
        <style>
            .translation-base, .translation-translation {
                width: 90%;
                padding: 5px;
                resize: vertical;
            }
        </style>
        ';
        return $Table->fetch() . $addButton  . $Script;
    }

    static public function checkActive()
    {
        return Fenix::getRequest()->getUrlSegment(0) == 'languages' && Fenix::getRequest()->getUrlSegment(1) != 'rubric';
    }
}