<?php
class Fenix_Languages_Collection_Load extends Fenix_Resource_Collection
{
    public function load($rubric = null)
    {
        return new Fenix_Object(array(
            'data' => array(
                'rubric'  => Fenix::getCollection('languages/rubric')->load($rubric),
                'languages' => Fenix::getCollection('languages/languages')->load($rubric)
            )
        ));
    }
}