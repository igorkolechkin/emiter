<?php
class Fenix_Languages_Collection_Language extends Fenix_Object
{
    public function load($language, $rubric)
    {
        if ($language instanceof Zend_Db_Table_Row) {
            $language = $language->toArray();
        }

        $language['url']     = Fenix::getUrl('languages/' . ($rubric != null ? $rubric->url_key . '/' : null) . $language['url_key']);
        $language['gallery'] = $this->getGallery($language['gallery']);

        $this->setData($language);

        return $this;
    }

    public function getGallery($gallery)
    {
        $gallery = (array) unserialize($gallery);
        $gallery = array_map(function($value){
            $value['image'] = HOME_DIR_URL . $value['image'];
            return $value;
        }, $gallery);

        return new Fenix_Object_Rowset(array('data' => $gallery));
    }

    /**
     * Работаем с картинкой
     *
     * @return bool|string
     */
    public function getImage()
    {
        if ($this->getData('image') != null) {
            $info              = (object) unserialize($this->getData('image_info'));
            $imageUrl          = Fenix::createImageFromStreamInfo($this->getData('image'), $info);
            return $imageUrl;
        }

        return false;
    }

    /**
     * Кадрируем изображение
     *
     * @param null $width
     * @param null $height
     * @param array $bg_color
     * @return bool|string
     */
    public function getImageFrame($width = null, $height = null, $bg_color = array(255,255,255))
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::frame($image, $width, $height, $bg_color);
        }

        return false;
    }

    /**
     * Изменяем размер изображения
     *
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function getImageResize($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::resize($image, $width, $height);
        }

        return false;
    }

    public function getImageAdapt($width = null, $height = null)
    {
        if ($this->getData('image') != null) {
            $info  = (object) unserialize($this->getData('image_info'));
            $image = Fenix::createImageFromStreamInfo($this->getData('image'), $info, true);

            return Fenix_Image::adapt($image, $width, $height);
        }

        return false;
    }
}