<?php
class Fenix_Languages_Collection_Languages extends Fenix_Resource_Collection
{
    public function load($rubric)
    {
        $_languages = Fenix::getModel('languages/languages')->getLanguagesList($rubric);

        $_languages = $_languages->getCurrentItems()->toArray();

        $_data = array();
        foreach ($_languages AS $_language) {
            $_data[] = Fenix::getCollection('languages/language')->load($_language, $rubric);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data
        ));
    }

    public function getLastLanguages()
    {
        $_languages = Fenix::getModel('languages/languages')->getLastLanguages();

        $_data = array();
        foreach ($_languages AS $_language) {
            $_data[] = Fenix::getCollection('languages/language')->load($_language, null);
        }

        return new Fenix_Object_Rowset(array(
            'data'     => $_data,
            'rowClass' => false
        ));
    }
}