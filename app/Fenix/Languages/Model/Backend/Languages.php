<?php
class Fenix_Languages_Model_Backend_Languages extends Fenix_Resource_Model
{
    /**
     * Язык по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getLanguageById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('languages/languages');

        $this->setTable('languages');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->where('a.id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * Список статей
     *
     * @return Zend_Db_Select
     */
    public function getLanguagesAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('languages/languages');

        $this->setTable('languages');

        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select ->order('a.create_date desc');

        return $Select;
    }

    /**
     * Новый язык
     *
     * @param $Form
     * @param $req
     * @return int
     */
    public function addLanguage($Form, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->addRecord($req);

        // Удаляем все старые связи
        /*$this->setTable('languages_relations')
            ->delete('language_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'language_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }*/

        return (int) $id;
    }

    /**
     * Редактировать язык
     *
     * @param $Form
     * @param $current
     * @param $req
     * @return int
     */
    public function editLanguage($Form, $current, $req)
    {
        $req->setPost('create_date', $req->getPost('create_date') . ' ' . $req->getPost('create_time'));

        $id = $Form->editRecord($current, $req);

        // Удаляем все старые связи
        /*$this->setTable('languages_relations')
             ->delete('language_id = ' . (int) $id);

        // Добавляем новые
        foreach ((array) $req->getPost('rubric') AS $_rubricId) {
            $this->insert(array(
                'language_id' => (int) $id,
                'rubric_id'  => (int) $_rubricId
            ));
        }*/
        //Сохраняем переводы в базу данных
        $base_string = $req->getPost('base_string');
        $translation = $req->getPost('translation');
        foreach ((array)$req->getPost('translate_id') as $i => $_id) {
            if (isset($base_string[$i]) && isset($translation[$i])) {
                $data = array(
                    'language_code' => $req->getPost('code'),
                    'base_string'   => $base_string[$i],
                    'translation'   => $translation[$i]
                );
                if ($_id && trim($_id) != '') {
                    if (trim($base_string[$i]) == '' && trim($translation[$i]) == '') {
                        $this->setTable('languages_translations')
                             ->delete('id = ' . (int)$_id);
                    } else {
                        $this->setTable('languages_translations')
                             ->update($data, 'id = ' . (int)$_id);
                    }
                } else {
                    $this->setTable('languages_translations')
                         ->insert($data);
                }
            }
        }
        //Сохраняем в файл
        Fenix::getModel('languages/backend_translations')->saveToFile($req->getPost('code'));

        return (int) $id;
    }

    /**
     * Удаление статьи
     *
     * @param $current
     */
    public function deleteLanguage($current)
    {
        $Creator = Fenix::getCreatorUI();
        $Creator ->loadPlugin('Form_Generator')
                 ->setSource('languages/languages', $current->attributeset)
                 ->deleteRecord($current);

        /*$this->setTable('languages_relations')
             ->delete('language_id = ' . (int) $current->id);*/
    }
}