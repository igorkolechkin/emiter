<?php
class Fenix_Languages_Model_Backend_Translations extends Fenix_Resource_Model
{

    const TABLE_TRANSLATIONS = 'languages_translations';
    /**
     * Список переводов как селект
     *
     * @return Zend_Db_Select
     */
    public function getTranslationsAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('languages/translations');

        $this->setTable('languages_translations');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            't' => $this->_name
        ), $Engine->getColumns(array('prefix' => 't')));

        $Select ->order('t.id asc');

        return $Select;
    }

    /**
     * Список переводов
     *
     * @param null $languageCode
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getTranslationsList($languageCode = null)
    {
        $Select = $this->getTranslationsAsSelect();
        if ($languageCode)
            $Select->where('t.language_code = ?', (string)$languageCode);
        return $this->fetchAll($Select);
    }

    /**
     * Сохраняем в файл переводы
     * @param $languageCode
     * @return bool
     */
    public function saveToFile($languageCode)
    {
        $translations = self::getTranslationsList($languageCode);
        $filename     = LNG_DIR_ABSOLUTE . $languageCode . '/frontend/all.csv';
        if (is_file($filename)) {
            $fp = fopen($filename, 'w');
            fwrite($fp, '"",""' . "\n");
            foreach ($translations as $i => $record) {
                $data = '"' . $record->base_string . '","' . $record->translation . '"' . "\n";
                fwrite($fp, $data);
            }
            fclose($fp);
            return true;
        }
        return false;
    }

}