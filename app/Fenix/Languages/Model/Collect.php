<?php

class Fenix_Languages_Model_Collect extends  Fenix_Resource_Model {


    /**
     * СОбираем строки для перевода и заносим их в базу если их там нет
     * @param $base_string
     */
    public function collect($base_string){

        if(self::isActive()){
            $language =Fenix_Language::getInstance();
            $this->setTable(Fenix_Languages_Model_Backend_Translations::TABLE_TRANSLATIONS);
            foreach($language->getLanguagesList() as $lang){
                $Select = $this->select()
                               ->from($this->_name,'*')
                               ->where('language_code = ?',$lang->code)
                               ->where('base_string = ?', $base_string)
                               ->limit(1);

                $Result = $this->fetchRow($Select);
                if($Result === null){
                    $data = array(
                        'language_code' => $lang->code,
                        'base_string'   => $base_string
                    );
                    $this->insert($data);
                }
            }

        }
    }

    /**
     * Проверяет включен ли сбор строк
     * @return bool
     */
    public function isActive(){
        if (Fenix::getRequest()->getAccessLevel() != 'backend') {
            if (Fenix::getConfig('languages/translate/collect'))
                return true;
        }
        return false;
    }

} 