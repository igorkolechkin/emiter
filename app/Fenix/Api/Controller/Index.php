<?php
class Fenix_Api_Controller_Index extends Fenix_Controller_Action
{

    public function indexAction()
    {
        try
        {
            if (!isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
                throw new Exception("Пустой запрос");
            }

            // Распарсинг XML
            $xml = new Zend_Config_Xml($GLOBALS['HTTP_RAW_POST_DATA']);

            $fp = fopen(TMP_DIR_ABSOLUTE . "/sync/" . date('Y_m_d-H_i_s-') . $xml->sync . ".xml", "w+");
            fwrite($fp, $GLOBALS['HTTP_RAW_POST_DATA']);
            fclose($fp);

            // Модель реализующая функции АПИ
            $Api = Fenix::getModel('api/api');
            if($Api->checkAPiKey($xml)==false)
                die($this->_returnXml(Fenix_Api_Model_Api::ERROR_AUTH, 'Ошибка авторизации'));

            switch ($xml->sync) {
                case 'getCategories':
                    $result =  $Api->getCategories($xml);
                break;
                case 'getOrders':
                    $result =  $Api->getOrders($xml);
                break;
                case 'syncPrice':
                    $result =  $Api->syncPrice($xml);
                break;
                case 'syncProducts':
                    $result =  $Api->syncProducts($xml);
                break;
                case 'getProducts':
                    $result =  $Api->getProducts($xml);
                break;
                case 'syncDiscount':
                    $result =  $Api->syncDiscount($xml);
                break;
                case 'syncStores':
                    $result =  $Api->syncStores($xml);
                break;

                default:
                    die($this->_returnXml(Fenix_Api_Model_Api::ERROR_ACTION, 'Неизвестная функция'));
            }

            $fp = fopen(TMP_DIR_ABSOLUTE . "/sync/" . date('Y_m_d-H_i_s-') . $xml->sync . "-response.xml", "w+");
            fwrite($fp, $result);
            fclose($fp);

            echo  $result;
        }
        catch (Exception $e)
        {
            die($this->_returnXml(Fenix_Api_Model_Api::ERROR_XML, 'Неверный формат'));
        }
    }

    private function _returnXml($code, $message)
    {
        if($code=='error') $code = 2;
        if($code=='key') $code = 1;
        if($code=='function') $code = 3;

        //<!-- 1-Ошибка проверки ключа | 2-Ошибка в структуре xml | 3-Неизвестная функция -->
        $xml = '<?xml version="1.0"?>
<response>
    <sync>syncError</sync>
    <error>'.$code.'</error>
    <message><![CDATA[' . htmlspecialchars($message) . ']]></message>
</response>
';

        return $xml;
    }

    public function importAction()
    {

        try
        {
            if ($content = file(TMP_DIR_ABSOLUTE.'syncProducts-request.xml')) {
                throw new Exception("Пустой запрос");
            }

            // Распарсинг XML
            $xml = new Zend_Config_Xml($content);

            // Модель реализующая функции АПИ
            $Api = Fenix::getModel('api/api');
            if($Api->checkAPiKey($xml)==false)
                die($this->_returnXml(Fenix_Api_Model_Api::ERROR_AUTH, 'Ошибка авторизации'));

            switch ($xml->sync) {
                case 'syncCategories':
                    echo $Api->syncCategories($xml);
                    break;
                case 'getOrders':
                    echo $Api->getOrders($xml);
                    break;
                case 'syncPrice':
                    echo $Api->syncPrice($xml);
                    break;
                case 'syncProducts':
                    echo $Api->syncProducts($xml);
                    break;
                case 'syncDiscount':
                    echo $Api->syncDiscount($xml);
                    break;
                case 'syncStores':
                    echo $Api->syncStores($xml);
                    break;

                default:
                    die($this->_returnXml(Fenix_Api_Model_Api::ERROR_ACTION, 'Неизвестная функция'));
            }
        }
        catch (Exception $e)
        {
            die($this->_returnXml(Fenix_Api_Model_Api::ERROR_XML, 'Неверный формат'));
        }
    }
}