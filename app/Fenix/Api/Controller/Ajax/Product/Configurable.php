<?php
class Fenix_Api_Controller_Ajax_Product_Configurable extends Fenix_Controller_Action
{
    private $_ajax;
    public function preDispatch()
    {
        $this->_ajax = Fenix::getModel('api/ajax');
    }
    public function priceAction()
    {
        try{
            $productId = Fenix::getRequest()->getPost('product_id');
            $values    = Fenix::getRequest()->getPost('values');
            $product = Fenix::getModel('catalog/products')->getProductById($productId);
            if($product == null){
                $this->_ajax->error('Товар не существует id:' . $productId);
                exit;
            }

            $priceInfo = Fenix::getModel('catalog/products_configurable')->getProductPrice($product, $values);

            $this->_ajax->response((array)$priceInfo);

        } catch(Exception $e){

            $this->_ajax->error($e->getMessage());

        }
    }
}