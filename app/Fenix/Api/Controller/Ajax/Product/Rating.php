<?php
class Fenix_Api_Controller_Ajax_Product_Rating extends Fenix_Controller_Action
{
    private $_ajax;
    public function preDispatch()
    {
        $this->_ajax = Fenix::getModel('api/ajax');
    }

    public function indexAction()
    {
        $productId = Fenix::getRequest()->getPost('product_id');

        $rating = Fenix::getModel('catalog/reviews')->getAverageRating($productId);

        $this->_ajax->response($rating->toArray());
    }
}