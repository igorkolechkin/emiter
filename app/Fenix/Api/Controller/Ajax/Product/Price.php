<?php
class Fenix_Api_Controller_Ajax_Product_Price extends Fenix_Controller_Action
{
    private $_ajax;
    public function preDispatch()
    {
        $this->_ajax = Fenix::getModel('api/ajax');
    }

    public function indexAction()
    {
        $productId    = Fenix::getRequest()->getPost('product_id');

        $materials  = Fenix::getRequest()->getPost('materials');
        $attributes = Fenix::getRequest()->getPost('attributes');
        $options    = Fenix::getRequest()->getPost('options');

        $params = array(
            'materials'  => $materials,
            'attributes' => $attributes,
            'options'    => $options
        );
        $product = Fenix::getModel('catalog/products')->getProductById($productId);
        if($product == null){
            throw new Exception('Товар не существует');
        }

        $priceInfo = Fenix::getModel('catalog/products')->getPriceInfo($product, $params);

        $priceInfo->setData('price_formatted', Fenix::getHelper('catalog/decorator')->decoratePrice($priceInfo->price));
        $priceInfo->setData('price_old_formatted', '');
        $priceInfo->setData('discount_amount', 0);

        if($priceInfo->price_old > $priceInfo->price)
        {
            $priceInfo->setData('price_old',$priceInfo->price_old);
            $priceInfo->setData('price_old_formatted',Fenix::getHelper('catalog/decorator')->decoratePrice($priceInfo->price_old));
            $priceInfo->setData('discount_amount', $priceInfo->price_old - $priceInfo->price);
        }
        $this->_ajax->response($priceInfo->toArray());
    }
}