<?php
class Fenix_Api_Controller_Test extends Fenix_Controller_Action
{
    public function productsAction()
        {
            $filename = TMP_DIR_ABSOLUTE . '/api_test/syncPrice-request.xml';
            if (file_exists($filename)) {
                $xmlProducts = file_get_contents($filename);
                //$xml         = new Zend_Config_Xml($xmlProducts);

                $ch          = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlProducts);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $response = curl_exec($ch);
                curl_close($ch);

                Fenix::dump($response);
            } else {
                die('no file:' . $filename);
            }
    }
    public function categoriesAction()
        {

            $xmlProducts = '<?xml version="1.0" encoding="UTF-8"?>
<request>
	<api_key>e90516031d7eb02e59b5e3167c49e51b</api_key>
	<sync>getCategories</sync>
</request>
';



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlProducts);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        Fenix::dump($response);
    }
    public function getproductsAction()
    {

        $xmlProducts = '<?xml version="1.0" encoding="UTF-8"?>
<request>
	<api_key>e90516031d7eb02e59b5e3167c49e51b</api_key>
	<sync>getProducts</sync>
	<parent>62</parent>
</request>
';



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlProducts);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);
Fenix::dump($response);
        $xml =new Zend_Config_Xml($response);
        //Fenix::dump($xml->products->product->{0}->attributes);
        Fenix::dump($response);
    }

    public function priceAction()
    {
        $filename = TMP_DIR_ABSOLUTE . '/api_test/syncPrice-request.xml';
        if (file_exists($filename)) {
            $xmlProducts = file_get_contents($filename);
            //$xml         = new Zend_Config_Xml($xmlProducts);

            $ch          = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlProducts);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);

            Fenix::dump($response);
        } else {
            die('no file:' . $filename);
        }
    }
    public function discountAction()
    {

        $xmlPrice = '<?xml version="1.0" encoding="UTF-8"?>
<request>
	<api_key>1cd6cb5bf39ff45aa7d979f380a5632a</api_key>
	<sync>syncDiscount</sync>
	<list>
		<item>
			<discount_card>000000174732</discount_card>
			<cellphone/>
		</item>
		<item>
			<discount_card>000000141987</discount_card>
			<cellphone>+38(067)560-89-81</cellphone>
		</item>
		<item>
			<discount_card>000000173896</discount_card>
			<cellphone>+38(097)239-56-60</cellphone>
		</item>
		<item>
			<discount_card>000000173681</discount_card>
			<cellphone>+38(093)957-26-63</cellphone>
		</item>
		<item>
			<discount_card>000000173797</discount_card>
			<cellphone>+38(096)298-62-09</cellphone>
		</item>
		<item>
			<discount_card>000000173933</discount_card>
			<cellphone>+38(098)887-20-84</cellphone>
		</item>
		<item>
			<discount_card>000000173926</discount_card>
			<cellphone>+38(067)718-49-89</cellphone>
		</item>
		<item>
			<discount_card>000000173919</discount_card>
			<cellphone>+38(096)973-57-51</cellphone>
		</item>
		<item>
			<discount_card>000000118187</discount_card>
			<cellphone>+38(063)574-10-69</cellphone>
		</item>
		<item>
			<discount_card>000000173810</discount_card>
			<cellphone>+38(067)197-59-43</cellphone>
		</item>
		<item>
			<discount_card>000000173803</discount_card>
			<cellphone>+38(095)242-96-31</cellphone>
		</item>
		<item>
			<discount_card>000000173988</discount_card>
			<cellphone>+38(093)915-50-87</cellphone>
		</item>
		<item>
			<discount_card>000000173971</discount_card>
			<cellphone>+38(068)411-45-97</cellphone>
		</item>
		<item>
			<discount_card>000000173827</discount_card>
			<cellphone>+38(097)519-27-90</cellphone>
		</item>
		<item>
			<discount_card>000000157322</discount_card>
			<cellphone>+38(097)364-48-57</cellphone>
		</item>
		<item>
			<discount_card>000000173964</discount_card>
			<cellphone>+38(067)920-13-38</cellphone>
		</item>
		<item>
			<discount_card>000000173957</discount_card>
			<cellphone>+38(095)365-45-08</cellphone>
		</item>
		<item>
			<discount_card>000000173940</discount_card>
			<cellphone>+38(098)214-08-07</cellphone>
		</item>
		<item>
			<discount_card>000000173834</discount_card>
			<cellphone>+38(063)998-21-44</cellphone>
		</item>
		<item>
			<discount_card>000000173773</discount_card>
			<cellphone>+38(093)886-69-38</cellphone>
		</item>
		<item>
			<discount_card>000000158251</discount_card>
			<cellphone>+38(093)809-42-74</cellphone>
		</item>
		<item>
			<discount_card>000000158244</discount_card>
			<cellphone/>
		</item>
		<item>
			<discount_card>000000158121</discount_card>
			<cellphone>+38(097)593-38-12</cellphone>
		</item>
		<item>
			<discount_card>000000117746</discount_card>
			<cellphone>+38(097)542-88-43</cellphone>
		</item>
		<item>
			<discount_card>000000100007</discount_card>
			<cellphone>+38(066)706-81-37</cellphone>
		</item>
		<item>
			<discount_card>000000100014</discount_card>
			<cellphone>+38(067)681-19-11</cellphone>
		</item>
		<item>
			<discount_card>000000100021</discount_card>
			<cellphone>+38(097)986-32-04</cellphone>
		</item>
		<item>
			<discount_card>000000100038</discount_card>
			<cellphone>+38(096)340-35-81</cellphone>
		</item>
		<item>
			<discount_card>000000100045</discount_card>
			<cellphone/>
		</item>
		<item>
			<discount_card>000000100052</discount_card>
			<cellphone>+38(098)400-94-77</cellphone>
		</item>
		<item>
			<discount_card>000000100069</discount_card>
			<cellphone>+38(097)512-95-17</cellphone>
		</item>
		<item>
			<discount_card>000000100076</discount_card>
			<cellphone>+38(067)560-33-65</cellphone>
		</item>
		<item>
			<discount_card>000000100083</discount_card>
			<cellphone>+38(066)257-29-77</cellphone>
		</item>
		<item>
			<discount_card>000000100090</discount_card>
			<cellphone>+38(067)695-09-64</cellphone>
		</item>
		<item>
			<discount_card>000000100106</discount_card>
			<cellphone>+38(067)773-21-44</cellphone>
		</item>
		<item>
			<discount_card>000000100113</discount_card>
			<cellphone>+38(096)760-99-57</cellphone>
		</item>
		<item>
			<discount_card>000000100120</discount_card>
			<cellphone>+38(067)569-01-33</cellphone>
		</item>
		<item>
			<discount_card>000000100137</discount_card>
			<cellphone>+38(067)875-09-92</cellphone>
		</item>
		<item>
			<discount_card>000000100144</discount_card>
			<cellphone>+38(067)389-02-90</cellphone>
		</item>
		<item>
			<discount_card>000000100151</discount_card>
			<cellphone>+38(098)552-97-98</cellphone>
		</item>
		<item>
			<discount_card>000000100168</discount_card>
			<cellphone>+38(097)131-47-15</cellphone>
		</item>
		<item>
			<discount_card>000000100175</discount_card>
			<cellphone>+38(097)724-65-54</cellphone>
		</item>
		<item>
			<discount_card>000000100182</discount_card>
			<cellphone>+38(067)569-24-52</cellphone>
		</item>
		<item>
			<discount_card>000000100199</discount_card>
			<cellphone>+38(093)993-30-25</cellphone>
		</item>
    </list>
</request>
';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://sts.fnx.dp.ua/api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlPrice);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        Fenix::dump($response);
    }
    public function storeAction()
    {

        $xmlPrice = '<?xml version="1.0" encoding="UTF-8"?>
<request>
	<api_key>1cd6cb5bf39ff45aa7d979f380a5632a</api_key>
	<sync>syncPrice</sync>
	<products>
		<product>
			<id>199569</id>
			<barcode>21995699</barcode>
			<weight>1,4</weight>
			<size/>
			<carat>0</carat>
			<sku>3061</sku>
			<qty>0</qty>
			<price_pr1>101</price_pr1>
			<price_pr2>116</price_pr2>
		</product>
		<product>
			<id>331583</id>
			<barcode>23315839</barcode>
			<weight>3,81</weight>
			<size/>
			<carat>0</carat>
			<sku>72258о</sku>
			<qty>0</qty>
			<price_pr1>245</price_pr1>
			<price_pr2>283</price_pr2>
		</product>
		<product>
			<id>376220</id>
			<barcode>23762206</barcode>
			<weight>1,51</weight>
			<size/>
			<carat>0</carat>
			<sku>80011Б</sku>
			<qty>0</qty>
			<price_pr1>161</price_pr1>
			<price_pr2>185</price_pr2>
		</product>
		<product>
			<id>364745</id>
			<barcode>23647459</barcode>
			<weight>1,21</weight>
			<size/>
			<carat>0</carat>
			<sku>80035</sku>
			<qty>0</qty>
			<price_pr1>135</price_pr1>
			<price_pr2>156</price_pr2>
		</product>
		<product>
			<id>404259</id>
			<barcode>24042598</barcode>
			<weight>2,32</weight>
			<size>16</size>
			<carat>0</carat>
			<sku>JR-3137</sku>
			<qty>0</qty>
			<price_pr1>324</price_pr1>
			<price_pr2>374</price_pr2>
		</product>
		<product>
			<id>411346</id>
			<barcode>24113465</barcode>
			<weight>4,28</weight>
			<size/>
			<carat>0</carat>
			<sku>SK-SA022-Eol</sku>
			<qty>0</qty>
			<price_pr1>599</price_pr1>
			<price_pr2>699</price_pr2>
		</product>
		<product>
			<id>411269</id>
			<barcode>24112697</barcode>
			<weight>1,94</weight>
			<size/>
			<carat>0</carat>
			<sku>SK-SA022-Рol</sku>
			<qty>0</qty>
			<price_pr1>179</price_pr1>
			<price_pr2>209</price_pr2>
		</product>
		<product>
			<id>415893</id>
			<barcode>24158930</barcode>
			<weight>2,26</weight>
			<size>17</size>
			<carat>0</carat>
			<sku>TR-3047</sku>
			<qty>0</qty>
			<price_pr1>399</price_pr1>
			<price_pr2>467</price_pr2>
		</product>
		<product>
			<id>199848</id>
			<barcode>21998485</barcode>
			<weight>0,3</weight>
			<size/>
			<carat>0</carat>
			<sku>С1Ст/608</sku>
			<qty>0</qty>
			<price_pr1>75</price_pr1>
			<price_pr2>87</price_pr2>
		</product>
	</products>
</request>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://sts.fnx.dp.ua/api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlPrice);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        Fenix::dump($response);
    }

    /**
     * Тест выгрузки заказов
     */
    public function ordersAction()
    {
        //Шаблон запроса
        $filename = TMP_DIR_ABSOLUTE . 'sync_templates/getOrders-request.xml';

        if(file_exists($filename)){
            $xmlContent = file_get_contents($filename);

            /* $xml = new Zend_Config_Xml($xmlContent);
             $result  = Fenix::getModel('api/api')->getOrders($xml);

             $fp = fopen(TMP_DIR_ABSOLUTE . "/sync/files/result_getOrders-request.xml","w+");
             fwrite($fp,$result);
             fclose($fp);*/
        }
        // echo $result;

        //-------------------------------------------------------------------------
        /**
         *
         * Эмуляция запроса от 1с
         */

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlContent);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        curl_close($ch);

        Fenix::dump('http://'.$_SERVER['HTTP_HOST'].'/api',$response);

        //echo 'orders done';
    }


    public function parentAction(){
        //Fenix::getModel('api/api')->setParent();
    }
}