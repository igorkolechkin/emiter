<?php

/**
 * Class Fenix_Api_Model_Ajax
 */
class Fenix_Api_Model_Ajax  extends Fenix_Resource_Model
{
    public function error($message, $data = null)
    {

        header('Content-Type: application/json');
        header('HTTP/1.0 400 Bad request');

        if($data == null){
            $data = array();
        }
        $data['error']   = 1;
        $data['message'] = $message;

        echo json_encode($data);
    }

    public function response($data)
    {
        header('Content-Type: application/json');
        $data['error'] = 0;
        echo json_encode($data);
    }
}