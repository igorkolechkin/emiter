<?php
/**
 * Класс реализующий функции API
 *
 * Class Fenix_Api_Model_Api
 */
class Fenix_Api_Model_Api extends Fenix_Resource_Model
{
    const ERROR_XML = 2;
    const ERROR_AUTH = 1;
    const ERROR_ACTION = 3;
    const API_KEY = 'e90516031d7eb02e59b5e3167c49e51b'; // md5('stsapi')
    const NL = "\n";
    const TAB = "\t";

    public function checkAPiKey($xml)
    {
        if ($xml->api_key == self::API_KEY)
            return true;
        else
            return false;
    }
    public function syncProducts($xml){
        error_reporting(0);
        $response = '<?xml version="1.0"?>
        <response>
            <sync>syncProducts</sync>       <!-- Синхронизация товаров -->
            <products>
                <product>
                    <id>123</id>            <!-- (bigint) Партия уникальный-->
                    <status>create</status> <!-- create|update|error-->
                    <error>0</error>        <!-- код ошибки -->
                </product>
                <!-- повторение тега product -->
            </products>
        </response>
        ';
        $content = '';
        $countAll = 0;
        $countUpdate = 0;
        $countCreate = 0;
        $countError = 0;


        if($xml->products->product->count()>0){
            foreach($xml->products->product as $product){
                $countAll ++;
                $countCreate++;
                $importData = $product->toArray();
                $catalogData = array();
                $storeData = array();
                $parent = array();

                $req = new Fenix_Controller_Request_Http();
                //Отчет
                $content .= '<product>';
                $content .= '<sku>' . $product->sku. '</sku>';
                $content .= '<status>create</status>';
                $content .= '</product>' . self::NL;
                /**
                 * Собираем данные по товару
                 */
/*                foreach($importData as $key =>$value){

                    switch($key){
                        //Проверяем существование категорий
                        case 'parent':
                            if (is_array($value) && isset($value['id'])) {

                                if (is_array($value['id'])) {
                                    foreach ($value['id'] as $parentId) {
                                        $category = Fenix::getModel('catalog/categories')->getCategoryById($parentId);
                                        if ($category) {
                                            $parent[] = $parentId;
                                        }
                                    }
                                } else {
                                    $parentId = $value['id'];
                                    $category = Fenix::getModel('catalog/categories')->getCategoryById($parentId);
                                    if ($category) {
                                        $parent[] = $parentId;
                                    }
                                }
                            }
                            break;
                        case 'sku':
                        case 'sku_russian':
                            $catalogData['sku'] = $value;
                            break;
                        case 'type_of_product_russian':
                        case 'type_of_product_ukrainian':
                        case 'supplier_russian':
                        case 'supplier_ukrainian':
                        case 'carat':
                        case 'width':
                        case 'height':
                        case 'material_russian':
                        case 'material_ukrainian':
                        case 'sample':
                        case 'inset_russian':
                        case 'inset_ukrainian':
                        case 'form_russian':
                        case 'form_ukrainian':
                        case 'color_russian':
                        case 'color_ukrainian':
                        case 'closure_russian':
                        case 'closure_ukrainian':
                        case 'plot_russian':
                        case 'plot_ukrainian':
                        case 'gender_russian':
                        case 'gender_ukrainian':
                        case 'category_russian':
                        case 'category_ukrainian':
                        case 'details_russian':
                        case 'details_ukrainian':
                            $catalogData[$key] = $value;
                        break;
                    }

                }*/

                //$catalogProduct = Fenix::getModel('catalog/products')->getProductBySku($catalogData['sku']);
/*
                try {
                    //Товар в каталоге
                    if ($catalogProduct) { //Обновление
                        $this->setTable('catalog_products');
                        $result = $this->update($catalogData, 'id = ' . (int)$catalogProduct->id);
                        $content .= '<product>';
                        $content .= '<id>' . $catalogProduct->id . '</id>';
                        $content .= '<sku>' . $catalogProduct->sku . '</sku>';
                        $content .= '<status>update</status>';
                        $content .= '<error>0</error>';
                        $content .= '</product>' . self::NL;
                        $countUpdate++;
                    } else { //Добавление
                        $countCreate++;
                        //если не указан урл ки для нового товара создаем его из ид
                        if (!isset($catalogData['url_key'])) {
                            $catalogData['url_key'] = $catalogData['sku'];
                        }
                        if (!isset($catalogData['title_russian'])) {
                            $catalogData['title_russian'] = $catalogData['sku'];
                        }

                        $this->setTable('catalog_products');
                        $result = $this->insert($catalogData);
                        //Отчет
                        $content .= '<product>';
                        $content .= '<id>' . $product->id . '</id>';
                        $content .= '<sku>' . $catalogData['sku'] . '</sku>';
                        if ($result > 0) {
                            $content .= '<status>create</status>';
                            $content .= '<error>0</error>';
                        } else {
                            $content .= '<status>error</status>';
                            $content .= '<error>1</error>';
                        }
                        $content .= '</product>' . self::NL;
                    }
                    $productId = ($catalogProduct) ? $catalogProduct->id : $result;

                    // Обновляем категории
                    if(count($parent)>0)
                        $categories = $parent;
                    else
                        $categories = array(178);

                    // Удаляем старые
                    $this->setTable('catalog_relations')
                         ->delete('product_id = ' . (int) $productId);
                    //Добавляем новые

                    $this->setTable('catalog_relations');
                    $this->delete('product_id = ' . $productId);

                    if($categories && is_array($categories)){
                        foreach($categories as $i=> $categoryId){

                            $_category = Fenix::getModel('catalog/categories')->getCategoryById($categoryId);
                            $this->setTable('catalog_relations');

                            $data = array(
                                'parent'     => $categoryId,
                                'product_id' => $productId,
                                'left'       => $_category->left,
                                'right'      => $_category->right
                            );

                            $this->insert($data);
                            if($i==0){ // Ставим первую категорию как родительскую
                                $data = array('parent'=>$categoryId);
                                $this->setTable('catalog_products');
                                $this->update($data, 'id = ' . (int)$productId);
                            }
                        }
                    }else{
                        // Если нет выбраной категории ставим как родительскую корень
                        $data = array('parent' => 1);
                        $this->setTable('catalog_products');
                        $this->update($data, 'id = ' . 178);
                    }
                } catch (Exception $e) {
                    Fenix::dump($e);
                }*/
            }
        }
        $content .= self::NL . '<!-- all:'.$countAll.' create:'.$countCreate.' update:'.$countUpdate.' error:'.$countError.'-->';
        $response = '<?xml version="1.0"?>
<response>
    <sync>syncProducts</sync>
    <products>
'.$content.'
    </products>
</response>
';
        return $response;
    }
    public function syncOrders(){
        $response = '<?xml version="1.0"?>
<response>
    <sync>syncOrders</sync>                  <!-- Синхронизация заказов -->
    <orders>
        <order>
            <id></id>                        <!-- (int) ID Заказа-->
            <date>2014-12-31 23:59:59</date> <!-- Дата оформления заказа -->
            <status></status>                <!-- (int) Статус заказа  1-В ожидании | 2-В обработке | 3-Отменён | 4-Завершён -->
            <delivery></delivery>            <!-- (float) Стоимость доставки -->
            <discount></discount>            <!-- (float) Сумма скидки в грн -->
            <total_sum></total_sum>          <!-- (float) Сумма итого к оплате-->
            <market></market>                <!-- (int) ID Магазина -->
            <customer>
                <card></card>                <!-- (bigint) Номер карты покупателя -->
            </customer>
            <products>
                <product>
                    <id></id>               <!-- (bigint) Партия уникальный-->
                    <sku></sku>             <!-- (varchar 255) Артикул-->
                    <kit></kit>             <!-- (int) ID Комплекта -->
                    <qty></qty>             <!-- (int) Кол-во-->
                    <price></price>         <!-- (float) Цена по которой продано -->
                    <price_pr1></price_pr1> <!-- (float) Цена PR1 в момент покупки -->
                    <price_pr2></price_pr2> <!-- (float) Цена PR2 в момент покупки-->
                </product>
                <!-- повторение тега product -->
            </products>
        </order>
        <!-- повторение тега order -->
    </orders>
</response>
';
        return $response;
    }

    public function getOrders($xml){

        //Период
        $beginDate = $xml->period->begin;
        $endDate   = $xml->period->end;

        //Список заказов
        $Select = Fenix::getModel('checkout/backend_orders')->getOrdersListAsSelect();
        $Select->where('create_date BETWEEN "' . $beginDate . '" AND "' . $endDate . '"');
        $orders  = $this->fetchAll($Select);
        $orders = $orders->toArray();
        $content ='';

        for ($i = 0; $i < count($orders); $i++) {
            $order = Fenix::getCollection('checkout/order')->getOrderInfo($orders[$i]['id']);
            if ($order) {
            } else {
                continue;
            }

            $content .= self::TAB . '<order>' . self::NL;
            $content .= self::TAB . self::TAB . '<id>'          . $order->id . '</id>' . self::NL;
            $content .= self::TAB . self::TAB . '<create_date>' . $order->create_date. '</create_date>' . self::NL;
            $content .= self::TAB . self::TAB . '<status>'      . $order->status. '</status>' . self::NL;
            $content .= self::TAB . self::TAB . '<customer_id>' . $order->customer_id . '</customer_id>' . self::NL;
            $content .= self::TAB . self::TAB . '<name>'        . Fenix::xmlProtect($order->name) . '</name>' . self::NL;
            $content .= self::TAB . self::TAB . '<cellphone>'   . Fenix::xmlProtect($order->cellphone) . '</cellphone>' . self::NL;
            $content .= self::TAB . self::TAB . '<email>'       . Fenix::xmlProtect($order->email) . '</email>' . self::NL;
            $content .= self::TAB . self::TAB . '<comment>'     . Fenix::xmlProtect($order->comment) . '</comment>' . self::NL;

            $content .= self::TAB . self::TAB . '<delivery_type>'     . Fenix::xmlProtect($order->delivery_type) . '</delivery_type>' . self::NL;
            $content .= self::TAB . self::TAB . '<delivery_city>'     . Fenix::xmlProtect($order->delivery_city) . '</delivery_city>' . self::NL;
            $content .= self::TAB . self::TAB . '<delivery_warehouse>'. Fenix::xmlProtect($order->delivery_warehouse) . '</delivery_warehouse>' . self::NL;
            $content .= self::TAB . self::TAB . '<delivery_address>'  . Fenix::xmlProtect($order->delivery_address) . '</delivery_address>' . self::NL;
            //$content .= self::TAB . self::TAB . '<store_id>'          . Fenix::xmlProtect($order->store_id) . '</store_id>' . self::NL;
            $content .= self::TAB . self::TAB . '<delivery_price>'    . Fenix::xmlProtect($order->delivery_price) . '</delivery_price>' . self::NL;
            $content .= self::TAB . self::TAB . '<pay_variants>'      . Fenix::xmlProtect($order->pay_variants) . '</pay_variants>' . self::NL;
            $content .= self::TAB . self::TAB . '<discount_card>'     . Fenix::xmlProtect($order->discount_card) . '</discount_card>' . self::NL;
            $content .= self::TAB . self::TAB . '<discount_percent>'  . Fenix::xmlProtect($order->discount_percent) . '</discount_percent>' . self::NL;
            $content .= self::TAB . self::TAB . '<discount_amount>'   . Fenix::xmlProtect($order->discount_amount) . '</discount_amount>' . self::NL;
            $content .= self::TAB . self::TAB . '<is_epay>'           . Fenix::xmlProtect($order->is_epay) . '</is_epay>' . self::NL;

            $content .= self::TAB . self::TAB . '<total_price>'           . Fenix::xmlProtect($order->total_price) . '</total_price>' . self::NL;
            $content .= self::TAB . self::TAB . '<total_order_price>'     . Fenix::xmlProtect($order->total_order_price) . '</total_order_price>' . self::NL;
            $content .= self::TAB . self::TAB . '<total_discount_price>'  . Fenix::xmlProtect($order->total_discount_price) . '</total_discount_price>' . self::NL;
            $content .= self::TAB . self::TAB . '<total_nodiscount_price>'. Fenix::xmlProtect($order->total_nodiscount_price) . '</total_nodiscount_price>' . self::NL;
            $content .= self::TAB . self::TAB . '<total_qty>'             . Fenix::xmlProtect($order->total_qty) . '</total_qty>' . self::NL;

            $productsContent = '';
            if($order->products) {
                foreach($order->products as $product){
                    $productsContent .= self::TAB . self::TAB . self::TAB . '<product>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . self::TAB . '<id_1c>'       . Fenix::xmlProtect($product->id_1c) . '</id_1c>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . self::TAB . '<qty>'         . Fenix::xmlProtect($product->getOrderQty()) . '</qty>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . self::TAB . '<discount>'    . Fenix::xmlProtect($product->getCpDiscount()) . '</discount>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . self::TAB . '<unit_price>'  . Fenix::xmlProtect($product->getOrderUnitPrice()) . '</unit_price>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . self::TAB . '<order_price>' . Fenix::xmlProtect($product->getCpOrderPrice()) . '</order_price>' . self::NL;
                    $productsContent .= self::TAB . self::TAB . self::TAB . '</product>' . self::NL;
                }
            }

            $content .= self::TAB . self::TAB . '<products>' . self::NL;
            $content .= $productsContent;
            $content .= self::TAB . self::TAB . '</products>' . self::NL;
            $content .= self::TAB . '</order>' . self::NL;

        }



        $xml = '<?xml version="1.0"?>' . self::NL;
        $xml .= '<response>' . self::NL;
        $xml .= '<sync>getOrders</sync>' . self::NL;
        $xml .= '<list>' . self::NL;
        $xml .= $content;
        $xml .= '</list>' . self::NL;
        $xml .= '</response>' . self::NL;

        return $xml;
    }

    /**
     * Возвращаем список категорий
     * @param $xml
     * @return string
     */
    public function getProducts($xml){

        $_category = Fenix::getModel('catalog/categories')->getCategoryById($xml->parent);
        $category = Fenix::getCollection('catalog/category_category')->setCategory($_category->toArray());


        $productsList = Fenix::getModel('catalog/products')->getProductsListAll($category);
        $productsCollection = Fenix::getCollection('catalog/products')->setList(
            $productsList
        );

        $content = '';
        foreach($productsCollection as $_product){
            $content .= '<product>' . self::NL;
            $content .= self::TAB.'<id_1c>' .$_product->getId1c(). '</id_1c>' . self::NL;
            $content .= self::TAB.'<parent>' .$_product->getParent(). '</parent>' . self::NL;

            $features = Fenix::getModel('catalog/products')->getCardAttributes(
                $_product->getParent()
            );

            $Result   = array();
            $Lang     = Fenix_Language::getInstance();
            $content .= self::TAB . '<attributes>' . self::NL;
            foreach ($features AS $_feature) {
                $attributename = ($_feature->split_by_lang == '1' ? $_feature->sys_title . '_' . $Lang->getCurrentLanguage()->name : $_feature->sys_title);
                $value = $_product->getData($attributename);
                $content .= self::TAB . self::TAB . '<' . $attributename . '>' . self::NL;
                $content .= self::TAB . self::TAB . self::TAB . '<unit>' . $_feature->unit . '</unit>' . self::NL;
                $content .= self::TAB . self::TAB . self::TAB . '<value>' . $value . '</value>' . self::NL;
                $content .= self::TAB . self::TAB . '</' . $attributename . '>' . self::NL;
            }
            $content .= self::TAB . self::TAB . '<price_for>' . self::NL;
            $content .= self::TAB . self::TAB . self::TAB . '<unit>' . $_product->getPriceFor() . '</unit>' . self::NL;
            $content .= self::TAB . self::TAB . self::TAB . '<value>' . $_product->getPriceForQty() . '</value>' . self::NL;
            $content .= self::TAB . self::TAB . '</price_for>' . self::NL;

            $content .= self::TAB . self::TAB . '<price>' . self::NL;
            $content .= self::TAB . self::TAB . self::TAB . '<unit></unit>' . self::NL;
            $content .= self::TAB . self::TAB . self::TAB . '<value>' . $_product->getPrice() . '</value>' . self::NL;
            $content .= self::TAB . self::TAB . '</price>' . self::NL;

            $content .= self::TAB . '</attributes>' . self::NL;

            $content .= '</product>' . self::NL;
        }

$response = '<?xml version="1.0"?>
<response>
    <sync>getProducts</sync>
    <products>
    ' . $content . '
    </products>
</response>
';

        return $response;
    }

    public function syncPrice($xml){

        $content = '';
        $idList = array();
        $counterUpdate = 0;
        $counterError = 0;
        if($xml->products->count()>0){
            try{
            foreach($xml->products->product as $index => $product){

                //Обработка данных
                $importData= array();
                foreach ($product->toArray() as $key => $value) {
                    switch ($key) {
                        default:
                            $importData[$key] = $value;
                    }
                }

                $id_1c = $importData['id_1c'];
                //Проверка товара
                $product = Fenix::getModel('catalog/products')->getProductById1c($id_1c);

                if ($product) {
                    //Обновляем
                    $idList[] = $id_1c;
                    $this->setTable('catalog_products');
                    $this->update($importData, $this->getAdapter()->quoteInto('id = ?', (int)$product->id));

                    //Отчет
                    $content .= '<product>';
                    $content .= '<id>' . $id_1c . '</id>';
                    $content .= '<status>update</status>';
                    $content .= '<error>0</error>';
                    $content .= '</product>' . self::NL;
                    $counterUpdate++;
                } else {
                    //Отчет
                    $content .= '<product>' . self::NL;
                    $content .= '<id>' . $id_1c . '</id>';
                    $content .= '<status>not found</status>';
                    $content .= '<error>1</error>';
                    $content .= '</product>' . self::NL;
                    $counterError++;
                }
            }
                $content .= '<!-- update:' . $counterUpdate . ' error:' . $counterError . '-->';
            }catch (Exception $e){
                //Отчет
                $content .= '<error>critical error parsing data product index:' . $index . '</error>' . self::NL;
                $counterError++;
            }
        }

$response = '<?xml version="1.0"?>
<response>
    <sync>syncPrice</sync>
    <products>
'.$content.'
    </products>
</response>
';
        return $response;
    }

    /**
     * Возвращаем список категорий
     * @param $xml
     * @return string
     */
    public function getCategories($xml){

        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
                       ->select();
        $Select ->from($this);
        $Select ->order('parent asc');
        $categories = $this->fetchAll($Select);

        $content = '';
        foreach ($categories as $_category) {
            /*
             <category>
                    <id></id>                          <!-- (int) ID Категории -->
                    <parent></parent>                  <!-- (int) ID Родительской категории  (1 - Корень) -->
                    <title_russian></title_russian>    <!-- (varchar 255) Название категории -->
                    <title_ukrainian></title_ukrainian><!-- (varchar 255) Название категории -->
             </category>
             */

            $content .= '       <category>'  . self::NL;
            $content .= '           <id>'              . $_category->id            . '</id>' . self::NL;
            $content .= '           <parent>'          . $_category->parent        . '</parent>' . self::NL;
            $content .= '           <title_russian>'   . $_category->title_russian . '</title_russian>' . self::NL;
            //$content .= '           <title_ukrainian>' . $_category->title_ukrainian . '</title_ukrainian>' . self::NL;
            $content .= '       </category>' . self::NL;
        }

$response = '<?xml version="1.0"?>
<response>
    <sync>getCategories</sync>
    <categories>
' . $content . '
    </categories>
</response>
';

        return $response;
    }
    public function syncDiscount($xml){
        /*$response = '<?xml version="1.0"?>
        <response>
        <sync>syncDiscount</sync>       <!-- Синхронизация скидок -->
            <list>
                <item>
                    <discount_card></discount_card>            <!-- (bigint) Номер карты-->
                    <status>create</status> <!-- create|update|error-->
                    <error>0</error>        <!-- код ошибки -->
                </item>
                <!-- повторение тега item -->
            </list>
        </response>
        ';*/

        $content = '';
        if($xml->list->item->count()>0){
            foreach($xml->list->item as $item){

                $discount = Fenix::getModel('customer/backend_discount')->getDiscountById($item->discount_card);
                $data = $item->toArray();

                //Экземпляры товаров
                if ($discount) { //Обновляем
                    $this->setTable('customer_discount_card');
                    $this->update($data, 'id = ' . (int)$discount->id);

                    $content .='<item>';
                    $content .='<discount_card>'.$item->discount_card.'</discount_card>';
                    $content .='<status>update</status>';
                    $content .='<error>0</error>';
                    $content .='</item>';
                } else { //Добавляем
                    $this->setTable('customer_discount_card');
                    $this->insert($data);

                    $content .='<item>'.self::NL;
                    $content .='<discount_card>'.$item->discount_card.'</discount_card>';
                    $content .='<status>create</status>';
                    $content .='<error>0</error>';
                    $content .='</item>';
                }
            }
        }
        $response = '<?xml version="1.0"?>
<response>
    <sync>syncStores</sync>       <!-- Синхронизация магазинов -->
    <list>
        ' . $content . '
    </list>
</response>
';
        return $response;
    }
    public function syncStores($xml){
            $content = '';
            if($xml->list->item->count()>0){
                foreach($xml->list->item as $item){

                    $store = Fenix::getModel('catalog/stores')->getStoreBy1cid($item->id);
                    $data = array(
                        'id_1c'             => $item->id,
                        'title_russian'     => $item->title_russian,
                        'title_ukrainian'   => $item->title_ukrainian,
                        'address_russian'   => $item->address_russian,
                        'address_ukrainian' => $item->address_ukrainian,
                        'city_russian'      => $item->city_russian,
                        'city_ukrainian'    => $item->city_ukrainian,
                        'is_public'         => $item->is_public
                    );

                    if($store){ //Обновление
                        $result =  Fenix::getModel('catalog/stores')->editStore($store, $data);
                        $content .='<item>'.self::NL;
                        $content .='<id>'.$item->id.'</id>';
                        //Отчет
                        if ($result > 0) {
                            $content .= '<status>update</status>';
                            $content .= '<error>0</error>';
                        } else {
                            $content .= '<status>error</status>';
                            $content .= '<error>1</error>';
                        }
                        $content .='</item>';
                    }
                    else{//Добавление
                        $result = Fenix::getModel('catalog/stores')->addStore($data);
                        //Отчет
                        $content .='<item>'.self::NL;
                        $content .='<id>'.$item->id.'</id>';
                        if ($result > 0) {
                            $content .= '<status>create</status>';
                            $content .= '<error>0</error>';
                        } else {
                            $content .= '<status>error</status>';
                            $content .= '<error>1</error>';
                        }
                        $content .='</item>';

                    }
                }
            }
        $response = '<?xml version="1.0"?>
<response>
    <sync>syncStores</sync>       <!-- Синхронизация магазинов -->
    <list>
        ' . $content . '
    </list>
</response>
';
        return $response;
    }

    /*public function setParent(){
        $this->setTable('catalog_products');
        $select = $this->select()->from($this->_name,array('id','parent'));

        $list = $this->fetchAll($select);

        foreach ($list as $product) {
            $categoryId = $product->parent;
            $productId  = $product->id;
            $_category  = Fenix::getModel('catalog/categories')->getCategoryById($categoryId);

            if($_category){

            }else {
                $_category  = Fenix::getModel('catalog/categories')->getCategoryById(2);
            }

                $this->setTable('catalog_relations');

                $data = array(
                    'parent'     => $categoryId,
                    'product_id' => $productId,
                    'left'       => ($_category ? $_category->left : 0),
                    'right'      => ($_category ? $_category->right : 0)
                );

                $this->insert($data);

        }

    }*/
}