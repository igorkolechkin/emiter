<?php
class Fenix_Default_Controller_Index extends Fenix_Controller_Action
{
    public function indexAction()
    {
        Fenix_Debug::log('default controller enter');
        $Creator = Fenix::getCreatorUI();

        $Page = Fenix::getCollection('core/structure')->setUrl();
        // Это страницы?
        if ($Page !== false) {

            //Устанавливаем Meta-данные
            $this->setMeta($Creator, $Page->getCurrent());

            $Creator->getView()->assign(array(
                'current'    => $Page->getCurrent(),
                'navigation' => $Page->getNavigation(),
                'page'       => $Page
            ));
            if ($Page->getCurrent()->getUrlKey() == 'default' && $Page->getCurrent()->getParent() == 1) {

                $Creator->getView()->assign(array(
                    'isMain' => true
                ));

                $Creator->setLayout()
                        ->render('layout/page/main.phtml');
            } else {
                // Хлебные крошки
                $_crumbs = array();
                $_crumbs[] = array(
                    'label' => Fenix::lang("Главная"),
                    'uri' => Fenix::getUrl(),
                    'id' => 'main'
                );

                $url = '';
                foreach ($Page->getNavigation() AS $_page) {
                    $url .= $_page->getUrl() . '/';
                    $_crumbs[] = array(
                        'label' => $_page->getTitle(),
                        'uri' => $url,
                        'id' => 'page_' . $_page->getId()
                    );
                }

                $this->_helper->BreadCrumbs($_crumbs);

                if ($Page->getCurrent()->getTemplate() != '')
                    $Creator->setLayout()
                            ->render('layout/page/' . $Page->getCurrent()->getTemplate() . '.phtml');
                else
                    $Creator->setLayout()
                            ->render('layout/page/regular.phtml');
            }
        }
        // Это товар?
        elseif ($product = Fenix::getModel('catalog/products')->setUrl()) {
            if(Fenix::getRequest()->getUriSegment(1))
            {
                throw new Exception();
            }
            $_controller = new Fenix_Catalog_Controller_Product($this->getRequest(), $this->getResponse());
            $_controller->indexAction($product);
        }
        // Это категория?
        elseif ($category = Fenix::getModel('catalog/categories')->setUrl()) {
            //Показывать наборы?
            if (Fenix::getRequest()->getUriSegment(1) == 'multiset') {
                //Категория с наборами
                $_controller = new Fenix_Catalog_Controller_Index($this->getRequest(), $this->getResponse());
                $_controller->multisetAction($category);
            } else {
                //Просто категория с товарами
                $_controller = new Fenix_Catalog_Controller_Index($this->getRequest(), $this->getResponse());
                $_controller->indexAction($category);
            }
        } else {
            throw new Exception();
        }
    }
}