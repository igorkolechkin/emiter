<?php
class Fenix_Default_Controller_Admin_Index extends Fenix_Controller_Action
{
    /**
     * Панель управления
     */
    public function indexAction()
    {
    	$this->getHelper('rules')->checkRedirect('systemAdminView');

        Fenix::getModel('core/update')->checkUpdatesSilence();




        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => '',
                'id'    => 'main'
            )
        ));

        $Creator = Fenix::getCreatorUI();
        
        $Creator->getView()
                ->headTitle(Fenix::lang("Панель управления"));
        
        $Creator ->setLayout()
                 ->render('layout/page/main.phtml');
    }    
}