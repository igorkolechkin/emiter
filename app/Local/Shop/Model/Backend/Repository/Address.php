<?php

class Local_Shop_Model_Backend_Repository_Address extends Fenix_Resource_Model
{
    public function findAllAddress()
    {
        $Select = $this->createQuerySelect();
        $Select->order('position asc');

        return $this->fetchAll($Select);
    }

    public function createQuerySelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('shop/address');

        $this->setTable('shop_address');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array($this->_name), $Engine->getColumns());

        return $Select;
    }

    public function findAddress($id)
    {
        $Select = $this->createQuerySelect();

        $Select->where('id = ?', (int) $id);

        return $this->fetchRow($Select);
    }
}