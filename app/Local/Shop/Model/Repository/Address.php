<?php

class Local_Shop_Model_Repository_Address extends Fenix_Resource_Model
{
    public function findAllAddresses()
    {
        $Select = $this->createQuerySelect();
        $Select->where('is_public = ?', '1');
        $Select->order('position asc');

        return $this->fetchAll($Select);
    }

    public function findAllDeliveryAddresses()
    {
        $Select = $this->createQuerySelect();
        $Select->where('is_public = ?', '1');
        $Select->where('is_delivery = ?', '1');
        $Select->order('position asc');

        return $this->fetchAll($Select);
    }

    public function createQuerySelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('shop/address');

        $this->setTable('shop_address');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array($this->_name), $Engine->getColumns());

        return $Select;
    }
}