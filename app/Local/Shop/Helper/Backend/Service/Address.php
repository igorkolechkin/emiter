<?php

class Local_Shop_Helper_Backend_Service_Address extends Fenix_Resource_Helper
{
    protected $modelAddressRepository;

    public function __construct()
    {
        $this->modelAddressRepository = Fenix::getModel('shop/backend_repository_address');
    }

    public function getQuery() {
        return $this->modelAddressRepository->createQuerySelect();
    }

    public function getAddress($id) {
        return $this->modelAddressRepository->findAddress($id);
    }
}