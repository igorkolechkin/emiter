<?php

class Local_Shop_Helper_Service_Shop extends Fenix_Resource_Helper
{
    protected $modelAddressRepository;

    public function __construct()
    {
        $this->modelAddressRepository = Fenix::getModel('shop/repository_address');
    }

    public function getAllActiveAddress()
    {
        return $this->modelAddressRepository->findAllAddresses();
    }

    public function getAllActiveDeliveryAddress()
    {
        return $this->modelAddressRepository->findAllDeliveryAddresses();
    }

}