<?php

class Local_Shop_Controller_Admin_Address_Delete extends Local_Shop_Controller_Admin_BaseShop
{
    public function preDispatch()
    {
        parent::preDispatch();

        $this->config = (object) array(
            'pathToXml'            => 'shop/address',
            'messageSuccess'       => Fenix::lang('Адрес магазина успешно удалена'),
            'redirectUri'          => 'shop/address',
            'messageErrorNotFound' => Fenix::lang('Адрес магазина не найден')
        );

        $this->engine = new Fenix_Engine_Database();
        $this->engine->setDatabaseTemplate($this->config->pathToXml);

        $this->engine->prepare()
            ->execute();
    }

    public function indexAction()
    {
        $address = Fenix::getHelper('shop/backend_service_address')->getAddress(
            $this->getRequest()->getParam('id')
        );

        if ($address == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage($this->config->messageErrorNotFound)
                ->saveSession();

            Fenix::redirect($this->config->redirectUri . '/list');
        }

        $Creator = Fenix::getCreatorUI();

        $Creator->loadPlugin('Form_Generator')
            ->setSource($this->config->pathToXml, 'default')
            ->deleteRecord($address);

        $Creator->loadPlugin('Events_Session')
            ->setType(Creator_Events::TYPE_OK)
            ->setMessage($this->config->messageSuccess)
            ->saveSession();

        Fenix::redirect($this->config->redirectUri . '/list');
    }
}