<?php

class Local_Shop_Controller_Admin_Address_Write extends Local_Shop_Controller_Admin_BaseShop
{
    public function preDispatch()
    {
        parent::preDispatch();

        $this->config = (object) array(
            'moduleTitle'          => Fenix::lang('Адреса магазинов'),
            'moduleUrl'            => Fenix::getUrl('shop/address/list'),
            'moduleAddTitle'       => Fenix::lang('Новый адрес магазина'),
            'moduleAddUrl'         => Fenix::getUrl('shop/address/write/add'),
            'moduleEditTitle'      => Fenix::lang('Редактирование адреса магазина'),
            'moduleEditUrl'        => Fenix::getUrl('shop/address/write/edit/id/{$data->id}'),
            'moduleDeleteUrl'      => Fenix::getUrl('shop/address/delete/id/{$data->id}'),
            'pathToXml'            => 'shop/address',
            'messageSuccess'       => Fenix::lang('Новый адрес магазина создан'),
            'messageUpdateSuccess' => Fenix::lang('Адрес магазина отредактирован'),
            'redirectUri'          => 'shop/address',
            'messageErrorNotFound' => Fenix::lang('Адрес магазина не найден')
        );

        $this->engine = new Fenix_Engine_Database();
        $this->engine->setDatabaseTemplate($this->config->pathToXml);

        $this->engine->prepare()
            ->execute();
    }

    public function addAction()
    {
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => $this->config->moduleTitle,
                'uri'   => $this->config->moduleUrl,
                'id'    => 'address'
            ),
            array(
                'label' => $this->config->moduleAddTitle,
                'uri'   => $this->config->moduleAddUrl,
                'id'    => 'add'
            )
        ));

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        // Источник
        $Form->setSource($this->config->pathToXml, 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->addRecord(
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage($this->config->messageSuccess)
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect($this->config->redirectUri . '/list');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect($this->config->redirectUri . '/write/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect($this->config->redirectUri . '/write/edit/id/' . $id);
            }

            Fenix::redirect($this->config->redirectUri . '/list');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle($this->config->moduleAddTitle);

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $address = Fenix::getHelper('shop/backend_service_address')->getAddress(
            $this->getRequest()->getParam('id')
        );

        if ($address == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage($this->config->messageErrorNotFound)
                ->saveSession();

            Fenix::redirect($this->config->redirectUri . '/list');
        }

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => $this->config->moduleTitle,
                'uri'   => $this->config->moduleUrl,
                'id'    => 'address'
            ),
            array(
                'label' => $this->config->moduleEditTitle,
                'uri'   => Fenix::getUrl($this->config->redirectUri . '/write/edit/id/' . $address->id),
                'id'    => 'edit'
            )
        ));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($address->toArray())
            ->setData('current', $address);

        // Источник
        $Form->setSource($this->config->pathToXml, 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->editRecord($address, $this->getRequest());

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage($this->config->messageUpdateSuccess)
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect($this->config->redirectUri . '/list');
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect($this->config->redirectUri . '/write/add');
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect($this->config->redirectUri . '/write/edit/id/' . $id);
            }

            Fenix::redirect($this->config->redirectUri . '/list');
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle($this->config->moduleEditTitle);

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }
}