<?php

class Local_Shop_Controller_Admin_Address_List extends Local_Shop_Controller_Admin_BaseShop
{
    public function preDispatch()
    {
        parent::preDispatch();

        $this->config = (object) array(
            'moduleTitle'     => Fenix::lang('Адреса магазинов'),
            'moduleUrl'       => Fenix::getUrl('shop/address/list'),
            'moduleAddUrl'    => Fenix::getUrl('shop/address/write/add'),
            'moduleEditUrl'   => Fenix::getUrl('shop/address/write/edit/id/{$data->id}'),
            'moduleDeleteUrl' => Fenix::getUrl('shop/address/delete/id/{$data->id}'),
            'pathToXml'       => 'shop/address'
        );

        $this->engine = new Fenix_Engine_Database();
        $this->engine->setDatabaseTemplate($this->config->pathToXml);

        $this->engine->prepare()
            ->execute();
    }

    public function indexAction()
    {
        $addressList = Fenix::getHelper('shop/backend_service_address')->getQuery();

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => $this->config->moduleTitle,
                'uri'   => $this->config->moduleUrl,
                'id'    => 'address'
            )
        ));

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')
                ->setValue(Fenix::lang("Новый адрес"))
                ->setType('button')
                ->appendClass('btn-primary')
                ->setOnclick('self.location.href=\'' . $this->config->moduleAddUrl . '\'')
                ->fetch()
        ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle($this->config->moduleTitle)
            ->setButtonset($Buttonset->fetch());

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('addressList')
            ->setTitle($this->config->moduleTitle)
            ->setData($addressList)
            ->setStandartButtonset();

        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => $this->config->moduleEditUrl
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => $this->config->moduleDeleteUrl
        ));

        // Тайтл страницы
        $Creator->getView()
            ->headTitle($this->config->moduleTitle);

        // Рендер страницы
        $Creator->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch($this->config->pathToXml)
        ));
    }
}