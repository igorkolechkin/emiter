<?php

class Local_Shop_Controller_Admin_BaseShop extends Fenix_Controller_Action
{
    protected $engine;

    protected $config = array();

    public function preDispatch()
    {
        $this->getHelper('rules')->checkRedirect('calculator');

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            )
        ));
    }
}