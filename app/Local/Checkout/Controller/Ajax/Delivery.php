<?php

class Local_Checkout_Controller_Ajax_Delivery extends Fenix_Checkout_Controller_Delivery
{
    public function novaposhtaAction()
    {
        switch ($this->getRequest()->getQuery('action')) {
            case 'warehouse':
                $list = Fenix::getModel('checkout/delivery_novaposhta')->getWarehouseList(
                    $this->getRequest()->getQuery('city')
                );

                echo Zend_Json::encode($list->getData());
                break;
        }

        exit;
    }

    public function citiesAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() === false) {
            exit('Некорректный формат отправки данных на сервер, только Ajax');
        }

        $data = [];
        $cities = Fenix::getModel('checkout/delivery_novaposhta2')->getCitiesList();

        foreach ($cities as $city) {
            $data['cities'][] = $city['name'];
        }

        echo Zend_Json::encode($data);
        exit();
    }

    public function shopsAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() === false) {
            exit('Некорректный формат отправки данных на сервер, только Ajax');
        }

        $data = [];
        $addressList = Fenix::getHelper('shop/service_shop')->getAllActiveDeliveryAddress();

        foreach ($addressList as $address) {
            $tmp = [$address->city, $address->address];
            $data['shops'][] = implode(', ', $tmp);
        }

        echo Zend_Json::encode($data);
        exit();
    }
}