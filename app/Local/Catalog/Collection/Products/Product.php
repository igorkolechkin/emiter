<?php

class Local_Catalog_Collection_Products_Product extends Fenix_Catalog_Collection_Products_Product
{
    public function setShortDescriptionProduct($_product, $priceInfo = null)
    {
        $_product['url'] = Fenix::getUrl($_product['url_key']);
        $_product['url_relative'] = $_product['url_key'];
        $_product['buy'] = Fenix::getUrl('checkout/process/add/pid/' . $_product['id'] . '/');

        // Цены
        $discount = (double)$_product['discount'] ? (double)$_product['discount'] : (isset($_product['sale_discount']) ? (double)$_product['sale_discount'] : 0);
        $priceOld = Fenix::getModel('catalog/products')->getProductPrice($_product);
        if ($priceInfo) {
            $discount = $priceInfo->discount;
            $priceOld = $priceInfo->price_old;
        }

        $_product['unit_price'] = $priceOld;
        $_product['price'] = $priceOld - round($priceOld * $discount / 100);

        if ($discount > 0) {
            $_product['price_old'] = $priceOld;
        }

        $_product['price_formatted'] = str_replace(',', '.',
            Fenix::getHelper('catalog/decorator')->decoratePrice($_product['price']));
        if ($_product['price_old'] > 0) {
            $_product['price_old_formatted'] = Fenix::getHelper('catalog/decorator')->decoratePrice($_product['price_old']);
        }

        if (fmod($_product['price'], 1) > 0) {
            $_product['price'] = round($_product['price'], 2);
        }

        $this->setData($_product);


        return $this;
    }
}