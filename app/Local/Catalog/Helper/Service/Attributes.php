<?php

class Local_Catalog_Helper_Service_Attributes extends Fenix_Resource_Helper
{
    private $attributeRepository;

    private $attributeCommand;

    public function __construct()
    {
        $this->attributeRepository = Fenix::getModel('catalog/repository_attributes');
    }

    public function getAttributeToProduct($productId = 0)
    {
        $data = array();
        $results = $this->attributeRepository->findAttributesToProduct($productId);

        if ($results->count() > 0) {
            foreach ($results as $result) {
                $data[$result->attribute_value_id] = $result->stock;
            }
        }

        return $data;
    }

    public function updateAttributeToStock($data = array())
    {
        if ( ! $data) {
            return;
        }

        $this->attributeCommand = Fenix::getModel('catalog/command_attributes');

        $this->attributeCommand->updateAttributeToStock($data);
    }
}