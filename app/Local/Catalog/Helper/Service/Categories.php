<?php

class Local_Catalog_Helper_Service_Categories extends Fenix_Resource_Helper
{
    protected $model;

    protected $menuItems = [];

    protected $menuItemsWithChildren = [];

    public function __construct()
    {
        $this->model = Fenix::getModel('catalog/repository_categories');
    }

    public function renderMenu($parent_id = 0, $depth = 1)
    {
        $this->menuItems = $this->model->findAllCategories();

        $this->createMenuWithChildren();

        return $this->buildTree($parent_id, $depth);
    }

    public function renderColumnMenu()
    {
        $this->menuItems = $this->model->findAllCategories();

        $this->createMenuWithChildren();

        $results = Fenix::getModel('catalog/repository_builder')->findAll();

        $columns = [];
        foreach ($results as $result) {
            $columns[$result['parent']][$result['column_number']][] = $result;
        }

        $html = '<ul id="mainMenu" class="nav flex-column">';
        foreach ($this->menuItemsWithChildren[1] as $parent) {
            $icon = '';
            if (isset($columns[$parent->id])) {
                $html .= '<li class="nav-item dropdown">';
                $icon = '<i class="icon-arrow_menu_right"></i>';
            } else {
                $html .= '<li class="nav-item">';
            }
            $html .= '<a href="' . Fenix::getUrl($parent->url_key) . '" class="nav-link">' . $parent->title . $icon . '</a>';

            if (isset($columns[$parent->id])) {
                $html .= '<div class="dropdown-menu">';
                $html .= '<div class="container-fluid">';
                $html .= '<div class="row">';
                foreach ($columns[$parent->id] as $column) {
                    $html .= '<ul class="col">';
                    foreach ($column as $item) {
                        if (isset($this->menuItemsWithChildren[$item['id']])) {
                            $html .= '<li class="children-menu">';
                        } else {
                            $html .= '<li>';
                        }
                        $html .= '<a href="' . Fenix::getUrl($item['url_key']) . '" class="nav-link">' . $item['title'] . '</a>';
                        if (isset($this->menuItemsWithChildren[$item['id']])) {
                            $html .= '<ul>';
                            foreach ($this->menuItemsWithChildren[$item['id']] as $child) {
                                $html .= '<li>';
                                $html .= '<a href="' . Fenix::getUrl($child['url_key']) . '" class="nav-link">' . $child['title'] . '</a>';
                                $html .= '</li>';
                            }
                            $html .= '</ul>';
                        }
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                }
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            }

            $html .= '</li>';
        }
        $html .= '</ul>';

        return $html;
    }

    public function getCategoriesForCard()
    {
        $where = [
            'where' => [
                'on_main' => '1'
            ]
        ];

        $this->menuItems = $this->model->findCategoriesByConditions($where);

        return $this->createCard();
    }

    public function getCategoriesForCardWithChildren()
    {
        $this->menuItems = $this->model->findAllCategories();

        $this->createMenuWithChildren();

        return $this->createCardWithChildren();
    }

    public function clearMenuItems()
    {
        $this->menuItems = [];

        return $this;
    }

    protected function createMenuWithChildren()
    {
        foreach ($this->menuItems as $item) {
            if ($item->parent > 0) {
                $this->menuItemsWithChildren[$item->parent][] = $item;
            }
        }
    }

    protected function createCard()
    {
        $data = [];

        foreach ($this->menuItems as $item) {
            if ($item->parent == 1) {
                $tmp = [
                    'title' => $item->title,
                    'url'   => Fenix::getUrl($item->url_key),
                    'image' => ''
                ];

                $filename = '';
                if ($item->image_main_page != '') {
                    $filename = Fenix::getUploadImagePath($item->image_main_page);
                }

                if (file_exists($filename)) {
                    $tmp['image'] = Fenix::getUploadImageUrl($item->image_main_page);
                }

                $data[] = $tmp;
            }
        }

        return $data;
    }

    protected function createCardWithChildren()
    {
        $data = [];
        foreach ($this->menuItems as $item) {
            if ($item->parent == 1) {
                $tmp = [
                    'title'    => $item->title,
                    'url'      => Fenix::getUrl($item->url_key),
                    'image'    => '',
                    'children' => []
                ];

                $filename = Fenix::getUploadImagePath($item->image_category);

                if (file_exists($filename)) {
                    $tmp['image'] = Fenix_Image::frame($item->image_category, 360, 288);
                }

                $where = [
                    'where' => [
                        'parent' => $item->id
                    ]
                ];

                $childrenResults = $this->model->findCategoriesByConditions($where, 5);
                if ($childrenResults) {
                    foreach ($childrenResults as $children) {
                        $tmp['children'][] = [
                            'title' => $children->title,
                            'url'   => Fenix::getUrl($children->url_key)
                        ];
                    }
                }

                $data[] = $tmp;
            }
        }

        return $data;
    }

    /**
     * Рекурсивная функция построения меню
     *
     * @param    int $parent_id Текущее значение родительского пункта
     * @param    int $depth Глубина меню
     * @return    string    Отформатированное меню с html тегами
     */
    protected function buildTree($parent_id = 0, $depth = 1)
    {
        static $level = 0;
        $level++;

        if (($depth > 0) && ($depth < $level)) {
            $level--;

            return null;
        }

        $tree = '';

        if (is_array($this->menuItemsWithChildren) && isset($this->menuItemsWithChildren[$parent_id])) {
            $cssId = '';
            $cssClass = '';

            if ($level == 1) {
                $cssId = ' id="mobileMenu"';
                $cssClass = '';
            }

            if ($level >= 2) {
                $cssClass = 'class="dropdown-menu"';
            }

            $tree .= '<ul' . $cssId . ' ' . $cssClass . '>';

            foreach ($this->menuItemsWithChildren[$parent_id] as $item) {
                $icon = '';

                $url = Fenix::getUrl($item->url_key);

                if (isset($this->menuItemsWithChildren[$item->id])) {
                    $tree .= '<li class="nav-item dropdown">';
                    $icon = '<i class="icon-arrow_menu_right"></i>';
                } else {
                    $tree .= '<li class="nav-item">';
                }

                $tree .= '<a href="' . $url . '" class="nav-link">' . $item->title . $icon . '</a>';

                $tree .= $this->buildTree($item->id, $depth);

                $tree .= '</li>';
            }

            $tree .= '</ul>';
        } else {
            $level--;

            return null;
        }
        $level--;

        return $tree;
    }
}