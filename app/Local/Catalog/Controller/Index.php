<?php

class Local_Catalog_Controller_Index extends Fenix_Catalog_Controller_Index
{
    public function indexAction($categoryInfo = null)
    {
        Fenix_Debug::log('indexAction begin');

        if ($categoryInfo == null) {
            $this->_rootAction();
        } else {

            $subCategories = Fenix::getModel('catalog/categories')->getNavCategoriesList($categoryInfo->getCurrent()->getId());

            if ($subCategories->count() > 0) {
                $this->_categoriesAction($categoryInfo, $subCategories);
            } else {
                $this->_listAction($categoryInfo);
            }
        }

        Fenix_Debug::log('indexAction end');
    }

    private function _rootAction()
    {

        Fenix_Debug::log('_rootAction begin');

        $categoryInfo = Fenix::getModel('catalog/categories')->setUrl();
        if ($categoryInfo) {
            $currentCategory = $categoryInfo->getCurrent();
        }
        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        $_crumbs[] = array(
            'label' => Fenix::lang("Каталог"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'catalog'
        );

        $this->_helper->BreadCrumbs($_crumbs);

        // Отображение
        $Creator = Fenix::getCreatorUI();

        if ( ! empty($currentCategory)) {
            //Устанавливаем Meta-данные
            $this->setMeta($Creator, $currentCategory);
        }

        $Creator->getView()->assign(array(
            'categoryList' => Fenix::getHelper('catalog/service_categories')->clearMenuItems()->getCategoriesForCardWithChildren()
        ));

        //Корень каталога
        $Creator->setLayout()
            ->render('catalog/root.phtml');

        Fenix_Debug::log('_rootAction end');
    }

    private function _categoriesAction($categoryInfo, $subCategories)
    {
        Fenix_Debug::log('_categoriesAction begin');

        $currentCategory = $categoryInfo->getCurrent();
        $currentNavigation = $categoryInfo->getNavigation();

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        foreach ($currentNavigation AS $_category) {
            $_crumbs[] = array(
                'label' => $_category->getTitle(),
                'uri'   => $_category->getUrl(),
                'id'    => 'category_' . $_category->getId()
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        // Отображение
        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $currentCategory);

        $Creator->getView()->assign(array(
            'category'     => $currentCategory,
            'navigation'   => $currentNavigation,
            'collection'   => $categoryInfo,
            'categoryList' => $subCategories
        ));

        //Список товаров
        $Creator->setLayout()
            ->render('catalog/categories.phtml');

        Fenix_Debug::log('index controller end');
    }

    private function _listAction($categoryInfo)
    {
        Fenix_Debug::log(' begin');
        if (Fenix::getRequest()->getQuery('perPage')) {
            $perPage = Fenix::getRequest()->getQuery('perPage');
        } else {
            $perPage = Fenix::getConfig('catalog_products_per_page') ? Fenix::getConfig('catalog_products_per_page') : Fenix::DEFAULT_PER_PAGE;
        }

        /**
         * Обработка пост запросов
         */
        if ($this->getRequest()->isXmlHttpRequest()) {
            switch ($this->getRequest()->getPost('action')) {
                case 'updateFilter':
                    $attribute = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                        $this->getRequest()->getPost('attribute')
                    );

                    if ($attribute == null) {
                        exit;
                    }

                    echo Fenix::getModel('catalog/filter')->prepareFilterUrl(
                        $attribute,
                        (object)array(
                            'value' => $this->getRequest()->getPost('min') . '-' . $this->getRequest()->getPost('max')
                        )
                    );
                    break;
                case 'load-more':
                    if ($categoryInfo) {
                        $currentCategory = $categoryInfo->getCurrent();
                    } else {
                        $currentCategory = null;
                    }

                    //Список товаров
                    $paginator = Fenix::getModel('catalog/products')->getProductsList($currentCategory);

                    $productsList = array();
                    foreach ($paginator as $item) {
                        $productsList[] = Fenix::getCollection('catalog/products_product')->setProduct($item->toArray());
                    }
                    $productsList = new Fenix_Object_Rowset(array('data' => $productsList, 'rowClass' => false));

                    $Creator = Fenix::getCreatorUI();
                    $Creator->getView()->assign(array(
                        'category'     => $currentCategory,
                        'productsList' => $productsList,
                        'perPage'      => $perPage
                    ));
                    //Список товаров
                    echo $Creator->getView()
                        ->render('catalog/load-more.phtml');
                    break;
            }
            exit;
        }

        $currentCategory = $categoryInfo->getCurrent();
        $currentNavigation = $categoryInfo->getNavigation();

        //Список товаров
        $paginator = Fenix::getModel('catalog/products')->getProductsList($currentCategory);

        $productsList = Fenix::getCollection('catalog/products')->setList($paginator);

        // Хлебные крошки
        $_crumbs = array();
        $_crumbs[] = array(
            'label' => Fenix::lang("Главная"),
            'uri'   => Fenix::getUrl(),
            'id'    => 'main'
        );

        foreach ($currentNavigation AS $_category) {
            $_crumbs[] = array(
                'label' => $_category->getTitle(),
                'uri'   => $_category->getUrl(),
                'id'    => 'category_' . $_category->getId()
            );
        }

        $this->_helper->BreadCrumbs($_crumbs);

        $Creator = Fenix::getCreatorUI();

        //Устанавливаем Meta-данные
        $this->setMeta($Creator, $currentCategory, $paginator);

        $banner = Fenix::getCollection('banners/banners')->getActive($currentCategory->banner_id);
        if (count($banner) > 0) {
            $Creator->getView()->assign(array(
                'banner' => (object)$banner
            ));
        }

        $Creator->getView()->assign(array(
            'category'   => $currentCategory,
            'navigation' => $currentNavigation,
            'collection' => $categoryInfo,
            'products'   => $productsList,
            'paginator'  => $paginator,
            'perPage'    => $perPage,
            'utpItems'   => Fenix::getHelper('core/service_utp')->setUtpName('utp.on.hover')->getUtpItems()
        ));

        //Список товаров
        $Creator->setLayout()
            ->render('catalog/category.phtml');

        Fenix_Debug::log('_listAction end');
    }

    public function getMeta($currentCategory, Zend_Paginator $paginator = null)
    {
        /** Массив meta-полей */
        $metaArr = parent::getMeta($currentCategory, $paginator)->getData();

        if (isset($metaArr['robots']) && ! empty($metaArr['robots'])) {
            $page_robots[] = $metaArr['robots'];
        }


        /** ПЕРЕОПРЕДЕЛЕНИЕ META тег ROBOTS */
        // используется ли фильтр
        $filter = Fenix::getRequest()->getParam('filter');

        // по умолчанию "Для категорий без фильтра" - имеет самый низкий приоритет (index, follow)
        $page_robots[] = Fenix::getConfig('seo/filter/robots_catalog');

        $is_index = true;
        $filter_attributes = array();
        if ($filter) {
            /** Поиск атрибута, коотрого не нужно индексировать */
            $filter_attributes = Fenix::getModel('catalog/filter')->parseFilterString($filter);

            foreach ($filter_attributes AS $_attribute => $_values) {
                $attributeData = Fenix::getModel('catalog/backend_attributes')->getAttributeBySysTitle(
                    $_attribute
                );
                if ($attributeData && ! $attributeData->is_index) {
                    $is_index = false;
                }
            }

            if ($is_index) {
                if (count($filter_attributes) > 1) {
                    // "Для фильтра из нескольких атрибутов" - noindex, nofollow
                    $page_robots[] = Fenix::getConfig('seo/filter/robots_many_attributes');
                } else {
                    $first = array_shift(array_slice($filter_attributes, 0));
                    if (count($first) > 1) {
                        // "Для фильтра из нескольких значений одного атрибута" - noindex, nofollow
                        $page_robots[] = Fenix::getConfig('seo/filter/robots_many_values');
                    } else {
                        // "Для фильтра из одного атрибута и значения" - index, follow
                        $page_robots[] = Fenix::getConfig('seo/filter/robots_attribute_with_value');
                    }
                }
            } else {
                $page_robots[] = 'noindex, nofollow';
            }
        }

        // Получаем самый приоритетный robots
        $page_robots = array_unique($page_robots);
        sort($page_robots);
        $page_robots = array_pop($page_robots);

        $metaArr['robots'] = $page_robots;

        /** ПЕРЕОПРЕДЕЛЕНИЕ META тега CANONICAL для страниц фильтра */
        if ($filter) {
            //устанавливаем canonical для страниц с индексируемым фильтром
            if ($is_index && count($filter_attributes) == 1) {
                $metaArr['canonical'] = $metaArr['options']['currentUrl'] . '/filter/' . $filter;
            }
        }

        return new Fenix_Object_Rowset(array('data' => $metaArr));
    }
}