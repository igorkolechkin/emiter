<?php

class Local_Catalog_Controller_Ajax_Configurable extends Fenix_Controller_Action
{
    public function priceAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() === false) {
            exit();
        }

        $params = Fenix::getRequest()->getPost('params');
        $productId = Fenix::getRequest()->getPost('product_id');
        $product = Fenix::getModel('catalog/products')->getProductById($productId);

        if (isset($params['attributes'])) {
            $values = $params['attributes'];
        } else {
            $values = array();
        }

        $priceInfo = Fenix::getModel('catalog/products_configurable')->getProductPrice($product, $values);

        echo Zend_Json::encode($priceInfo->toArray());
        /*if ($priceInfo) {*/
//        $Creator = Fenix::getCreatorUI();
//        $Creator->assign('priceInfo', $priceInfo);
//        $Creator->render('catalog/products/configurable/price-block.phtml');
        /*}else{
            echo 'error';
        }*/
    }

    public function valuesAction()
    {
        $params = Fenix::getRequest()->getPost('params');
        $attr_sys_title = Fenix::getRequest()->getPost('sys_title');

        $values = Fenix::getModel('catalog/configurable')->getAttributeValuesByParams($attr_sys_title, $params);

        $data = array();
        foreach ($values AS $item) {
            $data[] = array(
                'value' => $item->value
            );
        }

        echo Zend_Json::encode($data);
        exit;
    }

    public function multisetAction()
    {

        //Цена товара по выбранным параметрам
        $params = Fenix::getRequest()->getPost('params');
        $priceInfo = Fenix::getModel('catalog/configurable')->getPriceInfo($params);


        //Товар
        $productId = $params['product_id'];
        $product = Fenix::getModel('catalog/products')->getProductbyId($productId);
        $product = Fenix::getCollection('catalog/products_product')->setProduct(
            $product->toArray(), $priceInfo
        );


        $Creator = Fenix::getCreatorUI();
        $Creator->assign('priceInfo', $priceInfo);
        $Creator->assign('current', $product);
        $Creator->assign(array(
            'title'     => null,
            'class'     => 'set-products',
            'slider_id' => 'set-products',
            'multiset'  => $product->getMultiSetList('1'),
            'current'   => $product
        ));
        $Creator->render('catalog/products/slider-multiset.phtml');

    }
}