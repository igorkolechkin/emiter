<?php

class Local_Catalog_Model_Backend_Attributes extends Fenix_Catalog_Model_Backend_Attributes
{
    private $attributeTypesForExclude = array('double', 'enum', 'int', 'text');

    private $newSortedValues = array();

    private $attributeByTypeValue = array();

    protected function additionalSorting($typeAttribute, $data = array())
    {
        mb_internal_encoding('UTF-8');
        $sortedValues = $data->toArray();
        unset($data);

        if ( ! $typeAttribute) {
            return $sortedValues;
        }

        if ( ! $sortedValues) {
            return $sortedValues;
        }

        $typeAttribute = mb_strtolower($typeAttribute);
        if (in_array($typeAttribute, $this->attributeTypesForExclude)) {
            return $sortedValues;
        }

        $newData = array();
        $tmp = array();

        foreach ($sortedValues as $value) {
            $value['content'] = $this->trimValue($value['content']);

            if ($value['content'] == '') {
                continue;
            }

            $newData[$value['id']] = mb_strtolower($value['content']);

            $tmp[$value['id']] = $value;
//            if ($this->isIntValue($value) === true) {
//                continue;
//            }
//
//            if ($this->isFloatValue($value) === true) {
//                continue;
//            }
//
//            if ($this->isTwoNumber($value) === true) {
//                continue;
//            }
//
//            if ($this->isThreeNumber($value) === true) {
//                continue;
//            }
//
//            if ($this->isNumberWithWords($value) === true) {
//                continue;
//            }
//
//            if ($this->isTwoNumberWithWords($value) === true) {
//                continue;
//            }
//
//            if ($this->isThreeNumberWithSlash($value) === true) {
//                continue;
//            }
//
//            if ($this->isTwoNumberWithString($value) === true) {
//                continue;
//            }
//
//            if ($this->isStringWithNumber($value) === true) {
//                continue;
//            }

//            $this->setNewContentForAttribute('string', $value, $value['content']);
        }

//        foreach ($this->attributeByTypeValue as $key => $value) {
//            $method = $this->createSortFunction($key);
//
//            if ($method) {
//                $this->$method($value);
//            }
//            else {
//                continue;
//            }
//        }

        natcasesort($newData);

        foreach ($newData as $id => $value) {
            $this->newSortedValues[] = (object) $tmp[$id];
        }
//        exit();
//        Fenix::dump($this->attributeByTypeValue);
//        Fenix::dump($this->newSortedValues);
//        Fenix::dump($newData, $this->newSortedValues);

        return $this->newSortedValues;
    }

    public function getSortedValues()
    {
        return $this->newSortedValues;
    }

    public function trimValue($value = '')
    {
        $value = trim($value);

        return $value;
    }

    private function setNewContentForAttribute($type, $value, $newContent)
    {
        $tmp['new_content'] = $newContent;
        $result = array_merge($value, $tmp);

        $this->attributeByTypeValue[$type][$value['id']] = $result;
    }

    private function isIntValue($value)
    {
        $re = '/^(\d+)$/';

        preg_match_all($re, $value['content'], $matches);

        if ( ! empty($matches[1])) {
            $this->setNewContentForAttribute('integer', $value, $matches[1][0]);

            return true;
        }
        else {
            return false;
        }
    }

    private function isFloatValue($value)
    {
        $re = '/^(\d+\.\d+)$/';

        preg_match_all($re, $value['content'], $matches);
        if ( ! empty($matches[1])) {
            $this->setNewContentForAttribute('float', $value, $matches[1][0]);

            return true;
        }
        else {
            return false;
        }
    }

    private function isTwoNumber($value)
    {
        $re = '/^(\d+)(\D+)(\d+)$/';

        preg_match_all($re, $value['content'], $matches);
        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3]))) {
            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0],
            );

            $this->setNewContentForAttribute('two_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isThreeNumber($value)
    {
        $re = '/^(\d+)(\D+)(\d+)(\D+)(\d+)$/';

        preg_match_all($re, $value['content'], $matches);
        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3])) && ( ! empty($matches[4])) && ( ! empty($matches[5]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0],
                $matches[4][0],
                $matches[5][0],
            );

            $this->setNewContentForAttribute('three_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isNumberWithWords($value)
    {
        $re = '/^(\d+)(\s+)(\D+)$/';

        preg_match_all($re, $value['content'], $matches);

        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0]
            );

            $this->setNewContentForAttribute('number_with_words', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isTwoNumberWithWords($value)
    {
        $re = '/^(\d+)(\D+)(\d+)(\s+)(\D+)$/';

        preg_match_all($re, $value['content'], $matches);

        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3])) && ( ! empty($matches[4])) && ( ! empty($matches[5]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0],
                $matches[4][0],
                $matches[5][0]
            );

            $this->setNewContentForAttribute('two_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isThreeNumberWithSlash($value)
    {
        $re = '/^(\d+)(\D+)(\d+)(\D+)(\d+)(\D+)(\d+)$/';

        preg_match_all($re, $value['content'], $matches);

        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3])) && ( ! empty($matches[4])) && ( ! empty($matches[5])) && ( ! empty($matches[6])) && ( ! empty($matches[7]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0],
                $matches[4][0],
                $matches[5][0],
                $matches[6][0],
                $matches[7][0]
            );

            $this->setNewContentForAttribute('three_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isTwoNumberWithString($value)
    {
        $re = '/^(\d+)(\D+)(\d+)(\D+)(.*)$/';

        preg_match_all($re, $value['content'], $matches);

        if (( ! empty($matches[1])) && ( ! empty($matches[2])) && ( ! empty($matches[3])) && ( ! empty($matches[4])) && ( ! empty($matches[5]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
                $matches[3][0],
                $matches[4][0],
                $matches[5][0]
            );

            $this->setNewContentForAttribute('two_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    private function isStringWithNumber($value)
    {
        $re = '/^(\D+)(\d+)$/';

        preg_match_all($re, $value['content'], $matches);

        if (( ! empty($matches[1])) && ( ! empty($matches[2]))) {

            $data = array(
                $matches[1][0],
                $matches[2][0],
            );

            $this->setNewContentForAttribute('string_with_number', $value, $data);

            return true;
        }
        else {
            return false;
        }
    }

    public function sortInteger($data = array())
    {
        $newData = array();
        foreach ($data as $value) {
            $newData[$value['id']] = $value['new_content'];
        }

        asort($newData, SORT_NUMERIC);

        foreach ($newData as $id => $value) {
            unset($data[$id]['new_content']);
            $this->newSortedValues['integer'][] = $data[$id];
        }
    }

    public function sortFloat($data = array())
    {
        $newData = array();
        foreach ($data as $value) {
            $newData[$value['id']] = $value['new_content'];
        }

        asort($newData, SORT_NUMERIC);

        foreach ($newData as $id => $value) {
            unset($data[$id]['new_content']);
            $this->newSortedValues['float'][] = $data[$id];
        }
    }

    public function sortString($data = array())
    {
        $newData = array();
        foreach ($data as $value) {
            $newData[$value['id']] = $value['new_content'];
        }

        natsort($newData);

        foreach ($newData as $id => $value) {
            unset($data[$id]['new_content']);
            $this->newSortedValues['string'][] = $data[$id];
        }
    }

    public function sortTwoNumber($data = array())
    {
        $newData = array();

        foreach ($data as $value) {
            $newData[$value['id']] = $value['content'];
        }

        natsort($newData);

        foreach ($newData as $id => $value) {
            unset($data[$id]['new_content']);
            $this->newSortedValues['string'][] = $data[$id];
        }
    }

    public function sortThreeNumber($data = array())
    {
        $newData = array();

        foreach ($data as $value) {
            $newData[$value['id']] = $value['new_content'];
        }

        natsort($newData);

        foreach ($newData as $id => $value) {
            unset($data[$id]['new_content']);
            $this->newSortedValues['three_number'][] = $data[$id];
        }
        Fenix::dump($this->newSortedValues['three_number']);
    }

    private function createSortFunction($key)
    {
        echo $method = 'sort' . str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $key)));
        if (method_exists($this, $method)) {
            return $method;
        }
    }

    //TO DO: обновить класс сделать через хелперы то что не относится к модели
    static function cmp($a, $b)
    {
        $a = strtolower($a[0]);
        $b = strtolower($b[0]);

        return strcasecmp($a, $b);
    }

    public function findAttributeById($id = 0)
    {
        $id = (int) $id;
        if ( ! $id) {
            return 0;
        }

        $Select = parent::getAttributesListAsSelect();

        $Select->where('id = ?', $id);

        $resultList = $this->fetchAll($Select);

        if ($resultList->count() == 0) {
            return 0;
        }
    }

    public function updateAttributePosition($id = 0)
    {
        $id = (int) $id;
        if ( ! $id) {
            return null;
        }
        $Select = $this->getAttributesListAsSelect();
        $Select->where('id = ?', $id);

        $resultList = $this->fetchAll($Select);

        foreach ($resultList as $attribute) {
            $tableName = $this->getAttributeTable($attribute);

            $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

            $cols[] = new Zend_Db_Expr('COUNT(avr.id) as relations_count');
            $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
                ->from(array(
                    'avc' => $this->getTable()
                ), $cols)
                ->joinLeft(array(
                    'avr' => $this->getTable('attr_values')
                ), 'avr.value_id = avc.id AND '
                    . 'avr.attribute_id=  avc.attribute_id', null);

            $Select->where('avc.attribute_id = ?', $attribute->id);
            $Select->group('avc.id');

            if ($attribute->split_by_lang == '1') {
                $lang = Fenix_Language::getInstance()->getCurrentLanguage();
                $Select->order('avc.content_' . $lang->name . ' asc');
            }
            else {
                $Select->order('avc.content asc');
            }

            $sortedValues = $this->fetchAll($Select);

            $sortedValues = self::additionalSorting($attribute->sql_type, $sortedValues);

            foreach ($sortedValues as $position => $value) {
                $data = array(
                    'position' => $position
                );
                $this->setTable($tableName);
                $this->update($data, $this->getAdapter()->quoteInto('id = ?', $value->id));
            }
        }
    }

    public function getDataTypes()
    {
        return array(
            'varchar',
            'text',
            'enum',
            'int',
            'double',
        );
    }

    public function getAttributeValuesInfoPaginator($attribute, $productId = null, $page = 1)
    {
        $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

        $cols = Fenix_Engine::getInstance()->setSource('catalog/' . $tableName)->getColumns();

        $cols[] = new Zend_Db_Expr('COUNT(avr.id) as relations_count');
        $Select = $this->setTable($tableName)->select()->setIntegrityCheck(false)
            ->from(array(
                'avc' => $this->getTable()
            ), $cols)
            ->joinLeft(array(
                'avr' => $this->getTable('attr_values')
            ), 'avr.value_id = avc.id AND '
                . 'avr.attribute_id=  avc.attribute_id', null);

        if ($productId) {
            $Select->where('avr.product_id = ?', $productId);
        }

        $Select->where('avc.attribute_id = ?', $attribute->id);
        $Select->group('avc.id');
        $Select->order('avc.position asc');

        //$Result = $this->fetchAll($Select);
        //Сколько товаров в категории
        $CountSelect = clone $Select;
        $CountSelect->reset(Zend_Db_Select::COLUMNS);
        $CountSelect->columns(
            array(
                new Zend_Db_Expr('COUNT(DISTINCT avc.id) as values_count')
            )
        );
        $CountSelect->reset(Zend_Db_Select::GROUP);
        $CountSelect->reset(Zend_Db_Select::ORDER);
        $CountSelect->limit(1);

        $countResult = $this->fetchRow($CountSelect);
        if ($countResult) {
            $Count = (int)$countResult->values_count;
        } else {
            $Count = 0;
        }

        $perPage = 50;

        $page = Fenix::getRequest()->getQuery("page");

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($Select);
        $adapter->setRowCount($Count);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber((int)$page)
            ->setItemCountPerPage($perPage);

//Fenix::dump($paginator);
        return $paginator;
    }

    /**
     * Удаление значения атрибута
     * @param $attributeSysTitle
     * @param $valueId
     */
    public function deleteAttributeValue($attributeSysTitle, $valueId)
    {
        $attribute = $this->getAttributeBySysTitle($attributeSysTitle);
        if ($attribute) {
            $tableName = Fenix::getModel('catalog/backend_attributes')->getAttributeTable($attribute);

            //удаляем значение
            $this->setTable($tableName)
                ->delete($this->getAdapter()->quoteInto('id = ?', $valueId));

            //удаляем связь с товарами
            $this->setTable('attr_values')
                ->delete(
                    $this->getAdapter()->quoteInto('attribute_id = ?', $attribute->id) . ' AND ' .
                    $this->getAdapter()->quoteInto('value_id = ?', $valueId)
                );
        }
    }
}