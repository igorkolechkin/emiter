<?php

/**
 * Class Fenix_Catalog_Model_Backend_Configurable_Attributes
 */
class Local_Catalog_Model_Configurable_Attributes extends Fenix_Resource_Model
{

    /**
     * @param $productId
     * @param $attributeId
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributeToProductRelation($productId, $attributeId)
    {

        $this->setTable('product_conf_attr');

        $Select = $this->select();
        $Select ->from($this->_name);
        $Select ->where('product_id = ?', $productId);
        $Select ->where('attribute_id = ?', $attributeId);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    /**
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributesList($productId){

        $Select = Fenix::getModel('catalog/attributes')->getAttributesListAsSelect();
        $this->setTable('product_conf_attr');
        $Select->join(array(
            'pca' => $this->getTable('product_conf_attr')
        ), 'a.id = pca.attribute_id', null);
        $Select->where('pca.product_id = ?', $productId);

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    /**
     * @param $productId
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getAttributesIdList($productId){

        $Select = Fenix::getModel('catalog/backend_attributes')->getAttributesListAsSelect();
        $this->setTable('product_conf_attr');
        $Select->join(array(
            'pca' => $this->getTable('product_conf_attr')
        ),'a.id = pca.attribute_id', null);
        $Select ->where('pca.product_id = ?', $productId);

        $Result = $this->fetchAll($Select);
        $data   = array();
        foreach ($Result as $item) {
            $data[] = $item->attribute_id;
        }
        return $Result;
    }
}