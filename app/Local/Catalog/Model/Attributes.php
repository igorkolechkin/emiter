<?php

class Local_Catalog_Model_Attributes extends Fenix_Resource_Model
{
    const ATTRIBUTE_SOURCE_SYSTEM = 'system';
    const ATTRIBUTE_SOURCE_USER   = 'user';

    public function getAttributesListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes_frontend');

        $this->setTable('catalog_attributes');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns());

        $Select->order('a.title_russian asc');

        return $Select;
    }

    /**
     * Формируем название таблицы для хранения значений атрибута
     * @param Zend_Db_Table_Row $attribute
     * @return string
     */
    public function getAttributeTable($attribute)
    {
        $tableName = 'attr_values_' . strtolower($attribute->sql_type);
        if ($attribute->split_by_lang == '1') {
            $tableName .= '_lang';

            return $tableName;
        }

        return $tableName;
    }
}