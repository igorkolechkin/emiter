<?php

class Local_Catalog_Model_Repository_Categories extends Fenix_Catalog_Model_Categories
{
    public function findAllCategories()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getColumns());

        $Select->where('is_public = ?', '1');
        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function findCategoriesByConditions($condition = array(), $limit = 0)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/categories');

        $Select = $this->setTable('catalog')
            ->select();

        $Select->from($this, $Engine->getColumns());

        $Select->where('is_public = ?', '1');

        if ($condition) {
            array_walk($condition, function ($value, $key) use ($Select) {
                if (method_exists($Select, $key)) {
                    foreach ($value as $field => $param) {
                        $Select->$key($field . '= ?', $param);
                    }
                }
            });
        }

        $limit = (int)$limit;

        if ($limit) {
            $Select->limit($limit);
        }

        $Select->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }
}