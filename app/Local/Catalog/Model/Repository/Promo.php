<?php

class Local_Catalog_Model_Repository_Promo extends Fenix_Resource_Model
{
    public function findBlockByName($name)
    {
        $this->setTable('catalog_promo');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('name = ?', $name);
        $Select->where('is_public = ?', '1');
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function findAllBlockByActive() {
        $this->setTable('catalog_promo');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('is_public = ?', '1');
        $Select->order('position asc');

        return $this->fetchAll($Select);

    }

    public function findProductsByBlockId($blockId)
    {
        $this->setTable('catalog_promo_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'cpp' => $this->getTable()
        ), null);

        // Джоиним товарчеги
        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'cpp.product_id = p.id', Fenix::getModel('catalog/products')->productColumns());

        $Select->where('cpp.block_id = ?', (int) $blockId);
        $Select->where('p.is_public = ?', '1');
        $Select->where('p.is_visible = ?', '1');
        $Select->order('cpp.position asc');

        return $this->fetchAll($Select);
    }
}