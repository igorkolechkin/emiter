<?php

class Local_Catalog_Model_Repository_Attributes extends Fenix_Catalog_Model_Backend_Attributes
{
    public function findAttributeToStock()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('catalog/attributes');

        $this->setTable('catalog_attributes');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('check_in_stock = ?', '1');

        return $this->fetchAll($Select);
    }

    public function findAttributesToProduct($product_id)
    {
        $this->setTable('catalog_product_attribute_stock');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('product_id = ?', (int)$product_id);

        return $this->fetchAll($Select);
    }
}