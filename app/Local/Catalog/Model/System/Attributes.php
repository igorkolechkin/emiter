<?php
class Local_Catalog_Model_System_Attributes extends Fenix_Resource_Model
{
    /**
     * Стандартный Select для атрибутов
     * @return Zend_Db_Select
     */
    public function getAttributesListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/system_attributes_frontend');

        $this   ->setTable('catalog_system_attributes');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from(array(
            'a' => $this->_name
        ), $Engine ->getColumns());

        $Select ->order('a.title_russian asc');

        return $Select;
    }

    /**
     * Список атрибутов помеченных как активные
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getActiveAttributesList()
    {
        $Select = $this->getAttributesListAsSelect();
        $Select ->where('a.is_active = ?', '1');

        $Result = $this->fetchAll($Select);

        return $Result;
    }
}