<?php
class Local_Catalog_Model_Attributeset extends Fenix_Resource_Model
{
    public function getAttributesetSelect($currentId)
    {
        $list = $this->getAttributesetList();

        $Select = Fenix::getCreatorUI()
                        ->loadPlugin('Form_Select')
                        ->setName('attributeset_id')
                        ->setSelected($currentId);

        foreach ($list AS $_attributeset) {
            $Select->addOption($_attributeset->id, $_attributeset->title);
        }

        return $Select->fetch();
    }

    /**
     * Наболр атрибутов по идентификатору
     *
     * @param $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function getAttributesetById($id)
    {$Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset');

        $this   ->setTable('catalog_attributeset');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getAttributesetListAsSelect()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset');

        $this   ->setTable('catalog_attributeset');

        $Select = $this->select()
                       ->setIntegrityCheck(false);

        $Select ->from($this->_name, $Engine ->getColumns());

        $Select ->order('title_russian asc');

        return $Select;
    }

    public function getAttributesetList()
    {
        $Select = $this->getAttributesetListAsSelect();
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    // --------------------- ГРУППЫ

    public function getGroupById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset_groups');

        $this   ->setTable('catalog_attributeset_groups');

        $Select = $this->select();
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('id = ?', (int) $id);
        $Select ->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }

    public function getGroupsList($parent)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine ->setSource('catalog/attributeset_groups');

        $this   ->setTable('catalog_attributeset_groups');
        $Select = $this->select()
                       ->setIntegrityCheck(false);
        $Select ->from($this->_name, $Engine ->getColumns());
        $Select ->where('parent = ?', (int) $parent);
        $Select ->order('position asc');

        $Result = $this->fetchAll($Select);

        return $Result;
    }



    /**
     * Список атрибутов группы
     *
     * @param int $groupId Идентификатор группы
     * @return Zend_Db_Table_Rowset
     */
    public function getGroupAttributesList($groupId)
    {
        $List = $this->setTable('catalog_attributeset_groups_attributes')->fetchAll(
            $this->select()
                 ->from($this)
                 ->where('group_id = ?', $groupId)
                 ->order('position asc')
        );

        $Attributes = array();

        foreach ($List AS $list) {
            $Engine = Fenix_Engine::getInstance();
            $Engine ->setSource('catalog/attributes');

            $this->setTable('catalog_attributes');

            $Select = $this->select();
            $Select ->from($this->_name, $Engine ->getColumns());
            $Select ->where('id = ?', (int) $list->attribute_id);
            $Select ->limit(1);

            $Test   = $this->fetchRow($Select);

            if ($Test != null)
                $Attributes[] = $Test->toArray();
        }

        return new Zend_Db_Table_Rowset(array(
            'data' => $Attributes
        ));
    }

    /**
     * Список доступных атрибутов модуля
     *
     * @param Zend_Db_Table_Row $attributesetId Набор атрибутов
     * @return Zend_Db_Table_Rowset
     */
    public function getAvailableAttributesList($attributesetId)
    {
        $AllAttributes = Fenix::getModel('catalog/backend_attributes')->getAttributesList();

        $Result = array();

        foreach ($AllAttributes AS $attribute)
        {
            $Test = $this->setTable('catalog_attributeset_groups_attributes')->fetchRow(
                'attributeset_id = ' . (int) $attributesetId . ' AND ' .
                'attribute_id = ' . (int) $attribute->id
            );

            if ($Test == null)
                $Result[] = $attribute->toArray();
        }

        return new Zend_Db_Table_Rowset(array(
            'data' => $Result
        ));

    }

}