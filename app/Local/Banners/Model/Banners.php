<?php

class Local_Banners_Model_Banners extends Fenix_Resource_Model
{
    public function findById($id)
    {
        $this->setTable('banners');

        $Select = $this->select();
        $Select->from($this->_name);
        $Select->where('id = ?', (int) $id);
        $Select->where('is_public = ?', '1');
        $Select->where('image != \'\'');
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}