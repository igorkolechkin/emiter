<?php

class Local_Banners_Helper_Backend_Banners extends Fenix_Resource_Helper
{

    public function renderBannerSelect($categoryId, $bannerSysTitle)
    {
        $bannersList = Fenix::getModel('banners/backend_banners')->getBannersList();

        $Field = Fenix::getCreatorUI()->loadPlugin('Form_Select')
            ->setId('banner_id')
            ->setName('banner_id');

        $Field->addOption('', 'Не выбран');
        foreach ($bannersList as $banner) {
            $Field->addOption($banner->id, $banner->title_russian);
        }
        if ($bannerSysTitle) {
            $Field->setSelected($bannerSysTitle);
        }

        return $Field->fetch();
    }

    public function renderProductBannerSelect($categoryId, $bannerSysTitle)
    {
        $bannersList = Fenix::getModel('banners/backend_banners')->getBannersList();

        $Field = Fenix::getCreatorUI()->loadPlugin('Form_Select')
            ->setId('banner_product')
            ->setName('banner_product');

        $Field->addOption('', 'Не выбран');
        foreach ($bannersList as $banner) {
            $Field->addOption($banner->id, $banner->title_russian);
        }
        if ($bannerSysTitle) {
            $Field->setSelected($bannerSysTitle);
        }

        return $Field->fetch();
    }
}