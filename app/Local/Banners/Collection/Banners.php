<?php

class  Local_Banners_Collection_Banners extends Fenix_Banners_Collection_Banners
{
    public function getActive($bannerId = 0)
    {
        $bannerId = (int) $bannerId;
        $data = array();

        if ( ! $bannerId) {
            return $data;
        }

        $result = Fenix::getModel('banners/banners')->findById($bannerId);
        if ( ! $result instanceof Zend_Db_Table_Row) {
            return $data;
        }

        $banner = $result->toArray();
        if ( ! file_exists(Fenix_Image::getImagePath($banner['image']))) {
            return $data;
        }

        $data = $banner;

        return $data;
    }
}