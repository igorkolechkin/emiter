<?php

class Local_Core_Model_Menu extends Fenix_Core_Model_Menu
{
    public function findAllMenuItem($menuId)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/menu_item');

        $this->setTable('menu_item');

        $Select = $this->select()->setIntegrityCheck(false);

        $Select->from(array(
            's' => $this->_name,
        ), $Engine->getColumns(array(
            'prefix' => 's',
        )));

        $Select->where('s.is_public = ?', '1');
        $Select->where('s.parent = ?', (int) $menuId);
        $Select->order('s.parent');
        $Select->order('s.position');

        $Result = $this->fetchAll($Select);

        if ($Result) {
            return $Result;
        }
        else {
            return [];
        }
    }
}