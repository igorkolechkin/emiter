<?php

class Local_Core_Model_Themes extends Fenix_Core_Model_Themes
{
    /**
     * Генерация логотипа
     *
     * @return string
     */
    public function getLogoInFooter()
    {
        if (Fenix::getTheme()->logo_in_footer) {
            return HOME_DIR_URL . Fenix::getTheme()->logo_in_footer;
        }
        else {
            return null;
        }
    }
}