<?php

class Local_Core_Model_Utp extends Fenix_Core_Model_Utp
{
    public function findAll()
    {
        $Select = parent::getUtpListAsSelect();
        $Result = $this->fetchAll($Select);

        return $Result;
    }

    public function findActiveBlockById($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/utp');

        $this->setTable('core_utp');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('id = ?', (int) $id)
            ->where('is_public = ?', '1');
        $Select->limit(1);

        $Result = $this->fetchRow($Select);

        return $Result;
    }
}