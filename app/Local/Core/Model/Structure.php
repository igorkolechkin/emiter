<?php

class Local_Core_Model_Structure extends Fenix_Core_Model_Structure
{
    /**
     * Страница по идентификатору
     *
     * @param $url
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function findPageByUrl($url)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/structure');

        $this->setTable('structure');

        $Select = $this->select();
        $Select->from($this->_name, $Engine->getColumns());
        $Select->where('url_key = ?', $url);
        $Select->limit(1);

        return $this->fetchRow($Select);
    }

    public function getChildrenList($parent = 1)
    {
        $Select = $this->getPagesListAsSelect($parent);

        $Select->where('c.is_public = ?', '1');

        return $this->fetchAll($Select);
    }

    public function getChildrenListOnMain($parent = 1)
    {
        $Select = $this->getPagesListAsSelect($parent);

        $Select->where('c.is_public = ?', '1');
        $Select->where('c.on_main = ?', '1');

        return $this->fetchAll($Select);
    }
}