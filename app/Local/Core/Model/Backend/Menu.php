<?php

class Local_Core_Model_Backend_Menu extends Fenix_Core_Model_Backend_Menu
{
    public function findAllMenuItem($menuId)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/menu_item');

        $this->setTable('menu_item');

        $Select = $this->select()->setIntegrityCheck(false);

        $Select->from(array(
            's' => $this->_name,
        ), $Engine->getColumns(array(
            'prefix' => 's',
        )));

        $Select->where('s.parent = ?', (int)$menuId);

        $Result = $this->fetchAll($Select);

        if ($Result) {
            return $Result;
        } else {
            return [];
        }
    }
}