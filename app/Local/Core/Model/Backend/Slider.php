<?php

class Local_Core_Model_Backend_Slider extends Fenix_Resource_Model
{
    public function findAll()
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('core/slider');

        $this->setTable('core_slider');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            's' => $this->_name
        ), $Engine->getColumns(array(
            'prefix' => 's'
        )));

        $Select->order('s.position asc');

        return $this->fetchAll($Select);
    }
}