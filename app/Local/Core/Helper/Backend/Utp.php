<?php

class Local_Core_Helper_Backend_Utp extends Fenix_Resource_Helper
{
    public function utp()
    {
        $Creator = Fenix::getCreatorUI();

        $utpId = (int) parent::getRequest()->getPost('utp_id');

        $selectUtp = $Creator->loadPlugin('Form_Select')
            ->setName('utp_id')
            ->setSelected($utpId);

        $selectUtp->addOption(0, 'Без блока');

        $utpList = Fenix::getModel('core/utp')->findAll();

        if ($utpList) {
            foreach ($utpList as $utp) {
                $selectUtp->addOption($utp->id, $utp->title);
            }
        }

        return $selectUtp->fetch();
    }
}