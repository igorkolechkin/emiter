<?php

class Local_Core_Helper_Backend_Content extends Fenix_Resource_Helper
{
    protected $data = array();

    protected $row;

    protected $col;

    protected $creator;

    protected $request;

    protected $languageCodes;

    protected $jsonData = array();

    public function __construct()
    {
        $this->creator = Fenix::getCreatorUI();

        $this->request = parent::getRequest();

        $language = Fenix_Language::getInstance();
        $languages = $language->getLanguagesList()->toArray();
        foreach ($languages as $language) {
            $this->languageCodes[] = $language['code'];
        }
    }

    public function contentWithHeaders($row, $col, $blockContent)
    {
        $data = $this->returnData($blockContent);

        $content = '';

        $title = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][title]')
            ->setValue(isset($data[$row][$col]['title']) ? $data[$row][$col]['title'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Заголовок')
            ->setContent($title)
            ->fetch();

        $helpTitle = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][help_title]')
            ->setValue(isset($data[$row][$col]['help_title']) ? $data[$row][$col]['help_title'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Вспомогательный заголовок')
            ->setContent($helpTitle)
            ->fetch();

        $wysiwyg = $this->creator->loadPlugin('Form_Wysiwyg')
            ->setName('block[' . $row . '][' . $col . '][content]')
            ->setId('blockContent' . $row . $col)
            ->setValue(isset($data[$row][$col]['content']) ? $data[$row][$col]['content'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Содержимое')
            ->setContent($wysiwyg)
            ->fetch();

        return $content;
    }

    public function utp($row, $col, $blockContent)
    {
        $data = $this->returnData($blockContent);

        $content = '';

        $selectUtp = $this->creator->loadPlugin('Form_Select')
            ->setName('block[' . $row . '][' . $col . '][utp]')
            ->setSelected(isset($data[$row][$col]['utp']) ? $data[$row][$col]['utp'] : '');

        $selectUtp->addOption(0, 'Без блока');

        $utpList = Fenix::getModel('core/utp')->findAll();

        if ($utpList) {
            foreach ($utpList as $utp) {
                $selectUtp->addOption($utp->id, $utp->title);
            }
        }

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Блок УТП')
            ->setContent($selectUtp->fetch())
            ->fetch();

        return $content;
    }

    public function headers($row, $col, $blockContent)
    {
        $data = $this->returnData($blockContent);

        $content = '';

        $title = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][title]')
            ->setValue(isset($data[$row][$col]['title']) ? $data[$row][$col]['title'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Заголовок')
            ->setContent($title)
            ->fetch();

        $helpTitle = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][help_title]')
            ->setValue(isset($data[$row][$col]['help_title']) ? $data[$row][$col]['help_title'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Вспомогательный заголовок')
            ->setContent($helpTitle)
            ->fetch();

        return $content;
    }

    public function contentWithButton($row, $col, $blockContent)
    {
        $data = $this->returnData($blockContent);

        $content = '';

        $wysiwyg = $this->creator->loadPlugin('Form_Wysiwyg')
            ->setName('block[' . $row . '][' . $col . '][content]')
            ->setId('blockContent' . $row . $col)
            ->setValue(isset($data[$row][$col]['content']) ? $data[$row][$col]['content'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Содержимое')
            ->setContent($wysiwyg)
            ->fetch();

        $link = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][url]')
            ->setValue(isset($data[$row][$col]['url']) ? $data[$row][$col]['url'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Ссылка')
            ->setContent($link)
            ->fetch();

        $linkTitle = $this->creator->loadPlugin('Form_Text')
            ->setName('block[' . $row . '][' . $col . '][url_title]')
            ->setValue(isset($data[$row][$col]['url_title']) ? $data[$row][$col]['url_title'] : '')
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Текст кнопки')
            ->setContent($linkTitle)
            ->fetch();

        return $content;
    }

    public function jsonDecoder($data)
    {
        return json_decode($data, true);
    }

    public function returnData($blockContent)
    {
        if ($this->getRequest()->getServer('REQUEST_METHOD') == 'POST') {
            $data = $this->getRequest()->getPost('block');
        }
        else {

            $data = $this->jsonDecoder($blockContent);
        }

        return $data;
    }

    public function addressBlock($row, $col, $blockContent)
    {
        $this->data = self::returnData($blockContent);
        $this->row = (int) $row;
        $this->col = (int) $col;

        $content = '';

        $block = function ($name, $language = '') {
            if ($language) {
                return 'block[' . $this->row . '][' . $this->col . '][' . $language . '][' . (string) $name . ']';
            }
            else {
                return 'block[' . $this->row . '][' . $this->col . '][' . (string) $name . ']';
            }
        };

//        $branch = $this->creator->loadPlugin('Form_Text')
//            ->setName($block('branch'))
//            ->setValue($this->getValue('branch'))
//            ->fetch();
//
//        $content .= $this->creator->loadPlugin('Row')
//            ->setLabel('Филиал')
//            ->setContent($branch)
//            ->fetch();
//
//        $nameShop = $this->creator->loadPlugin('Form_Text')
//            ->setName($block('name_shop'))
//            ->setValue($this->getValue('name_shop'))
//            ->fetch();
//
//        $content .= $this->creator->loadPlugin('Row')
//            ->setLabel('Название магазина')
//            ->setContent($nameShop)
//            ->fetch();

        $nameOffice = [];
        $country = [];
        $city = [];
        $address = [];
        $phone = [];

        foreach ($this->languageCodes as $code) {
            $nameOffice[] = $this->creator->loadPlugin('Form_Textarea')
                ->setName($block('office_title', $code))
                ->setValue($this->getMultiLanguageValue('office_title', $code))
                ->setAttributes(array('rows' => 3))
                ->fetch();

            $country[] = $this->creator->loadPlugin('Form_Text')
                ->setName($block('country', $code))
                ->setValue($this->getMultiLanguageValue('country', $code))
                ->fetch();

            $city[] = $this->creator->loadPlugin('Form_Text')
                ->setName($block('city', $code))
                ->setValue($this->getMultiLanguageValue('city', $code))
                ->fetch();

            $address[] = $this->creator->loadPlugin('Form_Textarea')
                ->setName($block('address', $code))
                ->setValue($this->getMultiLanguageValue('address', $code))
                ->setAttributes(array('rows' => 5, 'style' => 'height:150px'))
                ->fetch();

            $phone[] = $this->creator->loadPlugin('Form_Textarea')
                ->setName($block('phone', $code))
                ->setValue($this->getMultiLanguageValue('phone', $code))
                ->setAttributes(array('rows' => 5, 'style' => 'height:150px'))
                ->fetch();
        }

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Офис')
            ->setContent($nameOffice)
            ->fetch();


        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Страна')
            ->setContent($country)
            ->fetch();


        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Город')
            ->setContent($city)
            ->fetch();

        $google_coordinates = $this->creator->loadPlugin('Form_Text')
            ->setName($block('google_coordinates'))
            ->setValue($this->getValue('google_coordinates'))
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Кординаты для карты Google')
            ->setContent($google_coordinates)
            ->fetch();


        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Адресный блок')
            ->setContent($address)
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('Телефоны')
            ->setContent($phone)
            ->fetch();

        $email = $this->creator->loadPlugin('Form_Textarea')
            ->setName($block('email'))
            ->setValue($this->getValue('email'))
            ->setAttributes(array('rows' => 5, 'style' => 'height:150px'))
            ->fetch();

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('E-mails')
            ->setContent($email)
            ->fetch();

        $onMain = $this->creator->loadPlugin('Form_Select')
            ->setName($block('on_main'))
            ->setSelected($this->getValue('on_main'));

        $onMain->addOption(0, 'Нет');
        $onMain->addOption(1, 'Да');

        $content .= $this->creator->loadPlugin('Row')
            ->setLabel('На главной')
            ->setContent($onMain->fetch())
            ->fetch();

        return $content;
    }

    private function getValue($name)
    {
        if (isset($this->data[$this->row][$this->col][$name])) {
            return $this->data[$this->row][$this->col][$name];
        }
        else {
            return false;
        }
    }


    public function getMultiLanguageValue($name, $language)
    {
        if (isset($this->data[$this->row][$this->col][$language][$name])) {
            return $this->data[$this->row][$this->col][$language][$name];
        }
        else {
            return false;
        }
    }
}