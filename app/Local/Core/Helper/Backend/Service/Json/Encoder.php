<?php

class Local_Core_Helper_Backend_Service_Json_Encoder
{
    protected $data = array();

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function encode()
    {
//        return array_map('json_encode', $this->data);
        return json_encode($this->data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }
}