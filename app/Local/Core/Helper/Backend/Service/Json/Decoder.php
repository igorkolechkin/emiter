<?php

class Local_Core_Helper_Backend_Service_Json_Decoder
{
    protected $data = '';

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function decode()
    {
        return json_decode($this->data, true);
    }

    public function decodeToObject()
    {
        return json_decode($this->data);
    }
}