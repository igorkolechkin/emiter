<?php

class Local_Core_Helper_Backend_Slider extends Fenix_Resource_Helper
{
    public function getSliders()
    {
        $Creator = Fenix::getCreatorUI();

        $sliderId = (int) parent::getRequest()->getPost('slider_id');

        $sliderSelect = $Creator->loadPlugin('Form_Select')
            ->setName('slider_id')
            ->setSelected($sliderId);

        $sliderSelect->addOption(0, 'Без слайдера');

        $sliderList = Fenix::getModel('core/backend_slider')->findAll();

        if ($sliderList) {
            foreach ($sliderList as $slider) {
                $sliderSelect->addOption($slider->id, $slider->title);
            }
        }

        return $sliderSelect->fetch();
    }
}