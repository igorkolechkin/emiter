<?php

class Local_Core_Helper_Service_Menu extends Fenix_Resource_Helper
{
    protected $model;

    protected $menuId = 0;

    protected $menuClassCss = 'navbar-nav';

    protected $menuItems = [];

    protected $menuItemsWithChildren = [];

    public function __construct()
    {
        $this->model = Fenix::getModel('core/menu');
    }

    public function renderMenu()
    {
        $args = func_get_args();

        if (isset($args[0])) {
            $menuLayout = $args[0];
        } else {
            return '';
        }

        $parentId = 0;
        if (isset($args[1])) {
            $parentId = (int)$args[1];
        }

        $depth = 1;
        if (isset($args[2])) {
            $depth = (int)$args[2];
        }

        $this->getMenuLayout($menuLayout);

        $this->menuItems = $this->model->findAllMenuItem($this->menuId);

        $this->createMenuWithChildren();

        return $this->buildTree($parentId, $depth);
    }

    public function renderCategoryWithFilterMenu()
    {
        $args = func_get_args();

        if (isset($args[0])) {
            $menuLayout = $args[0];
        } else {
            return '';
        }

        $parentId = 0;
        if (isset($args[1])) {
            $parentId = (int)$args[1];
        }

        $depth = 1;
        if (isset($args[2])) {
            $depth = (int)$args[2];
        }

        $this->getMenuLayout($menuLayout);

        $this->menuItems = $this->model->findAllMenuItem($this->menuId);

        $this->createMenuWithChildren();

        return $this->buildCategoryWithFilterTree($parentId, $depth);
    }

    public function renderSimpleMenu()
    {
        $args = func_get_args();

        if (isset($args[0])) {
            $menuLayout = $args[0];
        } else {
            return '';
        }

        $this->getMenuLayout($menuLayout);

        $this->menuItems = $this->model->findAllMenuItem($this->menuId);

        $this->createMenuWithChildren();

        return $this->buildTree(0, 1);
    }

    public function renderCategorySimpleMenu()
    {
        $this->menuItems = Fenix::getModel('catalog/categories')->getQueryCategoriesList(['in_menu' => '1']);

        $this->createCategoryWithChildren();

        return $this->buildTree(1, 1);
    }

    public function renderCategoryMenu($depth = 1)
    {
        $this->menuItems = Fenix::getModel('catalog/categories')->getQueryCategoriesList(['in_menu' => '1']);

        $this->createCategoryWithChildren();

        return $this->buildTree(1, $depth);
    }

    protected function getMenuLayout($menuLayout)
    {
        $result = $this->model->getMenuByName($menuLayout);

        if ($result) {
            $result = (object)$result->toArray();
            $this->menuId = $result->id;
        } else {
            $this->menuId = 0;
        }
    }

    protected function createMenuWithChildren()
    {
        $this->menuItemsWithChildren = [];
        foreach ($this->menuItems as $item) {
            $this->menuItemsWithChildren[$item->parent_menu_id][] = $item;
        }
    }

    protected function createCategoryWithChildren()
    {
        $this->menuItemsWithChildren = [];
        foreach ($this->menuItems as $item) {
            $this->menuItemsWithChildren[$item->parent][] = $item;
        }
    }

    public function getMenuWithChildren($menuLayout)
    {
        $this->getMenuLayout($menuLayout);

        $this->menuItems = $this->model->findAllMenuItem($this->menuId);

        $this->createMenuWithChildren();

        return $this->menuItemsWithChildren;
    }

    public function setClassCss($classCss = '')
    {
        $classCss = trim($classCss);
        if ($classCss != '') {
            $this->menuClassCss = $classCss;
        }

        return $this;
    }

    /**
     * Рекурсивная функция построения меню
     *
     * @param    int $parent_id Текущее значение родительского пункта
     * @param    int $depth Глубина меню
     * @return    string    Отформатированное меню с html тегами
     */
    protected function buildTree($parent_id = 0, $depth = 1)
    {
        static $level = 0;
        static $path = [];

        $level++;

        if (($depth > 0) && ($depth < $level)) {
            $level--;

            return null;
        }

        if (is_array($this->menuItemsWithChildren) && isset($this->menuItemsWithChildren[$parent_id])) {
            $tree = '';
            if ($level == 1) {
                $tree .= '<ul class="' . $this->menuClassCss . '">';
            } else {
                $tree .= '<ul class="dropdown-menu">';
            }
            foreach ($this->menuItemsWithChildren[$parent_id] as $item) {
                $tree .= '<li class="nav-item">';
                if ($item->url_key != '#') {
                    $tree .= '<a class="nav-link" href="' . Fenix::getUrl($item->url_key) . '">' . $item->title . '</a>';
                } else {
                    $tree .= '<a class="nav-link" href="javascript:void(0);">' . $item->title . '</a>';
                }
                $tree .= $this->buildTree($item->id, $depth);
                $tree .= '</li>';
            }
            $tree .= '</ul>';
        } else {
            $level--;

            return null;
        }

        $level--;

        return $tree;
    }

    /**
     * Рекурсивная функция построения меню
     *
     * @param    int $parent_id Текущее значение родительского пункта
     * @param    int $depth Глубина меню
     * @return    string    Отформатированное меню с html тегами
     */
    protected function buildCategoryWithFilterTree($parent_id = 0, $depth = 1)
    {
        static $level = 0;
        static $path = [];

        $level++;

        if (($depth > 0) && ($depth < $level)) {
            $level--;

            return null;
        }

        if (is_array($this->menuItemsWithChildren) && isset($this->menuItemsWithChildren[$parent_id])) {
            $tree = '';
            if ($level == 1) {
                $tree .= '<ul class="' . $this->menuClassCss . '">';
            } elseif ($level == 2) {
                $tree .= '<ul class="dropdown-menu">';
            } else {
                $tree .= '<ul>';
            }
            foreach ($this->menuItemsWithChildren[$parent_id] as $item) {
                $tree .= '<li class="nav-item">';
                if ($item->url_key != '#') {
                    $tree .= '<a class="nav-link" href="' . Fenix::getUrl($item->url_key) . '">' . $item->title . '</a>';
                } else {
                    $tree .= '<a class="nav-link" href="javascript:void(0);">' . $item->title . '</a>';
                }
                $tree .= $this->buildCategoryWithFilterTree($item->id, $depth);
                $tree .= '</li>';
            }
            $tree .= '</ul>';
        } else {
            $level--;

            return null;
        }

        $level--;

        return $tree;
    }
}