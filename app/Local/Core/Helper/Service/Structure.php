<?php

class Local_Core_Helper_Service_Structure extends Fenix_Resource_Helper
{
    protected $modelStructure;

    public function __construct()
    {
        $this->modelStructure = Fenix::getModel('core/structure');
    }

    public function getPageChildren($pageId = 0)
    {
        return $this->modelStructure->getChildrenList($pageId);
    }

    public function getPageChildrenOnMain($pageId = 0)
    {
        return $this->modelStructure->getChildrenListOnMain($pageId);
    }

    public function getPageChildrenForInside($pageId = 0, $pageUrl = '')
    {
        $data = [];
        $pageId = (int) $pageId;
        if ( ! $pageId) {
            return $data;
        }

        $results = $this->getPageChildren($pageId);
        if ($results->count() < 1) {
            return $data;
        }

        $pageUrl = trim($pageUrl);
        if ($pageUrl != '') {
            $pageUrl .= '/';
        }

        foreach ($results as $result) {
            $systemName = $this->createSystemName($result->url_key);

            $image = '';
            if (isset($result->image)) {
                $image = Fenix_Image::resize($result->image, 550, 350);
            }

            $gallery = (array)unserialize($result->gallery);

            $dataGallery = [];
            foreach ($gallery AS $galleryImage) {
                $dataGallery[] = Fenix::getCollection('core/pages_gallery')->setImage($galleryImage);
            }

            $information = '';
            if (isset($result->block_information)) {
                $information = $result->block_information;
            }

            $data['page' . $systemName] = [
                'title'            => $result->title,
                'image'            => $image,
                'shortDescription' => $result->shot_description,
                'priceDescription' => $this->parserContent($result->price_description),
                'gallery'          => $dataGallery,
                'information'      => $information,
                'pageUrl'          => Fenix::getUrl($pageUrl . $result->url_key)
            ];
        }

        return $data;
    }

    public function getPageChildrenForMainPage($pageId = 0)
    {
        $data = [];
        $pageId = (int) $pageId;
        if ( ! $pageId) {
            return $data;
        }

        $page = $this->modelStructure->getPageById($pageId);

        if ($page == null) {
            return $data;
        }

        $results = $this->getPageChildrenOnMain($pageId);

        if ($results->count() < 1) {
            return $data;
        }

        $pageUrl = trim($page->url_key);
        if ($pageUrl != '') {
            $pageUrl .= '/';
        }

        foreach ($results as $result) {
            $utpList = [];
            if (isset($result->utp_id) && $result->utp_id > 0) {
                $utpList = Fenix::getHelper('core/service_utp')->setUtpId($result->utp_id)->getUtpItems();
            }

            $image = '';
            if (isset($result->image)) {
                $image = Fenix_Image::adapt($result->image, 472, 396);
            }

            $icon = '';
            if (isset($result->block_icon)) {
                $icon = Fenix::getUploadImageUrl($result->block_icon);
            }

            if (isset($result->block_image)) {
                $image = Fenix::getUploadImageUrl($result->block_image);
            }

            $slideItems = [];
            if (isset($result->slider_id) && $result->slider_id > 0) {
                $slider = Fenix::getCollection('core/slider')->getSliderById($result->slider_id);
            }

            if ( ! empty($slider)) {

                $slider = $slider->getSlideList();
                $slides = $slider->getData();

                foreach ($slides as $slide) {
                    if ($slide['image']) {
                        $slideItems[] = [
                            'image' => $slide['image'],
                            'title' => $slide['title']
                        ];
                    }
                }
            }

            $information = '';
            if (isset($result->block_information)) {
                $information = $result->block_information;
            }

            $systemName = $this->createSystemName($result->url_key);

            $data['page' . $systemName] = [
                'title'       => $result->title,
                'icon'        => $icon,
                'image'       => $image,
                'utpList'     => $utpList,
                'slideItems'  => $slideItems,
                'pageUrl'     => Fenix::getUrl($pageUrl . $result->url_key),
                'information' => $information
            ];
        }

        return $data;
    }

    public function getPage($pageId)
    {
        return $this->modelStructure->getPageById($pageId);
    }

    /**
     * Возвращает из массива найденный объект по ключу
     * Example: $key = pageImportExport
     *
     * @param string $key
     * @param array $data
     * @return null|object
     */
    public function getPageBlock($key, $data = array())
    {
        $keyFormated = 'page' . $this->createSystemName($key);

        if (isset($data[$keyFormated])) {
            return (object)$data[$keyFormated];
        }

        return null;
    }

    /**
     * Converts url_key to UrlKey
     * Example: $urlKey = import-export, return ImportExport
     *
     * @param string $urlKey
     * @return string
     */
    private function createSystemName($urlKey)
    {
        return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $urlKey)));
    }

    private function parserContent($content)
    {
        $content = trim($content);
        if ($content == '') {
            return '';
        }

        $replace = ['<p>', '</p>'];
        $content = str_replace($replace, '', $content);

        $icon = '';
        $description = '';
        $priceBlock = '';

        if (preg_match('%\[icon\](.*)\[/icon\]%si', $content, $regs)) {
            $icon = $regs[1];
        }

        if (preg_match('%\[description\](.*)\[/description\]%si', $content, $regs)) {
            $description = $regs[1];
        }

        if (preg_match('%\[price_block\](.*)\[/price_block\]%si', $content, $regs)) {
            $priceBlock = $regs[1];
        }

        $replace = ['[', ']', 'price'];
        $replaceTo = ['<', '>', 'span'];
        $priceBlock = str_replace($replace, $replaceTo, $priceBlock);

        return (object)[
            'icon'        => $icon,
            'description' => explode('<br />', $description),
            'price'       => $priceBlock
        ];
    }
}