<?php

class Local_Core_Helper_Service_Setting extends Fenix_Resource_Helper
{
    protected $model;

    protected $creator;

    public function __construct()
    {
        $this->model = Fenix::getModel('core/themes');

        $this->creator = Fenix::getCreatorUI();
    }

    public function renderLogoInHeader()
    {
        $isMain = false;
        if ( ! empty($this->creator->getView()->getVars()['isMain'])) {
            $isMain = true;
        }
        $logo = $this->model->getLogo();
        if ($isMain) {
            $logo = Fenix_Image::resize(substr($logo, strlen(HOME_DIR_URL)), 205, 87);
        }
        $image = '<img src="' . $logo . '" class="img-fluid">';

        if ( ! $isMain) {
            $image = '<a href="' . Fenix::getUrl() . '">' . $image . '</a>';
        }

        return $image;
    }

    public function renderLogoInFooter()
    {
        $isMain = false;
        if ( ! empty($this->creator->getView()->getVars()['isMain'])) {
            $isMain = true;
        }
        $logo = $this->model->getLogoInFooter();
        $image = '<img src="' . $logo . '">';

        if ( ! $isMain) {
            $image = '<a href="' . Fenix::getUrl() . '">' . $image . '</a>';
        }

        return $image;
    }

    public function getPhoneInHeader($tag = '', $wrapString = '')
    {
        $data = [];
        $phones = trim(Fenix::getConfig('general/project/cellphone'));
        if ($phones) {
            $decoratorPhone = Fenix::getHelper('core/decorator_phone');
            $phones = explode(chr(10), $phones);
            foreach ($phones as $phone) {
                $phone = $decoratorPhone::decorate($phone);
                $data[] = $phone->getPhoneWithTag($tag, $wrapString);
            }
        }

        return implode(PHP_EOL, $data);
    }

    public function renderYouTubeChannelLink()
    {
        $link = '<a href="#link" class="youtube" target="_blank">';
        $link .= 'смотрите нас на канале <i class="icon-youtube-squared"></i>';
        $link .= '</a>';

        $youTubeChannel = trim(Fenix::getConfig('general/google/channel_youtube'));
        if ($youTubeChannel != '') {
            return str_replace('#link', $youTubeChannel, $link);
        }
    }

//    public function getLanguageMenu()
//    {
//        $languages = Fenix_Language::getInstance();
//        $active = $languages->getCurrentLanguage();
//        $default = $languages->getDefaultLanguage();
//
//        $uri = $this->getRequest()->getRequestUri();
//
//        $languageList = $languages->getLanguagesList();
//        $data = [];
//
//        if ($languageList->count() > 0) {
//
//            $pattern = array_map(function ($language) {
//                return '\/' . $language['code'];
//            }, $languageList->toArray());
//
//            $pattern[] = '\/';
//
//            $pattern = implode('|', $pattern);
//
//            $clearUri = preg_replace('/^' . $pattern . '\/?/', '', $uri);
//
//            foreach ($languageList as $language) {
//                $lang = '';
//                if ($language->code != $default->code) {
//                    $lang = '/' . $language->code;
//                }
//                else {
//                    $clearUri = '/' . $clearUri;
//                }
//
//                $isActive = false;
//                if (($language->code == $active->code)) {
//                    $isActive = true;
//                }
//
//                $data[] = array(
//                    'url'    => $lang . $clearUri,
//                    'code'   => $language->code,
//                    'active' => $isActive
//                );
//            }
//        }
//
//        return $data;
//    }

    public function renderEmails($typeBlock = 'header')
    {
        $blocks = ['header', 'footer'];
        $data = [];

        if ( ! in_array($typeBlock, $blocks)) {
            return $data;
        }

        $emails = trim(Fenix::getConfig('general/email/' . $typeBlock));
        $emails = explode(chr(10), $emails);
        foreach ($emails as $email) {
            $email = trim($email);
            if ($email != '') {
                $data[] = $email;
            }
        }

        return $data;
    }
}