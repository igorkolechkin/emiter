<?php

class Fenix_Core_Helper_Service_Language extends Fenix_Resource_Helper
{
    protected $language;

    protected $languageList;

    protected $currentLanguage;

    protected $defaultLanguage;

    public function __construct()
    {
        $this->language = Fenix_Language::getInstance();
    }

    public function getHrefLang()
    {
        $this->setLanguageList();

        $this->setDefaultLanguage();

        if ( ! $this->languageList->count() > 0) {
            return '';
        }

        $clearUri = $this->clearUri();
        $links = [];

        foreach ($this->languageList as $language) {
            $segmentLink[] = $_SERVER['HTTP_HOST'];
            if ($language->code != $this->defaultLanguage->code) {
                $segmentLink[] = $language->code . '/' . $clearUri;
                $hrefLang = $language->code;
            } else {
                $segmentLink[] = $clearUri;
                $hrefLang = 'x-default';
            }

            $href = $_SERVER['REQUEST_SCHEME'] . '://' . implode('/', $segmentLink);
            $links[] = '<link rel="alternate" href="' . $href . '" hreflang="' . $hrefLang . '"/>';
            unset($segmentLink);
        }

        return implode(PHP_EOL, $links);
    }

    public function renderLanguageMenu()
    {
        $this->setLanguageList();

        $this->setCurrentLanguage();

        $data = [];

        if ( ! $this->languageList->count() > 0) {
            return $data;
        }

        $clearUri = $this->clearUri();
        $scheme = parent::getRequest()->getScheme();
        $httpHost = parent::getRequest()->getHttpHost();

        foreach ($this->languageList as $language) {
            $isActive = false;

            if (($language->code == $this->currentLanguage->code)) {
                $isActive = true;
            }

            $data[] = (object)[
                'url'    => $scheme . '://' . $httpHost . '/' . $language->code . '/' . $clearUri,
                'code'   => $language->code,
                'active' => $isActive,
                'short'  => $language->short,
                'label'  => $language->label
            ];
        }

        return Fenix::getCreatorUI()->getView()->partial('layout/html/menu/language.phtml', ['languages' => $data]);
    }

    public function setLanguageList()
    {
        if ( ! $this->languageList) {
            $this->languageList = $this->language->getLanguagesList();
        }
    }

    public function setCurrentLanguage()
    {
        if ( ! $this->currentLanguage) {
            $this->currentLanguage = $this->language->getCurrentLanguage();
        }
    }

    public function setDefaultLanguage()
    {
        if ( ! $this->defaultLanguage) {
            $this->defaultLanguage = $this->language->getDefaultLanguage();
        }
    }

    protected function clearUri()
    {
        $segments = parent::getRequest()->getParsedInfo()->segments;

        if ($segments[0] == 'default') {
            return '';
        }

        return implode('/', $segments);
    }
}