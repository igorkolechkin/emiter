<?php

class Local_Core_Helper_Service_Utp
{
    protected $model;

    protected $items = [];

    protected $nameUtp = '';

    protected $blockUtp;

    public function __construct()
    {
        $this->model = Fenix::getModel('core/utp');
    }

    public function setUtpId($id)
    {
        $this->clearParams();

        $id = (int) $id;
        if ( ! $id) {
            return $this;
        }
        $result = $this->model->findActiveBlockById($id);

        if ($result == null) {
            return $this;
        }
        $this->nameUtp = $result->system_name;

        return $this;
    }

    public function setUtpName($name)
    {
        $this->clearParams();

        $this->nameUtp = $name;

        return $this;
    }

    public function render()
    {

    }

    public function getBlockUtp()
    {
        return $this->blockUtp;
    }

    public function getUtpItems()
    {
        if ( ! $this->nameUtp) {
            return $this->items;
        }

        $result = $this->model->getUtpByName($this->nameUtp);

        if ($result) {

            $utpList = $this->model->getPublicBlockList($result->id);

            $this->setBlockUtp($result);

            foreach ($utpList AS $utp) {
                $tmp = $utp->toArray();

                if ((trim($tmp['image']) != '') && file_exists(HOME_DIR_ABSOLUTE . $tmp['image']) && ! is_dir(HOME_DIR_ABSOLUTE . $tmp['image'])) {
                    $image = HOME_DIR_URL . $tmp['image'];
                    $tmp['image'] = $image;
                }

                $this->items[] = $tmp;
            }
        }

        return $this->items;
    }

    private function setBlockUtp($result)
    {
        if ($result instanceof Zend_Db_Table_Row) {
            $this->blockUtp = (object) [
                'title'             => trim($result->title),
                'image'             => trim($result->image),
                'additionalContent' => trim($result->additional_content),
                'systemName'        => trim(str_replace('.', '-', $result->system_name)),
                'link'              => trim($result->url_key)
            ];
        }
    }

    private function clearParams()
    {
        $this->items = [];

        $this->nameUtp = '';

        $this->blockUtp = null;
    }
}