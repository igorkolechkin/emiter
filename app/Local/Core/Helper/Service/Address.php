<?php

class Local_Core_Helper_Service_Address extends Fenix_Resource_Helper
{
    protected $modelStructure;

    protected $htmlContent;

    protected $languageCode;


    public function __construct()
    {
        $this->modelStructure = Fenix::getModel('core/structure');

        $page = $this->modelStructure->findPageByUrl('contacts');

        $this->htmlContent = $page->html_content;

        $languages = Fenix_Language::getInstance();
        $this->languageCode = $languages->getCurrentLanguage()->code;
    }

    public function getAddressOnMain()
    {
        $data = array();

        $decodeHtmlContent = Fenix::getHelper('core/backend_service_json_decoder')->setData($this->htmlContent)->decode();

        foreach ($decodeHtmlContent as $rows) {
            foreach ($rows as $row) {
                $city = trim($row[$this->languageCode]['city']);
                $address = trim($row[$this->languageCode]['address']);
                if ($city == '' && $address == '') {
                    continue;
                }

                if ($row['on_main'] == 1) {
                    $data = array(
                        'officeTitle'       => $row[$this->languageCode]['office_title'],
                        'country'           => $row[$this->languageCode]['country'],
                        'city'              => $row[$this->languageCode]['city'],
                        'address'           => $row[$this->languageCode]['address'],
                        'phone'             => $row[$this->languageCode]['phone'],
                        'googleCoordinates' => $this->getGoogleCoordinatesFormatted($row['google_coordinates']),
                        'email'             => $row['email'],
                    );
                }
            }
        }

        return $data;
    }

    public function getAddress()
    {
        $data = array();

        $decodeHtmlContent = Fenix::getHelper('core/backend_service_json_decoder')->setData($this->htmlContent)->decode();

        foreach ($decodeHtmlContent as $rows) {
            foreach ($rows as $row) {
                $city = trim($row[$this->languageCode]['city']);
                $address = trim($row[$this->languageCode]['address']);
                if ($city == '' && $address == '') {
                    continue;
                }

                $data[] = array(
                    'officeTitle'       => $row[$this->languageCode]['office_title'],
                    'country'           => $row[$this->languageCode]['country'],
                    'city'              => $row[$this->languageCode]['city'],
                    'address'           => $row[$this->languageCode]['address'],
                    'phone'             => $row[$this->languageCode]['phone'],
                    'googleCoordinates' => $this->getGoogleCoordinatesFormatted($row['google_coordinates']),
                    'email'             => $row['email'],
                );
            }
        }

        return $data;
    }

    private function getGoogleCoordinatesFormatted($string = '')
    {
        $string = trim($string);
        if ($string == '') {
            return '';
        }
        $googleCoordinates = explode(',', $string);

        array_walk($googleCoordinates, function (&$item) {
            return $item = trim($item);
        });

        return $googleCoordinates;
    }
}