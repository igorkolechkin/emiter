<?php

class Local_Core_Helper_Decorator_Phone
{
    protected $entity;

    protected $operatorCode;

    protected $cssClasses = array();

    public function __construct($entity = null)
    {
        if ($entity !== null) {
            $this->entity = $entity;
            $this->cssClasses = array('vodafone', 'lifecell');
            $this->cutOperatorCode();
        }
    }

    public static function decorate($entity)
    {
        $entity = trim($entity);

        return new self($entity);
    }

    public function getPhone()
    {
        if ($this->entity == '') {
            return '';
        }

        $phoneText = preg_replace('/(\d{3}-\d{2}-\d{2})$/', '<b>\1</b>', $this->entity);

        return '<a href="tel:' . $this->clearPhone() . '">' . $phoneText . '</a>';
    }

    public function getPhoneForPageContact()
    {
        if ($this->entity == '') {
            return '';
        }

        return '<p><a href="tel:' . $this->clearPhone() . '">' . $this->entity . '</a></p>';
    }

    public function getPhoneWithTag($tag = 'sup', $wrapString = '+38')
    {
        if ($this->entity == '') {
            return '';
        }

        if (strstr('+', $wrapString)) {
            $wrapString = '\'' . $wrapString;
        }

        if ($tag) {
            $phoneText = preg_replace('/^(' . $wrapString . ')/', '<' . $tag . '>\1</' . $tag . '>', $this->entity);
        } else {
            $phoneText = $this->entity;
        }

        return '<p>' . $phoneText . '</p>';
    }

    public function getPhoneWithCssIcon()
    {
        if ($this->entity == '') {
            return '';
        }

        $phoneText = preg_replace('/^(\(.*\))(.*)?/', '\1<b>\2</b>', $this->entity);

        $cssClass = 'list-group-item list-group-item-action operator';

        if ($this->isOperatorVodafone() == true) {
            return '<a href="tel:' . $this->clearPhone() . '" class="' . $cssClass . ' vodafone">' . $phoneText . '</a>';
        }

        if ($this->isOperatorLifeCell() == true) {
            return '<a href="tel:' . $this->clearPhone() . '" class="' . $cssClass . ' lifecell">' . $phoneText . '</a>';
        }

        if ($this->isOperatorKievstar() == true) {
            return '<a href="tel:' . $this->clearPhone() . '" class="' . $cssClass . ' kyivstar">' . $phoneText . '</a>';
        }
    }

    public function wrapOperator($tag = 'span')
    {
        $tag = trim($tag);

        if ($tag) {
            $this->entity = str_replace($this->operatorCode, '<' . $tag . '>' . $this->operatorCode . '</' . $tag . '>',
                $this->entity);
        }

        return $this;
    }

    private function clearPhone()
    {
        return preg_replace('/[\+\s\(\)-]/', '', $this->entity);
    }

    private function cutOperatorCode()
    {
        if (strstr('(', $this->entity)) {
            $re = '/\((\d{3})\)/';
        } else {
            $re = '/(0\s+800)/';
        }

        preg_match($re, $this->entity, $matches, PREG_OFFSET_CAPTURE, 0);

        if (empty($matches[1])) {
            $this->operatorCode = '';

            return;
        }

        $this->operatorCode = (string)$matches[1][0];
    }

    private function isOperatorVodafone()
    {
        $codes = array('050', '099', '066');
        if (in_array($this->operatorCode, $codes)) {
            return true;
        }
    }

    private function isOperatorLifeCell()
    {
        $codes = array('073', '093');
        if (in_array($this->operatorCode, $codes)) {
            return true;
        }
    }

    private function isOperatorKievstar()
    {
        $codes = array('067', '096');
        if (in_array($this->operatorCode, $codes)) {
            return true;
        }
    }

    private function isOperatorCity()
    {
        $codes = array('056');
        if (in_array($this->operatorCode, $codes)) {
            return true;
        }
    }

    private function isOperatorFreeCall()
    {
        $codes = array('0 800');
        if (in_array($this->operatorCode, $codes)) {
            return true;
        }
    }
}