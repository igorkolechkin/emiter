<?php

class Local_Core_Helper_Menu_Item extends Fenix_Resource_Helper
{
    protected $url;

    protected $structureModel;

    protected $categoryModel;

    protected $request;

    protected $structureId;

    protected $categoryId;

    public function __construct()
    {
        $this->request = $this->getRequest();

        $this->structureId = $this->request->getPost('structure_id');
        if ($this->structureId) {
            $this->structureModel = \Fenix::getModel('core/structure');
        }

        $this->categoryId = $this->request->getPost('category_id');
        if ($this->categoryId) {
            $this->categoryModel = \Fenix::getModel('catalog/backend_categories');
        }

        $this->setUrl($this->request->getPost('url_key'));

        $this->setUrlFromStructure();
        $this->setUrlFromCategoryProduct();
    }

    public function setUrl($url = '')
    {
        $url = trim($url);
        $this->url = $url;
    }

    public function getUrl()
    {
        return (string)$this->url;
    }

    private function setUrlFromStructure()
    {
        if ($this->structureModel) {
            $structure = $this->structureModel->getPageById($this->structureId);

            $parents = $this->structureModel->getNavigation($structure)->toArray();

            $uriList = array_map(function ($parent) {
                return $parent['url_key'];
            }, $parents);

            $uri = implode('/', $uriList);

            if ($uri == 'default') {
                $this->setUrl('/');
            } else {
                $this->setUrl($uri);
            }
        }
    }

    private function setUrlFromCategoryProduct()
    {
        if ($this->categoryModel) {
            $category = $this->categoryModel->getCategoryById($this->categoryId);

            if (($category->url_key != '') || ($category->url_key != 'tovary-bez-kategorii')) {
                $this->setUrl($category->url_key);
            }
        }
    }
}