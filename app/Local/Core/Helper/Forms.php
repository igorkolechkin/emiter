<?php

class Local_Core_Helper_Forms extends Fenix_Core_Helper_Forms
{
    public function getUploadResumeForm($job_name = null)
    {
        $Form = Fenix::getCreatorUI()->loadPlugin('Form');
        $Row = array();

        // Событие
        $Row[] = $Form->loadPlugin('Events_Session')->fetch();

        // Файл
        $Field = $Form->loadPlugin('Form_File')
            ->setName('resume')
            ->setFile($Form->getRequest()->getPost('resume'))
            ->setError('eeee')
            ->setAttributes(array(
                'required' => 'required',
                'title'    => Fenix::lang('Выберите Ваше резюме'),
            ))
            ->fetch();

        $Row[] = $Form->loadPlugin('Row')
            // ->setLabel(Fenix::lang("Отправить резюме"))
            ->setContent($Field)
            ->fetch();

        // Название вакансии
        $job_name = ($job_name) ? $job_name : $Form->getRequest()->getPost('job_name');
        $Field = $Form->loadPlugin('Form_Text')
            ->setName('job_name')
            ->setType('hidden')
            ->setValue($job_name)
            ->fetch();

        $Row[] = $Form->loadPlugin('Row')
            ->setContent($Field)
            ->fetch();

        // Кнопуля
        $Field = $Form->loadPlugin('Button')
            ->setValue(Fenix::lang("Отправить резюме"))
            ->setType('submit')
            ->setClass('btn yellow-30')
            ->fetch();
        $Row[] = $Form->loadPlugin('Row')
            ->setContent($Field)
            ->fetch();

        $Row[] = $Form->loadPlugin('Container')
            // ->setLabel(Fenix::lang("Отправить резюме"))
            ->setContent('Примечание: размер файла не должен превышать 2Mb')
            ->fetch();

        $Form->setContent($Row);
        $Form->compile();


        // Send mail
        $postFiles = $Form->getRequest()->getFiles();
        if ($this->resumeFileValidate($Form)) {

            //Список email'ов
            $emailsList = Fenix::getConfig('general/job_email');
            $emailsList = explode("\n", $emailsList);

            //Отправляем
            foreach ($emailsList as $_email) {
                if (trim($_email) == '') {
                    continue;
                }

                $mail = new Zend_Mail('UTF-8');

                /**
                 * Создаём вложение, читаем файл
                 */
                $logo = new Zend_Mime_Part(file_get_contents($postFiles['resume']['tmp_name']));
                /**
                 * Указываем тип содержимого файла
                 */
                $logo->type = 'application/octet-stream';
                $logo->disposition = Zend_Mime::DISPOSITION_INLINE;
                /**
                 * Каким способом закодировать файл в письме
                 */
                $logo->encoding = Zend_Mime::ENCODING_BASE64;
                /**
                 * Название файла в письме
                 */
                $logo->filename = $postFiles['resume']['name'];
                /**
                 * Идентификатор содержимого.
                 * По нему можно обращаться к файлу в теле письма
                 */
                $logo->id = md5(time());
                /**
                 * Описание вложеного файла
                 */
                $logo->description = 'Резюме';
                /**
                 * Добавляем вложение в письмо
                 */
                $mail->addAttachment($logo);
                $mail->addTo(trim($_email));
                $mail->setFrom('HR@' . $_SERVER['HTTP_HOST']);
                $mail->setSubject('Отзыв на вакансию - ' . $_SERVER['HTTP_HOST']);
                $mail->setBodyHtml('Отзыв на вакансию: ' . $Form->getRequest()->getPost('job_name'), 'UTF-8',
                    Zend_Mime::ENCODING_BASE64);
                $mail->send();
            }
        }

        return $Form;
    }

    public function resumeFileValidate($Form)
    {
        $postFiles = $Form->getRequest()->getFiles();
        if (isset($postFiles['resume']) && ($postFiles['resume']['error'] == 0)) {
            $success_types = array(
                'application/pdf',
                'text/html',
                'application/x-msexcel',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'text/richtext',
                'text/plain',
            );

            return in_array($postFiles['resume']['type'], $success_types);
        }

        return false;
    }

    public function getResumeFileMsg($Form)
    {
        if ( ! $Form->getRequest()->getPost()) {
            return '';
        }

        if ($this->resumeFileValidate($Form)) {

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang('Ваше резюме успешно отправлено'))
                ->saveSession();

            return Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();

        } else {

            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang('Пожалуйста, прикрепите текстовый файл.'))
                ->saveSession();

            return Fenix::getCreatorUI()->loadPlugin('Events_Session')->fetch();
        }
    }
}