<?php

class Local_Core_Controller_Admin_Menu_Items extends Fenix_Core_Controller_Admin_Menu_Items
{
    public function addAction()
    {
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => Fenix::getUrl('core/menu'),
                'id'    => 'slider',
            ),
            array(
                'label' => $this->_menu->title,
                'uri'   => Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id),
                'id'    => 'menu',
            ),
            array(
                'label' => Fenix::lang("Добавить пункт меню"),
                'uri'   => '',
                'id'    => 'add',
            ),
        ));

        if ($this->getRequest()->getParam('parent') !== null) {
            $parent_id = $this->getRequest()->getParam('parent');
        }
        else {
            $parent_id = 0;
        }

        if ($parent_id) {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }
        else {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        // Url автоматом
        $url_key = $this->getRequest()->getPost('url_key');
        if (empty($url_key)) {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl(str_replace(' ', '-', $url_key)));

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('menu', $this->_menu)
            ->setData('current', null)
            ->setData('button_beck', $buttonBecklink);

        // Источник
        $Form->setSource('core/menu_item', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_menu->id);

            $this->getRequest()->setPost('url_key', $this->helperMenu->getUrl());

            $id = Fenix::getModel('core/backend_menu')->addItem(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Пункт меню создан"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/menu/items/add/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/menu/items/edit/sid/' . $this->_menu->id . '/id/' . $id . '/parent/' . $parent_id);
            }

            Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новый пункт меню"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        $currentSlide = Fenix::getModel('core/backend_menu')->getItemById(
            $this->getRequest()->getParam('id')
        );

        if ($currentSlide == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Пункт меню не найден"))
                ->saveSession();

            Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        if ($this->getRequest()->getParam('parent') !== null) {
            $parent_id = $this->getRequest()->getParam('parent');
        }
        else {
            $parent_id = 0;
        }

        if ($parent_id) {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }
        else {
            $buttonBecklink = Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id);
        }

        // Url автоматом
        $url_key = $this->getRequest()->getPost('url_key');
        if (empty($url_key)) {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl($url_key));

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => Fenix::getUrl('core/menu'),
                'id'    => 'menus',
            ),
            array(
                'label' => $this->_menu->title,
                'uri'   => Fenix::getUrl('core/menu/items/sid/' . $this->_menu->id),
                'id'    => 'menu',
            ),
            array(
                'label' => Fenix::lang("Редактировать пункт меню"),
                'uri'   => '',
                'id'    => 'edit',
            ),
        ));

        $Creator = Fenix::getCreatorUI();


        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentSlide->toArray())
            ->setData('current', $currentSlide)
            ->setData('menu', $this->_menu)
            ->setData('button_beck', $buttonBecklink);

        // Источник
        $Form->setSource('core/menu_item', 'default')
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('parent', $this->_menu->id);

            $this->getRequest()->setPost('url_key', $this->helperMenu->getUrl());

            $id = Fenix::getModel('core/backend_menu')->editItem(
                $Form,
                $currentSlide,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Пункт меню отредактирован"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/menu/items/add/sid/' . $this->_menu->id . '/parent/' . $parent_id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/menu/items/edit/sid/' . $this->_menu->id . '/id/' . $id . '/parent/' . $parent_id);
            }

            Fenix::redirect('core/menu/items/sid/' . $this->_menu->id . '/parent/' . $parent_id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать пункт меню"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }
}