<?php

class Local_Core_Controller_Admin_Menu extends Fenix_Core_Controller_Admin_Menu
{

    public function indexAction()
    {
        $slidersList = Fenix::getModel('core/menu')->getMenuListAsSelect();

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')->setContent(array(
            $Creator->loadPlugin('Button')->appendClass('btn-primary')->setValue(Fenix::lang("Новое меню"))->setType('button')->setOnclick('self.location.href=\'' . Fenix::getUrl('core/menu/add') . '\'')->fetch(),
        ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')->setTitle(Fenix::lang("Управление меню"))->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main',
            ),
            array(
                'label' => Fenix::lang("Управление меню"),
                'uri'   => '',
                'id'    => 'last',
            ),
        ));

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')->setTableId('menusList')->setTitle(Fenix::lang("Управление меню"))->setData($slidersList)->setStandartButtonset();

        $Table->setCellCallback('title', function ($value, $data, $column, $table) {
            return '<a href="' . Fenix::getUrl('core/menu/items/sid/' . $data->id) . '">' . $value . '</a>';
        });

        $Table->addAction([
            'type'  => 'link',
            'icon'  => 'copy',
            'title' => Fenix::lang("Копировать"),
            'url'   => Fenix::getUrl('core/menu/copy/id/{$data->id}')
        ]);

        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/menu/edit/id/{$data->id}'),
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/menu/delete/id/{$data->id}'),
        ));

        // Тайтл страницы
        $Creator->getView()->headTitle(Fenix::lang("Управление меню"));

        // Рендер страницы
        $Creator->setLayout()->oneColumn(array(
            $Title->fetch(),
            $Event->fetch(),
            $Table->fetch('core/menu'),
        ));
    }

    public function copyAction()
    {
        $menuLayout = Fenix::getModel('core/menu')->getMenuById($this->getRequest()->getParam('id'));;

        if ($menuLayout == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage('Шаблон меню не найден')
                ->saveSession();

            Fenix::redirect('core/menu');
        }

        $postData = $this->getRequest()->getPost();
        if (empty($postData)) {
            $deleteKeys = ['id', 'create_date', 'create_id', 'modify_date', 'modify_id'];
            $newMenuLayout = $menuLayout->toArray();
            $data = [];
            foreach ($newMenuLayout as $key => $value) {
                if ( ! in_array($key, $deleteKeys)) {
                    $data[$key] = $value;
                }
            }

            $this->getRequest()->setPost($data);
        }

        $Creator = Fenix::getCreatorUI();

        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setSource('core/menu', 'default')
            ->renderSource();

        $Form->compile();

        if ($Form->ok()) {

            $id = $Form->addRecord($this->getRequest());

            $menuItems = Fenix::getModel('core/menu')->findAllMenuItem($menuLayout->id);

            $newMenuItems = $menuItems->toArray();
            foreach ($newMenuItems as $menuItem) {
                unset($menuItem['id']);
                $menuItem['parent_menu_id'] = 0;
                $menuItem['parent'] = $id;

                $this->getRequest()->clearParams();

                $Form->setSource('core/menu_item', 'default');
                $this->getRequest()->setPost($menuItem);
                $Form->addRecord($this->getRequest());
            }

            $Creator->loadPlugin('Events_Session')->setType(Creator_Events::TYPE_OK)->setMessage(Fenix::lang('Новое меню создано'))->saveSession();

            Fenix::redirect('core/menu');
        }
        // Тайтл страницы
        $Creator->getView()->headTitle(Fenix::lang("Новое меню"));

        $Creator->setLayout()->oneColumn($Form->fetch());
    }
}