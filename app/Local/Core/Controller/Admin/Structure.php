<?php

class Local_Core_Controller_Admin_Structure extends Fenix_Core_Controller_Admin_Structure
{
    public function addAction()
    {
        // Тестирование родительской страницы
        $parentId = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);

        if ($parentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/1');
        }

        $navigation = Fenix::getModel('core/structure')->getNavigation(
            $parentPage
        );

        // Хлебные крошки
        $_crumb = array();
        $_crumb[] = array(
            'label' => Fenix::lang("Панель управления"),
            'id'    => 'home',
            'uri'   => Fenix::getUrl()
        );
        $_crumb[] = array(
            'label' => Fenix::lang("Страницы"),
            'id'    => 'structure',
            'uri'   => Fenix::getUrl('core/structure/parent/1')
        );

        foreach ($navigation AS $i => $_page) {
            $_crumb[] = array(
                'label' => $_page->title,
                'id'    => 'id_' . $_page->id,
                'uri'   => ($navigation->count() > 0 ? Fenix::getUrl('core/structure/parent/1') : null)
            );
        }

        $_crumb[] = array(
            'label' => Fenix::lang("Создать"),
            'id'    => 'add',
            'uri'   => ''
        );

        // Хлебные крошки
        $this->_helper->BreadCrumbs($_crumb);


        $attributeset = ($this->getRequest()->getParam('attributeset') == null ? 'default' : $this->getRequest()->getParam('attributeset'));

        // Url автоматом
        $url_key = $this->getRequest()->getPost('url_key');
        if (empty($url_key)) {
            $url_key = $this->getRequest()->getPost('title_russian');
        }

        $this->getRequest()->setPost('url_key', Fenix::stringProtectUrl(str_replace(' ', '-', $url_key)));

        // Работа с формой
        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setData('current', null)
            ->setData('parentPage', $parentPage);

        // Источник
        $Form->setSource('core/structure', $attributeset)
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {

            $this->getRequest()->setPost('attributeset', $attributeset);
            $this->getRequest()->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));
            $this->getRequest()->setPost('parent', $parentPage->id);

            $block = $this->getRequest()->getPost('block');
            if ($block != null) {
                $service = Fenix::getHelper('core/backend_service_json_encoder');
                $this->getRequest()->setPost('html_content', $service->setData($block)->encode());
            }

            $id = Fenix::getModel('core/structure')->addPage(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Страница создана"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/structure/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/structure/add/attributeset/' . $attributeset . '/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/structure/edit/attributeset/' . $attributeset . '/id/' . $id . '/parent/' . $parentPage->id);
            }

            Fenix::redirect('core/structure/attributeset/' . $attributeset . '/parent/' . $parentPage->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Новая статическая страница"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function editAction()
    {
        // Тестирование родительской страницы
        $parentId = (int) $this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);

        if ($parentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/1');
        }

        // Редактируемая страница
        $currentPage = Fenix::getModel('core/structure')->getPageById(
            $this->getRequest()->getParam('id')
        );

        if ($currentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/' . $parentPage->id);
        }

        $Creator = Fenix::getCreatorUI();

        // Форма
        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentPage->toArray())
            ->setData('current', $currentPage)
            ->setData('parentPage', $parentPage);

        // Источник
        $Form->setSource('core/structure', $currentPage->attributeset)
            ->renderSource();

        // Компиляция
        $Form->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('modify_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('modify_date', date('Y-m-d H:i:s'));

            $block = $this->getRequest()->getPost('block');
            if ($block != null) {
                $service = Fenix::getHelper('core/backend_service_json_encoder');
                $this->getRequest()->setPost('html_content', $service->setData($block)->encode());
            }

            $id = Fenix::getModel('core/structure')->editPage(
                $Form,
                $currentPage,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Статическая страница изменена"))
                ->saveSession();

            if ($this->getRequest()->getPost('save')) {
                Fenix::redirect('core/structure/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('save_add')) {
                Fenix::redirect('core/structure/add/attributeset/' . $currentPage->attributeset . '/parent/' . $parentPage->id);
            }
            elseif ($this->getRequest()->getPost('apply')) {
                Fenix::redirect('core/structure/edit/attributeset/' . $currentPage->attributeset . '/id/' . $id . '/parent/' . $parentPage->id);
            }

            Fenix::redirect('core/structure/attributeset/' . $currentPage->attributeset . '/parent/' . $parentPage->id);
        }

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("Редактировать статическую страницу"));

        $Creator->setLayout()
            ->oneColumn($Form->fetch());
    }

    public function copyAction()
    {
        $parentId = (int)$this->getRequest()->getParam('parent');
        $parentPage = Fenix::getModel('core/structure')->getPageById($parentId);

        if ($parentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Родительская страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/1');
        }

        // Редактируемая страница
        $currentPage = Fenix::getModel('core/structure')->getPageById(
            $this->getRequest()->getParam('id')
        );

        if ($currentPage == null) {
            Fenix::getCreatorUI()
                ->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_ERROR)
                ->setMessage(Fenix::lang("Выбранная Вами страница не найдена"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/' . $parentPage->id);
        }

        $Creator = Fenix::getCreatorUI();

        $Form = $Creator->loadPlugin('Form_Generator');

        $Form->setDefaults($currentPage->toArray())
            ->setData('current', $currentPage)
            ->setData('parentPage', $parentPage);

        // Источник
        $Form->setSource('core/structure', $currentPage->attributeset)
            ->renderSource();

        $Form->compile();

        if ($Form->ok()) {
            $this->getRequest()->setPost('attributeset', $currentPage->attributeset);
            $this->getRequest()->setPost('create_id', Fenix::getModel('session/auth')->getUser()->id);
            $this->getRequest()->setPost('create_date', date('Y-m-d H:i:s'));
            $this->getRequest()->setPost('parent', $parentPage->id);

            $block = $this->getRequest()->getPost('block');
            if ($block != null) {
                $service = Fenix::getHelper('core/backend_service_json_encoder');
                $this->getRequest()->setPost('html_content', $service->setData($block)->encode());
            }

            $id = Fenix::getModel('core/structure')->addPage(
                $Form,
                $this->getRequest()
            );

            $Creator->loadPlugin('Events_Session')
                ->setType(Creator_Events::TYPE_OK)
                ->setMessage(Fenix::lang("Страница создана"))
                ->saveSession();

            Fenix::redirect('core/structure/parent/' . $parentPage->id);
        }
        // Тайтл страницы
        $Creator->getView()->headTitle(Fenix::lang('Новая страница'));

        $Creator->setLayout()->oneColumn($Form->fetch());
    }
}