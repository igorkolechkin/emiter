<?php

class Local_Core_Controller_Admin_Utp extends Fenix_Core_Controller_Admin_Utp
{
    public function indexAction()
    {
        $utpList = Fenix::getModel('core/utp')->getUtpListAsSelect();

        /**
         * Отображение
         */
        $Creator = Fenix::getCreatorUI();

        // Событие
        $Event = $Creator->loadPlugin('Events_Session');

        // Кнопули
        $Buttonset = $Creator->loadPlugin('Buttonset')
            ->setContent(array(
                $Creator->loadPlugin('Button')
                    ->appendClass('btn-primary')
                    ->setValue(Fenix::lang("Новый блок"))
                    ->setType('button')
                    ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/utp/add') . '\'')
                    ->fetch()
            ));

        // Заголовок страницы
        $Title = $Creator->loadPlugin('Title')
            ->setTitle(Fenix::lang("УТП"))
            ->setDetails(Fenix::lang("Можно использовать для создания различных УТП блоков на разных страницах сайта"))
            ->setButtonset($Buttonset->fetch());

        // Хлебные крошки
        $this->_helper->BreadCrumbs(array(
            array(
                'label' => Fenix::lang("Панель управления"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'main'
            ),
            array(
                'label' => Fenix::lang("УТП"),
                'uri'   => '',
                'id'    => 'last'
            )
        ));

        // Таблица
        $Table = $Creator->loadPlugin('Table_Db_Generator')
            ->setTableId('utpList')
            ->setTitle(Fenix::lang("УТП"))
            ->setData($utpList)
            ->setStandartButtonset();

        $Table->setCellCallback('name', function ($value, $data, $column, $table) {
            return '<a href="' . Fenix::getUrl('core/utp/content/sid/' . $data->id) . '">' . $value . '</a>';
        });

        $Table->addAction(array(
            'type'  => 'link',
            'icon'  => 'edit',
            'title' => Fenix::lang("Редактировать"),
            'url'   => Fenix::getUrl('core/utp/edit/id/{$data->id}')
        ));
        $Table->addAction(array(
            'type'  => 'confirm',
            'icon'  => 'trash',
            'title' => Fenix::lang("Удалить"),
            'url'   => Fenix::getUrl('core/utp/delete/id/{$data->id}')
        ));

        // Тайтл страницы
        $Creator->getView()
            ->headTitle(Fenix::lang("УТП"));

        // Рендер страницы
        $Creator->setLayout()
            ->oneColumn(array(
                $Title->fetch(),
                $Creator->loadPlugin('Events')->appendClass('warning')->setMessage(Fenix::lang("Внимание!!! Загружайте изображения в том размере, который будет отображаться на сайте"))->fetch(),
                $Event->fetch(),
                $Table->fetch('core/utp')
            ));
    }
}