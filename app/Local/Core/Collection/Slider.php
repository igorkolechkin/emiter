<?php

class Local_Core_Collection_Slider extends Fenix_Core_Collection_Slider
{
    public function getSliderById($id)
    {
        $Slider = Fenix::getModel('core/slider')->getSliderById($id);

        if ($Slider == null) {
            return null;
        }

        $_slider = $Slider->toArray();
        $_slider['slide_list'] = parent::getSlideList($Slider);

        return new Fenix_Object(array(
            'data' => $_slider
        ));
    }

}