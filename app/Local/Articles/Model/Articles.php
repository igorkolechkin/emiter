<?php

class Local_Articles_Model_Articles extends Fenix_Articles_Model_Articles
{
    /**
     *
     * Получение последних записей по Id
     *
     * @return Zend_Db_Table_Rowset_Abstract
     */
    public function getLastVideoArticlesByRubricId($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('articles/articles');

        $this->setTable('articles');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->join(array(
            'rel' => $this->getTable('articles_relations')
        ), 'a.id = rel.article_id', false);

        $Select->where('rel.rubric_id = ?', (int) $id);
        $Select->where('a.is_public = ?', '1');
        $Select->where('a.video_url != ?', '');
        $Select->order('a.create_date desc');

        $Select->limit(
            (int) (Fenix::getConfig('articles/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('articles/general/last_count'))
        );

        return $this->fetchAll($Select);
    }


    public function getLastArticlesByRubricId($id)
    {
        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('articles/articles');

        $this->setTable('articles');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'a' => $this->_name
        ), $Engine->getColumns(array('prefix' => 'a')));

        $Select->join(array(
            'rel' => $this->getTable('articles_relations')
        ), 'a.id = rel.article_id', false);

        $Select->where('rel.rubric_id = ?', (int) $id);
        $Select->where('a.is_public = ?', '1');
        $Select->order('a.create_date desc');

        $Select->limit(
            (int) (Fenix::getConfig('articles/general/last_count') <= 0 ? self::DEFAULT_LAST_ARTICLES : Fenix::getConfig('articles/general/last_count'))
        );

        return $this->fetchAll($Select);
    }

    public function getProductByArticleIds($ids)
    {
        $data = array();

        if ( ! $ids) {
            return $data;
        }

        if ( ! is_array($ids)) {
            return $data;
        }

        $this->setTable('articles_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'ap' => $this->_name
        ), array('ap.block_id'));

        $Select->join(array(
            'p' => $this->getTable('catalog_products')
        ), 'p.id = ap.product_id', array('p.url_key'));

        $Select->join(array(
            'c' => $this->getTable('catalog')
        ), 'c.id = p.parent', false);

        $Select->where('p.is_public = ?', '1');
        $Select->where('p.is_visible = ?', '1');
        $Select->where('c.is_public = ?', '1');
        $Select->where('ap.block_id IN(?)', $ids);

        return $this->fetchAll($Select);
    }

    public function getArticleByProductId($productId = 0)
    {
        $data = array();

        $productId = (int) $productId;

        if ( ! $productId) {
            return $data;
        }

        $Engine = Fenix_Engine::getInstance();
        $Engine->setSource('articles/articles');

        $this->setTable('articles_products');

        $Select = $this->select()
            ->setIntegrityCheck(false);

        $Select->from(array(
            'ap' => $this->_name
        ), array('ap.block_id'));

        $Select->join(array(
            'a' => $this->getTable('articles')
        ), 'a.id = ap.block_id', $Engine->getColumns(array('prefix' => 'a')));

        $Select->where('a.is_public = ?', '1');
        $Select->where('a.video_url != \'\'');

        return $this->fetchRow($Select);
    }
}