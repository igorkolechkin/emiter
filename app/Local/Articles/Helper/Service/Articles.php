<?php

class Local_Articles_Helper_Service_Articles extends Fenix_Resource_Helper
{
    protected $rubricModel;
    protected $articleModel;

    public function __construct()
    {
        $this->rubricModel = Fenix::getModel('articles/rubric');
        $this->articleModel = Fenix::getModel('articles/articles');
    }

    public function getLastArticles($rubricUrl)
    {
        $data = array();
        $rubricUrl = trim($rubricUrl);
        if ($rubricUrl == '') {
            return $data;
        }

        $rubric = $this->rubricModel->getRubric($rubricUrl);

        if ( ! $rubric) {
            return $data;
        }

        $articles = $this->articleModel->getLastArticlesByRubricId($rubric->id);

        if ($articles->count() <= 0) {
            return $data;
        }

        $data = [
            'rubric'       => $rubric->toArray(),
            'publications' => $articles->toArray()
        ];

        return $data;
    }

    public function getLastVideoArticles()
    {
        $data = array();

        $rubric = $this->rubricModel->getRubric('video-review');

        if ( ! $rubric) {
            return $data;
        }

        $articles = $this->articleModel->getLastVideoArticlesByRubricId($rubric->id);

        if ($articles->count() <= 0) {
            return $data;
        }

        $ids = array();
        $newPublications = array();
        $publications = $articles->toArray();

        foreach ($publications as $publication) {
            $ids[] = $publication['id'];
            $newPublications[$publication['id']] = $publication;
        }

        $products = $this->articleModel->getProductByArticleIds($ids);
        if ($products->count() > 0) {
            foreach ($products as $product) {
                $newPublications[$publication['id']]['product_uri'] = $product['url_key'];
            }
        }

        $data = [
            'rubric'       => $rubric->toArray(),
            'publications' => $newPublications
        ];

        return $data;
    }
}