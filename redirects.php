<?php
//if ( ! isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
//    $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//    header("HTTP/1.1 301 Moved Permanently");
//    header("Location: $redirect");
//}

if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}

if (substr($_SERVER['SERVER_NAME'], 0, 3) == 'www') {
    $uri = str_replace('www.', '', $_SERVER['SERVER_NAME']);
    $uri .= $_SERVER['REQUEST_URI'];

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $uri);
    exit;
}
/**
 * http://domain.com/catalog/ =>
 * http://domain.com/catalog
 */
$uri = $_SERVER['SERVER_NAME'];
$uri .= $_SERVER['REQUEST_URI'];
if (strlen($_SERVER['REQUEST_URI']) > 1 && substr($_SERVER['REQUEST_URI'], -1) == "/") {
    $uri = substr($uri, 0, -1);
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $uri);
    exit;
}
/**
 * http://domain.com/catalog?page=1 =>
 * http://domain.com/catalog
 */
$uri = $_SERVER['SERVER_NAME'];
$uri .= $_SERVER['REQUEST_URI'];
if (strlen($_SERVER['REQUEST_URI']) > 1 && substr($_SERVER['REQUEST_URI'], -7) == "?page=1") {
    $uri = substr($uri, 0, -7);
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $uri);
    exit;
}

/**
 * http://domain.com/index.php =>
 * http://domain.com
 */
if ($_SERVER['REQUEST_URI'] == '/index.php') {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $_SERVER['HTTP_HOST']);
    exit;
}

if ($_SERVER['REQUEST_URI'] == '/index.html') {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $_SERVER['HTTP_HOST']);
    exit;
}
if ($_SERVER['REQUEST_URI'] == '/index') {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $_SERVER['HTTP_HOST']);
    exit;
}

$uri = str_replace('www.', '', $_SERVER['SERVER_NAME']);
$uri .= $_SERVER['REQUEST_URI'];

if (strpos($uri, '//') !== false) {
    $uri = str_replace('//', '/', $uri);

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $uri);
    exit;
}


$uri = $_SERVER['REQUEST_URI'];
if ((strlen($uri) == 3 && substr($uri, 0, 3) == '/ru') || substr($uri, 0, 4) == '/ru/') {
    $langCode = substr($uri, 0, 3);
    $uri = str_replace($langCode, '', $uri);
    $uri = $protocol . $_SERVER['SERVER_NAME'] . $uri;

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $uri);
    exit;
}


$uri = $_SERVER['SERVER_NAME'];
$uri .= $_SERVER['REQUEST_URI'];
//Fenix::dump(substr($_SERVER['REQUEST_URI'], -5));
if (strlen($_SERVER['REQUEST_URI']) > 1 && substr($_SERVER['REQUEST_URI'], -6) == "filter") {
    $uri = substr($uri, 0, -7);
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $uri);
    exit;
}


/**
 * http://domain.com/ABC123?ABc=Bcd =>
 * http://domain.com/abc123?ABc=Bcd
 */
$server = $_SERVER['SERVER_NAME'];
$uri = $_SERVER['REQUEST_URI'];
if (strpos($uri, '?')) {
    $path = substr($uri, 0, strpos($uri, '?'));
    $query = substr($uri, strpos($uri, '?'));
} else {
    $path = $uri;
    $query = '';
}

if (
    preg_match('/[A-Z]+/', $path)
    && substr($_SERVER['REQUEST_URI'], 0, 4) != '/acp'
    && substr($_SERVER['REQUEST_URI'], 0, 4) != '/var'
    && strpos($_SERVER['REQUEST_URI'], '/filter/') === false
    && substr($_SERVER['REQUEST_URI'], 0, 21) != '/home') {

    $path = str_replace(array(
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z'
    ), array(
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z'
    ), $path);

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: ' . $protocol . $server . $path . $query);
    exit;
}