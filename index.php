<?php
mb_internal_encoding("UTF-8");

require_once 'redirects.php';

if (in_array($_SERVER['REMOTE_ADDR'], array('217.24.161.42'))) {
    error_reporting(-1);
    ini_set('display_errors', 1);
}
else {
    ini_set('display_errors', 0);
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
}
//session_save_path("tmp");
ini_set('allow_call_time_pass_reference', 1);
ini_set('memory_limit', '256M');
ini_set('soap.wsdl_cache_enabled', '0');

// Корневая директория
define("DS", DIRECTORY_SEPARATOR);
define("BASE_DIR", dirname(__FILE__) . DS);

require_once 'etc/const.php';
require_once LIB_DIR_ABSOLUTE . 'Fenix.php';


// Указываем список директорий, в которых будем искать файлы
$includePath = implode(PATH_SEPARATOR, array(
    BASE_DIR,
    APP_DIR_ABSOLUTE,
    LIB_DIR_ABSOLUTE,
));

if (function_exists("date_default_timezone_set") && function_exists("date_default_timezone_get")) {
    @date_default_timezone_set(@date_default_timezone_get());
}

$includePath .= get_include_path();
set_include_path($includePath);

// подключаем автолоадер компосера
try {
    require_once LIB_DIR_ABSOLUTE . 'Composer/vendor/autoload.php';
}
catch (Exception $e) {
    new Fenix_Exception($e);
}

// Регистрация автозагрузчика
try {
    $autoloaderScript = 'Zend/Loader/Autoloader.php';

    if ( ! file_exists(LIB_DIR_ABSOLUTE . $autoloaderScript)) {
        throw new Exception("Автозагрузчик не найден <strong>" . $autoloaderScript . "</strong>");
    }

    require_once $autoloaderScript;

    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->registerNamespace("Fenix_");
    $autoloader->setFallbackAutoloader(true);
}
catch (Exception $e) {
    require_once('Fenix/Exception.php');
    new Fenix_Exception($e);
}

// Стартуем сессию
Zend_Session::start();


// Коннектимся к базе данных ZEND
try {
    $dbСonfigFile = ETC_DIR_ABSOLUTE;

    if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
        $dbСonfigFile .= "database_local.xml";
    }
    else {
        $dbСonfigFile .= "database.xml";
    }

    $database = new Zend_Config_Xml($dbСonfigFile, "database");

    Zend_Registry::set('database', $database);

    $db = Zend_Db::factory($database);

    Zend_Db_Table_Abstract::setDefaultAdapter($db);
    $db->query('SET NAMES utf8');

    //Регаем дебаг бар
    if ((Fenix::getStaticConfig()->settings->enable_zfdebug == '1' && Fenix::isDev())) {
        try {
            $autoloader = Zend_Loader_Autoloader::getInstance();
            $autoloader->registerNamespace('ZFDebug');

            $options = array(
                'image_path' => 'var' . DS . 'images',
                'plugins'    => array(
                    'Variables',
                    'Database' => array('adapter' => $db),
                    'File'     => array('basePath' => BASE_DIR),
                    'Exception',
                    'Html',
                    'Log',
                )
            );

            $debug = new ZFDebug_Controller_Plugin_Debug($options);
            $frontController = Zend_Controller_Front::getInstance();
            $frontController->registerPlugin($debug);

        }
        catch (Exception $e) {
            new Fenix_Exception($e);
        }
    }

    /** Профилирование и Дебаг **/
    // Стартуем Xhprof
    define('XHPROF_ENABLED', Fenix_Xhprof::start());

    //Включаем профайлер для БД и Fenix_Debug
    if ((Fenix::getStaticConfig()->settings->dev_mode == '1' && Fenix::isDev()) || XHPROF_ENABLED) {
        $db->getProfiler()
            ->setEnabled(true);

        Fenix_Debug::$enabled = true;
        Fenix_Debug::resetCache();
        Fenix_Debug::log('init application');
    }

    Zend_Registry::set('db', $db);

    // Сначала создается объект кэша
    $cache = Fenix_Cache::getCache('core/database_meta');

    if ($cache !== false) {
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }
}
catch (Zend_Db_Exception $e) {
    $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Сообщение базы данных</h4>';
    $tpl .= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
    $tpl .= $e->getMessage();
    $tpl .= '</div></div>';

    die($tpl);
}


// Инициализация вида
try {
    Zend_Layout::startMvc(array(
        'layoutPath' => 'var/',
    ));

    // Получение объекта Zend_Layout
    $layout = Zend_Layout::getMvcInstance();

    // Инициализация объекта Zend_View
    $view = $layout->getView();

    $view->addScriptPath('var/');

    Zend_Registry::set("getView", $view);

    // Настройка расширения макетов
    $layout->setViewSuffix('phtml');

    // Установка объекта Zend_View
    $layout->setView($view);

    // Настройка расширения view скриптов с помощью Action помошников
    $viewRenderer = new Fenix_Core_Plugin_Controller_Helper_ViewRenderer();
    $viewRenderer
        ->setView($view)
        ->setViewSuffix('phtml');

    Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
}
catch (Exception $e) {
    new Fenix_Exception($e);
}

// Загрузка плагинов контроллера
try {
    $PluginsXml = Fenix::assembleXml('plugins.xml');

    Fenix::loadControllerPlugins($PluginsXml);
    Fenix::loadControllerHelpers($PluginsXml);
    Fenix::loadViewHelpers($PluginsXml);
}
catch (Exception $e) {
    new Fenix_Exception($e);
}

try {
    $request = new Fenix_Controller_Request_Http();

    $languageLib = new Fenix_Language();

    /**
     * Загружаем язык
     */
    $locale = new Zend_Locale($languageLib->getCurrentLanguage()->locale);

    $translate = new Zend_Translate(
        array(
            'adapter'   => 'csv',
            'content'   => LNG_DIR_ABSOLUTE . $languageLib->getCurrentLanguage()->code . '/' . 'frontend',
            'scan'      => Zend_Translate::LOCALE_DIRECTORY,
            'locale'    => $locale,
            'delimiter' => ',',
        )
    );

    Zend_Registry::set('Zend_Translate', $translate);

}
catch (Exception $e) {
    new Fenix_Exception($e);
}

//Проверяем устройство
require_once('mobile.php');
$detect = new Mobile_Detect;
if ($detect->isMobile()):
    $platform['type'] = 'mobile';
else:
    $platform['type'] = 'desctop';
endif;
Zend_Registry::set('Platform', $platform);

// Запуск приложения
try {
    //ob_start ('compress_html');
    $locale = new Zend_Locale('uk_UA');
    Zend_Registry::set('Zend_Locale', $locale);
    $FrontController = Zend_Controller_Front::getInstance();
    $FrontController->setRequest(new Fenix_Controller_Request_Http());
    $FrontController->setDispatcher(new Fenix_Controller_Dispatcher());
    $FrontController->addModuleDirectory(APP_DIR_ABSOLUTE);
    $FrontController->throwexceptions(true)
        ->dispatch();
    //ob_end_flush();
}
catch (Exception $e) {
    new Fenix_Exception($e);
}