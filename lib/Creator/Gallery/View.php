<?php
class Creator_Form_Gallery_View extends Fenix_Creator_Abstract
{ 
    private $_imagesList  = null,
            $_containerId = null;
    
    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.gallery.container');
        $this->setClass('form-element-gallery')
             ->setId(md5(microtime()));
    }
    
    public function addImagesContainer($imageslist = null)
    {
        $this->setAttributeGroup('creator.form.gallery.images');
        $this->setClass('images-container')
             ->setId(md5(microtime()));
        
        $this->_imagesList = $imageslist;
        
        return $this;
    }
    
    public function fetch()
    {
        $this->_containerId = $this->_attr['creator.form.gallery.container']['id'];
        
        $result = $this->_getScripts();
        
        $result.= '<div' . $this->getAttributeSet('creator.form.gallery.container') . '>';
        $result.= '<ul' . $this->getAttributeSet('creator.form.gallery.images') . '>';
        
        // Ранее загруженные изображения
        if ($this->_imagesList != null)
        {
            $images = array();
            
            if (is_array($this->_imagesList))
                $images = $this->_imagesList;
            else
                $images = unserialize($this->_imagesList);
            
            foreach ($images AS $i => $image)
            { 
                if (file_exists(VAR_DIR . 'media/images/' . $image['image']) && $image['image'] != null)
                {
                    $result.= '<li>';
                    $result.= '<a class="fancybox-' . $this->_containerId . '" rel="fancybox-' . $this->_containerId . '" href="' . UPLOAD_IMAGES_URI . $image['image'] . '"><img src="' . Fenix_Image::adapt($image['image'], 140, 140) . '" alt="' . $image['image'] . '" /></a>';
                    $result.= '</li>';
                }
            }
        }
        
        $result.= '</ul><div class="clearfix"></div></div>';
        
        return $result;
    }
    
    private function _getScripts()
    {
        ob_start() ?>
        
        <script>
            $(function(){
                $(".fancybox-<?php echo $this->_containerId ?>").fancybox({
            		prevEffect	: 'fade',
            		nextEffect	: 'fade',
            		helpers	: {
                        title	: {
            				type: 'inside'
            			},
            			thumbs	: {
            				width	: 50,
            				height	: 50
            			}
            		}
            	});
            });
        </script>
        
        <?php return ob_get_clean();
    }
}