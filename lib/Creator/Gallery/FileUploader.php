<?php
mb_internal_encoding("UTF-8");

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../Fenix.php';

// If you want to ignore the uploaded files, 
// set $demo_mode to true;
$demo_mode = false;
$upload_dir = '../../../tmp/uploads/';
$allowed_ext = array('jpg', 'jpeg', 'png', 'gif');


if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
    exit_status('Error! Wrong HTTP method!');
}


if (array_key_exists('gallery', $_FILES) && $_FILES['gallery']['error'] == 0) {

    $pic = $_FILES['gallery'];
    $fileExtension = get_extension($pic['name']);

    if ( ! in_array($fileExtension, $allowed_ext)) {
        exit_status('Only ' . implode(',', $allowed_ext) . ' files are allowed!');
    }

    if ($demo_mode) {

        // File uploads are ignored. We only log them.

        $line = implode('		', array(date('r'), $_SERVER['REMOTE_ADDR'], $pic['size'], $pic['name']));
        file_put_contents('log.txt', $line . PHP_EOL, FILE_APPEND);

        exit_status('Uploads are ignored in demo mode.');
    }

    $newFileName = mb_strtolower($pic['name']);
    $newFileName = stristr($newFileName, '.' . $fileExtension, true);
    $newFileName = Fenix::clearString(Fenix::translitRuEng($newFileName)) . '.' . $fileExtension;

    // Move the uploaded file from the temporary
    // directory to the uploads folder:

    if (move_uploaded_file($pic['tmp_name'], $upload_dir . $newFileName)) {
        $data = [
            'status'   => 'File was uploaded successfuly!',
            'filename' => $newFileName
        ];
        exit_status($data);
    }
}

exit_status('Something went wrong with your upload!');


// Helper functions

function exit_status($data = array())
{
    echo json_encode($data);
    exit;
}

function get_extension($file_name)
{
    $ext = explode('.', $file_name);
    $ext = array_pop($ext);

    return strtolower($ext);
}