<?php
class Creator_Buttonset extends Creator_Abstract
{
    private $_content = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.buttonset')
             ->setClass('btn-group');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Кнопули
        if (isset($Block->content)) {
            $Attributes = false;
            if (isset($Block->attributes)) {
                $Attributes = $Block->attributes->toArray();
                unset($Block->attributes);
            }
            
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }
        
        return $Plugin;
    }
    
    public function setContent($content)
    {
        $this->_content[] = $content;
        return $this;
    }    
    
    public function getContent()
    {
        return $this->renderContent($this->_content);
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/buttonset.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}