<?php
class Creator_Container extends Creator_Abstract
{
    private $_content = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->setAttributeGroup('creator.container')
             ->setFnx('true');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Кнопули
        if (isset($Block->content)) {
            $Attributes = false;
            if (isset($Block->attributes)) {
                $Attributes = $Block->attributes->toArray();
                unset($Block->attributes);
            }
            
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }
        
        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());
        
        return $Plugin;
    }
    
    public function setContent($content)
    {
        $this->_content[] = $content;
        return $this;
    }    

    public function getContent()
    {
        return $this->renderContent($this->_content);
    }
    
    public function fetch()
    {
        $this->_view->assign('Error', null);
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/container.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}