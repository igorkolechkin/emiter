<?php
class Creator_Multiupload extends Creator_Abstract
{ 
    private $_name  = 0,
            $_files = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->setAttributeGroup('creator.multiupload');
        $this->setClass('multiupload')
             ->setId('id_' . md5(microtime()));
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        // Имя
        if (isset($Block->name)) {
            if (isset($Block->name->value)) {
                $Plugin->setName($Block->name->value);
            }
            else {
                $Plugin->setName($Block->name);
            }
            unset($Block->name);
        }
        
        return $Plugin;
    }
    
    public function setFiles(array $files)
    {
        $this->_files = $files;
        return $this;
    }
    
    public function getFiles()
    {
        return $this->_files;
    }
    
    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }
    
    public function getName()
    {
        return $this->_name;
    }
    
    public function fetch()
    {
        $this->_view->assign('Error', null);
        $this->_view
             ->assign('Plugin', $this);
        
        $result = $this->_view->render('creator/multiuploader.phtml');
        return $result;
    }
}