<?php
class Creator_JTree_Actions extends Fenix_Resource_Model
{
    private $_tree  = null,
            $_index = array();
    
    public function setTree($tree)
    {
        $this->_tree = $tree;
    }
    
    public function getInitOpen($nodeId)
    {
        $parent = $nodeId;
        $result = array();
        
        $this->setTable($this->_tree->getTable());
        
        while ($parent > 0) {
            $_select = $this->select()
                            ->from($this, array('id', 'parent'))
                            ->where('id = ?', $parent);
            $_result = $this->fetchRow($_select);
        
            $result[] = 'node_' . $_result->id;
            $parent = $_result->parent;
        }
        
        $result = array_reverse($result);
        
        return Zend_Json::encode($result);
    }
    
    
	private function _get_node($id) 
    {
        $Select = $this->setTable($this->_tree->getTable())
                       ->select()
                       ->from($this, $this->_tree->getFields())
                       ->where('id = ?', (int) $id);
        
        $Result = $this->fetchRow($Select);
        
        if ($Result == null)
            return false;
       
        return $Result;
	}
	private function _get_children($id, $recursive = false) 
    {
		if ($recursive) {
			$node = $this->_get_node($id);
			
            $Select = $this->setTable($this->_tree->getTable())
                           ->select()
                           ->from($this, $this->_tree->getFields())
                           ->where('left >= ?', (int) $node->left)
                           ->where('right <= ?', (int) $node->right)
                           ->order('left asc');
        }
		else {
            $Select = $this->setTable($this->_tree->getTable())
                           ->select()
                           ->from($this, $this->_tree->getFields())
                           ->where('parent = ?', (int) $id)
                           ->order('position asc');
        }
            
        return $this->fetchAll($Select);
    }
    
    
    public function get_children($data)
    {
        $smarty = Fenix_Smarty::getInstance();
        $result = array();
		
        $tmp = $this->_get_children((int) $data['id']);

        foreach($tmp AS $node) {
            $smarty->assign('node', $node);

            $result[] = array(
                "attr" => array(
                    "id"           => "node_" . $node->id, 
                    'data-url'     => $smarty->fetchString($data['url']),
                    'data-editurl' => $smarty->fetchString($data['editurl']),
                ),
                "data"  => ($node->id == 1 ? 'Корень' :  $node->title_russian),
                "state" => "closed"
            );
        }
        
        return json_encode($result);
    }
    
    private function _dump($data)
    {
        $f = fopen('__dump/' . microtime(), 'w');
        fputs($f, print_r($data, 1));
        fclose($f);
    }
}