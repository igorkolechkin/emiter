<?php
class Creator_Callback extends Creator_Abstract
{
    private $_callback = null;
    
    public function setCallback($callback)
    {
        $this->_callback = $callback;
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        $Params = array();

        if (isset($Block->param)) {
            $_params = array();
            
            if (isset($Block->param->{0})) {
                $_params = $Block->param;
            }
            else {
                $_params[] = $Block->param;
            }
            
            foreach ($_params AS $_param) {
                $Params[] = $this->getSmarty()->fetchString($_param);
            }
        }

        if (isset($Block->class)) {
            
            $Plugin->setCallback(array(
                'class'  => $Block->class,
                'method' => $Block->method,
                'params' => $Params
            ));
        }

        return $Plugin;
    }
    
    public function fetch()
    {
        if (is_callable($this->_callback)) {
            $_callback = $this->_callback;
            return $_callback();
        }
        elseif (is_array($this->_callback)) {
            $class  = $this->_callback['class']; 
            $class  = new $class();

            $method = new ReflectionMethod($class, $this->_callback['method']);

            return $method->invokeArgs($class, (array) $this->_callback['params']);
        }
    }
}