<?php
class Creator_Form_Calendar extends Creator_Abstract
{
    private $_title     = null;
    private $_content   = null;
    private $_formId = '';
    private $_UIoptions = array();
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.calendar')
             ->setClass('form-control')
             ->setId('guicalendar_' . Fenix::getRand(6));
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Описание строки
        if (isset($Block->ui)) {
            $Plugin->setUIOptions($Block->ui->toArray());

            unset($Block->ui);
        }

        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }
    
    public function setFormId($formId){
        $this->_formId = $formId;
    }
    
    public function getFormId(){
        return $this->_formId;
    }

    public function setUIOptions(array $options)
    {
        $this->_UIoptions = array_merge($this->_UIoptions, $options);
        return $this;
    }
    
    public function getOptionsJson()
    {
        return Zend_Json::encode($this->_UIoptions);
    }
    
    public function setTitle($text)
    {
        $this->_title = $text;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->_title;
    }
    
    public function setContent($text)
    {
        $this->_content = $text;
        return $this;
    }
    
    public function getContent()
    {
        return $this->_content;
    }
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/calendar.phtml');
    }
}