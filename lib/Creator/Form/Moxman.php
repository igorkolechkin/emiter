<?php
class Creator_Form_Moxman extends Creator_Abstract
{
    private $_value = array();
    private $_formId = '';

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.moxman')
             ->setId(md5(microtime()));
    }

    public function generator($Generator, $Plugin, $Block)
    {
        if (isset($Block->value)) {
            $Plugin->setValue($this->getSmarty()->fetchString($Block->value));

            unset($Block->value);
        }


        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }


    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }

    /**
     * Список изображений по умолчанию
     *
     * @param array $defaults
     */
    public function setValue($value)
    {
        $this->_value = $value;

        return $this;
    }

    /**
     * Список изображений по умолчанию
     *
     * @return array
     */
    public function getValue()
    {
        return $this->_value;
    }

    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);

        $this->_view
             ->assign('Plugin', $this);

        return $this->_view
                    ->render('creator/form/moxman.phtml');
    }
}