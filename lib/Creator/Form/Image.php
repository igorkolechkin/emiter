<?php

class Creator_Form_Image extends Creator_Abstract
{
    private $_details = null;
    private $_image = null;
    private $_imageInfo = null;
    private $_storage = 'filesystem';
    private $_formId = '';

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.image')
            ->setClass('gui-form-image')
            ->setType('file');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Описание строки
        if (isset($Block->details)) {
            if (isset($Block->details->value)) {
                $Plugin->setDetails($Block->details->value);
            } else {
                $Plugin->setDetails($Block->details);
            }

            if (isset($Block->details->attributes)) {
                $Plugin->setAttributes($Block->details->attributes->toArray());
            }

            unset($Plugin->details);
        }

        if (isset($Block->setId)) {
            $Plugin->setId($Block->setId);
            unset($Block->setId);
        }

        if (isset($Block->setImage)) {
            $Plugin->setImage($this->getSmarty()->fetchString($Block->setImage));
            unset($Block->setImage);
        }

        if (isset($Block->setStorage)) {
            $Plugin->setStorage($Block->setStorage);
            unset($Block->setStorage);
        }


        if (isset($Block->setImageInfo)) {
            $Plugin->setImageInfo($this->getSmarty()->fetchString($Block->setImageInfo));
            unset($Block->setImageInfo);
        }

        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }


    public function setFormId($formId)
    {
        $this->_formId = $formId;
    }

    public function getFormId()
    {
        return $this->_formId;
    }

    public function getImageInfo()
    {
        return ($this->_imageInfo == null ? false : (object)unserialize($this->_imageInfo));
    }

    public function setImageInfo($imageInfo)
    {
        $this->_imageInfo = $imageInfo;
        return $this;
    }

    public function getImage()
    {
        return $this->_image;
    }

    public function setImage($image)
    {
        $this->_image = $image;
        return $this;
    }

    public function getStorage()
    {
        return $this->_storage;
    }

    public function setStorage($storage)
    {
        $this->_storage = $storage;
        return $this;
    }

    public function setDetails($text)
    {
        $this->_details = $text;

        return $this;
    }

    public function getDetails()
    {
        return $this->_details;
    }

    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);

        $error = $this->_validateField();

        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
            $this->appendClass('gui-field-error');
        } else {
            $this->_view->assign('Error', null);
        }

        $this->_view
            ->assign('Plugin', $this);

        return $this->_view
            ->render('creator/form/image.phtml');
    }
}