<?php
class Creator_Form_ErrorBlock extends Creator_Abstract
{
    private $_errors = array();
    private $_formId = '';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.form.errorblock')
             ->setClass('gui-form-errorblock');
    }

    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }

    public function setErrorArray($errors)
    {
        $this->_errors = $errors;
        return $this;
    }

    public function getErrorArray()
    {
        return $this->_errors;
    }
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/errorblock.phtml');
    }
}