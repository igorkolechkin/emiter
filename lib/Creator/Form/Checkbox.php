<?php
class Creator_Form_Checkbox extends Creator_Abstract
{
    private $_formId = '';

    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.form.input')
             ->setClass('gui-form-checkbox')
             ->setType('checkbox')
             ->setValue(1);
    }

    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }
    
    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);
        
        $error = $this->_validateField();
        
        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
            $this->appendClass('gui-field-error');
        }
        else {
            $this->_view->assign('Error', null);
        }
        
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/checkbox.phtml');
    }
}