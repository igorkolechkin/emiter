<?php

class Creator_Form_Generator extends Creator_Form
{
    private $_xml = array();
    private $_engine = array();
    public $_xmlContent = null;

    private $_dbRecordHtmlPlugins = array(
        'Creator_Form_Checkbox',
        'Creator_Form_Image',
        'Creator_Form_Radio',
        'Creator_Form_Select',
        'Creator_Form_Text',
        'Creator_Form_Textarea',
        'Creator_Form_Moxman',
        'Creator_Gallery',
        'Creator_Form_File',
        'Creator_Form_Rating',
        'Creator_Form_Wysiwyg'
    );

    static private $_dbRecordHtmlBlocks = array();

    static public function appendDbField($block)
    {
        self::$_dbRecordHtmlBlocks[] = $block;
    }

    static public function getDbFields()
    {
        return new ArrayObject(self::$_dbRecordHtmlBlocks, ArrayObject::ARRAY_AS_PROPS);
    }

    public function setSource($template, $attributeset)
    {
        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate($template);

        $this->_engine = $Engine;

        $Xml = $Engine->getXml();

        if (isset($Xml->creator->form->{$attributeset})) {
            $this->_xml = $Xml->creator->form->{$attributeset};
        }

        return $this;
    }

    public function setXmlSource(Zend_Config $source)
    {
        $this->_xml = $source;

        return $this;
    }

    public function renderSource()
    {
        if (isset($this->_xml->content)) {
            $this->setContent($this->renderBlock($this->_xml->content));
        }
        return $this;
    }

    public function renderBlock($_content, $_test = false)
    {
        $block = array();

        if ($_content instanceof Zend_Config || $_content instanceof Zend_Config_Xml) {
            if (isset($_content->block->{0})) {
                $block = $_content->block;
            } else {
                $block[] = $_content->block;
            }

            $this->getSmarty()
                ->assign('request', $this->getRequest());
            $this->getSmarty()
                ->assign('data', $this->_data);

            $Result = null;
            foreach ($block AS $_block) {
                if (isset($_block->plugin)) {
                    $_plugin = $this->loadPlugin($_block->plugin);

                    unset($_block->plugin);

                    if (in_array(get_class($_plugin), $this->_dbRecordHtmlPlugins)) {
                        Creator_Form_Generator::appendDbField(clone $_block);
                    }

                    if (method_exists($_plugin, 'generator')) {

                        if (isset($_block->sql)) {
                            unset($_block->sql);
                        }

                        $Reflection = new ReflectionMethod($_plugin, 'generator');

                        $GeneratedPlugin = $Reflection->invokeArgs($_plugin, array($this, $_plugin, $_block));

                        $Result .= $this->getSmarty()->fetchString($GeneratedPlugin->fetch());
                    } else {
                        $Result .= 'В плагине ' . get_class($_plugin) . ' не объявлен метод генерации';
                    }
                }
            }
            return $Result;
        } else {
            return $_content;
        }
    }


    /**
     * Новая запись в таблице, по шаблону XML
     *
     * @param Fenix_Controller_Request_Http $req
     * @return int Идентификатор строки
     */
    public function addRecord(Fenix_Controller_Request_Http $req)
    {
        $data = array();
        $Model = Fenix::getModel();
        $Model->setTable($this->_engine->getXml()->table->name);

        $nextId = $Model->nextId();

        foreach (Creator_Form_Generator::getDbFields() AS $_block) {

            if (isset($_block->sql)) {
                switch ($_block->sql->html) {
                    case 'text':
                        if (isset($_block->setSplitByLang)) {
                            foreach ($this->getLang()->getLanguagesList() AS $_lang) {
                                $data[$_block->sql->column . '_' . $_lang->name] = stripcslashes($this->getRequest()->getPost(
                                    $_block->sql->column . '_' . $_lang->name
                                ));
                            }
                        } else {
                            $data[$_block->sql->column] = stripcslashes($this->getRequest()->getPost(
                                $_block->sql->column
                            ));
                        }
                        break;

                    case 'textarea':
                        if (isset($_block->setSplitByLang)) {
                            foreach ($this->getLang()->getLanguagesList() AS $_lang) {
                                $data[$_block->sql->column . '_' . $_lang->name] = stripcslashes($this->getRequest()->getPost(
                                    $_block->sql->column . '_' . $_lang->name
                                ));
                            }
                        } else {
                            $data[$_block->sql->column] = stripcslashes($this->getRequest()->getPost(
                                $_block->sql->column
                            ));
                        }
                        break;

                    case 'image':
                        $file = '';
                        $_files = $req->getFiles($_block->name);

                        if ($_files->tmp_name != null) {
                            if (!is_dir(HOME_DIR_ABSOLUTE . $this->_engine->getXml()->table->name . '/page' . $nextId . '/')) {
                                mkdir(HOME_DIR_ABSOLUTE . $this->_engine->getXml()->table->name . '/page' . $nextId . '/', 0777, true);
                            }

                            if ($file != null && file_exists(HOME_DIR_ABSOLUTE . $file)) {
                                unlink(HOME_DIR_ABSOLUTE . $file);
                            }

                            copy($_files->tmp_name, HOME_DIR_ABSOLUTE . $this->_engine->getXml()->table->name . '/page' . $nextId . '/' . $_files->name);
                            $file = $this->_engine->getXml()->table->name . '/page' . $nextId . '/' . $_files->name;
                        }

                        $data[$_block->sql->column] = $file;
                        $req->setPost($_block->sql->column, $data[$_block->sql->column]);

                        // Сохраняем информацию об изображении
                        $imageInfo = Fenix_ImageInfo::getImageInfo($file);

                        $req->setPost($_block->sql->column . '_info', serialize($imageInfo));

                        break;

                    case 'file':
                        $columnData = $_block->sql->column;
                        $_files = $req->getFiles($_block->name);

                        // Если файл существует - сохраняем
                        if ($_files->tmp_name != null) {
                            if ($_block->path && $_block->path != '')
                                $filesDir = $this->_engine->getXml()->table->name . '/' . $_block->path . '/' . $record->id . '/';
                            else
                                $filesDir = $this->_engine->getXml()->table->name . '/item_' . $record->id . '/';

                            if (!is_dir(HOME_DIR_ABSOLUTE . $filesDir)) {
                                mkdir(HOME_DIR_ABSOLUTE . $filesDir, 0777, true);
                            }

                            copy($_files->tmp_name, HOME_DIR_ABSOLUTE . $filesDir . $_files->name);

                            $md5Filename = $_files->name;
                            $file = $filesDir . $md5Filename;
                        }

                        $data[$_block->sql->column] = $file;
                        $req->setPost($_block->sql->column, $data[$_block->sql->column]);

                        break;


                    case 'moxman':
                        $_gallery = array();

                        foreach ((array)$req->getPost($_block->sql->column) AS $_url) {
                            $_gallery[] = array(
                                'url' => $_url
                            );
                        }

                        $req->setPost($_block->sql->column, serialize($_gallery));

                        break;

                    case 'gallery':
                        $gallery = Fenix::getModel('core/common')->uploadImages($req->getPost($_block->sql->column), 'articles/' . $nextId . '/', null);
                        $req->setPost($_block->sql->column, serialize($gallery));
                        break;
                }
            }
        }

        $_newData = array();
        foreach ($req->getPost() AS $_key => $_value) {
            if ($this->_isInColumns($_key))
                $_newData[$_key] = (array_key_exists($_key, $data) ? $data[$_key] : $_value);
        }

        $resultId = $Model->insert($_newData);
        return $resultId;
    }


    public function editRecord(Zend_Db_Table_Row $record, Fenix_Controller_Request_Http $req)
    {
        $data = array();
        foreach (Creator_Form_Generator::getDbFields() AS $_block) {
            if (isset($_block->sql)) {
                switch ($_block->sql->html) {
                    case 'text':
                        if (isset($_block->setSplitByLang)) {
                            foreach ($this->getLang()->getLanguagesList() AS $_lang) {
                                $data[$_block->sql->column . '_' . $_lang->name] = stripcslashes($this->getRequest()->getPost(
                                    $_block->sql->column . '_' . $_lang->name
                                ));
                            }
                        } else {
                            $data[$_block->sql->column] = stripcslashes($this->getRequest()->getPost(
                                $_block->sql->column
                            ));
                        }
                        break;

                    case 'textarea':
                        if (isset($_block->setSplitByLang)) {
                            foreach ($this->getLang()->getLanguagesList() AS $_lang) {
                                $data[$_block->sql->column . '_' . $_lang->name] = stripcslashes($this->getRequest()->getPost(
                                    $_block->sql->column . '_' . $_lang->name
                                ));
                            }
                        } else {
                            $data[$_block->sql->column] = stripcslashes($this->getRequest()->getPost(
                                $_block->sql->column
                            ));
                        }
                        break;

                    case 'image':
                        $columnData = $_block->sql->column;
                        $file = $record->{$columnData};
                        $_files = $req->getFiles($_block->name);

                        // Удаляем файл в случаях: если загружен новый или если выбрано удаление
                        if (($req->getPost($_block->name . '_delete') == '1' && $file != null) || ($_files->name != null && $file != null)) {
                            unlink(HOME_DIR_ABSOLUTE . $file);
                            $file = '';
                        }

                        // Если файл существует - сохраняем
                        if ($_files->tmp_name != null) {
                            if ($_block->path && $_block->path != '')
                                $filesDir = $this->_engine->getXml()->table->name . '/' . $_block->path . '/' . $record->id . '/';
                            else
                                $filesDir = $this->_engine->getXml()->table->name . '/item_' . $record->id . '/';

                            if (!is_dir(HOME_DIR_ABSOLUTE . $filesDir)) {
                                mkdir(HOME_DIR_ABSOLUTE . $filesDir, 0777, true);
                            }

                            //Удаляем
                            if ($file != null && file_exists(HOME_DIR_ABSOLUTE . $file)) {
                                unlink(HOME_DIR_ABSOLUTE . $file);
                            }

                            copy($_files->tmp_name, HOME_DIR_ABSOLUTE . $filesDir . $_files->name);

                            $ext = pathinfo(HOME_DIR_ABSOLUTE . $filesDir . $_files->name, PATHINFO_EXTENSION);
                            $md5Filename = md5($_files->name) . '.' . $ext;
                            rename(HOME_DIR_ABSOLUTE . $filesDir . $_files->name, HOME_DIR_ABSOLUTE . $filesDir . $md5Filename);
                            $file = $filesDir . $md5Filename;
                        }

                        $data[$_block->sql->column] = $file;
                        $req->setPost($_block->sql->column, $data[$_block->sql->column]);

                        // Очистка информации об изображении
                        if (($req->getPost($_block->name . '_delete') == '1')) {
                            $req->setPost($_block->sql->column . '_info', '');
                        } elseif ($_files->size > 0) {
                            // Сохраняем информацию об изображении
                            $imageInfo = Fenix_ImageInfo::getImageInfo($file);

                            $req->setPost($_block->sql->column . '_info', serialize($imageInfo));
                        }

                        break;

                    case 'file':
                        $columnData = $_block->sql->column;
                        $file = $record->{$columnData};
                        $_files = $req->getFiles($_block->name);

                        // Удаляем файл в случаях: если загружен новый или если выбрано удаление
                        if (($req->getPost($_block->name . '_delete') == '1' && $file != null) || ($_files->name != null && $file != null)) {
                            unlink(HOME_DIR_ABSOLUTE . $file);
                            $file = '';
                        }

                        // Если файл существует - сохраняем
                        if ($_files->tmp_name != null) {
                            if ($_block->path && $_block->path != '')
                                $filesDir = $this->_engine->getXml()->table->name . '/' . $_block->path . '/' . $record->id . '/';
                            else
                                $filesDir = $this->_engine->getXml()->table->name . '/item_' . $record->id . '/';

                            if (!is_dir(HOME_DIR_ABSOLUTE . $filesDir)) {
                                mkdir(HOME_DIR_ABSOLUTE . $filesDir, 0777, true);
                            }

                            // Удаляем
                            if ($file != null && file_exists(HOME_DIR_ABSOLUTE . $file)) {
                                unlink(HOME_DIR_ABSOLUTE . $file);
                            }

                            copy($_files->tmp_name, HOME_DIR_ABSOLUTE . $filesDir . $_files->name);

                            $md5Filename = $_files->name;
                            $file = $filesDir . $md5Filename;
                        }

                        $data[$_block->sql->column] = $file;
                        $req->setPost($_block->sql->column, $data[$_block->sql->column]);

                        break;

                    case 'moxman':
                        $_gallery = array();

                        foreach ((array)$req->getPost($_block->sql->column) AS $_url) {
                            $_gallery[] = array(
                                'url' => $_url
                            );
                        }
                        $req->setPost($_block->sql->column, serialize($_gallery));
                        break;

                    case 'gallery':
                        $gallery = Fenix::getModel('core/common')->uploadImages($req->getPost($_block->sql->column), 'articles/' . $record->id . '/', null);
                        $req->setPost($_block->sql->column, serialize($gallery));
                        break;
                }
            }
        }

        $_newData = array();
        foreach ($req->getPost() AS $_key => $_value) {
            if ($this->_isInColumns($_key))
                $_newData[$_key] = (array_key_exists($_key, $data) ? $data[$_key] : $_value);
        }

        $Model = Fenix::getModel();
        $Model->setTable($this->_engine->getXml()->table->name);

        $Model->update($_newData, 'id = ' . (int)$record->id);

        return $record->id;
    }

    public function deleteRecord(Zend_Db_Table_Row $record)
    {
        $this->renderSource();

        foreach (Creator_Form_Generator::getDbFields() AS $_block) {
            if (isset($_block->sql)) {
                switch ($_block->sql->html) {
                    case 'gallery':
                        $_gallery = (array)unserialize($record->{$_block->name});
                        foreach ($_gallery AS $_image) {
                            if (file_exists(HOME_DIR_ABSOLUTE . $_image['image'])) {
                                unlink(HOME_DIR_ABSOLUTE . $_image['image']);
                            }
                        }
                        break;
                    case 'image':
                        if (!empty($record->{$_block->name}) && file_exists(HOME_DIR_ABSOLUTE . $record->{$_block->name})) {
                            unlink(HOME_DIR_ABSOLUTE . $record->{$_block->name});
                            $file = '';
                        }
                        break;
                }
            }
        }

        $Model = Fenix::getModel();
        $Model->setTable($this->_engine->getXml()->table->name);
        $Model->delete('id = ' . (int)$record->id);

        return $record->id;
    }

    private function _isInColumns($column)
    {
        return in_array($column, $this->_engine->getColumnsList());
    }
}