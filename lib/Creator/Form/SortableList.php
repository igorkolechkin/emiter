<?php
class Creator_Form_SortableList extends Creator_Abstract
{
    private $_details  = null,
        $_options  = array(),
        $_selected = null;
    public  $loadIn = false;
    private $_formId = '';

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.select')
             ->setClass('gui-form-select');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Описание строки
        if (isset($Block->details)) {
            if (isset($Block->details->value)) {
                $Plugin->setDetails($Block->details->value);
            }
            else {
                $Plugin->setDetails($Block->details);
            }

            if (isset($Block->details->attributes)) {
                $Plugin->setAttributes($Block->details->attributes->toArray());
            }

            unset($Plugin->details);
        }

        // Список опшинов
        if (isset($Block->options)) {
            if (isset($Block->options->generator)) {
                $_generator = Fenix::getModel($Block->options->generator->class);
                $method = $Block->options->generator->method;
                $_generator->{$method}($Plugin, $Block);
            }
            else {
                $options = array();

                if (isset($Block->options->option->{0})) {
                    $options = $Block->options->option;
                }
                else {
                    $options[] = $Block->options;
                }

                foreach ($options AS $_option) {
                    $Plugin->addOption($_option->name, $_option->_value);
                }
            }
            unset($Block->options);
        }

        if (isset($Block->selected)) {
            $Plugin->setSelected($this->getSmarty()->fetchString($Block->selected));

            unset($Block->selected);
        }

        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }

    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }

    public function getSelected()
    {
        return $this->_selected;
    }

    public function setSelected($selected)
    {
        $this->_selected = $selected;
        return $this;
    }

    public function loadIn(array $_options)
    {
        $this->loadIn = $_options;
        return $this;
    }

    public function addItem($value, $class= null)
    {
        $this->_options[] = array(
            'value' => $value,
            'class' => $class
        );

        return $this;
    }

    public function getOptions()
    {
        return $this->_options;
    }

    public function setDetails($text)
    {
        $this->_details = $text;
        return $this;
    }

    public function getDetails()
    {
        return $this->_details;
    }

    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);

        $error = $this->_validateField();

        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
            $this->appendClass('gui-field-error');
        }
        else {
            $this->_view->assign('Error', null);
        }

        $this->_view
            ->assign('Plugin', $this);

        return $this->_view
            ->render('creator/form/sortable-list.phtml');
    }
}