<?php
class Creator_Form_File extends Creator_Abstract
{
    private $_details      = null;
    private $_file         = null;
    public  $isUploadFile  = 1;
    private $_storage      = 'filesystem';
    private $_formId = '';

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.file')
             ->setClass('gui-form-image')
             ->setType('file');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Описание строки
        if (isset($Block->details)) {
            if (isset($Block->details->value)) {
                $Plugin->setDetails($Block->details->value);
            }
            else {
                $Plugin->setDetails($Block->details);
            }

            if (isset($Block->details->attributes)) {
                $Plugin->setAttributes($Block->details->attributes->toArray());
            }

            unset($Plugin->details);
        }

        if (isset($Block->setId)) {
            $Plugin->setId($Block->setId);
            unset($Block->setId);
        }

        if (isset($Block->setImage)) {
            $Plugin->setImage($this->getSmarty()->fetchString($Block->setImage));
            unset($Block->setImage);
        }

        if (isset($Block->setFile)) {
            $Plugin->setFile($this->getSmarty()->fetchString($Block->setFile));
            unset($Block->setFile);
        }

        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }

    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }

    public function getFile()
    {
        return $this->_file;
    }

    public function setFile($file)
    {
        $this->_file = $file;
        return $this;
    }

    public function setDetails($text)
    {
        $this->_details = $text;

        return $this;
    }

    public function getDetails()
    {
        return $this->_details;
    }

    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);

        $error = $this->_validateField();

        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
            $this->appendClass('gui-field-error');
        }
        else {
            $this->_view->assign('Error', null);
        }

        $this->_view
             ->assign('Plugin', $this);

        return $this->_view
                    ->render('creator/form/file.phtml');
    }
}