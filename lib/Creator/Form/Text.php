<?php

    class Creator_Form_Text extends Creator_Abstract {
        private $_details          = null;
        private $_is_multiple      = false;
        private $_mask             = null;
        private $_autocompleteData = array();
        private $_unique_checker = array();
        private $_formId = '';

        public function __construct() {
            parent::__construct();

            $this->setAttributeGroup('creator.form.input')
                ->setClass('form-control input-block-level')
                 ->setType('text');
        }

        public function setFormId($formId){
            $this->_formId = $formId;
        }

        public function getFormId(){
            return $this->_formId;
        }

        public function setTranslitSource($input_name)
        {
            $this->_attr[$this->_attrGroup]['translit_source'] = $input_name;
            return $this;
        }

        public function getUniqueChecker()
        {
            if(!empty($this->_unique_checker)){
                return $this->_unique_checker;
            }
            return false;
        }

        public function setUniqueChecker($table, $field, $current = null)
        {
            $this->_unique_checker = array('table' => $table, 'field' => $field, 'current' => $current);
        }

        public function setTranslit()
        {
            $this->_attr[$this->_attrGroup]['translit'] = true;
            return $this;
        }

        public function generator($Generator, $Plugin, $Block) {
            if(isset($Block->setValidator)) {

                $validator = array();

                if(isset($Block->setValidator->{0})) {
                    $validator = $Block->setValidator;
                } else {
                    $validator[] = $Block->setValidator;
                }

                if(isset($Block->setUniqueChecker)){
                    $checker = $Block->setUniqueChecker;
                    if(!isset($checker->table)){
                        throw new Exception('Указание таблицы для проверки уникальности обязательно');
                    }elseif (!isset($checker->field)){
                        throw new Exception('Указание столбца для проверки уникальности обязательно');
                    }
                    $current = null;
                    if(isset($checker->current)){
                        $current = $checker->current;
                    }

                    $this->_unique_checker = array('table' => $checker->table, 'field' => $checker->field, 'current' => $current);
                    unset($Block->setUniqueChecker);
                }

                foreach($validator AS $_validator) {

                    $_class = $_validator->validator;
                    unset($_validator->validator);

                    $if = true;
                    if(isset($_validator->if)) {
                        $if = (bool)$this->getSmarty()->fetchString($_validator->if);
                    }

                    if($if === true) {
                        $_options = (array)$_validator->toArray();
                        foreach($_options AS $_key => $_value) {
                            $_options[ $_key ] = $this->getSmarty()->fetchString($_value);
                        }
                        $object = new $_class($_options);
                        $this->setValidator($object);
                    }
                }

                unset($Block->setValidator);
            }
            // Описание строки
            if(isset($Block->details)) {
                if(isset($Block->details->value)) {
                    $Plugin->setDetails($Block->details->value);
                } else {
                    $Plugin->setDetails($Block->details);
                }

                if(isset($Block->details->attributes)) {
                    $Plugin->setAttributes($Block->details->attributes->toArray());
                }

                unset($Plugin->details);
            }

            if(isset($Block->is_multiple)) {
                if(isset($Block->is_multiple->value)) {
                    $Plugin->setMultiple($Block->is_multiple->value);
                } else {
                    $Plugin->setMultiple($Block->is_multiple);
                }
                unset($Plugin->is_multiple);
            }

            // Разбивка на языки
            if(isset($Block->setSplitByLang)) {

                $Plugin->setSplitByLang();
                unset($Plugin->setSplitByLang);
            }

            // Вызываем магический метод __call
            $Plugin->setAttributes($Block->toArray());

            return $Plugin;
        }

        public function setDetails($text) {
            $this->_details = $text;

            return $this;
        }

        public function getDetails() {
            $details = '';
            if($this->_is_multiple) {
                $details = Fenix::lang('Для ввода нового значения нажмите клавишу Enter') . '<br>';
            }

            $details .= $this->_details;

            return $details;
        }

        public function setMultiple($value) {
            $this->_is_multiple = (int)$value;

            return $this;
        }

        public function getMultiple() {
            return $this->_is_multiple;
        }

        /** Set Autocomplete data for jQuery UI autocomplete
         *  https://jqueryui.com/autocomplete/
         */
        public function setAutocompleteData(array $data) {
            $this->_autocompleteData = array();

            foreach($data as $languageName => $_languageData){
                $this->_autocompleteData[$languageName] = array_unique($_languageData);
            }

            return $this;
        }

        /** Get Autocomplete data for jQuery UI autocomplete
         *  https://jqueryui.com/autocomplete/
         */
        public function getAutocompleteData($languageName) {
            if(!empty($this->_autocompleteData[$languageName])) {
                // $dataStr = '["' . implode('","', $this->_autocompleteData[$langCode]) . '"]';
                $dataStr = '[';
                foreach($this->_autocompleteData[$languageName] as $_item) {
                    $dataStr .= '"' . addslashes(str_replace(array("\t", "\r", "\n"), '', strip_tags($_item))) . '",';
                }
                $dataStr .= ']';
            } else {
                $dataStr = '[]';
            }

            return $dataStr;
        }

        public function fetch() {
            Creator_UI::addHtmlField($this->getName(), $this);

            $error = $this->_validateField();

            if(sizeof($error) > 0) {
                $this->_view->assign('Error', $error);
                $this->appendClass('gui-field-error');
            } else {
                $this->_view->assign('Error', null);
            }

            $this->_view
                ->assign('Plugin', $this);

            return $this->_view
                ->render('creator/form/input.phtml');
        }

        /**
         * Задаем маску для инпута
         *
         * @param string $mask
         *
         * @return $this
         */
        public function setMask($mask) {
            $this->_mask = $mask;

            return $this;
        }

        /**
         * Получаем маску для инпута
         * @return string
         */
        public function getMask() {
            return $this->_mask;
        }
    }