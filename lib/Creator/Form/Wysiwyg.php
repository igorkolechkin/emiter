<?php
class Creator_Form_Wysiwyg extends Creator_Abstract
{
    private $_value   = null;
    private $_details = null;
    private $_formId = '';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.form.wysiwyg')
             ->setClass('gui-form-wysiwyg')
             ->setId('guiwysiwyg_' . Fenix::getRand(6));
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        // Описание
        if (isset($Block->details)) {
            if (isset($Block->details->value)) {
                $Plugin->setDetails($Block->details->value);
            }
            else {
                $Plugin->setDetails($Block->details);
            }
            
            if (isset($Block->details->attributes)) {
                $Plugin->setAttributes($Block->details->attributes->toArray());
            }
            
            unset($Plugin->details);
        }
        
        if (isset($Block->textarea)) {
            $Plugin->setValue($Block->textarea);
            unset($Block->textarea);
        }

        // Разбивка на языки 
        if (isset($Block->setSplitByLang)) {
            
            $Plugin->setSplitByLang();
            unset($Plugin->setSplitByLang);
        }
                
        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());
        
        return $Plugin;
    }

    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }
    
    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
    
    public function setDetails($text)
    {
        $this->_details = $text;
        
        return $this;
    }
    
    public function getDetails()
    {
        return $this->_details;
    }
    
    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);
        
        $error = $this->_validateField();
        
        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
        }
        else {
            $this->_view->assign('Error', null);
        }

        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/wysiwyg.phtml');
    }
}