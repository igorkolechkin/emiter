<?php
class Creator_Form_Radio extends Creator_Abstract
{
    private $_details = null,
            $_options = array();
    private $_formId = '';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.form.radio')
             ->setClass('gui-form-radio')
             ->setType('radio');
    }


    public function setFormId($formId){
        $this->_formId = $formId;
    }

    public function getFormId(){
        return $this->_formId;
    }
        
    public function addOption($key, $value)
    {
        $this->_options[] = array(
            'key'   => $key,
            'value' => $value
        );
        
        return $this;
    }
    
    public function getOptions()
    {
        return $this->_options;
    }
    
    public function setDetails($text)
    {
        $this->_details = $text;
        return $this;
    }
    
    public function getDetails()
    {
        return $this->_details;
    }
    
    public function fetch()
    {
        Creator_UI::addHtmlField($this->getName(), $this);
        
        $error = $this->_validateField();
        
        if (sizeof($error) > 0) {
            $this->_view->assign('Error', $error);
            $this->appendClass('gui-field-error');
        }
        else {
            $this->_view->assign('Error', null);
        }
        
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/radio.phtml');
    }
}