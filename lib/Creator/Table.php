<?php
	
	class Creator_Table extends Creator_Abstract {
		private $_title = null,
			$_cell = array(),
			$_form = false;
		
		private $_columns = array();
		
		protected $_checkall = false;
		
		public function __construct() {
			parent::__construct();
			
			$this->setAttributeGroup('creator.table')
			     ->setClass('table')
			     ->appendClass('table-hover')
			     ->appendClass('table-striped')
			     ->appendClass('table-bordered');
		}
		
		public function getForm() {
			return $this->_form;
		}
		
		public function setForm($form) {
			$this->_form = $form;
			
			return $this;
		}
		
		public function getCheckall() {
			if($this->_checkall === false) {
				return false;
			}
			
			return (object)$this->_checkall;
		}
		
		public function setCheckall($checkall = array()) {
			
			if(!array_key_exists('name', $checkall)) {
				$checkall['name'] = 'row';
			}
			
			$this->_checkall = $checkall;
			
			return $this;
		}
		
		public function getTableId() {
			return $this->_attr['creator.table']['id'];
		}
		
		public function setTitle($text) {
			$this->_title = $text;
			$this->setAttributeGroup('creator.table.caption');
			
			return $this;
		}
		
		public function getTitle() {
			return $this->_title;
		}
		
		public function addHeader($cellId, $content = '') {
			$rowId = 'header';
			
			$this->setAttributeGroup('creator.table.' . $rowId . '.' . $cellId);
			$this->setClass('gui-table-header')
			     ->setId($rowId . '-' . $cellId);
			
			$this->_cell['header'][ $rowId ][ $cellId ] = array(
				'rowId'   => $rowId,
				'cellId'  => $cellId,
				'content' => $content,
			);
			
			return $this;
		}
		
		public function addFilter($cellId, $content = '') {
			$rowId = 'filter';
			
			$this->setAttributeGroup('creator.table.' . $rowId . '.' . $cellId);
			$this->setClass('gui-table-filter')
			     ->setId($rowId . '-' . $cellId);
			
			$this->_cell['filter'][ $rowId ][ $cellId ] = array(
				'rowId'   => $rowId,
				'cellId'  => $cellId,
				'content' => $content,
			);
			
			return $this;
		}
		
		public function addCell($rowId, $cellId, $content = '', $rowAttributes = array()) {
			$this->setAttributeGroup('creator.table.' . $rowId . '.' . $cellId);
			$this->setClass('gui-table-cell')
			     ->setId($rowId . '-' . $cellId);
			
			$this->_cell['cell'][ $rowId ][ $cellId ] = array(
				'rowId'   => $rowId,
				'cellId'  => $cellId,
				'content' => $content,
				'tr'      => $rowAttributes,
			);
			
			return $this;
		}
		
		public function addColumn(array $column) {
			$this->_columns[] = $column;
			
			return $this;
		}
		
		public function getCellList() {
			return $this->_cell;
		}
		
		public function fetch() {
			foreach($this->_columns AS $row => $_column) {
				foreach($_column as $coll => $cell) {
					$this->addCell('body_' . $row, $coll, $cell);
				}
			}


			$this->_view
				->assign('Plugin', $this);
			
			return $this->_view
				->render('creator/table/table.phtml');
		}
		
		public function __toString() {
			return $this->fetch();
		}
	}