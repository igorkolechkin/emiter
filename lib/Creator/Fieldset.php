<?php
class Creator_Fieldset extends Creator_Abstract
{
    private $_title   = null;
    private $_content = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.fieldset')
             ->setId('guifieldset_' . Fenix::getRand(6))
             ->setClass('gui-fieldset');
        
        $this->setImage();
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты филдсета
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }
        
        // Легенда
        if (isset($Block->legend)) {
            if (isset($Block->legend->value)) {
                $Plugin->setLegend($Block->legend->value);
            }
            else {
                $Plugin->setLegend($Block->legend);
            }
            
            if (isset($Block->legend->attributes)) {
                $Plugin->setAttributes($Block->legend->attributes->toArray());
            }
        }
        
        // Контент
        if (isset($Block->content)) {
            
            $Attributes = false;
            if (isset($Block->content->attributes)) {
                $Attributes = $Block->content->attributes->toArray();
                unset($Block->content->attributes);
            }
            
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }

        return $Plugin;
    }
    
    public function setLegend($title)
    {
        $this->_title = $title;
        $this->setAttributeGroup('creator.fieldset.legend')
             ->setClass('gui-fieldset-legend');
        return $this;
    }    
    public function getLegend()
    {
        return $this->_title;
    }    
    
    public function setContent($content)
    {
        $this->_content = $content;
        $this->setAttributeGroup('creator.fieldset.content')
             ->setClass('gui-fieldset-content');
        
        return $this;
    }
    public function getContent()
    {
        return $this->_content;
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/fieldset.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}