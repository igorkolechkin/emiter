<?php
class Creator_Link extends Creator_Abstract
{
    private $_label = null;
	
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.link');
    }
	
    public function setLabel($label)
    {
        $this->_label = $label;
        return $this;	
    }

    public function fetch()
    {
       	$result = '<a' . $this->getAttributeSet('creator.link') . '>' . $this->_label . '</a>';
        return $result;
    }
    
    public function __toString()
    {
        return $this->fetch();
    }
}