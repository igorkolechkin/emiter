<?php
class Creator_Calendar extends Creator_Abstract
{
    private $_content = null;
    private $_events  = array();
    
    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.calendar');
    }
    
    public function setContent($content)
    {
        $this->_content = $content;
        return $this;
    }
    
    public function setEvents(array $events)
    {
        $this->_events = $events;
        
        return $this;
    }
  
    public function getEvents()
    {
        return $this->_events;
    }
        
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);

        return $this->_view
                    ->render('creator/calendar.phtml');
    }
}