<?php
class Creator_Button extends Creator_Abstract
{
    private $_icon = null;

    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.form.button')
             ->setClass('btn')
             ->setType('button');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        if (isset($Block->icon)) {
            $Attributes = false;
            if (isset($Block->icon->attributes)) {
                $Attributes = $Block->icon->attributes->toArray();
                unset($Block->icon->attributes);
            }

            $Plugin->setIcon($Block->icon);
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->icon);
        }

        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());
        
        return $Plugin;
    }

    /**
     * @param null $icon
     * @return Creator_Button
     */
    public function setIcon($icon)
    {
        $this->_icon = $icon;
        return $this;
    }

    /**
     * @return null
     */
    public function getIcon()
    {
        return $this->_icon;
    }

    public function fetch()
    {
        $this->_view->assign('Error', null);
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/form/button.phtml');
    }
}