<?php
class Creator_Table_Db extends Creator_Table
{
    private $_id        = null;
    private $_tablename = null;
    private $_columns   = array();
    private $_data      = null;
    private $_buttonset = array();
    private $_actions   = array();
    private $_form      = false;
    private $_defaultPerpage = 15;
    private $_upLevel        = null;
    private $_rowCallback    = null;
    
    public function __construct() {
        parent::__construct();
        $this->setForm();
    }

    public function setTablename($tablename)
    {
        $this->_tablename = $tablename;
        return $this;
    }

    public function getTablename()
    {
        return $this->_tablename;
    }

    public function addAction(array $action) {
        $this->_actions[] = $action;
        return $this;
    }
    
    public function getForm() {
        return $this->_form;
    }

    public function setForm($form = true) {
        $this->_form = $form;
        return $this;
    }
    
    public function getUpLevel() {
        return $this->_upLevel;
    }

    public function setUpLevel($upLevel) {
        $this->_upLevel = $upLevel;
        return $this;
    }
        
    public function getButtonset() {
        return $this->_buttonset;
    }

    public function setButtonset(array $buttonset) {
        $this->_buttonset = $buttonset;
        return $this;
    }
    
    public function setRowCallback($rowCallback) {
        $this->_rowCallback = $rowCallback;
        return $this;
    }

    public function setStandartButtonset() {
        
        $this->setButtonset(array(
            $this->loadPlugin('Button')
                 ->appendClass('btn-info')
                 ->setValue("Фильтровать")
                 ->setType('submit')
                 ->fetch(),
            $this->loadPlugin('Button')
                 ->appendClass('btn-inverse')
                 ->setValue("Сброс")
                 ->setType('button')
                 ->setOnclick('self.location.href=\'?' . $this->_prepareActionUrl('unsetFilter') . '\'')
                 ->fetch(),
            $this->loadPlugin('Button')
                 ->appendClass('btn-success')
                 ->setValue("Сохранить")
                 ->setType('submit')
                 ->fetch(),
            $this->loadPlugin('Button')
                 ->appendClass('btn-danger')
                 ->setValue("Удалить")
                 ->setType('submit')
                 ->fetch(),
        ));
        
        return $this;
    }

    public function setButtonsetMailbox() {

        $this->setButtonset(array(
            $this->loadPlugin('Button')
                ->appendClass('btn-warning')
                ->setValue("Рассылка")
                ->setType('submit')
                ->fetch(),
            $this->loadPlugin('Button')
                ->appendClass('btn-info')
                ->setValue("Редактировать шаблон письма")
                ->setType('button')
                ->setOnclick('self.location.href=\'' . Fenix::getUrl('core/mail/edit/id/9') . '\'')
                ->fetch()
        ));

        return $this;
    }

    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }
    public function getData()
    {
        return $data;
    }
    
    public function addColumn(Creator_Table_Db_Column $column)
    {
        $this->_columns[] = $column;
        return $this;
    }
    
    public function setTableId($id)
    {
        $this->_id = $id;
        return $this;
    }
    
    public function getTableId()
    {
        return $this->_id;
    }
    
    public function getModel()
    {
        return Fenix::getModel();
    }
    
    public function prepareData()
    {
        $dbСonfig  = $this->_data
                          ->getAdapter()
                          ->getConfig();
        if ($this->_tablename == null) {
            $table = str_replace($dbСonfig['prefix'], '', $this->_data->getTable()->getTable());
        }
        else {
            $table = $this->_tablename;
        }

        $Model = $this->getModel();
        $Model ->setTable($table);
        
        $query = (array) $this->getRequest()->getQuery('_cT');
        if (isset($query[$this->_id]))
        {
            $query = $query[$this->_id];

            foreach ($query AS $action => $_query) {
                switch ($action) {
                    case 'sorting':
                        $column = key($_query);
                        $value  = $_query[$column];

                        if ($value != 'asc' && $value != 'desc')
                            $value = 'asc';

                        $this->_data->reset(Zend_Db_Select::ORDER);
                        $this->_data->order($column . ' ' . $value);
                    break;
                    
                    case 'filter':
                        foreach ($_query AS $column => $value) {

                            if (is_array($value)) {
                                if (!isset($value['from']))
                                    $value['from'] = null;
                                if (!isset($value['to']))
                                    $value['to'] = null;

                                if ($value['from'] != null && $value['to'] == null) {
                                    $this->_data
                                         ->where($column . ' >= ?', $value['from']);
                                }
                                elseif ($value['from'] != null && $value['to'] != null) {
                                    $From = $this->_data->getAdapter()->quoteInto("?", $value['from']);
                                    $To   = $this->_data->getAdapter()->quoteInto("?", $value['to']);

                                    $this->_data
                                         ->where("{$column} >= {$From} AND {$column} <= {$To}");
                                }
                                elseif ($value['from'] == null && $value['to'] != null) {
                                    $this->_data
                                         ->where($column . ' <= ?', $value['to']);
                                }
                            }
                            else {
                                $this->_data
                                     ->where($column . ' LIKE ?', '%' . $value . '%');
                            }
                        }
                    break;
                    
                    case 'edit':

                        foreach ($_query AS $rowId => $updateData) {
                            $Model->update($updateData, 'id = ' . (int) $rowId);
                        }

                        $updatePosition = false;
                        foreach ($_query AS $rowId => $updateData) {
                            if (array_key_exists('position', $updateData)) {
                                $updatePosition = true;
                                break;
                            }
                        }

                        if ($updatePosition === true) {
                           /* $Select = $Model->select()
                                            ->from($Model, array('id', 'position'))
                                            ->order('position asc');

                            if ($this->getRequest()->getParam('parent') > 0) {
                                $Select->where('parent = ?', (int) $this->getRequest()->getParam('parent'));
                            }

                            $Result = $Model->fetchAll($Select);

                            $i = 0;
                            foreach ($Result AS $_rowData) {
                                $Model->update(array(
                                    'position' => ($i + 1)
                                ), 'id = ' . (int) $_rowData->id);

                                $i++;
                            }*/
                        }
                        
                        header('Location: ' . $this->getRequest()->getPathinfo() . '?' . $this->_prepareActionUrl('unsetEdit'));
                        exit;
                        
                    break;
                }
            }
        }

        $adapter   = new Zend_Paginator_Adapter_DbTableSelect($this->_data);
        $adapter   ->setRowCount($Model->fetchAll($this->_data)->count());

        $perPage   = $this->_defaultPerpage;
        $curPage   = $this->_getUrlValue('page', 'curr');
        
        $paginator = new Zend_Paginator($adapter);
        $paginator ->setCurrentPageNumber((int) $curPage)
                   ->setItemCountPerPage((int) $perPage);
        
        return $paginator;
    }
    
    private function _getUrlValue($action, $sql)
    {
        $query = (array) $this->getRequest()->getQuery();
        switch ($action) {
            case 'sorting':
                if (isset($query['_cT'][$this->_id][$action][$sql]))
                    return $query['_cT'][$this->_id][$action][$sql];
            break;
            
            case 'page':
                if (isset($query['_cT'][$this->_id][$action][$sql]))
                    return $query['_cT'][$this->_id][$action][$sql];
            break;
            
            case 'filter':
                if (isset($query['_cT'][$this->_id][$action][$sql]))
                    return $query['_cT'][$this->_id][$action][$sql];
            break;
        }
        
        return;
    }
    
    private function _prepareActionUrl($action, $sql = null, $value = null)
    {
        $query = (array) $this->getRequest()->getQuery();
        
        switch ($action) {
            case 'sorting':
                if (isset($query['_cT'][$this->_id][$action]))
                    unset($query['_cT'][$this->_id][$action]);
                
                $query['_cT'][$this->_id][$action][$sql] = $value;
            break;
            
            case 'multipleDelete':
                $query['_cT'][$this->_id][$action][$sql] = $value;
            break;
            
            case 'page':
                if (isset($query['_cT'][$this->_id][$action]))
                    unset($query['_cT'][$this->_id][$action]);
                
                $query['_cT'][$this->_id][$action][$sql] = $value;
            break;
            
            case 'unsetFilter':
                if (isset($query['_cT'][$this->_id]['filter']))
                    unset($query['_cT'][$this->_id]['filter']);
                if (isset($query['_cT'][$this->_id]['sorting']))
                    unset($query['_cT'][$this->_id]['sorting']);
                if (isset($query['_cT'][$this->_id]['page']))
                    unset($query['_cT'][$this->_id]['page']);
            break;
            
            case 'unsetEdit':
                if (isset($query['_cT'][$this->_id]['edit']))
                    unset($query['_cT'][$this->_id]['edit']);
            break;
        }
        
        $query = http_build_query($query);
        
        return $query;
    }
    
    public function buildFormQuery()
    {
        $query = (array) $this->getRequest()->getQuery();
        
        if (isset($query['_cT'][$this->_id]['filter']))
            unset($query['_cT'][$this->_id]['filter']);
        
        $query = http_build_query($query);
        $query = rawurldecode($query);
        
        $_queryList = explode('&', $query);
        $hiddenList = array();
        
        foreach ($_queryList AS $_queryPart) {
            if ($_queryPart != null) {
                $_queryParts = explode('=', $_queryPart);
                $hiddenList[$_queryParts[0]] = $_queryParts[1];
            }
        }
        return $hiddenList;
    }
    
    private function _parseCellValue($cellValue, $column, $data)
    {
        if ($column->getEditable() !== false) {
            $editable = $column->getEditable();
            
            switch ($editable['html']) {
                case 'text':
                    $Field = $this->loadPlugin('Form_Text')
                                  ->setStyle('width:100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box; height:28px;')
                                  ->setId('id_' . md5('_cT[' . $this->_id . '][edit][' . $data->id . '][' . $column->getSqlCellName() . ']'))
                                  ->setName('_cT[' . $this->_id . '][edit][' . $data->id . '][' . $column->getSqlCellName() . ']')
                                  ->setValue($cellValue);

                    $cellValue = $Field->fetch();
                break;
                
                case 'select':
                    $Field = $this->loadPlugin('Form_Select')
                                  ->setStyle('width:70px;')
                                  ->setId('id_' . md5('_cT[' . $this->_id . '][edit][' . $data->id . '][' . $column->getSqlCellName() . ']'))
                                  ->setName('_cT[' . $this->_id . '][edit][' . $data->id . '][' . $column->getSqlCellName() . ']')
                                  ->setSelected($cellValue);

                    foreach ($editable['options']['option'] AS $_option) {
                        $Field->addOption($_option['name'], $_option['_value']);
                    }

                    $cellValue = $Field->fetch();
                break;
                
                default:
                    return $cellValue;
            }
        }
        
        return $cellValue;
    }
    
    public function fetch()
    {
        foreach ($this->_columns AS $_column) {
            if ($_column->getHeaderCallback() != null && is_callable($_column->getHeaderCallback())) {
                $_callback   = $_column->getHeaderCallback();
                
                $headerValue = $_callback($_column->getTitle(), $_column, $this);
            }
            else {
                $headerValue = $_column->getTitle();
            }
            
            if ($_column->getSortable() === true) {
                $headerValue = '<span class="table-header-sortable">' . $headerValue . '</span>';
            }
            
            $this->addHeader($_column->getId(), $headerValue);
            
            if ($_column->getSortable() === true) {
                
                $sortDirection = null;
                $urlValue = $this->_getUrlValue('sorting', $_column->getSqlCellNameAs());
                if ($urlValue != null && $urlValue == 'asc')
                    $sortDirection = 'desc';
                elseif ($urlValue != null && $urlValue == 'desc')
                    $sortDirection = 'asc';
                
                if ($sortDirection == 'asc') {
                    $headerValue.= '<span class="glyphicon glyphicon-arrow-up"></span>';
                }
                elseif ($sortDirection == 'desc') {
                    $headerValue.= '<span class="glyphicon glyphicon-arrow-down"></span>';
                }
                
            }
            
            $this->addHeader($_column->getId(), $headerValue);
            if ($_column->getSortable() === true) {
                if ($sortDirection == 'asc') {
                    $this->appendClass('table-sorting-desc');
                }
                elseif ($sortDirection == 'desc') {
                    $this->appendClass('table-sorting-asc');
                }
                
                $this->appendClass('hovered')
                     ->setOnclick('self.location.href=\'?' . $this->_prepareActionUrl('sorting', $_column->getSqlCellNameAs(), ($sortDirection == null ? 'asc' : $sortDirection)) . '\'');
            }
            
            $this->setAttributes($_column->getHeaderAttributes());
        }
        
        if (sizeof($this->_actions) > 0) {
            $this->addHeader('tableDbActions', null)
                 ->setColspan(sizeof($this->_actions));
        }
        
        /**
         * Фильтр
         */
        $useFilter = false;
        foreach ($this->_columns AS $_column) {
            if ($_column->getFilter() !== false) {
                $useFilter = true; 
                break;
            }
        }
        
        if ($useFilter === true) {
            foreach ($this->_columns AS $_column) {

                $filterField = null;
                
                switch ($_column->getFilter('html')) {
                    case 'text':
                    default:
                        $urlValue = $this->_getUrlValue('filter', $_column->getSqlCellNameAs());
                        
                        if ($_column->getFilter('type') == 'single') {
                            
                            switch ($_column->getFilter('html')) {

                                case 'text':
                                default:
                                    $filterField = $this->loadPlugin('Form_Text')
                                        ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . ']')
                                        ->setValue($urlValue)
                                        ->fetch();
                                    break;

                                case 'select':
                                    $filterField = $this->loadPlugin('Form_Select')
                                                        ->setStyle('width:70px;')
                                                        ->setId('id_' . md5('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . ']'))
                                                        ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . ']')
                                                        ->setSelected($urlValue);
                                    
                                    $_options = $_column->getFilter('options');
                                    foreach ($_options['option'] AS $_option) {
                                        $filterField->addOption($_option['name'], $_option['_value']);
                                    }
                                    
                                    $filterField = $filterField->fetch();
                                break;
                            }
                        }
                        elseif ($_column->getFilter('type') == 'range') {
                            $filterField = $this->loadPlugin('Form_Text')
                                                ->appendStyle('margin-bottom:5px;')
                                                ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . '][from]')
                                                ->setValue((array_key_exists('from', (array) $urlValue) ? $urlValue['from'] : null))
                                                ->fetch();
                            $filterField.= $this->loadPlugin('Form_Text')
                                                ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . '][to]')
                                                ->setValue((array_key_exists('to', (array) $urlValue) ? $urlValue['to'] : null))
                                                ->fetch();
                        }
                    break;


                    case 'calendar':
                        $filterField = $this->loadPlugin('Form_Calendar')
                                            ->appendStyle('margin-bottom:5px;')
                                            ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . '][from]')
                                            ->setValue((array_key_exists('from', (array) $urlValue) ? $urlValue['from'] : null))
                                            ->fetch();
                        $filterField.= $this->loadPlugin('Form_Calendar')
                                            ->setName('_cT[' . $this->_id . '][filter][' . $_column->getSqlCellNameAs() . '][to]')
                                            ->setValue((array_key_exists('to', (array) $urlValue) ? $urlValue['to'] : null))
                                            ->fetch();
                    break;

                }


                $this->addFilter($_column->getId(), $filterField);
            }
            
            if (sizeof($this->_actions) > 0) {
                $this->addFilter('tableDbActions', null)
                     ->setColspan(sizeof($this->_actions));
            }
        }
        
        /**
         * Данные
         */
        $data = $this->prepareData();
        $dataRowId = array();
        
        foreach ($data as $i => $_data) {
            
            $this->getSmarty()
                 ->assign('data', $_data);
            
            $dataRowId[$i] = $_data->id;
            
            foreach ($this->_columns AS $_column) {
                $_dbCellName = $_column->getSqlCellName();
                
                try {
                    $cellValue = $_data->{$_dbCellName};
                }
                catch(Zend_Db_Table_Row_Exception $e) {
                    $cellValue = 'N/A';
                }
                
                if ($_column->getCellCallback() != null && is_callable($_column->getCellCallback())) {
                    $_callback = $_column->getCellCallback();
                    $cellValue = $_callback($cellValue, $_data, $_column, $this);
                }
                else {
                    $cellValue = $this->_parseCellValue($cellValue, $_column, $_data);
                }
                
                $this->addCell(
                    'row_' . $i,
                    $_column->getId(),
                    $cellValue
                );
                
                $this->setAttributes($_column->getCellAttributes());
            }
            
            foreach ($this->_actions AS $j => $_action) {
                
                if (isset($_action['url'])) {
                    $_action['url'] = $this->getSmarty()->fetchString($_action['url']);
                }
                
                switch ($_action['type']) {
                    case 'link':
                        $this->addCell('row_' . $i, 'dbTableAction_' . $j, '<a title="' . (isset($_action['title']) ? $_action['title'] : null) . '" '.(isset($_action['target']) ? 'target="'.$_action['target'].'"' : null).' href="' . $_action['url'] . '"><i class="icon-' . $_action['icon'] . '"></i></a>')
                             ->appendClass('table-action');
                        /*$this->addCell('row_' . $i, 'dbTableAction_' . $j,
                            '<div class="hidden-phone visible-desktop btn-group"><button title="' . (isset($_action['title']) ? $_action['title'] : null) . '" onclick="self.location.href=' . $_action['url'] . '" class="btn btn-mini btn-success"><i style="font-size:20px;" class="icon-'  . $_action['icon'] . ' bigger-120"></i></button></div>'
                        );*/
                    break;
                    
                    case 'confirm':
                        $this->addCell('row_' . $i, 'dbTableAction_' . $j, '<a title="' . (isset($_action['title']) ? $_action['title'] : null) . '" href="' . $_action['url'] . '" onclick="return confirm(\'Подтвердите действие\')"><i class="icon-' . $_action['icon'] . '"></i></a>')
                             ->appendClass('table-action');
                    break;
                    
                    case 'sorting':
                        
                        $Sorting = $this->loadPlugin('Table_Db_Column');
                        $Sorting ->setEditable($_action['options']);
                        $Sorting ->setSqlCellName('position');
                        
                        $Field = $this->_parseCellValue($_data->position, $Sorting, $_data);
                        
                        $this->addCell('row_' . $i, 'dbTableAction_' . $j, $Field)
                             ->appendClass('table-sorting-cell');
                    break;
                }
                $this->setWidth(20);
            }
            
            if (is_callable($this->_rowCallback)) {
                $_func = $this->_rowCallback;
                $this->setAttributeGroup('creator.table.row.row_' . $i);
                $_func($this, $_data);
            }
        }
        $this->getSmarty()
             ->assign('dataRowId', $dataRowId);
        $this->getView()
             ->assign('dataList',  $data)
             ->assign('url',       urldecode(Fenix::getRequest()->getPathInfo() . '?' . $this->_prepareActionUrl('page', 'curr', '%s')));
        
        $this->_attr['creator.table']['id'] = $this->getTableId();
        return parent::fetch();
    }
}