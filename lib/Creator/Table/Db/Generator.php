<?php

class Creator_Table_Db_Generator extends Creator_Table_Db
{
    private $_xml      = array();
    private $_callback = array();

    public function setSource($template)
    {
        $Engine = new Fenix_Engine_Database();
        $Engine->setDatabaseTemplate($template);

        $Xml = $Engine->getXml();

        if (isset($Xml->creator->table)) {
            $this->_xml = $Xml->creator->table;
        }

        return $this;
    }

    public function setCellCallback($id, $callback)
    {
        $this->_callback['cell'][$id] = $callback;

        return $this;
    }

    public function fetch($source = null)
    {
        $this->setSource($source);

        $columns = array();

        if (isset($this->_xml->columns->{0})) {
            $columns = $this->_xml->columns;
        }
        else {
            $columns[] = $this->_xml->columns;
        }

        foreach ($columns AS $_column) {
            $TableDbColumn = $this->loadPlugin('Table_Db_Column');

            if (isset($_column->title)) {
                $TableDbColumn->setTitle($_column->title);
            }

            if (isset($_column->sortable)) {
                $TableDbColumn->setSortable((bool) $_column->sortable);
            }

            if (isset($_column->id)) {
                $TableDbColumn->setId($_column->id);
            }

            if (isset($_column->sqlcellname)) {
                $TableDbColumn->setSqlCellName($_column->sqlcellname, (isset($_column->sqlcellnameas) ? $_column->sqlcellnameas : null));
            }

            if (isset($_column->headerattributes)) {
                $TableDbColumn->setHeaderAttributes($_column->headerattributes->toArray());
            }

            if (isset($_column->cellattributes)) {
                $TableDbColumn->setCellAttributes($_column->cellattributes->toArray());
            }

            if (isset($_column->filter)) {
                $TableDbColumn->setFilter($_column->filter->toArray());
            }

            if (isset($_column->editable)) {
                $TableDbColumn->setEditable($_column->editable->toArray());
            }

            if (isset($this->_callback['cell'][$_column->id])) {
                $TableDbColumn->setCellCallback($this->_callback['cell'][$_column->id]);
            }

            $this->addColumn($TableDbColumn);
        }

        return parent::fetch();
    }
}