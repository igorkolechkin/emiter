<?php
class Creator_Table_Db_Column extends Creator_Abstract
{
    private $_title = null;
    private $_id    = null;
    private $_headerAttributes = array();
    private $_headerCallback   = null;
    private $_cellAttributes   = array();
    private $_cellCallback     = null;
    private $_sortable         = false;
    private $_editable         = false;
    private $_sqlName          = null;
    private $_sqlAs            = null;
    private $_filter           = false;
    
    public function getTitle()
    {
        return $this->_title;
    }
    public function setTitle($title) 
    {
        $this->_title = $title;
        return $this;
    }
    
    public function getEditable() {
        return $this->_editable;
    }

    public function setEditable($editable) {
        $this->_editable = $editable;
        return $this;
    }

    public function getFilter($key = null) {
        if ($key == null)
            return $this->_filter;
        
        if (isset($this->_filter[$key]))
            return $this->_filter[$key];
        
        return null;
    }

    public function setFilter(array $filter) {
        $this->_filter = $filter;
        return $this;
    }
        
    public function getId() 
    {
        return $this->_id;
    }

    public function setId($id) 
    {
        $this->_id = $id;
        return $this;
    }
    
    public function getSortable() {
        return $this->_sortable;
    }

    public function setSortable($sortable = true) {
        $this->_sortable = $sortable;
        return $this;
    }

    public function getHeaderAttributes() {
        return $this->_headerAttributes;
    }

    public function setHeaderAttributes($headerAttributes) {
        $this->_headerAttributes = $headerAttributes;
        return $this;
    }
    
    public function getHeaderCallback() {
        return $this->_headerCallback;
    }

    public function setHeaderCallback($headerCallback) {
        $this->_headerCallback = $headerCallback;
        return $this;
    }

    public function getCellAttributes() {
        return $this->_cellAttributes;
    }

    public function setCellAttributes($cellAttributes) {
        $this->_cellAttributes = $cellAttributes;
        return $this;
    }
    
    public function getCellCallback() {
        return $this->_cellCallback;
    }

    public function setCellCallback($cellCallback) {
        $this->_cellCallback = $cellCallback;
        return $this;
    }

    public function setSqlCellName($sqlName, $sqlAs = null) {
        $this->_sqlName = $sqlName;
        $this->_sqlAs   = ($sqlAs == null ? $sqlName : $sqlAs);
        
        return $this;
    }
    public function getSqlCellName() {
        return $this->_sqlName;
    }
    public function getSqlCellNameAs() {
        return $this->_sqlAs;
    }
            
    public function fetch(){}
}