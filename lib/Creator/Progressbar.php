<?php
class Creator_Progressbar extends Creator_Abstract
{
    const BAR_COLOR_INFO    = 'info';
    const BAR_COLOR_SUCCESS = 'success';
    const BAR_COLOR_WARNING = 'warning';
    const BAR_COLOR_DANGER  = 'danger';
    private $_content = array();
    private $_values = array();
    private $_min    = array();
    private $_max    = array();
    private $_labels = array();
    private $_colors = array();

    private $_process_url = null;
    private $_params = null;
    private $_is_striped = false;

    public function __construct()
    {
        parent::__construct();
        $this->setAttributeGroup('creator.container')
             ->setFnx('true');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Кнопули
        if (isset($Block->content)) {
            $Attributes = false;
            if (isset($Block->attributes)) {
                $Attributes = $Block->attributes->toArray();
                unset($Block->attributes);
            }
            
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }
        
        // Вызываем магический метод __call
        $Plugin->setAttributes($Block->toArray());
        
        return $Plugin;
    }
    
    public function setContent($content)
    {
        $this->_content[] = $content;
        return $this;
    }    

    public function getContent()
    {
        return $this->renderContent($this->_content);
    }
    
    public function fetch()
    {
        $this->_view->assign('Error', null);
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/progressbar.phtml');
    }

    public function setValue($value, $index = null)
    {
        if ($index != null)
            $this->_values[$index] = $value;
        else {
            if (is_array($value))
                $this->_values = $value;
            else
                $this->_values = array($value);
        }
        return $this;
    }

    public function getValue($index = null){
        if($index){
            if(isset($this->_values[$index]))
                return $this->_values[$index];
            else
                return null;
        }

        if(isset($this->_values[0]))
            return $this->_values[0];
        else
            return null;
    }

    public function getValues(){
        return $this->_values;
    }

    public function setLabel($label, $index = null)
    {
        if ($index)
            $this->_labels[$index] = $label;
        else {
            if (is_array($label))
                $this->_labels = $label;
            else
                $this->_labels = array($label);
        }
        return $this;
    }

    public function getLabel($index = null){
        if($index){
            if(isset($this->_labels[$index]))
                return $this->_labels[$index];
            else
                return null;
        }

        if(isset($this->_labels[0]))
            return $this->_labels[0];
        else
            return null;
    }
    public function setColor($color, $index = null)
    {
        if($index)
            $this->_colors[$index] = $color;
        else {
            if (is_array($color))
                $this->_colors = $color;
            else
                $this->_colors = array($color);
        }
        return $this;
    }

    public function getColor($index = null){
        if($index){
            if(isset($this->_colors[$index]))
                return $this->_colors[$index];
            else
                return null;
        }

        if(isset($this->_colors[0]))
            return $this->_colors[0];
        else
            return null;
    }

    public function setMin($min, $index = null)
    {
        if ($index)
            $this->_min[$index] = $min;
        else {
            if (is_array($min))
                $this->_min = $min;
            else
                $this->_min = array($min);
        }

        return $this;
    }

    public function getMin($index = null){
        if($index){
            if(isset($this->_min[$index]))
                return $this->_min[$index];
            else
                return null;
        }

        if(isset($this->_min[0]))
            return $this->_min[0];
        else
            return null;
    }
    public function setMax($max, $index = null)
    {
        if($index)
            $this->_max[$index] = $max;
        else {
            if (is_array($max))
                $this->_max = $max;
            else
                $this->_max = array($max);
        }
        return $this;
    }

    public function getMax($index = null){
        if($index){
            if(isset($this->_max[$index]))
                return $this->_max[$index];
            else
                return null;
        }

        if(isset($this->_max[0]))
            return $this->_max[0];
        else
            return null;
    }
    public function __toString()
    {
        return (string)$this->fetch();
    }

    /**
     * @return boolean
     */
    public function isStriped()
    {
        return $this->_is_striped;
    }

    /**
     * @param boolean $is_striped
     * @return $this
     */
    public function setIsStriped($is_striped = true)
    {
        $this->_is_striped = $is_striped;
        return $this;
    }

    /**
     * @return null
     */
    public function getProcessUrl()
    {
        return $this->_process_url;
    }

    /**
     * @param null $process_url
     * @return $this
     */
    public function setProcessUrl($process_url)
    {
        $this->_process_url = $process_url;

        return $this;
    }

    /**
     * @return null
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @param null $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->_params = $params;

        return $this;
    }
}