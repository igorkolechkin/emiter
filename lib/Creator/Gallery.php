<?php

class Creator_Gallery extends Creator_Abstract
{
    private $_containerId = null,
        $_dropzoneId = null,
        $_previewsId = null,
        $_imagesId = null,
        $_filesName = 'gallery',
        $_fileUploader = '/lib/Creator/Gallery/FileUploader.php',
        $_imagesList = null,
        $_attributes = null;

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form.gallery.container');
        $this->setClass('form-element-gallery')
            ->setId(md5(microtime()));
    }

    public function generator($Generator, $Plugin, $Block)
    {
        $Plugin->addDropZone();
        $Plugin->addPreviewsContainer();
        $Plugin->addImagesContainer(Fenix::getRequest()->getPost($Block->name));
        $Plugin->setAttributes($Block->toArray());

        return $Plugin;
    }

    public function setAttributes($attributes)
    {
        $this->_attributes = $attributes;

        return $this;
    }

    public function getAttributes()
    {
        return $this->_attributes;
    }

    public function setFilesName($name)
    {
        $this->_filesName = $name;

        return $this;
    }

    public function setFileUploader($uri)
    {
        $this->_fileUploader = $uri;

        return $this;
    }

    public function addDropZone()
    {
        $this->setAttributeGroup('creator.form.gallery.dropzone');
        $this->setClass('drop-zone')
            ->setId(md5(microtime()));

        return $this;
    }

    public function addPreviewsContainer()
    {
        $this->setAttributeGroup('creator.form.gallery.imagescontainer');
        $this->setClass('previews-container')
            ->setId(md5(microtime()));

        return $this;
    }

    public function addImagesContainer($imageslist = null)
    {
        $this->setAttributeGroup('creator.form.gallery.images');
        $this->setClass('images-container')
            ->setId(md5(microtime()));

        //Fenix::dump($imageslist);
        if (is_array($imageslist) && isset($imageslist['old'])) {
            //Fenix::dump($imageslist);
            $result = array();
            foreach ($imageslist['old'] as $i => $_image) {
                $result[$i] = array('image' => $_image);

                if (isset($imageslist['delete'][$_image])) {
                    $result[$i]['delete'] = $imageslist['delete'][$_image];
                }

                if ( ! empty($imageslist['meta']) && $this->getMetaFields() && count($this->getMetaFields()) > 0) {
                    foreach ($this->getMetaFields() as $_field) {
                        $result[$i][$_field['name']] = $imageslist['meta'][$_image][$_field['name']];
                    }
                }
            }

            $imageslist = $result;
        }
        //Fenix::dump($imageslist);

        $this->_imagesList = $imageslist;

        return $this;
    }

    public function fetch()
    {
        if ( ! isset($this->_attr['creator.form.gallery.dropzone'])) {
            throw new Exception('Необходимо создать зону для сброса изображений');
        }

        if ( ! isset($this->_attr['creator.form.gallery.imagescontainer'])) {
            throw new Exception('Необходимо создать зону для отображения изображений');
        }

        if ( ! isset($this->_attr['creator.form.gallery.images'])) {
            throw new Exception('Необходимо создать зону для уже загруженных изображений');
        }

        Creator_UI::addHtmlField($this->_filesName, $this);

        $this->_containerId = $this->_attr['creator.form.gallery.container']['id'];
        $this->_dropzoneId = $this->_attr['creator.form.gallery.dropzone']['id'];
        $this->_previewsId = $this->_attr['creator.form.gallery.imagescontainer']['id'];
        $this->_imagesId = $this->_attr['creator.form.gallery.images']['id'];

        $result = $this->_getScripts();

        $result .= '<div' . $this->getAttributeSet('creator.form.gallery.container') . '>';

        $result .= '<div class="alert" id="' . $this->_containerId . '_alert"><b>*ВНИМАНИЕ:</b> Размер файла не должен превышать 2 МБ. В названии файла разрешены только латинские буквы, цифры, символы тире (-) и нижние подчеркивание (_).<br>Не загружайте много изображений за один раз, рекомендуется не более 5 - 10 изображений за раз.</div>';

        $result .= '<br><br><div' . $this->getAttributeSet('creator.form.gallery.dropzone') . '><span class="message">Переместите файлы сюда</span></div>';
        //$result.= '<div><input type="file" multiple="multiple" id="upload_button" /></div>';

        $result .= '<div>
                            <label style="text-align: center" for="gallery_uploader">
                                <b>ИЛИ</b><br>
			                    Выберите изображения на компьютере
                            </label>
							<input type="file" id="gallery_uploader" name="' . $this->_filesName . '[new]" multiple>
						</div>';
        $result .= '';

        $result .= '<div' . $this->getAttributeSet('creator.form.gallery.imagescontainer') . '></div>';


        $result .= '<br><br><div class="well" id="' . $this->_containerId . '_meta">Нажмите на изображение, чтобы заполнить дополнительные данные.</div>';

        $result .= '<ul' . $this->getAttributeSet('creator.form.gallery.images') . '>';

        // Новые изображения
        if ($this->getRequest()->getPost($this->_filesName) != null) {
            $gallery = $this->getRequest()->getPost($this->_filesName);

            if (isset($gallery['new']) && isset($gallery['new'][0])) {
                foreach ($gallery['new'] AS $image) {
                    if (file_exists(TMP_DIR_ABSOLUTE . 'uploads/' . $image) && $image != null) {
                        $result .= '<li style="cursor:pointer;">';
                        $result .= '<img src="' . TMP_DIR_URL . 'uploads/' . $image . '" alt="' . $image . '" />';
                        $result .= '<input type="hidden" name="' . $this->_filesName . '[new][]" value="' . $image . '" />';

                        if ($this->getMetaFields() && count($this->getMetaFields()) > 0) {
                            foreach ($this->getMetaFields() as $_field) {
                                $result .= '<input class="img-meta img-meta-' . $_field['name'] . '" data-name="' . $_field['name'] . '" type="hidden" name="' . $this->_filesName . '[meta][' . $image . '][' . $_field['name'] . ']" value="" />';
                            }
                        }

                        if (isset($gallery['delete'][$image]) && $gallery['delete'][$image] == '1') {
                            $checked_attr = ' checked="checked"';
                        } else {
                            $checked_attr = '';
                        }
                        $result .= '<label title="' . Fenix::lang('Выберите, чтобы удалить изображение') . '"><input type="checkbox" name="' . $this->_filesName . '[delete][' . $image . ']" value="1" ' . $checked_attr . ' /><span class="lbl"></span><span class="delete-label">' . Fenix::lang('удалить') . '</span></label>';

                        $result .= '</li>';
                    }
                }
            }
        }

        // Ранее загруженные изображения
        if ($this->_imagesList != null) {
            $images = array();

            if (is_array($this->_imagesList)) {
                $images = $this->_imagesList;
            } else {
                $images = unserialize($this->_imagesList);
            }

            foreach ($images AS $i => $image) {
                if (isset($image['image']) && $image['image'] != null && file_exists(HOME_DIR_ABSOLUTE . $image['image'])) {
                    $result .= '<li style="cursor:pointer;">';
                    $result .= '<img src="' . Fenix_Image::adapt($image['image'], 150,
                            150) . '" alt="' . $image['image'] . '" />';
                    $result .= '<input type="hidden" name="' . $this->_filesName . '[old][]" value="' . $image['image'] . '" />';

                    if ($this->getMetaFields() && count($this->getMetaFields()) > 0) {
                        foreach ($this->getMetaFields() as $_field) {
                            $_field['html'] = (isset($_field['html']) && ! empty($_field['html'])) ? $_field['html'] : '';
                            switch ($_field['html']) {
                                case 'text':
                                case 'input':
                                case 'textarea':
                                default:
                                    $result .= '<textarea class="img-meta img-meta-' . $_field['name'] . '" data-name="' . $_field['name'] . '" style="display:none;" name="' . $this->_filesName . '[meta][' . $image['image'] . '][' . $_field['name'] . ']">' . (isset($image[$_field['name']]) ? $image[$_field['name']] : null) . '</textarea>';
                                    break;
                            }

                        }
                    }

                    if (isset($image['delete']) && $image['delete'] == '1') {
                        $checked_attr = ' checked="checked"';
                    } else {
                        $checked_attr = '';
                    }

                    $result .= '<label title="' . Fenix::lang('Выберите, чтобы удалить изображение') . '"><input type="checkbox" name="' . $this->_filesName . '[delete][' . $image['image'] . ']" value="1" ' . $checked_attr . '/><span class="lbl"></span><span class="delete-label">' . Fenix::lang('удалить') . '</span></label>';
                    $result .= '</li>';
                }
            }
        }

        $result .= '</ul><div class="clearfix"></div></div>';

        return $result;
    }

    private function _getScripts()
    {
        ob_start() ?>

        <script>
            function __imgMetaTemplate(containerCount, fields) {
                <?php
                $unique_id = rand(1000, 9999);
                $imgMetaTemplate = '';
                if ($this->getMetaFields() && count($this->getMetaFields()) > 0) {
                    foreach ($this->getMetaFields() as $_field) {
                        $_field['html'] = (isset($_field['html']) && ! empty($_field['html'])) ? $_field['html'] : '';
                        $editor_class = (isset($_field['editor']) && ! empty($_field['editor']) && $_field['editor'] == '1') ? 'view-editor' : '';
                        switch ($_field['html']) {
                            case 'textarea':
                            case 'text':
                            case 'input':
                            default:
                                $imgMetaTemplate .= '	<div class="control-group">
														<div class="control-label">' . $_field['title'] . '</div>
														<div class="controls">
															<textarea class="form-control gallery-img-meta-' . $unique_id . ' ' . $editor_class . '" id="gallery-img-meta-' . $unique_id . '-' . $_field['name'] . '" type="text" data-name="' . $_field['name'] . '" name="img_' . $_field['name'] . '">\'+fields["' . $_field['name'] . '"]+\'</textarea>
														</div>
													</div>';
                                break;
                        }
                    }
                }
                $imgMetaTemplate .= "	<div class=\"control-group\">
												<div class=\"control-label\"></div>
												<div class=\"controls\">
													<button onclick=\"return __imgMetaSave(' + containerCount + ', $(\'.gallery-img-meta-$unique_id\'))\" class=\"btn btn-success\" name=\"save\" value=\"Сохранить\" type=\"button\">Сохранить</button>
												</div>
											</div>";

                $imgMetaTemplate = str_replace(array("\t", "\r", "\n"), '', $imgMetaTemplate);
                ?>

                return '<?php echo $imgMetaTemplate; ?>';
            }

            function __imgMetaSave(containerCount, elements) {

                $('#<?php echo $this->_containerId ?>_meta')
                    .html('Данные сохранены. Нажмите на изображение, чтобы заполнить дополнительные данные');

                $("#<?php echo $this->_imagesId ?>").find('li').each(function (i) {
                    var $this = $(this);
                    if (i == containerCount) {
                        $(elements).each(function () {
                            console.log($this.find('.img-meta-' + $(this).attr('data-name')), $(this).val());
                            $this.find('.img-meta-' + $(this).attr('data-name')).val($(this).val());
                        });
                        return false;
                    }
                });

                return false;
            }

            $(function () {
                //templateMY
                $("#<?php echo $this->_imagesId ?>").sortable({
                    placeholder: "placeholder"
                });
                //$("#<?php echo $this->_imagesId ?>").disableSelection();

                $("#<?php echo $this->_imagesId ?>").find('li').each(function (i) {
                    $(this).click(function () {
                        var fields = [];
                        $(this).find('.img-meta').each(function () {
                            fields[$(this).attr('data-name')] = $(this).val();
                        });

                        $('#<?php echo $this->_containerId ?>_meta').html(__imgMetaTemplate(i, fields));
                        viewEditor();
                        //return false;
                    })
                });

                $("#<?php echo $this->_imagesId ?>").find('li label').click(function (e) {
                    e.stopPropagation();
                });

                $('#gallery_uploader').ace_file_input({
                    no_file: 'Ничего не выбрано ...',
                    btn_choose: 'Выберите',
                    btn_change: 'Изменить',
                    droppable: true,
                    onchange: null,
                    thumbnail: false
                });

                $('#gallery_uploader').change(function () {
                    var $input = $("#gallery_uploader");
                    var $files = $input.prop('files');


                    $($files).each(function (a) {
                        var formData = new FormData;
                        formData.append('gallery', $files[a]);
                        var $photo = formData.get('gallery');
                        if (!$photo.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }

                        createImage($photo);
                        $.ajax({
                            url: '<?php echo $this->_fileUploader ?>',
                            data: formData,
                            dataType: "json",
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                $("input[value='" + $photo['name'] + "'").val(data['filename']);
                                $.data($photo).addClass('done');
                            }
                        })

                    });
                });

                var dropbox = $('#<?php echo $this->_dropzoneId ?>'),
                    message = $('.message', dropbox),
                    previews = $('#<?php echo $this->_previewsId ?>');

                dropbox.filedrop({
                    fallback_id: 'upload_button',
                    paramname: '<?php echo $this->_filesName ?>',
                    maxfiles: 50,
                    maxfilesize: 20,
                    url: '<?php echo $this->_fileUploader ?>',

                    dragEnter: function (e) {
                        dropbox.addClass('enter')
                    },
                    //dragOver : function(e) {
                    //  dropbox.removeClass('enter')
                    // } ,
                    dragLeave: function (e) {
                        dropbox.removeClass('enter')
                    },
                    drop: function (e) {
                        dropbox.removeClass('enter')
                    },

                    uploadFinished: function (i, file, response/*json*/) {
                        $.data(file).addClass('done');
                    },

                    error: function (err, file) {
                        switch (err) {
                            case 'BrowserNotSupported':
                                showMessage('Your browser does not support HTML5 file uploads!');
                                break;
                            case 'TooManyFiles':
                                alert('Too many files! Please select 5 at most! (configurable)');
                                break;
                            case 'FileTooLarge':
                                alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
                                break;
                            default:
                                break;
                        }
                    },

                    beforeEach: function (file) {
                        if (!file.type.match(/^image\//)) {
                            alert('Only images are allowed!');
                            return false;
                        }
                    },

                    uploadStarted: function (i, file, len) {
                        createImage(file);
                    },

                    progressUpdated: function (i, file, progress) {
                        $.data(file).find('.progress').width(progress);
                    }

                });

                var template = '<div class="preview">' +
                    '<span class="imageHolder">' +
                    '<img />' +
                    '<span class="uploaded"></span>' +
                    '</span>' +
                    '<div class="progressHolder">' +
                    '<div class="progress"></div>' +
                    '</div>' +
                    '</div>';


                function createImage(file) {

                    var preview = $(template),
                        image = $('img', preview),
                        input = $('<input />');

                    input.attr('type', 'hidden')
                        .attr('name', '<?php echo $this->_filesName ?>[new][]')
                        .attr('value', file.name);

                    var reader = new FileReader();

                    image.width = 100;
                    image.height = 100;

                    reader.onload = function (e) {
                        image.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(file);

                    input.appendTo(previews)
                    preview.appendTo(previews);

                    $.data(file, preview);
                }

                function showMessage(msg) {
                    message.html(msg);
                }

                viewEditor();

                function viewEditor() {
                    /*CKEDITOR.replace('.view-editor', {
                     height : '300px'
                     });*/
                }
            });
        </script>

        <?php return ob_get_clean();
    }

    public function getMetaFields()
    {
        return (isset($this->_attributes['metafields']['field']))
            ? $this->_attributes['metafields']['field']
            // по умолчанию
            : array(
                array(
                    'title' => Fenix::lang('Название изображения'),
                    'name'  => 'meta_title',
                ),
                array(
                    'title' => Fenix::lang('Альтернативный текст'),
                    'name'  => 'meta_alt',
                ),
            );
    }
}