<?php
class Creator_UI extends Creator_Abstract
{
    const CLEAR_BOTH = '<div style="clear:both"></div>';
    
    static private $_htmlFields = array();
    static protected $_nonValid  = array();

    public function __construct() {
        parent::__construct();
        
        $view = $this->_actionController->view;
        
        // Путь к скриптам вида
        if ($this->getRequest()->getAccessLevel() == 'backend') {
            $view ->addScriptPath(
                THEMES_DIR_ABSOLUTE . "backend" . DS . "design" . DS
            );
        }
        else {
            $view ->addScriptPath(
                THEMES_DIR_ABSOLUTE . Fenix::getTheme()->name . DS . "design" . DS
            );
        }
    }
    
    public static function addHtmlField($id, $html)
    {
        self::$_htmlFields[$id] = $html;
        return;
    }
    
    public static function getHtmlField()
    {
        return new ArrayObject(self::$_htmlFields, ArrayObject::ARRAY_AS_PROPS);
    }
    
    public static function addNonValidField($field, $errors)
    {
        self::$_nonValid[] = array(
            'field'  => $field,
            'errors' => $errors
        );
        return;
    }
    
    public static function getNonValidFields()
    {
        return self::$_nonValid;
    }

    /**
     * Загрузка плагинов
     *
     * @param $pluginName
     * @return Fenix_Creator_Abstract
     */
    public function loadPlugin($pluginName)
    {
        try {
            Zend_Loader::loadClass('Creator_' . $pluginName);
        
            $pluginClass = 'Creator_' . $pluginName;
            return new $pluginClass;
        }
        catch (Exception $e) {
            new Fenix_Exception($e);
        }
    }
    
    public function oneColumn($content = array())
    {
        $this->_actionController
             ->view
             ->assign('content', $this->renderContent($content));
        
        $this->render('layout/page/one-column.phtml');
    }
    
    public function twoColumns($header = array(), $left = array(), $right = array())
    {
        $this->_actionController
             ->view
             ->assign('header', $this->renderContent($header))
             ->assign('left',   $this->renderContent($left))
             ->assign('right', $this->renderContent($right));
        
        $this->render('layout/page/two-columns.phtml');
    }
    
    public function twoColumnsLeft($header = array(), $left = array(), $center = array())
    {
        $this->_actionController
             ->view
             ->assign('header', $this->renderContent($header))
             ->assign('left',   $this->renderContent($left))
             ->assign('center', $this->renderContent($center));
        
        $this->render('layout/page/two-columns-left.phtml');
    }
    
    public function twoColumnsRight($header = array(), $center = array(), $right = array())
    {
        $this->_actionController
             ->view
             ->assign('header', $this->renderContent($header))
             ->assign('right',  $this->renderContent($right))
             ->assign('center', $this->renderContent($center));
        
        $this->render('layout/page/two-columns-right.phtml');
    }
    
    public function assign($key, $value = null)
    {
        $this->_actionController
             ->view
             ->assign($key, $value);
        
        return $this;
    }
    
    public function setLayout($layout = 'layout/layout')
    {
        // Объект вида
        $view = $this->_actionController->view;
        
        // Заголовок страницы
        if ($this->getRequest()->getAccessLevel() == 'backend') {
            $view->headTitle(Fenix::getSystemInfo()->getFullname());
        }
        
        // Включение основного каркаса страницы
        $this ->_actionController
              ->getHelper('layout')
              ->setLayout($layout);
        
        return $this;
    }
    public function render($script)
    {
        $this->_actionController
             ->renderScript($script);
    }
    public function fetch(){}
}