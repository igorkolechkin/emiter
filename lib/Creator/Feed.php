<?php
class Creator_Feed extends Creator_Abstract
{
    private $_item = array();

    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.feed')
             ->setId('guirow_' . Fenix::getRand(6))
             ->setClass('control-group');
    }

    public function addItems(array $items)
    {
        $this->_item = array_merge($this->_item, $items);
        return $this;
    }

    public function addItem($date, array $item)
    {
        $this->_item[$date][] = $item;
        return $this;
    }

    public function getItems()
    {
        return $this->_item;
    }

    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/feed.phtml');
    }
}