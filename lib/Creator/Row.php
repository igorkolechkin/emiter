<?php
class Creator_Row extends Creator_Abstract
{
    private $_label   = null;
    private $_content = null;
    private $_details = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.row')
             ->setId('guirow_' . Fenix::getRand(6))
             ->setClass('control-group');
        
        $this->setImage();
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты Строки
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }
        
        // Название строки
        if (isset($Block->label)) {
            if (isset($Block->label->value)) {
                $Plugin->setLabel($Block->label->value);
            }
            else {
                $Plugin->setLabel($Block->label);
            }
            
            if (isset($Block->label->attributes)) {
                $Plugin->setAttributes($Block->label->attributes->toArray());
            }
        }
        
        // Описание строки
        if (isset($Block->details)) {
            if (isset($Block->details->value)) {
                $Plugin->setDetails($Block->details->value);
            }
            else {
                $Plugin->setDetails($Block->details);
            }
            
            if (isset($Block->details->attributes)) {
                $Plugin->setAttributes($Block->details->attributes->toArray());
            }
        }
        
        // Контент
        if (isset($Block->content)) {
            
            $Attributes = false;
            if (isset($Block->content->attributes)) {
                $Attributes = $Block->content->attributes->toArray();
                unset($Block->content->attributes);
            }
            
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }

        return $Plugin;
    }

    public function setLabel($text)
    {
        $this->_label = $text;
        $this->setAttributeGroup('creator.row.label')
             ->setClass('control-label')
             ->setFor('l__');
        return $this;
    }

    public function getLabel()
    {
        return $this->_label;
    }
    
    public function setDetails($text)
    {
        $this->_details = $text;
        $this->setAttributeGroup('creator.row.details')
             ->setClass('gui-row-details')
             ->setTitle($text);
        return $this;
    }

    public function getDetails()
    {
        return $this->_details;
    }    
    
    public function setContent($content)
    {
        $this->_content = $content;
        $this->setAttributeGroup('creator.row.content')
            ->setClass('controls');
        
        return $this;
    }

    public function getContent()
    {
        return $this->_content;
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/row.phtml');
    }
}