<?php
class Creator_Block extends Creator_Abstract
{
    private $_title   = null;
    private $_content = null;

    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.block')
             ->setClass('widget-box');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты филдсета
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }

        // Заголовок
        if (isset($Block->title)) {
            if (isset($Block->title->value)) {
                $Plugin->setTitle($Block->title->value);
            }
            else {
                $Plugin->setTitle($Block->title);
            }

            if (isset($Block->title->attributes)) {
                $Plugin->setAttributes($Block->title->attributes->toArray());
            }
        }

        // Контент
        if (isset($Block->content)) {
            $Attributes = false;
            if (isset($Block->content->attributes)) {
                $Attributes = $Block->content->attributes->toArray();
                unset($Block->content->attributes);
            }
            $Plugin->setContent($Generator->renderBlock($Block->content, true));
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->content);
        }

        return $Plugin;
    }

    public function setTitle($title)
    {
        $this->_title = $title;
        $this->setAttributeGroup('creator.block.title')
             ->setClass('widget-header')
             ->appendClass('widget-header-flat');

        return $this;
    }
    public function getTitle()
    {
        return $this->_title;
    }

    public function setContent($content)
    {
        $this->_content = $content;
        $this->setAttributeGroup('creator.block.content')
             ->setClass('widget-body');

        return $this;
    }
    public function getContent()
    {
        return $this->_content;
    }

    public function fetch()
    {
        $this->_view->assign('Error', null);
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/block.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}