<?php
class Creator_Accordion extends Creator_Abstract
{
    private $_items     = array();
    private $_UIoptions = array();
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.accordion')
             ->setId('guiaccordion_' . Fenix::getRand(6))
             ->setClass('accordion');
    }
    
    public function setUIOptions(array $options)
    {
        $this->_UIoptions = array_merge($this->_UIoptions, $options);
        return $this;
    }
    
    public function getOptionsJson()
    {
        return Zend_Json::encode($this->_UIoptions);
    }
    
    public function addItem($label, $content)
    {
        $this->_items[] = array(
            'label'   => $label,
            'content' => $content
        );
        
        return $this;
    }    
    
    public function getItems()
    {
        return $this->_items;
    }
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/accordion.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}