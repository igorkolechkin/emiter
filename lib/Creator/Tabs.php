<?php
class Creator_Tabs extends Creator_Abstract
{
    private $_id        = null;
    private $_tab       = array();
    private $_UIoptions = array();
    private $_navPos    = 'top';

    public function __construct()
    {
        parent::__construct();

        $this->setId('guitabs_' . Fenix::getRand(6));

        $this->setAttributeGroup('creator.tabs');
        $this->setClass('tabbable');
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }
        
        $Tabs = array();
        
        if (isset($Block->addTab->{0})) {
            $Tabs = $Block->addTab;
        }
        else {
            $Tabs[] = $Block->addTab;
        }
        
        foreach ($Tabs AS $_tab) {
            $Plugin->addTab($_tab->id, $_tab->label, $Generator->renderBlock($_tab->content));
        }
        
        return $Plugin;
    }

    /**
     * @param string $navPos
     * @return Creator_Tabs
     */
    public function setNavPosition($navPos)
    {
        switch ($navPos)
        {
            case 'top':
            default:

            break;

            case 'bottom':
                $this->appendClass('tabs-below');
            break;

            case 'right':
                $this->appendClass('tabs-right');
            break;

            case 'left':
                $this->appendClass('tabs-left');
            break;
        }

        $this->_navPos = $navPos;
        return $this;
    }

    /**
     * @return string
     */
    public function getNavPosition()
    {
        return $this->_navPos;
    }

    /**
     *
     * @param int $id
     * @return Creator_Tabs
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    public function setUIOptions(array $options)
    {
        $this->_UIoptions = array_merge($this->_UIoptions, $options);
        return $this;
    }
    
    public function getOptionsJson()
    {
        return Zend_Json::encode($this->_UIoptions);
    }

    public function addTab($id, $label, $content)
    {
        $this->_tab[$id] = array(
            'id'      => $id,
            'label'   => $label,
            'content' => $content
        );
        
        return $this;
    }    
    
    public function getTabs()
    {
        return $this->_tab;
    }
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/tabs.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}