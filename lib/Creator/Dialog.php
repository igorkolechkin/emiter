<?php
class Creator_Dialog extends Creator_Abstract
{
    private $_title     = null;
    private $_content   = null;
    private $_UIoptions = array(
        'autoOpen'  => false,
        'modal'     => true
    );
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.dialog')
             ->setId('guidialog_' . Fenix::getRand(6));
    }
    
    public function toButton()
    {
        return '$(\'#' . $this->getId() . '\').modal(\'toggle\');';
    }
    
    public function setUIOptions(array $options)
    {
        $this->_UIoptions = array_merge($this->_UIoptions, $options);
        return $this;
    }
    
    public function getOptionsJson()
    {
        return Zend_Json::encode($this->_UIoptions);
    }
    
    public function setTitle($text)
    {
        $this->_title = $text;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->_title;
    }
    
    public function setContent($text)
    {
        $this->_content = $text;
        return $this;
    }
    
    public function getContent()
    {
        return $this->_content;
    }

    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/dialog.phtml');
    }
}