<?php
class Creator_Sticky extends Creator_Abstract
{
    private $_content = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.sticky')
             ->setId('sticky_' . Fenix::getRand(6));
    }
    
    public function setContent($content)
    {
        $this->_content[] = $content;
        return $this;
    }    
    
    public function getContent()
    {
        return $this->renderContent($this->_content);
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/sticky.phtml');
    }
}