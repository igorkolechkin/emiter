<?php
class Creator_Events extends Creator_Abstract
{
    const TYPE_INFO    = 'alert-info';
    const TYPE_OK      = 'alert-success';
    const TYPE_WARNING = 'alert-warning';
    const TYPE_ERROR   = 'alert-danger';
    
    private $_message  = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.events')
             ->setId('guievents_' . Fenix::getRand(6))
             ->setClass('alert')
             ->appendClass('fade in');
    }
    
    public function setMessage($text)
    {
        $this->_message = $text;
        return $this;
    }
    
    public function getMessage()
    {
        return $this->_message;
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/events.phtml');
    }
}