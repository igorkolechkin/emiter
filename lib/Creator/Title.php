<?php
class Creator_Title extends Creator_Abstract
{
    private $_title     = null;
    private $_details   = null;
    private $_buttonset = null;
    private $_image     = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.title')
             ->setId('guititle_' . Fenix::getRand(6))
             ->setClass('gui-title');
        
        $this->setImage();
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты Строки
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }
        
        // Заголовок
        if (isset($Block->setTitle)) {
            if (isset($Block->setTitle->value)) {
                $Plugin->setTitle($this->getSmarty()->fetchString($Block->setTitle->value));
            }
            else {
                $Plugin->setTitle($this->getSmarty()->fetchString($Block->setTitle));
            }
            
            if (isset($Block->setTitle->attributes)) {
                $Plugin->setAttributes($Block->setTitle->attributes->toArray());
            }
        }
        
        // Изображение
        if (isset($Block->setImage)) {
            
            if (isset($Block->setImage->src)) {
                $Plugin->setImage(Fenix::getAppEtcUrl($this->getSmarty()->fetchString($Block->setImage->src), $this->getSmarty()->fetchString($Block->setImage->module)));
            }
            
            if (isset($Block->setImage->attributes)) {
                $Plugin->setAttributes($Block->setImage->attributes->toArray());
            }
        }
        
        // Кнопули
        if (isset($Block->setButtonset->content)) {
            $Attributes = false;
            if (isset($Block->setButtonset->attributes)) {
                $Attributes = $Block->buttonset->attributes->toArray();
                unset($Block->setButtonset->attributes);
            }
            
            $Plugin->setButtonset(
                $this->loadPlugin('Buttonset')
                     ->setContent($Generator->renderBlock($Block->setButtonset->content, true))
                     ->fetch()
            );
            if ($Attributes !== false) {
                $Plugin->setAttributes($Attributes);
            }
            unset($Block->buttonset);
        }
        
        return $Plugin;
    }
    
    public function setTitle($title)
    {
        $this->_title = $title;
        $this->setAttributeGroup('creator.title.title')
             ->setClass('gui-title-text');
        return $this;
    }    
    public function getTitle()
    {
        return $this->_title;
    }


    public function setDetails($details)
    {
        $this->_details = $details;
        return $this;
    }

    public function getDetails()
    {
        return $this->_details;
    }

    public function setButtonset($buttonset)
    {
        $this->_buttonset = $buttonset;
        $this->setAttributeGroup('creator.title.buttonset')
             ->setClass('gui-title-buttonset')
             ->setId('guititlebuttonset_' . Fenix::getRand(6));
        return $this;
    }

    public function getButtonset()
    {
        return $this->_buttonset;
    }    

    public function setImage($url = null)
    {
        $this->_image = $url;
        $this->setAttributeGroup('creator.title.image')
             ->setClass('gui-title-image');
        
        return $this;
    }

    public function getImage()
    {
        return $this->_image;
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/title.phtml');
    }

    public function __toString()
    {
        return $this->fetch();
    }
}