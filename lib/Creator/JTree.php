<?php
class Creator_JTree extends Creator_Abstract
{
    private $_id          = null,
            $_treePlugins = array(
                'themes',
                'cookies'
            ),
            $_table       = null,
            $_actionUrl   = null,
            $_initOpen    = 0,
            $_fields      = array(
                'id',
                'parent',
                'left',
                'right',
                'level',
                'position',
                'title_russian'
            ),
            $_options     = array();

    public function __construct()
    {
        parent::__construct();

        $this->_actionUrl = $this->getRequest()->getRequestUri();
    }

    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    public function setInitOpen($id)
    {
        $this->_initOpen = $id;
        return $this;
    }

    public function getInitOpen()
    {
        $jsTree = new Creator_JTree_Actions();
        $jsTree ->setTree($this);
        $initOpen = $jsTree->getinitOpen($this->_initOpen);
        return $initOpen;

    }

    public function setUrl($url)
    {
        $this->_options['url'] = $url;
        return $this;
    }

    public function getUrl()
    {
        return isset($this->_options['url']) ? $this->_options['url'] : null;
    }

    public function setEditUrl($url)
    {
        $this->_options['editurl'] = $url;
        return $this;
    }

    public function getEditUrl()
    {
        return isset($this->_options['editurl']) ? $this->_options['editurl'] : null;
    }

    public function setRootUrl($url)
    {
        $this->_options['rooturl'] = $url;
        return $this;
    }

    public function getRootUrl()
    {
        return isset($this->_options['rooturl']) ? $this->_options['rooturl'] : null;
    }

    public function setPlugins(array $plugins)
    {
        $this->_treePlugins = $plugins;
        return $this;
    }

    public function getPlugins()
    {
        return Zend_Json::encode($this->_treePlugins);
    }

    public function setTable($table)
    {
        $this->_table = $table;
        return $this;
    }

    public function getTable()
    {
        return $this->_table;
    }

    public function getFields()
    {
        return $this->_fields;
    }

    private function _checkInitialData()
    {
        if ($this->_id == null)
            throw new Exception('Вы должны указать идентификатор дерева');

        if ($this->_actionUrl == null)
            throw new Exception('Вы должны указать URL для обработки действий');

        if ($this->_table == null)
            throw new Exception('Вы должны указать имя таблицы');

        if (sizeof($this->_treePlugins) == 0)
            throw new Exception('Вы должны указать список плагинов');
    }

    public function fetch()
    {
        $this->_checkInitialData();
        $this->_executeActions();

        $result = $this->_scripts();
        $result.= '<div class="jstree-container" id="' . $this->_id . '"></div>';

        return $result;
    }

    private function _executeActions()
    {
        $jsTree = new Creator_JTree_Actions();
        $jsTree ->setTree($this);

        if (isset($_REQUEST["operation"]) && $_REQUEST["operation"] && strpos($_REQUEST["operation"], "_") !== 0 && method_exists($jsTree, $_REQUEST["operation"]))
        {
            header("HTTP/1.0 200 OK");
            header('Content-type: application/json; charset=utf-8');
            header("Cache-Control: no-cache, must-revalidate");
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Pragma: no-cache");
            echo $jsTree->{$_REQUEST["operation"]}($_REQUEST);
            die();
        }
    }

    private function _scripts()
    {
        ob_start() ?>
        <script type="text/javascript">
            $(function () {
            	var tree = $("#<?php  echo $this->_id ?>").jstree({
                    "plugins" : <?php echo $this->getPlugins() ?>,

                    "themes" : {
                        "theme" : "fenix",
                        "dots"  : true,
                        "icons" : true
                    },

                    "core" : {
                        "initially_open" : <?php echo $this->getInitOpen() ?>
                    },
                    "json_data" : {
        	            "ajax" : {
        	                "url" : "<?php  echo $this->_actionUrl ?>",
        	                "data" : function (n) {
        	                    return {
        	                        'operation' : "get_children",
        	                        'id' : n.attr ? n.attr("id").replace("node_","") : '0',
                                    'url' : '<?php echo $this->getUrl() ?>',
                                    'editurl' : '<?php echo $this->getEditUrl() ?>',
                                    'rooturl' : '<?php echo $this->getRootUrl() ?>'
        	                    };
        	                }
        	            }
        	        }
                });
                tree.bind("select_node.jstree", function (event, data) {

                    if (data.rslt.obj.data('url') != 0 && data.rslt.obj.data('url') != "#" && data.rslt.obj.data('url') != 'undefined')
                        self.location.href=data.rslt.obj.data('url');

                    return false;
                });

                tree.bind("dblclick.jstree", function (event) {
                    var node = $(event.target).closest("li");
                    if (node.data('editurl') != 0 && node.data('editurl') != "#" && node.data('editurl') != 'undefined')
                        self.location.href=node.data('editurl');

                    return false;
                });
            });
        </script>
        <?php return ob_get_clean();
    }
}