<?php
abstract class Creator_Abstract
{
    protected $_actionController,
              $_view;

    protected $_attr      = array(),
              $_attrGroup = null;

    protected $_validator  = array();

    protected $_splitByLang= false;

    public function __construct()
    {
        $this->_actionController = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->getActionController();
        $this->_view = $this->_actionController->view;
    }

    public function setSplitByLang()
    {
        $this->_splitByLang = true;
        return $this;
    }
    public function getSplitByLang()
    {
        return $this->_splitByLang;
    }

    public function getCache()
    {
        return new Zend_Session_Namespace('Creator_Cache');
    }

    /**
     * Загрузка плагинов
     *
     * @param $pluginName
     * @throws Fenix_Exceptio
     * @return Fenix_Creator_Abstract
     */
    public function loadPlugin($pluginName)
    {
        try {
            Zend_Loader::loadClass('Creator_' . $pluginName);

            $pluginClass = 'Creator_' . $pluginName;
            return new $pluginClass;
        }
        catch (Exception $e) {
            new Fenix_Exception($e);
        }
    }

    public function getLang()
    {
        return Fenix_Language::getInstance();
    }

    public function getSmarty()
    {
        return Fenix_Smarty::getInstance();
    }

    public function getView()
    {
        return $this->_view;
    }

    public function getRequest()
    {
        return $this->_actionController->getRequest();
    }

    public function setAttributeGroup($group)
    {
        $this->_attrGroup = $group;
        return $this;
    }

    public function getAttributeGroup()
    {
        return $this->_attrGroup;
    }

    public function getAttributes($group)
    {
        if (array_key_exists($group, $this->_attr))
            return (object) $this->_attr[$group];
        return;
    }

    public function getAttributeSet($group)
    {
        $result         = '';
        $attributeArray = null;

        if (is_array($group)) {
            $attributeArray = $group;
        }
        else {
            if (isset($this->_attr[$group])) {
                $attributeArray = $this->_attr[$group];

                if (is_array($attributeArray) && sizeof($attributeArray) > 0) {
                    foreach ($attributeArray AS $attr => $value) {
                        $result.= ' ' . $attr . '="' . $value . '"';
                    }
                }
            }
        }

        return $result;
    }

    public function setValidator($validator)
    {
        if (is_callable($validator)) {
            $this->_validator[] = $validator();
        }
        else {
            $this->_validator[] = $validator;
        }

        return $this;
    }

    protected function _validateField()
    {
        if ($this->getRequest()->isPost() || $this->getRequest()->isXmlHttpRequest() && sizeof($this->_validator) > 0) {

            if ($this->fieldNameIsArray($this->getName())) {
                //Если имя в формате массива (system[title])
                $arrayName = $this->getNameArrayName($this->getName());
                $arrayKey  = $this->getNameArrayKey($this->getName());
                $post  = $this->getRequest()->getPost($arrayName);
                $Value = $post[$arrayKey];
            } else {
                //Обычное поле
                $Value =  $this->getRequest()->getPost($this->getName());
            }

            $error = $this->_callValidators(
                $this,
                $this->getName(),
                $Value
            );

            return $error;
        }

        return array();
    }
    private function _callValidators($Field, $Name, $Value)
    {

        $errorArray  = array();
        foreach ($this->_validator AS $validator) {
            if ($validator instanceof Zend_Validate_Abstract) {

                if ($Field->getSplitByLang() === true) {
                    foreach ($this->getLang()->getLanguagesList() AS $_lang) {

                        if ($this->fieldNameIsArray($Field->getName())) {
                            //Если имя в формате массива (system[title])
                            $arrayName = $this->getNameArrayName($Field->getName());
                            $arrayKey  = $this->getNameArrayKey($Field->getName());
                            $post  = $this->getRequest()->getPost($arrayName);
                            $Value = $post[$arrayKey];
                        } else {
                            //Обычное поле
                            $Value = $this->getRequest()->getPost($Field->getName() . '_' . $_lang->name);
                        }

                        if (!$validator->isValid($Value)) {
                            $errorArray[] = new Fenix_Object(array(
                                'data' => array(
                                    'validator' => $validator,
                                    'errors'    => $validator->getErrors(),
                                    'prefix'    => $_lang->label . ': ',
                                    'messages'  => (object) $validator->getMessages()
                                )
                            ));
                        }
                    }
                }
                else {
                    if (!$validator->isValid($Value)) {
                        $errorArray[] = new Fenix_Object(array(
                            'data' => array(
                                'validator' => $validator,
                                'errors'    => $validator->getErrors(),
                                'messages'  => (object) $validator->getMessages()
                            )
                        ));
                    }
                }
            }
        }

        if (sizeof($errorArray) > 0) {
            Creator_UI::addNonValidField($Field, $errorArray);
        }

        return $errorArray;
    }

    /**
     * Проверяет имя поля в формате массива или нет (system[title])
     * @param $filedName
     * @return bool
     */
    public function fieldNameIsArray($filedName){
        if (preg_match('/\A(.+)\[(.+)\]\Z/', $filedName)) {
            # Successful match
            return true;
        } else {
            # Match attempt failed
            return false;
        }
    }
    /**
     * Вовращает название массива поля (system[title])
     * @param $filedName
     * @return bool
     */
    public function getNameArrayName($filedName){
        if (preg_match('/(.+)\[(.+)\]/', $filedName, $regs)) {
            $result = $regs[1];

        } else {
            $result = "";
        }
        return $result;
    }
    /**
     * Вовращает название ключа поля (system[title])
     * @param $filedName
     * @return bool
     */
    public function getNameArrayKey($filedName){
        if (preg_match('/(.+)\[(.+)\]/', $filedName, $regs)) {
            $result = $regs[2];

        } else {
            $result = "";
        }
        return $result;
    }

    public function setAttributes($attributes, $group = null)
    {
        if ($group == null)
            $group = $this->_attrGroup;

        $this->_attr[$group] = array_merge($this->_attr[$group], $attributes);

        return $this;
    }

    public function __call($method, $params)
    {
        if (substr($method, 0, 3) == "set") {
            if (isset($params[0])) {
                $method = strtolower(substr($method, 3, strlen($method)));
                $this->_attr[$this->_attrGroup][$method] = $params[0];
            }

            return $this;
        }
        elseif (substr($method, 0, 3) == "get") {
            $method = strtolower(substr($method, 3, strlen($method)));
            return isset($this->_attr[(@$params[0] == null ? $this->_attrGroup : $params[0])][$method])
                        ? $this->_attr[(@$params[0] == null ? $this->_attrGroup : $params[0])][$method]
                        : null;
        }
        elseif (substr($method, 0, 6) == "append") {
            $method = strtolower(substr($method, 6, strlen($method)));

            if (isset($this->_attr[$this->_attrGroup][$method]))
                $this->_attr[$this->_attrGroup][$method] = $this->_attr[$this->_attrGroup][$method] . ' ' . $params[0];
            else
                $this->_attr[$this->_attrGroup][$method] = $params[0];

            return $this;
        }
        elseif (substr($method, 0, 7) == "prepend") {
            $method = strtolower(substr($method, 7, strlen($method)));

            if (isset($this->_attr[$this->_attrGroup][$method]))
                $this->_attr[$this->_attrGroup][$method] = $params[0] . ' ' . $this->_attr[$this->_attrGroup][$method];
            else
                $this->_attr[$this->_attrGroup][$method] = $params[0];

            return $this;
        }
        elseif (substr($method, 0, 5) == "unset")
        {
            $method = strtolower(substr($method, 5, strlen($method)));

            if (isset($this->_attr[$this->_attrGroup][$method]))
               unset($this->_attr[$this->_attrGroup][$method]);

            return $this;
        }
        else {
            return $this;
        }
    }
    /**
     * Рендеринг контента
     *
     * @param mixed $content Содержимое
     * @return string
     */
    public function renderContent($content)
    {
        $result = null;

        if (is_array($content)) {
            foreach ($content AS $_content) {
                $result.= $this->renderContent($_content);
            }
        }
        else {
            $result = $content;
        }

        return $result;
    }

    abstract public function fetch();

    public function toString()
    {
        return $this->fetch();
    }
    /*
    public function __toString()
    {
        return $this->toString();
    }*/
}