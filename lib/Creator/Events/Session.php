<?php
class Creator_Events_Session extends Creator_Abstract
{
    private $_type = null;
    private $_message = null;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function generator($Generator, $Plugin, $Block)
    {
        return $Plugin;
    }
          
    public function getType() {
        return $this->_type;
    }

    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    public function getMessage() {
        return $this->_message;
    }

    public function setMessage($message) {
        $this->_message = $message;
        return $this;
    }
    
    public function saveSession()
    {
        $level = Fenix::getRequest()->getAccessLevel();
        
        $this->getCache()->events = array(
            'session.' . $level => array(
                'type' => $this->_type,
                'message' => $this->_message
            )
        );
    }
    
    public function fetch()
    {
        $level = Fenix::getRequest()->getAccessLevel();
        
        $cache = $this->getCache()->events;
        
        if (array_key_exists('session.' . $level, (array) $cache)) {
            $type = null;
            if (array_key_exists('type', $cache['session.' . $level]))
                $type = $cache['session.' . $level]['type'];
            
            $message = null;
            if (array_key_exists('message', $cache['session.' . $level]))
                $message = $cache['session.' . $level]['message'];
            
            unset($this->getCache()->events['session.' . $level]);
            
            return $this->loadPlugin('Events')
                        ->appendClass($type)
                        ->setMessage($message)
                        ->fetch();
        }
    }
}