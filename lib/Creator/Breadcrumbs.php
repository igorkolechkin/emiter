<?php
class Creator_Breadcrumbs extends Creator_Abstract
{
    private $_crumb = null;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->setAttributeGroup('creator.breadcrumbs')
             ->setClass('breadcrumb');
        
        $this->setImage();
    }

    public function generator($Generator, $Plugin, $Block)
    {
        // Атрибуты Строки
        if (isset($Block->attributes)) {
            $Plugin->setAttributes($Block->attributes->toArray());
        }
        
        $crumbs = array();

        if (isset($Block->addCrumb->{0})) {
            $crumbs = $Block->addCrumb;
        }
        else {
            $crumbs[] = $Block->addCrumb;
        }
        
        foreach ($crumbs AS $_crumb) {
            $_options = array();
            if (isset($_crumb->url)) {
                $_options['url'] = $this->getSmarty()->fetchString($_crumb->url);
            }
            if (isset($_crumb->label)) {
                $_options['label'] = $this->getSmarty()->fetchString($_crumb->label);
            }
            
            $Plugin->addCrumb($_options);
        }
        
        return $Plugin;
    }
    
    public function count()
    {
        return sizeof($this->_crumb);
    }
    
    public function addCrumb($options)
    {
        $this->_crumb[] = $options;
        $this->setAttributeGroup('creator.breadcrumbs.crumb.' . sizeof($this->_crumb ));
        return $this;
    }    
    public function getCrumbs()
    {
        return $this->_crumb;
    }    
    
    public function fetch()
    {
        $this->_view
             ->assign('Plugin', $this);
        
        return $this->_view
                    ->render('creator/breadcrumbs.phtml');
    }
}