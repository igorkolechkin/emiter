<?php
class Creator_Form extends Creator_Abstract
{
    private $_content  = array(),
            $_compiled = null,
            $_ajax  = false;
    protected $_defaults = array();

    public $unique_id = '';
    public $containerClass = '';

    protected $_data = null;

    public function __construct()
    {
        parent::__construct();

        $this->setAttributeGroup('creator.form')
             ->setId('guiform_' . Fenix::getRand(6))
             ->setAction('')
             ->setMethod('post')
             ->setClass('form-horizontal')
             ->setRole('form')
             ->setEnctype('multipart/form-data');
    }
    /**
     * Загрузка плагинов
     *
     * @param $pluginName
     * @throws Fenix_Exceptio
     * @return Fenix_Creator_Abstract
     */
    public function loadPlugin($pluginName)
    {
        try {
            Zend_Loader::loadClass('Creator_' . $pluginName);

            $pluginClass = 'Creator_' . $pluginName;
            $plugin = new $pluginClass;

	        if(method_exists($plugin, 'setFormId')) {
		        $plugin->setFormId($this->getId());
	        }

            return $plugin;
        }
        catch (Exception $e) {
            new Fenix_Exception($e);
        }
    }

    public function getData($key)
    {
        if (array_key_exists($key, $this->_data))
            return $this->_data[$key];
        return;
    }

    public function setData($key, $value)
    {
        $this->_data[$key] = $value;
        return $this;
    }

    public function setDefaults($defaults)
    {
        if ($this->getRequest()->getPost('creatorModalLoad') == '1') {
            $this->_defaults = array_merge($this->_defaults, $defaults);

            if (is_array($defaults)) {
                foreach ($defaults AS $_key => $_value) {
                    $this->getRequest()->setPost($_key, (string) $_value);
                }
            }
        }
        else {
            if ($this->getRequest()->getPost('ajaxSend') != 1 && !$this->getRequest()->isPost()) {
                $this->_defaults = array_merge($this->_defaults, $defaults);

                if (is_array($defaults)) {
                    foreach ($defaults AS $_key => $_value) {
                        $this->getRequest()->setPost($_key, ($_value == null ? '' : $_value));
                    }
                }
            }
        }
        return $this;
    }

    public function getDefaults($param)
    {
        return $this->getRequest()->getPost($param);
    }

    public function setContent($content)
    {
        $this->_content[] = $content;
        $this->setAttributeGroup('creator.form.content.' . sizeof($this->_content));
        return $this;
    }

    public function getContent()
    {
        return $this->_content;
    }

    public function ok()
    {
        if ($this->getRequest()->isPost() || $this->getRequest()->isXmlHttpRequest()) {
            if (sizeof(Creator_UI::getNonValidFields()) > 0) {
                return false;
            }
            return true;
        }
        return false;
    }

    public function compile()
    {
        $this->_compiled = $this->_render();
        return $this;
    }

    public function _render()
    {
        if (isset($this->_attr['creator.form']['id'])) {
            $this->_view->containerId = $this->_attr['creator.form']['id'];
            unset($this->_attr['creator.form']['id']);
        }

        $this->_view
             ->assign('Form', $this);

        return $this->_view
                    ->render('creator/form.phtml');
    }

    public function fetch()
    {
        return $this->_compiled;
    }

    /**
     * Включение аякса
     *
     * @param bool $ajax
     * @return $this
     */
    public function enableAjax($ajax = true)
    {
        $this->_ajax = $ajax;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAjaxEnabled()
    {
        return $this->_ajax;
    }

}