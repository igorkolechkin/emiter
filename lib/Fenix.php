<?php

class Fenix
{
    const ALL              = null;
    const IS_ACTIVE        = '1';
    const IS_PUBLIC        = '1';
    const ON_MAIN          = '1';
    const NOT_MAIN         = '0';
    const DEFAULT_PER_PAGE = 12;

    public static $time          = 0;
    public static $objectCreated = 0;
    public static $objectLoaded  = 0;

    private $instances = array();

    /**
     * Генерация GUID
     *
     * @param null $namespace
     * @return string
     */
    public static function getGuid($namespace = null)
    {
        $uid = uniqid("", true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];

        $hash = strtoupper(hash('ripemd128', $uid . md5($data)));

        return substr($hash, 0, 8) . '-' . substr($hash, 8, 4) . '-' . substr($hash, 12, 4) . '-' . substr($hash, 16,
                4) . '-' . substr($hash, 20, 12);
    }

    /**
     * Данные браузера и ОС клиента
     *
     * @return Fenix_Browser
     */
    public static function getBrowser()
    {
        return new Fenix_Browser();
    }

    /**
     * Импорт дампа БД
     *
     * @param $file
     * @param string $delimiter
     * @return bool
     */
    public static function sqlDumpFileImport($file, $delimiter = ';')
    {
        set_time_limit(0);
        if (is_file($file) === true) {
            $file = fopen($file, 'r');
            if (is_resource($file) === true) {
                $query = array();
                while (feof($file) === false) {
                    $query[] = fgets($file);
                    if (preg_match('~' . preg_quote($delimiter, '~') . '\s*$~iS', end($query)) === 1) {
                        $query = trim(implode('', $query));
                        if (mysql_query($query) === false) {
                            //echo '<h3>ERROR: ' . $query . '</h3>' . "\n";
                        } else {
                            //echo '<h3>SUCCESS: ' . $query . '</h3>' . "\n";
                        }
                        while (ob_get_level() > 0) {
                            ob_end_flush();
                        }
                        flush();
                    }

                    if (is_string($query) === true) {
                        $query = array();
                    }
                }

                return fclose($file);
            }
        }

        return false;
    }

    /**
     * Перевод фразы на выбранный язык из CSV
     *
     * @return mixed
     */
    public static function lang()
    {
        $lang = func_get_args();

        //Сбор строк для перевода
        Fenix::getModel('languages/collect')->collect($lang[0]);

        if ( ! isset($lang[0])) {
            throw new Exception("Укажите на строку перевода");
        }

        $lang[0] = Zend_Registry::get('Zend_Translate')
            ->_($lang[0]);

        return call_user_func_array("sprintf", $lang);
    }

    /**
     * Выбранная тема сайта
     *
     * @return mixed
     */
    public static function getTheme()
    {
        return Zend_Registry::get('FenixEngine_Theme');
    }

    /**
     * Статический конфиг. Файл etc/config.xml
     *
     * @param $app
     * @throws Exception
     * @return mixed|Zend_Config_Xml
     */
    public static function getStaticConfig($app = null)
    {
        if (Zend_Registry::isRegistered('FenixEngineStaticConfig' . $app)) {
            return Zend_Registry::get('FenixEngineStaticConfig' . $app);
        }

        if ($app == null) {
            $xml = ETC_DIR_ABSOLUTE . 'config.xml';
        } else {
            $xmlLocal = APP_DIR_ABSOLUTE . 'Local/' . $app . '/' . APP_ETC_DIR . 'config.xml';
            $xmlFenix = APP_DIR_ABSOLUTE . 'Fenix/' . $app . '/' . APP_ETC_DIR . 'config.xml';

            if (file_exists($xmlLocal)) {
                $xml = $xmlLocal;
            } elseif (file_exists($xmlFenix)) {
                $xml = $xmlFenix;
            } else {
                throw new Exception("Статический файл конфигурации " . $app . " не найден");
            }
        }
        $config = new Zend_Config_Xml($xml, 'static');

        Zend_Registry::set('FenixEngineStaticConfig' . $app, $config);

        return $config;
    }

    /**
     * Создаем файл изображения
     *
     * @static
     * @param string $stream Фодержимое картинки
     * @param string $filename Название файла
     * @param string $extention Формат файла
     * @return string
     */
    public static function createImageFromStream($stream, $filename = null, $extention = 'jpg')
    {
        $path = TMP_DIR_ABSOLUTE . 'cache/images/';
        if ( ! is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $url = TMP_DIR_URL . 'cache/images/';
        $file = ($filename == null ? md5($stream) . '.' . $extention : $filename);

        if ( ! file_exists($path . $file)) {
            $_stream = fopen($path . $file, 'w');
            fputs($_stream, $stream);
            fclose($_stream);
        }

        return $url . $file;
    }

    /**
     * Создаем файл изображения по инфо
     *
     * @static
     * @param string $stream Фодержимое картинки
     * @param string $info
     * @return string
     */
    public static function createImageFromStreamInfo($stream, $info, $absolute = false)
    {
        if ($info === false) {
            return false;
        }

        if (is_array($info)) {
            $info = (object)$info;
        }

        $path = TMP_DIR_ABSOLUTE . 'cache/images/';
        if ( ! is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $url = TMP_DIR_URL . 'cache/images/';
        $file = md5($stream) . '.' . $info->file->name;

        if ( ! file_exists($path . $file)) {
            $_stream = fopen($path . $file, 'w');
            fputs($_stream, $stream);
            fclose($_stream);
        }

        if ($absolute === true) {
            return $path . $file;
        }

        return $url . $file;
    }

    public static function getUploadImagePath($image)
    {

        return HOME_DIR_ABSOLUTE . $image;
    }

    public static function getUploadImageUrl($image)
    {

        return HOME_DIR_URL . $image;
    }

    /**
     * Объект конфигуации
     *
     * @static
     * @return stdClass
     */
    public static function getConfig($param = null, $default = null)
    {
        if ($param == null) {
            return new Fenix_Engine_Settings();
        }

        $SettingsStorage = Zend_Registry::get('FenixEngine_Settings_Storage');

        if (array_key_exists($param, $SettingsStorage)) {
            if ($default != null) {
                if ($SettingsStorage[$param] == null) {
                    return $default;
                } else {
                    return $SettingsStorage[$param];
                }
            } else {
                return $SettingsStorage[$param];
            }
        }

        return '';
    }

    /**
     * Информация о системе
     *
     * @static
     * @return stdClass
     */
    public static function getSystemInfo()
    {
        $Object = new Fenix_Object();

        $Object->setSofttype('Система управления сайтом')
            ->setName('Fenix engine')
            ->setVersion('5.0')
            ->setFullname($Object->getSofttype() . ' ' . $Object->getName() . ' ' . $Object->getVersion());

        $Object->setDetails('Будущее описание');

        return $Object;
    }

    /**
     * Перенаправление
     *
     * @static
     * @param string $url Ссылка
     * @param bool $nowrap
     * @param int $redirect_code
     * @return void
     */
    public static function redirect($url, $nowrap = false, $redirect_code = 302)
    {
        if ($redirect_code == 301) {
            header("HTTP/1.1 301 Moved Permanently");
        }
        if ($nowrap !== false) {

            header("Cache-Control: no-cache");
            //Fenix::dump("Location: " . $url);
            header("Location: " . $url, $redirect_code);
        } else {
            header("Location: " . self::getUrl($url), $redirect_code);
        }

        exit;
    }

    /**
     * Скриптовое обновление страницы
     *
     * @static
     * @return void
     */
    public static function refresh()
    {
        echo "<script>location.reload()</script>";
        exit;
    }

    public static function getUrl($path = null, $vars = null)
    {
        $path = trim($path, '/') . '/';//добавляем специально слеш в конец
        $langCode = Fenix_Language::getInstance()->getCurrentLanguageCode();
        $langCodeDef = Fenix_Language::getInstance()->getDefaultLanguageCode();

        $result = '/';

        if (self::getRequest()->getAccessLevel() == "backend") {
            $result .= ACP_NAME . "/";
        }

        //удаляем из $path возможное наличие приставки текущего языка
        $path = strpos($path, $langCode . '/') === 0 ? substr($path, strlen($langCode . '/')) : $path;

        //добавляем приставку языка, если это не язык по умолчанию
        $path = ($langCode != null && $langCode != $langCodeDef ? $langCode . ($path == null ? '' : '/') : '') . $path;

        // доп фильртация
        $path = preg_replace('~/+~', '/', $path);

        $result .= $path;

        // убираем слеш на конце
        $result = rtrim($result, '/');

        return PROTOCOL . DOMAIN . $result;
    }

    public static function getCreatorUI()
    {
        return new Creator_UI();
    }

    public static function getAppEtcUrl($path, $module)
    {
        $module = ucfirst($module);
        $relativePath = $module . '/' . APP_ETC_DIR . $path;
        $url = null;
        if (file_exists(APP_DIR_ABSOLUTE . 'Local/' . $relativePath)) {
            $url = APP_DIR_URL . 'Local/' . $relativePath;
        } elseif (file_exists(APP_DIR_ABSOLUTE . 'Fenix/' . $relativePath)) {
            $url = APP_DIR_URL . 'Fenix/' . $relativePath;
        }

        return $url;
    }

    public static function getSkinUrl($path)
    {
        $result = THEMES_DIR_CLEAN;

        if (self::getRequest()->getAccessLevel() == "backend") {
            $result .= "backend/";
        } else {
            $result .= Fenix::getTheme()->name . '/';
        }

        $result .= "skin/" . $path;

        if (Fenix::getStaticConfig()->settings->relative_links == '1') {
            return "/" . $result;
        } else {
            return "http://" . DOMAIN . "/" . $result;
        }
    }

    /**
     * Перевод секунд в минуты
     *
     * @static
     * @param int $playtimeseconds Секунды
     * @return string
     */
    public static function seconds2minutes($playtimeseconds)
    {
        $sign = $playtimeseconds < 0 ? '-' : '';

        $playtimeseconds = abs($playtimeseconds);
        $contentseconds = round((($playtimeseconds / 60) - floor($playtimeseconds / 60)) * 60);
        $contentminutes = floor($playtimeseconds / 60);

        if ($contentseconds >= 60) {
            $contentseconds -= 60;
            $contentminutes++;
        }

        return $sign . intval($contentminutes) . ':' . str_pad($contentseconds, 2, 0, STR_PAD_LEFT);
    }

    /**
     * Smarty truncate modifier plugin
     *
     * Type:     modifier<br>
     * Name:     truncate<br>
     * Purpose:  Truncate a string to a certain length if necessary,
     *           optionally splitting in the middle of a word, and
     *           appending the $etc string or inserting $etc into the middle.
     * @link http://smarty.php.net/manual/en/language.modifier.truncate.php
     *          truncate (Smarty online manual)
     * @author   Monte Ohrt <monte at ohrt dot com>
     * @param string
     * @param integer
     * @param string
     * @param boolean
     * @param boolean
     * @return string
     */
    static function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
    {
        if ($length == 0) {
            return '';
        }

        if (function_exists('mb_strlen')) {

            $charset = function_exists('mb_strlen') ? 'UTF-8' : 'ISO-8859-1';

            if (mb_strlen($string, $charset) > $length) {
                $length -= min($length, mb_strlen($etc, $charset));
                if ( ! $break_words && ! $middle) {
                    $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1, $charset));
                }
                if ( ! $middle) {
                    return mb_substr($string, 0, $length, $charset) . $etc;
                }

                return mb_substr($string, 0, $length / 2, $charset) . $etc . mb_substr($string, -$length / 2, $length,
                        $charset);
            }

            return $string;
        }

        // no MBString fallback
        if (isset($string[$length])) {
            $length -= min($length, strlen($etc));
            if ( ! $break_words && ! $middle) {
                $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
            }
            if ( ! $middle) {
                return substr($string, 0, $length) . $etc;
            }

            return substr($string, 0, $length / 2) . $etc . substr($string, -$length / 2);
        }

        return $string;
    }

    /**
     * Подготовка строки для использования в адресной строке и тд
     *
     * @static
     * @param string $string
     * @return string
     */
    public static function stringProtect($string)
    {
        $string = self::translitRuEng($string);
        $string = str_replace(" +", " ", $string);
        $string = str_replace(" ", "_", $string);
        $string = str_replace("/", "_", $string);
        $string = str_replace("'", "", $string);
        $string = htmlspecialchars($string, ENT_QUOTES);
        $string = strtolower($string);

        return $string;
    }

    public static function stringProtectUrl($string)
    {
        $string = trim($string);

        $string = str_replace(" ", "-", $string);
        $string = str_replace("_", "-", $string);
        $string = str_replace("–", "-", $string);
        $string = str_replace(".", "-", $string);
        $string = str_replace("+", "", $string);
        $string = str_replace("%", "-", $string);
        $string = str_replace(",", "-", $string);
        //$string = str_replace("/", "-", $string);

        $string = str_replace("'", "", $string);
        $string = str_replace("\"", "", $string);
        $string = str_replace("»", "", $string);
        $string = str_replace("«", "", $string);
        $string = str_replace(">", "", $string);
        $string = str_replace("<", "", $string);
        $string = str_replace("»", "", $string);
        $string = str_replace("«", "", $string);
        $string = str_replace("|", "", $string);

        $string = str_replace("`", "", $string);
        $string = str_replace("!", "", $string);
        $string = str_replace("#", "", $string);
        $string = str_replace("№", "", $string);
        $string = str_replace("$", "", $string);
        $string = str_replace(";", "", $string);
        $string = str_replace(":", "", $string);
        $string = str_replace("%", "", $string);
        $string = str_replace("^", "", $string);
        $string = str_replace("*", "", $string);
        $string = str_replace("(", "", $string);
        $string = str_replace(")", "", $string);


        $string = self::translitRuEng($string);

        $string = htmlspecialchars($string, ENT_QUOTES);
        $string = strtolower($string);

        $string = preg_replace("/[^A-Za-z0-9\-\.]/", '-', $string);

        $string = preg_replace('/\-+/', '-', $string);

        $string = trim($string, '-');

        return $string;
    }

    /**
     *   Таблицы ГОСТ-а 16876-71
     *
     *   Буквы j и h используются в качестве модификаторов, причем j ставится перед
     *   основной буквой, а h - после основной буквы.
     *
     *    а - a       к - k       х - kh
     *    б - b       л - l       ц - c
     *    в - v       м - m       ч - ch
     *    г - g       н - n       ш - sh
     *    д - d       о - o       щ - shh
     *    е - e       п - p       ъ - "
     *    ё - jo      р - r       ы - y
     *    ж - zh      с - s       ь - '
     *    з - z       т - t       э - eh
     *    и - i       у - u       ю - ju
     *    й - jj      ф - f       я - ja
     *
     * @static
     * @param string $word Строка в кириллице
     * @return string
     */
    public static function translitRuEng($word)
    {
        $word = trim($word);

        $word = str_replace("а", "a", $word);
        $word = str_replace("б", "b", $word);
        $word = str_replace("в", "v", $word);
        $word = str_replace("г", "g", $word);
        $word = str_replace("д", "d", $word);
        $word = str_replace("е", "e", $word);
        $word = str_replace("ё", "jo", $word);
        $word = str_replace("ж", "zh", $word);
        $word = str_replace("з", "z", $word);
        $word = str_replace("и", "i", $word);
        $word = str_replace("й", "jj", $word);
        $word = str_replace("к", "k", $word);
        $word = str_replace("л", "l", $word);
        $word = str_replace("м", "m", $word);
        $word = str_replace("н", "n", $word);
        $word = str_replace("о", "o", $word);
        $word = str_replace("п", "p", $word);
        $word = str_replace("р", "r", $word);
        $word = str_replace("с", "s", $word);
        $word = str_replace("т", "t", $word);
        $word = str_replace("у", "u", $word);
        $word = str_replace("ф", "f", $word);
        $word = str_replace("х", "kh", $word);
        $word = str_replace("ц", "c", $word);
        $word = str_replace("ч", "ch", $word);
        $word = str_replace("ш", "sh", $word);
        $word = str_replace("щ", "shh", $word);
        $word = str_replace("ъ", '', $word);
        $word = str_replace("ы", "y", $word);
        $word = str_replace("ь", "", $word);
        $word = str_replace("э", "eh", $word);
        $word = str_replace("ю", "ju", $word);
        $word = str_replace("я", "ja", $word);
        $word = str_replace(" ", "_", $word);

        $word = str_replace("А", "A", $word);
        $word = str_replace("Б", "B", $word);
        $word = str_replace("В", "V", $word);
        $word = str_replace("Г", "G", $word);
        $word = str_replace("Д", "D", $word);
        $word = str_replace("Е", "E", $word);
        $word = str_replace("Ё", "JO", $word);
        $word = str_replace("Ж", "ZH", $word);
        $word = str_replace("З", "Z", $word);
        $word = str_replace("И", "I", $word);
        $word = str_replace("Й", "JJ", $word);
        $word = str_replace("К", "K", $word);
        $word = str_replace("Л", "L", $word);
        $word = str_replace("М", "M", $word);
        $word = str_replace("Н", "N", $word);
        $word = str_replace("О", "O", $word);
        $word = str_replace("П", "P", $word);
        $word = str_replace("Р", "R", $word);
        $word = str_replace("С", "S", $word);
        $word = str_replace("Т", "T", $word);
        $word = str_replace("У", "U", $word);
        $word = str_replace("Ф", "F", $word);
        $word = str_replace("Х", "KH", $word);
        $word = str_replace("Ц", "C", $word);
        $word = str_replace("Ч", "CH", $word);
        $word = str_replace("Ш", "SH", $word);
        $word = str_replace("Щ", "SHH", $word);
        $word = str_replace("Ъ", '', $word);
        $word = str_replace("Ы", "Y", $word);
        $word = str_replace("Ь", "", $word);
        $word = str_replace("Э", "EH", $word);
        $word = str_replace("Ю", "JU", $word);
        $word = str_replace("Я", "JA", $word);

        return $word;
    }

    public static function TranslitFileName($file)
    {
        if ( ! is_object($file)) {
            throw new InvalidArgumentException('Некорректное значение переменной $file, должен передаваться объект');
        }
        $fileNameArray = explode('.', $file->name);
        $fileExtension = array_pop($fileNameArray);
        $fileName = implode('', $fileNameArray);
        $fileName = mb_strtolower(self::clearString(self::translitRuEng($fileName)));

        return $fileName . '.' . $fileExtension;
    }

    //удаление всех недопустимых символов в имени файла
    public static function clearString($string)
    {

        // удаление всех символов, кроме букв, цифр, дефисов, подчеркиваний и пробелов
        $NOT_acceptable_characters_regex = '#[^-a-zA-Z0-9_ ]#';
        $string = preg_replace($NOT_acceptable_characters_regex, '', $string);

        // замена всех дефисов, подчеркиваний и пробелов дефисами
        $string = preg_replace('#[-_ ]+#', '_', $string);

        // удаление всех начальных и конечных пробелов
        $string = trim($string);

        // возврат измененной строки
        return $string;
    }

    /**
     * Функция выводящая случайные символы
     *
     * @param integer $count
     * @param boolean $hash
     * @param array $char
     * @return string
     */
    public static function getRand(
        $count = 1,
        $hash = false,
        $char = array(
            'a',
            'A',
            'b',
            'B',
            'c',
            'C',
            'd',
            'D',
            'e',
            'E',
            'f',
            'F',
            'g',
            'G',
            'h',
            'H',
            'i',
            'I',
            'j',
            'J',
            'k',
            'K',
            'l',
            'L',
            'm',
            'M',
            'n',
            'N',
            'o',
            'O',
            'p',
            'P',
            'q',
            'Q',
            'r',
            'R',
            's',
            'S',
            't',
            'T',
            'u',
            'U',
            'v',
            'V',
            'w',
            'W',
            'x',
            'X',
            'y',
            'Y',
            'z',
            'Z',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0'
        )
    ) {
        $max_chars = count($char) - 1;
        srand((double)microtime() * 1000000);

        $rand_str = '';

        for ($i = 0; $i < $count; $i++) {
            $rand_str = ($i == 0 ? $char[rand(0, $max_chars)] : $rand_str . $char[rand(0, $max_chars)]);
        }

        return ($hash) ? md5($rand_str) : $rand_str;
    }

    /**
     * Преобразование строки с разделителем "_" в кемелкейс
     * Например, core_controller_index преобразует в
     * coreControllerIndex
     *
     * @static
     * @param string $str строка
     * @return string
     */
    public static function toCamelCase($str, $capitalise_first_char = false)
    {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }

        $func = create_function('$c', 'return strtoupper($c[1]);');

        return preg_replace_callback('/_([a-z])/', $func, $str);
    }

    /**
     * Обратная функция toCamelCase
     * Например, coreControllerIndex преобразует в
     * core_controller_index
     *
     * @static
     * @param string $str строка
     * @return string
     */
    public static function fromCamelCase($str)
    {
        $str[0] = strtolower($str[0]);
        $func = create_function('$c', 'return "_" . strtolower($c[1]);');

        return preg_replace_callback('/([A-Z])/', $func, $str);
    }

    /**
     * Форматирования размера
     * Функции задается число байт, и в зависимости от количества байт возвращает
     * число с приставкой Kb, Mb и т.д.
     *
     * @static
     * @param integer $size
     * @return string
     */
    public static function formatSize($size)
    {
        $unit = array('b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');

        if ($size == 0) {
            return "0 b";
        } else {
            return round($size / pow("1024", ($i = floor(log($size, "1024")))), "2") . " " . $unit[$i];
        }
    }

    /**
     * Обертка для htmlspecialchars
     *
     * @static
     * @param string $string Строка
     * @return string
     */
    public static function specialChars($string)
    {
        return htmlspecialchars($string);
    }

    /**
     * Функция честно стыренная с phpMyAdmin
     *
     * @static
     * @return array
     */
    public static function getPageSelector(
        $rows,
        $pageNow = 1,
        $nbTotalPage = 1,
        $showAll = 200,
        $sliceStart = 5,
        $sliceEnd = 5,
        $percent = 20,
        $range = 10,
        $prompt = ''
    ) {
        $increment = floor($nbTotalPage / $percent);
        $pageNowMinusRange = ($pageNow - $range);
        $pageNowPlusRange = ($pageNow + $range);

        if ($nbTotalPage < $showAll) {
            $pages = range(1, $nbTotalPage);
        } else {
            $pages = array();

            // Always show first X pages
            for ($i = 1; $i <= $sliceStart; $i++) {
                $pages[] = $i;
            }

            // Always show last X pages
            for ($i = $nbTotalPage - $sliceEnd; $i <= $nbTotalPage; $i++) {
                $pages[] = $i;
            }

            // Based on the number of results we add the specified
            // $percent percentage to each page number,
            // so that we have a representing page number every now and then to
            // immediately jump to specific pages.
            // As soon as we get near our currently chosen page ($pageNow -
            // $range), every page number will be shown.
            $i = $sliceStart;
            $x = $nbTotalPage - $sliceEnd;
            $met_boundary = false;
            while ($i <= $x) {
                if ($i >= $pageNowMinusRange && $i <= $pageNowPlusRange) {
                    // If our pageselector comes near the current page, we use 1
                    // counter increments
                    $i++;
                    $met_boundary = true;
                } else {
                    // We add the percentage increment to our current page to
                    // hop to the next one in range
                    $i += $increment;

                    // Make sure that we do not cross our boundaries.
                    if ($i > $pageNowMinusRange && ! $met_boundary) {
                        $i = $pageNowMinusRange;
                    }
                }

                if ($i > 0 && $i <= $x) {
                    $pages[] = $i;
                }
            }

            // Since because of ellipsing of the current page some numbers may be double,
            // we unify our array:
            sort($pages);
            $pages = array_unique($pages);
        }

        return $pages;
    } // end function

    /**
     * Изменение окончания слова в зависимости от цифры
     *
     * 'товаров', 'товар', 'товара'
     * @static
     * @param int $number Число
     * @param array $words Массив слов, которые будут выводиться в зависимости от $number
     * @return string
     */
    public static function declination($number, $words)
    {
        $numberLast = intval(substr(strval($number), -1, 1));
        $numberPreLast = intval(substr(strval($number), -2, 2));

        if (($numberLast == 0) || ((5 <= $numberLast) && ($numberLast <= 9)) || ((11 <= $numberPreLast) && ($numberPreLast <= 19))) {
            $type = 0;
        } elseif (($numberLast == 1) && ($numberPreLast != 11)) {
            $type = 1;
        } elseif ((2 <= $numberLast) && ($numberLast <= 4)) {
            $type = 2;
        }

        return $words[$type];
    }

    /**
     * Красивый вывод числа 1 192 555
     * @param $value
     * @param int $presision
     * @param string $dot
     * @param string $space
     * @return string
     */
    public static function number($value, $presision = 0, $dot = '.', $space = ' ')
    {
        return number_format($value, $presision, $dot, $space);
    }

    /**
     * Перевод времени в секундах в строку
     * Например, 36 секунд назад, 5 минут назад и тд
     *
     * @static
     * @param int $date Таймштамп
     * @return string
     */
    public static function date2str($date)
    {
        $d = time() - $date;

        if ($d < 60) {
            return $d . self::declination($d, array(
                    ' секунд назад',
                    ' секунда назад',
                    ' секунды назад'
                ));
        } elseif ($d < 3600) {
            $_d = round($d / 60);

            return $_d . self::declination($_d, array(
                    ' минут назад',
                    ' минута назад',
                    ' минуты назад'
                ));
        } elseif ($d < 86400) {
            $_d = round($d / 3600);

            return $_d . self::declination($_d, array(
                    ' часов назад',
                    ' час назад',
                    ' часа назад'
                ));
        } else {
            $_d = round($d / 86400);

            return $_d . self::declination($_d, array(
                    ' дней назад',
                    ' день назад',
                    ' дня назад'
                ));
        }
    }

    /**
     * Загрузка объекта запроса
     *
     * @static
     * @return Fenix_Controller_Request_Http
     */
    public static function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    /**
     * Загрузка объекта для работы с датой
     *
     * @static
     * @param string $time Время и дата
     * @param string $timezone Часовой пояс
     * @return DateTime
     */
    public static function getDate($time = null, $timezone = null)
    {
        return new DateTime($time);
    }

    /**
     * Загрузка коллекций данных
     *
     * @static
     * @param string $type Путь к класу
     * @return Fenix_Object
     */
    public static function getCollection($type)
    {
        return self::_loadClass($type, 'Collection');
    }

    /**
     * Загрузка моделей
     *
     * @static
     * @param string $type Путь к класу
     * @return Fenix_Resource_Model
     */
    public static function getModel($type = null)
    {
        if ($type == null) {
            return new Fenix_Resource_Model();
        }

        return self::_loadClass($type, 'Model');
    }

    /**
     * Загрузка помощников
     *
     * @static
     * @param string $type Путь к класу
     * @return Fenix_Resource_Helper
     */
    public static function getHelper($type)
    {
        return self::_loadClass($type, 'Helper');
    }

    /**
     * Загрузка блоков
     *
     * @static
     * @param string $type Путь к класу
     * @return Fenix_Resource_Block
     */
    public static function getBlock($type)
    {
        return self::_loadClass($type, 'Block');
    }

    /**
     * Загрузка классов моделей, помощников и блоков
     */
    private static function _loadClass($type, $namespace)
    {
        $start = microtime(true);
        $cacheId = '_loadClass_' . md5($type . $namespace);
        $useRegistry = true;
        if (
            $useRegistry &&
            in_array($namespace, array('Collection', 'Block')) === false &&
            Zend_Registry::isRegistered($cacheId)) {

            $object = Zend_Registry::get($cacheId);

            $time = microtime(true) - $start;
            Fenix::$time += $time;
            Fenix::$objectLoaded += 1;

            return $object;
        }
        list($app, $path) = explode('/', $type);

        $className = array($app, $namespace);
        $className = array_merge($className, (array)explode('_', $path));
        $className = array_map(function ($value) {
            return ucfirst($value);
        }, $className);
        $className = implode('_', $className);

        // Local
        try {
            $class2load = 'Local_' . $className;
            Zend_Loader::loadClass($class2load);
        } catch (Exception $e) {
            // Fenix
            try {
                $class2load = 'Fenix_' . $className;
                Zend_Loader::loadClass($class2load);
            } catch (Exception $e) {
                new Fenix_Exception($e);
            }
        }

        $object = new $class2load();

        if ($useRegistry && in_array($namespace, array('Collection', 'Block')) === false) {
            Zend_Registry::set($cacheId, $object);
        }

        $time = microtime(true) - $start;
        Fenix::$time += $time;
        Fenix::$objectCreated += 1;

        return $object;
    }

    /**
     * Сборка XML файлов из app
     */
    public static function assembleXml($xml, $asObject = false)
    {
        $Dir = new Fenix_Dir();
        $xmlData = array();

        // Local
        $Stream = $Dir->index(APP_DIR_ABSOLUTE . 'Local' . DS);
        $dirList = $Dir->getDirList($Stream);

        if ($dirList !== false) {
            for ($i = 0, $max_i = sizeof($dirList); $i < $max_i; $i++) {
                $dataXML = $dirList[$i]['realpath'] . DS . APP_ETC_DIR . $xml;
                if (file_exists($dataXML)) {
                    $xmlData[] = new Zend_Config_Xml($dataXML, null, true);
                }
            }
        }

        // Fenix
        $Stream = $Dir->index(APP_DIR_ABSOLUTE . 'Fenix' . DS);
        $dirList = $Dir->getDirList($Stream);

        if ($dirList !== false) {
            for ($i = 0, $max_i = sizeof($dirList); $i < $max_i; $i++) {
                $dataXML = $dirList[$i]['realpath'] . DS . APP_ETC_DIR . $xml;
                if (file_exists($dataXML)) {
                    $xmlData[] = new Zend_Config_Xml($dataXML, null, true);
                }
            }
        }

        if (sizeof($xmlData) > 0) {
            $result = $xmlData[0];
            for ($i = 1, $max_i = sizeof($xmlData); $i < $max_i; $i++) {
                $result->merge($xmlData[$i]);
            }

            $xmlData = array();

            if ($asObject === true) {
                return $result;
            }

            $xmlData = $result->toArray();
        }

        return $xmlData;
    }

    public static function loadControllerPlugins(array $xml)
    {
        if (isset($xml['controller']['plugins'])) {
            $plugins = $pluginsList = array();
            foreach ($xml['controller']['plugins'] AS $pluginClass => $plugin) {
                if (isset($plugin['offset'])) {
                    $plugin['name'] = $pluginClass;
                    $pluginsList[$plugin['offset']] = $plugin;
                }
            }
            ksort($plugins);

            if (sizeof($pluginsList) > 0) {
                foreach ($pluginsList AS $plugin) {
                    if (isset($plugin['name']) && Zend_Loader::loadClass($plugin['name']) !== false) {
                        $pluginClass = $plugin['name'];
                        Zend_Controller_Front::getInstance()->registerPlugin(new $pluginClass());
                    }
                }
            }
        }
    }

    public static function loadControllerHelpers(array $xml)
    {
        if (isset($xml['controller']['helpers'])) {
            $helpers = $helpersList = array();

            foreach ($xml['controller']['helpers'] AS $helperClass => $helper) {
                if (isset($helper['offset'])) {
                    $helper['name'] = $helperClass;
                    $helpersList[$helper['offset']] = $helper;
                }
            }
            ksort($helpers);

            if (sizeof($helpersList) > 0) {
                foreach ($helpersList AS $helper) {
                    if (isset($helper['name']) && @Zend_Loader::loadClass($helper['name']) !== false) {
                        $helperClass = $helper['name'];
                        Zend_Controller_Action_HelperBroker::addHelper(new $helperClass());
                    }
                }
            }
        }
    }

    public static function loadViewHelpers(array $xml)
    {
        if (isset($xml['view'])) {
            $helpersList = array();

            $helpersList = $xml['view']['helpers'];

            if (sizeof($helpersList) > 0) {
                $view = Zend_Layout::getMvcInstance()->getView();
                foreach ($helpersList AS $helper) {
                    $view->addHelperPath(APP_DIR_ABSOLUTE . $helper['path'], $helper['namespace']);
                }
            }
        }
    }

    static public function isDev()
    {
        $developers = array(
            "91.193.69.155",
            "93.183.232.63",
            "194.29.63.35",
            "217.24.161.42",
            "77.222.158.169",
            "127.0.0.1"
        );

        return in_array($_SERVER['REMOTE_ADDR'], $developers);
    }

    static public function dd($var, $clear = true)
    {
        if ( ! self::isDev()) {
            return;
        }

        $dump_data = $var;
        if (func_num_args() > 1) {
            $dump_data = func_get_args();
        }

        $debug_backtrace = debug_backtrace();
        $debug_shift = array_shift($debug_backtrace);

        $path = str_replace(BASE_DIR, '', $debug_shift['file']);

        $data = array(
            'backtrace'  => $debug_backtrace,
            'line'       => $debug_shift['line'],
            'file'       => $path,
            'properties' => $dump_data,
            'methods'    => get_class_methods($dump_data)
        );

        if ($clear) {
            while (ob_get_level()) {
                ob_end_clean();
            }
            Symfony\Component\VarDumper\VarDumper::dump($data, $clear);
            exit();
        }

        return Symfony\Component\VarDumper\VarDumper::dump($data, $clear);
    }

    static public function dump($var = null)
    {
        if ( ! self::isDev()) {
            return;
        }

        while (ob_get_level()) {
            ob_end_clean();
        }

        if (func_num_args() > "1") {
            $var = func_get_args();
        }

        echo '<pre>';
        $debug_backtrace = debug_backtrace();
        $trace = array_shift($debug_backtrace);
        echo "<b>Debug <font color=red>" . $trace['file'] . "</font> on line <font color=red>{$trace['line']}</font></b>:\r\n";
        $file = file($trace['file']);
        echo str_replace("\t", "",
            "<div style='background: #f5f5f5; padding: 0.2em 0em;'>" . htmlspecialchars($file[$trace['line'] - 1]) . "</div>\r\n");

        echo '<b>Type</b>: ' . gettype($var) . "\r\n";

        if (is_string($var)) {
            echo "<b>Lenght</b>: " . strlen($var) . "\r\n";
        }

        if (is_array($var)) {
            echo "<b>Elemnts</b>: " . count($var) . "\r\n";
        }

        echo '<b>' . gettype($var) . '</b>: ';

        if (is_string($var)) {
            echo htmlspecialchars($var);
        } elseif (is_object($var)) {
            $print_r = array();

            $print_r['var'] = $var;
            $print_r['methods'] = get_class_methods($var);

            print_r($print_r);
        } else {
            $print_r = print_r($var, true);

            if ((strstr($print_r, '<') !== false) || (strstr($print_r, '>') !== false)) {
                $print_r = htmlspecialchars($print_r);
            }
            echo $print_r;
        }

        echo '</pre>';

        exit;
    }

    public static function decorateMonth($month)
    {
        switch ($month) {
            case '01':
                return Fenix::lang('Январь');
                break;
            case '02':
                return Fenix::lang('Февраль');
                break;
            case '03':
                return Fenix::lang('Март');
                break;
            case '04':
                return Fenix::lang('Апрель');
                break;
            case '05':
                return Fenix::lang('Май');
                break;
            case '06':
                return Fenix::lang('Июнь');
                break;
            case '07':
                return Fenix::lang('Июль');
                break;
            case '08':
                return Fenix::lang('Август');
                break;
            case '09':
                return Fenix::lang('Сентябрь');
                break;
            case '10':
                return Fenix::lang('Октябрь');
                break;
            case '11':
                return Fenix::lang('Ноябрь');
                break;
            case '12':
                return Fenix::lang('Декабрь');
                break;
        }
    }

    public static function xmlProtect($str)
    {
        $list = array(
            '&'       => '&amp;',
            '"'       => '&quot;',
            '>'       => '&gt;',
            '<'       => '&lt;',
            "'"       => '&apos;',
            '&laquo;' => '&quot;',
            '&ndash;' => '-'
        );

        foreach ($list as $search => $replace) {
            $str = str_replace($search, $replace, $str);
        }

        return $str;
    }

    public static function getImageUrl($path)
    {
        return HOME_DIR_URL . $path;
    }

    public static function compressHtml($html)
    {
        if (Fenix::getStaticConfig()->settings->optimisation_mode == '1') {
            /** Оптимизация через OptimizeHTML */
            $html = OptimizeHTML::html($html);

            /** Оптимизация через replace */
//            $html = str_replace('"//', '"replacslashesdoublequotes', $html);
//            $html = preg_replace('@(<!--.*?-->|/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|((?<!:)//.*)|[\t\r\n]@i', '', $html);
//            $html = preg_replace('@(\s){2,}@', '\1', $html);
//            $html = str_replace('"replacslashesdoublequotes', '"//', $html);
        }

        return $html;
    }

    static public function checkHrefsProtocol($content)
    {
        /** http */
        $content = str_replace(array(
            'href="http://' . $_SERVER['HTTP_HOST'] . '/',
            'href="http://www.' . $_SERVER['HTTP_HOST'] . '/',
        ), 'href="/', $content);
        $content = str_replace(array(
            'href="http://' . $_SERVER['HTTP_HOST'] . '"',
            'href="http://www.' . $_SERVER['HTTP_HOST'] . '"',
        ), 'href="/"', $content);
        $content = str_replace(array(
            'src="http://' . $_SERVER['HTTP_HOST'] . '/',
            'src="http://www.' . $_SERVER['HTTP_HOST'] . '/',
        ), 'src="/', $content);


        /** https */
        $content = str_replace(array(
            'href="https://' . $_SERVER['HTTP_HOST'] . '/',
            'href="https://www.' . $_SERVER['HTTP_HOST'] . '/',
        ), 'href="/', $content);
        $content = str_replace(array(
            'href="https://' . $_SERVER['HTTP_HOST'] . '"',
            'href="https://www.' . $_SERVER['HTTP_HOST'] . '"',
        ), 'href="/"', $content);
        $content = str_replace(array(
            'src="https://' . $_SERVER['HTTP_HOST'] . '/',
            'src="https://www.' . $_SERVER['HTTP_HOST'] . '/',
        ), 'src="/', $content);

        return $content;
    }

    public static function inlinePrepare($css)
    {
        $css = str_replace("url('../images", "url('" . Fenix::getSkinUrl('images'), $css);
        $css = str_replace('url("../images', 'url("' . Fenix::getSkinUrl('images'), $css);

        return $css;
    }

    public static function removeDirectory($dir)
    {
        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? Fenix::removeDirectory($obj) : unlink($obj);
            }
        }

        return rmdir($dir);
    }

    public static function getQueryString($query_arr = null)
    {
        $queries = array();
        if ($query_arr === null) {
            $query_arr = self::getRequest()->getQuery();
        }

        if ( ! empty($query_arr)) {
            foreach ($query_arr as $param => $value) {
                $queries[] = $param . '=' . $value;
            }
        }

        if ( ! empty($queries)) {
            return '?' . implode('&', $queries);
        }

        return '';
    }

    /**
     *  Возвращает true - если текущий запрос был сделан через AJAX или POST
     *
     *  Пока тестируется - не знаю как себя покажет.
     */
    public static function isAJAX()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}