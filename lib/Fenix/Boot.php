<?php
class Fenix_Boot
{
    public function init()
    {
        // Создаем список директорий, в которых будем искать необходимые классы
        $this->_setIncludesDirectoryList(
            BASE_DIR,
            APP_DIR_ABSOLUTE,
            LIB_DIR_ABSOLUTE
        );
        
        // Включаем автозагрузчик классов
        $this->_autoLoader();
        
        // Стартуем сессию
        Zend_Session::start();
        
        return $this;
    }
    
    public function initDb()
    {
        try {
            $dbСonfigFile = ETC_DIR_ABSOLUTE . "database.xml";
            
            $database     = new Zend_Config_Xml($dbСonfigFile, "database");
            
            Zend_Registry::set('database', $database);
              
            $db = Zend_Db::factory($database);
            
            Zend_Db_Table_Abstract::setDefaultAdapter($db);
            $db->query('SET NAMES utf8');
            
            $db->getProfiler()
               ->setEnabled(true);
            
            Zend_Registry::set('db', $db);
            
            // Сначала создается объект кэша
            $cache = Fenix_Cache::getCache('core/database_meta');

            Fenix::dump($cache);
            if ($cache !== false)
                Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);            

        }
        catch (Zend_Db_Exception $e) {
            $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Сообщение базы данных</h4>';
            $tpl.= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
            $tpl.= $e->getMessage();
            $tpl.= '</div></div>';
            die($tpl);
        }
        catch (Exception $e) {
            $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Сообщение базы данных</h4>';
            $tpl.= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
            $tpl.= $e->getMessage();
            $tpl.= '</div></div>';
            
            die($tpl);
        }
        
        return $this;
    }
    
    /** 
     * Запуск приложения
     * 
     * @access private
     * @package FenixEngine
     * @access AleXX
     * @return void 
     */
    public function run()
    {
        try
        {
            
        Zend_Layout::startMvc(array(
            'layoutPath' => 'var/'
        ));

        // Получение объекта Zend_Layout
        $layout = Zend_Layout::getMvcInstance();
        
        // Инициализация объекта Zend_View
        //$layout->setView(new Fenix_View());
        $view = $layout->getView();
        
        $view->addScriptPath('var/');
       //$view->setHelperPath("Fenix/View/Helper", "Fenix_View_Helper_");
        
        Zend_Registry::set("getView", $view);
        
        // Настройка расширения макетов
        $layout->setViewSuffix('phtml');

        // Установка объекта Zend_View
        $layout->setView($view);
        
        // Настройка расширения view скриптов с помощью Action помошников
        
        $viewRenderer = new Fenix_Controller_Action_Helper_ViewRenderer();
        $viewRenderer
            ->setView($view)
            ->setViewSuffix('phtml');
        
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
                    
            
            
            Fenix::dump(new Fenix_Controller_Request_Http());
            
            $locale = new Zend_Locale('uk_UA');
            Zend_Registry::set('Zend_Locale', $locale);
            
            $FrontController = Zend_Controller_Front::getInstance();
            
            $FrontController->setRequest(new Fenix_Controller_Request_Http());
            $FrontController->setDispatcher(new Fenix_Controller_Dispatcher());
            
            $FrontController->addModuleDirectory(APP_DIR_ABSOLUTE);
            
            $FrontController->throwexceptions(true);
            
            $FrontController->dispatch();
        }
        catch (Exception $e)
        {
            new Fenix_Exception($e);
        }        
    }
    
    /**
     * Создание списка загрузочных директорий
     * 
     * @access private
     * @package FenixEngine
     * @access AleXX
     * @return void 
     */
    private function _setIncludesDirectoryList()
    {
        $path_list = array();
        $path_list = func_get_args();
        $paths     = implode(PATH_SEPARATOR, $path_list);
        
        if (!empty($paths)) {
            $paths.= get_include_path();
        } 
        else {
            $paths = get_include_path();
        }
        
        set_include_path($paths);

        return;
    }

    /**
     * Регистрация автозагрузчика классов
     * 
     * @access private
     * @package FenixEngine
     * @author AleXX
     * @throws Fenix_Exception
     * @return void 
     */
    private function _autoLoader()
    {
        try {
            $autoloaderScript = 'Zend/Loader/Autoloader.php';
           
            if (!file_exists(LIB_DIR_ABSOLUTE . $autoloaderScript))
                throw new Exception("Автозагрузчик не найден <strong>" . $autoloaderScript . "</strong>");
            
            require_once $autoloaderScript;
            
            $autoloader = Zend_Loader_Autoloader::getInstance();
            $autoloader ->registerNamespace("Fenix_");
            $autoloader ->setFallbackAutoloader(true);
        }
        catch (Exception $e) {
            require_once ('Fenix/Exception.php');
            new Fenix_Exception($e);
        }
    }    
}