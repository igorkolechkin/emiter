<?php
class Fenix_Object
{
    /**
     * Тут мы храним данные
     */
    private $_data = array();
    
    public function __construct($options = array())
    {
        if (isset($options['data']))
            $this->_data = $options['data'];
    }

    public function count()
    {
        return sizeof($this->_data);
    }

    /**
     * Записывает данные в массив
     *
     * @param string|array $key
     * @param mixed $value
     * @return Fenix_Object
     */
    public function setData($key, $value=null)
    {
        if (is_array($key)) {
            $this->_data = $key;
        }
        else {
            $this->_data[$key] = $value;
        }
        
        return $this;
    }

    /**
     * Извлекет данные из массива
     *
     * @param string $key
     * @return mixed|null
     */
    public function getData($key = null)
    {
        if ($key == null)
            return $this->_data;
        
        if(isset($this->_data[$key])) 
            return $this->_data[$key];

        return null;
    }

    /**
     * Возвращает все данные объекта массивом
     * @return mixed|null
     */
    public function toArray(){
        return $this->getData();
    }
    
    /**
     * Магический вызов
     * 
     * @return Fenix_Object
     */
    public function __call($method, $args)
    {
        switch(substr($method, 0, 3))
        {
            case 'set':
                $key = substr($method, 3);         
                $key = Fenix::fromCamelCase($key);
                
                $this->setData($key, (isset($args[0]) ? $args[0] : null));
            break;
            
            case 'get':
                $key = substr($method, 3);         
                $key = Fenix::fromCamelCase($key);
                
                return $this->getData($key, (isset($args[0]) ? $args[0] : null));
            break;
        }
        
        return $this;
    }
    
    public function __get($key)
    {
        return $this->getData($key);
    }
}