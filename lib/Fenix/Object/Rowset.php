<?php
class Fenix_Object_Rowset implements Iterator, Countable, ArrayAccess
{
    private $_data    = array(),
        $_pointer = 0,
        $_count   = 0,
        $_rowClass = 'Fenix_Object';

    public function __construct($options = array())
    {
        if (isset($options['data']))
            $this->_data = $options['data'];

        if (isset($options['rowClass']))
            $this->_rowClass = $options['rowClass'];
    }

    public function getData()
    {
        return $this->_data;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_data[] = $value;
        } else {
            $this->_data[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->_data[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->_data[$offset]);
    }

    public function offsetGet($offset)
    {
        $data = isset($this->_data[$offset]) ? $this->_data[$offset] : null;

        return new $this->_rowClass(array(
            'data' => $data
        ));
    }

    public function count()
    {
        return sizeof($this->_data);
    }

    public function rewind()
    {
        reset($this->_data);
    }

    public function current()
    {
        $data = current($this->_data);

        if($this->_rowClass == '' || $this->_rowClass == false){
            return $data;
        }

        return new $this->_rowClass(array(
            'data' => $data
        ));
    }

    public function key()
    {
        $data = key($this->_data);
        return $data;
    }

    public function next()
    {
        $data = next($this->_data);
        return $data;
    }

    public function valid()
    {
        $key  = key($this->_data);
        $data = ($key !== null && $key !== false);

        return $data;
    }
}