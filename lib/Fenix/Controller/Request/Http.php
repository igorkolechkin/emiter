<?php
class Fenix_Controller_Request_Http extends Zend_Controller_Request_Http
{
    private $_parsedPathInfo = null;
    
    /**
     * @staticvar
     * @var Fenix_Controller_Request_Http
     */
    static private $_instance = null;
    
    static function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new Fenix_Controller_Request_Http();
       
        return self::$_instance;
    }  
    
    public function __construct()
    {
        parent::__construct();
        $this->_parsedPathInfo = $this->preparePathinfo();
    }

    public function getParsedInfo()
    {
        return $this->_parsedPathInfo;
    }

    public function getFiles($key = null)
    {
        if ($key == null)
            return $_FILES;
        
        if (!array_key_exists($key, $_FILES))
            return null;
            
        return (object) $_FILES[$key];
    }
    
    public function getAccessLevel()
    {
        if ($this->_parsedPathInfo->isAcp)
            return 'backend';
        
        return 'frontend';
    }

    public function getUrlSegment($index = 0)
    {
        return $this->getUriSegment($index);
    }

    public function getUriSegment($index = 0)
    {
        if (isset($this->_parsedPathInfo->segments[$index]))
            return $this->_parsedPathInfo->segments[$index];
        
        return null;
    }
    
    public function preparePathinfo($pathinfo = null)
    {
        // Для тестирования языка
        $Language = Fenix_Language::getInstance();
        
        // Получение пасинфо
        if ($pathinfo == null)
            $pathinfo = $this->getPathinfo();

        $list = explode('/', $pathinfo);

        // Тестируем первый элемент массива
        if (isset($list[0]) && $list[0] == null)
            array_shift($list);

        // Проверяем на язык
        $lang = $Language->getCurrentLanguage()->code;
        if (isset($list[0]) && $list[0] == $lang) {
            array_shift($list);
        }
        // Индекс последнего элемента
        $lastIndex = (sizeof($list) - 1);
        
        // Тестируем последний элемент массива
        if (isset($list[$lastIndex]) && $list[$lastIndex] == null)
            array_pop($list);

        if (sizeof($list) == 0)
            $list[] = 'default';

        // Индекс последнего элемента
        $lastIndex = (sizeof($list) - 1);
        
        if (substr($list[$lastIndex], -5) == '.html') {
            $list[$lastIndex] = str_replace('.html', '', $list[$lastIndex]);
        }

        if (substr($list[$lastIndex], -4) == '.htm') {
            $list[$lastIndex] = str_replace('.htm', '', $list[$lastIndex]);
        }

        // Проверяем на ACP
        $isAcp = false;
        if (isset($list[0]) && $list[0] == ACP_NAME) {
            $isAcp = true;
            array_shift($list);

        }
        //Fenix::dump($list);
        // Формирование ответа
        $response = (object) array(
            'language' => $lang,
            'isAcp'    => $isAcp,
            'segments' => $list,
            'sizeof'   => sizeof($list)
        );

        //Fenix::dump($response);
        
        return $response;
    }
}