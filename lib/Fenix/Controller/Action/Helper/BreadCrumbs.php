<?php
class Fenix_Controller_Action_Helper_BreadCrumbs extends Zend_Controller_Action_Helper_Abstract
{    
    public function direct($navigation)
    {
        if ($navigation instanceof Fenix_Engine_Rowset)
        {
            $_navigation = array();
            $_navigation[] = array(
                'label' => Fenix::lang("Главная"),
                'uri'   => Fenix::getUrl(),
                'id'    => 'row_0'
            );
            
            foreach ($navigation AS $row)
            {
                $_navigation[] = array(
                    'label' => $row->title,
                    'uri'   => $row->getUrl(),
                    'id'    => 'row_' . $row->id
                );
            }
        }
        else
        {
            $_navigation = $navigation;
        }
        
        $breadcrumbs = new Zend_Navigation($_navigation);
        
        $this->getActionController()
             ->view
             ->navigation($breadcrumbs)
             ->breadcrumbs()
             ->setSeparator(Fenix::getConfig('systemBreadcrumbsSeparator'));
    }
}