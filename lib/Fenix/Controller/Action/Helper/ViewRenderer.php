<?php
class Fenix_Controller_Action_Helper_ViewRenderer extends Zend_Controller_Action_Helper_ViewRenderer
{
    public function getModuleDirectory()
    {
        return THEMES_DIR;
    }
}