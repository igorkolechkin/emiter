<?php
class Fenix_Controller_Action_Helper_Acl extends Zend_Controller_Action_Helper_Abstract
{
    private $_acl           = array();
    private $_currentUser   = array();
    private $_checked       = false;

    public function __construct()
    {
    }
    
    public function setupAcl()
    {
        // Проверка прав доступа
        $Acl = new Fenix_Acl();
        
        // Таблица ролей
        $AclTable = $Acl->prepareAclTable();
        
        $this->_currentUser = Fenix::getModel("users/session")->current();
        
        if (!$this->_currentUser)
            return;
        
        // Проверка прав доступа к бекенду
        if ($this->getRequest()->getAccessLevel() == "backend")
        {
            $BackendResources = $AclTable->admin;
            
            // Регистрируем роль пользователя
            $userRole     = new Zend_Acl_Role($this->_currentUser->email); 
            $Acl          ->addRole($userRole);
            
            $Model        = new Fenix_Resource_Model();

            // Разрешаем все владельцу
            if ($this->_currentUser->is_owner)
                $Acl ->allow($userRole, null, null);
            
            foreach ($BackendResources AS $ResourceName => $ResourceOption)
            {
                $Resource = new Zend_Acl_Resource($ResourceName);
                $Acl->add($Resource);
            
                $Select = $Model->setTable("users_acl")
                                ->select()
                                ->from($Model)
                                ->where('user_id = ?', $this->_currentUser->id)
                                ->where('role_name = ?', $ResourceName)
                                ->limit(1);
                
                $Result = $Model->fetchRow($Select);
                
                $allow = array();
                $deny  = array();
                
                if ($Result == null)
                {
                    $Model ->setTable("users_acl")
                           ->insert(array(
                                'user_id'   => $this->_currentUser->id,
                                'group_id'  => 0,
                                'role_name' => $ResourceName
                           ));
                    
                    $allow = array();
                    $deny  = array(
                        'view',
                        'add',
                        'edit',
                        'drop'
                    );
                }
                else
                {
                    if ($Result->access_view)
                        $allow[] = 'view';
                    else
                        $deny[]  = 'view';
                        
                    if ($Result->access_add)
                        $allow[] = 'add';
                    else
                        $deny[]  = 'add';
                        
                    if ($Result->access_edit)
                        $allow[] = 'edit';
                    else
                        $deny[]  = 'edit';

                    if ($Result->access_drop)
                        $allow[] = 'drop';
                    else
                        $deny[]  = 'drop';
                }
                
                // Применяем привилегии к пользователю не владельцу
                if ($this->_currentUser->is_owner)
                {
                    $this->_checked = true;
                    $Acl ->allow($userRole, $Resource, array(
                        'view', 'add', 'edit', 'drop'
                    ));
                }
                else
                {
                    $Acl ->allow($userRole, $Resource, $allow);
                    $Acl ->deny($userRole, $Resource, $deny);
                }
            
                $this->_acl = $Acl;
            }
        }
    }

    public function acl($userRole = null, $Resource = null, $privileges = null)
    {
        $deny = false;
        $this->_checked = true;
        
        if ($userRole != null && $Resource == null && $privileges == null)
        {
            $deny = !$this->_acl
                          ->isAllowed($this->_currentUser->email, 
                                      get_class($this->getActionController()), 
                                      $userRole);
        }
        elseif ($userRole != null && $Resource != null && $privileges == null)
        {
            $deny = !$this->_acl
                          ->isAllowed($this->_currentUser->email, 
                                      $Resource, 
                                      $userRole);
        }
        else
        {
            $deny = !$this->_acl
                          ->isAllowed($userRole, $Resource, $privileges);
        }
        
        if ($deny && $this->_currentUser)
        {
            new Fenix_Exception(Fenix::lang("Доступ запрещён"));
        }
    }
    
    public function denyAll()
    {
        // Проверка прав доступа к бекенду
        if ($this->getRequest()->getAccessLevel() == "backend")
            if (!$this->_checked && $this->_currentUser)
                new Fenix_Exception(Fenix::lang("Доступ запрещён"));
    }
}