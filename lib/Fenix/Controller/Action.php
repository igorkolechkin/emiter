<?php

class Fenix_Controller_Action extends Zend_Controller_Action
{
    protected $serviceLanguage;

    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array()
    ) {
        parent::__construct($request, $response, $invokeArgs);
        // Отключение авторендеринга
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $this->serviceLanguage = Fenix::getHelper('core/service_language');
    }

    public function __call($methodName, $args)
    {
        $this->indexAction();
    }


    /**
     * Обработка заголовка If-Modified-Since
     */
    function setLastModify($page = null)
    {
        if ( ! $page) {
            return false;
        }

        if (is_int($page)) {
            $last_modify_timestamp = $page;
        } elseif (is_string($page)) {
            if (ctype_digit($page)) {
                $last_modify_timestamp = (int)$page;
            } else {
                $last_modify_timestamp = strtotime($page);
            }
        } elseif (is_object($page)) {
            if ($page->modify_date != null && strtotime($page->modify_date) > 0) {
                $last_modify_timestamp = strtotime($page->modify_date);
            } elseif ($page->create_date != null && strtotime($page->create_date) > 0) {
                $last_modify_timestamp = strtotime($page->create_date);
            } else {
                return false;
            }
        } elseif (is_array($page)) {
            if (isset($page['modify_date']) && strtotime($page['modify_date']) > 0) {
                $last_modify_timestamp = strtotime($page['modify_date']);
            } elseif (isset($page['create_date']) && strtotime($page['create_date']) > 0) {
                $last_modify_timestamp = strtotime($page['create_date']);
            } else {
                return false;
            }
        } else {
            return false;
        }

        if ($last_modify_timestamp > 0) {
            $IfModifiedSince = false;
            if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
                $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
            }
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
            }

            if ($IfModifiedSince && $IfModifiedSince >= $last_modify_timestamp) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                exit;
            }
            header("Cache-Control: ", true);
            header("Expires: " . gmdate("D, d M Y H:i:s", time() + 1 * 60 * 60) . " GMT", true);
            header("Last-Modified: " . gmdate("D, d M Y H:i:s", $last_modify_timestamp) . " GMT", true);
        }

        return false;
    }


    /**
     *  Метод возвращает список Мета данных для метатегов страницы
     *
     * @param $currentPage Object | array
     *                     IF Object - is required methods:
     *                     getUrl() - returning correct URL for page
     *                     getSeo() - returning correct SEO information about this page
     *
     *          IF Array - required structure:
     *                  array(
     *                        'url'         => (string) 'Page URL',
     *                        'title'       => (string) 'Page title',
     *                        'keywords'    => (string) 'Page keywords',
     *                        'description' => (string) 'Page description',
     *
     *                        'seo' => NOT Required -> Object OR Array -> params: title, keywords, description
     *                  )
     *
     * @param $paginator   Zend_Paginator | null
     *
     * @return Fenix_Object_Rowset
     */
    public function getMeta($currentPage, Zend_Paginator $paginator = null)
    {
        /** Массив meta-полей */
        $metaArr = array(
            'title'       => '',
            'keywords'    => '',
            'description' => '',
            'robots'      => '',
            'canonical'   => '',
            'next'        => '',
            'prev'        => '',
            'options'     => '', //доп массив для сохранения результатов работы
        );

        /** Получаем необходимые данные о странице */
        if (is_object($currentPage)) {

            $currentUrl = $currentPage->getUrl();

        } elseif (is_array($currentPage) && isset($currentPage['url']) && ! empty($currentPage['url'])) {

            $currentUrl = $currentPage['url'];

        } else {
            $currentUrl = Fenix::getUrl('/');
        }
        $metaArr['options']['currentUrl'] = $currentUrl;

        // Кол-во записей на страницу
        $perPage = Fenix::getRequest()->getQuery('perPage') ? Fenix::getRequest()->getQuery('perPage') : Fenix::getRequest()->getQuery('perpage');
        $metaArr['options']['perPage'] = $perPage;

        // Номер текущей страницы
        $currentPageNumber = Fenix::getRequest()->getParam('page');
        $metaArr['options']['currentPageNumber'] = $currentPageNumber;

        // используется ли сортировка
        $sort = Fenix::getRequest()->getParam('sort');
        $metaArr['options']['sort'] = $sort;

        // присутствуют ли какие либо $_GET параметры
        $getParams = Fenix::getRequest()->getQuery();
        //удаляем GET параметры для которых свои robots
        unset($getParams['perPage']);
        unset($getParams['perpage']);
        unset($getParams['page']);
        unset($getParams['sort']);
        $isGetParams = ! empty($getParams);
        $metaArr['options']['isGetParams'] = $isGetParams;


        // META тег Title
        // META тег Keywords
        // META тег Description
        if (is_object($currentPage)) {

            $metaArr['title'] = $currentPage->getSeo()->getTitle();
            $metaArr['keywords'] = $currentPage->getSeo()->getKeywords();
            $metaArr['description'] = $currentPage->getSeo()->getDescription();

        } elseif (is_array($currentPage) && isset($currentPage['seo'])) {

            if (is_object($currentPage['seo'])) {

                $metaArr['title'] = $currentPage['seo']->getTitle();
                $metaArr['keywords'] = $currentPage['seo']->getKeywords();
                $metaArr['description'] = $currentPage['seo']->getDescription();

            } elseif (is_array($currentPage['seo']) && isset($currentPage['seo']['title'])) {

                $metaArr['title'] = $currentPage['seo']['title'];
                $metaArr['keywords'] = $currentPage['seo']['keywords'];
                $metaArr['description'] = $currentPage['seo']['description'];

            }

        } elseif (is_array($currentPage) && isset($currentPage['title'])) {

            $metaArr['title'] = (isset($currentPage['title'])) ? $currentPage['title'] : '';
            $metaArr['keywords'] = (isset($currentPage['keywords'])) ? $currentPage['keywords'] : '';
            $metaArr['description'] = (isset($currentPage['description'])) ? $currentPage['description'] : '';

        }


        //Добавление номера страница в мета данные
        if ($currentPageNumber != null && $currentPageNumber > 1) {
            $prefixMeta = Fenix::lang('Страница') . ' №' . $currentPageNumber . ' - ';

            if (isset($metaArr['title'])) {
                $metaArr['title'] = $prefixMeta . $metaArr['title'];
            }
            if (isset($metaArr['keywords'])) {
                $metaArr['keywords'] = $prefixMeta . $metaArr['keywords'];
            }
            if (isset($metaArr['description'])) {
                $metaArr['description'] = $prefixMeta . $metaArr['description'];
            }
        }


        // META тег ROBOTS
        $page_robots[] = 'index, follow';// по умолчанию

        // НЕ индексировать страницу при доп GET параметрах кроме
        // perPage - кол-во записей на страницу
        // page - пагинатор
        // sotr - сортировка
        if ($isGetParams) {
            $page_robots[] = 'noindex, nofollow';
        }

        // robots для пагинатора
        if ( ! empty($currentPageNumber) && (int)$currentPageNumber > 1) {
            $page_robots[] = 'index, follow';
        }

        // robots для сортировки
        if ($sort) {
            $page_robots[] = 'noindex, follow';
        }

        // robots для сортировки
        if ($perPage) {
            $page_robots[] = 'noindex, follow';
        }

        // robots для страниц с другой валютой
        //			if(isset($_SESSION['user_currency']) && $_SESSION['user_currency'] != Fenix::getHelper('core/currency')->getDefaultCurrency()->code) {
        //				$page_robots[] = 'noindex, nofollow';
        //			}

        // Получаем самый приоритетный robots
        $page_robots = array_unique($page_robots);
        sort($page_robots);
        $page_robots = array_pop($page_robots);

        $metaArr['robots'] = $page_robots;


        // META тег rel="canonical"
        $metaArr['canonical'] = $currentUrl;


        // META тег rel="next" - Следующая
        // META тег rel="prev" - Предидущая
        if ($paginator != null) {
            $url = Fenix::getRequest()->getPathInfo();//берем не $currentUrl, чтоб парвильно обработать /filter/
            // получаем все текущие GET параметры
            $query = Fenix::getRequest()->getQuery();

            $query['page'] = '%s'; // добавляем/обновляем свой GET параметр page для пагинатора в виде маски для sprintf
            $query_str = Fenix::getQueryString($query); //получаем строку GET параметров

            $urlMask = $url . $query_str; // строим маску для урла пагинации

            $currentPageNumber = ($currentPageNumber) ? (int)$currentPageNumber : 1;

            //next
            if (($currentPageNumber * $paginator->getItemCountPerPage() + 1) <= $paginator->getTotalItemCount()) {
                $metaArr['next'] = sprintf($urlMask, ($currentPageNumber + 1));
            }

            //prev
            if ($currentPageNumber > 2) {
                $metaArr['prev'] = sprintf($urlMask, ($currentPageNumber - 1));
            } elseif ($currentPageNumber == 2) {
                unset($query['page']);
                $query_str = Fenix::getQueryString($query); //получаем строку GET параметров
                $metaArr['prev'] = $url . $query_str;
            }
        }

        return new Fenix_Object_Rowset(array('data' => $metaArr));
    }

    /**
     *  Метод устанавливает метаинформацию на странице
     *
     * @param $Creator
     *
     * @param $currentPage Object | array
     *                     IF Object - is required methods:
     *                     getUrl() - returning correct URL for page
     *                     getSeo() - returning correct SEO information about this page
     *
     *                     IF Array - required structure:
     *                     array(
     *                        'url'         => (string) 'Page URL',
     *                        'title'       => (string) 'Page title',
     *                        'keywords'    => (string) 'Page keywords',
     *                        'description' => (string) 'Page description',
     *
     *                        'seo' => NOT Required -> Object | Array -> params: title, keywords, description
     *                     )
     *
     * @param $paginator   Zend_Paginator | null
     *
     * @return true
     */
    public function setMeta(&$Creator, $currentPage, Zend_Paginator $paginator = null)
    {
        $this->setLastModify($currentPage); // заголовок If-Modified-Since
        $metaData = $this->getMeta($currentPage, $paginator)->getData();

        foreach ($metaData as $metaName => $metaValue) {
            if ( ! empty($metaValue)) {
                switch ($metaName) {
                    case 'title':
                        $Creator->getView()->headTitle($metaValue);
                        break;
                    case 'canonical':
                        $Creator->getView()->headLink(array(
                            'rel'  => 'canonical',
                            'href' => $metaValue,
                        ));
                        break;
                    case 'next':
                        $Creator->getView()->headLink(array(
                            'rel'  => 'next',
                            'href' => $metaValue,
                        ));
                        break;
                    case 'prev':
                        $Creator->getView()->headLink(array(
                            'rel'  => 'prev',
                            'href' => $metaValue,
                        ));
                        break;
                    case 'options':
                        break;
                    default:
                        $Creator->getView()->headMeta()->setName($metaName, $metaValue);
                        break;
                }
            }
        }

        return true;
    }
}