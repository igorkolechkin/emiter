<?php
class Fenix_Controller_Dispatcher extends Zend_Controller_Dispatcher_Standard
{
    /**
     * Процесс диспетчеризации контроллера
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @return array|false
     */
    private function _processDispatch(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response)
    {
        $pathInfo  = $request->preparePathinfo();
        $classList = $this->_prepareClassList($pathInfo);
        $acp       = ($pathInfo->isAcp ? '_Admin' : null);

        foreach ($classList AS $i => $_class) {
            // Первая попытка
            if ($this->_loadClass($_class['first']['local']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['first']['local'];
            }
            // Вторая попытка
            elseif ($this->_loadClass($_class['first']['fenix']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['first']['fenix'];
            }
            // Третья попытка
            elseif ($this->_loadClass($_class['second']['local']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['second']['local'];
            }
            // Четвёртая попытка
            elseif ($this->_loadClass($_class['second']['fenix']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['second']['fenix'];
            }
            // Пятая попытка
            elseif ($this->_loadClass($_class['third']['local']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['third']['local'];
            }
            // Шестая попытка
            elseif ($this->_loadClass($_class['third']['fenix']['class'])) {
                $this->parseParams($request, $pathInfo, $i);
                return $_class['third']['fenix'];
            }
        }

        $finalTest = array(
            'local' => array(
                'class'  => 'Local_Default_Controller' . $acp . '_Index',
                'action' => 'index'
            ),
            'fenix' => array(
                'class'  => 'Fenix_Default_Controller' . $acp . '_Index',
                'action' => 'index'
            )
        );

        if ($this->_loadClass($finalTest['local']['class']))
            return $finalTest['local'];

        if ($this->_loadClass($finalTest['fenix']['class']))
            return $finalTest['fenix'];

        return false;
    }

    public function parseParams($request, $pathInfo, $j)
    {
        if ($j == 1) {
            $j = $j + 1;
        }

        $params  = array();
        $_params = array();

        for ($counter = 1, $i = ($pathInfo->sizeof - 1); $i >= 0; $i--, $counter++) {
            $_params[] = $pathInfo->segments[$i];
            if ($counter == $j) {
                break;
            }
        }

        $_params = array_reverse($_params);

        for ($i = 1; $i < sizeof($_params); $i = $i + 2) {
            $params[$_params[($i - 1)]] = $_params[$i];
        }

        $request->setParams($params);
    }

    /**
     * Проверка на существование класса
     *
     * @param string $className Название класса
     * @return bool
     */
    private function _loadClass($className)
    {
        try {
            Zend_Loader::loadClass($className);
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * Подготовка списка классов для тестирования
     *
     * @param stdClass $pathInfo Подготовленный пасинфо
     * @return array
     */
    private function _prepareClassList($pathInfo)
    {
        $classList = array();

        $acp = ($pathInfo->isAcp ? '_Admin' : null);

        if ($pathInfo->sizeof == 1) {

            $module = $pathInfo->segments[0];

            $classList[0]['first'] = array(
                'local' => array(
                    'class'  => 'Local_' . ucfirst($module) . '_Controller' . $acp . '_Index',
                    'action' => 'index'
                ),
                'fenix' => array(
                    'class'  => 'Fenix_' . ucfirst($module) . '_Controller' . $acp . '_Index',
                    'action' => 'index'
                )
            );

            $classList[0]['second'] = array(
                'local' => array(
                    'class'  => 'Local_Default_Controller' . $acp . '_Index',
                    'action' => 'index'
                ),
                'fenix' => array(
                    'class'  => 'Fenix_Default_Controller' . $acp . '_Index',
                    'action' => $module
                )
            );
        }
        else {
            for ($i = ($pathInfo->sizeof), $j = 0; $i >= 0; $i--, $j++) {

                $tmpSegments = array();

                if ($i == 0)
                    break;

                $tmpSegments = $tmpSegmentsSecond
                    = $tmpSegmentsThird
                    = array_slice($pathInfo->segments, 0, $i);

                if ($i == 1)
                    $tmpSegments[] = 'index';

                // Первая попытка
                $controllerClass = $this->_prepareClassName($tmpSegments, $acp);
                $classList[$j]['first'] = array(
                    'local' => array(
                        'class'  => 'Local_' . $controllerClass,
                        'action' => 'index'
                    ),
                    'fenix' => array(
                        'class'  => 'Fenix_' . $controllerClass,
                        'action' => 'index'
                    )
                );

                // Вторая попытка
                $action = array_pop($tmpSegmentsSecond);
                $tmpSegmentsSecond[] = 'index';

                if ($i == 1)
                    array_unshift($tmpSegmentsSecond, 'default');

                $controllerClass = $this->_prepareClassName($tmpSegmentsSecond, $acp);
                $classList[$j]['second'] = array(
                    'local' => array(
                        'class'  => 'Local_' . $controllerClass,
                        'action' => $action
                    ),
                    'fenix' => array(
                        'class'  => 'Fenix_' . $controllerClass,
                        'action' => $action
                    )
                );

                // Третья попытка
                $action = array_pop($tmpSegmentsThird);

                if ($i == 1)
                    array_unshift($tmpSegmentsThird, 'default');

                $controllerClass = $this->_prepareClassName($tmpSegmentsThird, $acp);
                $classList[$j]['third'] = array(
                    'local' => array(
                        'class'  => 'Local_' . $controllerClass,
                        'action' => $action
                    ),
                    'fenix' => array(
                        'class'  => 'Fenix_' . $controllerClass,
                        'action' => $action
                    )
                );
            }
        }

        return $classList;
    }

    /**
     * Подготовка имени класса
     *
     * @param array $segments Сегменты URL
     * @return string
     */
    private function _prepareClassName(array $segments, $acp)
    {
        $first = array_shift($segments);

        array_unshift($segments, 'Controller' . $acp);
        array_unshift($segments, $first);

        $segments = array_map(function($value){
            return ucfirst($value);
        }, $segments);

        return implode('_' , $segments);
    }

    /**
     * Диспетчер контроллера
     *
     * Тут мы переопределяем стандартный диспетчер под наши нужды
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @return void
     * @throws Zend_Controller_Dispatcher_Exception
     */
    public function dispatch(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response)
    {
        $options = $this->_processDispatch($request, $response);

        $className = $options['class'];
        $action    = $options['action'] . 'Action';

        /**
         * Instantiate controller with request, response, and invocation
         * arguments; throw exception if it's not an action controller
         */
        $controller = new $className($request, $this->getResponse(), $this->getParams());
        if (!($controller instanceof Zend_Controller_Action_Interface) &&
            !($controller instanceof Zend_Controller_Action)) {
            require_once 'Zend/Controller/Dispatcher/Exception.php';
            throw new Zend_Controller_Dispatcher_Exception(
                'Controller "' . $className . '" is not an instance of Zend_Controller_Action_Interface'
            );
        }

        /**
         * Dispatch the method call
         */
        $request->setDispatched(true);

        // by default, buffer output
        $disableOb = $this->getParam('disableOutputBuffering');
        $obLevel   = ob_get_level();
        if (empty($disableOb)) {
            ob_start();
        }

        try {
            $controller->dispatch($action);
        } catch (Exception $e) {
            // Clean output buffer on error
            $curObLevel = ob_get_level();
            if ($curObLevel > $obLevel) {
                do {
                    ob_get_clean();
                    $curObLevel = ob_get_level();
                } while ($curObLevel > $obLevel);
            }
            throw $e;
        }

        if (empty($disableOb)) {
            $content = ob_get_clean();
            $response->appendBody($content);
        }

        // Destroy the page controller instance and reflection objects
        unset($controller);
    }
}