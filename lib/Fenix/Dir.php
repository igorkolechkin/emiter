<?php
class Fenix_Dir
{
	private $_current_dir	= null;
	private $_dir_list		= array();
	private $_files_list	= array();
	private $_listing_dir	= array();
    
    public function __construct($dir = null)
    {
        if ($dir != null)
            $this->index($dir);
    }
    
	public function index($dir)
	{
		$this->checkDir($dir);
		unset($dir);
		
		// Сканируем директорию
		$dir_stream=opendir($this->_current_dir);
		
		while($dir = readdir($dir_stream)) {
			if ($dir != "." && $dir != "..") {
				if (is_dir($this->_current_dir . $dir)) {
					$this->_dir_list[(int) $dir_stream][] =
							  array('name'			=> $dir,
									'create'		=> filectime($this->_current_dir.$dir),
									'modify'		=> filemtime($this->_current_dir.$dir),
									'chmod'			=> substr(decoct(fileperms($this->_current_dir.$dir)), 1),
									'path'			=> $this->_current_dir.$dir,
									'realpath'		=> realpath($this->_current_dir.$dir),
									'is_readable'	=> is_readable($this->_current_dir.$dir),
									'is_writeable'	=> is_writeable($this->_current_dir.$dir));
				}
				else {
					$this->_files_list[(int) $dir_stream][] =
							  array('filename'		=> $dir,
									'path'			=> $this->_current_dir.$dir,
									'realpath'		=> realpath($this->_current_dir.$dir),
									'filetype'		=> filetype($this->_current_dir.$dir),
									'chmod'			=> substr(decoct(fileperms($this->_current_dir.$dir)), 2),
									'is_readable'	=> is_readable($this->_current_dir.$dir),
									'is_writeable'	=> is_writeable($this->_current_dir.$dir),
									'is_executable'	=> is_executable($this->_current_dir.$dir),
									'fileatime'		=> fileatime($this->_current_dir.$dir),
									'filemtime'		=> filemtime($this->_current_dir.$dir),
									'filectime'		=> filectime($this->_current_dir.$dir),
									'filesize'		=> filesize($this->_current_dir.$dir),
									'fileperms'		=> substr(sprintf('%o', fileperms($this->_current_dir.$dir)), -4));
				}
			}
		}
		
		closedir($dir_stream);
		
		return $dir_stream;
	}
    
	public function getDirList($stream)
	{
		return isset($this->_dir_list[(int) $stream]) ? $this->_dir_list[(int) $stream] : false;
	}
	
	public function getFilesList($stream)
	{
		return isset($this->_files_list[(int) $stream]) ? $this->_files_list[(int) $stream] : false;
	}
	
	public function clean($stream)
	{
		unset($this->_dir_list[(int) $stream]);
		unset($this->_files_list[(int) $stream]);
	}

    public function removeDirectory($dir, $deleteRoot = false)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object)) {
                        $this->removeDirectory($dir . "/" . $object); 
                    } 
                    else { 
                        if (file_exists($dir . "/" . $object))
                            unlink($dir . "/" . $object);
                    }
                }
            }
            
            if ($deleteRoot === false)
                rmdir($dir);
            
            unset($objects);
        }
    }     
    
	private function checkDir($dir)
	{
        try {
    		if (!is_dir($dir)) {
    			throw new Exception("Директория <strong>&laquo;".$dir."&raquo;</strong> не существует");
    		}
    		else {
    			$this->_current_dir = $dir;
    			if (substr($this->_current_dir, '-1') != "/") {
    				$this->_current_dir = $this->_current_dir.'/';
    			}
    		}
        }
        catch (Exception $e)  {
            new Fenix_Exception($e);
        }
	}
}