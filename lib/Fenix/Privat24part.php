<?php

class Fenix_Api_Privat24part
{
    const STORE_ID             = '684748967B3442299293';
    const STORE_PASSWORD       = '30c7524ce15d4fcd9f7a09129f0fd173';
    const MERCHANT_TYPE_PARTS  = 'PP';
    const MERCHANT_TYPE_CREDIT = 'II';

    const CURRENCY_UAH = 980;
    const CURRENCY_USD = 840;
    const CURRENCY_RUS = 643;

    const DEFAULT_PARTS_COUNT = 2;

    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAIL    = 'FAIL';

    const ORDER_STATUS_DELIVERED               = 'DELIVERED';
    const ORDER_STATUS_IN_PROGRESS             = 'IN_PROGRESS';
    const ORDER_STATUS_CANCELLED               = 'CANCELLED';
    const ORDER_STATUS_ABANDONED_SHOPPING_CART = 'ABANDONED_SHOPPING_CART';
    const ORDER_STATUS_INITIALIZED             = 'INITIALIZED';

    const PAYMENT_URL = 'https://payparts2.privatbank.ua/ipp/v2/payment?token=';

    private $_order = null;

    private $_storeId;
    private $_orderId;
    private $_amount;
    private $_currency;
    private $_partsCount;
    private $_merchantType;
    private $_responseUrl;
    private $_redirectUrl;
    private $_products_string;
    private $_password;

    private $_xml;

    public function __construct($order, $merchantType)
    {

        $partsCount = self::DEFAULT_PARTS_COUNT;

        if ($order->pay_parts > $partsCount) {
            $partsCount = $order->pay_parts;
        }

        $this->_order = $order;

        $this->_storeId = self::STORE_ID;
        $this->_orderId = $this->_order->getId() . '-' . $this->_order->getTokenCount();
        $this->_amount = sprintf("%.2f", $this->_order->getTotalPrice() + $this->_order->getDeliveryPrice());
        $this->_currency = Fenix_Api_Privat24part::CURRENCY_UAH;
        $this->_partsCount = $partsCount;
        $this->_merchantType = $merchantType;
        $this->_responseUrl = Fenix::getUrl('checkout/privat24part/server');
        $this->_redirectUrl = Fenix::getUrl('checkout/privat24part/return');
        $this->_products_string = '';
        $this->_password = self::STORE_PASSWORD;
//        /Fenix::dump($this);
    }

    public function getSignature($amount)
    {

        $signature = base64_encode($this->hextobin(sha1(
            $this->_password
            . $this->_storeId
            . $this->_orderId
            . str_replace(array(',', '.'), '', sprintf("%.2f", $amount))
            . $this->_currency
            . $this->_partsCount
            . $this->_merchantType
            . $this->_responseUrl
            . $this->_redirectUrl
            . $this->_products_string
            . $this->_password
        )));

        return $signature;
    }

    public function getPaymentUrl($respondXML)
    {
        return Fenix_Api_Privat24part::PAYMENT_URL . $respondXML->token;
    }

    public function getPaymentUrlByToken($token)
    {
        return Fenix_Api_Privat24part::PAYMENT_URL . $token;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $storeId = self::STORE_ID;


        //$discount = $this->_order->getDiscountAmount();
        //$totalOrder = $this->_order->getTotalOrderPrice() + $discount;
        $partsCount = 2;


        $t = "\t";
        $nl = "\n";
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . $nl;
        $xml .= '<payment>' . $nl;
        $xml .= $t . '<storeId>' . $this->_storeId . '</storeId>' . $nl;
        $xml .= $t . '<orderId>' . $this->_orderId . '</orderId>' . $nl;


        $count = 0;

        $xmlProducts = '';
        $amount = 0;
        foreach ($this->_order->getProducts() as $_product) {
            $count++;
            $price = $_product->getCpPrice();
            $product_title = trim($_product->getTitle());

            //$unitPrice = $price * $_product->getOrderQty();
            //$percent   = round($unitPrice / $totalOrder, 1);
            /*Fenix::dump($price, $unitPrice, $percent);
            if($count != $this->_order->getProducts()->count()){
                //Fenix::dump($discount, $percent, $discount * $percent);
                $discountPerProduct =  round($discount * $percent, 0) / $_product->getOrderQty();
                //Fenix::dump($discountPerProduct);
                $price    = $price - $discountPerProduct ;
                $discount = $discount - $discountPerProduct;
            }else{
                //Fenix::dump($price, $discount);
                $price     = $price - $discount/$_product->getOrderQty();
            }*/
            if ($this->_partsCount == 2) {
                $price = round($price * (float) Fenix::getConfig('pay_parts/part2'));
            }
            if ($this->_partsCount == 3) {
                $price = round($price * (float) Fenix::getConfig('pay_parts/part3'));
            }
            if ($this->_partsCount == 4) {
                $price = round($price * (float) Fenix::getConfig('pay_parts/part4'));
            }
            if ($this->_partsCount == 5) {
                $price = round($price * (float) Fenix::getConfig('pay_parts/part5'));
            }
            if ($this->_partsCount == 6) {
                $price = round($price * (float) Fenix::getConfig('pay_parts/part6'));
            }
            $amount = $amount + $price * $_product->getCpQty();

            $xmlProducts .= $t . $t . '<products>' . $nl;
            $xmlProducts .= $t . $t . $t . '<name>' . $product_title . '</name>' . $nl;
            $xmlProducts .= $t . $t . $t . '<count>' . $_product->getCpQty() . '</count>' . $nl;
            $xmlProducts .= $t . $t . $t . '<price>' . sprintf("%.2f", $price) . '</price>' . $nl;
            $xmlProducts .= $t . $t . '</products>' . $nl;

            $this->_products_string .= ($product_title . $_product->getCpQty() . str_replace('.', '', sprintf("%.2f", $price)));
        }
        if ($this->_order->getDeliveryPrice() > 0) {
            $amount = $amount + $this->_order->getDeliveryPrice();
            $xmlProducts .= $t . $t . '<products>' . $nl;
            $xmlProducts .= $t . $t . $t . '<name>Доставка</name>' . $nl;
            $xmlProducts .= $t . $t . $t . '<count>1</count>' . $nl;
            $xmlProducts .= $t . $t . $t . '<price>' . sprintf("%.2f", $this->_order->getDeliveryPrice()) . '</price>' . $nl;
            $xmlProducts .= $t . $t . '</products>' . $nl;
            $this->_products_string .= ('Доставка' . '1' . str_replace('.', '', sprintf("%.2f", $this->_order->getDeliveryPrice())));
        }


        $xml .= $t . '<amount>' . sprintf("%.2f", $amount) . '</amount>' . $nl;
        $xml .= $t . '<currency>' . $this->_currency . '</currency>' . $nl;
        $xml .= $t . '<partsCount>' . $this->_partsCount . '</partsCount>' . $nl;
        $xml .= $t . '<merchantType>' . $this->_merchantType . '</merchantType>' . $nl;
        $xml .= $xmlProducts;
        $xml .= $t . '<responseUrl>' . $this->_responseUrl . '</responseUrl>' . $nl;
        $xml .= $t . '<redirectUrl>' . $this->_redirectUrl . '</redirectUrl>' . $nl;
        $xml .= $t . '<signature>' . $this->getSignature($amount) . '</signature>' . $nl;
        $xml .= '</payment>' . $nl;

        if (Fenix::isDev()) {
            //Fenix::dump($xml,$this->_products_string);
        }

        $this->xml = $xml;

        return $xml;
    }

    /**
     * @return Zend_Config_Xml
     */
    public function sendOrderInfo()
    {
        $url = 'https://payparts2.privatbank.ua/ipp/v2/payment/create';
        $xml = $this->getXml();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml'));
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $password);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if (Fenix::isDev()) {

            //Fenix::dump($xml,$this->_products_string,$output);
        }

        $respondXml = new Zend_Config_Xml($output);

        return $respondXml;
    }

    public function checkOrderRespondSignature($respondXml)
    {

        $mustBe = base64_encode($this->hextobin(sha1(
            $this->_password
            . $respondXml->state
            . $this->_storeId
            . $respondXml->message
            . $respondXml->token
            . $this->_orderId
            . $this->_password
        )));

        return $mustBe == $respondXml->signature;
    }

    public function checkSignature($signature)
    {

        $mustBe = base64_encode($this->hextobin(sha1(
            $this->_password
            . $this->_storeId
            . $this->_orderId
            . $this->_password
        )));

        return $mustBe == $signature;
    }

    private function hextobin($hexstr)
    {
        $n = strlen($hexstr);
        $sbin = "";
        $i = 0;
        while ($i < $n) {
            $a = substr($hexstr, $i, 2);
            $c = pack("H*", $a);
            if ($i == 0) {
                $sbin = $c;
            }
            else {
                $sbin .= $c;
            }
            $i += 2;
        }

        return $sbin;
    }

}