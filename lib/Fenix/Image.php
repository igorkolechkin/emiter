<?php
class Fenix_Image
{
    static public function setAdapter($adapter)
    {
        if (Zend_Registry::isRegistered('Fenix_Image_Adapter'))
            return Zend_Registry::isRegistered('Fenix_Image_Adapter');
    
        $adapter = 'Fenix_Image_Adapter_' . ucfirst($adapter);
        Zend_Registry::set('Fenix_Image_Adapter', new $adapter());
    }
    
    static public function getAdapter()
    {
        if (Zend_Registry::isRegistered('Fenix_Image_Adapter'))
            return Zend_Registry::isRegistered('Fenix_Image_Adapter');
        
        return new Fenix_Image_Adapter_Asido();
    }
    
    static function resize($image, $width = null, $height = null)
    {
        $image = self::getImagePath($image);

        return self::getAdapter()->resize($image, $width, $height);
    }
    static function resizeWatermark($image, $width = null, $height = null)
    {
        $image = self::getImagePath($image);

        $watermark = Fenix::getModel('core/themes')->getWatermark();
        return self::getAdapter()->resizeWatermark($image, $watermark, $width, $height);
    }
    static function frame($image, $width = null, $height = null, $bg = array(255, 255, 255))
    {
        $image = self::getImagePath($image);

        return self::getAdapter()->frame($image, $width, $height, $bg);
    }
    static function grayscale($image, $width = null, $height = null, $bg = array(255, 255, 255))
    {
        $image = self::getImagePath($image);

        return self::getAdapter()->grayscale($image, $width, $height, $bg);
    }
    static function adapt($image, $width = null, $height = null, $watermark = false)
    {
        $image = self::getImagePath($image);

        return self::getAdapter()->adapt($image, $width, $height, $watermark);
    }
    static function adaptWatermark($image, $width = null, $height = null)
    {
        $image = self::getImagePath($image);

        return self::getAdapter()->adaptWatermark($image, $width, $height);
    }
    static function frameWatermark($image, $width = null, $height = null, $scale = null)
    {
        $image = self::getImagePath($image);

        $watermark = Fenix::getModel('core/themes')->getWatermark();

        return self::getAdapter()->frameWatermark($image, $watermark, $width, $height, null, $scale);
    }

    /**
     *
     * Работа с изображением библиотечкой Intervention
     *
     * Документация тут
     *
     * http://image.intervention.io/getting_started/introduction
     *
     * @param $image
     * @return Fenix_Image_Adapter_Intervention
     */
    static function make($image)
    {
        $image = self::getImagePath($image);

        return new Fenix_Image_Adapter_Intervention($image);
    }

    static function getImagePath($image)
    {

        if (file_exists($image) && !is_dir(HOME_DIR_ABSOLUTE . $image)) {
            return $image;
        } elseif (file_exists(HOME_DIR_ABSOLUTE . $image) && !is_dir(HOME_DIR_ABSOLUTE . $image)) {
            return HOME_DIR_ABSOLUTE . $image;
        }
        //TODO: Допилить отображение логотипа вместо No-image после того как СИРИ сделает хранениелого не в BLOB
        
        return VAR_DIR_ABSOLUTE . 'images/file-not-found.png';
    }
}