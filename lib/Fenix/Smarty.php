<?php
class Fenix_Smarty
{
    private $_smarty = null;
    
    /**
     * @staticvar
     * @var Fenix_Engine
     */
    static private $_instance = null;
    
    static function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new Fenix_Smarty();
       
        return self::$_instance;
    }  
    
    public function __construct()
    {
        require_once 'Smarty/Smarty.class.php';
        require_once 'Smarty/sysplugins/smarty_internal_resource_string.php';
        require_once 'Smarty/sysplugins/smarty_internal_templatecompilerbase.php';
        require_once 'Smarty/sysplugins/smarty_internal_compilebase.php';
        require_once 'Smarty/sysplugins/smarty_internal_write_file.php';
        require_once 'Smarty/sysplugins/smarty_internal_templatelexer.php';
        require_once 'Smarty/sysplugins/smarty_internal_templateparser.php';
        require_once 'Smarty/sysplugins/smarty_internal_debug.php';

        $this->_smarty = new Smarty();
        $this->_smarty
             ->debugging = true;
        $this->_smarty
             ->use_sub_dirs = true;
        $this->_smarty->error_reporting = E_ALL;

        $this->_smarty
             //->setTemplateDir(THEMES_DIR . Fenix::getModel('core/themes')->getCurrentTheme()->sys_title . DS . "design")
             ->setCompileDir(TMP_DIR_ABSOLUTE . 'cache/smarty/');
        
        $this->_smarty->assign('request', Fenix::getRequest());
        $this->_smarty->assign('model',   new Fenix_Resource_Model());
        
        $this->_smarty->registerPlugin('function', 'static',    array(__CLASS__, 'callStatic'));
        $this->_smarty->registerPlugin('function', 'helper',    array(__CLASS__, 'getHelper'));
        $this->_smarty->registerPlugin('function', 'getConfig', array(__CLASS__, 'getConfig'));
    }
    
    static public function callStatic($params, $smarty)
    {
        if (isset($params['const']))
        {
            list($class, $const) = explode('::', $params['const']);
            
            $Reflection = new ReflectionObject(new $class());
            return $Reflection->getConstant($const);
        }
    }
    
    static public function getConfig($params, $smarty)
    {
        if (!isset($params['param']))
            return null;
        
        if ($params['param'] == null)
            return null;
            
        return Fenix::getConfig($params['param']);
    }

    static public function getHelper($params, $smarty)
    {
        $type = $params['type'];
        unset($params['type']);
        
        @list($type, $call) = explode(':', $type);
        
        if ($call == null)
            $call = 'getData';
        
        $ObjectHelper = Fenix::getHelper($type);
        
        return $ObjectHelper ->{$call}($params);
    }
    
    public function __call($func, $args)
    {
        return call_user_func_array(
            array(
                $this->_smarty,
                $func
            ),
            $args
        );
    }
    
    public function getSmarty()
    {
        return $this->_smarty;
    }
    
    public function fetchString($string, $escape = false)
    {
        $string = rawurldecode($string);
        $string = str_replace('&quot;', '"', $string);
        $string = str_replace('&gt;', '>', $string);

        if ($escape === true)
            $string = preg_replace('/(\[(.+?)\])/si', '{\2}', $string);

        try
        {
            return $this->_smarty
                        ->fetch('string:' . $string);
        }
        catch (Exception $e) {
            
        }
    }
}