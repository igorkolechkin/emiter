<?php

class Fenix_Language
{

    /**
     * @staticvar
     * @var Fenix_Language
     */
    static private $_instance     = null;
    static private $_current_code = null;

    function __construct()
    {
        if (Fenix::isAJAX() && isset($_SESSION['User']['lang']['current_code'])) {
            self::$_current_code = $_SESSION['User']['lang']['current_code'];
        }

        return true;
    }

    static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new Fenix_Language();
        }

        return self::$_instance;
    }

    public function getDefaultLanguage()
    {
        $default = 'default_site';

        foreach ($this->getLanguagesList() AS $name => $language) {
            if ($language->{$default}) {
                return $language;
            }
        }

        return false;
    }

    public function isCorrectLang($lang)
    {
        $list = $this->getLanguagesList();

        foreach ($list AS $name => $language) {
            if ($language->code == $lang) {
                return true;
            }
        }

        return false;
    }

    public function getDefaultLanguageCode()
    {
        $lang = $this->getDefaultLanguage();

        return $lang->code;
    }

    public function setCurrentLanguageByCode($code)
    {
        if (Zend_Registry::isRegistered('FenixLanguageCurrent')) {
            return Zend_Registry::get('FenixLanguageCurrent');
        }

        $list = $this->getLanguagesList();
        $current = array();

        foreach ($list AS $name => $language) {
            if ($language->code == $code) {
                $current = $language;
            }
        }

        Zend_Registry::set('FenixLanguageCurrent', $current);

        return $current;
    }

    public function getCurrentLanguage()
    {
        $currentLangCode = $this->getCurrentLanguageCode();
        if (($currentLanguage = $this->getLanguageByCode($currentLangCode)) !== false) {
            return $currentLanguage;
        }

        return $this->getDefaultLanguage();
    }

    public function getCurrentLanguageCode()
    {

        if (self::$_current_code != null) {
            return self::$_current_code;
        }

        $request = new Zend_Controller_Request_Http();
        $pathinfo = $request->getPathinfo();
        $list = explode('/', $pathinfo);

        // Тестируем первый элемент массива
        if (isset($list[0]) && $list[0] == null) {
            array_shift($list);
        }


        if ($this->isCorrectLang(@$list[0])) {
            $lang = array_shift($list);
        } else {
            $lang = $this->getDefaultLanguageCode();
        }

        /** Всегда обновляем инфу в сессии - будем опираться на нее при AJAX запросах */
        $_SESSION['User']['lang']['current_code'] = $lang;

        self::$_current_code = $lang;

        return $lang;
    }

    public function getLanguageByCode($code)
    {
        if (Zend_Registry::isRegistered('FenixLanguageCurrent')) {
            return Zend_Registry::get('FenixLanguageCurrent');
        }

        foreach ($this->getLanguagesList() AS $name => $language) {
            if ($language->code == $code) {
                Zend_Registry::set('FenixLanguageCurrent', $language);

                return $language;
            }
        }

        return false;
    }

    static public function getLanguagesList()
    {
        if (Zend_Registry::isRegistered('FenixLanguages')) {
            return Zend_Registry::get('FenixLanguages');
        }

        $languages = new Zend_Config_Xml(ETC_DIR_ABSOLUTE . "language.xml");

        Zend_Registry::set('FenixLanguages', $languages);

        return $languages;
    }

    public function isLanguageExists($name)
    {
        $list = self::getLanguagesList();
        foreach ($list AS $_lang) {
            if ($_lang->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function buildLink($code)
    {
        $request = new Fenix_Controller_Request_Http();

        // Разбиваем строку УРИ на эдементы и чистим её
        $pathInfo = array();
        $_clean = array();
        $pathInfo = explode('/', $request->getPathInfo());

        if (isset($pathInfo[0]) && $pathInfo[0] == null) {
            array_shift($pathInfo);
        }


        if (isset($pathInfo[0]) && $this->isCorrectLang($pathInfo[0])) {
            array_shift($pathInfo);
        }

        if ($this->isCorrectLang($code)) {
            array_unshift($pathInfo, $code);
        }

        /*
                if (isset($pathInfo[0]) && $this->getDefaultLanguageCode() == $pathInfo[0])
                    array_shift($pathInfo);
        */
        //Fenix::dump($pathInfo, $pathInfo[0],$pathInfo,$code,$this->getCurrentLanguage()->code);
        $_url = implode('/', $pathInfo);
        /*
         $_code = $this->getDefaultLanguageCode();

         if(substr($_url, 0, 2) == $_code)
             $_url = substr($_url, 3);*/

        //Fenix::dump($_url,substr($_url, 0, 2));
        $uri = "http://" . $_SERVER['HTTP_HOST'] . '/' . $_url;

        $uri = str_replace('/ru/', '/', $uri);

        $uri = str_replace('/ru', '', $uri);

        return $uri;
    }
}