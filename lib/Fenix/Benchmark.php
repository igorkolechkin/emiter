<?php
class Fenix_Benchmark
{
    private $marker = array();

    public function mark($name)
    {
        $this->marker[$name]=microtime();
    }
    public function elapsedTime($point1='', $point2='', $decimals=4)
    {
        if (!isset($this->marker[$point1])) {
            return '';
        }

        if (!isset($this->marker[$point2])) {
            $this->marker[$point2] = microtime();
        }

        list($sm, $ss) = explode(' ', $this->marker[$point1]);
        list($em, $es) = explode(' ', $this->marker[$point2]);

        return number_format(($em + $es) - ($sm + $ss), $decimals);
    }
    public function get_memory_usage()
    {
        return (!function_exists('memory_get_usage'))
                    ? 'N/A'
                    : memory_get_usage();
    }
}
?>