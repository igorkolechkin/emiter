<?php
class Fenix_View_Helper_Fenix extends Zend_View_Helper_Abstract
{
    private $_fenix = null;
    
    public function __construct()
    {
        $this->_fenix = Fenix::getInstance(); 
    }
    
    public function fenix($call = null)
    {
        if ($call == null)
            return $this->_fenix;
        else
            return $this->_fenix->{$call}();
    }
    
    public function __toString()
    {
        return '';
    }
}