<?php
class Fenix_View_Helper_Session extends Zend_View_Helper_Abstract
{
    public function session($action = 'user')
    {
        switch ($action)
        {
            case 'user':
                return Fenix::getModel("users/session")->current();
            break;
            case 'this':
                return $this;
            break;
        }
    }
}
