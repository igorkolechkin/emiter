<?php
class Fenix_View_Helper_GetVarUri extends Zend_View_Helper_Abstract
{
    private $_langCode = null;
    
    public function __construct()
    {
        $this->_langCode = Fenix_Language::getInstance()->getCurrentLanguageCode();
    }
    
    public function getVarUri($path = null)
    {
       return BASE_URL . VAR_CLEAN_DIR . $path; 
    }
    
    public function __toString()
    {
        return '';
    }
}