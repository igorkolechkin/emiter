<?php
class Fenix_View_Helper_Block extends Zend_View_Helper_Abstract
{
    private $_fenix = null;
    
    public function __construct()
    {
         
    }
    
    public function block($sys_title)
    {
        return Fenix::getModel('core/block')->getBlockBySysTitle($sys_title);
    }
    
    public function __toString()
    {
        return '';
    }
}