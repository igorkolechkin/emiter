<?php
class Fenix_View_Helper_GetUploadImagesUri extends Zend_View_Helper_Abstract
{
    public function __construct() {}
    
    public function getUploadImagesUri($path)
    {
        return UPLOAD_IMAGES_URI . $path;
    }
    
    public function __toString()
    {
        return '';
    }
}