<?php
class Fenix_View_Helper_GetSkinUri extends Zend_View_Helper_Abstract
{
    private $_result = null;
    
    public function __construct()
    {
        $this->_result = THEMES_CLEAN_DIR;
        
        if (Fenix::getRequest()->getAccessLevel() == "backend" || Fenix::getRequest()->getUriSegment(0) == "install")
            $this->_result.= "backend/";
        else
            $this->_result.= Fenix::getModel('core/themes')->getCurrentTheme()->sys_title . '/';
        
        $this->_result.= 'skin/';
    }
    
    public function getSkinUri($path = null, $vars = null)
    {
        return BASE_URL . $this->_result . $path; 
    }
    
    public function __toString()
    {
        return '';
    }
}