<?php
class Fenix_View_Helper_GetUser extends Zend_View_Helper_Abstract
{
    private $_session = null;
    
    public function __construct() {
        $this->_session = Fenix::getHelper('users/current');
    }
    
    public function getUser()
    {
        return $this->_session;
    }
    
    public function __toString()
    {
        return '';
    }
}