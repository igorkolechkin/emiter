<?php
class Fenix_View_Helper_GetConfig extends Zend_View_Helper_Abstract
{
    private $_settingsModel = null;
    
    public function __construct()
    {
        $this->_settingsModel = Fenix::getModel('core/settings'); 
    }
    
    public function getConfig($paramName = null)
    {
        if ($paramName == null)
            return $this->_settingsModel;
        
        return $this->_settingsModel->getParamValueByName($paramName);
    }
    
    public function __toString()
    {
        return '';
    }
}