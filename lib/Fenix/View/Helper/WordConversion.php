<?php
class Fenix_View_Helper_WordConversion extends Zend_View_Helper_Abstract
{
    public function __construct() {}
    
    public function wordConversion($number, $endings)
    {
		$numberLast    = intval(substr(strval($number), -1, 1));
		$numberPreLast = intval(substr(strval($number), -2, 2));
		
		if(($numberLast==0) or ((5<=$numberLast) and ($numberLast<=9)) or((11<=$numberPreLast) and ($numberPreLast<=19))){
		$type=0;
		}elseif(($numberLast==1) and ($numberPreLast!=11)){
		$type=1;
		}elseif((2<=$numberLast) and ($numberLast<=4)){
		$type=2;
		}
		
		return $endings[$type];
    }
    
    public function __toString()
    {
        return '';
    }
}
