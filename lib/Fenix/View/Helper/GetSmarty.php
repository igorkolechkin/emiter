<?php
class Fenix_View_Helper_GetSmarty extends Zend_View_Helper_Abstract
{
    private $_result = null;
    
    public function getSmarty()
    {
        return Zend_Registry::get('Smarty'); 
    }
    
    public function __toString()
    {
        return '';
    }
}