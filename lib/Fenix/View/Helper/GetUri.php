<?php
class Fenix_View_Helper_GetUri extends Zend_View_Helper_Abstract
{
    private $_langCode = null;
    
    public function __construct()
    {
        $this->_langCode = Fenix_Language::getInstance()->getCurrentLanguageCode();
    }
    
    public function getUri($path = null, $vars = null)
    {
        $result = null;
        $path   = ($this->_langCode != null ? $this->_langCode . ($path == null ? '' : '/') : '') . $path;
        $path   = str_replace('//', '/', $path);
        
        if (Fenix::getRequest()->getAccessLevel() == "backend")
            $result.= ACP_NAME . "/";
        
        if ($path != null && is_array($vars))
            foreach ($vars AS $_hash => $_value)
                $path = str_replace('#' . $_hash . '#', $_value, $path);
        
        return BASE_URL . $result . $path; 
    }
    
    public function __toString()
    {
        return '';
    }
}