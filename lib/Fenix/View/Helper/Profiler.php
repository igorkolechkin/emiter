<?php
class Fenix_View_Helper_Profiler extends Zend_View_Helper_Abstract
{
    public function profiler()
    {
        $profiler     = Zend_Registry::get('db')->getProfiler();
        
        $totalTime    = $profiler->getTotalElapsedSecs();
        $queryCount   = $profiler->getTotalNumQueries();
        $longestTime  = 0;
        $longestQuery = null;
         
        foreach ($profiler->getQueryProfiles() as $query) {
            if ($query->getElapsedSecs() > $longestTime) {
                $longestTime  = $query->getElapsedSecs();
                $longestQuery = $query->getQuery();
            }
        }
         
        $result = 'Выполнено <strong>' . $queryCount . '</strong> запросов за <strong>' . $totalTime . '</strong>  секунд' . "<br />\n";
        $result.= 'Среднее время выполнения запроса: <strong>' . $totalTime / $queryCount . '</strong> секунд' . "<br />\n";
        $result.= 'Запросов в секунду: <strong>' . $queryCount / $totalTime . "</strong><br />\n";
        $result.= 'Самое длительное время запроса: <strong>' . $longestTime . "</strong><br />\n";
        $result.= "Самый длительный запрос:<br /> \n<strong>" . $longestQuery . "</strong>\n";
        $result.= "<hr />";

        $query = array();

        foreach ($profiler->getQueryProfiles() AS $q)
            $query[] = '<strong>' . $q->getElapsedSecs() . '</strong><br />' . $q->getQuery();

        $result.= implode('<hr />', $query);

        if (Fenix::config('systemShowProfiler'))
            return new Fenix_Creator_Events('warning', $result);
        
        return null;
    }
}