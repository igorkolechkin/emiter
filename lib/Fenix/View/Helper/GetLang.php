<?php
class Fenix_View_Helper_GetLang extends Zend_View_Helper_Abstract
{
    public function __construct(){}
    
    public function getLang()
    {
        $lang = func_get_args();

        if (!isset($lang[0]))
            throw new Exception("������� �� ������ ��������");
            
        $lang[0] = Zend_Registry::get('Zend_Translate')
                        ->_($lang[0]);
        
        return call_user_func_array("sprintf", $lang);
    }
    
    public function __toString()
    {
        return '';
    }
}