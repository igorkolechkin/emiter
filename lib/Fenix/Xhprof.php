<?php
	
	/**
	 *  Модуль для профилирования приложения
	 */
	class Fenix_Xhprof {
		private        $_debugMsg = '';
		static private $_instance = null;

		static function getInstance() {
			if(self::$_instance == null) {
				self::$_instance = new Fenix_Xhprof();
			}

			return self::$_instance;
		}
		
		/** Старт сбора статистики */
		static public function start() {
			$is_debug = (isset($_REQUEST['debug'])) ? true : false;

			if(Fenix::getStaticConfig()->settings->enable_xhprof != '1' || strpos($_SERVER['REQUEST_URI'], '/acp/xhprof') === 0) {
				return false;
			}

			/** Ниже описаны условия, при каких нужно собирать статистику
			 *  данные условия можно изменять для конкретного проекта
			 */

			/** Собираем статистику только в пиковые часы нагрузки */
			$date = getdate();
			$hours = array(11, 12, 15, 17, 19, 20, 21);

			if($is_debug || Fenix::isDev() || in_array($date['hours'], $hours)) {
				/** Для разгрузки талицы на продакшине берем только один из десяти запросов */
				if($is_debug || Fenix::isDev() || rand(0, 10) == 7) {
					self::collectStatistics();
				}
			}
			
			return true;
		}
		
		/** Инициализация сбора статистики при помощи Xhprof */
		static public function collectStatistics() {
			if(extension_loaded('xhprof')) {
				
				include BASE_DIR . 'lib/Xhprof/utils/xhprof_runs.php';
				include BASE_DIR . 'lib/Xhprof/utils/xhprof_lib.php';
				xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
				
				register_shutdown_function(array('Fenix_Xhprof', 'endCollect'));
			}
		}
		
		/** Метод срабатывает перед окончанием работы PHP скрипта */
		static public function endCollect() {
			$runtime = microtime(true) - BEGIN_MICROTIME;
			$profiler_namespace = DOMAIN; // namespace приложения
			
			$xhprof_data = xhprof_disable();
			$xhprof_runs = new XHProfRuns_Default();
			$run_id = $xhprof_runs->save_run($xhprof_data, $profiler_namespace);
			
			//Замените относительный путь к залинкованной папке
			$profiler_url = PROTOCOL . DOMAIN . sprintf('/var/xhprof/?run=%s&source=%s', $run_id, $profiler_namespace);

			$user = Fenix::getModel('session/auth')->getUser();
			$user_id = ($user != null && is_object($user)) ? $user->id : 0;

			$browser = new Fenix_Browser();

			$request_data = Zend_Json::encode(array(
				'POST'   => $_POST,
				'GET'    => $_GET,
				'FILES'  => $_FILES,
				'COOKIE' => $_COOKIE,
			), true);
			$session_data = Zend_Json::encode($_SESSION, true);
			$browser_data = Zend_Json::encode($browser->toArray(), true);

			//			$userAgent = new Zend_Http_UserAgent();
			//			$device    = $userAgent->getDevice();

			$data = array(
				'run_id'             => $run_id,
				'profiler_namespace' => $profiler_namespace,
				'runtime'            => $runtime * 1000, // для записи в микросекудах, чтоб удобно фильтровать в таблице
				'url'                => PROTOCOL . DOMAIN . $_SERVER['REQUEST_URI'],
				'user_id'            => $user_id,
				'create_date'        => new Zend_Db_Expr('NOW()'),
				'server_code'        => http_response_code(),
				'request_data'       => $request_data,
				'session_data'       => $session_data,
				'browser_data'       => $browser_data,
				'debug_data'         => Fenix_Xhprof::getInstance()->getDebugMsg() . Fenix_Debug::fetch(),
			);
			
			
			/** Сохранение данных по статистике в БД */
			$save_result = Fenix::getModel('xhprof/backend_xhprof')->saveStatistic($data);
			
			if(Fenix::isDev() && !Fenix::getRequest()->isXmlHttpRequest()) {
				$footer_html = '';
				
				if($save_result !== false) {
					$footer_html .= '<br>данные по отчету сохранены.';
					$footer_html .= '<br><a href="/acp/xhprof/view/id/' . $save_result . '">Подробности статистики</a>';
				} else {
					$footer_html .= '<br>Возникла ошибка<br>';
				}

				/** Отображаем блок на фронте */
				Fenix::getHelper('xhprof/backend_xhprof')->viewFooterInfo($footer_html);
			}
			
			return $save_result;
		}

		public function setDebugMsg($msg = '') {
			if(Fenix::getStaticConfig()->settings->enable_xhprof != '1' || strpos($_SERVER['REQUEST_URI'], '/acp/xhprof') === 0) {
				$this->_debugMsg = 'XHProf debug disabled!';
			} else {
				$this->_debugMsg .= $msg . '<br><hr><br>';
			}
		}

		public function getDebugMsg() {
			return $this->_debugMsg;
		}
	}