<?php

	/**
	 * Отладка использования ресурсов и затрат времени
	 * Class Fenix_Debug
	 */
	class Fenix_Debug {

		//Вкл/Выкл
		static $enabled = false;

		/**
		 * Получаем объект кеша для сохранения данных
		 * @return bool|Zend_Session_Namespace
		 */
		static function getCache() {
			if(self::$enabled === false) {
				return false;
			}
			$cache = new Zend_Session_Namespace('Fenix_Debug_Cache');

			return $cache;
		}

		/**
		 * Обнуляем данные в кеше
		 */
		static function resetCache() {
			if(self::$enabled === false) {
				return false;
			}
			$cache = self::getCache();
			$cache->start = null;
			$cache->log = array();
		}

		/**
		 * Сохраняем состояние
		 *
		 * @param string $label
		 */
		static function log($label = '&nbsp;') {
			if(self::$enabled === false) {
				return false;
			}
			$cache = self::getCache();
			if($cache->start == null) {
				$cache->start = microtime(true);
			}
			$time = microtime(true) - $cache->start;

			$usage = memory_get_usage();
			$memory = round($usage / 1048576, 2) . " Mb ";
			$memory .= round($usage / 1024, 2) . " Kb ";
			$memory .= round($usage / 1024, 2) . " bytes ";

            $debug = Zend_Controller_Front::getInstance()->getPlugin('ZFDebug_Controller_Plugin_Debug');

            if($debug !== false){
                $logger = $debug->getPlugin('log');
                $logger->mark($label, true);
            }

			$cache->log[] =
				'<div class="debug-log">' .
				($label ? '<div class="debug-label">' . $label . '</div>' : '') .
				'<div class="debug-time">Time: ' . sprintf('%.5F сек.', $time) . '</div>' .
				'<div class="debug-memory">Mem usage is: ' . $memory . '</div>' .
				'<div class="debug-separator"></div>' .
				'</div>';
		}

		/**
		 * Сохраняем пиковые данные
		 */
		static function logPeak() {
			if(self::$enabled === false) {
				return false;
			}
			$cache = self::getCache();

			if($cache->start == null) {
				$cache->start = microtime(true);
			}
			$time = microtime(true) - $cache->start;

			$usage = memory_get_peak_usage(true);
			$memory = round($usage / 1048576, 2) . " Mb ";
			$memory .= round($usage / 1024, 2) . " Kb ";
			$memory .= round($usage / 1024, 2) . " bytes ";

			$cache->log[] =
				'<div class="debug-log">' .
				'<div class="debug-label">Fenix_Debug::logPeak()</div>' .
				'<div class="debug-time">Time: ' . sprintf('%.5F сек.', $time) . '</div>' .
				'<div class="debug-memory">Mem peak: ' . $memory . '</div>' .
				'<div class="debug-separator"></div>' .
				'</div>';
		}

		/**
		 * Вывод лога
		 * @return bool|string
		 */
		static function fetch() {
			/** не выводим статистику для других пользователей у которых прошел сбор статистики */
			if(self::$enabled === false || (!Fenix::isDev() && XHPROF_ENABLED)) {
				return false;
			}

			$cache = self::getCache();

			self::logPeak();

			$html = '';

			$html .= '
		        <style>
				        .debug-log-wrap{
					        margin: 40px 20px 20px;
						    padding: 20px;
						    border: 2px solid;
						    border-radius: 5px;
						    border-color: #bbbbbb #797979 #797979 #bbbbbb;
						    background: #f3f3f3;
						    box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.31);
					        font-family: "Courier New", Courier, monospace;
						    font-size: 14px;
						    line-height: 1.6;
				        }
				        
					    .debug-log{
					        overflow: hidden;
					        padding: 3px 0;
					    }
					    
					    .debug-log .debug-label{
					        padding: 3px 0;
					        width: 450px;
					        float: left;
					    }
					    
					    .debug-log .debug-time{
					        padding: 3px 0;
					        float: left;
					        width: 200px;
					    }
					    
					    .debug-log .debug-memory{
					        padding: 3px 0;
					        float: left;
					        width: 400px;
					    }
					    
					    .debug-log .debug-separator{
					
					    }
		        </style>
            ';

			$html .= '<div class="debug-log-wrap">';

			$html .= '</br>----</br>';
			$html .= sprintf('%.5F сек.', Fenix::$time) . ' Создано объектов ' . Fenix::$objectCreated . ' Загружено объектов ' . Fenix::$objectLoaded;
			$html .= '</br>----</br>';


			if($cache->log) {
				$html .= implode("\n", $cache->log);
			}

			$profiler = Zend_Registry::get('db')->getProfiler();
			$totalTime = $profiler->getTotalElapsedSecs();
			$queryCount = $profiler->getTotalNumQueries();
			$longestTime = 0;
			$longestQuery = null;
			$list = array();
			$listTime = array();
            $_query_list = array();

			foreach($profiler->getQueryProfiles() as $query) {
				if($query->getElapsedSecs() > $longestTime) {
					$longestTime = $query->getElapsedSecs();
					$longestQuery = $query->getQuery();
				}
				$key = md5($query->getQuery());
				$_query_list[] = $key;
				if(isset($list[ $key ])) {
					$list[ $key ][ count($list[ $key ]) ] = $query->getQuery();

					$listTime[ $key ][ count($listTime[ $key ]) ] = $query->getElapsedSecs();

				} else {
					$list[ $key ] = array($query->getQuery());
					$listTime[ $key ] = array($query->getElapsedSecs());
				}
			}

			$unique = array_unique($_query_list);

            foreach ($unique as $item) {
                if(($key = array_search($item, $_query_list)) !== false){
                    unset($_query_list[$key]);
                }
			}

            $duplicates_count = count($_query_list);


			$html .= 'Выполнено <strong>' . $queryCount . '</strong> запросов за <strong>' . $totalTime . '</strong>  секунд' . "<br />\n";
			$html .= 'Среднее время выполнения запроса: <strong>' . $totalTime / $queryCount . '</strong> секунд' . "<br />\n";
			$html .= 'Запросов в секунду: <strong>' . $queryCount / $totalTime . "</strong><br />\n";
			$html .= 'Самое длительное время запроса: <strong>' . $longestTime . "</strong><br />\n";
			$html .= "Самый длительный запрос:<br /> \n<strong>" . $longestQuery . "</strong><br />\n";
            $html .= '<br/>';
            $html .= 'Дублирующихся запросов <strong>' . $duplicates_count . '</strong>' . ":<br />\n";
            $html .= '<br/>';

            $html .= '<div style="font-weight: bold; border: 2px solid #ddd; padding: 5px;">';
            foreach ($_query_list as $item) {
                $html .= 'Исполнений: ' . count($list[$item]) . ', длительность первого запроса: ' . $listTime[$item][0] . "<br />\n";
                $html .= 'Запрос: ' . $list[$item][0] . "<br />\n";
                $html .= "===============================================================<br />\n";
                $html .= '<br/>';
            }
            $html .= '<div>';

			$html .= '<br/><br/>';

			//ksort($list);
			foreach($list as $key => $group) {
				$html .= '***' . $key . '*** [' . count($group) . ']</br>';

				foreach($group as $index => $query) {
					$html .= $index . ': ' . $listTime[ $key ][ $index ] . '</br>';
					//$html .= (string)$index;
					$html .= '</br>';
					$html .= $query;
					$html .= '</br>----</br>';
				}
			}

			$html .= '</div>';

			return $html;
		}
	}