<?php
class Fenix_Image_Adapter_Asido
{
    static private $_bg_color = array(255, 255, 255);

    static private function _includeAll()
    {
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.asido.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.gd.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.gd_hack.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.imagick_ext.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.imagick_ext_hack.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.imagick_shell.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.imagick_shell_hack.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.magick_wand.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.driver.shell.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.image.php";
        include_once LIB_DIR_ABSOLUTE . "Fenix/Image/Library/Asido/class.imagick.php";
    }

    static public function prepareImageName($original, $image, $width, $height, $action = 'frame')
    {
        $_tmp = explode(".", $image);
        $ext  = array_pop($_tmp);

        $imageName = md5(md5_file($original) . $action . $image . $width . $height) . ".{$ext}";

        // Первый уровень
        $levelZero = TMP_DIR_ABSOLUTE . "cache/images/";
        $levelOne  = substr($imageName, 0, 2) . '/';
        $levelTwo  = substr($imageName, 2, 3) . '/';

        //убираем ad для обхода блокировки AdBlock
        $levelOne = str_replace('ad', 'a-', $levelOne);
        $levelTwo = str_replace('ad', 'a-', $levelTwo);

        if (!is_dir($levelZero . $levelOne . $levelTwo))
            mkdir($levelZero . $levelOne . $levelTwo, 0777, true);

        return $levelOne . $levelTwo . $imageName;
    }

    static public function prepareImageUri($image)
    {

        return TMP_DIR_URL . "cache/images/" . $image;
    }

    static public function watermark($image, $watermark, $width = null, $height = null, $frame = true, $position = null, $scale = 1)
    {
        self::_includeAll();
        asido::driver('gd');

        if($position ==null)
            $position = 3003;// ASIDO_WATERMARK_TOP_RIGHT = 3003

        $original = (file_exists($image) ? $image : HOME_DIR_ABSOLUTE . $image);
        $new      = self::prepareImageName($original, $image, $width, $height, 'watermark');

        if (!file_exists(TMP_DIR_ABSOLUTE . "cache/images/" . $new)) {
            $i1 = asido::image($original, TMP_DIR_ABSOLUTE . "cache/images/" . $new);

            if (!$frame) {
                if ($width == null)
                    Asido::height($i1, $height);
                elseif ($height == null)
                    Asido::width($i1, $width);
                else
                    Asido::resize($i1, $width, $height, ASIDO_RESIZE_PROPORTIONAL);
            }
            else {
                Asido::Frame($i1, $width, $height, Asido::Color(self::$_bg_color[0], self::$_bg_color[1], self::$_bg_color[2]));
            }

            $ASIDO_WATERMARK_SCALABLE_ENABLED = 4001;
            Asido::watermark($i1, $watermark, $position, $ASIDO_WATERMARK_SCALABLE_ENABLED, $scale);

            $i1->Save(ASIDO_OVERWRITE_ENABLED);
        }

        return self::prepareImageUri($new);
    }

    static public function frameWatermark($image, $watermark, $width, $height){
        return self::watermark($image, $watermark, $width, $height);
    }


    static public function resize($image, $width = null, $height = null)
    {
        self::_includeAll();

        asido::driver('gd');

        $original = (file_exists($image) ? $image : HOME_DIR_ABSOLUTE . $image);
        $new      = self::prepareImageName($original, $image, $width, $height, 'resize');

        if (file_exists($original) && $image!="") {
            if (!file_exists(TMP_DIR_ABSOLUTE . "cache/images/" . $new)) {
                $i1 = asido::image($original, TMP_DIR_ABSOLUTE . "cache/images/" . $new);

                if ($width == null)
                    Asido::height($i1, $height);
                elseif ($height == null)
                    Asido::width($i1, $width);
                else
                    Asido::resize($i1, $width, $height, ASIDO_RESIZE_FIT);

                $i1->Save(ASIDO_OVERWRITE_ENABLED);
            }

            return self::prepareImageUri($new);
        }
    }

    static public function frame($image, $width, $height, $background = null)
    {
        self::_includeAll();
        asido::driver('gd');

        $original = (file_exists($image) ? $image : HOME_DIR_ABSOLUTE . $image);
        $new      = self::prepareImageName($original, $image, $width, $height, 'frame');

        if (!file_exists(TMP_DIR_ABSOLUTE . "cache/images/" . $new)) {
            if ($background != null)
                $_bg_color = $background;
            else
                $_bg_color = self::$_bg_color;

            $i1 = asido::image($original, TMP_DIR_ABSOLUTE . "cache/images/" . $new);

            Asido::Frame($i1,
                $width,
                $height,
                Asido::Color($_bg_color[0],
                    $_bg_color[1],
                    $_bg_color[2]));

            $i1->Save(ASIDO_OVERWRITE_ENABLED);
        }

        return self::prepareImageUri($new);
    }

    static public function grayscale($image, $width, $height, $background = null)
    {
        self::_includeAll();
        asido::driver('gd');

        $original = (file_exists($image) ? $image : HOME_DIR_ABSOLUTE . $image);
        $new      = self::prepareImageName($original, $image, $width, $height, 'grayscale');

        if (!file_exists(TMP_DIR_ABSOLUTE . "cache/images/" . $new)) {
            if ($background != null)
                $_bg_color = $background;
            else
                $_bg_color = self::$_bg_color;

            $i1 = asido::image($original, TMP_DIR_ABSOLUTE . "cache/images/" . $new);

            Asido::GreyScale($i1);

            Asido::Frame($i1,
                $width,
                $height,
                Asido::Color($_bg_color[0],
                    $_bg_color[1],
                    $_bg_color[2]));

            $i1->Save(ASIDO_OVERWRITE_ENABLED);
        }

        return self::prepareImageUri($new);
    }

    static public function adapt($image, $width, $height)
    {
        self::_includeAll();
        asido::driver('gd');

        $original = (file_exists($image) ? $image : HOME_DIR_ABSOLUTE . $image);
        $new      = self::prepareImageName($original, $image, $width, $height, 'adapt2');


        if (!file_exists(TMP_DIR_ABSOLUTE . "cache/images/" . $new)) {
            $i1 = asido::image($original, TMP_DIR_ABSOLUTE . "cache/images/" . $new);

            $originalInfo = getimagesize($original);

            if ( ($width / $height) < ($originalInfo[0] / $originalInfo[1])) {
                Asido::height($i1, $height);
                $i1->Save(ASIDO_OVERWRITE_ENABLED);

                $imageInfo = getimagesize($i1->target());

                $X = ($imageInfo[0] - $width) / 2;
                $W = $width;

                $Y = 0;
                $H = $height;

            }
            else {
                Asido::width($i1, $width);
                $i1->Save(ASIDO_OVERWRITE_ENABLED);

                $imageInfo = getimagesize($i1->target());

                $X = 0;
                $W = $width;

                $Y = ($imageInfo[1] - $height) / 2;
                $H = $height;
            }

            Asido::Crop($i1, $X, $Y, $W, $H);
            $i1->Save(ASIDO_OVERWRITE_ENABLED);
        }

        return self::prepareImageUri($new);
    }

}
