<?php

use Intervention\Image\ImageManager;
use Intervention\Image\Image;

class Fenix_Image_Adapter_Intervention
{

    private $image = null;
    private $original_image = null;
    private $options = array('driver' => 'gd');

    private $actions = array();

    public function __construct($image)
    {
        $this->original_image = $image;
        $manager = new ImageManager($this->options);
        $this->image = $manager->make($image);

        return $this;
    }

    /**
     *
     * Формируем имя и путь для кеша на основе параметров и методов вызванных у обработчика
     *
     * @return string
     */
    private function prepareImageName()
    {
        $ext = pathinfo($this->original_image, PATHINFO_EXTENSION);

        $action = '';
        foreach ($this->actions as $key => $_action) {
            foreach ($_action as $arg) {
                if (!is_numeric($arg) && !is_string($arg) && $arg != null) {
                    $reflection = new ReflectionFunction($arg);
                    if ($reflection->isClosure()) {
                        $arg = $reflection->__toString();
                    }
                }
                $action .= $key . $arg;
            }
        }

        $imageName = md5(md5_file($this->original_image) . $action . $this->original_image) . ".{$ext}";

        // Первый уровень
        $levelZero = TMP_DIR_ABSOLUTE . "cache/images/";
        $levelOne = substr($imageName, 0, 2) . '/';
        $levelTwo = substr($imageName, 2, 3) . '/';

        $path = $levelZero . $levelOne . $levelTwo;


        if (!is_dir($path))
            mkdir($path, 0777, true);

        return $levelOne . $levelTwo . $imageName;
    }


    public function __call($method, $args)
    {
        // создаем объект ImageManager
//        if ($this->image == null && $method == 'make') {
//            $this->original_image = $args[0];
//            $image_path = (file_exists($this->original_image) ? $this->original_image : HOME_DIR_ABSOLUTE . $this->original_image);
//
//            $manager = new ImageManager($this->options);
//            $this->image = $manager->make($image_path);
//        } else {
//            $this->actions[$method] = $args;
//        }

        //если в свойстве image объект не Image класса то фолс
        if (!$this->image instanceof Image) {
            return false;
        }

        $this->actions[$method] = $args;

        return $this;
    }

    /**
     *
     * Применяем все штуки к изображению которые попали в action
     *
     * @return Image|mixed|null
     */
    public function execute()
    {
        $result = $this->image;
        foreach ($this->actions as $action => $params) {
            try {
                $result = call_user_func_array(array($result, $action), $params);

            } catch (Exception $e) {
                Fenix::dump($e);
            }
        }
        return $result;
    }

    /**
     *
     * Получаем изображение из кеша или нет
     *
     * @return string
     */
    public function compile()
    {
        $cache_name = $this->prepareImageName();
        if (!file_exists(CACHE_DIR_ABSOLUTE . "images/" . $cache_name)) {

            $result = $this->execute();

            $result->save(CACHE_DIR_ABSOLUTE . "images/" . $cache_name);
        }
        return Fenix_Image_Adapter_Asido::prepareImageUri($cache_name);
    }

}
