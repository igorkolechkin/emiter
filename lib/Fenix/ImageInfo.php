<?php
class Fenix_ImageInfo
{
    /**
     * Преобразование размера файла из bite в указнную единицу измерения
     *
     * @integer $sizeBite
     * @string $binaryAttachments
     * @bool $withUnit
     *
     * return string
     */
    static public function getConvertImageWeight($sizeInBite, $binaryAttachments = 'kb', $withUnit = false)
    {
        $metrics = array(
            'kb','mb','gb'
        );

        // если нет такой единицы измерения, то возвращаем в байтах
        if(!in_array($binaryAttachments, $metrics)){
            return $sizeInBite;
        }

        $indexMetric = array_search($binaryAttachments, $metrics);

        for($metric = 0; $metric <= $indexMetric; $metric++){
            $sizeInBite /= 1024;

            if($metric == $indexMetric){
                $unit = $withUnit ? $metrics[$indexMetric] : '';
                return number_format($sizeInBite, 2) . ' ' . $unit;
            }
        }
    }

    /**
     * Информация об загружаемом изображении
     *
     * @string $imageFile (path to tmp image)
     *
     * return array
     */
    static public function getImageInfo($image)
    {
        $imageSize = getimagesize(HOME_DIR_ABSOLUTE.$image);

        $imageInfo = array(
            'width'  => $imageSize[0],
            'height' => $imageSize[1],
            'size'   => filesize(HOME_DIR_ABSOLUTE.$image)
        );

        return $imageInfo;
    }
}