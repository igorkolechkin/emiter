<?php

class Fenix_Engine_Database
{
    /**
     * Версия библиотеки
     */
    const VERSION = '0.0.3';

    private $_tableName = '',
        $_xml = null,
        $_action = '',
        $_columns = array(),
        $_metadata = null,
        $_app = null,
        $_file = null;

    /**
     * @staticvar
     * @var Fenix_Engine
     */
    static private $_instance = null;

    static function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new Fenix_Engine_Database();

        return self::$_instance;
    }

    public function setDatabaseTemplate($template)
    {
//         Fenix_Debug::log('set ' . $template . ' begin');
        /* $apcName = $template;
         if(apc_fetch($apcName))
             $this->_xml = apc_fetch($apcName);
         else{*/

        //если столбцы в свойстве есть то при смене источника очищаем это
        if(!empty($this->_columns)){
            $this->_columns = array();
        }

        if (is_array($template)) {
            $this->_attributeset
                = $template[1];
            $template = $template[0];
        }

        list($app, $template) = explode('/', $template);

        $this->_app = $app;

        $fileLocal = APP_DIR_ABSOLUTE . 'Local/' . ucfirst($app) . DS . APP_ETC_DIR . 'attributes' . DS . $template . '.xml';
        $fileFenix = APP_DIR_ABSOLUTE . 'Fenix/' . ucfirst($app) . DS . APP_ETC_DIR . 'attributes' . DS . $template . '.xml';


        if (file_exists($fileLocal)) {
            $this->_file = $fileLocal;
        } elseif (file_exists($fileFenix)) {
            $this->_file = $fileFenix;
        } else {
            throw new Exception('XML файл-шаблон базы данных не найден. Проверьте файл <strong>' . $this->_file . '</strong>');
        }

        $this->_xml = new Zend_Config_Xml($this->_file, null, array(
            'allowModifications' => true
        ));
        /*apc_store($apcName, $this->_xml,20);
    }*/

        //Fenix_Debug::log('set ' . $template . ' end');
        return $this;
    }

    public function getXml()
    {
        return $this->_xml;
    }

    public function getAttributesetList()
    {
        $_list = array();

        foreach ($this->_xml->creator->form AS $_attributeset => $_blocks) {
            $_list[$_attributeset] = (object)array(
                'label' => $_blocks->label,
                'details' => $_blocks->details
            );
        }

        return new ArrayObject($_list, ArrayObject::ARRAY_AS_PROPS);
    }

    public function getAttributesetListFormatted(array $_options)
    {
        $_list = $this->getAttributesetList();
        $return = '<dl>';
        $Smarty = Fenix_Smarty::getInstance();

        foreach ($_list AS $_attributeset => $_attributesetOptions) {

            $Smarty->assign('attributeset', $_attributeset);
            $url = $Smarty->fetchString($_options['url']);

            $return .= '<dt><a href="' . $url . '" class="list-group-item">' . $_attributesetOptions->label . '</a></dt>';
            $return .= '<dd style="margin-bottom:10px;">' . $_attributesetOptions->details . '</dd>';
        }
        $return .= '</dl>';
        return $return;
    }

    public function prepare()
    {
        if ($this->isMd5Actual())
            return $this;

        $Db = Zend_Registry::get('db');
        $DbConfig = $Db->getConfig('prefix');
        $this->_tables = $Db->listTables();
        $this->_table = $DbConfig['prefix'] . $this->_xml->table->name;

        // Если таблицы нет в списке
        if (!in_array($this->_table, $this->_tables)) {
            $this->createTable($this->_xml->table->name);

            if (isset($this->_xml->table->metadata))
                $this->setTableMetadata($this->_xml->table->metadata);
        } else {
            $this->alterTable($this->_xml->table->name);
        }

        // Данные колонок таблицы
        $columns = array();

        if (isset($this->_xml->fields->column->{0})) {
            $columns = $this->_xml->fields->column;
        } else {
            $columns[] = $this->_xml->fields->column;
        }

        foreach ($columns AS $columnOptions) {
            $column = Fenix_Engine_Database_Column::getInstance();

            $column->setName($columnOptions->name);
            $column->setType($columnOptions->type);

            if (isset($columnOptions->lenght))
                $column->setLenght($columnOptions->lenght);

            if (isset($columnOptions->null))
                $column->setNull(true);

            if (isset($columnOptions->default))
                $column->setDefault($columnOptions->default);

            if (isset($columnOptions->collation))
                $column->setCollation($columnOptions->collation);

            if (isset($columnOptions->attributes))
                $column->setAttributes($columnOptions->attributes);

            if (isset($columnOptions->index))
                $column->setIndex($columnOptions->index);

            if (isset($columnOptions->autoincrement))
                $column->setAutoIncrement(true);

            if (isset($columnOptions->comment))
                $column->setComment($columnOptions->comment);

            if (isset($columnOptions->splitByLang))
                $column->setSplitByLang(true);

            $this->setColumn($column);
        }

        return $this;
    }

    public function execute()
    {
        if ($this->isMd5Actual())
            return;

        $this->saveMd5();

        if ($this->_tableName == null)
            return;

        $Db = Zend_Registry::get('db');
        $DbConfig = $Db->getConfig('prefix');
        $tableName = $DbConfig['prefix'] . $this->_tableName;

        $lang = Fenix_Language::getInstance();
        $defaultLang = $lang->getDefaultLanguage();

        switch ($this->_action) {
            case 'createTable':
                if (!in_array($tableName, $Db->listTables())) {
                    // Собираем запрос до кучи
                    $result = 'CREATE TABLE ' . $tableName . " (\n";
                    $result .= '`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,';

                    $columns = array();
                    foreach ($this->_columns AS $column) {
                        $columns[] = $column->getColumn();
                    }

                    //да тут костыль! А я ХЗ откуда тут дублируется столбец которого нет
                    $columns = array_unique($columns);

                    $result .= implode(",\n", $columns) . "\n";
                    $result .= ') ' . $this->_metadata;

                    // Выполняем запрос
                    $Db->query($result);
                }
                break;

            case 'alterTable':

                //получаем подробности по текущей таблице
                $tableInfo = $Db->describeTable($tableName);

                $query = array();
                $exists_cols = array('id');

                foreach ($this->_columns AS $column) {
                    /*
                     * Изменение колонки должо происходить в несколько этапов
                     * 1. Если колонка уже разбита на языки, тогда работаем по сценарию:
                     *    1. Переименовываем колонку с языком по умолчанию в колонку без префикса языка
                     *    2. Удаляем остальные колонки
                     * 2. Если колонка не разбита на языки:
                     *    1. Переименовываем колонку на колонку с префиксом
                     *    2. Добавляем остальные префиксы 
                     */

                    /*
                     * Этап 1
                     * Проверяем разбита ли колонка на языки, для этого проверяем наличия колонки с префиксом языка
                     */

                    //сейчас столбец есть и разбит на языки
                    if (array_key_exists($column->getName() . '_' . $defaultLang->name, $tableInfo)) {
                        // Колонка уже разбита на языки, но мы, возможно не хотим убирать разбивку на языки, а просто изменить колонку
                        if (!$column->getSplitByLang()) {
                            /*
                             * Этап 1. Шаг 1.
                             * Так, колонка уже разбита на языки. Значит, нам необходимо выполнить пункт 1.1
                             */
                            $columnName = $column->getName() . '_' . $defaultLang->name;
                            //$column->setSplitByLang(false);
                            $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $columnName . '` ' . $column->getColumn();
                            $exists_cols[] = $column->getName();
                            /*
                             * Этап 1. Шаг 2.
                             * Убиваем ненужные колонки с другими языками
                             * Проходим циклом по языкам и убиваем все, кроме языка по умолчанию
                             */
                            foreach ($lang->getLanguagesList() AS $langName => $langOptions) {
                                if ($langName != $defaultLang->name && array_key_exists($column->getName() . '_' . $langName, $tableInfo)) {
                                    $query[] = $this->dropColumn($tableName, $column, $langName);
                                }
                            }
                        } else {
                            // Да! Мы не хотим удалять разбивку, просто изменяем...
                            foreach ($lang->getLanguagesList() AS $langName => $langOptions) {
                                if (array_key_exists($column->getName() . '_' . $langName, $tableInfo)) {
                                    $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $column->getName() . '_' . $langName . '` ' . $column->_assembleColumn($column->getName() . '_' . $langName);
                                } else {
                                    $query[] = $this->addColumn($tableName, $column, $langName);
                                }
                                $exists_cols[] = $column->getName() . '_' . $langName;
                            }
                        }
                    } /*
                     * Этап 2
                     * Если колонка не разбита на языки
                     */
                    else {
                        //новый столбец разбит на языки
                        if ($column->getSplitByLang()) {
                            //такой колонки не существует неязыковой
                            if (!array_key_exists($column->getName(), $tableInfo)) {
                                // Добавляем колонку разбитую на языки
                                foreach ($lang->getLanguagesList() AS $langName => $langOptions) {
                                    //если нашли разбивку на других языках меняем столбцы нет, то создаем
                                    if (array_key_exists($column->getName() . '_' . $langName, $tableInfo)) {
                                        $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $column->getName() . '_' . $langName . '` ' . $column->_assembleColumn($column->getName() . '_' . $langName);
                                    } else {
                                        $query[] = $this->addColumn($tableName, $column, $langName);
                                    }
                                    $exists_cols[] = $column->getName() . '_' . $langName;
                                }
                            } else {
                                //$column->setSplitByLang(false);

                                $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $column->getName() . '` ' . $column->_assembleColumn($column->getName() . '_' . $defaultLang->name);

                                /*
                                 * Этап 2. Шаг 2.
                                 * Добавляем колонки с другими языками
                                 */
                                foreach ($lang->getLanguagesList() AS $langName => $langOptions) {
                                    if ($langName != $defaultLang->name) {
                                        //если поля нет то создаем
                                        if (!array_key_exists($column->getName() . '_' . $langName, $tableInfo)) {
                                            $query[] = $this->addColumn($tableName, $column, $langName);
                                        } else {
                                            //если есть то обновляем
                                            $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $column->getName() . '_' . $langName . '` ' . $column->_assembleColumn($column->getName() . '_' . $langName);
                                        }
                                    }
                                    $exists_cols[] = $column->getName() . '_' . $langName;
                                }
                            }
                            //новый столбце не разбит на языки
                        } else {
                            //столбца нет, создаем, есть обновляем
                            if (!array_key_exists($column->getName(), $tableInfo)) {
                                $query[] = $this->addColumn($tableName, $column);
                            } else {
                                $query[] = 'ALTER TABLE ' . $tableName . ' CHANGE `' . $column->getName() . '` ' . $column->_assembleColumn($column->getName());
                            }
                            $exists_cols[] = $column->getName();
                            // Выполняем запрос
                        }
                    }
                }

                //создаем и меняем столбцы
                foreach ($query as $_query) {
                    $Db->query($_query);
                }

                /**
                 * Удаление колонок из таблицы в случае удаления их из файла-шаблона
                 */
                $new_table_data = $Db->describeTable($tableName);
                $new_table_data = array_keys($new_table_data);

                $cols_to_delete = array_diff($new_table_data, $exists_cols);

                if (count($cols_to_delete) > 0) {
                    foreach ($cols_to_delete as $col) {
                        $query = null;
                        $query = 'ALTER TABLE ' . $tableName . ' DROP `' . $col . '`';
                        $Db->query($query);
                    }
                }

                break;
        }
    }

    /**
     *
     * Создание столбца
     *
     * @param $table_name
     * @param Fenix_Engine_Database_Column $column
     * @param null $langName
     * @return string
     */
    private function addColumn($table_name, Fenix_Engine_Database_Column $column, $langName = null)
    {
        if ($langName === null) {
            return 'ALTER TABLE ' . $table_name . ' ADD ' . $column->_assembleColumn($column->getName());
        }
        return 'ALTER TABLE ' . $table_name . ' ADD ' . $column->_assembleColumn($column->getName() . '_' . $langName);
    }

    /**
     *
     * Удаление столбца
     *
     * @param $table_name
     * @param Fenix_Engine_Database_Column $column
     * @param null $langName
     * @return string
     */
    private function dropColumn($table_name, Fenix_Engine_Database_Column $column, $langName = null)
    {
        if ($langName === null) {
            return 'ALTER TABLE ' . $table_name . ' DROP `' . $column->getName() . '`';
        }
        return 'ALTER TABLE ' . $table_name . ' DROP `' . $column->getName() . '_' . $langName . '`';
    }

    /**
     * Проверяет наличие таблицы в БД, создает по шаблону если ее нет
     * @throws Zend_Exception
     */
    public function createIfNotExists()
    {
        if ($this->isMd5Actual())
            return;


        if ($this->_tableName == null)
            return;

        $Db = Zend_Registry::get('db');
        $DbConfig = $Db->getConfig('prefix');
        $tableName = $DbConfig['prefix'] . $this->_tableName;

        $lang = Fenix_Language::getInstance();
        $defaultLang = $lang->getDefaultLanguage();

        if (!in_array($tableName, $Db->listTables())) {
            // Собираем запрос до кучи
            $result = 'CREATE TABLE ' . $tableName . " (\n";
            $result .= '`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY';

            $columns = array();

            foreach ($this->_columns AS $column) {
                if ($column->getName()) {
                    $columns[] = $column->getColumn();
                }
            }
            if (count($columns) > 0) {
                $result .= ', ';
            }
            $result .= implode(",\n", $columns) . "\n";
            $result .= ') ' . $this->_metadata;

            // Выполняем запрос
            $Db->query($result);
        }

        $this->saveMd5();
    }

    public function saveMd5()
    {
        $md5 = md5_file($this->_file);
        //$cache = Fenix_Cache::getCache('core/engine_tables');
        $dirPath = CACHE_DIR_ABSOLUTE . 'table' . DS;
        if (is_dir($dirPath) == false) {
            mkdir($dirPath, 0777, true);
        }
        $hash = $dirPath . $this->_app . '-' . basename($this->_file) . '.md5';

        $f = fopen($hash, 'w');
        fwrite($f, $md5);
        fclose($f);
    }

    public function isMd5Actual()
    {
        $md5 = md5_file($this->_file);
        //$cache = Fenix_Cache::getCache('core/engine_tables');
        $file = CACHE_DIR_ABSOLUTE . 'table' . DS . $this->_app . '-' . basename($this->_file) . '.md5';

        if (!file_exists($file))
            return false;

        $md5hash = file($file);

        if (!isset($md5hash[0]))
            return false;

        if ($md5hash[0] == null)
            return false;

        if ($md5 == $md5hash[0])
            return true;

        return false;
    }

    public function createTable($name)
    {
        $this->_tableName = $name;
        $this->_action = 'createTable';
        $this->setTableMetadata();
        return $this;
    }

    public function alterTable($name)
    {
        $this->_tableName = $name;
        $this->_action = 'alterTable';
        return $this;
    }

    public function setTableMetadata($data = 'ENGINE MyISAM')
    {
        $this->_metadata = $data;
        return $this;
    }

    public function setColumn(Fenix_Engine_Database_Column $info)
    {
        $this->_columns[] = $info;
        return $this;
    }

    public function getColumnsList(array $options = array())
    {
        if (!array_key_exists('all_columns', $options)) {
            $options['all_columns'] = true;
        }

        $prefix = null;
        if (array_key_exists('prefix', $options)) {
            $prefix = $options['prefix'] . '.';
        }

        $columns = array($prefix . 'id');
        $lang = Fenix_Language::getInstance();

        foreach ($this->_xml->fields->column AS $_column) {
            if (isset($_column->splitByLang)) {
                if ($options['all_columns'] === true) {
                    foreach ($lang->getLanguagesList() AS $_name => $_lang) {
                        $columns[] = $prefix . $_column->name . '_' . $_lang->name;
                    }
                }

                $columns[] = $prefix . $_column->name . '_' . $lang->getCurrentLanguage()->name . ' AS ' . $_column->name;
            } else {
                $columns[] = $prefix . $_column->name;
            }
        }

        return $columns;
    }
}