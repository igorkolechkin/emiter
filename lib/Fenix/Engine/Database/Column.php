<?php
class Fenix_Engine_Database_Column
{
    private $_name          = '',
            $_type          = 'INT',
            $_lenght        = 11,
            $_null          = false,
            $_default       = null,
            $_collation     = null,
            $_index         = null,
            $_autoincrement = false,
            $_attributes    = null,
            $_comment       = false,
            $_split         = false;
    
    /**
     * @staticvar
     * @var Fenix_Engine
     */
    static private $_instance = null;
    
    static function getInstance()
    {
        #if (self::$_instance == null)
            self::$_instance = new Fenix_Engine_Database_Column();
       
        return self::$_instance;
    }
    
    public function setName($name)                { $this->_name = $name;             return $this; }
    public function setType($type)                { $this->_type = $type;             return $this; }
    public function setLenght($lenght)            { $this->_lenght = $lenght;         return $this; }
    public function setNull($null = false)        { $this->_null = $null;             return $this; }
    public function setDefault($default)          { $this->_default = $default;       return $this; }
    public function setCollation($collation)      { $this->_collation = $collation;   return $this; }
    public function setAttributes($attributes)    { $this->_attributes = $attributes; return $this; }
    public function setIndex($index)              { $this->_index = $index;           return $this; }
    public function setAutoIncrement($ai)         { $this->_autoincrement = $ai;      return $this; }
    public function setComment($comment)          { $this->_comment = $comment;       return $this; }
    public function setSplitByLang($split = true) { $this->_split = $split;           return $this; }
    
    public function getName()           { return $this->_name;          }
    public function getType()           { return $this->_type;          }
    public function getLenght()         { return $this->_lenght;        }
    public function getNull()           { return $this->_null;          }
    public function getDefault()        { return $this->_default;       }
    public function getCollation()      { return $this->_collation;     }
    public function getAttributes()     { return $this->_attributes;    }
    public function getIndex()          { return $this->_index;         }
    public function getAutoIncrement()  { return $this->_autoincrement; }
    public function getComment()        { return $this->_comment;       }
    public function getSplitByLang()    { return $this->_split;         }
    
    public function _assembleColumn($column)
    {
        $result   = array();
        $result[] = '`' . $column . '`';
        $result[] = $this->_type;
        
        if ($this->_lenght != null)
            $result[] = '(' . $this->_lenght . ')';
        
        if ($this->_collation != null)
            $result[] = 'COLLATE ' . $this->_collation;
        
        if ($this->_attributes)
            $result[] = $this->_attributes;
        
        if ($this->_null)
            $result[] = 'NULL';
        else
            $result[] = 'NOT NULL';
    
        if ($this->_default != null)
            $result[] = 'DEFAULT \'' . $this->_default . '\'';
    
        if ($this->_autoincrement)
            $result[] = 'AUTO_INCREMENT';
    
        if ($this->_index != null)
            $result[] = $this->_index;
    
        if ($this->_comment != null)
            $result[] = 'COMMENT \'' . $this->_comment . '\'';
        
        return implode(' ', $result); 
    }
    
    public function getColumn()
    {
        if ($this->_split == false) {
            return $this->_assembleColumn($this->_name);
        }
        else {
            $lang = Fenix_Language::getInstance()->getLanguagesList();
            
            $result = array();
            foreach ($lang AS $name => $options)
                $result[] = $this->_assembleColumn($this->_name . '_' . $name);
            
            return implode(",\n", $result);
        }
    }
}