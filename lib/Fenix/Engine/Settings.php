<?php
    class Fenix_Engine_Settings extends Fenix_Engine_Abstract
    {
        public function getDefaultsForForm()
        {
            $lang   = Fenix_Language::getInstance();

            $Model  = $this->getModel();
            $Select = $Model->setTable('settings')
                            ->select()
                            ->setIntegrityCheck(false);

            $Select ->from(array(
                'p' => $Model->getTable('settings')
            ));

            $Select ->joinLeft(array(
                's' => $Model->getTable('settings_storage')
            ), 'p.id = s.param_id');

            $Select ->order('p.param asc');

            $Result = $Model->fetchAll($Select);

            $Defaults = array();

            foreach ($Result AS $_result) {
                if ($_result->split_by_lang == '1') {
                    foreach ($lang->getLanguagesList() AS $_langName => $_langOptions) {
                        $Storage = Fenix::getModel()->setTable('settings_storage')->fetchRow(
                            'param_id = ' . (int) $_result->param_id . ' AND lang = \'' . $_langName . '\''
                        );

                        if ($Storage != null) {
                            $_column = 'value_' . $_result->type;
                            $Defaults[$_result->param . '_' . $Storage->lang] = $Storage->{$_column};
                        }
                    }
                }
                else {
                    $_column = 'value_' . $_result->type;
                    $Defaults[$_result->param] = $_result->{$_column};
                }
            }

            return $Defaults;
        }

        public function getXmlSection($section)
        {
            $SettingsXml = Fenix::assembleXml('settings.xml', true);

            if (isset($SettingsXml->form->{$section}))
                return $SettingsXml->form->{$section};

            return false;
        }

        public function getSections()
        {
            $SettingsXml = Fenix::assembleXml('settings.xml');

            // Актуальный список разделов
            $sectionsList  = array();

            if (array_key_exists('params', $SettingsXml)) {
                foreach ($SettingsXml['params'] AS $_section => $_options) {
                    $sectionsList[$_options['offset']] = (object) array(
                        'name'   => $_section,
                        'label'  => $_options['label'],
                        'icon'   => $_options['icon'],
                        'module' => $_options['module']
                    );
                }
            }

            ksort($sectionsList);
            $sectionsList = array_values($sectionsList);

            return new ArrayObject($sectionsList);
        }

        public function getSection($name)
        {
            $SettingsXml = Fenix::assembleXml('settings.xml');

            // Актуальный список разделов
            $section = null;

            if (array_key_exists('params', $SettingsXml)) {
                foreach ($SettingsXml['params'] AS $_section => $_options) {
                    if ($_section == $name) {
                        $section = array(
                            'name'  => $_section,
                            'label' => $_options['label'],
                            'icon'  => $_options['icon']
                        );
                    }
                }
            }

            $Object = new Fenix_Object(array('data' => $section));

            return $Object;
        }

        public function loadAll()
        {
            if (Fenix::getRequest()->getAccessLevel() == 'backend') {
                $data = $this->_loadAll();
            }
            else {
                $cache = Fenix_Cache::getCache('core/settings');
                if ($cache !== false) {
                    $cacheId = 'FenixEngine_Settings_Storage';

                    if (!$data = $cache->load($cacheId)) {
                        $data = $this->_loadAll();
                        $cache->save($data, $cacheId);
                    }
                }
                else {
                    $data = $this->_loadAll();
                }
            }

            Zend_Registry::set('FenixEngine_Settings_Storage', $data);
        }

        public function _loadAll()
        {
            $lang   = Fenix_Language::getInstance();

            $Model  = $this->getModel();
            $Select = $Model->setTable('settings')
                            ->select()
                            ->setIntegrityCheck(false);

            $Select ->from(array(
                'p' => $Model->getTable('settings')
            ));

            $Select ->joinLeft(array(
                's' => $Model->getTable('settings_storage')
            ), 'p.id = s.param_id');

            $Select->where('s.lang = ?', $lang->getCurrentLanguage()->name);
            $Select->where('p.split_by_lang = ?', '1');
            $Select->group('s.lang');
            $Select->group('p.id');
            $Select ->order('p.param asc');

            $splitByLangParams = $Model->fetchAll($Select);
            $Select->reset(Zend_Db_Select::WHERE);

            $Select->where('s.lang = ?', '');
            $Select->where('p.split_by_lang = ?', '0');
            $noSplitParams = $Model->fetchAll($Select);
            $params = array();
            foreach (array_merge($noSplitParams->toArray(), $splitByLangParams->toArray()) AS $_param) {
                $_param = (object) $_param;

                if ($_param->split_by_lang == '1') {
                    $_column = 'value_' . $_param->type;
                    $value   = $_param->{$_column};
                }
                else {

                    $_column = 'value_' . $_param->type;

                    $value   = null;
                    switch ($_param->html) {

                        case 'image':
                            $imageStream = $_param->{$_column};

                            if ($imageStream != null) {
                                $options     = unserialize($_param->options);
                                $imagePath   = TMP_DIR_ABSOLUTE . 'cache/images/';
                                $imageUrl    = TMP_DIR_URL . 'cache/images/';
                                $imageFile   = md5($imageStream) . '.' . $options['pathinfo']['extension'];

                                if (!file_exists($imagePath . $imageFile)) {
                                    $stream = fopen($imagePath . $imageFile, 'w');
                                    fputs($stream, $_param->{$_column});
                                    fclose($stream);
                                }

                                $value = $imageUrl . $imageFile;
                            }
                            break;

                        default:
                            $value = $_param->{$_column};
                    }
                }

                $params[$_param->param] = $value;
            }

            return $params;
        }

        public function set($paramName, $paramValue)
        {
            $lang        = Fenix_Language::getInstance();
            $_paramName  = explode('_', $paramName);
            $param_lang = array_pop($_paramName);
            if ($lang->isLanguageExists($param_lang)) {
                $paramName = implode('_', $_paramName);
            }

            $Model   = $this->getModel()
                            ->setTable('settings');
            $Storage = $this->getModel()
                            ->setTable('settings_storage');

            // Обновляемый параметр
            $param = $Model->fetchRow(
                $Model->getAdapter()->quoteInto('param = ?', $paramName)
            );

            if ($param == null){
                return;
            }

            // Подготовка хранилища
            if ($param->split_by_lang == '1') {
                $data = array(
                    'param_id' => $param->id,
                    'lang' => $param_lang,
                    'value_' . $param->type => $paramValue,
                );

                $storageTest = $Storage->fetchRow('param_id = ' . (int) $param->id . ' AND lang = \'' . $param_lang . '\'');
                if ($storageTest == null) {
                    $Storage->insert($data);
                }
                else {
                    $Storage->update($data, 'id = ' . (int) $storageTest->id);
                }
            }
            else {
                $storageTest = $Storage->fetchRow('param_id = ' . (int) $param->id);
                $options     = unserialize($param->options);

                // Выполняем манипуляции с типами данных
                switch ($param->html) {
                    case 'image':

                        if ($paramValue === false) {
                            $options    = array();
                            $paramValue = null;
                        }
                        else {
                            if (file_exists($paramValue)) {
                                $options['imageinfo'] = getimagesize($paramValue);
                                $options['pathinfo']  = pathinfo($paramValue);
                                //Fenix::dump($options);
                                $paramValue           = file_get_contents($paramValue);
                            }
                        }
                        break;
                }

                // Обновляем опции
                $Model->update(array(
                    'options' => serialize($options)
                ), $Model->getAdapter()->quoteInto('id = ?', $param->id));

                // Подготавливаем массив для записи в хранилище параметров
                $data = array(
                    'param_id' => $param->id,
                    'value_' . $param->type => $paramValue
                );

                // Производим запись
                if ($storageTest == null) {
                    $Storage->insert($data);
                }
                else {
                    $Storage->update($data, 'id = ' . (int) $storageTest->id);
                }
            }

            return $paramValue;
        }

        public function get($param)
        {
            $SettingsStorage = Zend_Registry::get('FenixEngine_Settings_Storage');

            if (array_key_exists($param, $SettingsStorage))
                return $SettingsStorage[$param];

            return;
        }

        public function saveSettings($req)
        {
            foreach ((array) $req->getPost() AS $_param => $_value) {
                $this->set($_param, $_value);
            }
        }

        public function updateSettings()
        {
            // Формируем список параметров, на основе файлов шаблонов
            $SettingsXml = Fenix::assembleXml('settings.xml');

            // Актуальный список параметров
            $actualList  = array();

            if (array_key_exists('params', $SettingsXml)) {
                foreach ($SettingsXml['params'] AS $_section => $_params) {

                    $_list = array();

                    if (isset($_params['param'][0])) {
                        $_list = $_params['param'];
                    }
                    else {
                        $_list[] = $_params['param'];
                    }
                    foreach ($_list AS $_paramOptions) {

                        if (!array_key_exists('splitByLang', $_paramOptions)) {
                            $_paramOptions['splitByLang'] = '0';
                        }

                        $actualList[$_paramOptions['name']] = $_paramOptions;
                    }
                }
            }

            $SettingsModel = $this->getModel()
                                  ->setTable('settings');

            foreach ($actualList AS $_paramName => $_paramOptions) {
                $Select = $SettingsModel->select()
                                        ->from($SettingsModel, array('id', 'param'))
                                        ->where('param = ?', $_paramName)
                                        ->limit(1);
                $Result = $SettingsModel->fetchRow($Select);


                if ($Result == null) {
                    $paramId = $SettingsModel->insert(array(
                        'param'         => $_paramName,
                        'type'          => $_paramOptions['type'],
                        'html'          => $_paramOptions['html'],
                        'split_by_lang' => $_paramOptions['splitByLang']
                    ));
                    $this->getModel()->setTable('settings_storage')->insert(array(
                        'param_id' => $paramId,
                    ));
                }
                else {
                    $SettingsModel->update(array(
                        'param'         => $_paramName,
                        'type'          => $_paramOptions['type'],
                        'html'          => $_paramOptions['html'],
                        'split_by_lang' => $_paramOptions['splitByLang']
                    ), $this->getModel()->getAdapter()->quoteInto('id = ?', $Result->id));
                }
            }

            return $this;
        }
    }