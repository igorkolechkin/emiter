<?php

trait Fenix_Traits_ImageCollection{
    public function getImage()
    {
        if ($this->getData('image') != '') {
            return HOME_DIR_URL . $this->getData('image');
        }else{
            return Fenix::getModel('core/themes')->getLogo();
        }
    }

    public function getImageFrame($width, $height)
    {
        if ($this->getData('image') != '') {
            return Fenix_Image::frame($this->getData('image'), $width, $height);
        }else{
            return Fenix::getModel('core/themes')->getLogo();
        }
    }

    public function getImageAdapt($width, $height)
    {
        if ($this->getData('image') != '') {
            return Fenix_Image::adapt($this->getData('image'), $width, $height);
        }else{
            return Fenix_Image::adapt(Fenix::getModel('core/themes')->getLogo() , $width, $height);
        }
    }

    public function getImageResize($width, $height)
    {
        if ($this->getData('image') != '') {
            return Fenix_Image::resize($this->getData('image'), $width, $height);
        }else{
            return Fenix_Image::resize(Fenix::getModel('core/themes')->getLogo() , $width, $height);
        }
    }
}