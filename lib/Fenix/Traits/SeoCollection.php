<?php

	trait Fenix_Traits_SeoCollection {
		/**
		 * SEO мета данные для страницы
		 *
		 * @param array  $page
		 * @param string $seoTemplateName
		 *
		 * @return object Fenix_Object
		 */
		private function getSeoMetadata(array $page, $seoTemplateName = '') {
			// присутствуют ли какие либо $_GET параметры
			$getParams = Fenix::getRequest()->getQuery();
			$isGetParams = !empty($getParams);
			
			/** Приоритет первый - то что указано в "странице" */

            $seo_title = '';
            $currentTitle = trim($page['title']);
            $currentSeoTitle = trim($page['seo_title']);
            if ($currentTitle != '') {
                $seo_title = $currentSeoTitle;
            }

            $seo_h1 = '';
            $currentSeoH1 = trim($page['seo_h1']);
            if ($currentSeoH1 != '') {
                $seo_h1 = $currentSeoH1;
            }

			$seo_keywords = trim($page['seo_keywords']);
			$seo_description = trim($page['seo_description']);
			$seo_text_title = isset($page['seo_text_title']) ? trim($page['seo_text_title']) : '';
			$seo_text = isset($page['seo_text']) ? trim($page['seo_text']) : '';

			/** SEO атрибуты для изображения */
			$seo_image_title = '';
			$seo_image_alt = '';

			/** Приоритет второй - то что указано в "по умолчанию" в СЕО шаблонах */
			if(!empty($seoTemplateName)) {
				$seo_def = Fenix::getModel('core/seo')->getSeoByName($seoTemplateName);

				if($seo_def != null) {
					if(empty($seo_title) && !empty($seo_def->seo_title)) {
						$seo_title = $seo_def->seo_title;
					}
					if(empty($seo_h1) && !empty($seo_def->seo_h1)) {
						$seo_h1 = $seo_def->seo_h1;
					}
					if(empty($seo_keywords) && !empty($seo_def->seo_keywords)) {
						$seo_keywords = $seo_def->seo_keywords;
					}
					if(empty($seo_description) && !empty($seo_def->seo_description)) {
						$seo_description = $seo_def->seo_description;
					}
					
					if(empty($seo_image_title) && !empty($seo_def->seo_image_title)) {
						$seo_image_title = $seo_def->seo_image_title;
					} elseif(empty($seo_def->seo_image_title)) {
						$seo_image_title = '%title%'; // значение по умолчанию
					}
					if(empty($seo_image_alt) && !empty($seo_def->seo_image_alt)) {
						$seo_image_alt = $seo_def->seo_image_alt;
					} elseif(empty($seo_def->seo_image_alt)) {
						$seo_image_alt = '%title%'; // значение по умолчанию
					}
					
					if(empty($seo_text_title) && !empty($seo_def->seo_text_title)) {
						$seo_text_title = $seo_def->seo_text_title;
					}
					if(empty($seo_text) && !empty($seo_def->seo_text)) {
						$seo_text = $seo_def->seo_text;
					}
				}
			}
			
			/** Если находимся на странице фильтра и для данного фильтра назначен свой SEO текст - выводим
			 *  В противном случае убираем SEO текст категории исключая дубли
			 */
			if($isGetParams) {
				$seo_text_title = '';
				$seo_text = '';
			}

            if ($seo_title == '') {
                $seo_title = $currentTitle;
            }

            if ($seo_h1 == '') {
                $seo_h1 = $currentTitle;
            }

            /** Формируем результатирующие данные */
			$return['title'] = Fenix::getModel('core/seo')->parseString($seo_title, $page);
			$return['h1'] = Fenix::getModel('core/seo')->parseString($seo_h1, $page);
			$return['keywords'] = Fenix::getModel('core/seo')->parseString($seo_keywords, $page);
			$return['description'] = Fenix::getModel('core/seo')->parseString($seo_description, $page);
			$return['image_title'] = Fenix::getModel('core/seo')->parseString($seo_image_title, $page);
			$return['image_alt'] = Fenix::getModel('core/seo')->parseString($seo_image_alt, $page);
			$return['seo_text_title'] = Fenix::getModel('core/seo')->parseString($seo_text_title, $page);
			$return['seo_text'] = Fenix::getModel('core/seo')->parseString($seo_text, $page);

			return new Fenix_Object(array(
				'data' => $return,
			));
		}
	}