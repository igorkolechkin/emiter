<?php
class Fenix_Exception
{
    public function __construct($exception)
    {
        header('HTTP/1.0 404 Not Found');
        try {
            $title   = "Произошла ошибка";
            if ($exception instanceof Exception) {
                $message = nl2br($exception->getMessage());
                $message.= '<div class="trace"><hr style="border: none; border-top: 1px dotted #000;" />' . $this->getTraceAsString($exception->getTrace()) . '</div>';
            }
            else {
                $message = $exception;
            }

            Fenix_Xhprof::getInstance()->setDebugMsg($message);

            $Creator = Fenix::getCreatorUI();
            $Creator ->getView()->headTitle($title);
            $Creator ->getView()->assign(array(
                'title'   => $title,
                'message' => $message
            ));

            echo $Creator->getView()->render('layout/error.phtml');

            exit;
        }
        catch (Zend_View_Exception $e) {
            $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Kernel Panic!</h4>';
            $tpl.= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
            $tpl.= nl2br($exception);
            $tpl.= '</div>';
            $tpl.= '</div>';

            Fenix_Xhprof::getInstance()->setDebugMsg($tpl);

            die ($tpl);
        }
    }

    private function getTraceAsString(array $backtrace)
    {
        $string = '';
        foreach ($backtrace as $key => $mess) {
            $args = array();
            foreach ($mess['args'] as $arg) {
                if(is_object($arg)){
                    $args[] = get_class($arg);
                }elseif(is_array($arg)){
                    $args[] = var_export($arg, true);
                }
                else{
                    $args[] = $arg;
                }
            }

            $string .= '#' . $key . ' ' . $mess['file'] . '(' . $mess['line'] . '): ' . (isset($mess['class'])?$mess['class']:'') . (isset($mess['type'])?$mess['type']:'') . $mess['function'] . '(' . implode(', ',$args) . ')' . "\n";
        }

        return nl2br($string);
    }
}