<?php
class Fenix_Resource_Model extends Zend_Db_Table_Abstract
{
    public function adapter()
    {
        return Zend_Registry::get('db');
    }

    public function getRequest()
    {
        return Fenix::getRequest();
    }
    
    public function setTable($table)
    {
        $this->_name = 'fe_' . $table;
        return $this;
    }
    
    public function getTable($table = null)
    {
        if ($table != null)
            return 'fe_' . $table;
        
        return $this->_name;
    }

    public function nextId()
    {
        $dbConfig = $this->getAdapter()->getConfig();

        $query = "SELECT AUTO_INCREMENT
				  FROM INFORMATION_SCHEMA.TABLES
				  WHERE TABLE_SCHEMA = '" . $dbConfig['dbname'] . "' AND
					    TABLE_NAME	 = '" . $this->_name . "'";

        $result = $this->getAdapter()->fetchOne($query);

        return $result;
    }
}