<?php
class Fenix_Resource_Collection
{
    private $_db     = null;
    private $_model  = null;
    private $_prefix = null;
    private $_select = null;
    private $_order  = array();


    public function __construct()
    {
        $this->_model = Fenix::getModel();
    }
    
    public function getModel()
    {
        return $this->_model;
    }
    
    protected function _setSource($source)
    {
        $this->_db = new Fenix_Engine_Database();
        $this->_db ->setDatabaseTemplate($source);
        
        // Устанавливаем таблицу
        $this->getModel()->setTable(
            $this->_db->getXml()->table->name
        );
        
        $this->_select = $this->getModel()->select();
        $this->_select->from($this->getModel(), $this->_db->getColumnsList(array(
            'all_columns' => false
        )));
        
        return $this;
    }

    protected function _getDb()
    {
        return $this->_db;
    }

    protected function _getSelect()
    {
        return $this->_select;
    }

    public function __call($method, $args)
    {
        switch(substr($method, 0, 3))
        {
            case 'set':
                $column = substr($method, 3);         
                $column = Fenix::fromCamelCase($column);
                $cond   = (isset($args[1]) ? $args[1] : '=');

                $this->_select
                     ->where('`' . $this->_prefix . $column . '` ' . $cond . ' ?', (isset($args[0]) ? $args[0] : null));
            break;
        }
        
        return $this;    
    }

    protected function _setOrder($def)
    {
        $this->_order[] = $def;
        return $this;
    }

    protected function _setLimit($l1, $l2 = null)
    {
        $this->_select->limit($l1, $l2);
        return $this;
    }

    protected function _fetchAll($select = null)
    {
        if ($select == null) {
            $select = $this->_select;
        }

        foreach ($this->_order AS $_order) {
            $select->order($_order);
        }

        return new Fenix_Object_Rowset(array(
            'data' => $this->getModel()->fetchAll(
                $select
            )->toArray()
        ));
    }

    protected function _fetchRow($select = null)
    {
        if ($select == null) {
            $select = $this->_select;
        }

        foreach ($this->_order AS $_order) {
            $select->order($_order);
        }

        $Result = $this->getModel()->fetchRow(
            $select
        );

        return new Fenix_Object(array(
            'data' => ($Result != null ? $Result->toArray() : array())
        ));
    }

}