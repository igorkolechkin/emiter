<?php
class Fenix_Cache
{
    public function getCacheList()
    {
        $cacheXml = Fenix::assembleXml('cache.xml', true);
        
        return $cacheXml;
    }
    
    static public function getCacheFromDatabase()
    {
        if (Zend_Registry::isRegistered('Fenix_Core_Cache'))
            return Zend_Registry::get('Fenix_Core_Cache');
        
        $list   = Fenix::getModel()->setTable('core_cache')->fetchAll();
        $result = array();
        
        foreach ($list AS $cache)
            $result[$cache->name] = $cache;
        
        Zend_Registry::set('Fenix_Core_Cache', $result);
        return $result;
    }
    
    static public function getCache($name)
    {
        list($app, $section) = explode('/', $name);
        
        $xml = new Zend_Config_Xml(APP_DIR_ABSOLUTE . 'Fenix/' . ucfirst($app) . DS . APP_ETC_DIR . 'cache.xml');
        
        if (!isset($xml->cache->{$section})) {
            throw new Exception("Раздел кеширования <strong>{$name}</strong> не найден");
        }
        
        $_section = $section;
        $section  = $xml->cache->{$section};
        
        $tableCacheList = Fenix_Cache::getCacheFromDatabase();

        if (isset($tableCacheList[$_section]) && $tableCacheList[$_section]->status == 0) {
            return false;
        }
        else {
            if ($section->status == '0') {
                return false;
            }
        }

        $frontendOptions = $section->frontendOptions->toArray();
        $backendOptions  = $section->backendOptions->toArray();

        if (isset($backendOptions['cache_dir'])) {
            //нет директорий, нет кеша
            if(!is_dir($dir = CACHE_DIR_ABSOLUTE . $backendOptions['cache_dir'])) return false;

            $backendOptions['cache_dir'] = $dir;
        }

        $cache = Zend_Cache::factory(
            $section->frontend, 
            $section->backend,
            $frontendOptions,
            $backendOptions
        );
        
        return $cache;
    }
    
    static public function getStatus($name)
    {
        $tableCacheList = Fenix_Cache::getCacheFromDatabase();
        
        if (isset($tableCacheList[$name])) {
            return (bool) $tableCacheList[$name]->status;
        }
        else {
            $cache = new Fenix_Cache();
            $list  = $cache->getCacheList();
            
            if (isset($list->cache->{$name}->status))
                return (bool) $list->cache->{$name}->status;
        }
    }
    
    static public function prepare($path = CACHE_DIR)
    {
        $frontendOptions = array(
           'lifetime' => 7200,
           'automatic_serialization' => true
        );
         
        $backendOptions = array(
            'cache_dir' => $path
        ); 
        
        return Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    }
}