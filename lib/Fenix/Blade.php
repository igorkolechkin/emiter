<?php

class Fenix_Blade
{

    static public $instance = null;
    static public $cache_alias = 'core/render';

    static public function render($template, array $params = array(), $cache_key = null)
    {
        $theme = Fenix::getTheme();

        $source = THEMES_DIR_ABSOLUTE . $theme->name . DS . 'design';
        $cache_dir = CACHE_DIR_ABSOLUTE . 'blade';

        if (self::$instance === null) {
            self::$instance = new duncan3dc\Laravel\BladeInstance($source, $cache_dir);
        }

        if ($cache_key !== null) {
            $lang = Fenix_Language::getInstance()->getCurrentLanguage();
            $cache = Fenix_Cache::getCache(self::$cache_alias);
            $cache_id = md5('Render_' . $template . '_' . $cache_key . '_' . $lang->name . '_' . $theme->name);
            if ($cache !== false) {
                if (!$data = $cache->load($cache_id)) {
                    $data = self::$instance->render($template, $params);
                    $cache->save($data, $cache_id);
                }
                return $data;
            }
        }

        return self::$instance->render($template, $params);
    }
}