<?php
class Fenix_Engine
{
    private $_db = null;
    
    /**
     * @staticvar
     * @var Fenix_Engine
     */
    static private $_instance = null;
    
    static function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new Fenix_Engine();
       
        return self::$_instance;
    }
    
    public function setSource($source)
    {
        $this->_db = new Fenix_Engine_Database();
        $this->_db ->setDatabaseTemplate($source);
        
        return $this;
    }

    public function getTable()
    {
        return $this->_db->getXml()->table->name;
    }

    public function columnIsSplitByLang($columnName)
    {
        if($this->_db->getXml() && $this->_db->getXml()->fields && $this->_db->getXml()->fields->column)
        {
            foreach($this->_db->getXml()->fields->column as $column){
                if($column->name == $columnName)
                {
                    $columnData = $column->toArray();
                    if(isset($columnData['splitByLang']))
                    {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            }
        }
        return false;
    }

    /**
     * Список колонок таблицы по шаблону
     * @param array $options
     * @return array
     */
    public function getColumns(array $options = array())
    {
        $prefix  = (array_key_exists('prefix', $options) ? $options['prefix'] . '.' : null);
        $columns = array($prefix . 'id');
        $lang    = Fenix_Language::getInstance()->getCurrentLanguage();
        $list    = Fenix_Language::getInstance()->getLanguagesList();

        foreach ($this->_db->getXml()->fields->column AS $_column) {
            if (isset($_column->splitByLang)) {
                foreach ($list AS $_langName => $_langOptions) {
                    if ($lang->name == $_langOptions->name) {
                        $columns[] = $prefix . $_column->name . '_' . $lang->name . ' AS ' . $_column->name;
                        $columns[] = $prefix . $_column->name . '_' . $lang->name;
                    }
                    else {
                        $columns[] = $prefix . $_column->name . '_' . $_langOptions->name;
                    }
                }
            }
            else {
                $columns[] = $prefix . $_column->name;
            }
        }

        return $columns;
    }

    /**
     * Список колонок таблицы для фронтенда только текущий язык
     * @param array $options
     * @return array
     */
    public function getFrontendColumns(array $options = array())
    {
        $prefix  = (array_key_exists('prefix', $options) ? $options['prefix'] . '.' : null);
        $columns = array($prefix . 'id');
        $lang    = Fenix_Language::getInstance()->getCurrentLanguage();
        $list    = Fenix_Language::getInstance()->getLanguagesList();

        foreach ($this->_db->getXml()->fields->column AS $_column) {
            if (isset($options['columns']) && in_array($_column->name, $options['columns']) == false) {
                continue;
            }
            if (isset($_column->splitByLang)) {
                foreach ($list AS $_langName => $_langOptions) {
                    if ($lang->name == $_langOptions->name) {
                        $columns[] = $prefix . $_column->name . '_' . $lang->name . ' AS ' . $_column->name;
                    }
                }
            }
            else {
                $columns[] = $prefix . $_column->name;
            }
        }

        return $columns;
    }

    public function uploadImage(array $options)
    {
        if (!is_dir($options['path'])) {
            mkdir($options['path'], 0777, true) or die('Не удалось создать');
        }

        if ($options['source'] != null) {

            if ($options['source']->tmp_name != null) {
                $newFilename = $options['source']->name;

                if (array_key_exists('rename', $options)) {
                    $pathinfo    = pathinfo($options['source']->name);
                    $newFilename = $options['rename'] . '.' . $pathinfo['extension'];
                }

                copy($options['source']->tmp_name, $options['path'] . $newFilename);

                return $options['relative_path']  . $newFilename;
            }
        }
    }

    public function removeImage($image)
    {
        if (file_exists($image) && !is_dir($image)) {
            unlink($image);
        }
    }
}