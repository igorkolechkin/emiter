<?php

use MatthiasMullie\Minify\JS;


class Fenix_Js
{

    private static $minify = null;

    private static $modify_time_summary = 0;

    private static $atributes = array();

    public static function add($file)
    {
        $path = BASE_DIR . $file;
        if(!file_exists($path)){
            $theme = Fenix::getTheme();
            $path = THEMES_DIR_ABSOLUTE . $theme->name . '/skin/' . $file;
            if(!file_exists($path)){
                throw new Exception('File not exist: ' . $path);
            }
        }

        self::$modify_time_summary += filemtime($path);

        if (self::$minify == null) {
            self::$minify = new JS($path);
        } else {
            self::$minify->add($path);
        }
    }


    /**
     * Set JS attributes
     *
     * @param array $data
     */
    public static function setAttributes(array $data)
    {
        self::$atributes = array_merge(self::$atributes, $data);
    }


    public static function compile()
    {
        $filename = md5(self::$modify_time_summary) . '.js';

        $pathAbsolute = TMP_DIR_ABSOLUTE . 'cache/js/' . $filename;

        //если нет кешированной версии то выполняем минимизацию
        if(!file_exists($pathAbsolute)){
            self::$minify->minify($pathAbsolute);
        }

        $attributes = '';

        if (is_array(self::$atributes) && count(self::$atributes) > 0) {
            foreach (self::$atributes as $name => $value) {
                $attributes .= ' ' . $name . '="' . $value . '"';
            }
        }

        self::$minify = null;
        self::$modify_time_summary = 0;
        return '<script type="text/javascript" ' . $attributes . ' src="'. TMP_DIR_URL . 'cache/js/' . $filename .'"></script>';
    }
}