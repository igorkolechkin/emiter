<?php
abstract class Fenix_Creator_Abstract
{
    protected $_view       = null;
    protected $_controller = null;
    
    protected $_attributes = array(
        'class',  'id',  'type',   'value',   'onclick',
        'width', 'cellpadding', 'cellspacing', 'border',
        'name', 'style', 'multiple', 'size', 'checked',
        'method', 'enctype', 'action', 'align', 'colspan',
        'placeholder', 'valign', 'onchange', 'disabled'
    );
    
    protected $_attr            = array();
    protected $_attrGroup       = null;
    protected $_option          = array();
    protected $_buttons         = array();
    protected $_SR_items        = array();
    protected $_validateEngine  = array();
    protected $_defaults        = array();
    
    public function getUri()
    {
        return "https://" . $_SERVER['HTTP_HOST'] . $this->getRequest()->getRequestUri();
    }
    
    public function addButton($button)
    {
        $this->_buttons[] = $button;
        
        return $this;
    }
    
    public function getRequest()
    {
        return Fenix::getRequest();
    }
    
    public function getMvcInstance()
    {
        return Zend_Layout::getMvcInstance();
    }
    public function getView()
    {
        return Zend_Layout::getMvcInstance()->getView();
    }
    public function getModel()
    {
        return new Fenix_Resource_Model();
    }
    
    public function setOption($param, $value = null)
    {
        $this->_option[$param] = $value;
        return $this;
    }
    public function getOption($param)
    {
        return @$this->_option[$param];
    }
    
    public function setDefaults($defaults)
    {
        if ($this->getRequest()->getPost('ajaxSend') != 1 &&
            !$this->getRequest()->isPost())
        {
            $this->_defaults = $defaults;

            if (is_array($defaults))
            {
                foreach ($defaults AS $_key => $_value)
                {
					if ($_value !== null)
	                    $this->getRequest()->setPost($_key, $_value);
                }
            }
        }
        
        return $this;
    }
    
    public function getDefaults($param)
    {
        return $this->getRequest()->getPost($param);
    }
    
    public function setAttributes($group, $attributes)
    {
        if (array_key_exists($group, $this->_attr) && is_array($this->_attr[$group]))
            $this->_attr[$group] = array_merge($this->_attr[$group], $attributes);
        else
            $this->_attr[$group] = $attributes;
        
        return $this;
    }
    public function setAttributeGroup($group)
    {
        $this->_attrGroup = $group;
        return $this;
    }
    public function getAttributeGroup()
    {
        return $this->_attrGroup;
    }
    
    public function splitByLang($value = true)
    {
        $this->setOption('splitByLang', $value);
        return $this;
    }
    
    public function wysiwyg($options = array())
    {
        if (!isset($options['toolbar']))
            $this->setOption('wysiwyg.theme', "Default");
        else
            $this->setOption('wysiwyg.theme', $options['toolbar']);
        
        $this->setOption('wysiwyg.options', $options);
        
        return $this;
    }
    
    public function parseVariables($data, $string)
    {
        if ($data instanceof Zend_Db_Table_Row)
        {
            $_array = $data->toArray();
            
            foreach ((array) $_array AS $column => $value)
            {
                $value = str_replace("''", "\"",$value);
                $value = htmlspecialchars($value);
                
                $string = str_replace("#{$column}#", $value, $string);
            }
        }
        
        return $string;
    }
    
    public function getAttributeSet($group)
    {
		$result         = '';
        $attributeArray = null;
        
        if (is_array($group))
            $attributeArray = $group;
        else
            if (isset($this->_attr[$group]))
                $attributeArray = $this->_attr[$group];
        
		if (is_array($attributeArray) && sizeof($attributeArray)>0)
		{
			foreach ($attributeArray AS $attr => $value)
			{
				if ($attr != "style")
				{
                    $result.= ' ' . $attr . '="' . $value . '"';
				}
				else
				{
					if (is_array($value) && sizeof($value)>0)
					{
						$result.= ' style="';
						
                        foreach ($value AS $style_param => $style_value)
                            $result.= $style_param . ': ' . $style_value . ';';
						
                        $result.= '"';
					}
				}
			}
		}

		return $result;
    }
    
    public function optionGroup($label)
    {
        $this->_SR_items['optgroup' . microtime()] = array(
            'type'  => 'optgroup',
            'label' => $label
        );
        
        return $this;
    }
    public function optionGroupEnd()
    {
        $this->_SR_items['optgroupend' . microtime()] = array(
            'type'  => 'optgroupend'
        );
        
        return $this;
    }
    public function option($param, $value)
    {
        $this->_SR_items[$param] = $value;
        
        return $this;
    }
    
    /**
     * Валидация поля
     * 
     * @param Zend_Validate $validateEngine
     * @return Fenix_Creator_Abstract
     */
    public function validate(Zend_Validate_Interface $validateEngine)
    {
        $this->_validateEngine[] = $validateEngine;
        return $this;
    }
    
    public function setAttribute($attr, $value)
    {
        $this->_attr[$this->_attrGroup][$attr] = $value;
        
        return $this;
    }
    
    public function __call($method, $params)
    {
        if (substr($method, 0, 3) == "set")
        {
            $method = strtolower(substr($method, 3, strlen($method)));
            
            if (in_array($method, $this->_attributes))
            {
                $this->_attr[$this->_attrGroup][$method] = $params[0];
            }
            return $this;
        }
        elseif (substr($method, 0, 3) == "get")
        {
            $method = strtolower(substr($method, 3, strlen($method)));
            return isset($this->_attr[$this->_attrGroup][$method]) 
                        ? $this->_attr[$this->_attrGroup][$method] 
                        : null;
        }
        elseif (substr($method, 0, 5) == "unset")
        {
            $method = strtolower(substr($method, 5, strlen($method)));
    
            if (in_array($method, $this->_attributes))
               unset($this->_attr[$this->_attrGroup][$method]);
    
            return $this;
        }
        else
        {
            throw new Exception("Вызов неизвестного метода: <strong>{$method}</strong>");
        }
    }
    
    public function __toString()
    {
        return $this->fetch();
    }
    public function toString()
    {
        echo $this->fetch();
    }
}