<?php
class Fenix_Creator_Form_Captcha extends Fenix_Creator_Abstract
{
    public $valid = false;

    public function __construct($name, $value=null)
    {
        $this->setOption('id', $name);
        $this->setAttributeGroup("form.element." . $name);

        $this->setClass("form-element-text")
             ->setType("text")
             ->setName($name)
             ->setValue($value);
    }
    
    public function fetch()
    {
       
        $captcha = new Zend_Captcha_Image(array(
            'name'    => 'auto_test',
            'wordLen' => 6,
            'timeout' => 300
        ));

        $captcha->setFont(VAR_DIR_ABSOLUTE . "fonts/font.ttf")
                ->setFontSize(18)
                
                ->setImgDir(TMP_DIR_ABSOLUTE . "captcha/")
                ->setImgUrl(TMP_DIR_URL . "captcha/")
                
                ->setWidth(160)
                ->setHeight(60)
                
                ->setDotNoiseLevel(10)
                ->setLineNoiseLevel(2)
                ->generate();

        $fenixCaptcha = new Zend_Session_Namespace('Fenix_Captcha');
        if (isset($_POST['captcha'])) $oldSession =  $fenixCaptcha->getIterator();
        $fenixCaptcha ->captcha = $captcha;

        $result = "<div class='captcha-img'>";
        $result .= $captcha->render();
        $result .= "</div><div class='capthca-renew'><a href='javascript:void(0);' class='renew-captcha-link'>".Fenix::lang('Обновить изображение')."</a></div>";
        $result .= "<div class='clearfix'></div>";

        $result.= '<input' . $this->getAttributeSet("form.element." . $this->getOption('id')) . '/>';
        $result.= "<input type='hidden' name='captchaId' value='{$captcha->getId()}' />";
        $result.= "<script>
        $('.renew-captcha-link').bind('click', function(){
            $.post('/newcaptcha', function( data ) {
                $( '.captcha').html( data );
            });
        });
        </script>";
        // Checking Captcha
        if (isset($_POST['captchaId'])){
            $array_copy = $oldSession->getArrayCopy();
            $word_get = $array_copy['captcha']->getWord();

            if (strtolower($_POST['captcha']) == $word_get)
            {
                $this->valid = true;
            }else{
                $this->valid = false;
                $result.= '<div class="captcha-error">'.Fenix::lang('Символы введены не верно!').'</div>';
            }
        }

        return $result;
    }
}