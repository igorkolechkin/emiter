<?php


use MatthiasMullie\Minify\CSS;


class Fenix_Css
{

    private static $minify = null;

    private static $modify_time_summary = 0;

    private static $atributes = array('rel' => 'stylesheet', 'type' => 'text/css');


    /**
     * singletone , add files to minifier
     *
     * @param $file
     */
    public static function add($file)
    {
        $path = BASE_DIR . $file;
        if(!file_exists($path)){
            $theme = Fenix::getTheme();
            $path = THEMES_DIR_ABSOLUTE . $theme->name . '/skin/' . $file;
            if(!file_exists($path)){
                throw new Exception('File not exist: ' . $path);
            }
        }

        self::$modify_time_summary += filemtime($path);

        if (self::$minify == null) {
            self::$minify = new CSS($path);
        } else {
            self::$minify->add($path);
        }
    }


    /**
     * Set CSS attributes
     *
     * @param array $data
     */
    public static function setAttributes(array $data)
    {
        self::$atributes = array_merge(self::$atributes, $data);
    }


    /**
     * compile minified css file
     * and destroy minify object instance
     *
     * @param bool $link_only
     * @return string
     */
    public static function compile($link_only = false)
    {
        $filename = md5(self::$modify_time_summary) . '.css';

        $pathAbsolute = TMP_DIR_ABSOLUTE . 'cache/css/' . $filename;

        //если нет кешированной версии то выполняем минимизацию и сохраняем в кеш
        if (!file_exists($pathAbsolute)) {
            self::$minify->minify($pathAbsolute);
        }

        self::$minify = null;
        self::$modify_time_summary = 0;

        $css_link = TMP_DIR_URL . 'cache/css/' . $filename;

        if (!$link_only) {
            $attributes = '';
            if (is_array(self::$atributes) && count(self::$atributes) > 0) {
                foreach (self::$atributes as $name => $value) {
                    $attributes .= ' ' . $name . '="' . $value . '"';
                }
            }
            return '<link href="' . $css_link . '"' . $attributes . '>';
        }

        return $css_link;
    }
}