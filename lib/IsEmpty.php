<?php
/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/Abstract.php';

class Fenix_Validate_IsEmpty extends Zend_Validate_Abstract
{
    const NOT_EMPTY = 'notEmptyError';
    
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_EMPTY => "Поле должно быть пустым"
    );

    protected $_options = array();
    
    /**
     * Constructor
     */
    public function __construct($_options = null)
    {
        if ($_options)
            $this->_options = $_options;
    }
    

    public function isValid($value)
    {
        if ($value == null || $value == '')
        {

            return true;
        }

        $this->_error(self::NOT_EMPTY);
        return false;
    }
}