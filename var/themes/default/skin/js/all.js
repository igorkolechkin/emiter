$(function () {
    $('.slider-wrapper .owl-carousel').hover(
        function () {
            $(this).addClass('hover');
        },
        function () {
            $(this).removeClass('hover');
        }
    );
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('body').addClass("fixed");
            //$('.services-read .banner-service').addClass("margin");
        }
        else {
            $('body').removeClass("fixed");
            //$('.services-read .banner-service').removeClass("margin");
        }
    });


    $("body").click(function () {
        //$(".search-block .search").hide();
        $("header .navbar-toggler").removeClass("active");
        $("#navbarNav").removeClass("active");
        $("header .navbar-toggler-catalog").removeClass("active");
        $(".category-list-small").removeClass("active");
    });

    $("header .navbar-toggler").click(function (e) {
        e.stopPropagation();
        $(".search-block .search-icon").removeClass("active");
        $(".search-block .search").removeClass("active");
        $("header .navbar-toggler-catalog").removeClass("active");
        $(".category-list-small").removeClass("active");
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            //$(".search-block .search").hide();
            $("#navbarNav").removeClass("active");
        } else {
            $(this).addClass("active");
            //$(".search-block .search").show();
            $("#navbarNav").addClass("active");
        }
    });
    $("#navbarNav").click(function (e) {
        e.stopPropagation();
    });

    $("header .navbar-toggler-catalog").click(function (e) {
        e.stopPropagation();
        $(".search-block .search-icon").removeClass("active");
        $(".search-block .search").removeClass("active");
        $("header .navbar-toggler").removeClass("active");
        $("#navbarNav").removeClass("active");
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            //$(".search-block .search").hide();
            $(".category-list-small").removeClass("active");
        } else {
            $(this).addClass("active");
            //$(".search-block .search").show();
            $(".category-list-small").addClass("active");
        }
    });
    $(".category-list-small").click(function (e) {
        e.stopPropagation();
    });


    // баннер слайдер на главной
    $('.owl-carousel.main-slider').owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        nav: false,
        pager: false,
        pagination: false,
        dots: false,
        dotData: false,
        autoPlay: 3000
        //rewind: true
    });

    // галерея в стр просмотра товара
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        infinite: false
    });
    $('.slider-nav').slick({
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: true,
        infinite: false
    });


    $(".fancy-link").fancybox({
        toolbar: true,
        arrows: true,
        infobar: false
    });

    $("#tabs-product").tabs();
    setTimeout(function () {
        $(".articles-list .article-block").addClass("animated fadeInRight");
    }, 1000);

    $(".product-block").each(function () {
        var $productBlock = $(this),
            $configurableBlock = $productBlock.find(".size-block"),
            $items = $configurableBlock.find(".hover-item"),
            productId = $productBlock.data('product');

        $productBlock.hover(function () {
            $configurableBlock.show();
        }, function () {
            $configurableBlock.hide();
        });

        $items.each(function () {
            var $this = $(this);
            $this.mouseenter(function () {
                var attr_id = $this.data('attr_id'),
                    value_id = $this.data('value_id');

                var attributes = {};
                attributes[attr_id] = value_id;

                var data = {};
                data.attributes = attributes;

                FenixProduct.updateConfigurableBlockPrice(productId, data, value_id);
            });

            $this.click(function () {
                var $this = $(this),
                    attr = $this.data('attr'),
                    attr_id = $this.data('attr_id'),
                    value = $this.data('value'),
                    value_id = $this.data('value_id'),
                    $form = $configurableBlock.find('form');

                $form.find('input[name=attr]').val(attr);
                $form.find('input[name=attr_id]').val(attr_id);
                $form.find('input[name=value]').val(value);
                $form.find('input[name=value_id]').val(value_id);

                $form.submit();
            });
        });
    });

});



/// адаптив
$(function(){
    $(".filter-trigger").click(function(){

            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(".filter-block").slideUp();
            } else {
                $(this).addClass("active");
                $(".filter-block").slideDown();
            }

    });

    $( window ).resize(function() {
        if($(window).width() > 991) {
            $(".filter-block").css({"display" : ""});
        } else {
        }
    });

});