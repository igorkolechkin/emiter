var head = document.getElementsByTagName('head')[0];

function include(scriptUrl) {
    document.write('<script src="' + FenixOptions.skinUrl + scriptUrl + '"></script>');
}

function appendCss(scriptUrl) {
    var link = '';
    link = document.createElement("link");
    link.rel = "stylesheet";
    link.href = FenixOptions.skinUrl + scriptUrl;
    head.appendChild(link);
}

var supportsTouch = 'ontouchstart' in document.documentElement || navigator.msMaxTouchPoints;

/* Page break
 ========================================================*/
(function ($) {
    $(document).ready(function () {
        var o = $(".more-separator");
        if (o.length > 0) {
            o.nextAll().wrapAll("<div class='collapse' id='collapseSeoText'></div>");
            o.parent().append("<a class='link-default show-all' data-toggle='collapse' href='#collapseSeoText' aria-expanded='false' aria-controls='collapseSeoText'>" + o.text() + "</a>");
            o.remove();
        }
    });
})(jQuery);

/* Scroll to top
 ========================================================*/
(function ($) {
    $(document).ready(function () {
        var $backTop = $("#backTop");

        $(window).scroll(function () {
            var $this = $(this);
            if ($this.scrollTop() > 200) {
                $backTop.stop(true).css({"display": "block"}).animate({"opacity": 1}, 400);
            } else {
                $backTop.stop(true).animate({"opacity": 0}, 400, function () {
                    $this.css({"display": "none"})
                });
            }
        });
        $backTop.click(function () {
            $('body, html').stop(true).animate({scrollTop: 0}, 600);
            return false;
        });
    });
})(jQuery);

/* Owl carousel
 ========================================================*/
(function ($) {
    var o = $(".owl-carousel");
    var upsell = $(".upsell-slider");
    var viewed = $(".viewed-slider");
    var related = $(".related-slider");

    if (o.length > 0 || upsell.length > 0 || viewed.length > 0) {

        include('owl_carousel/owl.carousel.min.js');
        appendCss('owl_carousel/css/owl.carousel.css');
        appendCss('owl_carousel/css/owl.theme.css');

        $(document).ready(function () {
            var settingOwlCarousel = {
                items: 1,
                loop: true,
                autoplay: false,
                autoplayTimeout: 5000,
                nav: true,
                navText: ['<span class="icon-arrow_left"></span>', '<span class="icon-arrow_right"></span>'],
                dots: false
            };

            if (o.length > 0) {
                $("#sliderOnMainPage").owlCarousel(settingOwlCarousel);
            }

            settingOwlCarousel.autoplay = false;
            settingOwlCarousel.nav = false;
            settingOwlCarousel.dots = false;
            settingOwlCarousel.margin = 30;
            settingOwlCarousel.loop = false;

            if (upsell.length > 0) {
                settingOwlCarousel.responsive = {
                    // breakpoint from 0 up
                    0: {
                        items: 1
                    },
                    // breakpoint from 480 up
                    480: {
                        items: 1
                    },
                    // breakpoint from 768 up
                    768: {
                        items: 2
                    },
                    1200: {
                        items: 4,
                        nav: true
                    }
                };
                upsell.owlCarousel(settingOwlCarousel);
            }

            if (related.length > 0) {
                settingOwlCarousel.responsive = {
                    // breakpoint from 0 up
                    0: {
                        items: 1
                    },
                    // breakpoint from 480 up
                    480: {
                        items: 1
                    },
                    // breakpoint from 768 up
                    768: {
                        items: 2
                    },
                    1200: {
                        items: 3,
                        nav: true
                    }
                };
                related.owlCarousel(settingOwlCarousel);
            }
        });
    }
})(jQuery);

/* Fancybox
 ========================================================*/
(function ($) {
    var o = $(".product-slider");

    if (o.length > 0) {
        include('slick/slick.min.js');
        appendCss('slick/slick.css');
        appendCss('slick/slick-theme.css');

        $(document).ready(function () {
            if (isMobile) {
                o.find('.slider-nav').addClass("d-none");
                $('.slider-for', o).slick();
            } else {
                // галерея в стр просмотра товара
                o.find('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: true,
                    asNavFor: '.slider-nav',
                    infinite: true
                });

                o.find('.slider-nav').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    dots: false,
                    focusOnSelect: true,
                    arrows: true,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '42px'
                });
            }

        });
    }
})(jQuery);

/* Fancybox
 ========================================================*/
(function ($) {
    var o = $("[data-fancybox]");
    if (o.length > 0) {
        include('fancybox/jquery.fancybox.min.js');
        appendCss('fancybox/jquery.fancybox.min.css');
    }
})(jQuery);

/* Follow a link
 ========================================================*/
(function ($) {
    var o = $("[data-location-url]");

    if (o.length > 0) {
        $(document).ready(function () {
            o.each(function () {
                $(this).hover(function () {
                    $(this).addClass("hover");
                }, function () {
                    $(this).removeClass("hover");
                });
                // $(this).find("img").wrap("<a href='" + $(this).data("locationUrl") + "'></a>");
                $(this).click(function () {
                    location.href = $(this).data("locationUrl")
                });
            })
        });
    }
})(jQuery);

/* Add Bootstrap Img-fluid
 ========================================================*/
(function ($) {
    var o = $(".seo-content");

    if (o.length > 0) {
        $(document).ready(function () {
            var $images = o.find("img");
            $images.each(function () {
                $(this).removeAttr("style").addClass("img-fluid");
            });
        });
    }
})(jQuery);

/* Additional Title
 ========================================================*/
(function ($) {
    var o = $(".additional-title");

    if (o.length > 0) {
        $(document).ready(function () {
            o.each(function () {
                var $this = $(this),
                    $parent = $(this).prev();

                $parent.attr("data-after", $this.text()).html("<span>" + $parent.text() + "</span>");
                $this.remove();
            });
        });
    }
})(jQuery);

/* Body
 ========================================================*/
(function ($) {
    var o = $(".contact-page");

    if (o.length > 0) {
        $(document).ready(function () {
            $("body").addClass("contact")
        });
    }
})(jQuery);

/* Favicon
 ========================================================*/
(function ($) {
    $(document).ready(function () {
        if (FenixOptions.favicon.trim() != '') {
            $.favicon(FenixOptions.favicon);
        }
    });
})(jQuery);

var $dropdownToggle = $(".form-search-container .dropdown-toggle");
var $dropdownMenu = $(".form-search-container");
/* Change Search
 ========================================================*/
(function ($) {
    $(document).ready(function () {
        $dropdownMenu.on('show.bs.dropdown', searchActivated);
        $dropdownMenu.on('hide.bs.dropdown', searchDeactivated);
    });
})(jQuery);

function searchActivated() {
    $dropdownToggle.find("i").removeClass("icon-search").addClass("icon-delete");
}

function searchDeactivated() {
    $dropdownToggle.find("i").removeClass("icon-delete").addClass("icon-search");
}

/* Change Header on Mobile
 ========================================================*/
(function ($) {

    include('js/jquery.header-change.js');

    $(document).ready(function () {
        $("header").header_change();
    });
})(jQuery);

/* Add Show/Hide button on Mobile for filter products
 ========================================================*/
(function ($) {
    var o = $(".filter-block");

    if (o.length > 0) {
        $(document).ready(function () {
            changeFilterBlock();
            $(window).resize(changeFilterBlock);
        });
    }

})(jQuery);

var isMobile = false;

function checkWindowWidth() {
    var currentWidth = window.innerWidth || document.documentElement.clientWidth;

    if (currentWidth <= 768) {
        isMobile = true;
    }
    else if (currentWidth > 768) {
        isMobile = false;
    }
}

function changeFilterBlock() {
    var $filterBlock = $(".filter-block");
    var $btn = $(".btn-filter");

    if (isMobile) {
        if (!$filterBlock.hasClass("collapse")) {
            $filterBlock.addClass("collapse");
        }
        if ($btn.hasClass("d-none")) {
            $btn.removeClass("d-none");
        }
    } else {
        $filterBlock.removeClass('collapse');
        $btn.addClass("d-none");
    }
}

(function ($) {
    checkWindowWidth();
    $(window).resize(checkWindowWidth);
})(jQuery);

(function ($) {
    $(".product-card").each(function () {
        var $productBlock = $(this),
            $configurableBlock = $productBlock.find(".size-block"),
            $items = $configurableBlock.find(".hover-item"),
            productId = $productBlock.data('product');

        $productBlock.hover(function () {
            $configurableBlock.show();
        }, function () {
            $configurableBlock.hide();
        });

        $items.mouseenter(function () {
            var $this = $(this);
            var attr_id = $this.data('attr_id'),
                value_id = $this.data('value_id');

            var attributes = {};
            attributes[attr_id] = value_id;

            var data = {};
            data.attributes = attributes;

            FenixProduct.updateConfigurableBlockPrice(productId, data, value_id);
        });
    });

    $(".hover-item").click(function () {
        var $this = $(this),
            attr = $this.data('attr'),
            attr_id = $this.data('attr_id'),
            value = $this.data('value'),
            value_id = $this.data('value_id'),
            $form = $this.parents(".size-block").find('form');

        $form.find('input[name=attr]').val(attr);
        $form.find('input[name=attr_id]').val(attr_id);
        $form.find('input[name=value]').val(value);
        $form.find('input[name=value_id]').val(value_id);

        $form.submit();
    });
})(jQuery);

var $productSection = $(".product");
var hasCloned = false;
var $productLeftContainer = $(".product-left-container");
var offsetProduct = $productLeftContainer.offset();
var leftPositionProduct = offsetProduct.left - 8;
var offsetBottom = $productSection.find(".review-list-container").offset();

function positionProduct() {
    var currentWidth = window.innerWidth || document.documentElement.clientWidth;

    if (currentWidth > 1024) {
        $productLeftContainer.css({
            top: 0,
            left: '15px',
            width: ($productLeftContainer.parent().outerWidth() - 30)
        });
    } else {
        $productLeftContainer.removeAttr("style");
    }
}

function fixedProduct() {
    var offset = $productSection.find("h1").offset();
    var currentWidth = window.innerWidth || document.documentElement.clientWidth;

    if (currentWidth > 1024) {
        if ($(this).scrollTop() >= offset.top) {
            if ($(this).scrollTop() >= offsetBottom.top) {
                $productLeftContainer.removeClass("position-fixed").css({
                    bottom: 0,
                    top: 'auto',
                    left: '15px'
                });
            } else {
                $productLeftContainer.addClass("position-fixed").css({
                    left: leftPositionProduct,
                    top: '25px'
                });
                $(".product-price-container", $productLeftContainer).show();
            }
        }
        else {
            $(".product-price-container", $productLeftContainer).hide();
            $productLeftContainer.removeClass("position-fixed").css({
                top: 0,
                left: '15px'
            });
        }
    }
}


(function ($) {
    var $priceContainer = $(".product-price-container").clone();

    if (!hasCloned) {
        $productLeftContainer.append($priceContainer);
        $(".product-price-container", $productLeftContainer).hide();
        hasCloned = true;
    }
    positionProduct();
    fixedProduct();
    $(window).scroll(fixedProduct).resize(positionProduct);
})(jQuery);

(function ($) {
    var $menuItems = $("#mainMenu .navbar-nav > li");
    $menuItems.hover(
        function () {
            $(this).addClass("hover-item");
        }, function () {
            $(this).removeClass("hover-item");
        }
    );
})(jQuery);
/* Pre loader
 ========================================================*/
(function ($) {
    $(window).on("load", function () {
        var $preloader = $(".pre-loader"),
            $body = $("body");

        $preloader.fadeOut(500);
        $body.removeClass("loading");
    });
})(jQuery);