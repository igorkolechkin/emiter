(function ($) {
    "use strict";
    $.fn.header_change = function (options) {
        var defaults = {
            headerChangeTarget: $(this), // Target the current HTML markup you wish to replace,
            headerScreenWidth: 768 // set the screen width you want header to change
        };

        options = $.extend(defaults, options);

        // get browser width
        var currentWidth = window.innerWidth || document.documentElement.clientWidth;

        var responsiveFlag = false;

        var clone;

        return this.each(function () {

            var header = options.headerChangeTarget;
            var screenWidth = options.headerScreenWidth;

            var changeHeader = function (status) {
                status == 'enable' ? cloneHeader() : originalHeader();
            };

            var originalHeader = function () {
                if (!!clone) {
                    header.removeClass("mobile").empty().append(clone);
                }
            };

            var cloneHeader = function () {
                clone = header.clone(true, true);
                var $logo = $(".logo", header).clone();
                var $navbar = $("#mainMenu").clone();
                var $nav = $(".nav", header).html();
                var $search = $(".form-search-container", header).clone(true, true);
                var $sidebar = $("#checkoutSidebar").clone(true, true);

                header.empty().addClass("mobile");
                header.append($logo).find(".logo").wrap("<div class='container-fluid'></div>");
                header.append($navbar).find(".container").append($search).append($sidebar);
                header.append($navbar).find(".navbar-nav").append($nav);
            };

            if (currentWidth <= screenWidth && responsiveFlag == false) {
                changeHeader('enable');
                $("body").addClass("mobile-body");
                responsiveFlag = true;
            }
            else if (currentWidth > screenWidth) {
                changeHeader('disable');
                $("body").removeClass("mobile-body");
                responsiveFlag = false;
            }

            $(window).resize(function () {
                currentWidth = window.innerWidth || document.documentElement.clientWidth;

                if (currentWidth <= screenWidth && responsiveFlag == false) {
                    changeHeader('enable');
                    $("body").addClass("mobile-body");
                    responsiveFlag = true;
                }
                else if (currentWidth > screenWidth) {
                    changeHeader('disable');
                    $("body").removeClass("mobile-body");
                    responsiveFlag = false;
                }
            });
        });
    };
})(jQuery);