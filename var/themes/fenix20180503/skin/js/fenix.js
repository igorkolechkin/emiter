var FenixCustomer = {
    addFavorites: function (trigger, pid) {
        trigger = $(trigger);
        var count = parseInt($('#fav_count').html());

        if (trigger.find('img').attr('class') == 'add') {
            trigger.parent().attr('class', 'favorites done');
            trigger.find('img').attr('class', 'remove');
            trigger.find('img').attr('src', '/var/themes/default/skin/images/heart_on.png');
            trigger.find('div').html(FenixOptions.wishlistdone);
            count = count + 1;
            $('#fav_count').html(count);
        }
        else {
            document.location.href = FenixOptions.langCode + '/customer/favorites';
            return false;
        }
        $.post('/customer/favorites/process', {pid: pid}, function (data) {
        });
        return false;
    },
    deleteFavorites: function (trigger, pid) {
        console.log(FenixOptions);
        if (FenixOptions.auth == false) {
            self.location = '/customer/session/login';
            return false;
        }
        var count = parseInt($('#fav_count').html());
        count = count - 1;
        $('#fav_count').html(count);
        $.post('/customer/favorites/process', {pid: pid}, function (data) {
        });
        return false;
    },

    addTracking: function (trigger, pid) {
        if (trigger.html() == 'Следить за ценой') {
            trigger.html('Не следить за ценой');
        }
        else {
            trigger.html('Следить за ценой');
        }
        $.post('/customer/tracking/process', {pid: pid}, function (data) {
        });
        return false;
    },

    addCompare: function (id, elem) {
        elem.blur();
        $.post('/catalog/compare/add', {
            'id': id
        }, function (result) {
            $(elem).addClass('active');
            $(elem).html('<span>Уже в сравнении</span><img width="20px" src="/var/themes/default/skin/images/libra_on.png" alt=""/>');
            $(elem).attr('onclick', 'FenixCustomer.deleteCompare(' + id + ',this)');
            //self.location = '/catalog/compare';
            console.log(result);
            FenixCompare.updateSidebar();
        });
        return false;
    },
    deleteCompare: function (id, elem) {
        elem.blur();
        $.post('/catalog/compare/delete', {
            'id': id
        }, function (result) {
            $(elem).removeClass('active');
            $(elem).html('<span>Добавить к сравнению</span><img width="20px" src="/var/themes/default/skin/images/libra_off.png" alt=""/>');
            $(elem).attr('onclick', 'FenixCustomer.addCompare(' + id + ',this)');
            FenixCompare.updateSidebar();
        });
        return false;
    },

    checkUser: function (email, password) {
        alert(email.val());
    }
};

var FenixCheckout = {
    buyOneClickDialog: function (pid, qty, product_name) {
        return FenixUI.dialog($('#buyOneclickDialog'), {
            width: 420,
            open: function () {
                var $dialog = $('#buyOneclickDialog');
                $('.ui-widget-overlay').click(function () {
                    FenixUI.closeDialog();
                });

                $dialog.find('input[name="pid"]').val(pid);
                $dialog.find('input[name="qty"]').val(qty);
                $dialog.find('input[name="product"]').val(product_name);
                $dialog.find('input[name="product_url"]').val(location.href);
            }
        });
    },
    addProduct: function (url, productId) {
        if (FenixOptions.debugProductCard) console.log('*** addProduct ***');
        if (FenixOptions.debugProductCard) console.log('Добавляем товар productId:' + productId + ' в корзину');

        var _this = this;
        var data = FenixProduct.getSelectedProductAttributes(productId);

        if (FenixOptions.debugProductCard) console.log('Выбранные параметры товара:');
        if (FenixOptions.debugProductCard) console.log(data);

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                _this.openDialog();
            }
        });
        return false;
    },
    addProductParts: function (url, productId) {
        var data = FenixProduct.getSelectedProductAttributes(productId);

        console.log(data);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                self.location = FenixOptions.langUrl + '/checkout?pay_variants=privat24part'
            }
        });

        return false;
    },
    removeProduct: function (id) {
        var _this = this;
        $('#checkout-product-' + id).remove();
        $.post('/checkout/process/remove', {id: id}, function () {
            $.post('/checkout/process/sidebar', {}, function (data) {
                $('#checkoutSidebar').html(data);
                FenixCheckout.loadCheckoutDialogContent();
            })
        });
        if ($('.cart-item').length == 0) {
            $('#checkoutTotalSum').html(0);
            $('#checkoutProductQty').html(0);
        }
        //this.updateCheckoutTotalBlock();

        return false;
    },

    plusQty: function (qtyBlock, itemTotalBlock) {
        var _value = parseInt(qtyBlock.val());
        var unitPrice = parseFloat(qtyBlock.data('price'));
        var productId = qtyBlock.data('product-id');
        _value++;
        qtyBlock.val(_value);
        itemTotalBlock.html((_value * unitPrice).toFixed(2));
        //this.updateCheckoutTotalBlock();

        FenixCheckout.updateProductQty(productId, _value);

        //$('.checkout-configurable select:first-of-type').trigger('change');
        /* qtyBlock.closest('.order-product')
         .find('.kit-product .qty .value').html(_value);*/

        return false;
    },

    minusQty: function (qtyBlock, itemTotalBlock) {
        var _value = parseInt(qtyBlock.val());
        var unitPrice = parseFloat(qtyBlock.data('price'));
        var productId = qtyBlock.data('product-id');

        if (_value <= 1) {
            qtyBlock.val(1);
            return false;
        }
        _value--;
        qtyBlock.val(_value);
        itemTotalBlock.html((_value * unitPrice).toFixed(2));
        // this.updateCheckoutTotalBlock();

        FenixCheckout.updateProductQty(productId, _value);

        /*qtyBlock.closest('.order-product')
         .find('.kit-product .qty .value').html(_value);*/
        //$('.checkout-configurable select:first-of-type').trigger('change');
        return false;
    },
    updateProductQty: function (productId, qty) {
        var url = '/checkout/process/qty';
        $.ajax({
            type: "POST",
            url: url,
            data: {
                product_id: productId,
                qty: qty
            },
            success: function () {
                FenixCheckout.loadCheckoutDialogContent();
            }
        });
    },
    updateCheckoutItemTotal: function (qtyBlock, itemTotalBlock) {
        //var unitPrice = parseFloat(qtyBlock.data('price'));
        var _value = parseInt(qtyBlock.val());
        var _id = parseFloat(qtyBlock.data('product-id'));
        //console.log(unitPrice);
        //console.log(_value);
        //console.log(itemTotalBlock.html());
        //itemTotalBlock.html((_value * unitPrice).toFixed(0)+'<span>грн</span>');

        this.updateProductQty(_id, _value);
    },

    updateCheckoutTotalBlock: function () {

    },

    openDialog: function () {
        return FenixUI.dialog($('#checkoutDialog'), {
            width: 840,
            dialogClass: "checkout-dialog",
            open: function () {
                FenixCheckout.loadCheckoutDialogContent();
                $('.ui-widget-overlay').click(function () {
                    FenixUI.closeDialog();
                });
            }
        });
    },

    loadCheckoutDialogContent: function () {
        var _this = this;
        $.ajax({
            url: '/checkout/process',
            type: "POST",
            data: {},
            beforeSend: function () {
                $("#checkoutDialogLoader").show()
            },
            success: function (data) {
                $('#checkoutDialogContent').html(data);
                var $checkoutDialog = $("#checkoutDialog");
                if ($(".ui-dialog-content", $checkoutDialog).length > 0) {
                    $checkoutDialog.dialog("option", "position", {
                        my: "center",
                        at: "center",
                        of: window
                    });
                }
                $("#checkoutDialogLoader").hide();
                FenixCheckout.loadCheckoutSidebar();
            },
            error: function () {
                _this.error();
                $("#checkoutDialogLoader").hide();
                $('#checkoutDialogContent').html('Сервис временно недоступен1');
            }
        });

        $.ajax({
            url: '/checkout/process/order',
            type: "POST",
            data: {},
            beforeSend: function () {
                $("#checkoutDialogLoader").show()
            },
            success: function (data) {
                $('#orderProductsTable').html(data);
                $("#checkoutDialogLoader").hide();
                FenixCheckout.loadCheckoutSidebar();
            },
            error: function () {
                _this.error();
                $("#checkoutDialogLoader").hide();
                $('#orderProductsTable').html('Сервис временно недоступен2');
            }
        });

        return false;
    },
    loadCheckoutSidebar: function () {
        $.post('/checkout/process/sidebar', {}, function (data) {
            $('#checkoutSidebar').html(data);
            var o = $("#paymentPrivat24Part");
            if (o.length > 0) {
                $(".active", o).trigger("click");
            }
        });
    },
    error: function () {
        return false;
    }
};

var FenixCompare = {
    holder: null,
    init: function () {
        this.holder = $('#compareHolder');
        this.holder.find('.image, .head').click(function () {
            console.log('open');
            FenixCompare.toogle();
        });
        $('body').click(function () {
            FenixCompare.close();
        });
        $('#compareHolder').click(function (e) {
            e.stopPropagation();
        })
    },
    toogle: function () {
        if (this.holder.find('.categories').hasClass('active')) {
            this.close();
        }
        else {
            this.open();
        }

    },
    close: function () {
        this.holder.find('.categories').fadeOut('fast');
        this.holder.find('.categories').removeClass('active')
    },
    open: function () {
        this.holder.find('.categories').fadeIn('fast');
        this.holder.find('.categories').addClass('active')
    },
    remove: function (elem, category) {
        var id = $(elem).closest('.product-block').data("product");
        var index = $(elem).closest('td').index();

        $.post('/catalog/compare/remove/id/' + id + '/category/' + category, {}, function () {
            $('.features-table tr').each(function () {
                $(this).find('td').eq(index).remove();
            });
            $('.features-table').css('width', $('#product-wrapper td').length * 230);

            FenixCompare.updateSidebar();
            //Удалили все товары уходим главную
            if ($('#product-wrapper td').length == 1) {
                self.location = '/';
            }
        });


    },
    removeCategory: function (elem, id) {
        $(elem).parent().remove();
        $.post('/catalog/compare/delete', {
            category: id
        }, function () {
            //снимаем с товаров
            $('.product-block').each(function () {
                if (Fenix.strpos($(this).data('category'), "'" + id + "'")) {
                    $(this).find('.compare-button').removeClass('active');
                    $(this).find('.compare-button').html('<span>Добавить к сравнению</span>');
                    $(this).find('.compare-button').attr('onclick', 'FenixCustomer.addCompare(' + $(this).data('product') + ',this)');
                }
            });
            //снимаем в карте товара
            if ($('.product-card').length > 0) {
                if (Fenix.strpos($('.product-card').data('category'), "'" + id + "'")) {
                    $('.product-card').find('.compare-button').removeClass('active');
                    $('.product-card').find('.compare-button').html('<span>Добавить к сравнению</span>');
                    $('.product-card').find('.compare-button').attr('onclick', 'FenixCustomer.addCompare(' + $('.product-card').data('product') + ',this)');
                }
            }

            FenixCompare.updateSidebar();
            /*if($('.compare-list').length>0)
             self.location = '/'*/
        });
    },
    removeCategoryAll: function (elem, id) {
        //$(elem).parent().remove();
        $.post('/catalog/compare/delete', {
            category: id
        }, function () {
            self.location = '/';
        });
    },
    updateSidebar: function () {
        $('#compareHolder').load('/catalog/compare/sidebar');
        console.log('updateSidebar');
    },
    showWarning: function () {
        $('#compareHolder .warning').show();
        return false;
    },
    addCompare: function (id, elem) {
        elem.blur();
        $.post('/catalog/compare/add', {
            'id': id
        }, function (result) {
            $(elem).addClass('active');
            $(elem).html('<span>Уже в сравнении</span>');
            $(elem).attr('onclick', 'FenixCompare.deleteCompare(' + id + ',this)');
            //self.location = '/catalog/compare';
            console.log(result);
            FenixCompare.updateSidebar();
        });
        return false;
    },
    deleteCompare: function (id, elem) {
        elem.blur();
        $.post('/catalog/compare/delete', {
            'id': id
        }, function (result) {
            $(elem).removeClass('active');
            $(elem).html('<span>Добавить к сравнению</span>');
            $(elem).attr('onclick', 'FenixCompare.addCompare(' + id + ',this)');
            FenixCompare.updateSidebar();
        });
        return false;
    }
};

var FenixUI = {
    loadingBlack: function ($elem) {
        var height = $elem.height();
        $elem.html('');
        $elem.append('<div class="ui-loading black"></div>');
        return $elem;
    },
    loadingOverlay: function ($elem) {
        var height = $elem.height();
        $elem.append('<div class="ui-loading overlay"></div>');
        return $elem;
    },
    loading: function ($elem) {
        var height = $elem.height();
        $elem.append('<div class="ui-loading"></div>');
        return $elem;
    },
    loadingStop: function ($elem) {
        $elem.find('.ui-loading').remove();
        return $elem;
    },
    dialog: function ($container, dialogOptions) {
        FenixUI.closeDialog();

        if (!('modal' in dialogOptions)) {
            dialogOptions.modal = true;
        }

        if (!('position' in dialogOptions)) {
            dialogOptions.position = {
                my: "center",
                at: "center",
                of: window
            }
        }

        if (!('open' in dialogOptions)) {
            dialogOptions.open = function () {
                $('.ui-widget-overlay').click(function () {
                    FenixUI.closeDialog();
                });
            }
        }

        if (!('title' in dialogOptions) && $container.data('title') != '') {
            dialogOptions.title = $container.data('title');
        }

        dialogOptions.resizable = false;
        dialogOptions.draggable = false;

        $container.dialog(dialogOptions);

        return false;
    },
    closeDialog: function () {
        $('.ui-dialog-content').dialog('close');
    },
    callbackDialog: function () {
        return FenixUI.dialog($('#callbackFormDialog'), {
            width: 500,
        });
    },
    feedbackDialog: function () {
        return FenixUI.dialog($('#callbackFormDialogFeedback'), {
            width: 500,
        });
    },
    entryDialog: function () {
        return FenixUI.dialog($('#entryDialog'), {
            width: 500,
        });
    },
    loginDialog: function () {
        return FenixUI.dialog($('#loginDialog'), {
            width: 500,
        });
    },
    registrationDialog: function () {
        return FenixUI.dialog($('#registrationDialog'), {
            width: 500,
        });
    },
    messageDialog: function (data) {
        return FenixUI.dialog($('#messageDialog').html(data), {
            width: 500,
        });
    },
    resumeDialog: function (data) {
        return FenixUI.dialog($('#resumeDialog').html(data), {
            width: 500,
        });
    },
    reviewDialog: function (data) {
        return FenixUI.dialog($('#reviewDialog').html(data), {
            width: 820,
        });
    }
};

var FenixProduct = {
    setRating: function (productId, blockStarsSelector, blockAverageSelector, blockCountSelector) {
        var url = '/api/ajax/product/rating';
        $.ajax({
            type: "POST",
            url: url,
            data: {product_id: productId},
            success: function (rating) {
                if (FenixOptions.debugProductCard) console.log('Загружаем рейтинг товара productId:' + productId);
                if (FenixOptions.debugProductCard) console.log(rating);

                if ($(blockAverageSelector).length > 0) {
                    $(blockAverageSelector).html(rating.average);

                }
                if ($(blockCountSelector).length > 0) {
                    $(blockCountSelector).html(rating.count);
                }
                if ($(blockStarsSelector).length > 0) {

                    $(blockStarsSelector).raty({
                        score: rating.average,
                        starOff: FenixOptions.skinUrl + 'images/star-off.png',
                        starOn: FenixOptions.skinUrl + 'images/star-on.png',
                        starHalf: FenixOptions.skinUrl + 'images/star-half.png',
                        readOnly: true,
                        click: function (score, evt) {
                            val = score;
                        }
                    });
                }
            }
        });
    },
    getSelectedProductAttributes: function (productId) {
        if (FenixOptions.debugProductCard) console.log('*** getSelectedProductAttributes ***');
        if (FenixOptions.debugProductCard) console.log('Собираем выбранные атрибуты товара');

        var data = {};

        //Если покупаем товар с его страницы
        if ($('#productCard' + productId).length > 0) {
            //Собираем выбранные материалы
            var attributes = {};
            var materials = {};
            var count = $('.material-list .material-item.active, .material-select .option.active').length;
            if (FenixOptions.debugProductCard) console.log('Найдено выбранных материалов:' + count);
            $('.product-configurable .material-list .material-item.active, .material-select .option.active').each(function () {
                var attributeId = $(this).data('attribute_id');
                var valueId = $(this).data('material_id');
                materials[attributeId] = valueId;
            });

            //Собираем атрибуты
            if (FenixOptions.debugProductCard) console.log('Найдено выбранных атрибутов:' + $('.configurable-block select .option.selected').length);
            $('.product-configurable .configurable-block select .option.selected').each(function () {
                var attributeId = $(this).data('attribute');
                // console.log('attributeId: '+attributeId);
                var valueId = $(this).data('value');
                //console.log('valueId: '+valueId);
                attributes[attributeId] = valueId;
            });

            //Собираем выбраные опции
            var options = {};
            if (FenixOptions.debugProductCard) console.log('Найдено выбранных опций:' + $('.extra-options .option a.active').length);
            $('.product-configurable .extra-options .option a.active').each(function (index) {
                var optionId = $(this).data('id');
                options[index] = optionId;
            });

            data['product_id'] = productId;
            data['attributes'] = attributes;
            data['materials'] = materials;
            data['options'] = options;
        }

        //Если покупаем товар с категории
        var $blockProduct = $("[data-product=" + productId + "]");

        if ($blockProduct.length > 0) {
            var $attribute = $blockProduct.find(".hover-item.selected");

            if ($attribute.length > 0) {
                var attributeId = $attribute.data('attr_id'),
                    valueId = $attribute.data('value_id'),
                    attributes = {};

                attributes[attributeId] = valueId;

                data['product_id'] = productId;
                data['attributes'] = attributes;
            }
        }

        return data;
    },
    getConfigurablePrice: function (productId, values) {
        var url = FenixOptions.langUrl + '/api/ajax/product/configurable/price';
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: {
                product_id: productId,
                values: values
            },
            success: function (data) {
                console.log(data);
                return data;
            }
        });
    },
    qtyPlus: function (id) {
        var _this = this;
        var qtyBlock = $('#' + id);
        var _value = parseInt(qtyBlock.val());
        if (isNaN(_value)) _value = 0;
        _value++;
        qtyBlock.val(_value);

        return false;
    },
    qtyMinus: function (id) {
        var _this = this;
        var qtyBlock = $('#' + id);
        var _value = parseInt(qtyBlock.val());
        if (isNaN(_value)) _value = 1;
        if (_value > 1)
            _value--;
        qtyBlock.val(_value);

        return false;
    },
    changeQty: function (_this) {
        if (isNaN(_this.value)) _this.value = 1;
        if (_this.value > 10000) _this.value = 10000;
        if (_this.value < 0 || _this.value == '') _this.value = 1;
    },
    updateProductCardPrice: function () {
        var id = $('#size').find(':selected').val();
        $('.price-variant').hide();
        $('.price-variant').removeClass('active');
        $('#' + id).show();
        $('#' + id).addClass('active');
    },
    updateConfigurableBlockPrice: function (productId, data, valueId) {
        var queueId = FenixQueue.newId(),
            url = FenixOptions.langUrl + '/catalog/ajax/configurable/price',
            $product = $("[data-product=" + productId + "]"),
            $productCost = $product.find(".product-cost"),
            $productCostOld = $product.find(".product-cost-old");

        var request = $.ajax({
            type: "POST",
            url: url,
            data: {product_id: productId, params: data},
            dataType: 'json'
        });
        request.done(function (data) {
            $product.find(".hover-item").removeClass("selected");

            if (data.price > 0) {
                $productCost.text(data.price);
            }

            if (data.price_old > 0 && $productCostOld.length > 0) {
                $productCost.text(data.price_old);
            }

            $("[data-value_id=" + valueId + "]", $product).addClass("selected");
        });
    }
};
var FenixCatalog = {
    loadMore: function (trigger) {
        var page = parseInt($(trigger).data('page'));
        var url = $(trigger).data('url');
        var count_page = $(trigger).data('page-count');
        var query_obj = Fenix.getUrlSearch();

        //добавляем к текущему урлу номер следующей страницы
        query_obj.page = page + 1;
        url += '?' + Fenix.setUrlSearch(query_obj, true);
        console.log(url);

        if (!$(trigger).hasClass('disabled')) {
            $(trigger).addClass('loading disabled');

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'action': 'load-more'
                },
                success: function (data) {
                    var pageNow = (page + 1);

                    $(trigger).removeClass('loading disabled');

                    $(trigger).data('page', pageNow);

                    $(".paginationControl .page-" + pageNow).addClass('current'); //выделяем страницу в пагинаторе

                    if (count_page <= pageNow) {
                        $(trigger).hide();//если следующая по выборке страница равна количеству - скрываем кнопку
                        $(".paginationControl .next").remove(); // удаляем кнопку "далее"
                    } else {
                        var next_url = $(".paginationControl .next").data('url').replace('%s', pageNow + 1);
                        $(".paginationControl .next").attr('href', next_url); //устанавливаем корректную ссылку на след
                                                                              // страницу
                    }

                    $('.load-more-target').append(data);
                },
                error: function (jqXHR, text, error) {
                    console.log(jqXHR, text, error);
                }
            });
        }
    }
};
var Fenix = {
    strpos: function (haystack, needle, offset) { // Find position of first occurrence of a string
        //
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        var i = haystack.indexOf(needle, offset); // returns -1
        return i >= 0 ? i : false;
    },
    /** Выравнивание высоты колонок в строке
     *  в передаваемом элементе container должны лежать только колонки!
     *
     *  Author HEVLASKIS
     *  */
    updateRowsList: function (options) {
        var $container = $(options.container);
        var $items = $(options.items) || $container.children();
        var breakpoints = options.breakpoints;
        var win_w = $(window).outerWidth();
        var min_h = 10000;


        var cols = options.cols || 1; // количество выводимых колоник по умолчанию
        var cols_margin = options.cols_margin || 0; // имеется ввиду суммарный margin по горизонтали


        var items_count = $items.length;

        /** Поддержка адаптивности */
        if (breakpoints) {
            $.each(breakpoints, function (index, obj) {
                if (win_w < index) {
                    cols = obj.cols;
                    cols_margin = obj.cols_margin || 0;
                }
            });
        }

        var rows = items_count / cols;
        //var width = (100 - (cols - 1) * cols_margin) / cols;

        //$items.css('width', width + '%');

        /** перебераем строки */
        for (var j = 0; j <= rows; j++) {
            var height = 0;
            var index = 0;
            var $item = '';

            /** перебераем колонки в строке - вычисляем максимальную высоту колонки */
            for (var i = 0; i <= cols; i++) {
                $item = $($items[j * cols + i]);

                $item.css('height', 'auto');

                if ($item.outerHeight() > height) {
                    height = $item.outerHeight();
                }
            }

            /** перебераем колонки в строке - устанавливаем максимальную высоту колонки */
            for (var i = 0; i <= cols; i++) {
                $item = $($items[j * cols + i]);
                $item.css('height', height);
            }

            if (height < min_h) {
                min_h = height;
            }
        }

        $container.css('min-height', min_h);

        return false;
    },

    /** Выравнивание разных по ширене колонок в строке так,
     * чтоб последняя была впритык с права,
     * а промежуточные колонки имели одинковый отступ между друг другом
     *
     * Author HEVLASKIS
     * */
    stretchColumns: function (container, options) {
        var $this = $(container);
        var $items = $this.children(':not(.clearfix)');
        var items_count = $items.length;

        $items.css('width', 'auto');

        var container_width = parseInt($this.css('width')) - 2;//2px на погрешность
        var columns = options.cols || items_count;
        var rows = Math.ceil(items_count / columns);
        var w_0 = 0;
        var average_width = 0;
        var w_i = 0;
        var w_2n = 0;
        var m_l = 0;
        var item_index = 0;

        for (var row = 0; row < (rows); row++) {
            w_2n = 0;
            w_0 = Math.ceil(parseFloat($($items[row * columns]).css('width')));
            average_width = container_width - w_0;

            $($items[row * columns]).css('width', w_0);

            for (var i = 1; i < columns; i++) {
                item_index = row * columns + i;

                w_i = Math.ceil(parseFloat($($items[item_index]).css('width')));

                $($items[item_index]).css('width', w_i);

                w_2n += w_i + 1;//1px на погрешность
            }

            if (average_width - w_2n < 0) {
                m_l = 10; //10px
                w_i = (container_width - m_l * (columns - 1)) / columns;

                for (var k = 1; k < columns; k++) {
                    item_index = row * columns + k;
                    $($items[item_index]).css({
                        'margin-left': m_l,
                        'width': w_i
                    });
                }
            } else {

                m_l = Math.ceil((average_width - w_2n) / ($items.slice(row * columns, (row + 1) * columns - 1).length));
                for (var k = 1; k < columns; k++) {
                    item_index = row * columns + k;
                    $($items[item_index]).css('margin-left', m_l);
                }
            }
        }


        $($items).addClass('stretched');
    },
    /**
     * Вспомогательная функция для работы с GET параметрами
     * Возвращает объект текущих (активных) GET параметров
     *
     * Author HEVLASKIS
     * */
    getUrlSearch: function () {
        var vars = decodeURIComponent(location.search.substr(1));
        if (!vars) {
            return {};
        }

        vars = vars.split('&');
        var query_obj = {};

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_obj[pair[0]] === "undefined") {
                query_obj[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else {
                if (typeof query_obj[pair[0]] === "string") {
                    var arr = [
                        query_obj[pair[0]],
                        decodeURIComponent(pair[1])
                    ];
                    query_obj[pair[0]] = arr;
                    // If third or later entry with this name
                } else {
                    query_obj[pair[0]].push(decodeURIComponent(pair[1]));
                }
            }
        }

        return query_obj;
    },
    /**
     * Вспомогательная функция для работы с GET параметрами
     * осуществляет переход на страницу, где в урле будут содержаться передаваемые GET параметры
     *
     * Author HEVLASKIS
     * */
    setUrlSearch: function (query_obj, is_return) {
        var query_string = '';

        for (var key in query_obj) {
            query_string += '&' + key + '=' + query_obj[key];
        }

        query_string = query_string.substr(1);

        if (is_return) {
            return query_string;
        }

        if (query_string) {
            location.search = query_string;
        } else {
            location = location.origin + location.pathname;
        }
    },

    /**
     * Проставляем классы для активных пунктов меню, чтоб отобразить визуально, где находиться пользователь
     *
     * Author HEVLASKIS
     * */
    updateMenuClasses: function ($menu) {
        var menu_id = $menu.attr('id');

        if (menu_id) {
            $menu.find('a').filter(function (index) {
                /** Внимание this.pathname доступен только в HTML5 */
                return this.pathname == location.pathname;
            }).each(function () {
                var $elem = $(this);

                //добавляем класс current для ссылки на текущую страницу
                $elem.addClass('current');

                while ($elem.attr('id') != menu_id) {
                    if ($elem.find(' > a').length > 0) {
                        //добавляем класс active для ссылок выше по дереву
                        $elem.find(' > a').addClass('active');
                    }

                    $elem = $elem.parent();
                }
            });
        } else {
            console.log('Fenix.updateMenuClasses() requires id attribute in menu element!!!');
        }
    },
    /**
     * Доп функция для удаления sliderkit с объекта DOM
     *
     * Author HEVLASKIS
     * */
    sliderkitDestroy: function (container) {
        $(container).attr('style', '');
        $(container)
            .find('.sliderkit-nav-clip, .sliderkit-nav-clip ul, .sliderkit-nav-clip li, .sliderkit-panels')
            .attr('style', '');
    },

    /**
     * Добавляет кнопку "читать полностью" внутри передаваемого контейнера
     *
     * Внимание: работает с кнопкой "Добавить разрыв для скрытия текста"
     *               а еще, скорее всего, все сломается если делать вложенные скрытые блоки!!!!
     *
     * как работает:
     *      находит в контейнере элемент с классом .more-separator
     *      потом его родительский элемент
     *      оборачивает весь текст после кнопки в .hidden-info и скрывает его
     *      добавляет после .hidden-info кнопку
     *      пример работы http://amestate.com.ua/articles/pokupka-nedvizhimosti-v-turcii (кнопки "ЧИТАТЬ ДАЛЬШЕ")
     *
     * Author HEVLASKIS
     * */
    addMoreBtn: function (container) {
        var $btns = $(container).find('.more-separator');


        $btns.each(function () {
            var $btn = $(this);
            var $container = $btn.parent();

            //$container.children().wrapAll('<div class="hidden-info">'); //скроет весь контент в контейнере
            $btn.nextAll().wrapAll('<div class="hidden-info">'); //скроет весь контент в контейнере после кнопки
                                                                 // .more-separator
            $btn.remove();

            var text_open = $btn.text();
            var text_close = 'Свернуть';


            $container.find('.hidden-info').each(function () {
                $(this)
                    .css('display', 'none')
                    .after('<span class="more-btn" data-text-open="' + text_open + '" data-text-close="' + text_close + '">' + text_open + '</span>');
            });

            $container.find('.more-btn').on('click', function () {
                var $btn = $(this);
                var $container = $btn.prev();

                if ($container.hasClass("hidden-info")) {
                    $container.slideToggle(300);
                    $btn.html($btn.text() == $btn.data('text-open') ? $btn.data('text-close') : $btn.data('text-open'));
                }
            });
        });
        //$container.find('.more-separator').remove();
    },
    /**
     * Обертка для работы с jQuery elevateZoom - ИНИЦИАЛИЗАЦИЯ
     *
     * Author HEVLASKIS
     * */
    zoomImageInit: function (target) {
        var $target = $(target);

        $target.elevateZoom({
            zoomWindowOffetx: 30,
            cursor: "crosshair",
            easing: true,
            borderSize: 0,
            scrollZoom: true,
        });
    },
    /**
     * Обертка для работы с jQuery elevateZoom - удаление, причем всех .zoomContainer ибо блять по другому никак
     *
     * Author HEVLASKIS
     * */
    zoomImageDestroy: function (target) {
        var $target = $(target);
        $.removeData($target, 'elevateZoom'); //remove zoom instance from image
        $('.zoomContainer').remove(); // remove zoom container from DOM
    }
};

var FenixRender = {
    productHover: function ($block, productId) {
        var url = '/catalog/ajax/blocks/hover/product_id/' + productId;
        $.ajax({
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                $block.html(data);
            }
        });
    },
    productGroups: function ($block, productId, attrSysTitle) {
        var url = '/catalog/ajax/blocks/groups/product_id/' + productId + '/attr_sys_title/' + attrSysTitle;
        $.ajax({
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                $block.html(data);
            }
        });
    }
};

var FenixQueue = {
    ids: [],
    last: null,
    newId: function () {
        var id = this.ids.length;
        this.setLast(this.ids[this.ids.length - 1]);

        this.ids.push(id);
        //console.log('create ' + index);
        return id;
    },
    setLast: function (id) {
        this.last = id;
        //console.log('set ' + index);
    },
    isLast: function (id) {
        if (this.last == null) {
            //console.log('is true ' + index);
            return true;
        }
        else if (this.last < id) {
            //console.log('is true ' + index);
            return true;
        } else {
            //console.log('is false ' + index);
            return false;
        }
    }
};