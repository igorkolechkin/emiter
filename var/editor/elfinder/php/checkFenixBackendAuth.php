<?php

// Корневая директория
define("DS", DIRECTORY_SEPARATOR);
define("BASE_DIR", dirname(__FILE__) . DS . '..' . DS . '..' . DS . '..' . DS . '..' . DS);


require_once BASE_DIR . 'etc/const.php';
require_once LIB_DIR_ABSOLUTE . 'Fenix.php';


// Указываем список директорий, в которых будем искать файлы
$includePath = implode(PATH_SEPARATOR, array(
    BASE_DIR,
    APP_DIR_ABSOLUTE,
    LIB_DIR_ABSOLUTE
));

$includePath .= get_include_path();
set_include_path($includePath);


// Регистрация автозагрузчика
try {
    $autoloaderScript = 'Zend/Loader/Autoloader.php';

    if ( ! file_exists(LIB_DIR_ABSOLUTE . $autoloaderScript)) {
        throw new Exception("Автозагрузчик не найден <strong>" . $autoloaderScript . "</strong>");
    }

    require_once $autoloaderScript;

    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->registerNamespace("Fenix_");
    $autoloader->setFallbackAutoloader(true);
}
catch (Exception $e) {
    require_once('Fenix/Exception.php');
    new Fenix_Exception($e);
}

// Стартуем сессию
Zend_Session::start();

// Коннектимся к базе данных
try {
    $dbСonfigFile = ETC_DIR_ABSOLUTE . "database.xml";
    $database = new Zend_Config_Xml($dbСonfigFile, "database");

    Zend_Registry::set('database', $database);

    $db = Zend_Db::factory($database);

    Zend_Db_Table_Abstract::setDefaultAdapter($db);
    $db->query('SET NAMES utf8');

    $db->getProfiler()
        ->setEnabled(true);

    Zend_Registry::set('db', $db);

    // Сначала создается объект кэша
    $cache = Fenix_Cache::getCache('core/database_meta');

    if ($cache !== false) {
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }
}
catch (Zend_Db_Exception $e) {
    $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Сообщение базы данных</h4>';
    $tpl .= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
    $tpl .= $e->getMessage();
    $tpl .= '</div></div>';

    die($tpl);
}
catch (Exception $e) {
    $tpl = '<div style="margin: 40px;font-family: Verdana;"><h4 style="margin-left: 10px;">Сообщение базы данных</h4>';
    $tpl .= '<div style="font-family: Monaco, Verdana, Sans-serif;font-size: 12px;background-color: #ffe0e0;border: 1px solid #ff3838;color: #002166;display: block;margin: 14px 0 5px 0;padding: 12px 10px 12px 10px;">';
    $tpl .= $e->getMessage();
    $tpl .= '</div></div>';

    die($tpl);
}
//Проверяем авторизацию в админ панель
try {
    $auth = Zend_Auth::getInstance();
    $session = new Zend_Auth_Storage_Session('Fenix_Engine_Auth_backend');
    $auth->setStorage($session);
    $_identity = $auth->getIdentity();
    $user = null;
    if ($_identity instanceof Zend_Db_Table_Row) {
        $user = Fenix::getModel('core/administrators')->getAdministratorById($_identity->id);
    }
    if ($user == null || $user->login != $_identity->login || $user->password != $_identity->password) {
        header('HTTP/1.0 404 Not Found');
        exit;
    }
}
catch (Exception $e) {
    header('HTTP/1.0 404 Not Found');
    exit;
}