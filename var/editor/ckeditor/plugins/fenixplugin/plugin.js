CKEDITOR.plugins.add('fenixplugin', {
	init : function(editor) {
		editor.addCommand("morebtn", {
			canUndo : !1,
			exec    : function(editor) {
				var el = editor.document.createElement('div');
				el.setAttribute('style', 'font-weight: bold;text-transform: uppercase;border-bottom: 2px solid darkblue; text-align: center; margin: 5px');
				el.setText('Читать полностью');
				el.addClass('more-separator');
				editor.insertElement(el);
			}
		});

		editor.ui.addButton("MoreBtn", {
			label   : 'Добавить разрыв для скрытия текста',
			command : "morebtn",
			icon    : CKEDITOR.plugins.getPath('fenixplugin') + 'more_btn.png'
		});
	}
});