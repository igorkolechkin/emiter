/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.language = 'ru';
    config.filebrowserBrowseUrl = '/var/editor/elfinder/elfinder.html?mode=file';
    config.filebrowserImageBrowseUrl = '/var/editor/elfinder/elfinder.html?mode=image';
    config.filebrowserFlashBrowseUrl = '/var/editor/elfinder/elfinder.html?mode=flash';

	// Referencing the new plugin
	config.extraPlugins = 'fenixplugin';
	config.allowedContent = true;
};
