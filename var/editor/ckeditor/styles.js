﻿/**
 * Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [
    /* Object Styles */
    {
        name: 'Изображение (left)',
        element: 'img',
        attributes: { 'class': 'left float-left mr-3 mb-3' }
    },

    {
        name: 'Изображение (right)',
        element: 'img',
        attributes: { 'class': 'right float-right ml-3 mb-3' }
    },

    {
        name: 'Изображение (rounded left)',
        element: 'img',
        attributes: { 'class': 'left float-left rounded mr-3 mb-3' }
    },

    {
        name: 'Изображение (rounded right)',
        element: 'img',
        attributes: { 'class': 'right float-right rounded ml-3 mb-3' }
    }
] );